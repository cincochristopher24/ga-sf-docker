<?php
$mapEntry = $model['mapEntry'];
$locationID = $model['locationID'];
$currentCityLocIDs = array();
?>

<strong>
<?php 
	if(is_object($mapEntry)) {
		echo $mapEntry->getCountry()->getName();
	}
?>
</strong>

<br/>

<!-- CITY OF FOR EACH -->												
<?php $i=0; foreach($mapEntry->getCountry()->getCityLocationList() as $entry): ?>
	<? $displayVal = (!$entry->getIsEditable()) ? 'none' : 'block';	?>
   	<span id="city_<?php echo $entry->getID() ?>" class="editing_mode cityCtr">
		<?php echo $entry->getName(); ?>
		<a title="Remove City" style="display: <?=$displayVal?>" class="delete_link" id="removeCity_<?php echo $entry->getID() ?>" onclick="confirmDelete(this,'')">
			Remove City
		</a>
	</span>

<?php $i++; $cityLocIDs[] = $entry->getID(); $currentCityLocIDs[] = $entry->getID(); endforeach; ?>
<!-- END CITY OF FOR EACH -->

<div id="addLocationLinks_<?php echo $locationID ?>" style="float:right">
	<span style="font-size:10px">Add:</span>
	<a onclick="showCities(<?php echo $locationID ?>, 'citiesContainer_<?php echo $locationID ?>', '<?php echo implode('.', $currentCityLocIDs) ?>')">
		From List
	</a>
	<strong>|</strong>
	<a onclick="displayMap('<?php echo $locationID ?>','<?php echo $mapEntry->getCountry()->getCountryID() ?>','<?php echo $mapEntry->getCountry()->getLatitude()."_".$mapEntry->getCountry()->getLongitude() ?>')">
		New Location
	</a>
</div>


<? if ($mapEntry->getIsEditable()) { ?>

<a title="Remove Country" class="delete_link" alt="remove" id="removeCountry_<?php echo $locationID ?>" onclick="confirmDelete(this,'<?php echo implode('.', $currentCityLocIDs) ?>')">
	Remove Country
</a>
<div style="clear:left"></div>
<? } ?>
