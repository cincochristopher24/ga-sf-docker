<?php
$pinBlueCity = 'http://www.goabroad.net/facebook/mytravelmap/images/corners/ga_tinymarkerBlueTransparent.png';
$pinYellowCity = 'http://www.goabroad.net/facebook/mytravelmap/images/corners/ga_tinymarkerYellowTransparent.png';

$editButtonName = count($mapEntries) ? 'Edit' : 'Add';
$topLinks .= <<< topLinks
    <fb:fbml version="1.1">
	<fb:visible-to-owner>
  		<div class="top_links UIMediaButton_Container clearfix" id="top_links" style="padding-bottom:2px;">
			<div style="float:left;">
  			<img src="http://www.goabroad.net/facebook/mytravelmap/images/icon-refresh.gif" alt="" class="refresh_icon"/>
      		<a href="http://apps.facebook.com/my-travel-map/index.php?action=refreshProfile&box=1" id="refresh">Refresh</a>
    		</div>
			<table cellspacing="0" cellpadding="0" border="0" style="float:right;margin-left:10px;display:inline-block;">
				<tr class="UIMediaButton_top_row">
					<td class="UIMediaButton_tl"></td>
					<td class="UIMediaButton_content" rowspan="2">
						<div class="UIMediaButton_bottom clearfix">
							<a href="http://apps.facebook.com/my-travel-map/?edit">$editButtonName Country Lists</a>
						</div>
					</td>
					<td class="UIMediaButton_tr"></td>
				</tr>
				<tr class="UIMediaButton_lower_row">
					<td class="UIMediaButton_bl"></td>
					<td class="UIMediaButton_br"></td>
				</tr>
			</table>
  		</div>
    </fb:visible-to-owner>
	</fb:fbml>
topLinks;

$fbml ='<fb:wide>';

// CSS STYLE -->
$fbml .= <<< Style
<style>
	.location_info {
	  background: #e4e4e4 url("http://www.goabroad.net/facebook/mytravelmap/images/location_info_bg.gif") repeat-x;
	  border:1px solid #dadada;
	  border-top-width:0;
	  padding:8px 29px 10px;
	  position:relative;
	}

	.location_info_extended {
	  padding:8px 8px 29px 8px;
	}

	.location_details {
	  color:#666;
	  margin-top:8px;
	}

	.location_info_extended .location_details {
	  margin:2px 0 -5px;
	  font-size:10px;
	  line-height:1.45em;
	}


	.location_navigation li{
	  position:absolute;
	  text-indent:-9999px;
	  top:8px;
	}

	.location_navigation .prev {
	  left:2px;
	}

	.location_navigation .next {
	  right:2px;
	}

	.location_info_extended .location_navigation li {
	  bottom:5px;
	  top:auto;
	}

	.location_info_extended .location_navigation .prev {
	  right:24px;
	  left:auto;
	}

	.location_info_extended .location_navigation .next {
	  right:6px;
	}

	.next_link, .prev_link{
	  display:block;
	  padding:2px;
	}

	.next_link span, .prev_link span{
	  height:14px;
	  width:14px;
	  background:transparent url("http://www.goabroad.net/facebook/mytravelmap/images/navigation_buttons.png") 0 0 no-repeat;
	  display:block;
	}

	.next_link span {
	  background-position:right top;
	}

	.prev_link:hover span{
	  background-position:0 -14px;
	}

	.next_link:hover span {
	  background-position:right -14px;
	}

	.journal_photo {
	  float:left;
	  border:2px solid #ddd;
	  margin-right:7px;
	}

	.location_flag {
	  float:left;
	  margin-right:7px;
	  width:38px;
	  height:19px;
	}

	.country_name {
	  margin:0;
	  font-size:13px;
	  color:#607199;
	}

	.location_info_extended .country_name {
	  margin-left:32px;
	  font-size:11px;
	}

	.refresh_icon {
	  position:relative;
	  top:3px;
	  left:3px;
	}

	.mute {
	  color:#999 !important;
	}

	.round_button {
	  background:transparent url("http://www.goabroad.net/facebook/mytravelmap/images/button.gif") right top no-repeat;
	  color:#575757;
	  text-shadow: 1px 1px 0px #fff;
	  padding:3px 0 4px 0;
	  margin-left:15px;
	  text-decoration:none;
	  line-height:20px;
	}

	.round_button:hover {
	  text-decoration:none;
	}

	.round_button span {
	  padding:3px 10px 4px 10px;
	  background:transparent url("http://www.goabroad.net/facebook/mytravelmap/images/button.gif") left -20px no-repeat;
	  margin-left:-10px;
	}

	.markerPointer {
	  height:8px;
	  width:13px;
	  position:absolute;
	  z-index:6;
	  bottom:2px;
	}

	#container { 
		background: #aac1d6 url("http://www.goabroad.net/facebook/mytravelmap/images/world_map_facebook2.gif") no-repeat scroll 0 0; 
		color: #ffffff; 
		position: relative; 
		height: 226px; 
		width: 380px;
	}
		
	#container div { position: absolute; }
	#container img { border-style: none; }	
	.top_links { 
		margin: 0 0 5px 0; 
		padding: 0 3px; 
		text-align: right; 
		font-weight: bold; 
	}
	
	.top_links_extended {font-weight:normal;}

	.wish { 
		border: 1px solid #e6e6e6; 
		margin: 5px 0 0; 
		padding: 8px 10px; 
		color: #777; 
		font-size: 11px; 
		background-color: #fafafa; 
		line-height:1.4em;
	}
	
	.wish {
	  color:#666;
	}
	.wish_extended {font-size:9px;padding:6px 7px;}
	.wish span { color: #60718b; font-weight: bold; }
	.wish_extended span {font-size:10px;}

	.imgTrans { filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0); }
	.blueMarker { width: 12px; height: 20px; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='$pinBlue'); }
	.greenMarker { width: 12px; height: 20px; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='$pinGreen'); }
	.yellowMarker { width: 12px; height: 20px; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='$pinYellow'); }
	.shadowMarker { width: 10px; height: 17px; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='$pinShadow'); }

	.blueCityMarker { width: 12px; height: 20px; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='$pinBlueCity'); }
	.yellowCityMarker { width: 12px; height: 20px; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='$pinYellowCity'); }
  	
	.click_invitation {
	  background-color:#fff;
	  padding:10px 20px;
	  position:absolute;
	  left:144px;
	  top:144px;
	  width:160px;
	  color:#474747;
	  z-index:5;
	  line-height:1.5em;
	}

	.click_invitation img {
	  float:right;
	  margin-top:22px;
	}

	.click_invitation strong {
	  font-size:14px;
	}

	#container .click_invitation {
	  left:90px;
	  top:50px;
	}
	
  
  
  .UIMediaButton_Container{display:block;text-decoration:none}
  .UIMediaButton_Container .UIMediaButton_top_row{height:100%}
  .UIMediaButton_Container .UIMediaButton_bottom{background:url(http://www.goabroad.net/facebook/mytravelmap/images/fb-button-background3.gif) transparent -7px bottom}
  .UIMediaButton_Container .UIMediaButton_content{background:url(http://www.goabroad.net/facebook/mytravelmap/images/fb-button-background4.png) no-repeat -7px 0;vertical-align:bottom}
  .UIMediaButton_Container .UIMediaButton_lower_row{height:100%}
  .UIMediaButton_bottom a{color:#333;font-size:11px;font-weight:bold;display:block;padding:3px 2px 4px 2px;text-shadow:white 0 1px 1px;float:left}
  .UIMediaButton_Container:active .UIMediaButton_content,
  .UIMediaButton_Container:active .UIMediaButton_bottom{background-image:url(http://www.goabroad.net/facebook/mytravelmap/images/fb-button-background3.gif)}
  .UIMediaButton_bottom a:hover {text-decoration:none;}
  .UIMediaButton_Container .UIMediaButton_tl,
  .UIMediaButton_Container .UIMediaButton_tr,
  .UIMediaButton_Container .UIMediaButton_bl,
  .UIMediaButton_Container .UIMediaButton_br{height:10px;}
  .ie7 .UIMediaButton_Container .UIMediaButton_tl,
  .ie7 .UIMediaButton_Container .UIMediaButton_tr{height:11px}
  .ie7 .UIMediaButton_Container .UIMediaButton_bl,
  .ie7 .UIMediaButton_Container .UIMediaButton_br{height:9px}
  .UIMediaButton_Container .UIMediaButton_tl{width:10px;background:url(http://www.goabroad.net/facebook/mytravelmap/images/fb-button-background4.png) no-repeat left top}
  .UIMediaButton_Container .UIMediaButton_tr{width:10px;background:url(http://www.goabroad.net/facebook/mytravelmap/images/fb-button-background4.png) no-repeat top right}
  .UIMediaButton_Container .UIMediaButton_bl{width:10px;background:url(http://www.goabroad.net/facebook/mytravelmap/images/fb-button-background4.png) no-repeat bottom left}
  .UIMediaButton_Container .UIMediaButton_br{width:10px;background:url(http://www.goabroad.net/facebook/mytravelmap/images/fb-button-background4.png) no-repeat bottom right}
  .UIMediaButton_Container:active .UIMediaButton_tl,
  .UIMediaButton_Container:active .UIMediaButton_tr,
  .UIMediaButton_Container:active .UIMediaButton_bl,
  .UIMediaButton_Container:active .UIMediaButton_br{background-image:url(http://www.goabroad.net/facebook/mytravelmap/images/fb-button-background3.gif)}
  
Style;

// set the individual countries' markers position in our CSS
$map = new Map(array('x'=>380, 'y'=>380));
$mapCity = new Map(array('x'=>378, 'y'=>403));
$i=0;
$defaultPointerLeft = null;
$showCityPins = $pinSetting != 0;
$showCountryPins = $pinSetting != 1;
$countries = $preselectedCtry = $countriesWithCities = '';
// IF HAS MAP ENTRIES -->
if ($mapEntries) :
	
	// SET CSS FOR MAP ENTRIES -->
  	foreach ($mapEntries as $entry) :

		if($showCountryPins) {
		    $shadowDivID = 'pinShadow_'.$i;
		    $markerDivID = 'pinBlue_'.$i;
		    $altMarkerDivID = 'pinYellow_'.$i;

		    $coordinates = $map->lngLatToXYCoords($entry->getCountry()->getLongitude(), $entry->getCountry()->getLatitude());

		    $y = $coordinates['y'];
		    $x = $coordinates['x'] - 2;

			if($entry->getCountry()->getCountryID() == 1322) {
				$x += 5;
			}

			$showdowY = $y-1;
			$showdowX = $x+1;
		    $fbml .= '#'.$shadowDivID. '{
		      	top: '.$showdowY.'px;
		      	left: '.$showdowX.'px;
		      	z-index: 0
		    }';
                                 
		    $x++;
		    $fbml .= '#'.$markerDivID. '{
		      	top: '.$y.'px;
		      	left: '.$x.'px;
		      	z-index: 5;
		    }';

		    $y++;
		    $fbml .= '#'.$altMarkerDivID. '{
		      	top: '.$y.'px;
		      	left: '.$x.'px;
		      	z-index: 5;
		    }';
			if($i == 0)	$defaultPointerLeft = $x.'px';
		    $i++;
		}
	
		if($showCityPins) :
			foreach ($entry->getCountry()->getCityLocationList() as $cityEntry) :
			    $shadowDivID = 'pinShadow_'.$i;
			    $markerDivID = 'pinBlue_'.$i;
			    $altMarkerDivID = 'pinYellow_'.$i;

			    $coordinates = $mapCity->lngLatToXYCoords($cityEntry->getLongitude(), $cityEntry->getLatitude());

			    $y = $coordinates['y'];
			    $x = $coordinates['x'] - 2;

				if($cityEntry->getCountryID() == 1322) {
					$x += 5;
				}

			    $x++;

			    $fbml .= '#'.$markerDivID. '{
			      	top: '.$y.'px;
			      	left: '.$x.'px;
			      	z-index: 5; 
				 }';

			    $y++;
			    $fbml .= '#'.$altMarkerDivID. '{
			      	top: '.$y.'px;
			      	left: '.$x.'px;
			      	z-index: 5;
				 }';
				if($i == 0)	$defaultPointerLeft = $x.'px';
			    $i++;
			endforeach;
		endif;
	
  	 endforeach;
	// END OF SET CSS FOR MAP ENTRIES -->


	$fbml .='</style>';
	$fbml .= <<< IE6Style
  <fb:user-agent includes="ie 6"> 
  <style type="text/css">
    .next_link span, .prev_link span{
  	  background-image:url("http://www.goabroad.net/facebook/mytravelmap/images/navigation_buttons.gif");
  	}
  	.UIMediaButton_Container .UIMediaButton_content,
    .UIMediaButton_Container .UIMediaButton_tl,
    .UIMediaButton_Container .UIMediaButton_tr,
    .UIMediaButton_Container .UIMediaButton_bl,
    .UIMediaButton_Container .UIMediaButton_br{background-image:url(http://www.goabroad.net/facebook/mytravelmap/images/fb-button-background4.gif)}
    .markerPointer {bottom:-2px;}
  </style>
  </fb:user-agent> 
IE6Style;
	// END OF CSS STYLE -->


	// TOP LINKS -->
	$fbml .= $topLinks;
	// END OF TOP LINKS -->

	$i = 0;
  	$isCtrySelected = false; //watch out for $entryCount
	$entryHelper = new EntryHelper;

	// MAP DIV CONTAINER -->
	$fbml .= '<div id="container" class="clearfix">';


	// $mapEntries FOR LOOP -->

	foreach ($mapEntries as $mapEntry) :
		$flag = $mapEntry->getCountry()->getFlag();
		$countryName = str_replace("&",'&amp;',$mapEntry->getCountry()->getName());
		$image = '';
		$ctryMarkup = '';
		
		if($showCountryPins) {
		
			if($travellogID = $mapEntry->getCountry()->getTravelLogID()) {
	  			$travelLog = new TravelLog($travellogID);
				$entryTips = new EntryTips;
				$ts = count($entryTips->getList($travellogID));
				$js = $mapEntry->getCountry()->getJournalEntriesCount();
				$photos = $travelLog->getPhotos();
				$ps = count($photos);
				$link = self::GOABROAD_DOTNET_URL.$entryHelper->getLink($travelLog);
	        	$entries = $js < 2 ? "$js Journal Entry" : "$js Journal Entries";
				$journalEntries = "$entries <span class='mute'>|</span> <a href='$link' target='_blank'>read latest &raquo;</a><br />";	

				$photoEntries = '';
				if($ps) {
	        		$photoEntries = $ps < 2 ? "$ps Photo" : "$ps Photos";
	                $photo =  $photos[rand(0,$ps-1)]->getPhotoLink('thumbnail');
			   		$image = "<img class='journal_photo' src='$photo'>";
					$photosLink = self::GOABROAD_DOTNET_URL."/collection.php?type=traveler&ID=".$travelLog->getTravelerID()."&context=travellog&genID=".$travellogID;
					$photoEntries = "$photoEntries <span class='mute'>|</span> <a href='$photosLink' target='_blank'>browse photos &raquo;</a><br />";
				}
				$travelTips = '';
				if($ts) {
	            	$travelTips = $ts < 2 ? "$ts Travel Tip"   : "$ts Travel Tips";													
				}
			}
      		$ctryMarkup .= <<< CtryMarkup
			<div>
			  $image
			  <img class="location_flag" src="$flag" alt="" />
			  <p class="country_name"><strong>$countryName</strong></p>
CtryMarkup;
			if($travellogID)  {
				 $ctryMarkup .= "<p class='location_details'>
				    $journalEntries 
				    $photoEntries
				    $travelTips
				  </p>";
			}
      		$ctryMarkup .= '<div style="clear: both"/></div>';

	      	$fbml .= <<< FbjsString
	        <fb:js-string var="entries.ctry$i">
	            $ctryMarkup
	        </fb:js-string>
FbjsString;
      		if (!$isCtrySelected) { $preselectedCtry = $ctryMarkup; }


	        // MARKER'S SHADOW -->
	      	$fbml .= <<< Marker
	        <div id="pinShadow_$i" class="shadowMarker">
	          	<img width="22" src="$pinShadow" class="imgTrans"/>
	        </div>
Marker;
			// END OF MARKER'S SHADOW -->


        	// MAIN MARKERS -->
	      	if (!$isCtrySelected) :
				$isCtrySelected = true;
	        	$fbml .= <<< Marker
	          	<div id="pinBlue_$i" class="blueMarker" onclick="showCountryInfo($i,this)" onmouseover="accentMarker(this)" style="display:none">
	            	<img class="imgTrans" title="$countryName" alt="" src="$pinBlue" />
	          	</div>
	          	<div id="pinYellow_$i" class="yellowMarker" onmouseover="accentMarker(this)">
	            	<img class="imgTrans" title="$countryName" alt="" src="$pinYellow" />
	          	</div>
Marker;
	     	else :
	        	$fbml .= <<< Marker
	          	<div id="pinBlue_$i" class="blueMarker" onclick="showCountryInfo($i,this)" onmouseover="accentMarker(this)">
	            	<img class="imgTrans" title="$countryName" alt="" src="$pinBlue" />
	          	</div>
	          	<div id="pinYellow_$i" class="yellowMarker" onmouseover="accentMarker(this)" style="display:none">
	            	<img class="imgTrans" title="$countryName" alt="" src="$pinYellow" />
	          	</div>
Marker;
	      	endif;
			$i++;
		}
			// END OF MAIN MARKERS -->

			// CITIES ENTRIES
			$cities = array();
			foreach ($mapEntry->getCountry()->getCityLocationList() as $cityEntry) :
				$cityName = str_replace("&",'&amp;',$cityEntry->getName()). ', '. $countryName;
            	$cities[] = $cityEntry->getName();
				if($showCityPins) {
					$cityJournalEntries = $cityPhotoEntries = $cityTravelTips = $cityImage = '';
					if($cityTravellogID = $cityEntry->getTravelLogID()) {
			  			$cityTravelLog = new TravelLog($cityTravellogID);
						$cityEntryTips = new EntryTips;
						$cityTips = count($cityEntryTips->getList($cityTravellogID));
						$cityEntryCount = $cityEntry->getJournalEntriesCount();
						$cityPhotos = $cityTravelLog->getPhotos();
						$cityPhotoCount = count($cityPhotos);
						$cityLink = self::GOABROAD_DOTNET_URL.$entryHelper->getLink($cityTravelLog);
			        	$cityEntries = $cityEntryCount < 2 ? "$cityEntryCount Journal Entry" : "$cityEntryCount Journal Entries";
						$cityJournalEntries = "$cityEntries <span class='mute'>|</span> <a href='$cityLink' target='_blank'>read latest &raquo;</a><br />";	

						if($cityPhotoCount) {
			        		$cityPhotoEntries = $cityPhotoCount < 2 ? "$cityPhotoCount Photo" : "$cityPhotoCount Photos";
			                $cityPhoto =  $cityPhotos[rand(0,$cityPhotoCount-1)]->getPhotoLink('thumbnail');
					   		$cityImage = "<img class='journal_photo' src='$cityPhoto'>";
							$cityPhotoLink = self::GOABROAD_DOTNET_URL."/collection.php?type=traveler&ID=".$cityTravelLog->getTravelerID()."&context=travellog&genID=".$cityTravellogID;
							$cityPhotoEntries = "$cityPhotoEntries <span class='mute'>|</span> <a href='$cityPhotoLink' target='_blank'>browse photos &raquo;</a><br />";
						}

						if($cityTips) {
			            	$cityTravelTips = $cityTips < 2 ? "$cityTips Travel Tip" : "$cityTips Travel Tips";													
						}
					}

                    $cityMarkup = "<div>
						$cityImage
						<img class='location_flag' src='$flag' alt=''/>
						<p class='country_name'><strong>$cityName</strong></p>";

					if($cityTravellogID)  {
						 $cityMarkup .= "<p class='location_details'>
						    $cityJournalEntries 
						    $cityPhotoEntries
						    $cityTravelTips
						  </p>";
					}

					$cityMarkup .= "<div style='clear: both'/></div>";
					
			      	$fbml .= <<< FbjsString
			        <fb:js-string var="entries.ctry$i">
						$cityMarkup
			        </fb:js-string>
FbjsString;
		      		if (!$isCtrySelected && $pinSetting == 1) { $preselectedCtry = $cityMarkup; }

		        	// MAIN MARKERS -->
			      	if (!$isCtrySelected) :
						$isCtrySelected = true;
			        	$fbml .= <<< Marker
			          	<div id="pinBlue_$i" class="blueCityMarker" style="display:none" onclick="showCountryInfo($i,this)" onmouseover="accentMarker(this)" >
			            	<img class="imgTrans" title="$cityName" alt="" src="$pinBlueCity" />
			          	</div>
			          	<div id="pinYellow_$i" class="yellowCityMarker" onmouseover="accentMarker(this)">
			            	<img class="imgTrans" title="$cityName" alt="" src="$pinYellowCity" />
			          	</div>
Marker;
			     	else :
			
			        	$fbml .= <<< Marker
			          	<div id="pinBlue_$i" class="blueCityMarker" onclick="showCountryInfo($i,this)" onmouseover="accentMarker(this)">
			            	<img class="imgTrans" title="$cityName" alt="" src="$pinBlueCity" />
			          	</div>
			          	<div id="pinYellow_$i" class="yellowCityMarker" onmouseover="accentMarker(this)" style="display:none">
			            	<img class="imgTrans" title="$cityName" alt="" src="$pinYellowCity" />
			          	</div>
Marker;
		      		endif;
					$i++;
				}
	  		endforeach;
			// END OF $cityEntry FOR LOOP -->
		$countriesWithCities .= '<strong>'.$countryName.':</strong> '. implode(', ', $cities).'<br>';
    	$countries .= $countries ? ', '.$countryName : $countryName;
		$citiesCount += count($cities);
  	endforeach;
	// END OF $entryCount FOR LOOP -->

	$fbml .= <<< mapTipBox
		<div class="click_invitation" id="click_invitation">
			<img src="http://www.goabroad.net/facebook/mytravelmap/images/click_invite_illustration.jpg" />
			<p>
				<strong>Tip:</strong><br />
				Click on the pins to browse through the locations.
			</p>
			<p id="tipCloseButton">
				<a onclick="$('tipCloseButton').getParentNode().setStyle('display','none')">close</a>
			</p>
		</div>
mapTipBox;

	
	if(!is_null($defaultPointerLeft)) {
		$fbml .= "<div id='pointer' class='markerPointer' style='left:$defaultPointerLeft'>
			<img src='http://www.goabroad.net/facebook/mytravelmap/images/arrow-up.gif'/>
		</div>";
	}
	

	$fbml .= '</div>';
	// END OF MAP DIV CONTAINER -->
	$countriesWithCities = substr($countriesWithCities,0,-4);
   	$fbml .="<fb:js-string var='countriesWithCities'>$countriesWithCities<br/><a href='#' onclick='hideCities()'>hide cities</a></fb:js-string>";
   	$fbml .="<fb:js-string var='countriesOnly'>$countries<br /><a href='#' onclick='showCities()'>show cities</a></fb:js-string>";

	$fbml .= <<< PrevNextButton
	<div class="location_info clearfix">
		<ul class="location_navigation">
	    	<li class="prev"><a class="prev_link" title="Previous" onclick="mapEntriesPrevNextControl('prev')"><span>Prev</span></a></li>
	    	<li class="next"><a class="next_link" title="Next" onclick="mapEntriesPrevNextControl('next')"><span>Next</span></a></li>
	  	</ul>
PrevNextButton;

		// COUNTRY JOURNALS INFO -->
		$fbml .= $preselectedCtry 
			? "<div id='ctryInfoDiv'>$preselectedCtry</div>" 
			: "<div id='ctryInfoDiv' style='display: none'> </div>";	
		// END OF COUNTRY JOURNALS INFO -->

	$fbml .= "</div>";

	$countriesCount = count($mapEntries);
	$countriesCount = $countriesCount < 2 ? "$countriesCount country" : "$countriesCount countries";
	$hasCities = ($citiesCount > 0) ? true : false;
	$citiesCount = $citiesCount < 2 ? "$citiesCount city" : "$citiesCount cities";
	// COUNTRYLIST -->
	if($countriesCount) :
		$locationCountPhrase = ($hasCities) ? "has been to $citiesCount in $countriesCount:" : "has been to $countriesCount:";
		$fbml .= <<< CountryList
		<div id="wish" class="wish">
			<span> 
				<fb:fbml version="1.1">
					<fb:name uid='$userID' firstnameonly='true' useyou='false'/>
			  	</fb:fbml>			
				$locationCountPhrase
			</span> <br />
			<div id="locationList">
				$countries 
				<br /><a href="#" onclick="showCities()">show cities</a>
			</div>
		</div>
CountryList;
	endif;
	// END OF COUNTRYLIST -->


// ELSE HAS NO MAP ENTRIES -->
else :
	$fbml .= "</style>$topLinks<div style='font-weight:bold;padding:10px'>No Countries traveled yet!</div>";
endif;
// END IF HAS MAP ENTRIES -->


// SRCIPTS -->
$fbml .= <<< Script
<script>
	var currentValue = 0;
	var totalCount =  $i - 1;

  	function showCountryInfo(id,elem) {
		var divElem = $('container').getChildNodes();
    	for (var i=0; i < divElem.length; i++) {
			if(divElem[i].getClassName() == 'yellowMarker' || divElem[i].getClassName() == 'yellowCityMarker') {
				divElem[i].setStyle('display', 'none');
				divElem[i].getPreviousSibling().setStyle('display', 'block');
			}			
    	}
		currentValue = id;
    	elem.setStyle('display', 'none');
    	elem.getNextSibling().setStyle('display', 'block');
    	$('pointer').setStyle('left', elem.getNextSibling().getAbsoluteLeft()-178 + 'px');
    	$('ctryInfoDiv').setInnerFBML(entries["ctry"+id]).setStyle('display', 'block');
  	}

	function mapEntriesPrevNextControl(action) {
		var i = currentValue;
		if(action == 'next') {
			i = i == totalCount ? 0 : i+1;
		} else {
			i = i == 0 ? totalCount : i-1;
		}
		currentValue = i;
		showCountryInfo(i,$('pinBlue_'+i));
		accentMarker($('pinYellow_'+i));
	}


  	function accentMarker(obj) {
    	for (var i=0; i < $i; i++) {
      		if (mainPin = document.getElementById('pinBlue_' + i)) {
        		mainPin.setStyle({zIndex: '4'});
      		}
      		if (altPin = document.getElementById('pinYellow_' + i)) {
        		altPin.setStyle({zIndex: '4'});
      		}
    	}
    	obj.setStyle({display: 'block', zIndex: '5'});
  	}

	function showCities() {
		document.getElementById('locationList').setInnerFBML(countriesWithCities);
	}

	function hideCities() {
		document.getElementById('locationList').setInnerFBML(countriesOnly);		
	}

  	function restoreMarker(obj) {
    	obj.setStyle('display', 'block');
  	}

	function closeTipBox() {
		$('click_invitation').setStyle('display','none');
	}

	function $(elemId) {
		return document.getElementById(elemId);
	}
</script>
Script;
// END OF SRCIPTS -->

$fbml .='</fb:wide>';
?>
