<?php
$user = $model['user'];
$isOwnerUser = $model['isOwnerUser'];
$selected = 'invite';
require_once 'mtm_dashboard.php'; 

//  Build your invite text
$invfbml = <<<FBML
I've added a map on my profile to show where I've traveled to. I want to check out your travel map. I sent you this so you can easily add your map, too.
<fb:req-choice url="http://apps.facebook.com/my-travel-map/" label="Add My Travel Map!" />
FBML;
?>

<div id="inner_wrapper" style="overflow:hidden">

	<fb:request-form type="My Travel Maps" action="<?php echo $model['action']; ?>" content="<?=htmlentities($invfbml)?>" invite="true">
	  <fb:multi-friend-selector max="20" actiontext="Here are your friends who don't have My Travel Maps.  Invite them to share their travel plans with you!" showborder="false" rows="5" exclude_ids="<?php echo $model['friendsWithApp']?>">
	</fb:request-form>

	<div class="foot"> My Travel Map
		<span> &copy; <a target="_blank" href="http://goabroad.net">GoAbroad
		Network</a>/ <a target="_blank" href="http://goabroad.com">GoAbroad.com</a>.
		<span> All Rights Reserved. </span>
		</span>
	</div>

</div>
