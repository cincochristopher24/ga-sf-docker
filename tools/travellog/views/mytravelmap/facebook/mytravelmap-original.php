<?php
/**
 * Default page of the My Travel Map application.
 */

$countryList = $model['countryList'];
$mapEntries = $model['mapEntries'];
$entriesCount = count($mapEntries);
$user = $model['user'];
?>

<?php require_once 'mtm_dashboard.php'; ?>

<fb:iframe src="http://www.goabroad.net/facebook/mytravelmap/index.php?action=genmap"
           width="641"
           height="600"
           scrolling="no"
/>

<div style="padding: 0 0 15px 0">
  <div id="rewriteDiv" style="width: 463px; margin: 10px 0 10px 80px; border: 1px solid #e6e6e6; padding: 10px; background-color: #fafafa; color: #777777;" >
    <span style="color: #60718B; font-weight: bold;">Countries I've Traveled To</span>(<?php echo $entriesCount; ?>): <?php $cnt=0; foreach($mapEntries as $entry) { $cnt++; echo $entriesCount == $cnt ? $entry->getCountry()->getName() : $entry->getCountry()->getName().', '; } ?>
    <div id="indicator" style="display: none">
      <img style="margin: 5px 0 0 0; float: left;" src="http://www.goabroad.com/facebook/traveltodo/images/loading.gif"><p style="float: left; margin: 10px 0 0 2px;">Loading...</p>
      <div style="clear: both;"></div>
    </div>
  </div>

  <a id="showEditForm" clicktoshow="editForm" clicktohide="showEditForm" style="padding: 0 0 0 470px; display: block; cursor: pointer">Edit Country List</a>

  <form style="display: none" id="editForm" action="index.php?action=saveCountryList" method="post">
    <div style="border: 1px solid #e6e6e6; margin: 10px 0 0 80px; width: 483px; height: 350px; overflow: auto;">
    <?php

    foreach ($countryList as $ctry) {
      $id=$ctry->getCountryID()
    ?>
      <input type="checkbox"
             name="wishList[]"
             value="<?php echo $id; ?>"
             id="c<?php echo $id; ?>"
         <?php
         foreach ($mapEntries as $entry) {
           if ($id == $entry->getCountry()->getCountryID()) { ?>
             checked="checked"
         <?php
             break;
           }//end if
         }//end foreach
         ?>
      ><label for="c<?php echo $id; ?>"><?php echo $ctry->getName(); ?></label><br/>
    <?php } ?>
    </div>
    <div style="padding: 10px 0 10px 80px; margin: 0 0 0 5px;">
      <input type="submit" value="Save"/>
      <a class="inputbutton" style="padding: 1px 5px 1px 5px;" clicktohide="editForm" clicktoshow="showEditForm" href="http://apps.facebook.com/my-travel-map">Cancel</a>
    </div>
  </form>
</div>