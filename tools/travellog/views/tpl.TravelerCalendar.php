
<style type="text/css">
	#popup_header strong
	{
		color:#FFFFFF;
	}
</style>
<?php echo $obj_view->render();?>

<?php echo $subNavigation->show();?>

<div class="layout_2" id="content_wrapper">
	<div id="wide_column">
		<div class="section" id="event_details">
				<h2 id="event_details_header"><span>Event Details</span></h2>
				<div class="content">
					<p class="loadingblock"  id="loading">
						<span id="imgLoading"><img src="/images/loading.gif" alt="loading" /></span>
						<em id="statusCaption">Loading data please wait...</em>
					</p>
					<div id="event_view"></div>
				</div>
		</div>
	</div>
	<? if($props['event_count'] > 0 || $props['powerful']): ?>
	<div id="narrow_column">
		<div class="section" id="quick_tasks">
			<!--<h2><span><?=$props['event_type']?></span></h2>-->
			<div class="content"><?=$traveler_event_lists?></div>	
		</div>	
	</div>
	<? endif; ?>
</div>
<?php require_once('Class.DateTimeHolder.php'); ?>
<script type="text/javascript" charset="utf-8">
	b = '<?php echo DateTimeHolder::getInstance()->toJsonData()?>';
	// set action for observeEvents called after sorting
	action = <?php echo strtolower($props['action']);?>;
	// get = '';
	// <?if(isset($_GET['dbhDebug'])):?>
	// 	get = '&dbhDebug=1';
	// <?endif;?>	
	LocalTimeConverter.convertAndSortEventDates(b,0);
</script>