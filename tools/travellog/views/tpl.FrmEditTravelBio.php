<?php
	/**
	 * tpl.FrmEditTravelBio.php
	 * @author Cheryl Ivy Q. Go
	 */

	Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	Template::setMainTemplateVar('page_location', 'My Passport');
	Template::includeDependentJs('/js/utils/Class.Request.js');
	$code = <<<BOF
<script type="text/javascript"> 
//<![CDATA[
	function confirm_entry(){
		input_box = confirm("Are you sure you want to unsynchronize your GoAbroad.Net and Facebook Travel Bio?");
		if (input_box){ 
			var req = new Request();
			req.registerEvent('onSuccess',function(){
				req.getResponseText().evalScripts();
				alert("Synchronization successful!");
				window.location.reload();
			});
			req.registerEvent('onFail',function(){alert('Fail...')});
			req.registerEvent('onProcess',function(){});
			req.setMethod('POST');
			//req.addFormVariable('action', 'unsynchronize');
			req.sendRequest("popup.php?action=unsynchronize");
		}
	}
//]]>
</script>
BOF;
	Template::includeDependent($code);	
	$subNavigation-> show();
?>

<div class="area" id="main">
	<div class="area" id="area_left">
		<div class="section" id="quick_tasks">
						
			<div class="content" style = "margin-left: 5px;">
				<div style = "margin-top: 5px;">
					<p style = "font-size: 11px; color: #36afee; width: 140px;"> Share your travel insights and experiences by answering your Travel Bio</p>
				</div>
				<?php if (!$synchronized) : ?>
				<div style = "padding-top: 5px;">
					<p style = "font-weight: bold;">Tip:</p>
				</div>
				<div style = "padding-bottom: 8px; border-bottom: 1px solid #dadada; width: 145px;">
					Show your friends how well-traveled you are by adding the <strong> Travel Bio </strong> app to your
					Facebook profile. You can share it with your networks and compare it with your friends Bio's! <br />
				</div>
				<div style = "padding-top: 10px; padding-left: 5px; width: 140px;">
					To export your answers to Facebook, just synchronize your accounts after adding the Travel 
					Bio app to your Facebook profile.
				<div style = "padding-top: 10px;">
					<a href = "javascript:void(0);" onclick = "window.open('popup.php?action=display', 'window2', 'resizable=no,scrollbars=no,width=420,height=200, top=' + ((screen.width/2)- 400) + ', left=450'); return false" style = "margin: 0px;"> What is Synchronize? </a>
				</div>
				
				</div>
				<?php else : ?>
				<div style = "padding-top: 30px; width: 140px;">	
					Your Travel Bio is synchronized with your Facebook profile app. Editing your answers here will 
					also change them on your Facebook profile.
				</div>
				<div style = "padding-top: 10px; padding-left: 5px;">
					To edit your answers separately, or to associate this GoAbroad Network account with a different Facebook account, you may 
					<a href = "javascript:void(0);" onclick = "confirm_entry();"> remove synchronization </a> here.
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<div class="area" id="area_right">
		<div class="section">
			<h1> <?= $labelAction ?> </h1>
			<div class="content">
				<form name = "frmEdit" action = "editTravelBio.php" method = "post">
				<?php
					foreach($answers as $iAns){
						if(!array_key_exists($iAns->getProfileQuestionID(),$structAns)){
							$structAns[$iAns->getProfileQuestionID()] = $iAns->getValues();
						}
					}
					foreach($profileQuestions as $iQuestion):
				?>
					<p>
						<div style = "font-weight: bold;"><?= ++$ctr . '. ' . $iQuestion->getQuestion() ?> </div>
						<div style = "padding-left: 10px; padding-top: 5px;">
						<?php 
							if($iQuestion->getQuestionType() == ProfileQuestion::$TEXT): 
								$val = (array_key_exists($iQuestion->getQuestionID(),$structAns))
									? $structAns[$iQuestion->getQuestionID()][0]->getValue()
									:'';
						?>
							<input type="text" name="txt_<?= $iQuestion->getQuestionID() ?>" value="<?= $val ?>" style = "border: 1px solid #898989;"/>
						<?php 
							elseif($iQuestion->getQuestionType() == ProfileQuestion::$TEXTAREA): 
								$val = (array_key_exists($iQuestion->getQuestionID(),$structAns))
									? $structAns[$iQuestion->getQuestionID()][0]->getValue()
									:'';
						?>
							<textarea cols="30" rows="5" name="txa_<?= $iQuestion->getQuestionID() ?>" style = "border: 1px solid #898989;"><?= $val ?></textarea>
						<?php elseif($iQuestion->getQuestionType() == ProfileQuestion::$CHECK_TEXTBOX): ?>				
							<ul class="checkbox_list">					
								<?php 						
									$choices = $iQuestion->getChoices();
									$selections = array();
									$textVal = '';						
									if(array_key_exists($iQuestion->getQuestionID(),$structAns)){
										foreach($structAns[$iQuestion->getQuestionID()] as $iChoice){							
											if($iChoice->isTypeCheckbox()){
												$selections[] = $iChoice->getValue(); 
											}
											elseif($iChoice->isTypeText()){
												$textVal = $iChoice->getValue(); 
											}
										}
									}
									foreach($choices as $iChoice):
								?>
									<li class="checkbox_list_item">
										<input type="checkbox" name="chk_<?= $iQuestion->getQuestionID() ?>[]" style="width:auto" value="<?= $iChoice->getChoice() ?>" <?= in_array($iChoice->getChoice(),$selections)?'checked="true"':'' ?>  style = "border: 1px solid #898989;"/>&nbsp;<?= $iChoice->getChoice() ?>
									</li>
								<?php endforeach;?>					
								<li class="checkbox_list_item checkbox_list_item_text">
									<input type="text" name="txtchk_<?= $iQuestion->getQuestionID() ?>" value="<?= $textVal ?>" style = "border: 1px solid #898989;"/>	
								</li>
							</ul>
						<?php endif; ?>
						</div>
					</p>
				<?
					endforeach;
				?>
				<ul class="form">
					<li class="actions">								
						<input name = "btnSubmit" class = "submit" value = "Save" type = "submit" />
					</li>
				</ul>
				<div class="clear"></div>
				</form>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>