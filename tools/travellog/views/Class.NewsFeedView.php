<?php

	require_once('travellog/model/NewsFeed/Class.Sections.php');
	require_once('travellog/model/NewsFeed/Class.EventType.php');
	require_once('Class.GaDateTime.php');
	require_once('Class.Template.php');
	
	class NewsFeedView{
		
		private $mUpdates = null;
		private $mTemplateUsed = null;
		
		public function render($updates){
			$tempUpdates = array();
			foreach($updates as $date => $feedItems){
				foreach($feedItems as $time => $feedItem){
					switch ($feedItem->getSection()){
						case Sections::TRAVEL_PLANS :
							$item = $this->processTravelPlanFeedItem($feedItem);
							break;
						case Sections::TRAVEL_BIO :
							$item = $this->processTravelBioFeedItem($feedItem);
							break;  
						case Sections::COMMENTS :
							$item = $this->processCommentsFeedItem($feedItem);
							break;
						case Sections::GROUPS :
							$item = $this->processGroupFeedItem($feedItem);
							break;
						case Sections::FRIEND :
							$item = $this->processFriendFeedItem($feedItem);
							break;
						case Sections::JOURNAL_ENTRY :
							$item = $this->processJournalEntryFeedItem($feedItem);
							break;
						case Sections::PHOTO :
							$item = $this->processPhotoFeedItem($feedItem);
							break;
						case Sections::BULLETIN :
							$item = $this->processBulletinFeedItem($feedItem);
							break;
						case Sections::CALENDAR_EVENT :
							$item = $this->processCalendarEventFeedItem($feedItem);
							break;
						default :
							throw new exception('Unknown NewsFeed section.');
					}
					$tempUpdates[$date][$time] = $item;
				}
			}
			return $tempUpdates; 
		}
		
		private function processFriendFeedItem($feedItem){
			$actionString = $feedItem->getDoerLink() . ' and ' . $feedItem->getRecipientLink() . ' are now friends.';
			$obj_item_details =$feedItem->getDetailOfItems();
			return array('actionString' => $actionString, 'doerPhoto' => $feedItem->getDoer()->getPrimaryPhoto(), 'feedItem' => $feedItem);
		}
		
		private function processCommentsFeedItem($feedItem){
			require_once('travellog/model/Class.PokeType.php');
			require_once('travellog/views/Class.PokesView.php');
			$obj_item_details = $feedItem->getDetailOfItems();
			$comment = $obj_item_details[0]; // comment feeds has 1 item per group
			$context = $comment->getContext();
			if(0 < $comment->getPokeType()){
				$poke = new PokeType($comment->getPokeType());
				$pokesview = new PokesView();
				$pokesview->inNewsFeed();
				$pokesview->setVisitorId(NewsFeed::getLoggedTraveler()->getTravelerID());
				$pokesview->setAuthorId($comment->getAuthor());
				$pokesview->setOwnerId($feedItem->getRecipientID());
				$pokesview->setPokeTypeId($poke->getPokeTypeID());
				switch(get_class($context)){
					case 'TravelLog' :
						$pokesview->setContextID($context->getTravellogID());
						$pokesview->setContext(3);
						$addMessage = '. ' . $feedItem->getDoerLink() . ' also said something about it.';
						break;
					case 'Photo' :
						$pokesview->setContextID($context->getPhotoID());
						$pokesview->setContext(2);
						$addMessage = '. ' . $feedItem->getDoerLink() . ' also wrote something about it.';
						break;
					default :
						$pokesview->setContextID($context->getTravelerID());
						$pokesview->setContext(1);
						$addMessage = ' and said something about ' . $feedItem->getPronoun(FALSE) . ' profile';
						break;
				}
				
				//$imgString = "<img src = " . $poke->getPath() . $poke->getPokeImage()  . "/>";
				$msg = preg_replace('/^(you)\s/', 'You ' , $pokesview->getMessage());
				$actionString = $msg . ('' != trim($comment->getTheText())? $addMessage : ''); 
			}
			else {
				switch(get_class($context)){
					case 'TravelLog' :
						$path = 'journal-entry.php?action=view&travellogID=' . $context->getTravelLogID();
						$actionString = $feedItem->getDoerLink() . ' said something about ' . $feedItem->getRecipientLink(TRUE) . ' ' . $feedItem->constructLink($path, 'journal');
						break;
					case 'Photo' :
						$path = 'photomanagement.php?action=vfullsize&cat=profile&genID=' . $feedItem->getRecipientID() . '&photoID=' . $context->getPhotoID();
						$actionString = $feedItem->getDoerLink() . ' said something about ' . $feedItem->getRecipientLink(TRUE) . ' ' . $feedItem->constructLink($path, 'photo'); 
						break;
					default :
						$actionString = $feedItem->getDoerLink() . ' said something on ' . $feedItem->getRecipientLink(TRUE) . ' profile.';
						break;
				}
			}
			if('' != trim($comment->getTheText()))
				return array('actionString' => $actionString, 'detail' => '<B>"' . $comment->getTheText() . '"</B>');
			else
				return array('actionString' => $actionString);
		}
		/**
		private function processCommentsFeedItem($feedItem){
			require_once('travellog/model/Class.PokeType.php');
			$obj_item_details = $feedItem->getDetailOfItems();
			$actionString = '';
			foreach($obj_item_details as $comment){
				$context = $comment->getContext();
				switch (get_class($context)){ 
					case 'TravelerProfile':
						switch($comment->getPokeType()){
							case PokeType::$SMILE :
								$actionString = $feedItem->getDoerLink() . ' smiled at ' . $feedItem->getRecipientLink();
								break;
							case PokeType::$THUMBSUP :
								$actionString = $feedItem->getDoerLink() . ' gave ' . $feedItem->getRecipientLink() . ' a thumbs up!';
								break;
							case PokeType::$CHEERSMATE :
								$actionString = $feedItem->getDoerLink() . (NewsFeed::isLoggedUser($feedItem->getDoerID()) ? ' said':' says') .', "Cheers mate!" to ' . $feedItem->getRecipientLink();
								break;
							case PokeType::$THAIWAI :
								$actionString .= $feedItem->getDoerLink() . ' greeted ' . $feedItem->getRecipientLink() . ' with a Thai Wai';
								break;
							case PokeType::$WAVE :
								$actionString .= $feedItem->getDoerLink() . ' waved at ' . $feedItem->getRecipientLink();
								break;
							case PokeType::$BOWDOWN :
								$actionString .= $feedItem->getDoerLink() . ' greeted ' . $feedItem->getRecipientLink() . ' with a bow.';
								break;
							case PokeType::$CONGRATULATE :
								$actionString .= $feedItem->getDoerLink() . ' congratulates ' . $feedItem->getRecipientLink();
								break;
							default;
						}
						break;
					case 'Photo' :
						$path = 'photomanagement.php?action=vfullsize&cat=profile&genID=' . $feedItem->getRecipientID() . '&photoID=' . $context->getPhotoID();
						switch($comment->getPokeType()){
							case PokeType::$SMILE :
								$actionString = $feedItem->getDoerLink(true) . ' <a href="' . $path . '">photo</a> made '. $feedItem->getRecipientLink() . ' smile';
								break;
							case PokeType::$THUMBSUP :
								$actionString = $feedItem->getDoerLink() . (NewsFeed::isLoggedUser($feedItem->getDoerID()) ? '':' has') . ' viewed ' . $feedItem->getRecipientLink(true) . ' <a href="' . $path . '">photo</a> and gave it a thumbs up';
								break;							
							case PokeType::$CHEERSMATE :
								$actionString = $feedItem->getDoerLink() . (NewsFeed::isLoggedUser($feedItem->getDoerID()) ? '':' has') . ' viewed ' . $feedItem->getRecipientLink(true) . ' <a href="' . $path . '">photo</a> and says, "Cheers, mate!"';
								break;
							case PokeType::$THAIWAI :
								$actionString = $feedItem->getDoerLink() . (NewsFeed::isLoggedUser($feedItem->getDoerID()) ? '':' has') . ' viewed ' . $feedItem->getRecipientLink(true) . ' <a href = "' . $path . '">photo</a> and gave it a Thai Wai';
								break;
							case PokeType::$WAVE :
								$actionString = $feedItem->getDoerLink() . ' waved at ' . $feedItem->getRecipientLink() . ' in ' . $feedItem->getPronoun(false) . ' <a href="' . $path . '">photo</a>';
								break;
							case PokeType::$BOWDOWN :
								$actionString = $feedItem->getDoerLink() . (NewsFeed::isLoggedUser($feedItem->getDoerID()) ? ' bow':' bows') . ' down to ' . $feedItem->getRecipientLink(true) . ' photographing skills in ' . $feedItem->getPronoun(false) . ' <a href="' . $path . '">photo</a>';
								break;
							case PokeType::$CONGRATULATE :
								$actionString = $feedItem->getDoerLink() . (NewsFeed::isLoggedUser($feedItem->getDoerID()) ? ' congratulate ':' congratulates ') .$feedItem->getRecipientLink() . ' on ' . $feedItem->getPronoun(false) . ' awesome <a href="' . $path . '">photo</a>';
								break;
							default;
						}
						break;
					case 'TravelLog' :
						$path = 'journal-entry.php?action=view&travellogID=' . $context->getTravelLogID();
						switch($comment->getPokeType()){
							case PokeType::$THUMBSUP :
								$actionString = $feedItem->getDoerLink() . (NewsFeed::isLoggedUser($feedItem->getDoerID()) ? '':' has') . ' read ' . $feedItem->getRecipientLink(true) . ' <a href = "' . $path . '">journal</a> and gave it a thumbs up';
								break;
							case PokeType::$SMILE :
								$actionString = $feedItem->getDoerLink(true) . ' <a href="' . $path . '">journal</a> made '. $feedItem->getRecipientLink() . ' smile';
								break;
							case PokeType::$CHEERSMATE :
								$actionString = $feedItem->getDoerLink() . (NewsFeed::isLoggedUser($feedItem->getDoerID()) ? '':' has') . ' read ' . $feedItem->getRecipientLink(true) . ' <a href = "' . $path . '">journal</a> and says "Cheers, mate!"';
								break;
							case PokeType::$THAIWAI :
								$actionString = $feedItem->getDoerLink() . (NewsFeed::isLoggedUser($feedItem->getDoerID()) ? '':' has') . ' read ' . $feedItem->getRecipientLink(true) . ' <a href = "' . $path . '">journal</a> and gave it a Thai Wai';
								break;
							case PokeType::$WAVE :
								$actionString = $feedItem->getDoerLink() . ' waved at ' . $feedItem->getRecipientLink() . ' in ' . $feedItem->getPronoun(false) . ' <a href="' . $path . '">journal</a>';
								break;
							case PokeType::$BOWDOWN :
								$actionString = $feedItem->getDoerLink() . (NewsFeed::isLoggedUser($feedItem->getDoerID()) ? ' bow':' bows') . ' down to ' . $feedItem->getRecipientLink(true) . ' <a href="' . $path . '">trip</a>';
								break;
							case PokeType::$CONGRATULATE :
								$actionString = $feedItem->getDoerLink() . (NewsFeed::isLoggedUser($feedItem->getDoerID()) ? ' congratulate ':' congratulates ') . $feedItem->getRecipientLink() . ' on this <a href="' . $path . '">experience</a>';
								break;
							default;
						}
						break;
					default;
				}
				if('' != $comment->getTheText()){
					if('' != $actionString)
						$actionString .= ' and wrote something about it.';
						//$actionString .= ' and wrote, "' . $comment->getTheText() . '"';
					else
						//$actionString = ' wrote something about it.';
						$actionString = $feedItem->getDoerLink() . ' wrote "' . $comment->getTheText() . '" to ' . $feedItem->getRecipientLink();
				} 
			}
			return array('actionString' => $actionString);
		} **/
		
		
		private function processCalendarEventFeedItem($feedItem){
			$obj_item_details = $feedItem->getDetailOfItems();
			$arr = array();
			foreach($obj_item_details as $event){
				$arr[] = "<b>" . $feedItem->constructLink($event->getLink(), $event->getTitle()) . "</b>";
			}
			//$actionString = $feedItem->getDoerLink() . ' created calendar ' . ((count($obj_item_details) > 1) ? 'events ' : 'event ') . implode(', ', $arr);
			$actionString = $feedItem->getDoerLink() . ' created ' . ((count($obj_item_details) > 1) ? count($obj_item_details) .' calendar events ' : ' a calendar event ');
			$detail = $this->myImploder($arr);
			return array('actionString' => $actionString, 'detail' => $detail);
		}
		
		private function processGroupFeedItem($feedItem){
			/*$action = '';
			if ('GROUP_MEMBERSHIP' == $feedItem->getContext())
				$action = 'joined';
			elseif ('GROUP_CREATION' == $feedItem->getContext())
				$action = 'created';
			$obj_item_details = $feedItem->getDetailOfItems(); // as group/club
			$arr = array();
			foreach($obj_item_details as $group){
				$arr[] = $feedItem->constructLink($group->getFriendlyURL(), $group->getName());		
			}
			if(count($arr) > 1){
				$action .= ' the groups/clubs ';
			}
			else
				$action .= ' the ' . $obj_item_details[0]->getLabel() . ' '; 
			$action .= implode(', ', $arr);
			$actionString = $feedItem->getDoerLink() . " $action";*/
			$obj_item_details = $feedItem->getDetailOfItems(); // as group/club
			$arr = array();
			foreach($obj_item_details as $group)
				$arr[] = '<b>' . $feedItem->constructLink($group->getFriendlyURL(), $group->getName()) . '</b>';
			
			$action = array('GROUP_MEMBERSHIP' => ' joined ', 'GROUP_CREATION' => ' created ');
			$actionString = $feedItem->getDoerLink() . ' ' . $action[$feedItem->getContext()] . 
							((count($arr) > 1)? ' ' . count($arr) . ' groups/clubs': ' a ' . strtolower($obj_item_details[0]->getLabel()));
			$detail = $this->myImploder($arr);
			//$detail = implode(", ", $arr);
			return array('actionString' => $actionString, 'detail' => $detail); 
		}
		
		private function processBulletinFeedItem($feedItem){
			$obj_item_details = $feedItem->getDetailOfItems();
			$arr = array();
			foreach($obj_item_details as $message){
				foreach($message->getDestination() as $dest){
					$path = "bulletinmanagement.php?hlEntryID=" .$message->getAttributeID();
					if('Group' == get_parent_class($dest)){
						$path .= "&groupID=" . $dest->getGroupID();
					}
				}
				$arr[] = "<b>" . $feedItem->constructLink($path, $message->getTitle()) . "</b>";
			}
			
			//$actionString = $feedItem->getDoerLink() . ' posted ' . ((count($obj_item_details) > 1)? 'bulletins ':'bulletin ') . implode(', ', $arr);
			$actionString = $feedItem->getDoerLink() . ' posted ' . ((count($obj_item_details) > 1)? count($obj_item_details). ' bulletins ':'a bulletin ');
			$detail = $this->myImploder($arr);
			return array('actionString' => $actionString, 'detail'=> $detail); 
		}
		
		private function processTravelPlanFeedItem($feedItem){
			$arr = array();
			foreach($feedItem->getDetailOfItems() as $tpWishID => $travelPlan) {
				$arr[] = '"<b>' . $travelPlan->getTitle() . '</b>"';// titles of each travel itinerary
			}
			
			switch($feedItem->getEvent()){
				case  EventType::INSERT:
					$action = 'added new';
					break;
				case EventType::UPDATE : 
					$action = 'edited ' . $feedItem->getPronoun();
					break;
				default :
					$action = '';
			}
			//$actionString = $feedItem->getDoerLink() . " $action travel " . (count($arr) > 1 ? count($arr) . " itineraries." : " itinerary.");
			//$detail = $this->myImploder($arr);
			$actionString = $feedItem->getDoerLink() . " $action travel" . ((count($arr) > 1)?" itineraries ":" itinerary ") . $this->myImploder($arr);
			return array('actionString' => $actionString);
		}
		
		private function processTravelBioFeedItem($feedItem){
			$obj_item_details = $feedItem->getDetailOfItems();
			$arr = array();
			
			foreach($obj_item_details as $answerValue)
				$arr[] = $answerValue->getValue();
			
			$actionString = $feedItem->getDoerLink() . ' edited ' . $feedItem->getPronoun() . ' TravelBio answers: (' . $this->myImploder($arr) . ')'; 
			return array('actionString' => $actionString);
		}
		
		private function processJournalEntryFeedItem($feedItem){
			$obj_item_details = $feedItem->getDetailOfItems();
			$arr = array();
			foreach($obj_item_details as $travellog){
				$path = 'journal-entry.php?action=view&travellogID=' . $travellog->getTravelLogID();
				$arr[] = $feedItem->constructLink($path, $travellog->getTitle());
			}
			
			switch($feedItem->getEvent()){
				case EventType::INSERT :
					$action = 'added new';
					break;
				case EventType::UPDATE : 
					$action = 'edited ' . $feedItem->getPronoun();
					break;
				default :
					$action = '';
			}
			
			$actionString = $feedItem->getDoerLink() . " $action journal " . ((count($arr) > 1)?"entries ":"entry ") . implode(', ', $arr);
			return array('actionString' => $actionString);
		}
		
		private function processPhotoFeedItem($feedItem){
			$obj_item_details = $feedItem->getDetailOfItems();
			$arr = array();
			foreach($obj_item_details as $photo){
				$arr[] = $photo->getThumbnailPhotoLink();
			}
			$actionString = $feedItem->getDoerLink() . ' uploaded ' . ((count($obj_item_details) > 1)?'photos ':'photo ' . implode(", ", $arr));
			return array('actionString' => $actionString);
		}
		
		private function myImploder($arr){
			return ((count($arr)<=2) ? implode(' and ', $arr) : implode(', ',array_slice($arr,0,count($arr)-1)) . ' and ' . $arr[count($arr)-1]);
		}
	}
?>
