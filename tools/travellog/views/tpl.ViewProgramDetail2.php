<?php
require_once('Class.GaDateTime.php');
require_once('Class.HtmlHelpers.php');
?>
<div class="section" id="program_details">
<span id="programID" style="display:none"><?= $programID ?></span>
<h1><?= $obj_program->getTitle() ?></h1>
<div class="content">
<div class="actions">
	<? if($ShowControls): ?>
		<a href="javascript:void(0)" onclick="manager.displayForm('mode=edit&pID=<?= $programID ?>')">Edit</a>&nbsp;|&nbsp;<a href="javascript:void(0)" onclick="manager.deleteProgram(<?= $programID ?>,<?= $groupID ?>)">Delete</a>&nbsp;|&nbsp;<a href="/photomanagement.php?action=add&cat=program&genID=<?= $programID ?>">Add Photo</a>&nbsp;|&nbsp;
	<? endif; ?>
	<a href="/photomanagement.php?action=view&cat=program&genID=<?= $programID ?>">View Photo</a>
</div>
<div id="program_info">
	<strong>Groups:</strong> <?= $subgroup_names ?><br />
	<strong>Start:</strong> <?= GaDateTime::friendlyFormat($obj_program->getTheDate()) ?><br />
	<strong>End:</strong> <?= GaDateTime::friendlyFormat($obj_program->getFinish()) ?><br />
	<div class="description">
	<?= HtmlHelpers::Textile($obj_program->getDescription()) ?>
	</div>
</div>

<div id="activitylist"><?= $inc_template ?></div>
</div>
</div>
