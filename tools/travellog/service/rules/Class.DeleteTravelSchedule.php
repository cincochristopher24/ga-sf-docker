<?php
require_once("travellog/route/model/Class.gaIAction.php");
class DeleteTravelSchedule implements gaIAction{
	function performAction( &$props ){
		require_once('travellog/model/Class.TravelSchedule.php');
		require_once('travellog/model/Class.TravelItem.php');
		
		$props['travelWishID'] = TravelSchedule::getTravelWishIDByTravelID( $props['travelID'] );
		$obj_item              = new TravelItem;
		
		$obj_item->setTravelWishID( $props['travelWishID'] );
		
		TravelSchedule::doDelete($obj_item);
		
		TravelSchedule::deleteTraveltoTravelWish($props); 
	}
}
?>
