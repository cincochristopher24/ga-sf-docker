<?php
require_once("travellog/route/model/Class.gaIRule.php");
require_once("Class.NullValidator.php");
require_once("Class.DateValidator.php");
class JournalEntrySaveRules implements gaIRule{
	function applyRules($props){
		$obj_null_validator = new NullValidator;
		$obj_date_validator = new DateValidator;

		try{
		
			$obj_null_validator->evaluate( 'Description', $props['description'] );
			$obj_null_validator->evaluate( 'Title'      , $props['title']       );
			$obj_null_validator->evaluate( 'Callout'    , $props['callout']     );
			$obj_date_validator->evaluate( 'Date'       , $props['arrivaldate'] );
			if( AbstractValidatorObserver::hasErrors() ){
				throw new Exception('Errors encountered during saving.'); 
			}
		}catch(Exception $e){ throw new Exception('Expected parameters are undefined.'); } 		
		
	}
}
?>
