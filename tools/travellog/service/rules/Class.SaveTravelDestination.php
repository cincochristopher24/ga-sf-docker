<?php
require_once("travellog/route/model/Class.gaIAction.php");
class SaveTravelDestination implements gaIAction{
	function performAction( &$props ){
		require_once('travellog/model/Class.TravelDestinationPeer.php');
		require_once('travellog/model/Class.TravelDestination.php');
		require_once('travellog/model/Class.TravelSchedule.php');
		
		$obj_destination       = new TravelDestination;
		$props['travelWishID'] = TravelSchedule::getTravelWishIDByTravelID( $props['travelID'] );
		
		$obj_destination->setTravelWishID( $props['travelWishID'] );
		$obj_destination->setCountryID   ( $props['countryID']    );
		$obj_destination->setStartDate   ( $props['arrivaldate']  );
		$obj_destination->setEndDate     ( $props['arrivaldate']  );
	
		TravelDestinationPeer::add($obj_destination);
	}
}
?>
