<?php
require_once("travellog/route/model/Class.gaIAssessor.php");
class IsDestinationExists implements gaIAssessor{
	function evaluate( $props ){
		require_once('travellog/model/Class.TravelDestinationPeer.php');
		return ( TravelDestinationPeer::destinationExists($props['travelWishID'], $props['countryID']) )? true : false;
	}
}
?>
