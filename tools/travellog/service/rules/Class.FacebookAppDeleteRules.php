<?php
require_once("travellog/route/model/Class.gaIRule.php");
class FacebookAppDeleteRules implements gaIRule{
	
	private $factory = null; 
	
	function __construct($factory){
		$this->factory = $factory;
	}
	
	function applyRules( $props ){
		
		if( $props['type'] == 'journal' )
			
			$this->factory->createAction('DeleteTravelSchedule')->performAction( $props );

		else{
			
			if( $this->factory->createAssessor('IsLocationExists')->evaluate( $props ) )
				$this->factory->createAction('UpdateTravelDestination')->performAction( $props );
				
			elseif( $this->factory->createAssessor('HasAnEntries')->evaluate( $props ) )
				$this->factory->createAction('DeleteTravelDestination')->performAction( $props );
				
			else
				$this->factory->createAction('DeleteTravelSchedule')->performAction( $props ); 
			
		}
	}
}
?>
