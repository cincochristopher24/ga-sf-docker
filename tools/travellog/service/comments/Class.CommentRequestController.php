<?php
/*
 * Created on Oct 10, 2007
 * Class.CommentRequestController.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 

class CommentRequestController{
	public static function getRequest($request){
		require_once("travellog/service/comments/Class.CommentRequestFactory.php");
		return CommentRequestFactory::create($request);
	}	
}
?>
