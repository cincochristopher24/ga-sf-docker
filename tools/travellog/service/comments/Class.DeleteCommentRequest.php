<?php
/*
 * Created on Oct 10, 2007
 * Class.DeleteCommentRequest.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */
 
class DeleteCommentRequest{
	
	function DeleteCommentRequest($request){
	  
		require_once('travellog/model/Class.Comment.php');	
		
		$comment = new Comment($request['commentid']);
		$contextID	=	$request['contextID'];
		$type		=	$request['context'];	
 	 		
 		$context = $comment->getContext();
 		$context->removeComment($comment); 	 		
 		$comment->Delete();

 		// invalidate cached views(html) of comments
 		require_once('Cache/ganetCacheProvider.php');
 		$cache	=	ganetCacheProvider::instance()->getCache();
 		$key 	=	'CommentsView_hashes_' . $type . '_' . $contextID;
 		if ($cache != null){
 			$hashKeys	=	$cache->get($key);
 			if ($hashKeys){
 				// found array of keys to invalidate
 				foreach ($hashKeys as $hashKey){
 					$cache->delete($hashKey);
 				}
 				
 				$cache->delete($key);
 			} 
 		} 
 		
 		
 		require_once('travellog/service/comments/Class.ViewCommentRequest.php');
		return new ViewCommentRequest($request);
	}
	
} 
 
?>
