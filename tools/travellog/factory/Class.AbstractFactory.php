<?php
abstract class AbstractFactory{
	
	protected $controllers = array(); 
	
	abstract function createController($name = 'Index');
}
?>
