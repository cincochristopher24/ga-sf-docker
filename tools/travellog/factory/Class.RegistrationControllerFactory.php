<?php
	/*
	 * Class.RegistrationControllerFactory.php
	 * Created on Nov 20, 2007
	 * created by marc
	 */
	
	require_once("travellog/helper/Class.RegisterHelper.php");
	require_once("travellog/factory/Class.ControllerFactory.php");
	require_once("Class.Constants.php");
	require_once('travellog/model/Class.SiteContext.php');
	
	class RegistrationControllerFactory{
		
		static $instance = NULL;
		static $controllerFactory = NULL;
		static $helper = NULL;
		
		static function getInstance(){
	 		
	 		if(  NULL == self::$instance ){
	 			self::$helper = RegisterHelper::getInstance();
	 			self::$controllerFactory = ControllerFactory::getInstance();
	 			self::$instance = new self;
	 		}
	 		
	 		return self::$instance;
	 	}
	 	
	 	function create(){
			// edits: replaced condition, check if in cobrand instead
			if(SiteContext::getInstance()->isInCobrand()){
	 			return self::$controllerFactory->createController("Registration");
	 		}
	 		else{
	 			if( constants::GROUP == self::$helper->getUser() ){
	 				require_once("travellog/controller/Class.AdvisorRegistrationController.php");
	 				return new AdvisorRegistrationController;
	 			}
	 			elseif( constants::GACOM_CLIENT == self::$helper->getUser() ){
	 				require_once("travellog/controller/Class.GAComRegistrationController.php");
	 				return new GAComRegistrationController;
	 			}
	 			else{
	 				require_once("travellog/controller/Class.TravelerRegistrationController.php");
	 				return new TravelerRegistrationController;
	 			}
	 				 
	 		}
	 		 
	 	}
	 	
	} 
?>
