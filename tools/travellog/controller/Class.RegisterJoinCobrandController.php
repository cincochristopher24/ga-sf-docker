<?php

require_once("travellog/factory/Class.FileFactory.php");
require_once("travellog/controller/Class.IController.php");

class RegisterJoinCobrandController implements IController{
	
	protected $config;
	protected $adminGroup;
	protected $file_factory;
	
	public function __construct(){
		$this->file_factory = FileFactory::getInstance();
		
		$this->file_factory->registerClass('Config');
		$this->file_factory->registerClass("AdminGroup", array("path" => "travellog/model/"));
		
 		$this->config = $this->file_factory->getClass('Config');

		$GLOBALS['CONFIG'] = $this->config;
	}
	
	public function performAction(){
		//if( !$this->config->getGroupID() ){
			if( $this->isAjaxRequest() ){
				header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
			}else{
				header("location: /");
			}
			exit;
		//}
		
		$this->adminGroup = $this->file_factory->getClass("AdminGroup", array($this->config->getGroupID()));
		
		echo "Page under construction...";
		exit;
	}
	
	protected function isAjaxRequest(){
		if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) 
			&& strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ){
			return TRUE;
		}
		if(	!empty($_SERVER['HTTP_X_REQUESTED_WITH']) 
			&& strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			return TRUE;
		}
		return FALSE;
	}
}