<?php
require_once('travellog/controller/Class.AbstractProgramController.php');
class GANETProgramController extends AbstractProgramController{
	
	function performAction(){
		$this->data           = array_merge($_GET, $_POST);
		$this->data['action'] = ( isset($this->data['action']))? $this->data['action'] : 'View';
		if( isset($this->data['context']) ){
			$activity_controller = $this->file_factory->getClass('ActivityController');
			$activity_controller->performAction();   				
		}
		else{
			parent::__applyRules(); 
			parent::performAction();
		}	        
	} 
}
?>
