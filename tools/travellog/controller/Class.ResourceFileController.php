<?		
		
   		require_once('Class.Template.php');		
   		require_once('travellog/model/Class.UploadManager.php');
		require_once('travellog/model/Class.Resume.php');
		require_once('travellog/model/Class.ResourceFiles.php');
		require_once('travellog/model/Class.Traveler.php');  
		require_once('travellog/model/Class.TravelerProfile.php');  
		require_once('Class.Connection.php');
		require_once('Class.Recordset.php');
		require_once("travellog/model/Class.SubNavigation.php");
	
	class ResourceFileController{		

		/**
		 * $loginID EQ travelerID if user is login else $loginID EQ 0
		 */
		private $loginID = 	0;
				
		/*******************************
		 * *****************************
		 * * $context EQ Resume, Program, AdminGroup
	     * ****************************
		 ******************************/			
		private $context; 
		
		/**
		 * $genericID is IDs of Context
		 */
		private $genericID;
		private $resourcefile;
		private $owner	= false;
		private $isparent	= false;
		private $subgroups = array();
		private $classcontext;
		
		private $parentgroup = NULL;
		
		private $headercaption	= NULL;
		private $backlink			= NULL;
		private $backcaption		= NULL;
		
		private $subnavigation;
		
		private $upload_files	= array();
		private $invalidfiles 	= NULL;
		private $validfiles	= NULL;
		
		private $norecordlabel	= NULL;
		
		private $resourcefiletype= NULL;
		
		function setLoginID($loginID){
			$this->loginID = $loginID;	
		}
		
		function setGenericID($genericID){
			$this->genericID = $genericID;	
		}
		
		
		function setContext($context){
			$this->context = $context;			
		}		
		
		
		function initialize(){
				
					$subNavigation = new SubNavigation();	
				
					switch($this->context){						
							case "resume":
									$resume 	= new Resume($this->genericID);
									$this->resourcefile = $resume->getResourceFiles(); 
									
									$this->resourcefiletype = 2;
									
									if($this->loginID == $this->genericID){			
										$subNavigation->setContextID($this->loginID);
										$subNavigation->setContext('TRAVELER');
										$subNavigation->setLinkToHighlight('RESUME');
									}
									
									$profile	= new TravelerProfile($this->genericID);									
									$this->headercaption = $profile->getUserName()."'s  uploaded Resume";
									
									$this->backlink 		= "/resume.php?action=view";
									$this->backcaption	= $profile->getUserName()."'s  "."Resume";
									
									if(!count($this->resourcefile)){
										$this->norecordlabel = "There are no uploaded resume yet.";	
									}
																	
									if($this->genericID == $this->loginID)
									$this->owner =   true;									
									$this->classcontext = $resume;
							break;				
							
							case "admingroup":
									
									$admingroup 	= new AdminGroup($this->genericID);
									
									if($admingroup->isParent()){
										$this->isparent =  true;
										$this->subgroups = $admingroup->getSubGroups();	
									}else{
										$this->parentgroup = $admingroup->getParent();
										$admgroup 	= new AdminGroup($this->parentgroup->getGroupID());
										$this->subgroups = $admgroup->getSubGroups();	
									}
									
									$this->resourcefiletype = 1;
									
									$subNavigation->setContextID($this->genericID);
									$subNavigation->setContext('GROUP');
									$subNavigation->setGroupType('ADMIN_GROUP');									
									$subNavigation->setLinkToHighlight('RESOURCE_FILES');
									
									$this->headercaption = "Resource Files";
									
									$this->backlink 		= "/group.php?gID=".$this->genericID;
									$this->backcaption	= $admingroup->getName()."'s  Group";	
									
									if(!count($this->resourcefile)){
										$this->norecordlabel = "There are no resource files yet.";	
									}
									
									if($admingroup->getAdministrator()->gettravelerID() == $this->loginID){
										$this->owner =   true;
									}
									
									if($this->owner){
										$this->resourcefile = $admingroup->getResourceFiles(); 
									}else{
									
										//check if member of the group
									
										$traveler =  new Traveler($this->loginID);
									
										//if member: get resourcefile where privacypreference eq member and show all
										if($admingroup->isMember($traveler)){
											$this->resourcefile = $admingroup->getResourceFiles(NULL,1);
										}else{  //else get files eq show all only
											$this->resourcefile = $admingroup->getResourceFiles(NULL,2); 
										}
									}
																		
									$this->classcontext = $admingroup;			
									
							break;
							
						
					}				
					
					$this->subnavigation = $subNavigation;
		}
		
		
		function getClassContext(){
			return $this->classcontext;	
		}
		
		/*
		* Parse Uploaded results
		* @param: array
		* return: array  
		*/
		function parseParam($param){
			
			
			$resultparam = $_SERVER['DOCUMENT_ROOT'].'/users/'.$param.'.params';
			
			if(file_exists($resultparam)){				
				$params 		= parse_ini_file($resultparam);				
				foreach($params as $p => $prs){
					if(strpos($p,'upfile') !== false){
						array_push($this->upload_files,$prs);
					}
				}
			}
				
			if(file_exists($resultparam)){
				unlink($_SERVER['DOCUMENT_ROOT'].'/users/'.$param.'.params');
			}
									
			return $this->upload_files;
		}
		
		
		
		
		function AddResourceFile($privacypreference=0){
			
			$uploadmanager = new UploadManager();
			$uploadmanager->setContext($this->classcontext);			
			$uploadmanager->setFiles($this->upload_files);
			$uploadmanager->setTypeUpload('PDF');
			$uploadmanager->upload();		
			
			if(count($uploadmanager->getValid())){																									
				foreach($uploadmanager->getValid() as $ufile){							
						$resourcefile = new ResourceFiles();
						$resourcefile->setContext($this->classcontext);
						$resourcefile->setTheType('PDF');
						$resourcefile->setFileName($ufile->getName());
						$resourcefile->setCaption($ufile->getCaption());										
						$resourcefile->setResourceFileType($this->resourcefiletype);
						$resourcefile->setPrivacypreference($privacypreference);
						$resourcefile->Create();	
				}						
				$this->validfiles = $uploadmanager->getValid();	
			}else{
				$this->invalidfiles = $uploadmanager->getInValid();	
			}				
				
		}
		
		function getValidFiles(){
			return $this->validfiles;
		}	
		
		function getInvalidFiles(){
			return $this->invalidfiles;
		}
					
		function getResourceFile(){
				return $this->resourcefile;
		}
		
		function getResourceFileCount(){
			return count($this->resourcefile);	
		}
				
		/**
		 * Check if user is the owner: Return True if owner else False 
		 */
		function checkResourceFileOwnerShip(){
			return $this->owner;
		}
		
		function Delete($resourcefileID){
			$resourceFiles 	= new ResourceFiles($resourcefileID);
			$resourceFiles->setContext($this->classcontext);
			$resourceFiles->Delete();			
		}
		/*
		function Update($afiles,$caption,$resourcefileID){

			
			$uploadmanager = new UploadManager();
			$uploadmanager->setContext($this->classcontext);			
			$uploadmanager->setFiles($afiles);
			$uploadmanager->setTypeUpload('PDF');
			$uploadmanager->upload();	
			
			if(count($uploadmanager->getValid())){																									
						foreach($uploadmanager->getValid() as $ufile){							
								
								$resourcefile = new ResourceFiles($resourcefileID);
								
								$uploadmanager->deletefile($resourcefile->getFileName());
								
								$resourcefile->setContext($this->classcontext);
								$resourcefile->setFileName($ufile->getName());								
								$resourcefile->setCaption($caption);								
								
								$resourcefile->Update();	
						}						
				return true;
			}else{
				$this->invalidfiles = $uploadmanager->getInValid();	
			}				
			return false;
						
		}
		*/
		
		function Update($resourcefileID){			
			$uploadmanager = new UploadManager();
			$uploadmanager->setContext($this->classcontext);			
			$uploadmanager->setFiles($this->upload_files);
			$uploadmanager->setTypeUpload('PDF');
			$uploadmanager->upload();	
			
			if($uploadmanager->getValid()){																									
				foreach($uploadmanager->getValid() as $ufile){	
						$resourcefile = new ResourceFiles($resourcefileID);
						$uploadmanager->deletefile($resourcefile->getFileName());								
						$resourcefile->setContext($this->classcontext);
						$resourcefile->setFileName($ufile->getName());
						$resourcefile->setCaption($ufile->getCaption());
						$resourcefile->Update();	
				}						
				$this->validfiles = $uploadmanager->getValid();		
			}else{
				$this->invalidfiles = $uploadmanager->getInValid();	
			}				
						
		}
		
		function UpdateCaption($resourcefileID,$caption){
			
			$resourcefile = new ResourceFiles($resourcefileID);									
			$resourcefile-> setCaption($caption);		
			$resourcefile->Update();
		}
		
		function setPrivacyPreference($resourcefileID,$preference){
			
			$resourcefile = new ResourceFiles($resourcefileID);									
			$resourcefile-> setPrivacypreference($preference);		
			$resourcefile->Update();
		}
		
		
		function showedit($resourcefileID){
			$resourcefile = new ResourceFiles($resourcefileID);
			return $resourcefile;
		}
		
		
		function getHeaderCaption(){
			return $this->headercaption;	
		}
		
		function getBackLink(){
			return $this->backlink;	
		}
		
		function getBackCaption(){
			return $this->backcaption;	
		}
		
		function getSubNavigation(){
			return $this->subnavigation;	
		}
		
		
		function getNorecordlabel(){
			return $this->norecordlabel;	
		}
		
		function isParent(){
			return $this->isparent;	
		}
		
		
		function getParentgroup(){
			return $this->parentgroup;	
		}
		
		function subGroups(){
			return $this->subgroups;	
		}
		
		
		function getAllFiles(){
			$subfiles	= array();
			$allfiles	= array();			 
			$allfileids	= array();
			$prgfiles	= array();
			
			if(count($this->subgroups)){
					foreach($this->subgroups as $subgroup){
						if($subgroup->getResourceFiles()){
							foreach($subgroup->getResourceFiles() as $rfiles){
								$subfiles[] = $rfiles; 
							}				
						}
					}
			}
			/*
			if($this->resourcefile){
				$arrfiles =  array_merge($this->resourcefile,$subfiles);
				
			}else{
				$arrfiles =  $subfiles;	
			}
			*/
			if($this->isparent){ //if not parentgroup get files from parent
				
				if($this->resourcefile){
				$arrfiles =  array_merge($this->resourcefile,$subfiles);
				
				}else{
					$arrfiles =  $subfiles;	
				}
				
				
			}else{
				$parentgroup 	= new AdminGroup($this->parentgroup->getGroupID());
				if($parentgroup->getResourceFiles()){
					foreach($parentgroup->getResourceFiles() as $prfiles){
						$prgfiles[] = $prfiles; 
					}				
				}
				
				$arrfiles =  array_merge($subfiles,$prgfiles);
				
			}
			
			
			
			
			//print_r($arrfiles);
			
			
			
			if(count($arrfiles)){
				
				foreach($arrfiles as $rfiles){
						if(!in_array($rfiles->getResourceFileID(),$allfileids)){
								array_push($allfiles,$rfiles);
								array_push($allfileids,$rfiles->getResourceFileID());
						}
				}	
			}	
			
			
					
			return	$allfiles;			
		}
	
	}



?>
