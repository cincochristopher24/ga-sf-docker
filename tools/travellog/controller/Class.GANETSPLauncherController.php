<?php
	
require_once("travellog/controller/Class.AbstractSPLauncherController.php");
	 
class GANETSPLauncherController extends AbstractSPLauncherController{
	
	protected function getDestinationServer(){
		if($_SERVER["SERVER_NAME"] == 'staging.goabroad.net'){
			return 'staging.goabroad.net';
		}
		
		return FALSE === strpos($_SERVER["SERVER_NAME"],".local") ? "goabroad.net" : "goabroad.net.local";
	}
}