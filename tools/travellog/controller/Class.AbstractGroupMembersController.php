<?php
	/*
	 *	@author CheWyl
	 *	11 November 2008
	 *
	 *	Last edited on:		22-Dec-2009
	 *	Purpose:			bug fixes for staff
	 */

	require_once 'travellog/factory/Class.FileFactory.php';
	require_once 'travellog/controller/Class.IController.php';
	require_once 'Class.Template.php';
	require_once ('travellog/model/Class.Condition.php');
	require_once ('travellog/model/Class.FilterOp.php');
	require_once ('travellog/model/Class.FilterCriteria2.php');

	abstract class AbstractGroupMembersController implements iController{

		protected $mIsLogged		= false;		
		protected $mIsSubGroup		= false;

		protected $mIsGroupExist	= false;
		protected $mIsAdminLogged	= false;
		protected $mIsMemberLogged	= false;
		protected $mIsStaffLogged	= false;
		protected $mIsSuperStaffLogged = false;

		protected $mGroup			= NULL;
		protected $mSession			= NULL;
		protected $file_factory		= NULL;
		protected $mLoggedUser		= NULL;
		protected $mMainGroupInfo	= NULL;

		protected $mGroupID			= 0;
		protected $mGroupType		= 0;
		protected $mViewerType		= 0;
		protected $arrError			= array();

		protected $mMode			= NULL;
		protected $mSmode			= NULL;
		
		const VIEW_MEMBERS					= 1;
		const VIEW_STAFF					= 2;
		const INVITE_MEMBERS				= 3;
		const ADD_STAFF						= 4;
		const VIEW_PENDING					= 5;
		const VIEW_REQUESTS					= 6;
		const PROCESS_MEMBER				= 7;
		const VIEW_MEMBERS_ACTIVITY_FEED 	= 8;

		function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template', array('path' => ''));
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('MembersPanel');
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('SessionManager');
			$this->file_factory->registerClass('CustomizedGroupSection');
			$this->file_factory->registerClass('GroupMembershipUpdates', array('path' => 'travellog/model/updates'));
			$this->file_factory->registerClass('ViewStaff',				array('path' => 'travellog/views/member_staff'));
			$this->file_factory->registerClass('AssignStaff',			array('path' => 'travellog/views/member_staff'));
			$this->file_factory->registerClass('InviteStaff',			array('path' => 'travellog/views/member_staff'));
			$this->file_factory->registerClass('ViewAccordion',			array('path' => 'travellog/views/member_staff'));
			$this->file_factory->registerClass('ViewPendingStaff',		array('path' => 'travellog/views/member_staff'));
			$this->file_factory->registerClass('ViewStaffHistory',		array('path' => 'travellog/views/member_staff'));
			$this->file_factory->registerClass('ViewHistory',			array('path' => 'travellog/views/member_staff'));			
			$this->file_factory->registerClass('ProfileCompFactory',	array('path' => 'travellog/components/profile/controller/'));
			
			$this->file_factory->registerClass('ViewPendingMember',		array('path' => 'travellog/views/member_staff'));
			$this->file_factory->registerClass('ViewSearchMember',		array('path' => 'travellog/views/member_staff'));
			$this->file_factory->registerClass('InviteMember',			array('path' => 'travellog/views/member_staff'));
			$this->file_factory->registerClass('ViewInviteMember',		array('path' => 'travellog/views/member_staff'));
			$this->file_factory->registerClass('ViewJoinRequest',		array('path' => 'travellog/views/member_staff'));
			$this->file_factory->registerClass('ViewMembersActivityFeed', array('path' => 'travellog/views/member_staff'));

			$this->file_factory->registerTemplate('LayoutMain');

			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');

			//require_once 'travellog/domains_bak/' . $_SERVER['SERVER_NAME'] . '.php';
		}

		function performAction(){
			$mParams = array();
			
			$mParams['gID']		= isset($_GET['gID']) ? $_GET['gID'] : 0;
			$mParams['cID']		= isset($_GET['cID']) ? $_GET['cID'] : 0;
			$mParams['tID']		= isset($_GET['tID']) ? $_GET['tID'] : 0;
			$mParams['mode']	= isset($_GET['mode']) ? $_GET['mode'] : 1;
			$mParams['type']	= isset($_GET['type']) ? $_GET['type'] : 0;
			$mParams['page']	= isset($_GET['page']) ? $_GET['page'] : 1;
			$mParams['sMode']	= isset($_GET['sMode'])? $_GET['sMode'] : NULL;
			
			$this->mMode = $mParams["mode"];
			$this->mGroupID = $mParams["gID"];
			$this->mSmode = $mParams["sMode"];
			
			if( !$this->mGroupID && isset($GLOBALS["CONFIG"]) ){
				$mParams["gID"] = $GLOBALS["CONFIG"]->getGroupID();
				$this->mGroupID = $mParams["gID"];
			}else if( !$this->mGroupID && !isset($GLOBALS["CONFIG"]) ){
				header("location: group.php");
				exit;
			}
			
			if( isset($_GET['sMode']) ){
				$this->_searchMembers($mParams);
				return;
			}
			
			if( isset($_POST["action"]) && 2 == $_POST["action"] ){
				$this->_processInvites($mParams);
			}
			
			$this->_prepareDependencies();

		
			switch($mParams['mode']){
				case self::VIEW_STAFF:
						$this->_viewStaff($mParams);
						break;
				case self::INVITE_MEMBERS:
						$this->_viewInviteMembers($mParams);
						break;
				case self::ADD_STAFF:
						if (isset($_POST['btnInvite'])){
							$vars = array_merge($mParams, $_POST);
							$this->_viewInviteStaff($vars);
						}
						else if (isset($_POST['btnAdd'])){
							$vars = array_merge($mParams, $_POST);
							$this->_viewAddStaff($vars);
						}
						else if (isset($_POST['btnCancel']))
							$this->_viewAssignStaff($mParams);
						else if (isset($_GET['Invite'])){
							if (isset($_GET['tID']))
								$this->_viewInviteToDiffGroup($mParams);
							else
								$this->_viewAssignStaff($mParams);
						}
						else
							$this->_viewAssignStaff($mParams);
						break;
				case self::VIEW_PENDING:
						$this->_viewPendingInvites($mParams);
						break;
				case self::VIEW_REQUESTS:
						$this->_viewRequests($mParams);
						break;
				case self::VIEW_MEMBERS_ACTIVITY_FEED:
						//$this->_viewMembersActivityFeed($mParams);
						header("location: /group.php");
						exit;
						break;
				default:
						$this->_viewMembers($mParams);
			}
		}

		function _defineCommonAttributes($_params = array())
		{
			$this->mSession = $this->file_factory->invokeStaticClass('SessionManager', 'getInstance');
			$mTravelerID = $this->mSession->get('travelerID');
			
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			
			$this->mTemplate = $this->file_factory->getClass('Template');
			$this->mTemplate->set_path('travellog/views/member_staff/');

			// check if user is currently logged in
			if (0 < $mTravelerID)
			{
				$this->mIsLogged	= true;
				$this->mLoggedUser	= $this->file_factory->getClass('Traveler', array($mTravelerID));

				// start GA Logger
				require_once 'gaLogs/Class.GaLogger.php';
				try{
					GaLogger::create()->start($mTravelerID, 'TRAVELER');
				}
				catch(exception $e){
				}
			}

			// if groupID is defined, instantiate group
			if ( 0 < $_params['gID'] )
			{
				$mGroup = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array($_params['gID']));
				
				if( 0 == count($mGroup) )
				{
					header("location: group.php");
					exit;	
				}

				if (!$mGroup[0]->IsSuspended())
				{					
					$this->mGroupID = $_params['gID'];
					$this->mGroup = $mGroup[0];

					// check group type, this feature is only for admin group
					if ($this->mGroup->getDiscriminator() == GROUP::FUN){
						if ($this->mIsLogged){
							if ($this->mGroup->getAdministratorID() == $mTravelerID){
								$this->mIsAdminLogged = true;
								$this->mViewerType = 1;
							}else if ($this->mGroup->isMember($this->mLoggedUser)){
								$this->mIsMemberLogged = true;
								$this->mViewerType = 4;
							}else{
								$this->mViewerType = 5;
							}
						}
						else{
							$this->mViewerType = 6;
						}
					}
					else if($this->mGroup->getDiscriminator() == GROUP::ADMIN){
						// if SUBGROUP
						if ($this->mGroup->isSubGroup()){
							$this->mIsSubGroup = true;
							$mParent = $this->mGroup->getParent();

							// check viewer type
							if ($this->mIsLogged){
								if ($mParent->getAdministratorID() == $mTravelerID){
									$this->mIsAdminLogged = true;
									$this->mViewerType = 1;
								}else if ($mParent->isSuperStaff($mTravelerID)){
									$this->mIsSuperStaffLogged = true;
									$this->mViewerType = 2;
								}else if ($this->mGroup->isStaff($mTravelerID)){
									$this->mIsStaffLogged = true;
									$this->mViewerType = 3;
								}else if ($this->mGroup->isMember($this->mLoggedUser)){
									$this->mIsMemberLogged = true;
									$this->mViewerType = 4;
								}else
									$this->mViewerType = 5;
							}
							else
								$this->mViewerType = 5;
						}
						// if PARENT GROUP
						else{
							// check viewer type
							if ($this->mIsLogged){
								if ($this->mGroup->getAdministratorID() == $mTravelerID){
									$this->mIsAdminLogged = true;
									$this->mViewerType = 1;
								}else if ($this->mGroup->isSuperStaff($mTravelerID)){
									$this->mIsSuperStaffLogged = true;
									$this->mViewerType = 2;
								}else if ($this->mGroup->isRegularStaff($mTravelerID)){
									$this->mIsStaffLogged = true;
									$this->mViewerType = 3;
								}else if ($this->mGroup->isMember($this->mLoggedUser)){
									$this->mIsMemberLogged = true;
									$this->mViewerType = 4;
								}else
									$this->mViewerType = 5;
							}
							else
								$this->mViewerType = 5;
						}
					}

					$this->mSubNav = $this->file_factory->getClass('SubNavigation');
					$this->mSubNav->setContext('GROUP');
					$this->mSubNav->setContextID($_params['gID']);
					$this->mSubNav->setLinkToHighlight('MEMBERS');
					if ($this->mGroup->getDiscriminator() == GROUP::ADMIN){
						$this->mSubNav->setGroupType('ADMIN_GROUP');
					}else if($this->mGroup->getDiscriminator() == GROUP::FUN){
						$this->mSubNav->setGroupType('FUN_GROUP');
					}

					$mFactory		= $this->file_factory->invokeStaticClass('ProfileCompFactory', 'getInstance', array());
					$mProfileComp	= $mFactory->create( ProfileCompFactory::$GROUP_CONTEXT );

					if(2 == $this->mGroup->getDiscriminator() && (1 < $this->mGroup->getParentID()))
						$mProfileComp->init($this->mGroup->getParentID());
					else
						$mProfileComp->init($this->mGroupID);
					$this->mMainGroupInfo = $mProfileComp->get_view();
				}
				else{
					header("location: group.php");
					exit;
				}
			}
		}

		function _checkPrivileges(){
			if (!$this->mIsLogged){
				header("location: login.php");
				exit;
			}
			else{
				if (4 == $this->mViewerType || 5 == $this->mViewerType){
					$location = "members.php?gID=" . $this->mGroupID . "&mode=2";
					header("location: $location");
					exit;
				}
			}
		}

		function _prepareDependencies(){
			//Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
			Template::setMainTemplateVar('layoutID', 'admin_group');

			Template::includeDependentCss('/min/f=css/member_staff.css');
			//Template::includeDependentJs('/js/jquery-1.1.4.pack.js');
			
			Template::includeDependentCss("/css/vsgStandardized.css");
			Template::includeDependentCss("/css/modalBox.css");
			
			switch($this->mMode){
				case self::VIEW_MEMBERS	:	Template::includeDependentCss('/min/f=css/Thickbox.css');
											Template::includeDependentJs('/min/g=MemberViewJs');
											//Template::includeDependentJs('/js/thickbox.js');
											//Template::includeDependentJs('/js/groupMemberSearchManager.js');
											//Template::includeDependentJs('/js/joinRequestsManager-1.1.js');
											//Template::includeDependentJs('/js/staffManager-1.1.js');
											Template::includeDependentJs('/min/f=js/prototype.js');
											break;

				case self::INVITE_MEMBERS:	Template::includeDependentJs('/min/g=InviteStaffJs');
											//Template::includeDependentJs('/js/inviteMembersManager.js');
											//Template::includeDependentJs('/js/staffManager-1.1.js');
											//Template::includeDependentJs('/min/f=js/prototype.js');
											break;

				case self::VIEW_PENDING	:	Template::includeDependentJs('/min/g=PendingInviteJs');
											//Template::includeDependentJs('/js/pendingInvitesManager.js');
											//Template::includeDependentJs('/js/staffManager-1.1.js');
											//Template::includeDependentJs('/min/f=js/prototype.js');
																						$code = <<<BOF
											<script type="text/javascript"> 
											//<![CDATA[
												StaffManager.setMode($this->mMode);
												StaffManager.setGroupId($this->mGroupID);
											//]]>
											</script>
BOF;
											Template::includeDependent($code);

											break;

				case self::VIEW_REQUESTS:	Template::includeDependentJs('/min/g=RequestMemberJs');
											//Template::includeDependentJs('/js/joinRequestsManager-1.1.js');
											//Template::includeDependentJs('/js/staffManager-1.1.js');
											//Template::includeDependentJs('/min/f=js/prototype.js');
											break;
				case self::VIEW_STAFF:

						Template::includeDependentCss('/min/f=css/Thickbox.css');
						Template::includeDependentJs('/min/g=ViewStaffJs');
						//Template::includeDependentJs('/js/thickbox.js');
						//Template::includeDependentJs('/js/staffManager-1.1.js');
						//Template::includeDependentJs('/min/f=js/prototype.js');
				$code = <<<BOF
				<script type="text/javascript"> 
				//<![CDATA[
					StaffManager.setMode($this->mMode);
					StaffManager.setGroupId($this->mGroupID);
				//]]>
				</script>
BOF;
						Template::includeDependent($code);

						break;
				case self::ADD_STAFF:

						Template::includeDependentJs('/min/g=AddStaffJs');
						//Template::includeDependentJs('/js/staffManager-1.1.js');
						//Template::includeDependentJs('/min/f=js/prototype.js');
				$code = <<<BOF
				<script type="text/javascript"> 
				//<![CDATA[
					StaffManager.setMode($this->mMode);
					StaffManager.setGroupId($this->mGroupID);
				//]]>
				</script>
BOF;
						Template::includeDependent($code);
						break;

				case self::VIEW_MEMBERS_ACTIVITY_FEED:
						Template::includeDependentCss('/min/f=css/Thickbox.css');
						Template::includeDependentJs('/min/g=MemberViewJs');
						//Template::includeDependentJs('/js/thickbox.js');
						//Template::includeDependentJs('/js/groupMemberSearchManager.js');
						//Template::includeDependentJs('/js/joinRequestsManager-1.1.js');
						//Template::includeDependentJs('/js/staffManager-1.1.js');
						//Template::includeDependentJs('/min/f=js/prototype.js');
				$code = <<<BOF
				<script type="text/javascript"> 
				//<![CDATA[
					gmsManager.setGroupID($this->mGroupID);
				//]]>
				</script>
BOF;
						Template::includeDependent($code);
						break;
				default:
						Template::includeDependentJs('/js/jquery.staffmanager.js');
			}
		}

		function _validate($_param = ''){
			if (($_param['mode'] == 4) && (isset($_param['btnInvite']))){
				if (0 == strlen($_param['email_textarea']))
					$this->arrError['email'] = 'Please enter at least one email address';
				else if (0 < strlen($_param['email_textarea'])){
					$mEmailAdds = str_replace(' ', '', $_param['email_textarea']);
					$mArrEmails = array_unique(explode(",", $mEmailAdds));

				 	if (0 < count($mArrEmails)) {
						foreach($mArrEmails as $each){
							$each = trim($each);

							// validate email
							if ($each) {
								//if (!er_egi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $each)){
								//if (!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i', $each)){	
								if (!preg_match('/^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i', $each)){	
									if (0 == count($this->arrError))
										$this->arrError['email'] = 'Please enter a valid email address';
								}
							}
						}
					}
				}

				if (0 == $_param['cmbStaffType'])
					$this->arrError['staff_type'] = 'Please choose a type of staff';
				if (0 == $_param['cmbGroup'])
					$this->arrError['group'] = 'Please choose a group from the list';					
			}
			else if (($_param['mode'] == 4) && (isset($_param['btnAdd']))){
				$arrEmailAdd = isset($_param['hdnEmailAdd']) ? explode(",", $_param['hdnEmailAdd']) : array();

				if (1 < count($arrEmailAdd)){
					// loop through non-members
					for ($ctr = 1; $ctr <= count($arrEmailAdd); $ctr++){
						$firstname = 'txtFirstName_temp' . $ctr;
						$lastname = 'txtLastName_temp' . $ctr;
						$username = 'txtUserName_temp' . $ctr;
						$password = 'txtPassword_temp' . $ctr;
						$mEmail = $arrEmailAdd[$ctr-1];

						if (isset($_param['chk_temp'.$ctr])){
							if (0 == strlen($_param[$firstname]))
								$this->arrError[$firstname] = 'First Name is a Required field';
							if (0 == strlen($_param[$lastname]))
								$this->arrError[$lastname] = 'Last Name is a Required field';
							if (0 == strlen($_param[$username]))
								$this->arrError[$username] = 'Username is a Required field';
							if (0 == strlen($_param[$password]))
								$this->arrError[$password] = 'Password is a Required field';						
							elseif (0 < strlen($_param[$password]) && 6 > strlen($_param[$password]))
								$this->arrError[$password] = 'Password must at least be six characters';

							if (0 < strlen($_param[$username])){
								// check if username is unique
								$mUserName = $_param[$username];
								$isUserNameTaken = $this->file_factory->invokeStaticClass('MembersPanel', 'isUserNameTaken', array($mUserName));
								if ($isUserNameTaken)
									$this->arrError[$mUserName] = 'Username <u>' . $mUserName . '</u> is already taken';
							}
						}
					}
				}
				elseif (1 == count($arrEmailAdd)){
					$mEmail = $arrEmailAdd[0];

					if (isset($_param['hdnTravelerId']) && isset($_param['chk_temp1']) || !isset($_param['hdnTravelerId'])){
						if (0 == strlen($_param['txtFirstName_temp1']))
							$this->arrError['txtFirstName_temp1'] = 'First Name is a Required field';
						if (0 == strlen($_param['txtLastName_temp1']))
							$this->arrError['txtLastName_temp1'] = 'Last Name is a Required field';
						if (0 == strlen($_param['txtUserName_temp1']))
							$this->arrError['txtUserName_temp1'] = 'Username is a Required field';
						elseif (0 < strlen($_param['txtUserName_temp1']) && 25 < strlen($_param['txtUserName_temp1']))
							$this->arrError['txtUserName_temp1'] = 'Username must only have at most 25 characters';
						if (0 == strlen($_param['txtPassword_temp1']))
							$this->arrError['txtPassword_temp1'] = 'Password is a Required field';						
						elseif (0 < strlen($_param['txtPassword_temp1']) && 6 > strlen($_param['txtPassword_temp1']))
							$this->arrError['txtPassword_temp1'] = 'Password must at least be six characters';

						if (0 < strlen($_param['txtUserName_temp1'])){
							// check if username is unique
							$mUserName = $_param['txtUserName_temp1'];
							$isUserNameTaken = $this->file_factory->invokeStaticClass('MembersPanel', 'isUserNameTaken', array($mUserName));

							if ($isUserNameTaken)
								$this->arrError['txtUserName_temp1'] = 'Username <u>' . $mUserName . '</u> is already taken';
						}
					}
				}
			}
		}

		function _viewMembers($_params = array()){
			$this->_defineCommonAttributes($_params);
			
			//prepare filter to determine if there are requests in this group
			$filter = NULL;
			if( 4 > $this->mViewerType ){
				require_once("travellog/views/member_staff/Class.JoinRequest.php");
			}
			if( !($this->mGroup instanceof AdminGroup) && !($this->mGroup instanceof Group) ){
				header("Location: /FileNotFound.php");
				exit;
			}
			if( 3 == $this->mViewerType && $this->mGroup->isParent() ){
				$staffedGroups = Traveler::getStaffedGroups($this->mLoggedUser->getTravelerID(),array($this->mGroup->getGroupID()));
				$staffedIDs = array();
				foreach($staffedGroups AS $sg){
					$staffedIDs[] = $sg->getGroupID();
				}
				unset($sg);
				
				//if logged user is a staff, set the subgroup ids of this staff
				JoinRequest::setStaffedSubgroupIDs($staffedIDs);
			
				$cond2 = new Condition;
				$cond2->setAttributeName("tblRequestList.groupID");
				$cond2->setOperation(FilterOp::$IN);
				$cond2->setValue("(".implode(',', $staffedIDs).")");
				$filter = new FilterCriteria2;
				$filter->addBooleanOp("AND");
				$filter->addCondition($cond2);
			}
			
			//check if there are requests for this group, relative to the logged user
			if( 4 > $this->mViewerType ){
				$jr_array = $this->mGroup->getJoinRequests(NULL,$filter);
				if( 0 < $jr_array[1] && !isset($_GET["mode"]) ){
					JoinRequest::setRequestArray($jr_array);
					$_GET["mode"] = 6;
					$_params["mode"] = 6;
					$this->mMode = $_params["mode"];
					$_params["isRedirect"] = TRUE;
					//ob_end_clean();
					//header("location: http://".$_SERVER['SERVER_NAME']."/members.php?gID=".$this->mGroupID."&mode=6");
					$this->_viewRequests($_params);
					exit;
				}
			}
			
			$this->_prepareDependencies();
			
			$mContent = $this->file_factory->getClass('ViewSearchMember');
			$mContent->setViewerType($this->mViewerType);
			$mContent->setLoggedUser($this->mLoggedUser);
			$mContent->setGroup($this->mGroup);
			$mContent->retrieve();

			// call accordion
			$mAccordion = NULL;
			if( 3 >= $this->mViewerType || $this->mGroup->hasStaff() || $this->mGroup->getCountStaffofSubGroups() ){
				$mAccordion = $this->file_factory->getClass('ViewAccordion');
				$mAccordion->setMode($_params['mode']);
				$mAccordion->setIsLogged($this->mIsLogged);
				$mAccordion->setViewerType($this->mViewerType);
				$mAccordion->setDiscriminator($this->mGroup->getDiscriminator());
				$mAccordion->setGroupId($this->mGroup->getGroupID());
				$mAccordion->setGroup($this->mGroup);
			}
			
			//call history
			/*if (in_array($this->mViewerType, array(1, 2, 3))) {
				$mHistory = $this->file_factory->getClass('ViewHistory');
				$mHistory->setGroupId($this->mGroup->getGroupID());
				$mHistory->setMode($_params['mode']);
				$mHistory->setLoggedUser($this->mLoggedUser);
			}*/
			$this->mTemplate->set("copying_notice_component", new GanetGroupCopyingNoticeComponent($this->mGroup));
			$this->mTemplate->set('group',			$this->mGroup);
			$this->mTemplate->set('mode',			$_params['mode']);
			$this->mTemplate->set('groupId',		$this->mGroup->getGroupID());
			$this->mTemplate->set('isLogged',		$this->mIsLogged);
			$this->mTemplate->set("accordion",		$mAccordion);
			$this->mTemplate->set("main_content",	$mContent);
			//$this->mTemplate->set("history_div",	isset($mHistory) ? $mHistory->render() : null);
			$this->mTemplate->set("subNavigation",	$this->mSubNav);
			$this->mTemplate->set('mainGroupInfo',	$this->mMainGroupInfo);
			//$this->mTemplate->set('showHistory',	3 >= $this->mViewerType ? TRUE : FALSE);
			$this->mTemplate->set('showHistory',	FALSE);

			$this->mTemplate->out('tpl.ViewMembers.php');
		}
		
		function _viewInviteMembers($_params=array()){
			$this->_defineCommonAttributes($_params);
			if( $this->mViewerType > 3 ){
				ob_end_clean();
				header("location: http://".$_SERVER['SERVER_NAME'].$this->mGroup->getFriendlyURL());
				exit;
			}
			if( !($this->mGroup instanceof AdminGroup) && !($this->mGroup instanceof Group) ){
				header("Location: /FileNotFound.php");
				exit;
			}
			
			$mContent = $this->file_factory->getClass('ViewInviteMember');
			$mContent->setLoggedUser($this->mLoggedUser);
			$mContent->setViewerType($this->mViewerType);
			$mContent->setGroup($this->mGroup);
			$mContent->retrieve();
			
			// call accordion
			$mAccordion = $this->file_factory->getClass('ViewAccordion');
			$mAccordion->setMode($_params['mode']);
			$mAccordion->setIsLogged($this->mIsLogged);
			$mAccordion->setViewerType($this->mViewerType);
			$mAccordion->setDiscriminator($this->mGroup->getDiscriminator());
			$mAccordion->setGroupId($this->mGroup->getGroupID());
			$mAccordion->setGroup($this->mGroup);
			
			//call history
			/*if (in_array($this->mViewerType, array(1, 2, 3))) {
				$mHistory = $this->file_factory->getClass('ViewHistory');
				$mHistory->setGroupId($this->mGroup->getGroupID());
				$mHistory->setMode($_params['mode']);
				$mHistory->setLoggedUser($this->mLoggedUser);
			}*/
			
			$this->mTemplate->set('group',			$this->mGroup);
			$this->mTemplate->set('mode',			$_params['mode']);
			$this->mTemplate->set('groupId',		$this->mGroup->getGroupID());
			$this->mTemplate->set('isLogged',		$this->mIsLogged);
			$this->mTemplate->set("accordion",		$mAccordion);
			$this->mTemplate->set("main_content",	$mContent);
			//$this->mTemplate->set("history_div",	isset($mHistory) ? $mHistory->render() : null);
			$this->mTemplate->set("subNavigation",	$this->mSubNav);
			$this->mTemplate->set('mainGroupInfo',	$this->mMainGroupInfo);
			//$this->mTemplate->set('showHistory',	3 >= $this->mViewerType ? TRUE : FALSE);
			$this->mTemplate->set('showHistory',	FALSE);

			$this->mTemplate->out('tpl.ViewMembers.php');
		}
		
		function _viewRequests($_params = array()){
			if( !isset($_params["isRedirect"]) ){
				$this->_defineCommonAttributes($_params);
			}
			if( $this->mViewerType > 3 ){
				ob_end_clean();
				header("location: http://".$_SERVER['SERVER_NAME'].$this->mGroup->getFriendlyURL());
				exit;
			}
			if( !($this->mGroup instanceof AdminGroup) && !($this->mGroup instanceof Group) ){
				header("Location: /FileNotFound.php");
				exit;
			}
			
			$mContent = $this->file_factory->getClass('ViewJoinRequest');
			$mContent->setViewerType($this->mViewerType);
			$mContent->setLoggedUser($this->mLoggedUser);
			$mContent->setGroup($this->mGroup);
			$mContent->retrieve();
			
			// call accordion
			$mAccordion = $this->file_factory->getClass('ViewAccordion');
			$mAccordion->setMode($_params['mode']);
			$mAccordion->setIsLogged($this->mIsLogged);
			$mAccordion->setViewerType($this->mViewerType);
			$mAccordion->setDiscriminator($this->mGroup->getDiscriminator());
			$mAccordion->setGroupId($this->mGroup->getGroupID());
			$mAccordion->setGroup($this->mGroup);
			
			//call history
			/*if (in_array($this->mViewerType, array(1, 2, 3))) {
				$mHistory = $this->file_factory->getClass('ViewHistory');
				$mHistory->setGroupId($this->mGroup->getGroupID());
				$mHistory->setMode($_params['mode']);
				$mHistory->setLoggedUser($this->mLoggedUser);
			}*/
			
			$this->mTemplate->set('group',			$this->mGroup);
			$this->mTemplate->set('mode',			$_params['mode']);
			$this->mTemplate->set('groupId',		$this->mGroup->getGroupID());
			$this->mTemplate->set('isLogged',		$this->mIsLogged);
			$this->mTemplate->set("accordion",		$mAccordion);
			$this->mTemplate->set("main_content",	$mContent);
			//$this->mTemplate->set("history_div",	isset($mHistory) ? $mHistory->render() : null);
			$this->mTemplate->set("subNavigation",	$this->mSubNav);
			$this->mTemplate->set('mainGroupInfo',	$this->mMainGroupInfo);
			//$this->mTemplate->set('showHistory',	3 >= $this->mViewerType ? TRUE : FALSE);
			$this->mTemplate->set('showHistory',	FALSE);

			$this->mTemplate->out('tpl.ViewMembers.php');
		}
		
		function _processInvites($_params=array()){
			$this->_defineCommonAttributes($_params);
			
			if( $this->mViewerType > 3 ){
				ob_end_clean();
				header("location: http://".$_SERVER['SERVER_NAME'].$this->mGroup->getFriendlyURL());
				exit;
			}
			
			if( !($this->mGroup instanceof AdminGroup) && !($this->mGroup instanceof Group) ){
				header("Location: /FileNotFound.php");
				exit;
			}
			
			$this->mInviteMembers = $this->file_factory->getClass('InviteMember');
			$this->mInviteMembers->setLoggedUser($this->mLoggedUser);
			$this->mInviteMembers->setViewerType($this->mViewerType);
			$this->mInviteMembers->setGroup($this->mGroup);
			$this->mInviteMembers->performAction();
		}

		/**
		 *	View staff controller function
		 */
		function _viewStaff($_params = array()){
			$this->_defineCommonAttributes($_params);

			if( !$this->mGroup instanceof Group )
			{
				header("Location: /FileNotFound.php");
				exit;
			}

			// call controller for the information to be shown in the wide panel
			$mContent = $this->file_factory->getClass('ViewStaff');
			$mContent->setGroup($this->mGroup);
			$mContent->setPage($_params['page']);
			$mContent->setIsLogged($this->mIsLogged);
			if ($_params['cID'] != $_params['gID'])
				$mContent->setSubGroupId($_params['cID']);
			$mContent->setLoggedUser($this->mLoggedUser);
			$mContent->setViewerType($this->mViewerType);
			$mContent->retrieve();

			// call accordion
			$mAccordion = NULL;
			if( 3 >= $this->mViewerType || $this->mGroup->hasStaff() || $this->mGroup->getCountStaffofSubGroups() ){
				$mAccordion = $this->file_factory->getClass('ViewAccordion');
				$mAccordion->setMode($_params['mode']);
				$mAccordion->setDiscriminator($this->mGroup->getDiscriminator());
				$mAccordion->setIsLogged($this->mIsLogged);
				$mAccordion->setViewerType($this->mViewerType);
				$mAccordion->setGroup($this->mGroup);
				$mAccordion->setGroupId($this->mGroup->getGroupID());
			}
			//call history
			/*if (in_array($this->mViewerType, array(1, 2, 3))) {
				$mHistory = $this->file_factory->getClass('ViewHistory');
				$mHistory->setGroupId($this->mGroup->getGroupID());
				$mHistory->setMode($_params['mode']);
				$mHistory->setLoggedUser($this->mLoggedUser);
			}*/

			$this->mTemplate->set('group',			$this->mGroup);
			$this->mTemplate->set('mode',			$_params['mode']);
			$this->mTemplate->set('groupId',		$this->mGroup->getGroupID());
			$this->mTemplate->set('isLogged',		$this->mIsLogged);
			$this->mTemplate->set("accordion",		$mAccordion);
			$this->mTemplate->set("main_content",	$mContent);
			//$this->mTemplate->set("history_div",	isset($mHistory) ? $mHistory->render() : null);
			$this->mTemplate->set("subNavigation",	$this->mSubNav);
			$this->mTemplate->set('mainGroupInfo',	$this->mMainGroupInfo);
			//$this->mTemplate->set('showHistory',	3 >= $this->mViewerType ? TRUE : FALSE);
			$this->mTemplate->set('showHistory',	FALSE);

			$this->mTemplate->out('tpl.ViewMembers.php');
		}

		function _viewAssignStaff($_params = array()){
			$this->_defineCommonAttributes($_params);
			
			if( !($this->mGroup instanceof AdminGroup) && !($this->mGroup instanceof Group) ){
				header("Location: /FileNotFound.php");
				exit;
			}
			
			if (Group::ADMIN == $this->mGroup->getDiscriminator()){
				$this->_checkPrivileges();

				// call controller for the information to be shown in the wide panel
				$mContent = $this->file_factory->getClass('AssignStaff');
				$mContent->setType($_params['type']);
				$mContent->setGroup($this->mGroup);
				$mContent->setIsLogged($this->mIsLogged);
				$mContent->setLoggedUser($this->mLoggedUser);
				$mContent->setViewerType($this->mViewerType);
				$mContent->setSubGroupId($_params['cID']);
				$mContent->retrieve();

				// call accordion
				$mAccordion = $this->file_factory->getClass('ViewAccordion');
				$mAccordion->setMode($_params['mode']);
				$mAccordion->setDiscriminator($this->mGroup->getDiscriminator());
				$mAccordion->setIsLogged($this->mIsLogged);
				$mAccordion->setViewerType($this->mViewerType);
				$mAccordion->setGroup($this->mGroup);
				$mAccordion->setGroupId($this->mGroup->getGroupID());

				//call history
				/*if (in_array($this->mViewerType, array(1, 2, 3))) {
					$mHistory = $this->file_factory->getClass('ViewHistory');
					$mHistory->setGroupId($this->mGroup->getGroupID());
					$mHistory->setMode($_params['mode']);
					$mHistory->setLoggedUser($this->mLoggedUser);
				}*/
			
				$this->mTemplate->set('group',			$this->mGroup);
				$this->mTemplate->set('mode',			$_params['mode']);
				$this->mTemplate->set('groupId',		$this->mGroup->getGroupID());
				$this->mTemplate->set('isLogged',		$this->mIsLogged);
				$this->mTemplate->set("accordion",		$mAccordion);
				$this->mTemplate->set("main_content",	$mContent);
				//$this->mTemplate->set("history_div",	isset($mHistory) ? $mHistory->render() : null);
				$this->mTemplate->set("subNavigation",	$this->mSubNav);
				$this->mTemplate->set('mainGroupInfo',	$this->mMainGroupInfo);
				//$this->mTemplate->set('showHistory',	3 >= $this->mViewerType ? TRUE : FALSE);
				$this->mTemplate->set('showHistory',	FALSE);

				$this->mTemplate->out('tpl.ViewMembers.php');
			}
			else
				header("location: members.php?gID=".$this->mGroup->getGroupID());
		}

		function _viewInviteStaff($_params = array()){
			$this->_defineCommonAttributes($_params);
			
			if( !($this->mGroup instanceof AdminGroup) && !($this->mGroup instanceof Group) ){
				header("Location: /FileNotFound.php");
				exit;
			}
			
			if (Group::ADMIN == $this->mGroup->getDiscriminator()){
				$this->_checkPrivileges();
				
				// validate
				$this->_validate($_params);

				$mType = $_params['cmbStaffType'];
				$mSubGroup = $_params['cmbGroup'];

				// call controller for the information to be shown in the wide panel
				if (0 == count($this->arrError)){
					$mEmailAdds = str_replace(' ', '', $_params['email_textarea']);
					$mArrEmails = array_unique(explode(",", $mEmailAdds));

					$mContent = $this->file_factory->getClass('InviteStaff');
					$mContent->setType($mType);
					$mContent->setGroup($this->mGroup);
					$mContent->setIsLogged($this->mIsLogged);
					$mContent->setLoggedUser($this->mLoggedUser);
					$mContent->setViewerType($this->mViewerType);
					$mContent->setSubGroupId($mSubGroup);
					$mContent->setArrEmails($mArrEmails);
					$mContent->retrieve();
				}
				else{
					$mContent = $this->file_factory->getClass('AssignStaff');
					$mContent->setType($mType);
					$mContent->setGroup($this->mGroup);
					$mContent->setIsLogged($this->mIsLogged);
					$mContent->setLoggedUser($this->mLoggedUser);
					$mContent->setViewerType($this->mViewerType);
					$mContent->setSubGroupId($mSubGroup);
					$mContent->setEmails(trim($_params['email_textarea']));
					$mContent->setErrors($this->arrError);
					$mContent->retrieve();
				}

				// call accordion
				$mAccordion = $this->file_factory->getClass('ViewAccordion');
				$mAccordion->setMode($_params['mode']);
				$mAccordion->setDiscriminator($this->mGroup->getDiscriminator());
				$mAccordion->setIsLogged($this->mIsLogged);
				$mAccordion->setViewerType($this->mViewerType);
				$mAccordion->setGroup($this->mGroup);
				$mAccordion->setGroupId($this->mGroup->getGroupID());

				//call history
				/*if (in_array($this->mViewerType, array(1, 2, 3))) {
					$mHistory = $this->file_factory->getClass('ViewHistory');
					$mHistory->setGroupId($this->mGroup->getGroupID());
					$mHistory->setMode($_params['mode']);
					$mHistory->setLoggedUser($this->mLoggedUser);
				}*/

				$this->mTemplate->set('group',			$this->mGroup);
				$this->mTemplate->set('mode',			$_params['mode']);
				$this->mTemplate->set('groupId',		$this->mGroup->getGroupID());
				$this->mTemplate->set('isLogged',		$this->mIsLogged);
				$this->mTemplate->set("accordion",		$mAccordion);
				$this->mTemplate->set("main_content",	$mContent);
				//$this->mTemplate->set("history_div",	isset($mHistory) ? $mHistory->render() : null);
				$this->mTemplate->set("subNavigation",	$this->mSubNav);
				$this->mTemplate->set('mainGroupInfo',	$this->mMainGroupInfo);
				//$this->mTemplate->set('showHistory',	3 >= $this->mViewerType ? TRUE : FALSE);
				$this->mTemplate->set('showHistory',	FALSE);

				$this->mTemplate->out('tpl.ViewMembers.php');
			}
			else
				header("location: members.php?gID=".$this->mGroup->getGroupID());
		}

		function _viewAddStaff($_params = array()){
			$this->_defineCommonAttributes($_params);

			$this->_checkPrivileges();

			// validate
			$this->_validate($_params);

			$successfulAdd = false;
			$mType = (isset($_params['cmbStaffType'])) ? $_params['cmbStaffType'] : 0;
			$mSubGroup = (isset($_params['cmbGroup'])) ? $_params['cmbGroup'] : 0;

			if( !($this->mGroup instanceof AdminGroup) && !($this->mGroup instanceof Group) ){
				header("Location: /FileNotFound.php");
				exit;
			}

			if (0 == count($this->arrError)){
				// add to db
				$this->AddStaff($_params);

				// call add template again
				$mContent = $this->file_factory->getClass('AssignStaff');				
				$mContent->setType($_params['type']);
				$mContent->setGroup($this->mGroup);
				$mContent->setIsLogged($this->mIsLogged);
				$mContent->setLoggedUser($this->mLoggedUser);
				$mContent->setViewerType($this->mViewerType);
				$mContent->setSubGroupId($_params['cID']);
				$mContent->retrieve();

				$successfulAdd = true;

				// call accordion
				$mAccordion = $this->file_factory->getClass('ViewAccordion');
				$mAccordion->setMode($_params['mode']);
				$mAccordion->setDiscriminator($this->mGroup->getDiscriminator());
				$mAccordion->setIsLogged($this->mIsLogged);
				$mAccordion->setViewerType($this->mViewerType);
				$mAccordion->setGroup($this->mGroup);
				$mAccordion->setGroupId($this->mGroup->getGroupID());

				//call history
				/*if (in_array($this->mViewerType, array(1, 2, 3))) {
					$mHistory = $this->file_factory->getClass('ViewHistory');
					$mHistory->setGroupId($this->mGroup->getGroupID());
					$mHistory->setMode(self::ADD_STAFF);
					$mHistory->setLoggedUser($this->mLoggedUser);
				}*/
			}
			else{
				$mContent = $this->file_factory->getClass('InviteStaff');				
				$mContent->setType($mType);
				$mContent->setGroup($this->mGroup);
				$mContent->setIsLogged($this->mIsLogged);
				$mContent->setLoggedUser($this->mLoggedUser);
				$mContent->setViewerType($this->mViewerType);
				$mContent->setSubGroupId($mSubGroup);
				$mContent->setErrors($this->arrError);
				$mContent->retrieveAdd($_params);

				// call accordion
				$mAccordion = $this->file_factory->getClass('ViewAccordion');
				$mAccordion->setMode($_params['mode']);
				$mAccordion->setDiscriminator($this->mGroup->getDiscriminator());
				$mAccordion->setIsLogged($this->mIsLogged);
				$mAccordion->setViewerType($this->mViewerType);
				$mAccordion->setGroup($this->mGroup);
				$mAccordion->setGroupId($this->mGroup->getGroupID());

				//call history
				/*if (in_array($this->mViewerType, array(1, 2, 3))) {
					$mHistory = $this->file_factory->getClass('ViewHistory');
					$mHistory->setGroupId($this->mGroup->getGroupID());
					$mHistory->setMode($_params['mode']);
					$mHistory->setLoggedUser($this->mLoggedUser);
				}*/
			}

			$this->mTemplate->set('group',			$this->mGroup);
			$this->mTemplate->set('mode',			$_params['mode']);
			$this->mTemplate->set('groupId',		$this->mGroup->getGroupID());
			$this->mTemplate->set('isLogged',		$this->mIsLogged);
			$this->mTemplate->set("accordion",		$mAccordion);
			$this->mTemplate->set("main_content",	$mContent);
			//$this->mTemplate->set("history_div",	isset($mHistory) ? $mHistory->render() : null);
			$this->mTemplate->set("subNavigation",	$this->mSubNav);
			$this->mTemplate->set("successfulAdd",	$successfulAdd);
			$this->mTemplate->set('mainGroupInfo',	$this->mMainGroupInfo);
			//$this->mTemplate->set('showHistory',	3 >= $this->mViewerType ? TRUE : FALSE);
			$this->mTemplate->set('showHistory',	FALSE);

			$this->mTemplate->out('tpl.ViewMembers.php');
		}

		function _viewPendingInvites($_params = array()){
			$this->_defineCommonAttributes($_params);
			$this->_checkPrivileges();

			if( !($this->mGroup instanceof AdminGroup) && !($this->mGroup instanceof Group) ){
				header("Location: /FileNotFound.php");
				exit;
			}

			// call controller for the information to be shown in the wide panel
			$mContent = NULL;
			if( 2 == $this->mGroup->getDiscriminator() ){
				$mContent = $this->file_factory->getClass('ViewPendingStaff');
				$mContent->setGroup($this->mGroup);
				$mContent->setLoggedUser($this->mLoggedUser);
				$mContent->setViewerType($this->mViewerType);
				$mContent->retrieve();
			}

			$mPendingMember = $this->file_factory->getClass('ViewPendingMember');
			$mPendingMember->setGroup($this->mGroup);
			$mPendingMember->setViewerType($this->mViewerType);
			$mPendingMember->setLoggedUser($this->mLoggedUser);
			$mPendingMember->retrieve();

			// call accordion
			$mAccordion = $this->file_factory->getClass('ViewAccordion');
			$mAccordion->setMode($_params['mode']);
			$mAccordion->setIsLogged($this->mIsLogged);
			$mAccordion->setViewerType($this->mViewerType);
			$mAccordion->setDiscriminator($this->mGroup->getDiscriminator());
			$mAccordion->setGroup($this->mGroup);
			$mAccordion->setGroupId($this->mGroup->getGroupID());

			//call history
			/*if (in_array($this->mViewerType, array(1, 2, 3))) {
				$mHistory = $this->file_factory->getClass('ViewHistory');
				$mHistory->setGroupId($this->mGroup->getGroupID());
				$mHistory->setMode($_params['mode']);
				$mHistory->setLoggedUser($this->mLoggedUser);
			}*/

			$this->mTemplate->set('group',			$this->mGroup);
			$this->mTemplate->set('mode',			$_params['mode']);
			$this->mTemplate->set('groupId',		$this->mGroup->getGroupID());
			$this->mTemplate->set('isLogged',		$this->mIsLogged);
			$this->mTemplate->set("accordion",		$mAccordion);
			$this->mTemplate->set("main_content",	$mContent);
			$this->mTemplate->set("pending_member",	$mPendingMember);
			//$this->mTemplate->set("history_div",	isset($mHistory) ? $mHistory->render() : null);
			$this->mTemplate->set("subNavigation",	$this->mSubNav);
			$this->mTemplate->set('mainGroupInfo',	$this->mMainGroupInfo);
			//$this->mTemplate->set('showHistory',	3 >= $this->mViewerType ? TRUE : FALSE);
			$this->mTemplate->set('showHistory',	FALSE);

			$this->mTemplate->out('tpl.ViewMembers.php');
		}
		
		function _viewMembersActivityFeed($_params){
			$this->_defineCommonAttributes($_params);
			$this->_checkPrivileges();
			$this->_prepareDependencies();
			
			if( !($this->mGroup instanceof AdminGroup) && !($this->mGroup instanceof Group) ){
				header("Location: /FileNotFound.php");
				exit;
			}
			
			$mContent = $this->file_factory->getClass('ViewMembersActivityFeed');
			
			$feeds = array();
			$indiFeeds = false;
			//$sort = ( 0 == $_params["sortByFilter"] )? "DESC" : "ASC";
			if($_params['tID'] > 0){
				require_once('travellog/model/updates/Class.GroupRecentActivity.php');
				$recentActivity = new GroupRecentActivity($this->mGroup);
				$membersactivityfeeds = $recentActivity->prepareUpdates(array(
					'LOGGED_TRAVELER'	=> $this->mLoggedUser,
					'SORT_ORDER'		=>	'DESC',
					'VIEWER_TYPE'		=>	$this->mViewerType,
					'FEED_OWNER'		=> $_params['tID']
				));
				
				require_once('travellog/model/updates/Class.JournalsFeed.php');
				$journalsfeed = new JournalsFeed($this->mGroup);
				$journalfeeds = $journalsfeed->prepareUpdates(
					array(
						'LOGGED_TRAVELER'	=> $this->mLoggedUser,
						'SORT_ORDER'		=> "DESC",
						'VIEWER_TYPE'		=> 6,
						'FEED_OWNER'		=> $_params['tID'] 
				));
				$feeds = array_merge($membersactivityfeeds,$journalfeeds);
				
				$temp = array();
				
				$updateKeys = array_keys($feeds);
				sort($updateKeys);
				$x = 1;
				foreach($updateKeys as $time){
					if( $x > 10 ){
						break;
					}	
					$temp[$time] = $feeds[$time];
					$x++ ;
				}
				$feeds = $temp;
				$indiFeeds = true;
				
				$traveler = new Traveler($_params['tID']);
				$affiliations = $this->mGroup->getSubGroupsByTravelerID($traveler->getTravelerID(), FALSE);
				
				$mContent->setMember($traveler);
				$mContent->setAffiliations($affiliations);
				$mContent->setViewerType($this->mViewerType);
				$mContent->setFeeds($feeds);
				
			}else{
				require_once('travellog/model/updates/Class.GroupRecentActivity.php');
				$recentActivity = new GroupRecentActivity($this->mGroup);
				$membersactivityfeeds = $recentActivity->prepareUpdates(array(
					'LOGGED_TRAVELER'	=> $this->mLoggedUser,
					'SORT_ORDER'		=>	'DESC',
					'VIEWER_TYPE'		=>	$this->mViewerType 
				));

				$feeds = $membersactivityfeeds;
				$indiFeeds = false;
			}	
			
			$mContent->setGroupId($_params['gID']);
			$mContent->setLoggedUser($this->mLoggedUser);
			$mContent->setFeeds($feeds);
			$mContent->setMode(self::VIEW_MEMBERS_ACTIVITY_FEED);
			$mContent->setShowIndiFeeds($indiFeeds);
			
			// call accordion
			$mAccordion = $this->file_factory->getClass('ViewAccordion');
			$mAccordion->setMode($_params['mode']);
			$mAccordion->setIsLogged($this->mIsLogged);
			$mAccordion->setViewerType($this->mViewerType);
			$mAccordion->setDiscriminator($this->mGroup->getDiscriminator());
			$mAccordion->setGroup($this->mGroup);
			$mAccordion->setGroupId($this->mGroup->getGroupID());

			//call history
			/*if (in_array($this->mViewerType, array(1, 2, 3))) {
				$mHistory = $this->file_factory->getClass('ViewHistory');
				$mHistory->setGroupId($this->mGroup->getGroupID());
				$mHistory->setMode($_params['mode']);
				$mHistory->setLoggedUser($this->mLoggedUser);
			}*/
			
			$this->mTemplate->set('group',			$this->mGroup);
			$this->mTemplate->set('mode',			$_params['mode']);
			$this->mTemplate->set('groupId',		$this->mGroup->getGroupID());
			$this->mTemplate->set('isLogged',		$this->mIsLogged);
			$this->mTemplate->set("accordion",		$mAccordion);
			$this->mTemplate->set("main_content",	$mContent);
			//$this->mTemplate->set("history_div",	isset($mHistory) ? $mHistory->render() : null);
			$this->mTemplate->set("subNavigation",	$this->mSubNav);
			$this->mTemplate->set('mainGroupInfo',	$this->mMainGroupInfo);
			//$this->mTemplate->set('showHistory',	3 >= $this->mViewerType ? TRUE : FALSE);
			$this->mTemplate->set('showHistory',	FALSE);
			
			$this->mTemplate->out('tpl.ViewMembers.php');
		}

		function _viewInviteToDiffGroup($_params = array()){
			$this->_defineCommonAttributes($_params);

			if( !($this->mGroup instanceof AdminGroup) && !($this->mGroup instanceof Group) ){
				header("Location: /FileNotFound.php");
				exit;
			}

			if (Group::ADMIN == $this->mGroup->getDiscriminator()){
				$this->_checkPrivileges();

				$mContent = $this->file_factory->getClass('InviteStaff');
				$mContent->setGroup($this->mGroup);
				$mContent->setIsLogged($this->mIsLogged);
				$mContent->setLoggedUser($this->mLoggedUser);
				$mContent->setViewerType($this->mViewerType);
				$mContent->setArrTravelerId( array($_params['tID']) );
				$mContent->retrieveInvite();

				// call accordion
				$mAccordion = $this->file_factory->getClass('ViewAccordion');
				$mAccordion->setMode($_params['mode']);
				$mAccordion->setIsLogged($this->mIsLogged);
				$mAccordion->setViewerType($this->mViewerType);
				$mAccordion->setDiscriminator($this->mGroup->getDiscriminator());
				$mAccordion->setGroup($this->mGroup);
				$mAccordion->setGroupId($this->mGroup->getGroupID());

				//call history
				/*if (in_array($this->mViewerType, array(1, 2, 3))) {
					$mHistory = $this->file_factory->getClass('ViewHistory');
					$mHistory->setGroupId($this->mGroup->getGroupID());
					$mHistory->setMode($_params['mode']);
					$mHistory->setLoggedUser($this->mLoggedUser);
				}*/

				$this->mTemplate->set('group',			$this->mGroup);
				$this->mTemplate->set('mode',			$_params['mode']);
				$this->mTemplate->set('groupId',		$this->mGroup->getGroupID());
				$this->mTemplate->set('isLogged',		$this->mIsLogged);
				$this->mTemplate->set("accordion",		$mAccordion);
				$this->mTemplate->set("main_content",	$mContent);
				//$this->mTemplate->set("history_div",	isset($mHistory) ? $mHistory->render() : null);
				$this->mTemplate->set("subNavigation",	$this->mSubNav);
				$this->mTemplate->set('mainGroupInfo',	$this->mMainGroupInfo);
				//$this->mTemplate->set('showHistory',	3 >= $this->mViewerType ? TRUE : FALSE);
				$this->mTemplate->set('showHistory',	FALSE);

				$this->mTemplate->out('tpl.ViewMembers.php');
			}
			else
				header("location: members.php?gID=".$this->mGroup->getGroupID());
		}

		function AddStaff($_param = array()){
			if (0 == count($this->arrError)){
				$default = "The administrator of the group " . $this->mGroup->getName() . " has invited you to be a staff of their group.";
				$editedMessage = isset($_param['hdnMessage']) ? trim($_param['hdnMessage']) : $default;
				$arrEmail = isset($_param['hdnEmailAdd']) ? explode(",", trim($_param['hdnEmailAdd'])) : array();
				$arrTravelerId = isset($_param['hdnTravelerId']) ? explode(",", trim($_param['hdnTravelerId'])) : array();

				if (0 < count($arrTravelerId)){
					// loop through members
					foreach ($arrTravelerId as $tID){
						if ((isset($_param['chk_'.$tID]) && 1 < count($arrTravelerId)) || 1 == count($arrTravelerId)){
							if (isset($_param['cmbGroup_'.$tID])){
								$mGroup = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array($_param['cmbGroup_'.$tID]));
								$mGroup = $mGroup[0];

								$mTraveler = $this->file_factory->getClass('Traveler', array($tID));

								// if group is not a parent group
								if (!$mGroup->isParent()){
									$mParent = $mGroup->getParent();

									if ($mParent->isMember($mTraveler)){
										if (!$mGroup->isMember($mTraveler))
											$mGroup->addMember($mTraveler);

										if (!$mGroup->isStaff($tID))
											$mGroup->addStaff($tID);
									}
									else
										$mGroup->addAsPendingStaff($mTraveler);
								}
								else{
									
									//do not allow non-owners to assign staff to parent groups
									//only group owners/advisors have the power to add super staff
									if ( 2 < $this->mViewerType ){
										continue;
									}
									
									$arrGroup = array();

									// check if member of parent group
									if ($mGroup->isMember($mTraveler)){
										// check if is inactive staff of group
										if ($mGroup->isInActiveStaff($tID))
											$arrGroup[] = $mGroup->getGroupID();
										else{
											$mGroup->addStaff($tID);
										}

										// get all subgroups of parent group
										$mSubGroups = $mGroup->getAllSubGroups();

										// check if not staff then assign as staff to each subgroup (also include as member of SG)
										foreach ($mSubGroups as $indSubGroup){
											if (!$indSubGroup->isMember($mTraveler)){
												$indSubGroup->addMember($mTraveler);
											}

											if (!$indSubGroup->isStaff($tID)){
												if ($indSubGroup->isInActiveStaff($tID))
													$arrGroup[] = $indSubGroup->getGroupID();
												else{
													$indSubGroup->addStaff($tID);
												}
											}
										}
										if (0 < count($arrGroup))
											$this->file_factory->invokeStaticClass('MembersPanel', 'assignAsParentStaff', array($arrGroup, $tID));
									}
									else{
										$arrGroup = $mTraveler->getPendingStaffInvitationsToGroup($mGroup->getGroupID());
										if (0 < count($arrGroup)){
											// delete all invitations as regular staff in all subgroups
											foreach($arrGroup as $eachGroup){
												$eachGroup->cancelAsPendingStaff($tID);
											}
										}
										// add as pending super staff
										$mGroup->addAsPendingStaff($mTraveler);
									}
								}

								/****** EMAIL Notification *******/
									require_once 'travellog/model/Class.StaffEmailNotification.php';

									$mEmail = new StaffEmailNotification;
									$mEmail->setGroup($mGroup);
									$mEmail->setTraveler($mTraveler);
									$mEmail->setMessage($editedMessage);
									$mEmail->retrieve();
									$mEmail->Send();
								/****** EMAIL Notification *******/
							}
						}						
					}
				}

				if (0 < count($arrEmail)){
					// loop through non-members
					for ($ctr = 1; $ctr <= count($arrEmail); $ctr++){
						$mFirstname = 'txtFirstName_temp' . $ctr;
						$mLastname = 'txtLastName_temp' . $ctr;
						$mUsername = 'txtUserName_temp' . $ctr;
						$mPassword = 'txtPassword_temp' . $ctr;
						$mEmail = $arrEmail[$ctr-1];
						
						if ((isset($_param['chk_temp'.$ctr]) && 1 < count($arrEmail)) || 1 == count($arrEmail)){
							if (isset($_param['cmbGroup_temp'.$ctr])){
								$mGroup = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array($_param['cmbGroup_temp'.$ctr]));
								$mGroup = $mGroup[0];

								// insert to table, etc
								$MembersPanel = $this->file_factory->getClass('MembersPanel');
								$MembersPanel->setGroup($this->mGroup);
								$MembersPanel->setFirstName(trim($_param[$mFirstname]));
								$MembersPanel->setLastName(trim($_param[$mLastname]));
								$MembersPanel->setEmailAddress(trim($mEmail));
								$MembersPanel->setUserName(trim($_param[$mUsername]));
								$MembersPanel->setPassword(trim($_param[$mPassword]));
								$mTravelerId = $MembersPanel->addNewTraveler();
								$mTraveler = $this->file_factory->getClass('Traveler', array($mTravelerId));

								// if group is not a parent group
								if (!$mGroup->isParent()){
									$mParent = $mGroup->getParent();

									if (!$mParent->isMember($mTraveler))
										$mParent->addMember($mTraveler);

									if (!$mGroup->isMember($mTraveler))
										$mGroup->addMember($mTraveler);

									if (!$mGroup->isStaff($mTravelerId))
										$mGroup->addStaff($mTravelerId);
								}
								else{
									
									//do not allow non-owners to assign staff to parent groups
									//only group owners/advisors have the power to add super staff
									if( $mGroup->getAdministratorID() != $this->mLoggedUser->getTravelerID() ){
										continue;
									}
									
									$arrGroup = array();

									// check if is inactive staff of group
									if ($mGroup->isInActiveStaff($mTravelerId))
										$arrGroup[] = $mGroup->getGroupID();
									else{
										$mGroup->addStaff($mTravelerId);
									}

									// get all subgroups of parent group
									$mSubGroups = $mGroup->getAllSubGroups();

									// check if not staff then assign as staff to each subgroup (also include as member of SG)
									foreach ($mSubGroups as $indSubGroup){
										if (!$indSubGroup->isMember($mTraveler)){
											$indSubGroup->addMember($mTraveler);
										}

										if (!$indSubGroup->isStaff($mTravelerId)){
											if ($indSubGroup->isInActiveStaff($mTravelerId))
												$arrGroup[] = $indSubGroup->getGroupID();
											else{
												$indSubGroup->addStaff($mTravelerId);
											}
										}
									}
									if (0 < count($arrGroup))
										$this->file_factory->invokeStaticClass('MembersPanel', 'assignAsParentStaff', array($arrGroup, $mTravelerId));
								}

								/****** EMAIL Notification *******/
									require_once 'travellog/model/Class.StaffEmailNotification.php';

									$mEmail = new StaffEmailNotification;
									$mEmail->setNewAccount(true);
									$mEmail->setGroup($mGroup);
									$mEmail->setTraveler($mTraveler);
									$mEmail->setMessage($editedMessage);
									$mEmail->retrieve();
									$mEmail->Send();
								/****** EMAIL Notification *******/
							}
						}						
					}
				}
			}
		}
		
		function _searchMembers($_params){
			$this->_defineCommonAttributes($_params);
			$this->mMode = self::VIEW_MEMBERS;
			$this->_prepareDependencies();
			
			if( !($this->mGroup instanceof AdminGroup) && !($this->mGroup instanceof Group) ){
				header("Location: /FileNotFound.php");
				exit;
			}
			
			$mContent = $this->file_factory->getClass('ViewSearchMember');
			$mContent->setViewerType($this->mViewerType);
			$mContent->setLoggedUser($this->mLoggedUser);
			$mContent->setGroup($this->mGroup);
			$mContent->retrieve();

			// call accordion
			$mAccordion = NULL;
			if( 3 >= $this->mViewerType || $this->mGroup->hasStaff() ){
				$mAccordion = $this->file_factory->getClass('ViewAccordion');
				$mAccordion->setMode($_params['mode']);
				$mAccordion->setIsLogged($this->mIsLogged);
				$mAccordion->setViewerType($this->mViewerType);
				$mAccordion->setDiscriminator($this->mGroup->getDiscriminator());
				$mAccordion->setGroupId($this->mGroup->getGroupID());
				$mAccordion->setGroup($this->mGroup);
			}
			
			//call history
			/*if (in_array($this->mViewerType, array(1, 2, 3))) {
				$mHistory = $this->file_factory->getClass('ViewHistory');
				$mHistory->setGroupId($this->mGroup->getGroupID());
				$mHistory->setMode($_params['mode']);
				$mHistory->setLoggedUser($this->mLoggedUser);
			}*/
								
			$this->mTemplate->set('group',			$this->mGroup);
			$this->mTemplate->set('mode',			$_params['mode']);
			$this->mTemplate->set('groupId',		$this->mGroup->getGroupID());
			$this->mTemplate->set('isLogged',		$this->mIsLogged);
			$this->mTemplate->set("accordion",		$mAccordion);
			$this->mTemplate->set("main_content",	$mContent);
			//$this->mTemplate->set("history_div",	isset($mHistory) ? $mHistory->render() : null);
			$this->mTemplate->set("subNavigation",	$this->mSubNav);
			$this->mTemplate->set('mainGroupInfo',	$this->mMainGroupInfo);
			//$this->mTemplate->set('showHistory',	3 >= $this->mViewerType ? TRUE : FALSE);
			$this->mTemplate->set('showHistory',	FALSE);

			$this->mTemplate->out('tpl.ViewMembers.php');
		}
		
	}
?>