<?php

	require_once 'travellog/factory/Class.FileFactory.php';
	require_once 'travellog/controller/Class.IController.php';

	abstract class AbstractHowToGuideController implements iController{
	
		function __construct() {
			$this->file_factory = FileFactory::getInstance();
 		}
 		
 		function performAction() {
 		}

	}
?>