<?php
require_once('travellog/controller/Class.AbstractTravelerController.php');
require_once('travellog/model/Class.TravelerSearchCriteria2.php');
require_once("Class.Criteria2.php");
class LatestTravelerController extends AbstractTravelerController{
	function viewContents(){
		
		$this->viewMain();
		$myobj        = new TravelerSearchCriteria2;
		$c            = new Criteria2();
		$inc_tpl      = new Template;
		$query_string = 'action=lastlogin';
		$c->setOrderBy('latestlogin DESC');
		$c->setLimit  ($this->offset.','.$this->rows); 
		if ($this->IsLogin) $c->mustNotBeEqual('travelerID', $this->travelerID);
		
		$obj_travelers  = $myobj->getAllTravelers($c);
		$obj_paging     = new Paging( $myobj->getTotalRecords(), $this->page, $query_string, $this->rows );
		$obj_iterator   = new ObjectIterator($obj_travelers,$obj_paging,true);
		
		$this->main_tpl->set( 'obj_paging'   , $obj_paging       );
		$this->main_tpl->set( 'obj_iterator' , $obj_iterator     );
		$this->main_tpl->set( 'selected'     , 3                 );
		$this->main_tpl->set( 'travelerID'   , $this->travelerID );
		$this->main_tpl->set( 'isLogin'      , $this->IsLogin    );
		$this->main_tpl->set( 'isAdvisor'    , $this->isAdvisor  );
		
		$this->main_tpl->set( 'mainContent', $this->main_tpl->fetch("travellog/views/tpl.ViewTravelersList.php") );
		$this->main_tpl->out("travellog/views/tpl.Travelers.php");		
	}
}
?>
