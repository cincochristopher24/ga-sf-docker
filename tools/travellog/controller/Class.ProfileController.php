<?php

require_once("Class.Connection.php");
require_once("travellog/model/Class.Country.php");


		
class ProfileController{
		/*
		public static function getCities($countryID){
				
				$conn = new Connection();
				$rs    = new Recordset($conn);
				$sql = "select city,cityID,locID from GoAbroad_Main.tbcity where approved =1 AND countryID='".$countryID."' ORDER BY city ASC";
				$results = $rs->Execute($sql);
				
				$resultassoc = array ();
				
				while ($row = mysql_fetch_assoc($results)	) {
					$resultassoc[] = $row;
				} 				
				return $resultassoc;									
		}
		
		*/

		// edited by: Cheryl Ivy Q. Go   07 June 2007
		public static function getCities($_countryID = 0, $_travID = 0){
			
			$conn 		= new Connection();
			$country 	= new Country($_countryID);

			return $country->getCities($_travID);
		}
		
		
		public static function getDaysInMonth ( $Year, $MonthInYear ){	
				
		   if ( in_array ( $MonthInYear, array ( 1, 3, 5, 7, 8, 10, 12 ) ) )
			   return 31;		
		   if ( in_array ( $MonthInYear, array ( 4, 6, 9, 11 ) ) )
			   return 30;			 
		   if ( $MonthInYear == 2 ){
			   if(is_int($Year)){
			   		return ( checkdate ( 2, 29, $Year ) ) ? 29 : 28;
			   }
		   }		
			  return false;
		}
}


?>