<?php
 
 	require_once("travellog/factory/Class.FileFactory.php");
	require_once("Class.Constants.php");
	require_once("travellog/helper/DisplayHelper.php");
	require_once("travellog/model/Class.Comment.php");
	require_once("travellog/model/Class.PokeType.php");
	require_once("travellog/views/Class.PokesView.php");
	require_once("travellog/model/admin/Class.AdminUserAccount.php");
	
	class ViewShoutOutController {
		
		function __construct(){
			
			$this->file_factory = FileFactory::getInstance();
			
			$this->file_factory->registerClass('SessionManager', array('path' => 'travellog/model/'));
			
			$this->session_manager = $this->file_factory->invokeStaticClass('SessionManager', 'getInstance');
			
			if( !$this->session_manager->getLogIn() || !$this->session_manager->isSiteAdmin()){
				header("location: login.php?failedLogin&redirect=viewShoutOut.php?" . $_SERVER['QUERY_STRING']);
				exit;			 
			}
			if (!AdminUserAccount::validateAdminView($this->session_manager->get('userAccess'), array(
					AdminUserAccount::ACCESS_MODERATOR, AdminUserAccount::ACCESS_SALES))){
						header("location:index.php");
						exit;
			}	
			
			$this->file_factory->registerClass('Traveler', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('RowsLimit', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('MainLayoutView', array('path' => 'travellog/views/'));
			$this->file_factory->registerClass('Template', array('path' => ''));
			$this->file_factory->registerClass('Paging', array('path' => ''));
			
			// views
			$this->file_factory->registerTemplate('LayoutMain');
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
		}
		
		function performAction(){
			return $this->viewShoutOut();
		}
		
		function viewShoutOut(){
			
			$searchOptions = array();
			$rowsPerPage =  isset($_REQUEST['rows']) ? $_REQUEST['rows'] : 10;
			$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
			$group = isset($_REQUEST['group']) ? $_REQUEST['group'] : null;
			$name = isset($_REQUEST['name']) ? $_REQUEST['name'] : null;
			$orderBy = isset($_REQUEST['order_by']) ? $_REQUEST['order_by'] : null;
			$searchOptions = $group ? array_merge(array(
					'group' => $group
				), $searchOptions) : $searchOptions;
			$searchOptions = $name ? array_merge(array(
					'name' => $name
				), $searchOptions) : $searchOptions;
			$searchOptions = $orderBy ? array_merge(array(
					'order_by' => $orderBy
				), $searchOptions) : $searchOptions;
			
			Template::setMainTemplateVar('layoutID', 'main_pages');
			Template::setMainTemplateVar('page_location', 'Admin');
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );   
			
			$unApprovedShoutOuts = Comment::getUnRegisteredUserShoutOut($this->file_factory->getClass('RowsLimit',array($rowsPerPage,($page-1)*$rowsPerPage)), $searchOptions);
			$count = Comment::getUnRegisteredUserShoutOutCount($searchOptions);
			$qrystr = http_build_query(array_merge($searchOptions, array('rows' => $rowsPerPage)));
			
			$vars = array(
				'group'        => $group,
				'name'         => $name,
				'orderBy'      => $orderBy,
				'shoutOutList' => $unApprovedShoutOuts,
				'count'        => $count,
				'paging'       => $this->file_factory->getClass('Paging',array( $count, $page, $qrystr, $rowsPerPage)));
			
			$tpl = $this->file_factory->getClass('Template');
			$tpl->set_path('travellog/views/admin/');
			$tpl->setVars($vars);
			$tpl->out('tpl.ViewUnRegisteredUserShoutOuts.php');
		}
	}
