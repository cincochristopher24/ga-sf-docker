<?php
	require_once('Class.Template.php');
	require_once('Class.Paging.php');
	
	require_once 'travellog/model/Class.Condition.php';
	require_once 'travellog/model/Class.FilterCriteria2.php';
	require_once('travellog/model/Class.Article.php');
	require_once('travellog/model/Class.Group.php');
	
	class AdminArticlesController{
		public $rpp = 10;
		
		public $page = 1;
		public $keywords = null;
		public $searchby = null;
		
		public $articles = array();
		public $pagingComp = null;
		
		const ARTICLE = 'article';
		const ENTRY   = 'entry';
		
		static function sortAE($a, $b){
			$aDate = '0000-00-00 00:00:00';
			$bDate = '0000-00-00 00:00:00';
			if($a instanceof Article)
				$aDate = $a->getLastEdited() == '0000-00-00 00:00:00' ? $a->getLogDate() : $a->getLastEdited();
			if($a instanceof TravelLog)
				$aDate = $a->getLogDate();
			if($a instanceof JournalsArticlesVO)
				$aDate = $a->getDate();
			if($b instanceof Article)
				$bDate = $b->getLastEdited() == '0000-00-00 00:00:00' ? $b->getLogDate() : $b->getLastEdited();
			if($b instanceof TravelLog)
				$bDate = $b->getLogDate();
			if($b instanceof JournalsArticlesVO)
				$bDate = $b->getDate();
			if($aDate == $bDate)
				return 0;
			else
				return ($aDate > $bDate) ? -1 : 1;
		}
		
		function setQueryArray($qryAr){
			$this->page = $qryAr['page'];
			$this->keywords = $qryAr['keywords'];
			$this->searchby = $qryAr['searchby'];
		}
		
		function pullDownArticle($id,$date,$type=null){
			if(is_null($type))
				$type = 'article';
			switch($type){
				case 'article':
					$article = new Article($id);
					$article->pulldown($date);
					break;
				case 'entry':
					$last_year = date('Y-m-d h:i:s', strtotime("-1 year"));
					$newDate = (is_null($date)) ? $last_year : $date;
					
					$entry = new TravelLog($id);
					$entry->updateDateLastEdited($newDate);
					
					$travel = $entry->getTravel();
					$travel->Update($newDate);
					break;
			}
		}
		
		function viewAllArticles($context=null,$data=null){
			$article = new Article;
			$total = null;
			if( strlen($this->keywords) ){
				
				$arrArticles = array();
				$arrEntries  = array();
				
				$cond = new Condition;
				if ( $this->searchby == 3 ){
					$grpFilteredGroups = $this->searchGroup();
					foreach($grpFilteredGroups as $group){
						$this->articleEntryFromGroup($group);
					}
				}
				else{
					if ( $this->searchby == 1 ):
						$cond->setAttributeName("username");
					else:
						$cond->setAttributeName("email");
					endif;
					$cond->setOperation(FilterOp::$LIKE);
					$cond->setValue($this->keywords);
					$filter = new FilterCriteria2();
					$filter->addBooleanOp('AND');
					$filter->addCondition($cond);	
				
					//get all travelers including advisor
					$travelers = Traveler::getAllTravelers($filter, null, 0, true);
					
					for($i=0;$i<count($travelers);$i++){						
						if(!$travelers[$i]->isAdvisor()){
							//get articles of traveler
							$articles = $article->getArticles('author',$travelers[$i]->getTravelerID());
							for($i2=0;$i2<count($articles);$i2++){
								//$this->articles[] = $articles[$i2];
								$this->articles[] = $articles[$i2];
							}
							
							//get journal entries of traveler
							$arrTravels = $travelers[$i]->getTravels();
							for($i3=0;$i3<count($arrTravels);$i3++){
								$arrTrips = $arrTravels[$i3]->getTrips();
								for($i4=0;$i4<count($arrTrips);$i4++){
									$travelLogs = $arrTrips[$i4]->getTravelLogs();
									for($i5=0;$i5<count($travelLogs);$i5++){
										$this->articles[] = $travelLogs[$i5];
									}
								}
							}
							
						}
						else {
							//it's a different story if traveler is advisor
							$groupID = $travelers[$i]->getAdvisorGroup();
							try{
								$group = new AdminGroup($groupID);
								$this->articleEntryFromGroup($group);	
							}
							catch(exception $e){}
						}
					}
					usort($this->articles, array('AdminArticlesController','sortAE'));
				}
			}else{
				$total = Travel::getEntriesAndArticles(null, true);
				$startrow = $this->rpp * ($this->page -1);
				$this->articles = Travel::getEntriesAndArticles(array('START' => $startrow, 'END' => $this->rpp));
			}
			
			$inc_tpl = new Template;
			$inc_tpl->set('keywords', $this->keywords);
			$inc_tpl->set('searchby', $this->searchby);
			$inc_tpl->set('action', 'articles.php');
			$search_template = $inc_tpl->fetch('travellog/views/admin/tpl.FrmTravelerGroupSearch.php');
			
			$this->configurePaging($total);
			$tpl = new Template();
			$tpl->set('articles', $this->articles);
			$tpl->set('paging', $this->pagingComp);
			$tpl->set('search_template', $search_template);
			$tpl->set('rowsperpage',$this->rpp);
			$tpl->set('totalrec',count($this->articles));
			$tpl->out('travellog/views/admin/tpl.ViewArticlesList.php');
		}
		
		function configurePaging($total = null){
			if (is_null($total)){
				$total = count($this->articles);
				$startrow = $this->rpp * ($this->page -1);
				$this->articles = array_slice($this->articles,$startrow,$this->rpp);
			}
			$this->pagingComp = new Paging($total,$this->page,'keywords='.$this->keywords.'&searchby='.$this->searchby,$this->rpp);

			
		}
		
		function searchGroup(){
			$cond1 = new Condition;
			$cond1->setAttributeName("parentID");
			$cond1->setOperation(FilterOp::$EQUAL);
			$cond1->setValue(0);
			
			$cond2 = new Condition;
			$cond2->setAttributeName("name");
			$cond2->setOperation(FilterOp::$LIKE);
			$cond2->setValue($this->keywords);
			
			$filter = new FilterCriteria2();
			$filter->addCondition($cond1);
			$filter->addBooleanOp('AND');
			$filter->addCondition($cond2);
			$filter->addBooleanOp('AND');
			
			$grpFilteredGroups = GROUP::getFilteredGroups($filter);
			return $grpFilteredGroups;
		}
		
		function articleEntryFromGroup($group){
			if($group instanceof AdminGroup){
				$advisor = $group->getAdministratorID();
				foreach($group->getEntriesAndArticles() as $article){
					if($article->getTravelerID() == $advisor)
						$this->articles[] = $article; 
				}
			}
		}
	}