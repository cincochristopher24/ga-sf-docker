<?php	
/***
 * Created on March 5, 09
 *
 */
	require_once 'travellog/model/Class.Condition.php';
	require_once 'travellog/model/Class.FilterCriteria2.php';
	require_once 'travellog/model/Class.SessionManager.php';
	require_once 'travellog/factory/Class.FileFactory.php';
	require_once 'travellog/model/admin/Class.ErrorLogs.php';
	require_once 'travellog/model/Class.RowsLimit.php';
	require_once 'Class.Paging.php';
	require_once 'travellog/model/admin/Class.AdminUserAccount.php';
	
	class ViewErrorLogsController {
		
		private $obj_session	= null;
		private $file_factory	= null;
		private $props			= array();
		
		public function __construct() {
			$this->obj_session = SessionManager::getInstance();
			$this->file_factory	= FileFactory::getInstance();
			$this->file_factory->registerClass('Template', array('path' => ''));
		}
		
		public function performAction(){
			if (!$this->obj_session->isSiteAdmin()) {
				header("location:login.php?failedLogin&redirect=errorlogs.php");
				exit;
			}
			if (!AdminUserAccount::validateAdminView($this->obj_session->get('userAccess'))){
						header("location:index.php");
						exit;
			}
			
			$this->initializeValues();
			
			$this->viewErrorLogs();
			
		}
		
		function initializeValues() {
			$this->props['page'] = (isset($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
			$this->props['action'] = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : "view";
			$this->props['limit'] = new RowsLimit(5,($this->props['page']-1)*5);
			
			$this->props['searchBy'] = (isset($_REQUEST['radSearchErrorLogs'])) ? $_REQUEST['radSearchErrorLogs'] : "";
			$this->props['searchBy'] = ('keyword' === trim($this->props['searchBy'])) ? (isset($_REQUEST['cmbKeyword'])?$_REQUEST['cmbKeyword']:$this->props['searchBy']) : $this->props['searchBy'];
			$this->props['searchBy'] = (isset($_REQUEST['searchBy'])) ? $_REQUEST['searchBy'] : $this->props['searchBy'];
			
			$this->props['keyword'] = $keyword = (isset($_REQUEST['txtKeyword'])) ? $_REQUEST['txtKeyword'] : "";
			$this->props['keyword'] = (isset($_REQUEST['keyword'])) ? trim($_REQUEST['keyword']) : trim($this->props['keyword']);
		}
		
		function getSearchData() {
			$log = new ErrorLogs();
			return array(
				'count'	=> $log->getAllLogsCount($this->props['searchBy'], $this->props['keyword']),
				'qrystr' => "action=search&searchBy=".$this->props['searchBy']."&keyword=".$this->props['keyword'],
				'logs'	=> $log->getLogs($this->props['limit'],$this->props['searchBy'], $this->props['keyword'])
			);
		}
		
		function getViewData() {
			$log = new ErrorLogs();
			return array(
				'count'	=> $log->getAllLogsCount(),
				'qrystr' => "",
				'logs'	=> $log->getLogs($this->props['limit']) 
			);
		}
		
		function viewErrorLogs() {
			switch($this->props['action']) {
				case 'search':
					$data = $this->getSearchData();
					break;
				case 'view':
				default:
					$data = $this->getViewData();
					break;
			}
			$paging = new Paging( $data['count'], $this->props['page'], $data['qrystr'], 5);
			
			$vars = array(
						'search' => $this->viewSearchForm(),
						'logs'	=> $data['logs'],
						'paging' => $paging,
						'count'	=> $data['count']
			);
			$this->showTemplate('tpl.ViewErrorLogs.php', $vars);
		}
		
		function viewSearchForm() {
			$vars = array(
					'searchBy'	=> $this->props['searchBy'],
					'keyword'	=> $this->props['keyword']
			);
			$tpl = $this->file_factory->getClass('Template');
			$tpl->set_path('travellog/views/admin/errorLogs/');
			$tpl->setVars($vars);
			$tpl->setTemplate('tpl.FrmSearchErrorLogs.php');
			return $tpl;
		}
		
		private function showTemplate($tplName, $vars = array(), $method = 'out') {
			$this->file_factory->invokeStaticClass('Template', 'setMainTemplate', array('travellog/views/tpl.LayoutMain.php'));
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			$tpl = $this->file_factory->getClass('Template');
			$tpl->includeDependentJs('js/errorLogs.js');
			$tpl->set_path('travellog/views/admin/errorLogs/');
			$tpl->setVars($vars);
			$tpl->{$method}($tplName);
		}
	}