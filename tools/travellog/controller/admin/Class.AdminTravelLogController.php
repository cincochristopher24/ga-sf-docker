<?php
require_once("travellog/model/Class.LocationFactory.php");
require_once("travellog/model/Class.Travel.php");
require_once("travellog/model/Class.TravelerProfile.php");
require_once("travellog/model/Class.Traveler.php");
require_once("travellog/model/Class.Trip.php");
require_once("travellog/model/Class.TravelLog.php");
require_once('Class.FormHelpers.php');
require_once('Class.Template.php');
require_once('Class.Date.php');
require_once("travellog/model/Class.SubNavigation.php");
require_once('travellog/model/navigator/Class.BackLink.php');
require_once('travellog/model/Class.Map.php');
require_once('travellog/model/Class.City.php');
require_once('travellog/model/Class.Country.php');

class AdminTravelLogController{

	function Update( $formvars ){
		require_once('travellog/model/Class.JournalLog.php');
		require_once('travellog/model/Class.JournalLogType.php');

		$trip        = new Trip  ( $formvars['tripID']   );
		$objTravel   = new Travel( $formvars['travelID'] );
		$locationID  = (intval($formvars['cityID']) > 0)?intval($formvars['cityID']) : intval($formvars['countryID']);
		$arrivaldate = $formvars['arrivaldate'];		
		$tripID      = Trip::TripExists( $formvars['travelID'], $locationID, $arrivaldate );
		$date        = ClassDate::instance(); 
						
		if( $tripID == 0 ){
			$trip->setLocationID($locationID);
			$trip->setArrival($arrivaldate);
			if( count($trip->getTravelLogs()) > 1 ) 
				$trip->Create();
			else{
				$trip->setTripID($formvars['tripID']);
				$trip->Update();
			} 
			$formvars['tripID'] = $trip->getTripID();
		}else{ 
			$trip->setLocationID( $locationID         );
			$trip->setTripID    ( $formvars['tripID'] );
			$trip->setArrival   ( $arrivaldate        );
		}
		
		$objTraveler = new Traveler($objTravel->getTravelerID());
		
		if( $formvars['currentlocation'] )
			$objTraveler->setPresentLocation( $locationID );
		elseif( $objTraveler->getPresentLocation() != NULL ){ 
			if( $objTraveler->getPresentLocation()->getLocationID() == $locationID )
				$objTraveler->setPresentLocation(0);
		}	
		
		$travellog = new TravelLog( $formvars['travellogID'] );
		$travellog->setTripID     ( $formvars['tripID']      );
		$travellog->setTitle      ( $formvars['title']       );
		$travellog->setDescription( $formvars['description'] );
		$travellog->setCallOut    ( $formvars['callout']     );
		$travellog->Update();
		
		$arrdate = explode('-',$formvars['lastupdate']);
		$now     = NULL;
		if( count($arrdate) == 3 ){
			if( checkdate($arrdate[1], $arrdate[2], $arrdate[0]) ){
				$now = date("Y-m-d H:i:s", strtotime($formvars['lastupdate']) );	
			}
		}
		$objTravel->Update($now);
	}

	function Edit( $formvars, $errors = array() ){
		$travellog        = new TravelLog( $formvars['travellogID'] );
		$travel           = new Travel($travellog->getTravelID());
		$trip             = new Trip($travellog->getTripID());
		$inctravellogtpl  = new Template();
		$maintpl          = new Template();
		$date             = ClassDate::instance();
		$factory          = LocationFactory::instance();
		$location         = $factory->create($trip->getLocationID());
		$countries        = Country::getCountryList();
		$map              = new Map();
		
		$formvars['travelerID'] = $travel->getTravelerID();
		
		if ( $formvars['locID'] == 0){	
			$formvars['locID']              = $location->getCountry()->getLocationID();
			$param                          = 'countryID='.$formvars['locID'].'&cityID='.$trip->getLocationID();
			$formvars['current_locationID'] = $trip->getLocationID();
		}else
			$param = 'countryID='.$formvars['locID'].'&cityID='.$formvars['current_locationID'];
			
		$formvars['fh'] = FormHelpers::CreateCollectionOption($countries,'getLocationID','getName',$formvars['locID']);
		
						
		$subNavigation    = new SubNavigation();
		
		$obj_backlink     = BackLink::instance();
		$obj_backlink->Create();
		
		if( !count($errors) ){
			$formvars['description'] = $travellog->getDescription();
			$formvars['title']       = $travellog->getTitle();
			$formvars['callout']     = $travellog->getCallOut();
			$formvars['lastupdate']  = date( 'Y-m-d', strtotime($travel->getLastUpdated()) );
		}
		
		$obj_current_location        = new Traveler( $formvars['travelerID'] );
		$location                    = $obj_current_location->getPresentLocation();
		$formvars['currentlocation'] = 0;
		if( $location != NULL ){
			if( $location->getLocationID() == $formvars['current_locationID'] ) 
				$formvars['currentlocation'] = 1;
		} 
		
		$formvars['calendar'] = FormHelpers::CreateCalendar($trip->getArrival());
		$formvars['isAdmin']         = 1;
		$formvars['travelID']        = $travellog->getTravelID();
		$formvars['tripID']          = $travellog->getTripID();
		$formvars['errors']          = $errors;
		$formvars['isAdministrator'] = 0;
		$formvars['isAdvisor']       = 0;
		$formvars['obj_backlink']    = $obj_backlink;
		$formvars['journal_title']   = $travellog->getTravel()->getTitle();
		$formvars['mode']            = 'Edit';
		$formvars['action']          = "http://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']."?action=update";
		
		$inctravellogtpl->set("arr_vars" , $formvars);
		
		
		
		$maintpl->set_vars
		(array(
			'title'         => 'Add Travel Log',
			'map'			=> $map,
			'form'          => $inctravellogtpl->fetch('travellog/views/tpl.FrmTravelLog.php'),
			'subNavigation' => $subNavigation
		));
			
				
		$code = <<<BOF
<script type="text/javascript"> 
//<![CDATA[
	var manager = new TravellogManager(); 
	window.onload = function(){ 
		initmb();
		manager.useLoadingMessage();
		manager.getSelectedCity('$param');
	}
//]]>
</script>
BOF;
				
		Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
		Template::includeDependent($map->getJsToInclude());
		Template::includeDependentJs("/min/f=js/prototype.js", array("include_here" => true));
		Template::includeDependentJs("/js/addnewcity.js");
		Template::includeDependentJs("/js/moo1.ajax.js");
		Template::includeDependentJs("/js/travellogManager.js");
		Template::includeDependentJs("/js/utils/Class.Request.js");
		Template::includeDependentJs("/js/modaldbox.js");
		//Template::includeDependentCss("/css/modaldbox.css"); File does not exist. Is this being used anyway? Please fix!
		Template::includeDependentCss("/min/f=css/basic.css");
		Template::includeDependent($code);
		$maintpl->out('travellog/views/tpl.TravelLog.php');
	}	

}	
?>
