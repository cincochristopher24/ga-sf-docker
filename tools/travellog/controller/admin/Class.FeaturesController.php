<?
	/**
	 * <b>Features Controller</b> class.
	 * @author Czarisse Daphne P. Dolina
	 * @version 1.0 PHP5
	 * @see http://www.GoAbroad.com/
	 * @copyright 2006-2007
	*/

	require_once 'travellog/model/Class.Travel.php';
	require_once 'travellog/model/Class.TravelerProfile.php';
	require_once 'travellog/model/Class.Condition.php';
	require_once 'travellog/model/Class.FilterCriteria2.php';
	require_once 'travellog/model/Class.Featured.php';
	require_once 'travellog/model/admin/Class.FeaturedSection.php';
	require_once 'Class.FormHelpers.php';
	require_once 'Class.Template.php';
	require_once 'Class.ObjectIterator.php';
	require_once 'Class.Paging.php';

	class FeaturesController{
	
		private static $searchby = '';
		private static $keywords = '';
		private static $sectionID = 0;
		private static $viewtype  = 'entry';
		private static $viewSuspend = false;
	
		private static $grpType 	= 'viewlist';
		private static $grpDiscrim 	= 'viewlist';
		private static $grpName 	= NULL;
	
			/**
			 *************  TRAVEL FUNCTIONS *************
			 */
		 
		public static function ViewAllTravels( $_sortby)
		{
			
			$travels = array();
			
			if( strlen(self::$keywords) ):
				
				$cond = new Condition;
				if ( self::$searchby == 1 ):
					$cond->setAttributeName("username");
				else:
					$cond->setAttributeName("email");
				endif;
				$cond->setOperation(FilterOp::$LIKE);
				$cond->setValue(self::$keywords);
				$filter = new FilterCriteria2();
				$filter->addBooleanOp('AND');
				$filter->addCondition($cond);	
			
				
				$travelers = Traveler::getAllTravelers($filter);
				
				for($i=0;$i<count($travelers);$i++){
					$arrTravels = $travelers[$i]->getTravels();	
					for($i2=0;$i2<count($arrTravels);$i2++){
						$travels[] = $arrTravels[$i2];
					}		
				}
				
			else:
				
				$cond = new Condition();
				$cond->setAttributeName($_sortby);
				$cond->setOperation(FilterOp::$ORDER_BY_DESC);
				
				
				$filter = new FilterCriteria2();
				$filter->addCondition($cond);
				
				$travels = Travel::getFilteredTravels($filter);
			
			
			endif;	
			
			
			
			$travSection	=	new FeaturedSection(self::$sectionID);
			$currFeat 		=	$travSection->getFeaturedItems();
			
			$qrystr = '';
			if( strlen(self::$sectionID))
				$qrystr .= 'sectionID=' . self::$sectionID ;
			
			( isset($_GET['page']) )? $page = $_GET['page'] : $page =1;
			
			$paging       = new Paging(count($travels),$page,$qrystr , 20);
	 		$iterator     = new ObjectIterator($travels,$paging);
			
			$inc_tpl = new Template;
			$inc_tpl->set('keywords', self::$keywords);
			$inc_tpl->set('searchby', self::$searchby);
			$inc_tpl->set('action', 'journals.php');
			$search_template = $inc_tpl->fetch('travellog/views/admin/tpl.FrmTravelerSearch.php');
			
			$maintpl  = new Template();
			$maintpl->set_vars
			( array(
					'title'   => 'Choose Journals to Feature',
					'section'   => $travSection,
					'paging'      => $paging,
					'iterator'    => $iterator,
					'currFeat' => $currFeat,
					'search_template' => $search_template,
					'obj_travels' => $travels
			));
			
			$maintpl->out("travellog/views/admin/tpl.TravelsAdmin.php");
			
		}
		
		
		/**
		 *************  TRAVELER FUNCTIONS *************
		 */
		 
		public static function ViewAllTravelers()
		{	error_reporting(E_ALL);
			/****
			*	Last Edited By: Cheryl Go	24 March 2009
			*	Purpose: Optimized Page
			***/

			$mQueryString = '';
			$mFilterCriteria2 = null;

			( isset($_GET['page']) ) ? $mPage = $_GET['page'] : $mPage = 1;

			if( strlen(self::$keywords)):
				$mCondition = new Condition;
				if ( self::$searchby == 1 ):
					$mCondition->setAttributeName("username");
				else:
					$mCondition->setAttributeName("email");
				endif;
				$mCondition->setOperation(FilterOp::$LIKE);
				$mCondition->setValue(self::$keywords);

				$mFilterCriteria2 = new FilterCriteria2();
				$mFilterCriteria2->addBooleanOp('AND');
				$mFilterCriteria2->addCondition($mCondition);	
			endif;

			$mOffset = ($mPage-1) * 20;
			$mRowsLimit = new RowsLimit(20, $mOffset);
			$mArrayTravelers = Traveler::getAllTravelers($mFilterCriteria2, $mRowsLimit, 0, true, self::$viewSuspend);
			$totalTravelers = Traveler::$statvar;

			$travelerSection	=	new FeaturedSection(FeaturedSection::$TRAVELER);
			$currFeat 			=	$travelerSection->getFeaturedItems();

			if( strlen(self::$searchby))
				$mQueryString .= 'searchby=' . self::$searchby ;
			if( strlen(self::$keywords) )
				$mQueryString .= '&amp;keywords=' . self::$keywords ;

			$mPaging	=  new Paging( $totalTravelers, $mPage, $mQueryString.'&amp;type=all', 20 );
			//$paging    = new Paging( count($travelers),$page, $qrystr. '&amp;type=all',20 );
			//$iterator  = new ObjectIterator($travelers,$paging);

			$inc_tpl = new Template;
			$inc_tpl->set('keywords', self::$keywords);
			$inc_tpl->set('searchby', self::$searchby);
			$inc_tpl->set('action', 'travelers.php');
			$inc_tpl->set('viewSuspend', self::$viewSuspend);
			$search_template = $inc_tpl->fetch('travellog/views/admin/tpl.FrmTravelerSearch.php');

			$maintpl  = new Template();
			$maintpl->set_vars
			( array(
					'title'   => 'Choose Travelers to Feature',
					'section'   => $travelerSection,
					'paging'      => $mPaging,
					'obj_travelers' => $mArrayTravelers,
					'currFeat' => $currFeat,
					'totalrec'	=> $totalTravelers,
					'rowsperpage'		=> 20,
					'search_template' => $search_template,
					'travelers_link' => '<a href="travelers.php?type=all">All Travelers</a>',
					'newest_traveler_link' => '<a href="travelers.php?type=new">Newest Travelers</a>',
					
			));
			$maintpl->out("travellog/views/admin/tpl.TravelersAdmin.php");
		}
		
		
		public static function viewNewestTravelers()
		{
			/***
			*	Last Edited By: Cheryl Go	24 March 2009
			*	Purpose: Optimized display performance
			****/

			( isset($_GET['page']) ) ? $mPage = $_GET['page'] : $mPage = 1;

			$mOffset = ($mPage-1) * 20;
			$mRowsLimit = new RowsLimit(20, $mOffset);
			$mArrayTravelers = Traveler::getNewestMembers($mRowsLimit);
			$totalTravelers = Traveler::$statvar;
			
			$travelerSection	=	new FeaturedSection(FeaturedSection::$TRAVELER);
			$currFeat 			=	$travelerSection->getFeaturedItems();

			$mPaging	=  new Paging( $totalTravelers, $mPage, 'type=new', 20 );

			$inc_tpl = new Template;
			$inc_tpl->set('keywords', self::$keywords);
			$inc_tpl->set('searchby', self::$searchby);
			$inc_tpl->set('action', 'travelers.php');
			$search_template = $inc_tpl->fetch('travellog/views/admin/tpl.FrmTravelerSearch.php');

			$maintpl  = new Template();
			$maintpl->set_vars
			( array(
					'title'   => 'Choose Travelers to Feature',
					'section'   => $travelerSection,
					'paging'      => $mPaging,
					'obj_travelers' => $mArrayTravelers,
					'currFeat' => $currFeat,
					'totalrec'	=> $totalTravelers,
					'rowsperpage'		=> 20,
					'search_template' => $search_template,
					'travelers_link' => '<a href="travelers.php?type=all">All Travelers</a>',
					'newest_traveler_link' => '<a href="travelers.php?type=new">Newest Travelers</a>'					
			));
			$maintpl->out("travellog/views/admin/tpl.TravelersAdmin.php");
		}
		
		
		public static function viewSpecialTravelers(){
			
			$specialsections = FeaturedSection::getSpecialSections();
			
			$currFeat = array();
			
			foreach($specialsections as $each){
				$specialSection	=	new FeaturedSection($each->getSectionID());
				$currFeat[$each->getSectionID()]  = $specialSection->getFeaturedItems();
			}
			
			if (self::$sectionID == 6)
				$travelers = Traveler::getMostTraveled();
			elseif (self::$sectionID == 7)
				$travelers = Traveler::getMostChronicles();
			elseif(self::$sectionID == 8)
				$travelers = Traveler::getMostPhotographs();
			else{
				$travelers = Traveler::getMostTraveled();
				self::$sectionID = 6;
			}
			$currSection = new FeaturedSection(self::$sectionID);
			
			( isset($_GET['page']) )? $page = $_GET['page'] : $page =1;
			
			$paging     = new Paging( count($travelers),$page,'type=special&sectionID=' . self::$sectionID, 20 );			
			$iterator   = new ObjectIterator($travelers,$paging);
			
			$inc_tpl = new Template;
			$inc_tpl->set('keywords', self::$keywords);
			$inc_tpl->set('searchby', self::$searchby);
			$inc_tpl->set('action', 'travelers.php');
			$search_template = $inc_tpl->fetch('travellog/views/admin/tpl.FrmTravelerSearch.php');
			
			$maintpl  = new Template();
			$maintpl->set_vars
			( array(
					'title'   => 'Select Travelers to be Featured',
					'paging'      => $paging,
					'iterator'    => $iterator,
					'obj_travelers' => $travelers,
					'currFeat' => $currFeat,
					'currSection' => $currSection,
					'specialsections' => $specialsections,
					'search_template' => $search_template,
					'mosttraveled_link' => '<a href="travelers.php?type=special&sectionID=6">Most Traveled</a>',
					'chronicler_link' => '<a href="travelers.php?type=special&sectionID=7">The Chronicler</a>',
					'photographer_link' => '<a href="travelers.php?type=special&sectionID=8">The Photographer</a>'					
			));
			
			
			$maintpl->out("travellog/views/admin/tpl.SpecialTravelersAdmin.php");

		}
		
		public static function viewTravelersStatusUpdates(){
			$filter = NULL;
			$qrystr = '';
			$rows = 50;
			
			if( strlen(self::$keywords)):
				$cond = new Condition;
				if ( self::$searchby == 1 ):
					$cond->setAttributeName("username");
				else:
					$cond->setAttributeName("email");
				endif;
				$cond->setOperation(FilterOp::$LIKE);
				$cond->setValue(self::$keywords);
				$filter = new FilterCriteria2();
				$filter->addBooleanOp('AND');
				$filter->addCondition($cond);	
			endif;
			
			( isset($_GET['page']) )? $page = $_GET['page'] : $page =1;
			
			$offset = $rows * ($page -1);			
			
			$rowslimit = new RowsLimit($rows,$offset);
			$travelers = Traveler::getAllTravelers($filter,$rowslimit);
			
			//$travelerSection	=	new FeaturedSection(FeaturedSection::$TRAVELER);
			//$currFeat 			=	$travelerSection->getFeaturedItems();
						
			if( strlen(self::$searchby))
				$qrystr .= 'searchby=' . self::$searchby ;
			if( strlen(self::$keywords) )
				$qrystr .= '&amp;keywords=' . self::$keywords ;
			if( strlen($qrystr) )
				$qrystr .= "&amp;";

			$paging    = new Paging( Traveler::$statvar,$page, $qrystr. 'type=status_updates',$rows );
			$iterator  = new ObjectIterator($travelers,$paging);
			
			$inc_tpl = new Template;
			$inc_tpl->set('keywords', self::$keywords);
			$inc_tpl->set('searchby', self::$searchby);
			$inc_tpl->set('action', 'travelers.php?type=status_updates');
			$inc_tpl->set('type', 'status_updates');
			$inc_tpl->set('showType', true);
			$search_template = $inc_tpl->fetch('travellog/views/admin/tpl.FrmTravelerSearch.php');
			
			$maintpl  = new Template();
			$maintpl->set_vars
			( array(
					'title'   => 'Choose Travelers to Feature',
					'paging'      => $paging,
					'iterator'    => $iterator,
					'obj_travelers' => $travelers,
					'search_template' => $search_template,
					'travelers_link' => '<a href="travelers.php?type=all">All Travelers</a>',
					'newest_traveler_link' => '<a href="travelers.php?type=new">Newest Travelers</a>',
					
			));
			
			
			
			$maintpl->out("travellog/views/admin/tpl.TravelersAdminStatusUpdate.php");
		}
		
		
		public static function setSearchBy($_searchby)
		{
			self::$searchby = $_searchby;
		}
		
		public static function setKeywords($_keywords)
		{
			self::$keywords = $_keywords;
		}
		
		public static function setSectionID($_sectionID)
		{
			self::$sectionID = $_sectionID;
		}
		
		public static function setViewType($_viewtype)
		{
			self::$viewtype = $_viewtype;
		}
		
		public static function setViewSuspend($_viewSuspend)
		{
			self::$viewSuspend = $_viewSuspend;
		}
	
	
	
		/**
		 *************  GROUP FUNCTIONS *************
		 */
		 
		public static function searchGroups()
		{
			$cond = new Condition;
			$cond->setAttributeName("parentID");
			$cond->setOperation(FilterOp::$EQUAL);
			$cond->setValue(0);
			
			$filter = new FilterCriteria2();
			$filter->addCondition($cond);
			$filter->addBooleanOp('AND');
			
			$cond2 = new Condition;
			$cond2->setAttributeName("name");
			$cond2->setOperation(FilterOp::$LIKE);
			$cond2->setValue(self::$grpName);
			
			$filter->addCondition($cond2);
			$filter->addBooleanOp('AND');
				
			$cond3 = new Condition;
			$cond3->setAttributeName("inverserank");
			$cond3->setOperation(FilterOp::$ORDER_BY_DESC);
			
			$order = new FilterCriteria2();
			$order->addCondition($cond3);

			$grpFilteredGroups = GROUP::getFilteredGroups($filter, null, $order);
			
			return $grpFilteredGroups; 
		}
		public static function viewGroups()
		{
			$cond = new Condition;
			$cond->setAttributeName("parentID");
			$cond->setOperation(FilterOp::$EQUAL);
			$cond->setValue(0);
			
			$filter = new FilterCriteria2();
			$filter->addCondition($cond);
			$filter->addBooleanOp('AND');
				
			$cond2 = new Condition;
			$cond2->setAttributeName("inverserank");
			$cond2->setOperation(FilterOp::$ORDER_BY_DESC);
			
			$order = new FilterCriteria2();
			$order->addCondition($cond2);

			$grpFilteredGroups = GROUP::getFilteredGroups($filter, null, $order);

			self::setGroupTools($grpFilteredGroups);		
		
		}
	
	
		public static function viewByGroupType()
		{
			
			$ordercond = new Condition;
			$ordercond->setAttributeName("inverserank");
			$ordercond->setOperation(FilterOp::$ORDER_BY_DESC);
			
			$order = new FilterCriteria2();
			$order->addCondition($ordercond);
				
			if ('viewlist' != self::$grpType)
				$grpFilteredGroups = GROUP::getGroupsByCategory(self::$grpType, null, $order);
			else{
				$cond = new Condition;
				$cond->setAttributeName("parentID");
				$cond->setOperation(FilterOp::$EQUAL);
				$cond->setValue(0);
				
				$filter = new FilterCriteria2();
				$filter->addCondition($cond);
				$filter->addBooleanOp('AND');
			
				$grpFilteredGroups = GROUP::getFilteredGroups($filter, null, $order);
			}
			
			if (isset ($_GET['cmbGrpType']))
				$qrystr = 'cmbGrpType=' . $_GET['cmbGrpType'];
			else
				$qrystr = '';
				
			self::setGroupTools($grpFilteredGroups, $qrystr);		
		
		}
	
		public static function viewByGroupDiscrim()
		{
			
			switch (self::$grpDiscrim){ 
				case "fun":
					
					//view all fun or view all admin
					$cond = new Condition;
					$cond->setAttributeName("discriminator");
					$cond->setOperation(FilterOp::$EQUAL);
					$cond->setValue(GROUP::FUN);
					
					$filter = new FilterCriteria2();
					$filter->addCondition($cond);
					$filter->addBooleanOp('AND');
					
					 //view both sort by inverserank desc 
					$ordercond = new Condition;
					$ordercond->setAttributeName("inverserank");
					$ordercond->setOperation(FilterOp::$ORDER_BY_DESC);
					
					$order = new FilterCriteria2();
					$order->addCondition($ordercond);
					
					$grpFilteredGroups = GROUP::getFilteredGroups($filter, null, $order);
					
					break;
					
				case "admin":
				
					//view all fun or view all admin
					$cond = new Condition;
					$cond->setAttributeName("discriminator");
					$cond->setOperation(FilterOp::$EQUAL);
					$cond->setValue(GROUP::ADMIN);
					
					$cond2 = new Condition;
					$cond2->setAttributeName("parentID");
					$cond2->setOperation(FilterOp::$EQUAL);
					$cond2->setValue(0);
					
					$filter = new FilterCriteria2();
					$filter->addCondition($cond);
					$filter->addBooleanOp('AND');
					$filter->addCondition($cond2);
					$filter->addBooleanOp('AND');
					
					 //view both sort by inverserank desc 
					$ordercond = new Condition;
					$ordercond->setAttributeName("inverserank");
					$ordercond->setOperation(FilterOp::$ORDER_BY_DESC);
					
					$order = new FilterCriteria2();
					$order->addCondition($ordercond);
					
					$grpFilteredGroups = GROUP::getFilteredGroups($filter, null, $order);
					
					break;
					
				case "viewlist":
			
					//view both sort by inverserank desc :: DEFAULT
					$cond = new Condition;
					$cond->setAttributeName("parentID");
					$cond->setOperation(FilterOp::$EQUAL);
					$cond->setValue(0);
					
					$filter = new FilterCriteria2();
					$filter->addCondition($cond);
					$filter->addBooleanOp('AND');
					
					$ordercond = new Condition;
					$ordercond->setAttributeName("inverserank");
					$ordercond->setOperation(FilterOp::$ORDER_BY_DESC);
					
					$order = new FilterCriteria2();
					$order->addCondition($ordercond);
					
					$grpFilteredGroups = GROUP::getFilteredGroups($filter, null, $order);
					
					break;
				
				case "suspend":
			
					$grpFilteredGroups = GROUP::getSuspendedGroups();
					
					break;
						
				default:
				
					//view both sort by inverserank desc :: DEFAULT
					$cond = new Condition;
					$cond->setAttributeName("parentID");
					$cond->setOperation(FilterOp::$EQUAL);
					$cond->setValue(0);
					
					$filter = new FilterCriteria2();
					$filter->addCondition($cond);
					$filter->addBooleanOp('AND');
					
					$ordercond = new Condition;
					$ordercond->setAttributeName("inverserank");
					$ordercond->setOperation(FilterOp::$ORDER_BY_DESC);
					
					$order = new FilterCriteria2();
					$order->addCondition($ordercond);
					
					$grpFilteredGroups = GROUP::getFilteredGroups($filter, null, $order);
					
					break;
			}
			
			
			
			if (isset ($_GET['cmbGrpDiscrim']))
				$qrystr = 'cmbGrpDiscrim=' . $_GET['cmbGrpDiscrim'];
			else
				$qrystr = '';
				
				
			self::setGroupTools($grpFilteredGroups, $qrystr);		
		
		}
		
		
		public static function viewBySearchGroup()
		{
			
			//search group by name
			$cond = new Condition;
			$cond->setAttributeName("name");
			$cond->setOperation(FilterOp::$LIKE);
			$cond->setValue(self::$grpName);
			
			$filter = new FilterCriteria2();
			$filter->addCondition($cond);
			$filter->addBooleanOp('AND');
			
			
			// sort by inverserank of group desc 
			$ordercond = new Condition;
			$ordercond->setAttributeName("inverserank");
			$ordercond->setOperation(FilterOp::$ORDER_BY_DESC);
			
			$order = new FilterCriteria2();
			$order->addCondition($ordercond);
				
			if (0 == strlen(self::$grpName)){			// if search key is empty string, since all groups are displayed, 
														// make sure only parent groups are displayed
				$cond2 = new Condition;
				$cond2->setAttributeName("parentID");
				$cond2->setOperation(FilterOp::$EQUAL);
				$cond2->setValue(0);
				
				$filter->addCondition($cond2);
				$filter->addBooleanOp('AND');	
				
				$grpFilteredGroups = GROUP::getFilteredGroups($filter, null, $order, null);						
			}
			
			else {
				
				// first, get parent groups (non-sub groups)			
				$cond2 = new Condition;
				$cond2->setAttributeName("parentID");
				$cond2->setOperation(FilterOp::$EQUAL);
				$cond2->setValue(0);
				
				$filter->addCondition($cond2);
				$filter->addBooleanOp('AND');
				
				$grpFilteredGroups1 = GROUP::getFilteredGroups($filter, null, $order, null);
				
				// next, get only sub groups; results will be merged later to avoid duplicates
				$cond2->setOperation(FilterOp::$NOT_EQUAL);
				
				$filter = new FilterCriteria2();
				$filter->addCondition($cond);
				$filter->addBooleanOp('AND');
				$filter->addCondition($cond2);
				$filter->addBooleanOp('AND');
				
				$condgrp = new Condition;
				$condgrp->setAttributeName("parentID");
				$condgrp->setOperation(FilterOp::$GROUP_BY);
				
				$groupsql = new FilterCriteria2();
				$groupsql->addCondition($condgrp);
				
				$grpFilteredGroups2 = GROUP::getFilteredGroups($filter, null, $order, $groupsql);			
				
				// then merge both results to remove duplicates
				if (is_array($grpFilteredGroups1) && is_array($grpFilteredGroups2)){
					$grpFilteredGroups = array_merge($grpFilteredGroups1, $grpFilteredGroups2);
					//$grpFilteredGroups = array_unique($grpFilteredGroups);
				}
				elseif (is_array($grpFilteredGroups1))
					$grpFilteredGroups = $grpFilteredGroups1;
				elseif (is_array($grpFilteredGroups2))
					$grpFilteredGroups = $grpFilteredGroups2;
				
			}
			
			if (isset ($_GET['txtGrpName']))
				$qrystr = 'txtGrpName=' . $_GET['txtGrpName'];
			else
				$qrystr = '';
				
			self::setGroupTools($grpFilteredGroups, $qrystr);			
			
		}
		
		public static function setGroupTools($grpFilteredGroups, $qrystr=''){
			
			$groupSection	=	new FeaturedSection(FeaturedSection::$GROUP);
			$currFeat 		=	$groupSection->getFeaturedItems();
			
			( isset($_GET['page']) )? $page = $_GET['page'] : $page =1;
			
			$paging    =  new Paging( count($grpFilteredGroups), $page, $qrystr , 10 );
			$iterator  =  new ObjectIterator( $grpFilteredGroups,$paging    );
			
			$formHelpers = new FormHelpers();
			
			$inc_tpl = new Template;
			$inc_tpl->set("grpType",self::$grpType);
			$inc_tpl->set("grpDiscrim",self::$grpDiscrim);
			$inc_tpl->set("grpName",self::$grpName);	
			$inc_tpl->set("grpCategories",$formHelpers->CreateOptions(GROUPCATEGORY::getGroupCategoriesList(), self::$grpType ) );	
			$filter_template = $inc_tpl->fetch('travellog/views/admin/tpl.FrmGroupFilters.php');
			$search_template = $inc_tpl->fetch('travellog/views/admin/tpl.FrmSearchGrpName.php');
			
			$maintpl  = new Template();
			$maintpl->set_vars
			( array(
					'title'			=> 'Choose Groups to Feature',
					'section'		=> $groupSection,
					'page'			=> $page,
					'paging'		=> $paging,
					'iterator'		=> $iterator,
					'currFeat'		=> $currFeat,
					'totalrec'		=> count($grpFilteredGroups),
					'filter_template'	=> $filter_template,
					'search_template'	=> $search_template
			));
			
			$maintpl->out("travellog/views/admin/tpl.GroupsAdmin.php");
			
		}
		
		public static function setGroupType($grpType)	{
			self::$grpType = $grpType;
		}
		
		public static function setGroupDiscrim($grpDiscrim) {
			self::$grpDiscrim = $grpDiscrim;
		}
		
		public static function setGroupName($grpName) {
			self::$grpName = $grpName;
		}
	
		
		/**
		 *************  PHOTO FUNCTIONS *************
		 */		 
		
		public static function getTravelLogs()
		{
			$cond = new Condition;
			$cond->setAttributeName("logdate");
			$cond->setOperation(FilterOp::$ORDER_BY_DESC);
			
			$order = new FilterCriteria2();
			$order->addCondition($cond);
		
			return TravelLog::getFilteredTravelLogs($order);
			 
		} 

		/****** Last Edited By: Cheryl Ivy Q. Go	22 September 2008 ******/
		public static function viewPhotos(){
			$mQueryString		= '';
			$mTravelers			= array();
			$mTravelLogs		= array();
			$mArrayPhotos		= array();
			$mFilteredGroups	= array();
			$mFilterCriteria2	= null;

			( isset($_GET['page']) ) ? $mPage = $_GET['page'] : $mPage = 1;

			if (self::$viewtype == 'entry' || self::$viewtype == 'profile'){
				if ( strlen(self::$keywords) ){
					$mCondition = new Condition();
					if (self::$searchby == 1)
						$mCondition->setAttributeName("username");
					else
						$mCondition->setAttributeName("email");

					$mCondition->setOperation(FilterOp::$LIKE);
					$mCondition->setValue(trim(self::$keywords));

					$mFilterCriteria2 = new FilterCriteria2();
					$mFilterCriteria2->addBooleanOp('AND');
					$mFilterCriteria2->addCondition($mCondition);
				}
				if (self::$viewtype == 'entry'){
					if( strlen(self::$keywords) ){
						$mTravelers = Traveler::getAllTravelers($mFilterCriteria2);
						if (0 < count($mTravelers)){
							foreach($mTravelers as $eachTraveler){
								$arrTravels = $eachTraveler->getTravels();
								foreach($arrTravels as $eachTravel){
									$arrTrips = $eachTravel->getTrips();
									foreach($arrTrips as $eachTrip){
										$mTravelLogs = $eachTrip->getTravelLogs();
										foreach($mTravelLogs as $eachTravelLog){
											$eachTravelLogPhoto = $eachTravelLog->getPhotos();
											if (NULL != $eachTravelLogPhoto)
												$mArrayPhotos = array_merge($mArrayPhotos, $eachTravelLogPhoto);
										}
									}
								}
							}
						}
						$totalPhotos = count($mArrayPhotos);
					}
					else{
						$mOffset = ($mPage-1) * 20;
						$mRowsLimit = new RowsLimit(20, $mOffset);
						$mArrayPhotos = TravelLog::getAdminFeaturedTravelLogPhotos($mRowsLimit);
						$totalPhotos = TravelLog::getTotalRec();
					}
				}
				elseif (self::$viewtype == 'profile'){
					$mOffset = ($mPage-1) * 20;
					$mRowsLimit = new RowsLimit(20, $mOffset);
					$mArrayPhotos = Traveler::getAdminFeaturedTravelerPhotos($mFilterCriteria2, $mRowsLimit);
					$totalPhotos = Traveler::$statvar;
				}
			}
			elseif (self::$viewtype == 'group'){
				if ( strlen(self::$keywords) ){
					$mCondition = new Condition();
					$mCondition->setAttributeName("name");
					$mCondition->setOperation(FilterOp::$LIKE);
					$mCondition->setValue(trim(self::$keywords));

					$mFilterCriteria2 = new FilterCriteria2();
					$mFilterCriteria2->addBooleanOp('AND');
					$mFilterCriteria2->addCondition($mCondition);
				}

				// sort by inverserank of group desc 
				$mOrderCondition = new Condition;
				$mOrderCondition->setAttributeName("inverserank");
				$mOrderCondition->setOperation(FilterOp::$ORDER_BY_DESC);

				$mOrder = new FilterCriteria2();
				$mOrder->addCondition($mOrderCondition);

				$mOffset = ($mPage-1) * 20;
				$mRowsLimit = new RowsLimit(20, $mOffset);
				$mArrayPhotos = Group::getAdminFeaturedGroupPhotos($mFilterCriteria2, $mRowsLimit);
				$totalPhotos = Group::$statvar;
			}

			$mPhotoSection	=	new FeaturedSection(FeaturedSection::$PHOTO);
			$mCurrFeat 		=	$mPhotoSection->getFeaturedItems();

			if( strlen(self::$viewtype) )
				$mQueryString .= 'viewtype=' . self::$viewtype ;				
			if( strlen(self::$searchby))
				$mQueryString .= '&amp;searchby=' . self::$searchby ;
			if( strlen(self::$keywords) )
				$mQueryString .= '&amp;keywords=' . self::$keywords ;

			$mPaging	=  new Paging( $totalPhotos, $mPage, $mQueryString, 20 , false);

			$mTemplate = new Template;
			$mTemplate->set('viewtype', self::$viewtype);
			$mTemplate->set('keywords', self::$keywords);
			$mTemplate->set('searchby', self::$searchby);
			$search_template = $mTemplate->fetch('travellog/views/admin/tpl.FrmPhotoSearch.php');
			
			$maintpl  = new Template();
			$maintpl->set_vars(
				array(
					'title'		=> 'Choose Photos to Feature',
					'paging'	=> $mPaging,
					'section'	=> $mPhotoSection,
					'viewtype'	=> self::$viewtype,
					'currFeat'	=> $mCurrFeat,
					'totalrec'	=> $totalPhotos,
					'photos'	=> $mArrayPhotos,
					'rowsperpage'		=> 20,
					'search_template'	=> $search_template,
			));

			$maintpl->out("travellog/views/admin/tpl.PhotosAdmin.php");			
		} 
		
		/**
		 *	Most recent entry functions
		 *	Last edited by:	miss cig		September 28, 2009
		 */
		public static function viewJournalEntries($date = null, $emptydate = false, $includeAdvisor = false, RowsLimit $rowsLimit)
		{
			$arrRecent = array();
			
			$mPage	 = isset($_GET['page']) ? $_GET['page'] : 1;
			$mRows	 = $rowsLimit->getRows();
			$mOffset = $mRows * ( $mPage - 1 );

			// if no date is specified
			if ( is_null($date) )
			{
				if( strlen(self::$keywords) )
				{
					// get all entries by specified keyword, retain the old way of getting entries

					// search by group name
					if ( 3 == self::$searchby )
					{
						self::setGroupName(self::$keywords);
						$groups = self::searchGroups();
						
						foreach ($groups as $group)
						{
							$advisor = $group->getAdministratorID();
							foreach($group->getTravels() as $travel)
							{
								foreach($travel->getTrips() as $trip)
								{
									foreach($trip->getTravelLogs() as $entry)
									{
										if($entry->getTravelerID() == $advisor)
										{
											$arrRecent[] = $entry;
										}
									}
								}
							}
						}
					}

					// search by traveler
					else
					{
						$cond = new Condition;
						// search by username
						if ( self::$searchby == 1 )
						{
							$cond->setAttributeName("username");
						}
						// search by email
						else
						{
							$cond->setAttributeName("email");
						}

						$cond->setOperation(FilterOp::$LIKE);
						$cond->setValue(self::$keywords);
						$filter = new FilterCriteria2();
						$filter->addBooleanOp('AND');
						$filter->addCondition($cond);	
					
						if ( $includeAdvisor )
						{
							$travelers = Traveler::getAllTravelers( $filter, NULL, 0, true );
						}
						else
						{
							$travelers = Traveler::getAllTravelers( $filter );
						}
						
						for ( $i=0;$i<count($travelers);$i++ )
						{
							if( !$travelers[$i]->isAdvisor() )
							{
								$arrTravels = $travelers[$i]->getTravels();
								for ( $i2=0;$i2<count($arrTravels);$i2++ )
								{
									$arrTrips = $arrTravels[$i2]->getTrips();
									for ( $i3=0;$i3<count($arrTrips);$i3++ )
									{
										$travelLogs = $arrTrips[$i3]->getTravelLogs();
										for ( $i4=0;$i4<count($travelLogs);$i4++ )
										{
											$arrRecent[] = 	$travelLogs[$i4];	
										}
									}
								}
							}
							else
							{
								$groupID = $travelers[$i]->getAdvisorGroup();
								try
								{
									$group = new AdminGroup($groupID);
									$advisor = $group->getAdministratorID();
									foreach ( $group->getTravels() as $travel )
									{
										foreach ( $travel->getTrips() as $trip )
										{
											foreach ( $trip->getTravelLogs() as $entry )
											{
												if ( $entry->getTravelerID() == $advisor )
												{
													$arrRecent[] = $entry;
												}
											}
										}
									}
								}
								catch(exception $e){}
							}
						}
					}

					// remove unpublished journal entry in $arrRecent  (K. Gordo 06/18/08)
					$tarray = array();
					foreach ( $arrRecent as $je )
					{
						if ( $je->getPublish() )
						{
							$tarray[] = $je;
						}
					}
					$totalEntries = count( $tarray );
					// use array_slice instead of the ObjectIterator model
					$arrRecent = array_slice( $tarray, $mOffset, $mRows );
				}
				elseif ( $emptydate )
				{
					// add rowslimit and use newly created function for faster query
					//$arrRecent =  TravelLog::getEmptyDatesJournalEntries();
					$arrRecent =  TravelLog::getPublishedEmptyDatesJournalEntries( $rowsLimit );
					$totalEntries = TravelLog::$totalEntries;
				}
				else
				{
					// add rowslimit and use newly created function for faster query
					//$arrRecent = TravelLog::getRecentJournalEntries();
					$arrRecent = TravelLog::getPublishedRecentJournalEntries( $rowsLimit );
					$totalEntries = TravelLog::$totalEntries;
				}
				$label = 'Choose Journal Entries to Feature';
			}
			else
			{
				// get entries starting with date
				// add rowslimit and use newly created function for faster query
				$arrRecent = TravelLog::getPublishedRecentJournalEntries( $rowsLimit, $date );
				$totalEntries = TravelLog::$totalEntries;
			
				if ( 0 == $totalEntries )
				{
					// if no recent entries found, get all entries
					$arrRecent = TravelLog::getPublishedRecentJournalEntries( $rowsLimit );
					$totalEntries = TravelLog::$totalEntries;
					$label = 'No entries were added/updated today. Choose from all journal entries.';
				}
				else
				{
					$label = 'Choose from journal entries added/updated today.';
				}
			}
			
			$entrySection	= new FeaturedSection(FeaturedSection::$JOURNAL_ENTRY);
			$currFeat 		= $entrySection->getFeaturedItems();
			/*
			// remove unpublished journal entry in $arrRecent  (K. Gordo 06/18/08)
			$tarray = array();
			foreach($arrRecent as $je){
				if ($je->getPublish())
					$tarray[] = $je;
			}
			$arrRecent = $tarray;
			
			( isset($_GET['page']) )? $page = $_GET['page'] : $page =1;
			
			$paging    =  new Paging(count($arrRecent), $page, 'keywords='.self::$keywords.'&searchby='.self::$searchby, 20);
			$iterator  =  new ObjectIterator( $arrRecent,$paging    );
			
			$inc_tpl = new Template;
			$inc_tpl->set('keywords', self::$keywords);
			$inc_tpl->set('searchby', self::$searchby);
			$inc_tpl->set('action', 'entries.php');
			$search_template = $inc_tpl->fetch('travellog/views/admin/tpl.FrmTravelerGroupSearch.php');
			
			$maintpl  = new Template();
			$maintpl->set_vars
			( array(
					'label'   => $label,
					'section'   => $entrySection,
					'currFeat'   => $currFeat,
					'search_template'   => $search_template,
					'totalrec' 	  => count($arrRecent),
					'paging'      => $paging,
					'iterator'    => $iterator
			));
			*/

			$mPaging = new Paging( $totalEntries, $mPage, 'keywords='.self::$keywords.'&searchby='.self::$searchby, $mRows, false );

			$inc_tpl = new Template;
			$inc_tpl->set('keywords', self::$keywords);
			$inc_tpl->set('searchby', self::$searchby);
			$inc_tpl->set('action', 'entries.php');
			$search_template = $inc_tpl->fetch('travellog/views/admin/tpl.FrmTravelerGroupSearch.php');
			
			$maintpl  = new Template();
			$maintpl->set_vars
			( array(
					'label'				=> $label,
					'section'			=> $entrySection,
					'currFeat'			=> $currFeat,
					'search_template'   => $search_template,
					'totalrec' 			=> $totalEntries,
					'paging'			=> $mPaging,
					//'iterator'			=> $iterator,
					'recentEntries'		=> $arrRecent
			));

			$entryViewType = (isset($_GET['type'])) ? $_GET['type'] : 'all' ;
			
			if ('recent'== $entryViewType){
				$maintpl->set('entries_link' , '<a href="entries.php">All Entries</a> | <a href="entries.php?type=emptydate">Entries w/ Invalid Dates</a>');
				$maintpl->set('showLabel' , 'SHOWING ALL RECENT ENTRIES');
			}
			else if ('emptydate'== $entryViewType){
				$maintpl->set('entries_link' , '<a href="entries.php">All Entries</a> | <a href="entries.php?type=recent">Recent Entries</a>');
				$maintpl->set('showLabel' , 'SHOWING ALL ENTRIES WITH INVALID DATES');
			}
			else if ('all' == $entryViewType){
				$maintpl->set('entries_link' , '<a href="entries.php?type=recent">Recent Entries</a> | <a href="entries.php?type=emptydate">Entries w/ Invalid Dates</a>');
				$maintpl->set('showLabel' , 'SHOWING ALL ENTRIES');
			}	
			
			$maintpl->set('entryViewType' , $entryViewType);
			$maintpl->out("travellog/views/admin/tpl.RecentEntriesAdmin.php");			
		}				 
	}