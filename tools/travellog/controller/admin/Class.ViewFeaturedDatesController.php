<?php
	/*********************************************************/
	/**  	@author Cheryl Ivy Q. Go						**/
	/**  	Created on 03 October 2008					**/
	/**  	ViewFeaturedDatesControllerController			**/
	/*********************************************************/

	require_once 'travellog/model/Class.Condition.php';
	require_once 'travellog/model/Class.FilterCriteria2.php';
	require_once 'travellog/model/Class.SessionManager.php';
	require_once 'travellog/factory/Class.FileFactory.php';
	require_once 'travellog/model/admin/Class.FeaturedSection.php';
	require_once 'travellog/model/admin/Class.AdminUserAccount.php';

	class ViewFeaturedDatesController{
		private $mPage		= 1;
		private $mType		= 0;
		private $mGenId		= 0;
		private $mSectionId	= 0;
		private $mDate	= '';
		private $mSession	= null;
		private $mFactory	= null;
		private $mTemplate	= null;

		function ViewFeaturedDatescontroller(){
			$this->mSession = SessionManager::getInstance();
			$this->mFactory = FileFactory::getInstance();

			$this->mFactory->registerClass('Template' , array ( 'path' => '' ));
			$this->mFactory->registerClass('SessionManager',	array('path'=> 'travellog/model/'));
			$this->mFactory->registerClass('FilterCriteria2',	array('path'=> 'travellog/model/'));
			$this->mFactory->registerClass('FileFactory',		array('path'=> 'travellog/factory/'));
			$this->mFactory->registerClass('FeaturedSection',	array('path'=> 'travellog/model/admin/'));

			$this->mFactory->registerTemplate('LayoutMain');
			$this->mFactory->setPath('CSS', '/css/');
			$this->mFactory->setPath('HEADER_FOOTER', 'travellog/views/');

			$this->performAction();
		}

		function performAction(){
			if (!$this->mSession->isSiteAdmin()){
				header("location:login.php?failedLogin&redirect=viewFeaturedDates.php?type=2");
				exit;
			}
			if (!AdminUserAccount::validateAdminView($obj_session->get('userAccess'), array(
					AdminUserAccount::ACCESS_SALES))){
						header("location:index.php");
						exit;
			}

			$vars = array_merge($_POST, $_GET);
			$this->mTemplate = $this->mFactory->getClass('Template');
			$this->mTemplate->set_path("travellog/views/admin/");

			$this->mType		= $vars['type'];
			$this->mPage		= (isset($vars['page'])) ? $vars['page'] : 1;
			$this->mGenId		= (isset($vars['genID'])) ? $vars['genID'] : 0;
			$this->mDate		= (isset($vars['date'])) ? $vars['date'] : date("Y-m-d");
			$this->mSectionId	= (isset($vars['sectionID'])) ? $vars['sectionID'] : 0;
		}

		function display(){
			switch($this->mType){
				case 1:
					$this->displayLogsPerItem();
					break;
				case 2:
					$this->displayLogsPerDate();
					break;
			}
		}

		function displayLogsPerItem(){
			$mFeaturedDates = array();
			switch($this->mSectionId){
				case FeaturedSection::$JOURNAL_ENTRY:
					$mContext = new TravelLog();
					$mContext->setTravelLogID($this->mGenId);
					$mFeaturedDates = $mContext->getDatesEntryIsFeatured();
					break;
				case FeaturedSection::$PHOTO:
					$mContext = new Photo(null);
					$mContext->setPhotoID($this->mGenId);
					$mFeaturedDates = $mContext->getDatesPhotoIsFeatured();
					break;
				case FeaturedSection::$TRAVELER:
					$mContext = new Traveler();
					$mContext->setTravelerID($this->mGenId);
					$mFeaturedDates = $mContext->getDatesTravelerIsFeatured();
					break;
				default:
					$mContext = new Group();
					$mContext->setGroupID($this->mGenId);
					$mFeaturedDates = $mContext->getDatesGroupIsFeatured();
					break;
			}

			$this->mTemplate->set_vars(
				array(
					'type'				=> $this->mType,
					'sectionID'			=> $this->mSectionId,
					'featuredDates'		=> $mFeaturedDates,
					'title'				=> 'Featured Travelers, Photos, Journal Entries and Groups',
				)
			);

			$this->mTemplate->out('tpl.ViewFeaturedDates.php');
		}

		function displayLogsPerDate(){
			$mFilter = null;
			$mFeatures = array();
			$d = new GaDateTime();

			$mFeaturedOnDate = FeaturedSection::getFeaturedOnDate($this->mSectionId, $this->mDate);
			$totalFeatured = count($mFeaturedOnDate);

			foreach($mFeaturedOnDate as $eachFeatured){
				$mTemp = array();
				$mGenId	= $eachFeatured['genID'];
				$mSection = $eachFeatured['sectionID'];

				if (FeaturedSection::$PHOTO == $mSection){						
					try{
						$mContext = FeaturedSection::instantiatePhotoFeature($mGenId);
						if (!strcasecmp(get_class($mContext), "Exception"))
							$mContext = $mGenId;
					}
					catch(exception $e){
						$mContext = $mGenId;
					}
				}
				else{
					$mFeature = new FeaturedSection($mSection);
					try{
						$mContext = $mFeature->instantiateItem($mGenId);
						if (!strcasecmp(get_class($mContext), "Exception"))
							$mContext = $mGenId;
					}
					catch(exception $e){
						$mContext = $mGenId;
					}
				}
				$mTemp['sectionID'] = $mSection;
				$mTemp['feature'] = $mContext;
				$mFeatures[] = $mTemp;
			}

			$this->mTemplate->set_vars(
				array(
					'date'			=> $this->mDate,
					'type'			=> $this->mType,
					'featured'		=> $mFeatures,
					'sectionID'		=> $this->mSectionId,
					'totalFeatured'	=> $totalFeatured
				)
			);

			$this->mTemplate->out('tpl.ViewAllFeatured.php');
		}

		public static function displayViewLink($_genId = 0, $_sectionId = 0){
			if (0 < $_sectionId){
				$mCondition = new Condition();
				$mCondition->setAttributeName('_NEW_sectionID');
				$mCondition->setOperation(FilterOp::$EQUAL);
				$mCondition->setValue($_sectionId);

				$mFilter = new FilterCriteria2();
				$mFilter->addBooleanOp('AND');
				$mFilter->addCondition($mCondition);
			}
			if (0 < $_genId){
				$mCondition = new Condition();
				$mCondition->setAttributeName('_NEW_genID');
				$mCondition->setOperation(FilterOp::$EQUAL);
				$mCondition->setValue($_genId);

				if (!$mFilter)				
					$mFilter = new FilterCriteria2();
				$mFilter->addBooleanOp('AND');
				$mFilter->addCondition($mCondition);
			}
			$mFeaturedDates = FeaturedSection::getUniqueDates($mFilter);

			if (1 < count($mFeaturedDates))
				return true;
			else
				return false;
		}
	}
?>