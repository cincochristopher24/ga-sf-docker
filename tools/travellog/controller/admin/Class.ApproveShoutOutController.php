<?php
/***
 * Created on Dec 16, 08
 *
 * * @author 
 * Purpose: 
 */
 
 	require_once("travellog/factory/Class.FileFactory.php");
	require_once("Class.Constants.php");
	require_once("travellog/helper/DisplayHelper.php");
	require_once("travellog/model/Class.Comment.php");
	require_once("travellog/model/Class.PokeType.php");
	require_once("travellog/views/Class.PokesView.php");
	require_once("travellog/model/admin/Class.AdminUserAccount.php");
	
	class ApproveShoutOutController {
		public static $instance = NULL;
		
		static function getInstance(){
			if( NULL == self::$instance )
				self::$instance = new ApproveShoutOutController;
			return self::$instance;	 
		}
		
		function __construct(){
			
			$this->file_factory = FileFactory::getInstance();
			
			$this->file_factory->registerClass('SessionManager', array('path' => 'travellog/model/'));
			//require_once("travellog/domains/" . $_SERVER['SERVER_NAME'] . ".php");
			
			$this->session_manager = $this->file_factory->invokeStaticClass('SessionManager', 'getInstance');
			
			if( !$this->session_manager->getLogIn() || !$this->session_manager->isSiteAdmin()){
				header("location: login.php?failedLogin&redirect=approveShoutOut.php?" . $_SERVER['QUERY_STRING']);
				exit;			 
			}
			if (!AdminUserAccount::validateAdminView($this->session_manager->get('userAccess'), array(
					AdminUserAccount::ACCESS_MODERATOR))){
						header("location:index.php");
						exit;
			}	
			
			$this->file_factory->registerClass('Traveler', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('RowsLimit', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('MainLayoutView', array('path' => 'travellog/views/'));
			$this->file_factory->registerClass('Template', array('path' => ''));
			$this->file_factory->registerClass('Paging', array('path' => ''));
			
			// views
			$this->file_factory->registerTemplate('LayoutMain');
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
		}
		
		function performAction(){
			if (isset($_POST['action'])) {
				if("send"==$_POST['action'])
					return $this->sendShoutOut();
				else if ("delete" == $_POST['action'])
					return $this->deleteShoutOut();
				else
					return $this->viewShoutOut();
			}
			else 
				return $this->viewShoutOut();
		}
		
		function viewShoutOut(){
			
			$rowsPerPage =  (isset($_REQUEST['rows']))? $_REQUEST['rows'] : 10;
			$page = (isset($_REQUEST['page']))? $_REQUEST['page'] : 1;
			
			Template::setMainTemplateVar('layoutID', 'main_pages');
			Template::setMainTemplateVar('page_location', 'Admin');
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );   
			
			$unApprovedShoutOuts = Comment::getAllUnapprovedShoutOut($this->file_factory->getClass('RowsLimit',array($rowsPerPage,($page-1)*$rowsPerPage)));
			$qrystr = "action=view&rows=" . $rowsPerPage;
			$count = Comment::getAllUnapprovedShoutOutCount();
			$vars = array(
				'shoutOutList' => $unApprovedShoutOuts,
				'count' => $count,
				'paging' => $this->file_factory->getClass('Paging',array( $count, $page, $qrystr, $rowsPerPage)));
			
			$tpl = $this->file_factory->getClass('Template');
			$tpl->set_path('travellog/views/admin/');
			$tpl->setVars($vars);
			$tpl->out('tpl.ViewUnapprovedShoutOuts.php');
		}

		function deleteShoutOut(){
			$shoutOuts = $_POST['chkCommentID'];
			foreach( $shoutOuts as $commentID ){
				$shoutOut = new Comment;
				$shoutOut->setCommentID($commentID);
				$shoutOut->getContext()->removeComment($shoutOut);
				$shoutOut->delete();
			}
			
			header("location: http://".$_SERVER['SERVER_NAME']."/admin/approveShoutOut.php");
		}
		
		function sendShoutOut(){
			$shoutOuts = $_POST['chkCommentID'];
			
			$approvedShoutouts = array();
			
			foreach( $shoutOuts as $commentID ){
				$shoutOut = new Comment($commentID);
				if ($shoutOut->getContext() instanceof Photo)
					$shoutOut->setCommentType(CommentType::$PHOTO);
				elseif ($shoutOut->getContext() instanceof TravelLog)
					$shoutOut->setCommentType(CommentType::$TRAVELLOG);
				elseif ($shoutOut->getContext() instanceof Article)
					$shoutOut->setCommentType(CommentType::$ARTICLE);
				elseif ($shoutOut->getContext() instanceof TravelerProfile)
					$shoutOut->setCommentType(CommentType::$PROFILE);
				
				$shoutOut->Approve();
				
				$approvedShoutouts[] = $shoutOut;
			}
			
			foreach($approvedShoutouts AS $_shoutOut){
				require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
				RealTimeNotifier::getInstantNotification(RealTimeNotifier::RECEIVED_GREETING, 
					array(
						'COMMENT' => $_shoutOut,
						'FOR_ADMIN' => false
					))->send();
			}
			
			// flush cache
			if( count($approvedShoutouts) ){
				require_once('Cache/ganetMemcache.php');
				$c = new ganetMemcache();
				$c->flush();
			}
			header("location: http://".$_SERVER['SERVER_NAME']."/admin/approveShoutOut.php");
		}
	}
 
?>