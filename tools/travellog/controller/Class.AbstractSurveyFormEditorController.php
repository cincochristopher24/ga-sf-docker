<?php
	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/factory/Class.FileFactory.php");
	require_once('JSON.php');
	
	abstract class AbstractSurveyFormEditorController
	{
		protected $file_factory = NULL;
		protected $obj_session  = NULL;
		
		public function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/views'));
			$this->obj_session  = SessionManager::getInstance();
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			$this->file_factory->registerClass('ToolMan', array('path'=>''));
			$this->file_factory->registerClass('AdminGroup');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('Template',array('path'=>''));
			$this->file_factory->registerClass('SurveyForm', array('path'=>'travellog/model/formeditor/'));
			
			/*****************************************************
			 * added by neri: 11-06-08
			 * registers the class used by the profile component 
			 *****************************************************/
			 
			$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
		}
		
		public function performAction(){
			//$travelerID = (isset($_SESSION) && array_key_exists('travelerID',$_SESSION) && $_SESSION['travelerID'] !=0 ? $_SESSION['travelerID']:0 );
			$travelerID = $this->obj_session->get('travelerID');
			if(!$travelerID){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/login.php'));
			}
			$oTraveler = $this->file_factory->getClass('Traveler',array($travelerID));
			
			$tplID = (array_key_exists('tplID',$_GET) && is_numeric($_GET['tplID']) ) ? $_GET['tplID'] : 0;
  			$frmID = (array_key_exists('frmID',$_GET) && is_numeric($_GET['frmID']) ) ? $_GET['frmID'] : 0;
  			
			if(!array_key_exists('gID',$_GET)){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			$oGroupFactory = $this->file_factory->invokeStaticClass('GroupFactory','instance');
			try{
				$groups = $oGroupFactory->create(array($_GET['gID']));
				$oGroup = count($groups) ? $groups[0] : null;
			}
			catch(exception $e){
				$oGroup = null;
			}
			if(is_null($oGroup) || $oGroup->getDiscriminator() != GROUP::ADMIN){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			if($oGroup->getAdministratorID() != $oTraveler->getTravelerID() && !$oGroup->isSuperStaff($oTraveler->getTravelerID())){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}

  			if(0!=$frmID){
				if(!$this->file_factory->invokeStaticClass('SurveyForm','isExists',array($frmID))){
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
				}
			}
			$frm = $this->file_factory->getClass('SurveyForm',array($frmID));
			
			//check if the form is owned by the group
			if(0!=$frmID && $frm->getGroupID() != $_GET['gID']){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			
			if(0!=$tplID){
				//check if the formID which is the tplID exists
				if(!$this->file_factory->invokeStaticClass('SurveyForm','isExists',array($tplID))){
		  			$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
		  		}
		  		//check if the form is owned by the group
		  		$tpl = $this->file_factory->getClass('SurveyForm',array($tplID));
		  		if($tpl->getGroupID() != $_GET['gID']){  			  			
		  			$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
		  		}
		  		//mold the form to the image of the template form
		  		$tpl->mold($frm);
			}
  			
		  	$json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
		  	//get the json data of the form to be edited, blank if it is a new form
		  	$jsonForm = $json->encode($frm->getData());
		  	
		  	//get the groups of the main group
		  	$gFactory =  $this->file_factory->invokeStaticClass('GroupFactory','instance');
			try{
				$groups = $oGroupFactory->create(array($_GET['gID']));
				$oGroup = count($groups) ? $groups[0] : null;
			}
			catch(exception $e){				
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}  	

		  	$grps = $oGroup->getSubGroups();
			$jsonGroups = array();	
			$jsonGroups[] = array('groupID'=>$oGroup->getGroupID(),'groupName'=>$oGroup->getName());
			if(is_array($grps)){
				foreach($grps as $iGroup){
					$jsonGroups[] = array('groupID'=>$iGroup->getGroupID(),'groupName'=>$iGroup->getName());
				}
			}
			
			$jsonGrpStr = $json->encode($jsonGroups);		
			$subNavigation = $this->file_factory->getClass('SubNavigation');
			$subNavigation->setContext('GROUP');	
			$subNavigation->setContextID($oGroup->getGroupID());
			$subNavigation->setGroupType('ADMIN_GROUP');
			$subNavigation->setLinkToHighlight('SURVEY_CENTER');
			
			/****************************************************
			 * edits by neri: 11-06-08
			 * added code for displaying the profile component
			 ****************************************************/
			
			$profile_comp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
			$profile_comp->init($oGroup->getGroupID());
			
  			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));	
			$tpl = $this->file_factory->getClass('Template');
			$tpl->set_path("travellog/views/");
			$tpl->set("groupID",$oGroup->getGroupID());
			$tpl->set("surveyFormID",$frmID);		
			$tpl->set("jsonGrpStr",$jsonGrpStr);
			$tpl->set("jsonForm",$jsonForm);
			$tpl->set("subNavigation",$subNavigation);
			$tpl->set("profile", $profile_comp->get_view());
			$tpl->out("tpl.SurveyFormEditor.php");		
		}
		
	}
?>