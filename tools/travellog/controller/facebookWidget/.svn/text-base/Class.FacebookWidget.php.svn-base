<?php
require_once("travellog/model/Class.SessionManager.php");
require_once("Class.Template.php");
require_once("Class.CookieManager.php");
class FacebookWidget {
	
	const CONTEXT = 'facebook';
	
	// setting constants
	const NO_RECORD = 2;
	const CONNECTED = 1;
	const NOT_CONNECTED = 0;
	
	protected $apiObj = null;
	protected $cookie_manager = null;
	
	private $tpl = '';
	private $authorizeUrl = '/splauncher.php?action=connect&context=facebook';
	private $widgetStatus = null;
	private $travelerID = 0; // make this as default
	private $connectErrorMessage = "";
	private $widgetSettings = null;
	
	public function __construct(){
		$this->tpl = new Template();
		$this->travelerID = isset($_GET['travelerID']) ? $_GET['travelerID'] : SessionManager::getInstance()->get('travelerID');
		$this->apiObj = OAuthAPIFactory::getInstance()->create(self::CONTEXT);

		if(isset($_GET['context']) AND $_GET['context'] == self::CONTEXT){
			$this->cookie_manager = CookieManager::getInstance();
			$this->connect();
		} //else {
			$this->prepareWidget();
	//	}		
	}
	
	public function render(){
		Template::includeDependentCss('/css/sharingAndConnectionsWidget.css');
		Template::includeDependentCss("/css/vsgStandardized.css");
		Template::includeDependentCss("/css/modalBox.css");
		Template::includeDependentCss('/css/Thickbox.css');
		
		Template::includeDependentJs('/js/thickbox.js');
		
		$this->tpl->set('authorizeUrl', $this->authorizeUrl);
		$this->tpl->set('widgetStatus', $this->widgetStatus);
		$this->tpl->set('travelerID', $this->travelerID);
		$this->tpl->set('errorMessage', $this->connectErrorMessage);
		$this->tpl->set('widgetSettings', $this->widgetSettings);
		$this->tpl->set('siteName', isset($GLOBALS['CONFIG']) ? $GLOBALS['CONFIG']->getSiteName() : 'GoAbroad');
		
		$template = $this->tpl->fetch('travellog/views/facebookWidget/tpl.ViewFacebookWidget.php');
		return $template;
	}
	
	private function prepareWidget(){
		$widgetConnection = $this->apiObj->checkWidgetConnection($this->travelerID);
		switch($widgetConnection){
			case 'ERROR_FACEBOOK_CONNECTION':
				$this->connectErrorMessage = "There seems to be a problem connecting to facebook.";
				$this->widgetStatus = self::NO_RECORD;
				break;
			case 'NOT_CONNECTED':
				$this->connectErrorMessage = "";
				$this->prepareWidgetSettings();
				$this->widgetStatus = self::NOT_CONNECTED;
				break;
			case 'CONNECTED':
				$this->connectErrorMessage = "";
				$this->prepareWidgetSettings();
				$this->widgetStatus = self::CONNECTED;
				break;
			case 'NO_RECORD':
				$this->widgetStatus = self::NO_RECORD;
			default:
				break;
		}
		
	}
	
	private function prepareWidgetSettings(){
		$ganetTravelerAccount = GanetTravelerAccountPeer::retrieveTravelerAccountByTravelerIDandServiceID($this->travelerID, $this->apiObj->getServiceProviderID());
		if($ganetTravelerAccount){
			$this->widgetSettings = GanetFacebookSettingPeer::retrieveSettingsByTravelerAccountID($ganetTravelerAccount);
		}
		
	}
	
	private function connect(){
		$token_index = self::CONTEXT.'_oauth_token';
		$oauth_token = $this->cookie_manager->get($token_index);
		if($oauth_token){
			$domain = FALSE === strpos($_SERVER["SERVER_NAME"],".local") ? ".goabroad.net" : ".goabroad.net.local";
			$this->cookie_manager->clear($token_index, "/", $domain);
			$status = $this->apiObj->saveWidgetConnection($oauth_token, $this->travelerID);
			if(!$status){
				$this->connectErrorMessage = 'The account you\'re trying to access is already in use.';
				$this->widgetStatus = self::NO_RECORD;
			}
			elseif($status) {
				$this->connectErrorMessage = '';
				$this->widgetStatus = self::CONNECTED;
				$this->prepareWidgetSettings();
			}
		}
		else {
			self::markConnect($this->travelerID);
			$this->prepareWidgetSettings();
		}
		
	}
	
	public static function markConnect($travelerID){
		$api = OAuthAPIFactory::getInstance()->create(self::CONTEXT);
		
		try{
			$ganetTravelerAccount = GanetTravelerAccountPeer::retrieveTravelerAccountByTravelerIDandServiceID($travelerID, $api->getServiceProviderID());

			if($ganetTravelerAccount){
				$settings = GanetFacebookSettingPeer::retrieveSettingsByTravelerAccountID($ganetTravelerAccount);
				// just in case the user has no settings yet
				if(is_null($settings)){
					$settings = new GanetFacebookSetting();
					$settings->setTravelerAccountID($ganetTravelerAccount->getID());
					//$settings->setMessage("Check out my GoAbroad Network account.");
					$settings->setMessage(OAuthFacebookAPI::DEFAULT_MESSAGE);
				}
				
				$settings->setConnectionStatus(1);
				$settings->save();
				
				// log action
				$api->log($ganetTravelerAccount, 'connect');
			}
		}catch(Exception $e){}
		
		
	//	$self = new self();
	//	echo $self->render();
	//	echo $self->ajaxPageRender();
	}
	
	public static function markDisconnect($travelerID){
		$api = OAuthAPIFactory::getInstance()->create(self::CONTEXT);
		
		try{
			$ganetTravelerAccount = GanetTravelerAccountPeer::retrieveTravelerAccountByTravelerIDandServiceID($travelerID, $api->getServiceProviderID());

			if($ganetTravelerAccount){
				$settings = GanetFacebookSettingPeer::retrieveSettingsByTravelerAccountID($ganetTravelerAccount);
				if($settings){
					$settings->setConnectionStatus(0);
					$settings->save();
					
					// log action
					$api->log($ganetTravelerAccount, 'disconnect');
				}
			}
		}catch(Exception $e){}
		
		
	//	$self = new self();
	//	echo $self->render();
	//	echo $self->ajaxPageRender();
	}
	
	public static function saveSettings($travelerID, $defaultText){
		$api = OAuthAPIFactory::getInstance()->create(self::CONTEXT);
		
		$ganetTravelerAccount = GanetTravelerAccountPeer::retrieveTravelerAccountByTravelerIDAndServiceID($travelerID, $api->getServiceProviderID());
		if($ganetTravelerAccount){
			$settings = GanetFacebookSettingPeer::retrieveSettingsByTravelerAccountID($ganetTravelerAccount);
			if(is_null($settings)){
				$settings = new GanetFacebookSetting();
				$settings->setTravelerAccountID($ganetTravelerAccount->getID());
				$settings->setConnectionStatus(1);
			}
			
			$settings->setMessage($defaultText);
			$settings->save();
			
			// log action
			$api->log($ganetTravelerAccount, 'changed post text');
		}
		
	}
	
}