<?php
/**
 * <b>Abstract Event</b> controller.
 * @author Aldwin S. Sabornido
 * @version 2.0 PHP5
 * @see http://www.GoAbroad.com/
 * @copyright 2006-2007
 */

require_once('travellog/model/navigator/Class.BackLink.php');
 
abstract class AbstractEvent
{
	/*protected
	
	$title       = NULL,
	
	$date        = NULL,
	
	$public      = NULL,
	
	$description = NULL,
		
	$eventID     = NULL,
	
	$errors      = NULL;
	
	
	public abstract function Add();
	
	public abstract function Save();
	
	public abstract function Edit();
	
	public abstract function Delete();
	
	public abstract function Update();
	
	public abstract function View( $page );
	
	public function setEventID( $eventID )
	{
		$this->eventID = $eventID;
	}
	public function getEventID()
	{
		return $this->eventID;
	}
	
	public function setTitle( $title )
	{
		$this->title = $title;
	}
	public function getTitle()
	{
		return $this->title;
	}
	
	public function setDate( $date )
	{
		$this->date = $date;
	}
	public function getDate()
	{
		return $this->date;
	}
	
	public function setDescription( $description )
	{
		$this->description = $description;
	}
	public function getDescription()
	{
		return $this->description;
	}
	
	public function setDisplayPublic($public)
	{
		$this->public = $public; 	
	}
	public function getDisplayPublic()
	{
		return $this->public; 	
	}
		
	public function setErrors( $errors )
	{
		$this->errors = $errors;
	}
	public function getErrors()
	{
		return $this->errors;
	}*/
	
	protected 
	
	$main_tpl      = NULL,
	
	$obj_date      = NULL,
	
	$obj_backlink  = NULL,
	
	$errors        = array(),
	
	$context       = NULL,
	
	$IsLogin       = false,
	
	$genericID     = NULL,
	
	$mode          = NULL;	
	
	function __construct()
	{
		$this->main_tpl     = new Template();
		$this->obj_date     = ClassDate::instance();
		$this->obj_backlink = BackLink::instance();
	}
	
	public abstract function isValidForm( $formvars );
	
	public abstract function getData();
	
	public abstract function displayForm( $formvars = array() );
	
	public abstract function addEntry( $formvars );
	
	public abstract function updateEntry( $formvars );
	
	public abstract function removeEntry();
	
	public function setMode( $mode )
	{
		$this->mode = $mode;
	}
	
	public function getMode()
	{
		return $this->mode;
	}
	
	public function setContext( $context )
	{
		$this->context = $context;
	}
	
	public function getContext()
	{
		return $this->context;
	}
	
	public function setGenID( $genericID )
	{
		$this->genericID = $genericID;
	}
	
	public function getGenID()
	{
		return $this->genericID;
	}
	
	public function setIsLogin( $IsLogin )
	{
		$this->IsLogin = $IsLogin;
	}
}
?>
