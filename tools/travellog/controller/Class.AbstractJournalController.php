<?php
require_once("Class.ToolMan.php");
require_once("travellog/controller/Class.IController.php");
require_once("travellog/factory/Class.FileFactory.php");

// Added by: Adelbert
require_once('travellog/controller/socialApps/Class.fbNeedToUpdateAccountController.php');
abstract class AbstractJournalController implements IController{
	
	protected static $REQUIRES_ACTION_RULES = array('ADD', 'SAVE', 'EDIT', 'UPDATE', 'DELETE', 'APPROVE', 'UNAPPROVE', 'MYJOURNALS', 'GROUPJOURNALS', 'GROUPMEMBERSJOURNAL', 'PUBLISHUNPUBLISH' );
	
	protected 
	
	$data         = array(),
	
	$obj_session  = NULL,
	
	$file_factory = NULL;
	
	function __construct(){
		$this->obj_session  = SessionManager::getInstance();     
		$this->file_factory = FileFactory::getInstance(); 
		
		$this->file_factory->registerClass('MainLayoutView', array('path' => 'travellog/views/'));
		$this->file_factory->registerClass('Traveler');
		$this->file_factory->registerClass('JournalPrivacyPreference');
		$this->file_factory->registerClass('AdminGroup');
		$this->file_factory->registerClass('GroupFactory');
		$this->file_factory->registerClass('HelpText');
		$this->file_factory->registerClass('Travel');
		$this->file_factory->registerClass('JournalLog'); 
		$this->file_factory->registerClass('LocationFactory');
		$this->file_factory->registerClass('TravelLog');
		$this->file_factory->registerClass('FilterCriteria2');
		$this->file_factory->registerClass('Condition');
		$this->file_factory->registerClass('GroupApprovedJournals'  , array('path' => 'travellog/dao/')); 
		$this->file_factory->registerClass('FeaturedSection'        , array('path' => 'travellog/model/admin/'));
		$this->file_factory->registerClass('BackLink'               , array('path' => 'travellog/model/navigator/'));
		$this->file_factory->registerClass('JournalsComp'       	, array('path' => 'travellog/views/','file'=>'JournalsComp'));
		$this->file_factory->registerClass('ArticleComp'       		, array('path' => 'travellog/views/','file'=>'ArticleComp'));
		$this->file_factory->registerClass('EntriesArticlesComp'	, array('path' => 'travellog/views/','file'=>'EntriesArticlesComp'));
		$this->file_factory->registerClass('GAMapView'              , array('path' => 'travellog/views/'));
		$this->file_factory->registerClass('Map');
		$this->file_factory->registerClass('LocationMarker');
		$this->file_factory->registerClass('MapFrame'               , array('file' => 'Map', 'class' => 'MapFrame'));
		
		$this->file_factory->registerClass('SubNavigation');
		$this->file_factory->registerClass('Template'               , array('path' => ''));
		$this->file_factory->registerClass('Paging'                 , array('path' => ''));
		$this->file_factory->registerClass('ObjectIterator'         , array('path' => ''));
		$this->file_factory->registerClass('GaDateTime'               , array('path' => ''));
		$this->file_factory->registerClass('StringFormattingHelper' , array('path' => ''));
		$this->file_factory->registerClass('Criteria2'               , array('path' => ''));
		$this->file_factory->registerClass('HtmlHelpers'            , array('path' => ''));
		
		//$this->file_factory->registerClass( 'ComponentProfileController' , array('path' => 'travellog/components/profile/controller/') );
		$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
		//$this->file_factory->registerClass( 'ComponentProfileView'       , array('path' => 'travellog/components/profile/views/')      );
		$this->file_factory->registerClass( 'RandomTravelBioView'        , array('path' => 'travellog/views/')   );
		$this->file_factory->registerClass( 'Search' );
		
		$this->file_factory->registerTemplate('TravelView', array('file' => 'new-TravelView'));
		$this->file_factory->registerTemplate('MainTravel');
		$this->file_factory->registerTemplate('MainTravelerJournals');		
		$this->file_factory->registerTemplate('LayoutMain');
		$this->file_factory->registerTemplate('FrmTravel');
		$this->file_factory->registerTemplate('Travel');
		$this->file_factory->registerTemplate('SearchResults');
		
		$this->file_factory->setPath('CSS', '/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');  
		//Alexandria
		//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
	}
	
	function performAction(){
		
		$this->beforePerformAction();
		
		$this->data           = array_merge($_GET, $_POST);
		$this->data['action'] = ( isset($this->data['action']))? $this->data['action'] : 'All';
		//TODO: I must transfer __applyRules function to a specific action method. 
		$this->__applyRules();
		try{
			switch( strtolower($this->data['action']) ){
				case "groupjournals":
					$this->_viewGroupJournals();
				break;
				
				case "groupmembersjournal":
					$this->_viewGroupJournals();
				break;
				
				case "myjournals":
					$this->_viewMyJournals();
				break;
				
				case "publish":
					$this->_approve();
				break;
				
				case "unpublish": 
					$this->_unapprove();
				break;
				
				case "edit":
				case "add":
					$this->__getData();
					$this->_viewAddForm();
				break;
				
				case "save":
				case "next":
					$this->__validateForm();
					$this->_save();
				break;
				
				case "delete":
					$this->_delete();
				break;
				
				case "publishunpublish":
					$this->_publishUnpublish();
				break;
				
				case "featureingroup":
					$this->_featureInGroup();
				break;
				
				case "unfeatureingroup":
					$this->_unfeatureInGroup();
				break;
				
				case "relatejournalphotostogroup":
					$this->_relateJournalPhotosToGroup();
				break;
				case "searchjournalsbyauthor":
					$this->_searchJournalsByAuthor();
				break;
				default:
					$this->_viewAllJournals();
			}
		}catch( Exception $e ){ //var_dump($e->getMessage());exit;
			//ToolMan::redirect('/journal.php');  	
		} 
	}
	
	protected function _viewAddForm(){
		require_once('travellog/model/Class.JournalEntryDraftPeer.php');
		require_once('Class.Criteria2.php');
		
		$travelerID                = $this->obj_session->get('travelerID');
		$obj_traveler              = $this->file_factory->getClass('Traveler', array($travelerID));
		$this->data['col_subgroups'] = array();
		$this->data['context']       = 1;
		$this->data['obj_help_text'] = $this->file_factory->getClass('HelpText');
		$this->data['obj_back_link'] = $this->file_factory->invokeStaticClass('BackLink','instance');
		$this->data['obj_back_link']->Create();
		$this->data['show_auto_save_alert'] = false;
		$obj_navigation = $this->file_factory->getClass('SubNavigation');
		$factory = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array());
		
		$curGroupID = (array_key_exists('gID',$this->data)? $this->data['gID'] : -1 );
		$contextGroupID = (array_key_exists('contextGroupID',$_GET) ? $_GET['contextGroupID'] : -1);
		
		//if(isset($_GET['contextGroupID']) || $obj_traveler->isAdvisor()){
		if(isset($_GET['contextGroupID'])){
			$contextGroupID = $_GET['contextGroupID'];
			$this->data['action'].= (false===strpos('?',$this->data['action'])?'?':'&').'contextGroupID='.$contextGroupID;
			//check if the logged traveler is a staff of the given contextGroupID
			$contextGroup = $this->file_factory->getClass('AdminGroup', array($contextGroupID));
			//$administeredGroupID = AdminGroup::getAdvisorGroupID($travelerID);
			//$isAdmin = $administeredGroupID == $contextGroupID || $administeredGroupID ==$contextGroup->getParentID();
			$arrGroupID = $obj_traveler->getGroupsAdministered(true);
			$isAdmin = in_array($contextGroupID, $arrGroupID) || in_array($contextGroup->getParentID(), $arrGroupID);
			$isStaff = $obj_traveler->isGroupStaff($contextGroupID);

			if(!$isAdmin && !$isStaff){
				ToolMan::redirect('/journal.php');
			}
			
			$this->data['context'] 	= 2;
			$this->data['gID']     	= ( isset($this->data['gID']) ? $this->data['gID']: $contextGroupID);
			
			$obj_admin_group       	= $this->file_factory->getClass('AdminGroup', array($this->data['gID']));
			$this->data['contextGroup'] 	= $contextGroup;
			$this->data['adminGroup']   	= $obj_admin_group;
			
			if( $contextGroup->isParent() ){
				$this->data['col_subgroups'] = $contextGroup->getSubGroups(); 
			}
			
			$obj_navigation->setContext('GROUP');
			$obj_navigation->setContextID($contextGroupID);
			
			$profile_comp = $factory->create(ProfileCompFactory::$GROUP_CONTEXT);
			$profile_comp->init($this->data['gID']);
			
			if (0 < $contextGroup->getParentID()) {
				$criteria = new Criteria2();
				$criteria->add(JournalEntryDraftPeer::IS_AUTO_SAVED, 1);
				$this->data['unfinished_entries_count'] = JournalEntryDraftPeer::retrieveDraftEntryCount($contextGroupID, 2, $criteria);
				$this->data['show_auto_save_alert'] = (0 < $this->data['unfinished_entries_count']) ? true : false;
				$this->data['view_drafts_link'] = "/journal_draft/view_saved_drafts/group/$contextGroupID";
			}
		}
		else{
			$this->data['addingAs'] = 'TRAVELER';
			
			$obj_navigation->setContext('TRAVELER');
			$obj_navigation->setContextID($travelerID);
			
			$profile_comp = $factory->create(ProfileCompFactory::$TRAVELER_CONTEXT);
			$profile_comp->init($travelerID);
		}
		
		$obj_template     = $this->file_factory->getClass('Template');
		$obj_inc_template = $this->file_factory->getClass('Template');
		
		$obj_navigation->setLinkToHighlight('MY_JOURNALS');
		
		$obj_inc_template->set('props', $this->data);
		
		$obj_template->set_vars(
			array(
				'title'         => "Add Travel",
				'form'          => $obj_inc_template->fetch($this->file_factory->getTemplate('FrmTravel')),
				'context'       => $this->data['context'],
				'subNavigation' => $obj_navigation,
				'profView'		=> $profile_comp->get_view()
			)
		);
		
		Template::setMainTemplate($this->file_factory->getTemplate('LayoutMain'));
		if( isset($_GET['contextGroupID']) ){
			if( isset($GLOBALS["CONFIG"]) ){
				Template::setMainTemplateVar('page_location', 'Home');
			}else{
				Template::setMainTemplateVar('page_location', 'Groups / Clubs');
			}
		}else{
			Template::setMainTemplateVar('page_location', 'My Passport');
		}
		Template::includeDependentJs('/min/f=js/prototype.js');
		Template::includeDependentJs('/js/interactive.form.js');
		Template::setMainTemplateVar('layoutID', 'journals');
		
		$obj_template->out($this->file_factory->getTemplate('Travel'));
	}
	
	protected function _save(){
		$travelerID   = $this->obj_session->get('travelerID');
		$obj_traveler = $this->file_factory->getClass('Traveler', array($travelerID) );
		$refID        = $travelerID;
		$refType      = 1;
		
		$this->data['description'] = preg_replace("#&lt;(.|\n)*?&gt;#","",strip_tags($this->data['description'],'<p><b><strong><i><em><u><span>'));
		
		//if(array_key_exists('contextGroupID',$_GET) || $obj_traveler->isAdvisor() ){
		if(array_key_exists('contextGroupID',$_GET)){
			$contextGroupID = (array_key_exists('contextGroupID',$_GET) ? $_GET['contextGroupID'] : 0);
			
			$contextGroup 		   	= $this->file_factory->getClass('AdminGroup', array($contextGroupID));
			//$administeredGroupID 	= AdminGroup::getAdvisorGroupID($travelerID);
			
			//$isAdmin = $administeredGroupID == $contextGroupID || $administeredGroupID == $contextGroup->getParentID();
			
			$arrGroupID = $obj_traveler->getGroupsAdministered(true);
			
			$isAdmin = in_array($contextGroupID, $arrGroupID) || in_array($contextGroup->getParentID(), $arrGroupID);
			$isStaff = $obj_traveler->isGroupStaff($contextGroupID);
			
			if(!$isAdmin && !$isStaff){
				ToolMan::redirect('/journal.php');
			}
			$refID    = $this->data['gID'];
			$refType  = 2;
		}
		/*elseif( $obj_traveler->isAdvisor()){
			$refID    = $this->data['gID'];
			$refType  = 2;   
		}*/
		
		
		if( $this->data['travelID'] ){

			$obj_travel = $this->file_factory->getClass('Travel', array($this->data['travelID']));
			$obj_travel->setRefID      ( $refID                   );
			$obj_travel->setRefType    ( $refType                 );
			$obj_travel->setTitle      ( $this->data['title']     );
			$obj_travel->setPublish    ( $this->data['publish']   );
			$obj_travel->setTravelerID ( $travelerID              );
			$obj_travel->setDescription( $this->data['description'] );
			$obj_travel->setPrivacyPref( $this->data['privacypref'] ); 
			$obj_travel->Update();
			
			$obj_travel->DeleteTravelGroup($this->data['travelID']);
			
			if( isset($this->data['gIDs']) && count($this->data['gIDs']) ){
				foreach( $this->data['gIDs'] as $groupID ){
					$obj_travel->CreateTravelGroup($groupID);
				}
			}
			
			// TODO: This should be transfered to observer class 		
			$obj_log = $this->file_factory->getClass('JournalLog');
			$obj_log->setTravelID( $this->data['travelID']            );
			$obj_log->setLogType ( JournalLogType::$UPDATED_JOURNAL );
			$obj_log->setLinkID  ( $this->data['travelID']            );
			$obj_log->Save();
			
			ToolMan::redirect('/passport.php');
		}	
		else{
			$obj_travel   = $this->file_factory->getClass('Travel');
			
			$obj_travel->setRefID      ( $refID                     );
			$obj_travel->setRefType    ( $refType                   );
			$obj_travel->setTitle      ( $this->data['title']       );
			$obj_travel->setPublish    ( $this->data['publish']     );
			$obj_travel->setTravelerID ( $travelerID                );
			$obj_travel->setDescription( $this->data['description'] );
			$obj_travel->setPrivacyPref( $this->data['privacypref'] );
			$obj_travel->Create();
			
			if( isset($this->data['gIDs']) && count($this->data['gIDs']) ){
				foreach( $this->data['gIDs'] as $groupID ){
					$obj_travel->CreateTravelGroup($groupID);
				}
			}

			// Added by: Adelbert Silla
			// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
			//           This is a preperation for updating application static views in facebook
			// Date added: Aug. 10, 2009
			// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
			fbNeedToUpdateAccountController::saveNewlyEditedAccount($travelerID, 4);
			
			ToolMan::redirect('/journal-entry.php?action=Add&travelID=' . $obj_travel->getTravelID());
		}
		
		
	}
	
	protected function _approve(){
		$this->file_factory->getClass('AdminGroup');
		$obj_group_approved = $this->file_factory->getClass('GroupApprovedJournals');
		$gID                = AdminGroup::getAdvisorGroupID( $this->obj_session->get('travelerID') );
		if( GroupApprovedJournals::isExistsJournal($this->data['travelID'], $gID) ){
			$obj_group_approved->setTravelID( $this->data['travelID'] );
			$obj_group_approved->setGroupID ( $gID );
			$obj_group_approved->GetFiltered(); 
			$obj_group_approved->setApproved( 1    );
			$obj_group_approved->Update();
		}
		else{
			$obj_group_approved->setGroupID ( $gID                    );
			$obj_group_approved->setTravelID( $this->data['travelID'] );		
			$obj_group_approved->setApproved( 1                       );
			$obj_group_approved->Save();
		}
		session_cache_limiter(""); 
		ToolMan::redirect('/journal.php?action=GroupMembersJournal');
	}
	
	protected function _unapprove(){
		$this->file_factory->getClass('AdminGroup');
		$obj_group_approved = $this->file_factory->getClass('GroupApprovedJournals');
		$gID        = AdminGroup::getAdvisorGroupID( $this->obj_session->get('travelerID') );
		$obj_group_approved->setTravelID( $this->data['travelID'] );
		$obj_group_approved->setGroupID ( $gID );
		$obj_group_approved->GetFiltered();  
		$obj_group_approved->setApproved(0);
		$obj_group_approved->Update();
		session_cache_limiter(""); 
		ToolMan::redirect('/journal.php?action=GroupMembersJournal'); 
	}
	
	protected function _delete(){
		try{
			$exists = true;
			$obj_travel = $this->file_factory->getClass('Travel', array($this->data['travelID']) );
		}
		catch(exception $e){
			$exists = false;
		}
		
		if($exists){	
			$obj_travel->Delete();
			$obj_log = $this->file_factory->getClass('JournalLog');
			$obj_log->setLogType ( 3                     );
			$obj_log->setTravelID( $this->data['travelID'] );
			$obj_log->Delete();	
		}
		
		// Added by: Adelbert Silla
		// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
		//           This is a preperation for updating application static views in facebook
		// Date added: Aug. 10, 2009
		// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
		fbNeedToUpdateAccountController::saveNewlyEditedAccount($this->obj_session->get('travelerID'),4);

		//ToolMan::redirect('/journal.php?action=MyJournals'); 
		ToolMan::redirect($_SERVER['HTTP_REFERER']);
	}
	
	function _publishUnpublish(){
		$obj_travel = $this->file_factory->getClass('Travel', array($this->data['travelID']) );
		$obj_travel->PublishUnpublishEntry();
		//ToolMan::redirect('/journal.php?action=MyJournals'); 
	}
	
	protected function _viewMyJournals(){
		$props                       = array();
		$props['is_login']           = $this->obj_session->getLogin();
		$props['travelerID']                = isset($this->data['travelerID']) ? $this->data['travelerID'] : $this->obj_session->get('travelerID');
		
		if (!$props['travelerID']){
			header('location: /journal.php');
			exit;
		}
		
		$traveler     = $this->file_factory->getClass('Traveler', array($props['travelerID']) );
		if(is_object($traveler) && ($traveler->isSuspended() || $traveler->isDeactivated())){
			header('location: /journal.php');
			exit;
		}
		//$obj_privacy_pref    = $this->file_factory->getClass('JournalPrivacyPreference');
		
		$props['is_owner']   = ( $traveler->getTravelerID() == $this->obj_session->get('travelerID') )? true : false;
		$loggedTraveler   	 = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')));
		
		// set up owner component
		//$profCont = $this->file_factory->getClass( 'ComponentProfileController' , array($props['travelerID']) );
		//$profView   = $profCont->get_view();
		$factory      = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array());
		$profile_comp = $factory->create( ProfileCompFactory::$TRAVELER_CONTEXT );
		$profile_comp->init($props['travelerID']);
		$profView = $profile_comp->get_view();
						  
		$obj_navigation = $this->file_factory->getClass('SubNavigation');
		
		$obj_navigation->setContext        ( 'TRAVELER'            );
		$obj_navigation->setContextID      ( $props['travelerID'] );
		$obj_navigation->setLinkToHighlight( 'MY_JOURNALS'      );
		if( $props['is_login'] && $props['is_owner'] ) 
			$obj_navigation->setDoNotShow(false);

		$obj_template = $this->file_factory->getClass('Template');
		
		$arr_contents['props']                = $props;
				
		$jCompContextAr 				=	$this->file_factory->invokeStaticClass('JournalsComp','convertURLParamToSetupArray');
		
		// setup for viewing all journals
		if (count($jCompContextAr) == 0){
			$jCompContextAr['VIEWMODE'] 			=	'PROFILE.ALL';
			if ($props['is_owner'])
				$jCompContextAr['SECURITY_CONTEXT'] 	=	'LOGIN';
			else
				$jCompContextAr['SECURITY_CONTEXT'] 	=	'PUBLIC';
		}
		
		if(isset($GLOBALS['CONFIG']))
			$jCompContextAr['COBRAND_PROFILE'] = true;
		
		if(isset($jCompContextAr['TYPE']) && $jCompContextAr['TYPE'] == 'ARTICLES')
			$jCompContextAr['CURRENT_PAGE'] = NULL;
				
		$journalsComp				=	$this->file_factory->getClass('JournalsComp',array($jCompContextAr,$props['travelerID'],$this->obj_session->get('travelerID')));
		
		$setup = array(
			'OWNER' => $props['is_owner'],
			'CONTEXT' => "author",
			'DATA' => $props['travelerID'],
			'SUBJECT' => $traveler,
			'RPP' => 6
		);
		
		$arr_setup = $this->file_factory->invokeStaticClass('ArticleComp','convertURLParamToSetupArray');
		
		if(!isset($arr_setup['TYPE']))
			$arr_setup['PAGE'] = NULL;
			
		$setup = array_merge($setup,$arr_setup,$jCompContextAr);							
		
		$articleComp = $this->file_factory->getClass('ArticleComp',array($setup));
		$this->IncGroupArticles = $articleComp;		
		$this->viewGroupArticles = ($articleComp->getArticlesCount() > 0) ? true : false;
		 
		$obj_template->set_vars
		( array(
			'journalsComp'			   => $journalsComp,
			'articlesComp'			   => $articleComp,	
			'objSubNavigation'         => $obj_navigation,
			'profView'     			   => $profView,
			'jCompContextAr'		   => $jCompContextAr	
		));
		
		Template::setMainTemplateVar('title', 'Travels and Travel Journals of GoAbroad Network Users');
		Template::setMainTemplateVar('metaDescription', 'View the travel journals of other users and be inspired to do the same.');
		Template::setMainTemplate($this->file_factory->getTemplate('LayoutMain'));
		Template::setMainTemplateVar('layoutID', 'journals');
		
		Template::includeDependentCss("/css/vsgStandardized.css");
		Template::includeDependentCss("/css/modalBox.css");
		
		if( $props['is_owner'] ){
			Template::setMainTemplateVar('page_location', 'My Passport');
		}else{
			Template::includeDependentCss("/css/Thickbox.css");
			Template::includeDependentJs("/js/thickbox.js");
			Template::setMainTemplateVar('page_location', 'Travelers');
		}
		Template::includeDependentJs("/min/f=js/jquery-latest.pack.js",array('bottom'=>false));
		
		$obj_template->out($this->file_factory->getTemplate('MainTravelerJournals'));
	}
	
	protected function _viewGroupJournals(){
		$props                       = array();
		$props['is_login']           = $this->obj_session->getLogin();
		$props['gID']                = $this->data['gID'];
		
		
		if (isset($this->data['tID']))
			$props['tID'] = 	$this->data['tID'];
		else
			$props['tID'] = 	0;	
		
		
		if (isset($this->data['jcp']))
			$props['jcp'] = 	$this->data['jcp'];
		else
			$props['jcp'] = 	'';		
				
	
		if (!$props['gID'])
			header('location: /journal.php');
		
		$obj_admin_group     = $this->file_factory->getClass('AdminGroup', array($this->data['gID']) );
		$obj_privacy_pref    = $this->file_factory->getClass('JournalPrivacyPreference');
		
		$props['is_owner']   = ( $obj_admin_group->getAdministratorID() == $this->obj_session->get('travelerID') )? true : false;
		$loggedTraveler   	 = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')));
		$props['is_owner']   = ( $obj_admin_group->getAdministratorID() == $this->obj_session->get('travelerID') || $loggedTraveler->isGroupStaff($this->data['gID']))? true : false;
		
		$props['is_parent']  = $obj_admin_group->isParent();
		$props['travelerID'] = $obj_admin_group->getAdministratorID();
		//if( !$props['is_owner'] ) $props['journal_page_owner'] = $obj_admin_group->getName();
		
		// set up owner component
		
		$factory      = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array());
		$profile_comp = $factory->create( ProfileCompFactory::$GROUP_CONTEXT );
		$profile_comp->init($props['gID']);
		$profView = $profile_comp->get_view();
						  
		$obj_navigation = $this->file_factory->getClass('SubNavigation');
		
		$obj_navigation->setContext        ( 'GROUP'            );
		$obj_navigation->setContextID      ( $this->data['gID'] );
		$obj_navigation->setGroupType      ( 'ADMIN_GROUP'      );
		$obj_navigation->setLinkToHighlight( 'MY_JOURNALS'      );
		if( $props['is_login'] && $props['is_owner'] ) 
			$obj_navigation->setDoNotShow(false);

		$obj_template = $this->file_factory->getClass('Template');
		
		$arr_contents['props']                = $props;
				
		$jCompContextAr 				=	$this->file_factory->invokeStaticClass('JournalsComp','convertURLParamToSetupArray');
		
		// setup for viewing all journals
		if (count($jCompContextAr) == 0){
			if ($props['is_owner']) {
				$jCompContextAr['VIEWMODE'] 			=	'GROUP.MANAGE';
				$jCompContextAr['SECURITY_CONTEXT'] 	=	'LOGIN';
				$jCompContextAr['VIEW_TAB'] 			=	'NEW';
			} else {
				$jCompContextAr['VIEWMODE'] 			=	'GROUP';				
				$jCompContextAr['SECURITY_CONTEXT'] 	=	'PUBLIC';
				$jCompContextAr['VIEW_TAB'] 			=	'STAFF_ADMIN_APPROVED';
			}
		}
		//if(isset($this->data['COMBINE']))
			$jCompContextAr['JOURNALS_ARTICLES'] = true;
		if($jCompContextAr['SECURITY_CONTEXT'] == 'LOGIN'){
			$journalsComp				=	$this->file_factory->getClass('JournalsComp',array($jCompContextAr,$props['gID'],$this->obj_session->get('travelerID')));
			$journalsComp->setJournalCriteria2(array('TRAVELERID'=>$props['tID']));
		}else{
			$journalsComp				=	$this->file_factory->getClass('EntriesArticlesComp',array($jCompContextAr,$props['gID'],$this->obj_session->get('travelerID')));
			//$journalsComp->setSearchCriteria2(array('TRAVELERID'=>$props['tID']));
		}	
		
		$viewBy = $props['is_owner']?'GROUP':'TRAVELER';
		require_once('travellog/model/Class.JournalEntryDraftPeer.php');
		
		
		$obj_template->set_vars
		( array(
			'journalsComp'			   => $journalsComp,
			'objSubNavigation'         => $obj_navigation,
			'profView'     			   => $profView,
			'jCompContextAr'		   => $jCompContextAr,
			'is_owner'				   => $props['is_owner'],
			'gID'					   => $props['gID'],
			'origParam'				   => $journalsComp->generateOrigURLParam(),
			'jcp'					   => $props['jcp'],
			'viewBy'				   => $viewBy,
			'journal_drafts_count' => JournalEntryDraftPeer::retrieveDraftEntryCount($props['gID'], 2)			
		));
		
		Template::setMainTemplateVar('title', 'Travels and Travel Journals of GoAbroad Network Users');
		Template::setMainTemplateVar('metaDescription', 'View the travel journals of other users and be inspired to do the same.');
		Template::setMainTemplate($this->file_factory->getTemplate('LayoutMain'));
		Template::setMainTemplateVar('layoutID', 'journals');
		//Template::setMainTemplateVar('page_location', 'Journals');
		require_once('travellog/helper/Class.NavigationHelper.php');
		if(NavigationHelper::isCobrand())
			Template::setMainTemplateVar('page_location', 'Home');
		else
			Template::setMainTemplateVar('page_location', 'Groups / Clubs');
		Template::includeDependentJs("/min/f=js/jquery-latest.pack.js",array('bottom'=>false));
		
		Template::includeDependentCss("/css/vsgStandardized.css");
		Template::includeDependentCss("/css/modalBox.css");
		Template::includeDependentCss("/css/Thickbox.css");
		Template::includeDependentJs("/js/thickbox.js");
		
		$obj_template->out($this->file_factory->getTemplate('MainTravel'));
	}
	
	protected function _viewAllJournals(){
		$props                    = array();
		$props['page']            = ( isset($this->data['page'])       )? $this->data['page']        : 1;
		$props['query_string']    = ( isset($this->data['travelerID']) )? '&travelerID='.$this->data['travelerID'] : '';
		$props['locationID']      = ( isset($this->data['locationID']) )? $this->data['locationID']  : 0;
		$props['tag']      		  = ( isset($this->data['tag']) )? $this->data['tag']  : NULL;
		$this->data['travelerID'] = ( $this->obj_session->getLogin()   )? $this->obj_session->get('travelerID') : 0;
		$props['is_login']        = $this->obj_session->getLogin();
		$props['travelerID']      = $this->data['travelerID'];
		$props['action']          = $this->data['action'];
		$props['show_author']     = true;
		$obj_paging               = NULL;
		$obj_iterator             = NULL; 
		$country_name             = NULL; 		
		$arr_contents             = array();
		$obj_condition            = $this->file_factory->getClass('Condition');
		$obj_filter               = $this->file_factory->getClass('FilterCriteria2');
		$jCompContextAr 				=	$this->file_factory->invokeStaticClass('JournalsComp','convertURLParamToSetupArray');
		
		
		// setup for viewing all journals
		if (count($jCompContextAr) == 0){
			$jCompContextAr['VIEWMODE'] 	=	'ALL';
		}
		//if(isset($this->data['COMBINE']))
			$jCompContextAr['JOURNALS_ARTICLES'] = true;
		// $journalsComp				=	$this->file_factory->getClass('JournalsComp',array($jCompContextAr,0));
		$journalsComp				=	$this->file_factory->getClass('EntriesArticlesComp',array($jCompContextAr,0,$this->obj_session->get('travelerID')));
 		$obj_criteria     =  $this->file_factory->getClass('Criteria2');
		$obj_criteria->mustBeEqual('_tb2.publishjournal', 1);
		$obj_criteria->mustBeEqual('_tb2.publishentry', 1);
		
		// if( $props['locationID'] ){
		// 	
		// 	$obj_factory            = $this->file_factory->invokeStaticClass('LocationFactory');
		// 	$obj_location           = $obj_factory->create($props['locationID']);
		// 	$country_name           = $obj_location->getName();
		// 	$journalsComp->setJournalCriteria2(array('LOCATIONID' => $props['locationID']));
		// }
		// 
		// if( $props['tag'] ){
		// 	$journalsComp->setJournalCriteria2(array('TAG' => $props['tag']));
		// }
		
		/***************************************************************************************/
		//                         SEARCH ENTRIES AND ARTICLES                                 //    
		/***************************************************************************************/
		if( $props['locationID'] ){
			$obj_factory            = $this->file_factory->invokeStaticClass('LocationFactory');
			$obj_location           = $obj_factory->create($props['locationID']);
			$country_name           = $obj_location->getName();
			$journalsComp->setSearchCriteria2(array('LOCATIONID' => $props['locationID']));
		}
		
		if( $props['tag'] ){
			$journalsComp->setSearchCriteria2(array('TAG' => $props['tag']));
		}
			
		$col_countries        =  $this->file_factory->invokeStaticClass( 'TravelLog', 'getCountriesWithLogs' );
		$obj_navigation       =  $this->file_factory->getClass('SubNavigation');
						
		if( isset($CONFIG) )
			$obj_navigation->setContext    ( 'GROUP'                   );
		else
			$obj_navigation->setContext    ( 'TRAVELER'                );
		
		$obj_navigation->setContextID      ( $this->data['travelerID'] );
		$obj_navigation->setLinkToHighlight( 'MY_JOURNALS'             );
		$obj_navigation->setDoNotShow(true);
		$obj_navigation->setDoNotShow(true);
		$obj_template = $this->file_factory->getClass('Template');
		$arr_contents['col_locations']        = $col_countries;		
		// $obj_map_view                         = $this->file_factory->getClass('GAMapView');		
		// $obj_map_view->setContents($arr_contents);
			
		$obj_template->set_vars
		( array(
			'journalsComp'			   => $journalsComp,		       		
			// 'obj_map_view'             => $obj_map_view,
			'props'                    => $props,
			'objSubNavigation'         => $obj_navigation,
			'collectionCountry'        => $col_countries,
			'objHelpText'              => $this->file_factory->getClass('HelpText')
		));
		
		//if(!isset($this->data['COMBINE']))
		$code = <<<BOF
<script type="text/javascript">   
//<![CDATA[
		var search = function(){			
			window.location.href = '/journal.php?action=All&locationID=' + jQuery("#countryID").attr("value");
		}		
//]]>   
</script>
BOF;
		/*else
		$code = <<<BOF
<script type="text/javascript">   
//<![CDATA[
	var search = function(){			
		window.location.href = '/journal.php?COMBINE&action=All&locationID=' + jQuery("#countryID").attr("value");
	}		
//]]>   
</script>
BOF;*/
		
		// Template::includeDependentCss( "/js/yahooCarousel/carousel.css"  );
		// Template::includeDependentJs("/js/jquery-1.1.4.pack.js"); 
		// Template::includeDependentJs("/js/yahoo/yahoo-dom-event.js", array('bottom' => true));
		// Template::includeDependentJs("/js/yahoo/utilities.js", array('bottom' => true));
		// Template::includeDependentJs("/js/yahoo/dragdrop-min.js", array('bottom' => true));
		// Template::includeDependentJs("/js/yahoo/container_core-min.js", array('bottom' => true));
		// Template::includeDependentJs("/js/yahooCarousel/carousel.js", array('bottom' => true));
				
		Template::includeDependent  ( $code , array('bottom' => true));
		Template::setMainTemplateVar('title', $country_name .' Travel Journals, Travel Blogs and Travel Experiences of GoAbroad Network Travelers');
		Template::setMainTemplateVar('metaDescription', 'View the ' .$country_name. ' travel blogs and journals of GoAbroad Network travelers and be inspired to do the same.');  
		Template::setMainTemplate($this->file_factory->getTemplate('LayoutMain'));
		Template::setMainTemplateVar('layoutID', 'main_pages');
		Template::setMainTemplateVar('page_location', 'Journals');  

		Template::includeDependentCss("/css/vsgStandardized.css");
		Template::includeDependentCss("/css/modalBox.css");
		Template::includeDependentCss('/css/Thickbox.css');
		Template::includeDependentJs('/js/thickbox.js');

		$obj_template->out($this->file_factory->getTemplate('TravelView'));
		
	}
	
	protected function _featureInGroup(){
		require_once('travellog/model/Class.Featured.php');
		Featured::setFeaturedTravelInGroup($this->data['gID'],$this->data['travelID']);
	}
	
	protected function _unfeatureInGroup(){
		require_once('travellog/model/Class.Featured.php');
		Featured::unsetFeaturedTravelInGroup($this->data['gID'],$this->data['travelID']);
	}
	
	// added by K. Gordo (relate journal photos of traveler to group)
	protected function _relateJournalPhotosToGroup(){
		if (isset($this->data['groupID']) && isset($this->data['travelID']) ){
			require_once('travellog/model/Class.Travel.php');
			
			$travel = new Travel($this->data['travelID']);
			$owner = $travel->getOwner();
						
			if (isset($this->data['authKey'])){				
				// called from a mail link
				//		we decrypt the username and password of the owner of the journal and compare it to the actual username and password
				//		in the case of a group we use the username and password of its administrator.
				// 		The encryption of authKey is in /ajaxpages/journals-comp.php 
				$authKey = $this->data['authKey'];
				try {
										
					if ($owner instanceof Traveler){
						
						$username = $owner->getUserName();
						$password = $owner->getPassword();
											
						if (crypt($username . $password,$authKey) == $authKey){							
							Travel::relateJournalPhotosToGroup($this->data['travelID'],$this->data['groupID']);	
						} 					
					} 	
					ToolMan::redirect('/group.php?gID=' . $this->data['groupID'] );									
					
				} catch (Exception $e){
					return;
				}
										
			} elseif($travelerID = $this->obj_session->get('travelerID')) {
				// called from site
				//		logged traveler must be the owner of the journal(travel)
				if ($owner instanceof Traveler){					
					if ($owner->getTravelerID() == $travelerID){						
						Travel::relateJournalPhotosToGroup($this->data['travelID'],$this->data['groupID']);
					}
				}
				ToolMan::redirect('/group.php?gID=' . $this->data['groupID'] );
			} 
				
		}
	}  
	// added by K. gordo (11/17/08)
	protected function _searchJournalsByAuthor(){
		$search = $this->file_factory->getClass('Search');
		$groupID = $this->data['gID'];
		
		$keyword = $this->data['keyword'];
		$viewBy  = $this->data['viewBy'];
		$origParam  = $this->data['origParam'];
		$jcp  = $this->data['jcp'];
		
		$obj_template = $this->file_factory->getClass('Template');
		$obj_inc_template = $this->file_factory->getClass('Template');
		
		$obj_inc_template->set_vars(
								array('displayBy' => 'TRAVELER',
								       'results'  => $search->searchTraveler($keyword,$groupID) ,
								       'viewBy'	  => $viewBy,
								       'origParam'=> $origParam,
								       'jcp'	  => $jcp ));
		$obj_template->set('contents',$obj_inc_template->fetch($this->file_factory->getTemplate('SearchResults')));
		$obj_template->out($this->file_factory->getTemplate('LayoutMain'));
		
	}
	
	protected function __applyRules(){
		if( in_array( strtoupper($this->data['action']), AbstractJournalController::$REQUIRES_ACTION_RULES) && !$this->obj_session->isSiteAdmin() ){  
			// TODO: If some actions requires a more complex rules use a rule object or method instead
			switch( strtolower($this->data['action']) ){
				case "groupjournals":
					$this->file_factory->getClass('AdminGroup');
					if( !isset($this->data['gID']) && !$this->obj_session->getLogin() ){
						ToolMan::redirect('/journal.php');
					}
					if( !isset( $this->data['gID'] ) ){
						$obj_traveler = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')));
						if( !$obj_traveler->isAdvisor() ){
							ToolMan::redirect('/journal.php');
						}
						$this->data['gID'] = AdminGroup::getAdvisorGroupID($this->obj_session->get('travelerID'));
					}	
				break;
				case "groupmembersjournal":
					if( !$this->obj_session->getLogin() ) ToolMan::redirect('/journal.php'); 
					$this->file_factory->getClass('AdminGroup');
					$obj_traveler = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')));
					if( !$obj_traveler->isAdvisor() ){
						ToolMan::redirect('/journal.php');
					}
					$this->data['gID'] = AdminGroup::getAdvisorGroupID($this->obj_session->get('travelerID'));
					$obj_admin_group = $this->file_factory->getClass('AdminGroup', array($this->data['gID']) );
					if( !$obj_admin_group->isParent() ){
						ToolMan::redirect('/group.php?gID='.$this->data['gID']); 
					}
				break;
				case "myjournals":
					if( !isset($this->data['travelerID']) && !$this->obj_session->getLogin() )
						ToolMan::redirect('/journal.php');
					if( isset($this->data['travelerID']) )	
						$obj_traveler = $this->file_factory->getClass('Traveler', array($this->data['travelerID']));
					else
						$obj_traveler = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')));
					/*if( $obj_traveler->isAdvisor() ){
						if( $this->obj_session->getLogin() ){ 
							$this->file_factory->getClass('AdminGroup');
							$gID = AdminGroup::getAdvisorGroupID( $this->obj_session->get('travelerID') );
							ToolMan::redirect('/group.php?gID=' . $gID);
						}
						else
							ToolMan::redirect('/journal.php'); 
					}*/
				break;
				
				case 'save':
					if( !$this->obj_session->getLogin() ){
						ToolMan::redirect('/journal.php'); 
					}
					if( isset($this->data['travelerID']) )	
						$obj_traveler = $this->file_factory->getClass('Traveler', array($this->data['travelerID']));
					else
						$obj_traveler = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')));
					if( !isset($_POST['action']) ){	
						if( $obj_traveler->isAdvisor() ){
							if( $this->obj_session->getLogin() ){ 
								$this->file_factory->getClass('AdminGroup');
								$gID = AdminGroup::getAdvisorGroupID( $this->obj_session->get('travelerID') );
								ToolMan::redirect('/group.php?gID=' . $gID);
							}
							else
								ToolMan::redirect('/journal.php');
						} 
					}	
				break;	

				case 'approve':
				case 'unapprove':
					if( !$this->obj_session->getLogin() ) ToolMan::redirect('/index.php');
					$obj_traveler    = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')) );
					if( !$obj_traveler->isAdvisor() ) ToolMan::redirect('/journal.php');
					$props           = array();
					$obj_admin_group = $this->file_factory->getClass('AdminGroup');
					$obj_journal     = $this->file_factory->getClass('Travel', array($this->data['travelID']));
					$props['tID']    = $obj_journal->getTravelerID();
					$props['gID']    = AdminGroup::getAdvisorGroupID( $this->obj_session->get('travelerID') );
					if( !$obj_admin_group->isMemberOfGroupNetwork($props) ){
						ToolMan::redirect('/journal.php');						
					} 
				break;
				
				case 'publishunpublish':
					if( !$this->obj_session->getLogin() ) ToolMan::redirect('/journal.php');
					if( !isset($this->data['travelID']) ) ToolMan::redirect('/journal.php?action=MyJournals');
					$obj_journal = $this->file_factory->getClass('Travel', array($this->data['travelID']));
					
					if( $obj_journal->getTravelerID() !=  $this->obj_session->get('travelerID') ) ToolMan::redirect('/journal.php?action=MyJournals');
				break;
				
				case 'delete':			  
				case 'edit':
					$obj_journal = $this->file_factory->getClass('Travel', array($this->data['travelID']));
					if(  $obj_journal->getTravelerID() != $this->obj_session->get('travelerID') )
						ToolMan::redirect('/passport.php');
				case 'add':
					if( !$this->obj_session->getLogin() ) ToolMan::redirect('/index.php');
					//$obj_traveler    = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')) );
					/*if( $obj_traveler->isAdvisor() && !array_key_exists('contextGroupID',$_GET)){
						$_GET['contextGroupID'] = AdminGroup::getAdvisorGroupID( $obj_traveler->getTravelerID() );
					}*/
					//if( array_key_exists('contextGroupID',$_GET) ){
					//	$_GET['contextGroupID'] = AdminGroup::getAdvisorGroupID( $obj_traveler->getTravelerID() );
					//}
				break;
			}
		}
	}
	
	protected function __getData(){
		if( array_key_exists('travelID', $this->data) && $this->data['travelID'] && strtolower($this->data['action']) != 'save' ){
			$obj_journal             	= $this->file_factory->getClass('Travel', array($this->data['travelID']));
			$this->data['title']       	= $obj_journal->getTitle();
			$this->data['publish']     	= $obj_journal->getPublish();
			$this->data['description'] 	= $obj_journal->getDescription();
			$this->data['privacypref'] 	= $obj_journal->getPrivacyPref();
			$this->data['gIDs']         = $obj_journal->RetrieveTravelGroup($this->data['travelID']); 
			$this->data['errors']      	= array();
			$this->data['action']      	= '/journal.php'; 
			$this->data['mode']        	= 'edit';
			$journalOwner 				= $obj_journal->getOwner();
			if($journalOwner instanceof AdminGroup){
				$_GET['contextGroupID'] = $journalOwner->getGroupID();
			}
		}
		else{
			$this->data['travelID']    = ( isset($this->data['travelID'])    )? $this->data['travelID']   : 0;
			$this->data['title']       = ( isset($this->data['title'])       )? $this->data['title']      : '';
			$this->data['publish']     = ( isset($this->data['publish'])     )? $this->data['publish']    : 1; 
			$this->data['gIDs']        = ( isset($this->data['gIDs'])        )? $this->data['gIDs']       : array();
			$this->data['description'] = ( isset($this->data['description']) )? $this->data['description']: '';
			$this->data['privacypref'] = ( isset($this->data['privacypref']) )? $this->data['privacypref']: 0;
			$this->data['errors']      = array();
			$this->data['action']      = '/journal.php';
			$this->data['mode']        = 'add';
		}	
	}
	
	protected function __validateForm(){
		$this->__getData();
		$errors = array();
		if( !preg_match('/.+/', $this->data['title']) ){
			$errors[] = 'Title is a required field!';
		}
		if( !preg_match('/.+/', $this->data['description']) ){
			$errors[] = 'Description is a required field!';
		}
		$this->data['errors'] = $errors;
		
		if( count($this->data['errors']) ){ 
			$this->_viewAddForm();
			exit;	
		}
	}
	
	
	
	protected function beforePerformAction(){
		
		if( $this->obj_session->getLogin() ){
			require_once('gaLogs/Class.GaLogger.php');
			GaLogger::create()->start($this->obj_session->get('travelerID'),'TRAVELER');  
		}
		
	}
	
}
?>
