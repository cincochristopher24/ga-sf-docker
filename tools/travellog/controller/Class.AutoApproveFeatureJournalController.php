<?php

	class AutoApproveFeatureJournalController{
		
		private $data = array();
		public function __construct(){}
		
		public function performAction(){
			$this->getRequest();
			switch($this->data['action']){
				case 'retrieve_groups':
					$this->retrieveGroups();
				break;
				case 'retrieve_groups_approved':
					$this->retrieveApprovedGroups();
				break;
				default;
			}
		}
		
		protected function retrieveGroups(){
			require_once('travellog/helper/Class.AutoApproveFeatureJournalHelper.php');
			$groups = AutoApproveFeatureJournalHelper::getAutoModeGroups($this->data['travelerID'],$this->data['travelID']);
			
			require_once('Class.Template.php');
			$tpl = new Template();
			$tpl->set('groups', $groups);			
			$tpl->set('approved', false);
			$tpl->out('travellog/views/tpl.ViewAutoApproveFeaureGroups.php');
		}
		
		protected function retrieveApprovedGroups(){
			require_once('travellog/helper/Class.AutoApproveFeatureJournalHelper.php');
			$groups = AutoApproveFeatureJournalHelper::getApprovedGroups($this->data['travelID']);
			
			require_once('Class.Template.php');
			$tpl = new Template();
			$tpl->set('groups', $groups);
			$tpl->set('approved', true);
			$tpl->out('travellog/views/tpl.ViewAutoApproveFeaureGroups.php');
		}
		
		private function getRequest(){
			$this->data = array_merge($_POST, $_GET);
			$this->data['travelerID'] = isset($this->data['travelerID']) ? $this->data['travelerID'] : 0;
			$this->data['travelID'] = isset($this->data['travelID']) ? $this->data['travelID'] : 0;
		}
	}