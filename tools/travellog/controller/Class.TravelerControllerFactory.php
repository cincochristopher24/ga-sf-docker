<?php
require_once('travellog/controller/Class.TravelerViewType.php');
class TravelerControllerFactory{
	private static $instance;
	
	public static function instance(){
 		if (self::$instance == null ){
 			self::$instance = new TravelerControllerFactory();
 		}
 		return self::$instance;	
 	}
 	
 	function create($view_type){
 		switch( strtolower($view_type) ){
 			case TravelerViewType::$ALL:
 				require_once('travellog/controller/Class.AllTravelersController.php');
 				$obj = new AllTravelersController;
 				$obj->setViewType(1);
 				break;
 			case TravelerViewType::$NEWEST:
 				require_once('travellog/controller/Class.NewestTravelerController.php');
 				$obj = new NewestTravelerController;
 				$obj->setViewType(3);
 				break;
 			case TravelerViewType::$POPULAR:
 				require_once('travellog/controller/Class.PopularTravelerController.php');
 				$obj = new PopularTravelerController;
 				$obj->setViewType(4);
 				break;
 			case TravelerViewType::$SEARCH:
 				require_once('travellog/controller/Class.KeywordsSearchTravelerController.php');
 				$obj = new KeywordsSearchTravelerController;
 				break;
 			case TravelerViewType::$INMYHOMETOWN:
 				require_once('travellog/controller/Class.TravelersInMyHometownController.php');
 				$obj = new TravelersInMyHometownController;
 				$obj->setViewType(2);
 				break;
 			case TravelerViewType::$HOMETOWNLOCATION:
 				require_once('travellog/controller/Class.HomeTravelerController.php');
 				$obj = new HomeTravelerController;
 				$obj->setViewType(7);
 				break;
 			case TravelerViewType::$CURRENTLOCATION:
 				require_once('travellog/controller/Class.CurrentTravelerController.php');
 				$obj = new CurrentTravelerController;
 				$obj->setViewType(6);
 				break;
 			case TravelerViewType::$LATEST:
 				require_once('travellog/controller/Class.LatestTravelerController.php');
 				$obj = new LatestTravelerController;
 				$obj->setViewType(5);
 				break;
 		}
 		
 		return $obj;
 	}
}
?>
