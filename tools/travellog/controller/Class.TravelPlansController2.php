<?php
require_once('travellog/model/Class.TravelSchedule.php');
require_once('travellog/model/Class.TravelPlan.php');
require_once('travellog/model/Class.TravelDestination.php');

class TravelPlansController2 {
	private $options = array();
	private $result = null;
	public $response = null;
	private $action = 'default';
	
	public function __construct($options = array()) {
		foreach ($options as $key => $value) {
			$this->options[$key] = $value;			
		}
		
		$this->transformRequestParameters();
		$this->{$this->action}();
		$this->createResponse();
	}

	private function transformRequestParameters() {
		if (isset($_REQUEST['action'])) {
			switch ($_REQUEST['action']) {
				case 'editCountryList':
					if (count($_POST['sel_countries'])) {
						$countryIDs = array();
						foreach ($_POST['sel_countries'] as $countryID) {
							$countryIDs[] = $countryID;
						}
						$this->options['countryIDs'] = $countryIDs;
					}					
					
					$this->action = 'editCountryList';
					
					break;
			}
		}
	}

	private function createResponse($action = null) {
		$action = $action || $this->action;
		switch ($action) {
			case 'editCountryList':
				$countryList = $this->file_factory->invokeStaticClass('TravelSchedule', 'getCountriesToTravel', array($options['travelerID']));
				$countries = $this->parseToXML(join(', ', $countryList['countryNames']));
				$numCountries = count($countryList);
				
				$json = "{
					countries : '{$countries}',
					numCountries : '{$numCountries}'
				}";			
			
				echo $json;
				
				break;
		}
	}	
	
	public function addOptions($options) {//sets or appends to $options array
	    foreach ($options as $key => $value) {
      		$this->options[$key] = $value;
		} 
	}
	
	public function editCountryList() {
		TravelSchedule::editCountriesToTravel($options['countryIDs'], $options['travelerID']);
	}
	
	private function parseToXML($htmlStr) {
  		$xmlStr = str_replace("&",'&amp;',$htmlStr);
	  	$xmlStr = str_replace('<','&lt;',$xmlStr);
	  	$xmlStr = str_replace('>','&gt;',$xmlStr);
	  	$xmlStr = str_replace('"','&quot;',$xmlStr);
	  	$xmlStr = str_replace("'",'&#39;',$xmlStr);
	  	
	  	return $xmlStr;
	}
}
?>