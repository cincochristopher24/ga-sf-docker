<?php
ini_set('output_buffering', 1);
require_once("travellog/model/Class.SessionManager.php");
require_once("Class.ToolMan.php");
require_once("travellog/controller/Class.IController.php");
require_once("travellog/factory/Class.FileFactory.php");
//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
abstract class AbstractGroupEventController implements IController{
	
	protected static $REQUIRES_ACTION_RULES = array('ADD', 'SAVE', 'EDIT', 'DELETE', 'REMOVE', 'MYJOURNALS' );
	
	protected  
	
	$obj_session  = NULL,
	
	$file_factory = NULL,
	
	$isAdmin = false,
	
	$isStaff = false,
	
	$powerful = false;
	
	function __construct(){
		$this->obj_session  = SessionManager::getInstance();
		$this->file_factory = FileFactory::getInstance();   
		$this->file_factory->registerClass('GroupEvent');
		$this->file_factory->registerClass('Calendar');
		$this->file_factory->registerClass('Traveler');
		$this->file_factory->registerClass('GroupFactory');
		$this->file_factory->registerClass('SubNavigation');
		$this->file_factory->registerClass('Paging'         , array('path' => ''));
		$this->file_factory->registerClass('ObjectIterator' , array('path' => ''));
		$this->file_factory->registerClass('Template'       , array('path' => ''));
		$this->file_factory->registerClass('HtmlHelpers'    , array('path' => ''));
		$this->file_factory->registerClass('GaDateTime'       , array('path' => ''));
		$this->file_factory->registerClass('ActivityController', array('path' => 'travellog/controller/'  , 'file' => 'GANETActivityController'));
		$this->file_factory->registerTemplate('LayoutMain');
		$this->file_factory->registerTemplate('Calendar');
		$this->file_factory->registerTemplate('FrmGroupEvent'); 
		$this->file_factory->registerTemplate('Event');
		$this->file_factory->registerTemplate('AdminGroupCalendar');
		$this->file_factory->registerTemplate('AdminGroupEventLists');
		$this->file_factory->registerTemplate('AdminGroupEventContent');
		
		/*****************************************************
		 * added by neri: 11-06-08
		 * registers the class used by the profile component 
		 *****************************************************/
		 
		$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
		$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
		
		$this->file_factory->setPath('CSS', '/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
	}
	
	function performAction(){
		$this->beforePerformAction();
		switch( strtolower($this->data['action']) ){
			case "create":
				$this->__getData();
				$this->_viewAddForm();
			break;
			
			case "save":
				$this->__validateForm();
				$this->_save();
			break;
			
			case "groupeventlists":
				$this->_viewGroupEventLists();
			break;
			
			case "eventdescription":
				$this->_viewEntryDescription();
			break;
			
			case "remove":
				$this->_remove();
			break;
			
			case "delete":
				$this->_delete();
			break;
			
			case "edit":
				if( count($_POST) && $this->data['eID'] ){
					$this->__getData();
					$this->_viewAddForm();
				}
				else $this->_viewGroupEvents();
			break;
			case "add":
			default:
				$this->_viewGroupEvents();
		}
	}	
	
	protected function _save(){
		
		if( $this->data['eID'] ){
			$obj_event = $this->file_factory->getClass('GroupEvent', array($this->data['eID']));
			$obj_event->setTitle         ( $this->data['title']       );
			$obj_event->setTheDate       ( $this->data['event_date'] . ' ' . $this->data['event_time']  );			
			$obj_event->setDescription   ( $this->data['description'] );
			$obj_event->setDisplayPublic ( $this->data['display']     ); 
			$obj_event->setTimeZoneID	 ( $this->data['timezone']	  );
			$obj_event->Update();
		}
		else{
			$obj_event = $this->file_factory->getClass('GroupEvent');
			$obj_event->setGroupID       ( $this->data['gID']         );
			$obj_event->setTitle         ( $this->data['title']       );
			$obj_event->setTheDate       ( $this->data['event_date'] . ' ' . $this->data['event_time']  );			
			$obj_event->setDescription   ( $this->data['description'] );
			$obj_event->setDisplayPublic ( $this->data['display']     );
			$obj_event->setTimeZoneID	 ( $this->data['timezone']	  );
			$obj_event->Create();
			
			// added by chris: send instant notification
			require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
			RealTimeNotifier::getInstantNotification(RealTimeNotifier::POST_EVENT, 
				array(
					'EVENT' => $obj_event
				))->send();
			// end add
		}	
	} 
	
	protected function _viewAddForm(){
		$obj_template              = $this->file_factory->getClass('Template');
		$obj_inc_template          = $this->file_factory->getClass('Template');
		
		$this->data['group'] = $this->__getAdminGroup();
		
		// added by chris - Jan. 21, 2009
		require_once('Class.TimeZone.php');
		$this->data['groupedTimezones'] = TimeZone::getGrouped();
		
		$obj_inc_template->set('props', $this->data);
		$obj_template->set('form' , $obj_inc_template->fetch($this->file_factory->getTemplate('FrmGroupEvent')) );
		$obj_template->set('props', $this->data);
		
		Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );
		
		echo $obj_template->fetch( $this->file_factory->getTemplate('Event') ); 
	}
	
	protected function _delete(){
		$obj_event = $this->file_factory->getClass('GroupEvent', array($this->data['eID']));
		$this->data['gID'] = $obj_event->getGroupID();
		$obj_event->Delete();
	}
	
	protected function _remove(){
		$this->_delete();
		$link = $this->data['from_where'];
		$qStr = '';
		if($this->data['from_where'] == 'group')
			$qStr = '?gID='.$this->data['gID'];
		ToolMan::redirect("/$link.php$qStr");       
		exit; 
	}
	
	protected function _viewEntryDescription(){
		$obj_template                  = $this->file_factory->getClass('Template');
		$this->data['obj_group_event'] = $this->file_factory->getClass('GroupEvent', array($this->data['eID']));
		$this->data['gID']             = $this->data['obj_group_event']->getGroupID();
		$obj_group                     = $this->__getAdminGroup();    
		$this->data['is_owner']        = ($obj_group->getAdministratorID() == $this->obj_session->get('travelerID') || $obj_group->isStaff($this->obj_session->get('travelerID'))) ? true: false; 
		$this->data['is_login']        = $this->obj_session->getLogin();
		$this->data['is_owner']        = ("TRAVELER" == $this->data['subject']) ? false : $this->data['is_owner']; 
		// convert time to local time
		require_once('Class.TimeZone.php');
		$converted = TimeZone::convertTimeToRemoteTime($this->data['obj_group_event']->getTheDate(), $this->data['obj_group_event']->getTimeZone(), $this->data['offset'], "%Y-%m-%d %H:%M:%S");
		$this->data['local_datetime'] = $converted['dateTime'];
		$this->data['local_timezone'] = $converted['timezone'];
		
		$obj_template->set( 'props', $this->data );       
		
		echo $obj_template->fetch($this->file_factory->getTemplate('AdminGroupEventContent'));
	}
	
	protected function _viewGroupEvents(){
		$this->__getData();
		$obj_group                = $this->__getAdminGroup();
		$obj_navigation           = $this->file_factory->getClass('SubNavigation');
		$obj_template             = $this->file_factory->getClass('Template');
		$this->data['event_type'] = 'Calendar';
		
		$obj_navigation->setContext('GROUP');
		$obj_navigation->setContextID($this->data['gID']);
		$obj_navigation->setLinkToHighlight('EVENTS');
		
		if ( strcasecmp( get_class( $obj_group ),'FunGroup') == 0 )
			$obj_navigation->setGroupType('FUN_GROUP');
		else
			$obj_navigation->setGroupType('ADMIN_GROUP'); 
				
		$obj_template->set( 'props'            , $this->data                             );
		$obj_template->set( 'group_event_lists', $this->__getGroupEventLists($obj_group) );
		$obj_template->set( 'event_type'       , 'Calendar'                              );
		$obj_template->set( 'subNavigation'    , $obj_navigation                         );	
		
		$gID     = $this->data['gID'];
		//$tID     = $obj_group->getAdministratorID();
		$tID = $this->obj_session->get('travelerID');
		$eventID = ( (strtolower($this->data['action']) == 'edit' || strtolower($this->data['action']) == 'view') && isset($this->data['eID']) && $this->data['eID'] )? $this->data['eID']: 0;
		$action  = strtolower($this->data['action']); 
		
		/****************************************************
		 * edits by neri: 11-04-08
		 * added code for displaying the profile component
		 ****************************************************/
		
		$profile_comp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
		$profile_comp->init($gID);
		$obj_template->set('profile', $profile_comp->get_view()							);
		
		$code = <<<BOF
<script type="text/javascript">   
//<![CDATA[
		gID     = $gID;
		tID     = $tID;
		eventID = $eventID;   
		action  = "$action"; 
		subject = "GROUP";
//]]>  
</script>
BOF;

		Template::includeDependent  ( $code                          ); 
		Template::includeDependentCss( '/min/f=css/styles.css'   		     ); 
		//Template::setMainTemplateVar( 'page_location', 'My Passport' );
		//Template::setMainTemplateVar( 'page_location', 'Groups / Clubs' );
		if( isset($GLOBALS['CONFIG']) )
			Template::setMainTemplateVar('title', $GLOBALS['CONFIG']->getSiteName() . ' Events');
		else
			Template::setMainTemplateVar('title', 'Events');	
		Template::setMainTemplate   ( $this->file_factory->getTemplate('LayoutMain') );
		
		$obj_template->out($this->file_factory->getTemplate('AdminGroupCalendar'));
	}
	
	protected function _viewGroupEventLists(){
		$obj_group = $this->__getAdminGroup();
		echo $this->__getGroupEventLists($obj_group);
	}
	
	protected function __getGroupEventLists( $obj_group ){ 
		$this->__hasPermissions($obj_group);
		$rows_per_page        = 5;
		$obj_template         = $this->file_factory->getClass('Template');
		
		$startdate = (!$this->powerful) ? date('Y-m-d') : NULL;
		
		if( strcasecmp(get_class($obj_group), 'AdminGroup') == 0 ){ 
			$col_events       = $this->__filter($obj_group->getCalendarActivities(NULL,$startdate));
			// $col_events       = $this->__filter($obj_group->getCalendarActivities());
		}
		else{
			$obj_calendar     = $this->file_factory->getClass('Calendar', array($obj_group));
			if( $this->obj_session->getLogin() ){
				$obj_traveler = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')));
				$obj_calendar->setPrivacyContext($obj_traveler);
			}
			$col_events       = $this->__filter($obj_calendar->getEvents(NULL,$startdate));        
			// $col_events       = $this->__filter($obj_calendar->getEvents(NULL,$startdate));        
		}
		
		if(!count($col_events) && !$this->powerful)
			header('Location: http://'.$_SERVER['SERVER_NAME'].'/group.php?gID='.$obj_group->getGroupID());
		
		$this->data['powerful'] = $this->powerful;
		$this->data['is_owner'] = ( $obj_group->getAdministratorID() == $this->obj_session->get('travelerID') )? true: false; 
		$this->data['page']     = ( isset($this->data['page']) )? $this->data['page']: 1;
		$this->data['eID']      = ( isset($this->data['eID'])  )? $this->data['eID'] : 0; 
		
		if( $this->data['eID'] ) $this->data['page'] = $this->__checkPosition($col_events, $rows_per_page);
		
		if( count($col_events) ){  
			$obj_paging   = $this->file_factory->getClass( 'Paging', array(count($col_events),$this->data['page'], 'action=GroupEventLists&gID='.$this->data['gID'], $rows_per_page) );
			$obj_iterator = $this->file_factory->getClass( 'ObjectIterator', array($col_events, $obj_paging) );
			$obj_template->set( 'obj_iterator' , $obj_iterator   );
			$obj_template->set( 'obj_paging'   , $obj_paging     );
			$obj_paging->setOnclick('jQuery(this).nextNprev');
		}
		$obj_template->set( 'props', $this->data );
		
		return $obj_template->fetch($this->file_factory->getTemplate('AdminGroupEventLists'));
	}
	
	protected function __filter($col_events){
		$temp_events = array();
		if( !$this->obj_session->getLogin() && count($col_events) ){
			foreach ( $col_events as $obj_event){
				if ( $obj_event->getDisplayPublic() == 1 ) $temp_events[] = $obj_event; 
			}
		}
		elseif( count($col_events) ){
			$temp_events = $col_events;
		}
		
		return $temp_events;
	}
	
	protected function __getAdminGroup(){
		$obj_group_factory = $this->file_factory->invokeStaticClass('GroupFactory');
		$obj_group         = $obj_group_factory->create(array($this->data['gID']));
		return $obj_group[0];	
	}
	
	protected function __checkPosition( $col_events, $rowsperpage ){
		$page = 1;
		for($ctr=0; $ctr < count($col_events); $ctr++ ){
			if ( $col_events[$ctr]->getEventID() == $this->data['eID'] ){
				$page = ceil(($ctr+1)/$rowsperpage);
				break;
			}
		}  
		return $page;
	}
	
	protected function __getData(){
		require_once('Class.GaDateTime.php');
		if( array_key_exists('eID', $this->data) && $this->data['eID'] && strtolower($this->data['action']) != 'save' ){
			$obj_event               = $this->file_factory->getClass('GroupEvent', array($this->data['eID']));
			$this->data['title']       = $obj_event->getTitle();
			$this->data['description'] = $obj_event->getDescription();
			$this->data['timezone']	   = $obj_event->getTimeZoneID();
			$this->data['display']     = $obj_event->getDisplayPublic();
			$this->data['gID']         = $obj_event->getGroupID();
			$this->data['event_date']  = $obj_event->getTheDate();
			$this->data['event_time']  = date('h:i A', strtotime($this->data['event_date']));
			$this->data['event_date']  = GaDateTime::dbDateFormat(strtotime($this->data['event_date']));
			$this->data['errors']      = array();
			$this->data['mode']        = 'edit';
		}
		else{
			$this->data['eID']         = ( isset($this->data['eID'])         )? $this->data['eID']        : 0;
			$this->data['title']       = ( isset($this->data['title'])       )? $this->data['title']      : '';
			$this->data['description'] = ( isset($this->data['description']) )? $this->data['description']: '';
			$this->data['display']     = ( isset($this->data['display'])     )? $this->data['display']    : 1;
			$this->data['gID']         = ( isset($this->data['gID'])         )? $this->data['gID']        : 0;
			$this->data['event_date']  = ( isset($this->data['event_date'])  )? $this->data['event_date'] : GaDateTime::dbDateTimeFormat();
			$this->data['event_time']  = date('h:i A', strtotime( isset($this->data['event_time']) ? $this->data['event_time'] : $this->data['event_date'] ));
			$this->data['event_date']  = GaDateTime::dbDateFormat(strtotime($this->data['event_date']));
			$this->data['timezone']    = ( isset($this->data['timezone'])    )? $this->data['timezone']   : 0;
			$this->data['errors']      = array();
			$this->data['mode']        = 'add';
		}	
	}
	
	protected function __applyRules(){ 
		if( in_array( strtoupper($this->data['action']), AbstractGroupEventController::$REQUIRES_ACTION_RULES) ){
			// TODO: If some actions requires a more complex rules use a rule object or method instead
			switch( strtolower($this->data['action']) ){
				case 'add':	
					if( !$this->obj_session->getLogin() ){
						ToolMan::redirect('/group.php?gID='.$this->data['gID']);
						exit;
					}
				break; 
				
				case 'save':
					if( !isset($_POST['action']) ){	
						echo '<h2>Requires a post method.</h2>';
						exit;
					}	
				break;
				case 'remove':
					if( !$this->obj_session->getLogin() ){
						ToolMan::redirect('/index.php');
						exit;
					}
					$link = $this->data['from_where']; 
					if( !isset($this->data['eID']) ){
						ToolMan::redirect('/'.$link.'.php'); 
						exit; 
					}
					$obj_event         = $this->file_factory->getClass('GroupEvent', array($this->data['eID']));
					$obj_group_factory = $this->file_factory->invokeStaticClass('GroupFactory');
					$obj_group         = $obj_group_factory->create(array($obj_event->getGroupID()));
					if(!($obj_group[0]->getAdministratorID() == $this->obj_session->get('travelerID') || $obj_group[0]->isStaff($this->obj_session->get('travelerID')))){
						ToolMan::redirect('/'.$link.'.php');
						exit;
					}	
				break;	
				case 'delete':	
				case 'edit':
					if( !$this->obj_session->getLogin() ){
						//ToolMan::redirect('/group.php?gID='.$this->data['gID']); 
						ToolMan::redirect('/login.php'); 
					}
					if( !isset($this->data['eID']) ){
						echo '<h2>Invalid eventID!!!</h2>';
						exit;
					}
					$obj_event         = $this->file_factory->getClass('GroupEvent', array($this->data['eID']));
					$obj_group_factory = $this->file_factory->invokeStaticClass('GroupFactory');
					$obj_group         = $obj_group_factory->create(array($obj_event->getGroupID()));
					if(!($obj_group[0]->getAdministratorID() == $this->obj_session->get('travelerID') || $obj_group[0]->isStaff($this->obj_session->get('travelerID')))){
						echo '<h2>You are not allowed to '. $this->data['action'] .' this event.</h2>';
						exit;
					}	
				case 'create':
					if( !$this->obj_session->getLogin() ){
						echo '<h2>You are not allowed to add.</h2>';
						exit;
					}
				break; 
			}
		}
	}
	
	protected function __hasPermissions($obj_group){
		if($obj_group instanceOf AdminGroup){
		
			$this->isAdmin = ($this->obj_session->get('travelerID') == $obj_group->getAdministrator()->getTravelerID()) ? true : false;
			$this->isStaff = $obj_group->isStaff($this->obj_session->get('travelerID'));

			if($this->isAdmin || $this->isStaff)
				$this->powerful = true;
		}else if($obj_group instanceOf FunGroup)
			$this->powerful = ($this->obj_session->get('travelerID') == $obj_group->getAdministrator()->getTravelerID()) ? true : false;
	}
	
	protected function __validateForm(){
		$this->__getData(); 
		$errors = array();
		$date   = explode('-', $this->data['event_date']);
		if( !preg_match('/.+/', $this->data['title']) ){
			$errors[] = 'Title is a required field!';
		}
		if( !preg_match('/.+/', $this->data['description']) ){
			$errors[] = 'Description is a required field!';
		}
		if( !preg_match('/.+/', $this->data['event_date']) ){ 
			$errors[] = 'Date is a required field!';
		}
		elseif( !checkdate($date[1],$date[2], $date[0]) ){
			$errors[] = 'Date is invalid!';
		}
		
		$this->data['errors'] = $errors;
		
		if( count($this->data['errors']) ){ 
			$this->_viewAddForm();
			exit;	
		}
	} 
	
	protected function beforePerformAction(){
		if( $this->obj_session->getLogin() ){
			require_once('gaLogs/Class.GaLogger.php');
			GaLogger::create()->start($this->obj_session->get('travelerID'),'TRAVELER');  
		}
	}
}
?>
