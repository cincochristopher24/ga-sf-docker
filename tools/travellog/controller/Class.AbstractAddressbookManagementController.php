<?php
	require_once("travellog/model/Class.SessionManager.php");
	require_once('travellog/factory/Class.FileFactory.php');
	require_once('class.phpmailer.php');
	//require_once('JSON.php');
	
	abstract class AbstractAddressbookManagementController
	{
		protected $file_factory 	= NULL;
		protected $obj_session  	= NULL;
		protected $loggedTraveler 	= NULL;
		protected $mIsModerator		= false;
		protected $mMethod			= NULL;
		protected $mAddressbook		= NULL;
		protected $mGroups			= array();
		protected $mGroupID			= 0;
		protected $mGroup 			= null;
		protected $mOwnerID			= 0;
		protected $pageTitle		= 'My Address Book';
		
		public function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/views'));
			
			$this->obj_session  = SessionManager::getInstance();
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			$this->file_factory->registerClass('ToolMan', array('path'=>''));
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('AdminGroup');
			$this->file_factory->registerClass('HelperGlobal', array('path'=>''));
			$this->file_factory->registerClass('Template', array('path'=>''));
			$this->file_factory->registerClass('AddressBook');
			$this->file_factory->registerClass('AddressBookEntry');
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('HelpText');
			
			// added by chris: Jan. 26, 2009
			$this->file_factory->registerClass('TravelerAddressBook');
			$this->file_factory->registerClass('GroupAddressBook');
			// end add
			
			$this->file_factory->registerClass('Paging', array('path'=>''));
			$this->file_factory->registerClass('ObjectIterator', array('path'=>''));
			
			$this->file_factory->registerClass('AddressBookEntryPreference');
			$this->file_factory->registerClass('TravelerProfile');
			$this->file_factory->registerClass('AdminGroup');
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('ConfirmationData');
			$this->file_factory->registerClass('Crypt', array('path'=>''));
			$this->file_factory->registerClass('GaString', array('path'=>''));
			
			/************************************************************
			 * includes the classes used by the profile component
			 * added by neri: 11-04-08
			 ************************************************************/
			
			$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
			
			//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
			
		}
		
		
		public function performAction(){
			$travelerID = $this->obj_session->get('travelerID');
			if(!$travelerID){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));		
			}
			
			require_once('gaLogs/Class.GaLogger.php');
			GaLogger::create()->start($travelerID,'TRAVELER');
			
			//check if the traveler is an advisor, if so, check if he has a main group already, if non is found, redirect to index.
			try{
				$this->mLoggedTraveler = $this->file_factory->getClass('Traveler',array($travelerID));
			}
			catch(exception $e){
				if(array_key_exists('debug',$_GET)){
					var_dump($e);
					exit;
				}
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));		
			}
			
			// EDITED by chris:
			if( array_key_exists('gID', $_GET) ){
				//check if the gID is a valid groupID
				$oGroupFactory = $this->file_factory->invokeStaticClass('GroupFactory','instance');
				try{
					$groups = $oGroupFactory->create(array($_GET['gID']));
					$oGroup = count($groups) ? $groups[0] : null;
				}
				catch(exception $e){
					$oGroup = null;
				}
				if( is_null($oGroup) ){
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
				}
				$this->mGroup 	= $oGroup;
				$this->mGroupID = $this->mGroup->getGroupID();
				
				$this->mIsModerator = $this->mGroup->getAdministratorID() == $this->mLoggedTraveler->getTravelerID()
					|| $this->mGroup->isSuperStaff($this->mLoggedTraveler->getTravelerID())
					|| $this->mGroup->isStaff($this->mLoggedTraveler->getTravelerID());
				if( !$this->mIsModerator )
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			/*elseif( $this->mLoggedTraveler->isAdvisor() ){
				$this->mIsModerator = true;
				$this->mGroupID		= $this->mLoggedTraveler->getAdvisorGroup();
				//$this->mGroupID 	= $this->mGroup->getGroupID();
			}*/
			$this->mOwnerID = $this->mLoggedTraveler->getTravelerID();
			// END EDIT
			
			if(!is_array($this->mGroups)){
				$this->mGroups = array();
			}
			$this->mMethod = (isset($_GET['action']))?$_GET['action']:'view';
			//$this->mAddressBook = $this->file_factory->getClass('AddressBook',array($this->mOwnerID));
			// check context
			$this->mAddressBook = $this->mGroupID 
				? $this->file_factory->getClass('GroupAddressBook',array($this->mGroupID))
				: $this->file_factory->getClass('TravelerAddressBook',array($this->mOwnerID));;
			$this->{$this->mMethod}();
			GaLogger::create()->performShutdown();
		}
		
		public function view($vars=array()){
			$allEntries =  $this->mAddressBook->getAddressBookEntries();
			$totalRecords = count($allEntries);
			$page = (isset($_GET['page']))?$page = $_GET['page']:$page=1;		
	 		$qryString = "method=view";
	 		$entries = array();
			foreach($allEntries as $entry){
				$entries[] = $entry;
			}
			
			$paging =  $this->file_factory->getClass('Paging',array($totalRecords,$page,$qryString,100));
			$iterator = $this->file_factory->getClass('ObjectIterator',array($entries,$paging));
			$this->showTemplate(array_merge(
				array(
					'numRec' 		=> $totalRecords,
					'paging' 		=> $paging,
					'iterator'		=> $iterator,
					'allEntries'	=> $allEntries,
				),
				$vars	
			));
		}
		
		
		public function save(){
			if($_POST['submit'] == 'Cancel'){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/addressbookmanagement.php'.($this->mIsModerator?'?gID='.$this->mGroupID:'')));
			}
			$_POST['txtEmail'] = trim($_POST['txtEmail']);
			//check if the email address is valid.
			$errCodes 	= array();
			$notice 	= ''; 
			if(!$this->file_factory->invokeStaticClass('ToolMan','isValidEmailAddress',array($_POST['txtEmail']))){
				$errCodes['EMAIL_ADDRESS'] = 'Please enter a valid email address.';
			}
			if(!strlen($_POST["txtName"])){
				$errCodes['NAME'] = 'Please fill out the name box.';
			}
			$post_entryID = strlen($_POST["hidAddressBookEntryId"])?$_POST["hidAddressBookEntryId"]:null;
			
			$uniqueEmail = 0 < $this->mGroupID 
				? $this->file_factory->invokeStaticClass('AddressBookEntry','isUniqueEmailAddressForGroup',array($this->mGroupID,trim($_POST["txtEmail"]),$post_entryID))
				: $this->file_factory->invokeStaticClass('AddressBookEntry','isUniqueEmailAddressForTraveler',array($this->mLoggedTraveler->getTravelerID(),trim($_POST["txtEmail"]),$post_entryID));
			
			if( !$uniqueEmail ){
				$errCodes['EMAIL_ADDRESS'] = 'Email address already exists in your address book.';
			}
			
			$addressBookEntry = $this->file_factory->getClass('AddressBookEntry',array($post_entryID));
			
			$sendConfirmation = false;
			// edited by chris: we will no longer send confirmation email since notify choice is commented out			
			if(null==$post_entryID){//adding
				/*if($_POST["radIsNotable"]){
					$sendConfirmation = true;
				}*/
			}
			else{//editing
				//if the isNotable has been change from no to yes OR if the email address has been changed and the existing isnotable is TRUE
				/*if( ( !$addressBookEntry->isNotable() && $_POST["radIsNotable"] ) || ( 0!=strcasecmp($addressBookEntry->getEmailAddress(),trim($_POST["txtEmail"])) ) ){
					$sendConfirmation = true;
				}*/
			}
			
			$addressBookEntry->setEmailAddress($_POST["txtEmail"]);
			$addressBookEntry->setName($_POST["txtName"]);
			$addressBookEntry->setDetails($_POST["txaDetails"]);
			//$addressBookEntry->setIsNotable($_POST["radIsNotable"]);						
			$addressBookEntry->setIsNotable(0);
			$addressBookEntry->setOwnerTravelerID($this->mOwnerID);
			$addressBookEntry->setOwnerGroupID($this->mGroupID); // added by chris: Jan. 26, 2009
			
			if(!count($errCodes)){
					
					if(!is_null($post_entryID)){
						$notice = 'Your address book entry was successfully updated.<br />';
					}
					else{
						$notice = 'Your address book entry was successfully added.<br />';
					}
					
					if(!$sendConfirmation){
						$addressBookEntry->save();
					}
					else{
						$addressBookEntry->setStatus(AddressBookEntry::PENDING);
						$addressBookEntry->save();
					}
					//work with the entry preferences
					//proceed only if the entry is notable
					
					// edited by chris: notify choice is already commented so no need to proceed		
					//if($_POST["radIsNotable"]){
					if(false){
						//loop through the preference checkboxes
						foreach($this->mGroups as $iGroup){						
							$isWithPhoto = false;
							$isWithJournal = false;
							//check if the groupID exists in the JOURNAL Post variable
							if(array_key_exists('chkAddJournal',$_POST) && in_array($iGroup->getGroupID(),$_POST['chkAddJournal'])){
								$isWithJournal = true;
							}
							//check if the groupID exists in the PHOTO post variable
							if(array_key_exists('chkAddPhoto',$_POST) && in_array($iGroup->getGroupID(),$_POST['chkAddPhoto'])){
								$isWithPhoto = true;
							}
							
							//$pref = AddressBookEntryPreference::getPreferenceByAddressBookEntryIDAndDoerTypAndDoer($addressBookEntry->getAddressBookEntryID(), AddressBookEntryPreference::GROUP, $iGroup->getGroupID());				
							$pref = $this->file_factory->invokeStaticClass('AddressBookEntryPreference','getPreferenceByAddressBookEntryIDAndDoerTypAndDoer',array($addressBookEntry->getAddressBookEntryID(),AddressBookEntryPreference::GROUP,$iGroup->getGroupID()));
							
							if(!is_null($pref)){
								if(!$isWithPhoto && !$isWithJournal){
									$pref->delete();
								}
								else{
									$pref->notifyOnAddPhoto($isWithPhoto);
									$pref->notifyOnAddJournal($isWithJournal);
								}
							}
							else{
								if($isWithPhoto || $isWithJournal){
									$pref = $this->file_factory->getClass('AddressBookEntryPreference');
									$pref->setAddressBookEntryID($addressBookEntry->getAddressBookEntryID());
									$pref->setDoerAsGroup($iGroup->getGroupID());
									$pref->save();
									if($isWithPhoto){
										$pref->notifyOnAddPhoto(true);
									}
									if($isWithJournal){
										$pref->notifyOnAddJournal(true);
									}
								}
							}
						}
					
				}
				else{
						//clear the preferences of this entry
						$addressBookEntry->clearPreferences();
						
				}
				
				if($sendConfirmation){
					$newEntryID = $addressBookEntry->getAddressBookEntryID();
					$succ = $this->sendConfirmationEmail($this->mLoggedTraveler->getTravelerID(),$newEntryID);
					if($succ){
						$notice .= 'A confimation email was sent to the email address of the addressbook entry that you have just '.(is_null($post_entryID)?'added':'updated').'.';
					}
					else{
						$notice .= 'But we are sorry because we were unable to send a confirmation email to the address that you stated on the addressbook entry.';
					}
				}
				$this->view(array(
					'crtlAction' 		=> 'saving',
					'sendConfirmation'	=> $sendConfirmation,
					'action'			=> (null==$post_entryID)?'ADD':'EDIT',
					'method'			=> 'view',
					'notice'			=> $notice
				));
			}
			else{
				$this->showTemplate(array(
					'method' 			=> (is_null($post_entryID))?'add':'edit',
					'addressBookEntry'	=> $addressBookEntry,
					'errCodes'			=> $errCodes,
					'notice'			=> $notice
				));
			}
			
		}
		
		/***
		## Commented since there is no import addressbook feature 
		
		public function saveList(){
			$json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
			$entries = $json->decode(stripslashes(urldecode( $_POST['entryList'] )));
			$entryList = array();
			foreach($entries as $iEntry){
				$summaryEntry = array();
				$summaryEntry['name'] = $iEntry['0'];
				$summaryEntry['emailAdd'] = $iEntry['1'];
				
				if(!strlen(trim($iEntry['0']))){
					$summaryEntry['status'] = 'Failed! Invalid name.';
				}
				if(!ToolMan::isValidEmailAddress($iEntry['1'])){
					$summaryEntry['status'] = 'Failed! Invalid emailAddress.'	;
				}
				elseif(!AddressBookEntry::isUniqueEmailAddressForTraveler($travelerID,trim($iEntry['1']))){
					$summaryEntry['status'] = 'Failed! Email address already exists in your address book.';
				}
				else{	
					$newEntry = new AddressBookEntry();
					$newEntry->setName($iEntry['0']);
					$newEntry->setEmailAddress($iEntry['1']);
					$newEntry->setOwnerTravelerID($travelerID);
					$newEntry->save();
					$summaryEntry['status'] = 'Successful';
				}
				$entryList[] = $summaryEntry;
			}
			$notice = 'Addressbook importing is done.';
			$tplAddressBookManagement->set('entryList',$entryList);
			$method='showListSummary';
		}
		***/
		
		public function edit(){
			//initialize address book entry with the addressbook entry id
			if(!isset($_GET['eID'])) {
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/addressbookmanagement.php'.($this->mIsModerator?'?gID='.$this->mGroupID:'')));
			}
			//make sure that the logged traveler is editing his/her own entry
			/***
			if(!$this->file_factory->invokeStaticClass('AddressBookEntry','travelerHasEntry',array($this->mLoggedTraveler->getTravelerID(),$_GET['eID']))){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/addressbookmanagement.php'));
			}
			***/
			//group the preferences by groupID
			$addressBookEntry = $this->file_factory->getClass('AddressBookEntry',array($_GET['eID']));
			
			if($addressBookEntry->getOwnerTravelerID() != $this->mOwnerID && !($this->mGroupID && $this->mIsModerator) ){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/addressbookmanagement.php'.($this->mIsModerator?'?gID='.$this->mGroupID:'')));
			}
			
			$prefs = $addressBookEntry->getPreferences();
			$mappedPrefs = array();
			foreach($prefs as $iPref){
				$mappedPrefs[$iPref->getDoerID()] = $iPref;
			}
			$this->showTemplate(array(
				'prefs' 			=> $mappedPrefs,
				'addressBookEntry' 	=> $addressBookEntry
			));
		}
		
		public function add(){
			$addressBookEntry = $this->file_factory->getClass('AddressBookEntry');
			$this->showTemplate(array(
				'addressBookEntry' 	=> $addressBookEntry
			));
		}
		
		public function delete(){
			if(!isset($_POST['chkAddressBookEntry']) && !isset($_GET['eID'])){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/addressbookmanagement.php'.($this->mIsModerator?'?gID='.$this->mGroupID:'')));
			}
			//make sure that the logged traveler is deleting his/her entries
			$arAddrBookEntry = array();
			if(isset($_GET['eID'])){				
				/***
				if(!$this->file_factory->invokeStaticClass('AddressBookEntry','travelerHasEntry',array($this->mLoggedTraveler->getTravelerID(),$_GET['eID']))){
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/addressbookmanagement.php'));
				}
				$arAddrBookEntry[] = $_GET['eID'];
				***/
				$addressBookEntry = $this->file_factory->getClass('AddressBookEntry',array($_GET['eID']));
				if($addressBookEntry->getOwnerTravelerID() != $this->mOwnerID && !($this->mGroupID && $this->mIsModerator) ){
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/addressbookmanagement.php'.($this->mIsModerator?'?gID='.$this->mGroupID:'')));
				}
				$addressBookEntry->delete();
			}
			/***
			foreach($arAddrBookEntry as $entryId){
				$curAddressBookEntry = $this->file_factory->getClass('AddressBookEntry',array($entryId));
				$curAddressBookEntry->delete();
			}
			***/
			$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/addressbookmanagement.php'.($this->mIsModerator?'?gID='.$this->mGroupID:'')));
		}
		
		public function showTemplate($tplVars=array()){
			/*****************************************************
			 * edits by neri: 11-04-08
			 * added codes for displaying the profile component
			 *****************************************************/
			
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			$tplAddressBookManagement = $this->file_factory->getClass('Template');
			$subNavigation = $this->file_factory->getClass('SubNavigation');
			if($this->mIsModerator){
				$subNavigation->setContext('GROUP');
				$subNavigation->setContextID($this->mGroupID);
				$subNavigation->setGroupType('ADMIN_GROUP');
				$profile_comp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
				$profile_comp->init($this->mGroupID);
			}
			else{	
				$subNavigation->setContext('TRAVELER');
				$subNavigation->setContextID($this->mLoggedTraveler->getTravelerID());
				$profile_comp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$TRAVELER_CONTEXT );
				$profile_comp->init($this->obj_session->get('travelerID'));
			}
			$subNavigation->setLinkToHighlight('ADDRESS_BOOK');
			$tplAddressBookManagement->set('subNavigation',$subNavigation);
			
			$helpText = $this->file_factory->getClass('HelpText');
	
			$tplAddressBookManagement->set('groups',$this->mGroups);
			$tplAddressBookManagement->set('helpText',$helpText);
			$tplAddressBookManagement->set('method',$this->mMethod);
			$tplAddressBookManagement->set('isModerator',$this->mIsModerator);
			$tplAddressBookManagement->set('groupID',$this->mGroupID);
			
			$errCodes  = array();
			$tplAddressBookManagement->set('errCodes',$errCodes);
			$notice = '';
			$tplAddressBookManagement->set('notice',$notice);
			foreach($tplVars as $key => $val){
				$tplAddressBookManagement->set($key,$val);
			}
			$tplAddressBookManagement->set('pageTitle',$this->pageTitle);
			$tplAddressBookManagement->set('profile',$profile_comp->get_view());
			
			$tplAddressBookManagement->out('travellog/views/tpl.AddressBookManagement.php');	
		}
		
		/**
		 * Function that sends and email confirmationcle
		 */	 
		protected function sendConfirmationEmail($travelerID, $entryID){		
			//check if the traveler is an advisor
			$addressBookEntry = $this->file_factory->getClass('AddressBookEntry',array($entryID));
			//$localTraveler = $this->mLoggedTraveler;
			
			// added by chris: use notification system to send this notification
			require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
			RealTimeNotifier::getInstantNotification(RealTimeNotifier::ADD_ADDRESSBOOK_ENTRY, 
				array(
					'ADDRESSBOOK_ENTRY'	=> $addressBookEntry,
					'LOGGED_TRAVELER'	=> $this->mLoggedTraveler,
					'IS_MODERATOR'		=> $this->mIsModerator,
					'GROUP_ID'			=> $this->mGroupID
				))->send();
			// end add
			
			/*
			if(!$this->mIsModerator){
				$travelerProfile = $this->file_factory->getClass('TravelerProfile',array($this->mLoggedTraveler->getTravelerID()));
				if (null == $travelerProfile->getGender()){
					$gender1 = 'his/her';
					$gender2 = 'him/her';
				}
				else{
					$gender1 = ('M' == $travelerProfile->getGender())?'his':'her';
					$gender2 = ('M' == $travelerProfile->getGender())?'him':'her';
				}
				$travelerUserName = ucwords($travelerProfile->getUserName());
				$travelerName = $travelerProfile->getFirstName() . ' '.$travelerProfile->getLastName().'('.$travelerUserName.')';			
				$userNameOwn = 	$this->file_factory->invokeStaticClass('GaString','makePossessive',array($travelerUserName));
				$profileLink = "http://".$_SERVER['SERVER_NAME']."/".$travelerUserName;
			}
			else{
				$gender1 = 'their';
				$gender2 = 'their';

				$gID = $this->mGroupID;
			  	$gFactory 	= $this->file_factory->invokeStaticClass('GroupFactory','instance');  				
				$oGroup  	= $gFactory->create(array($this->mGroupID));
				$travelerUserName = $oGroup[0]->getName();
				
				$travelerName = $travelerUserName;
				$userNameOwn = 	$this->file_factory->invokeStaticClass('GaString','makePossessive',array($travelerUserName));
				$profileLink = "http://".$_SERVER['SERVER_NAME'].$oGroup[0]->getFriendlyUrl();
			}
			
			$oID = $this->file_factory->invokeStaticClass('ConfirmationData','getEncryptedTravelerID',array($this->mOwnerID)); 
			$eID = $this->file_factory->invokeStaticClass('ConfirmationData','getEncryptedAddressBookEntryID',array($entryID));
			
			$crypt =  $this->file_factory->getClass('Crypt'); 
			$cryptDate = $crypt->encrypt(date("Y-m-d",time()));
			
			$addressBookconfr 	= "http://".$_SERVER['SERVER_NAME']."/confirmnotification.php?oID=$oID&amp;eID=$eID&cnf=1&amp;ds=".urlencode($cryptDate);
			$addressBookdeny 	= "http://".$_SERVER['SERVER_NAME']."/confirmnotification.php?oID=$oID&amp;eID=$eID&cnf=0&amp;ds=".urlencode($cryptDate);
			
			$emailMsg =	"Dear ".$addressBookEntry->getName()." ,\n\n" .			
						"Greetings from the GoAbroad Network and $travelerName! \n\n" .
											
						"You received this email because $travelerName added you to ". 
						"$gender1 GoAbroad Network Address Book, and would like you ".
						"to receive updates about $gender1 travel journals and photos on GoAbroad Network. \n" .
						"\n". 					
						"Goabroad Network is a social networking site dedicated to ". 
						"travelers worldwide. $travelerUserName shares $gender1 " .
						($this->mIsModerator?"organization's travel experiences ":"travels ")."on this ". 
						"website by writing travel journals".($this->mIsModerator?" or uploading photos. ":", uploading photos and sending messages to friends like you." ).
						" To view $userNameOwn GoAbroad Network profile, please click on the link below\n" .
						"\n". 					
						"$profileLink \n" .
						"\n".					
						"We would then like to ask for your permission about receiving these updates. ". 
						"If you click on the link below, you AGREE to be notified if ". 
						"$travelerUserName wrote new travel journals".($this->mIsModerator?" or uploaded new photos. ":", uploaded new photos or to receive messages from $gender2.")."\n". 				
						"\n". 					
						"$addressBookconfr\n" .
						"\n" .
						"If you prefer NOT to be notified of such updates, please click on the link below or do nothing and this request for confirmation will expire in 2 months\n" .
						"\n". 					
						"$addressBookdeny\n" .
						"\n" .
						"============================================================\n" .
						"\n".					
						"To create your own account on the GoAbroad Network, please visit \n" .
						"\n". 					
						"http://www.goabroad.net \n" .
						"\n".					
						"============================================================\n" .
						"\n".
						"Thank you and have a great day!\n" .
						"\n". 
						"The GoAbroad Network Team";
			//echo '<textarea cols="50" rows="30">'.$emailMsg.'</textarea>';
			//exit;
			$succ = true;
			*/
			/******************************************************************
			 * added by neri: 11-03-08
			 * sends only an email if the server name is not goabroad.net.local
			 ******************************************************************/
			/*if ('goabroad.net.local' != $_SERVER['SERVER_NAME']) {
				try{					
					$mail = new PHPMailer();
					$mail->IsSMTP();
					$mail->IsHTML(false);
					$mail->From = 'admin@goabroad.net';
					$mail->FromName = 'The GoAbroad Network';
					$mail->Subject  = "Please confirm to receive travel updates from $travelerName";
					$mail->Body = $emailMsg;
					$mail->AddAddress(trim($addressBookEntry->getEmailAddress()));
					$succ = $mail->Send();
				}
				catch(exception $e){
					$succ=false;
				}
			}
			return $succ;*/
		}		
	}
?>