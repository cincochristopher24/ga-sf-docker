<?php

require_once("travellog/factory/Class.FileFactory.php");

abstract class AbstractSPOAuthLoginController{
	
	protected $file_factory;
	protected $service_provider;
	
	abstract public function loginUser($travelerID=0,$oauth_api=NULL);
	
	public function __construct(){
		$this->file_factory = FileFactory::getInstance();
		
 		$this->file_factory->registerClass("SessionManager", array("path" => "travellog/model/"));
 		$this->file_factory->registerClass('Session', array('path' => 'travellog/model'));
 		$this->file_factory->registerClass("Connection", array("path" => ""));
 		$this->file_factory->registerClass("Recordset", array("path" => ""));
	}
	
	protected function startSession($travelerID=0){
 		$nConnection = $this->file_factory->getClass("Connection");
 		$nRecordset = $this->file_factory->getClass("Recordset",array($nConnection));
		
		//check if traveler is suspended
		$sql = "SELECT *
				FROM tblTraveler
				WHERE travelerID = {$travelerID} 
					AND isSuspended = 1
				LIMIT 1";
		$nRecordset->Execute($sql);
		//redirect to homepage if suspended
		if( $nRecordset->Recordcount() ){
			return FALSE;
		}
		
		/** 
		* update database info for user
		* set session variables
		*/
		$appendQuery = ", active = 1 ";
		
		$sqlquery = "UPDATE tblTraveler set latestlogin='" . date('c') . "' " . $appendQuery . " WHERE travelerID = " . $travelerID;
		$nRecordset->Execute($sqlquery);
		
		$sessionManager = $this->file_factory->invokeStaticClass("SessionManager","getInstance");
		$sessionManager->set("isLogin",1);
		$sessionManager->set("travelerID",$travelerID);
		$sessionManager->set("login",crypt($travelerID,CRYPT_BLOWFISH));
		$sessionManager->set("domain", $_SERVER["HTTP_HOST"]);
		
		$session = $this->file_factory->invokeStaticClass("Session","getInstance",array($travelerID));
		$session->startSession();
		
		return TRUE;
	}
	
	public function startTravelerSession($travelerID=0){
		return $this->startSession($travelerID);
	}
}