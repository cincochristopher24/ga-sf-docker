<?php
class TravelerViewType{
	public static $ALL              = 'all';
	public static $NEWEST           = 'newest';
	public static $POPULAR          = 'popular';
	public static $SEARCH           = 'search';
	public static $LATEST           = 'lastlogin';
	public static $INMYHOMETOWN     = 'inmyhometown';
	public static $HOMETOWNLOCATION = 'hometown';
	public static $CURRENTLOCATION  = 'current';
}
?>
