<?php
require_once("travellog/model/Class.SessionManager.php");
require_once("travellog/controller/Class.IController.php");
require_once("travellog/factory/Class.FileFactory.php");
abstract class AbstractPagesController implements IController{ 
	
	function __construct(){
		$this->obj_session = SessionManager::getInstance();
		$this->file_factory = FileFactory::getInstance(); 
		$this->file_factory->registerClass('SubNavigation');
		$this->file_factory->registerClass('Template'     , array('path' => ''));
		$this->file_factory->registerClass('HelperGlobal' , array('path' => ''));
		
		$this->file_factory->registerTemplate('LayoutMain');
		$this->file_factory->registerTemplate('PrivacyPolicy');
		$this->file_factory->registerTemplate('TermsDefault');
		$this->file_factory->registerTemplate('TermsAdvisor');
		$this->file_factory->registerTemplate('AboutUs');
		$this->file_factory->registerTemplate('Faq');
		$this->file_factory->registerTemplate('FeaturesOverview');
		$this->file_factory->registerTemplate('DefaultView');

		// added by adelbert - Jan. 14 2009
		$this->file_factory->registerTemplate('gaFileNotFound');
		$this->file_factory->registerTemplate('cbFileNotFound');
		
		$this->file_factory->registerTemplate('LayoutPopup');
				
		$this->file_factory->setPath('CSS', '/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
		
		$this->file_factory->getClass("Template");
		Template::includeDependentCss("/css/vsgStandardized.css");
		Template::includeDependentCss("/css/modalBox.css");
		Template::includeDependentCss('/css/Thickbox.css');
		Template::includeDependentJs('/js/thickbox.js');
	}
	
	function performAction(){                   

		// added by adelbert - Jan. 14 2009 - purpose: to avoid LayoutMain Templete in 'file_not_found' case
		if('file_not_found' != strtolower($this->data['action']))
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));

		switch( strtolower($this->data['action']) ){
			case "terms":
				$this->_viewTerms();
			break;
			
			case "privacy":
				$this->_viewPrivacyPolicy();
			break;
			
			case 'aboutus':
				$this->_viewAboutUs();
			break;
			
			case 'faq':
				$this->_viewFaq();
			break;
			
			case 'features_overview':
				$this->_viewFeaturesOverview();
			break;

            // added by adelbert
			case 'file_not_found':
				$this->_viewFileNotFound();
			break;
			
			case 'messagerules':
				$this->_viewMessageRules();
			break;	

		}
	}
	
	protected function _viewTerms(){
		$obj_template     = $this->file_factory->getClass('Template');
		$obj_inc_template = clone($obj_template);
		
		$obj_template->set('title', 'Terms of Use');
		if( isset($this->data['advisor']) )
			$obj_template->set('content', $obj_inc_template->fetch($this->file_factory->getTemplate('TermsAdvisor')) );
		else
			$obj_template->set('content', $obj_inc_template->fetch($this->file_factory->getTemplate('TermsDefault')) );
		
		$this->file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('title', 'Terms of Use - GoAbroad Network'));
		$this->file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('metaDescription', 'Although GoAbroad Network opens itself to anyone, we have certain restrictions that the visitor needs to be aware of and agree before becoming a member. Read along for more details.'));
		
		$obj_template->out($this->file_factory->getTemplate('DefaultView'));
	}
	
	protected function _viewPrivacyPolicy(){ 
		$obj_template     = $this->file_factory->getClass('Template');
		$obj_inc_template = clone($obj_template);
		
		$obj_template->set('title', 'Privacy Policy');
		$obj_template->set('content', $obj_inc_template->fetch($this->file_factory->getTemplate('PrivacyPolicy')) );
		
		$this->file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('title', 'Privacy Policy - GoAbroad Network'));
		$this->file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('metaDescription', 'GoAbroad Network value your privacy. If you want the specific details read more...'));
		
		$obj_template->out($this->file_factory->getTemplate('DefaultView'));			
	}
	
	protected function _viewAboutUs(){ 
		$obj_template     = $this->file_factory->getClass('Template');
		$obj_inc_template = clone($obj_template);
		
		$isLogged = ($this->obj_session->get('travelerID') > 0 ) ? true : false;
		$obj_inc_template->set('isLogged', $isLogged);
		
		$obj_template->set('title', 'About Us');		
		$obj_template->set('content', $obj_inc_template->fetch($this->file_factory->getTemplate('AboutUs')) );
		
		$this->file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('title', 'About the GoAbroad Network'));
		$this->file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('metaDescription', 'Check out the people behind this newest online social networking site for travelers.'));
		// Added a customized title and meta description - icasimpan Apr 24,2007
	 	
		
		$obj_template->out($this->file_factory->getTemplate('DefaultView'));			
	}
	
	protected function _viewFaq(){ 
		$obj_template     = $this->file_factory->getClass('Template');
		$obj_inc_template = clone($obj_template);
		
		
		//added by Daphne Jul.07.2009
		// display site logos and external links to the corresponding app homepage
		
		$containerIconClassName = array(
    		'Facebook'   => 'facebook_direct',
    		'hi5!'       => 'hi5_direct',
    		'MySpace'    => 'myspace_direct',
    		'Friendster' => 'friendster_direct',
    		'Bebo'       => 'bebo_direct',
    		'Orkut'      => 'orkut_direct'
    	);

		$facebookURL = 'http://apps.facebook.com/';
		$myspaceURL = 'http://www.myspace.com/';
		$orkutURL = 'http://www.orkut.com/Main#Application.aspx?view=canvas&appId=';
		$hi5URL = 'http://www.hi5.com/friend/apps/entry/www.goabroad.net/opensocial/';

		$mtmWidgetLinks = array( 
			'Facebook' => $facebookURL.'my-travel-map',
			'MySpace' => $myspaceURL.'mytravelmap',
			'Orkut' => $orkutURL.'50310940609',
			'hi5!' => $hi5URL.'myTravelMap/myTravelMap.xml');

		$mtpWidgetLinks = array( 
			'Facebook' => $facebookURL.'travel-todo',
			'MySpace' =>  $myspaceURL.'mytravelplans',
			'Orkut' => $orkutURL.'248776314859',
			'hi5!' =>  $hi5URL.'mytravelplans/mytravelplans.xml');

		$mtjWidgetLinks = array( 
			'Facebook' => $facebookURL.'mytraveljournals',
			'MySpace' => $myspaceURL.'mytraveljournals',
			'Orkut' => $orkutURL.'153451467499',				
			'hi5!' => $hi5URL.'myTravelJournals/myTravelJournals.xml');

		$mtbWidgetLinks = array( 
			'Facebook' => $facebookURL.'travelbio',
			'MySpace' => $myspaceURL.'mytravelbio',
			'Orkut' => $orkutURL.'812607160452',
			'hi5!' => $hi5URL.'myTravelBio/mytravelbio.xml');

		$links = array(
			'My Travel Map' 	 => $mtmWidgetLinks,
			'My Travel Plans' 	 => $mtpWidgetLinks,
			'My Travel Journals' => $mtjWidgetLinks,
			'My Travel Bio' 	 => $mtbWidgetLinks);
		$site = (isset($GLOBALS['CONFIG'])) ? $GLOBALS['CONFIG']->getSiteName() : "GoAbroad Network";
		$obj_inc_template->set('widgets', $links);
		$obj_inc_template->set('site', $site);
		$obj_inc_template->set('containerIconClassName', $containerIconClassName);	
			
		$obj_template->set('title', 'Frequently Asked Questions (FAQ)');
		$obj_template->set('content', $obj_inc_template->fetch($this->file_factory->getTemplate('Faq')) );
		
		$this->file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('title', 'Frequently Asked Questions (FAQ) - '. $site));
		$this->file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('metaDescription', 'New to '. $site .'? Familiarize yourself through this list of commonly asked questions.'));
		// Added a customized title and meta description - icasimpan Apr 24,2007
	 	
		
		$obj_template->out($this->file_factory->getTemplate('DefaultView'));			
	}
	
	protected function _viewFeaturesOverview(){ 
		$obj_template     = $this->file_factory->getClass('Template');
		$obj_inc_template = clone($obj_template);
		
		$obj_template->set('title', 'Features Overview');
		$obj_template->set('content', $obj_inc_template->fetch($this->file_factory->getTemplate('FeaturesOverview')) );
		
		$this->file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('title', 'Find out more about our Features'));
		$this->file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('metaDescription', 'Features Overview on GoAbroad Network'));
		// Added a customized title and meta description - icasimpan Apr 24,2007
	 	
		
		$obj_template->out($this->file_factory->getTemplate('DefaultView'));			
	}
    
	// added by adelbert - Jan. 14 2009
	protected function _viewFileNotFound(){   

		$obj_template     = $this->file_factory->getClass('Template');
		$obj_inc_template = clone($obj_template);

		$obj_template->set('title', '');

		$fileNotFoundTemplate = isset($GLOBALS['CONFIG']) ? 'cbFileNotFound' : 'gaFileNotFound';
		$obj_template->set('content', $obj_inc_template->fetch($this->file_factory->getTemplate($fileNotFoundTemplate)) );
		
		$this->file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('title', 'File Not Found'));
		$this->file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('metaDescription', 'File Not Found on GoAbroad Network'));
	 	
		
		$obj_template->out($this->file_factory->getTemplate('DefaultView'));			
	}
	
	protected function _viewMessageRules(){
		$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutPopup')));
		$obj_template     = $this->file_factory->getClass('Template');
		
		$site = (isset($GLOBALS['CONFIG'])) ? $GLOBALS['CONFIG']->getSiteName() : "GoAbroad Network";
		
		$this->file_factory->invokeStaticClass ('Template','setMainTemplateVar', array('title', $site.' : Online Community for Travelers'));
		$obj_template->set('site', $site);
		
		$obj_template->out('travellog/views/tpl.MessageRules.php');
		
	}


}
?>