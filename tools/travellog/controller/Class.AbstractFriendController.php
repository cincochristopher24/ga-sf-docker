<?php
	require_once("travellog/model/Class.SessionManager.php");
	require_once("Class.ToolMan.php");
	require_once("travellog/controller/Class.IController.php");
	require_once("travellog/factory/Class.FileFactory.php");
	//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');

	abstract class AbstractFriendController implements IController{ 
	
		protected static $REQUIRES_ACTION_RULES = array('ACCEPT', 'BLOCK', 'CANCEL', 'DELETE', 'REJECT', 'REQUEST', 'UNBLOCK', 'VIEW', 'VIEWFRIENDSLIST');
	
		protected  
	
		$obj_session  = NULL,
	
		$file_factory = NULL;
	
		function __construct(){
			$this->obj_session  = SessionManager::getInstance();
			$this->file_factory = FileFactory::getInstance(); 
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('PrivacyPreference');
		
			$this->file_factory->registerClass('Paging'         , array('path' => ''));
			$this->file_factory->registerClass('ObjectIterator' , array('path' => ''));
		
			$this->file_factory->registerClass('Template'       , array('path' => ''));
			$this->file_factory->registerClass('HtmlHelpers'    , array('path' => ''));
			$this->file_factory->registerClass('GaDateTime'       , array('path' => ''));
			$this->file_factory->registerClass('Date'           , array('path' => ''));
			
			$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
		
			$this->file_factory->registerTemplate('LayoutMain');
			$this->file_factory->registerTemplate('ViewSuccessInvite');
			$this->file_factory->registerTemplate('MainTemplate');
			$this->file_factory->registerTemplate('ViewFriendsList');
			$this->file_factory->registerTemplate('Friend');
		
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
		}
	
		function performAction(){
			$this->beforePerformAction(); 
		
			switch( strtolower($this->data['action']) ){
				case "accept":
					$this->_accept();
				break;
			
				case "block":
					$this->_block();
				break;
			
				case "cancel":
					$this->_cancel();
				break;
			
				case "delete":
					$this->_delete();
				break;
			
				case "reject":
				case "deny":
					$this->_reject();
				break;
			
				case "request":
					$this->_request();
				break;
			
				case "unblock":
					$this->_unblock();
				break;
			
				case "viewfriendslist":
					$this->__viewFriendsList();
				break;
				
				case "viewpendingrequests":
					$this->__viewPendingRequests();
					
				case "viewblockedusers":
					$this->__viewBlockedTravelers();
					
				
				default:
					$this->_view(); 
			}
		}	
		
		// function added by ianne 11/20/2008 -- success prompt after confirm
		protected function _success(){
			$obj_traveler = $this->file_factory->getClass('Traveler', array( $this->data['fID'] ));
			switch( strtolower($this->data['action']) ){
				case "block":
					$this->obj_session->set('custompopup_message',"You have successfully blocked ".$obj_traveler->getUserName().".");
					break;
				case "unblock":
					$this->obj_session->set('custompopup_message',"You have successfully unblocked ".$obj_traveler->getUserName().".");
					break;	
				case "accept":
					$this->obj_session->set('custompopup_message',"You are now friends with ".$obj_traveler->getUserName().".");
					break;
				case "delete":	
					$this->obj_session->set('custompopup_message',"You have successfully removed ".$obj_traveler->getUserName()." from your friends list.");
					break;
				case "deny":			
				case "reject":
					$this->obj_session->set('custompopup_message',"You have successfully denied ".$obj_traveler->getUserName()."&lsquo;s request to be your friend.");
					break;
				case "cancel":
					$this->obj_session->set('custompopup_message',"You successfully canceled your request to be ".$obj_traveler->getUserName()."&lsquo;s friend.");
					break;
				case "request":
					$this->obj_session->set('custompopup_message',"Your request has been successfully sent to ".$obj_traveler->getUserName().".");
					break;	
			}
		}
		
		protected function _accept(){
			$traveler = $this->file_factory->getClass('Traveler', array( $this->obj_session->get('travelerID') ));
			$traveler->addFriend( $this->data['fID'] );
			// edited by ianne - 11/20/2008
			// Invalidate caching			
			$cache = ganetCacheProvider::instance()->getCache();    	
			if (isset($cache)) {	  
				$hashes = $cache->get('Traveler_Friends_hashes_' . $traveler->getTravelerID());	   
				if (is_array($hashes)) {
					foreach ($hashes as $hash_key) {	   
			 			$cache->delete($hash_key);	   
			 		}
					$cache->set('Traveler_Friends_hashes_' . $traveler->getTravelerID(), array());	   
				}
				
				$hashes = $cache->get('Traveler_Friends_hashes_' . $this->data['fID']);	    
				if (is_array($hashes)) {
					foreach ($hashes as $hash_key) {	  
						$cache->delete($hash_key);	
					}	
					$cache->set('Traveler_Friends_hashes_' . $this->data['fID'], array());	    
				}	       	    	
			} 
			// end of caching invalidation
			
			$this->_success();
			if(isset($this->data['isAjax'])){
				echo $this->obj_session->get('custompopup_message');
				$this->obj_session->unsetVar('custompopup_message');
			}else{
				if(isset($this->data['profile']))
					ToolMan::redirect('/profile.php?travelerID='.$this->data['fID']);
				else
					ToolMan::redirect('/friend.php');
			}
		}
	
		protected function _block(){
			$obj_traveler = $this->file_factory->getClass('Traveler', array( $this->obj_session->get('travelerID') ));
			$obj_traveler->blockUser( $this->data['fID'] );
			$this->data['page'] = ( isset($this->data['page']))	?	$this->data['page']	:	1;
			// edited by ianne - 11/20/2008
			$cache = ganetCacheProvider::instance()->getCache();    	
			if (isset($cache)) {	  
				$hashes = $cache->get('Traveler_Friends_hashes_' . $obj_traveler->getTravelerID());	   
				if (is_array($hashes)) {
					foreach ($hashes as $hash_key) {	   
			 			$cache->delete($hash_key);	   
			 		}
					$cache->set('Traveler_Friends_hashes_' . $obj_traveler->getTravelerID(), array());	   
				}
				
				$hashes = $cache->get('Traveler_Friends_hashes_' . $this->data['fID']);	    
				if (is_array($hashes)) {
					foreach ($hashes as $hash_key) {	  
						$cache->delete($hash_key);	
					}	
					$cache->set('Traveler_Friends_hashes_' . $this->data['fID'], array());	    
				}	       	    	
			} 
			// end of caching invalidation
			
			$this->_success();
			if(isset($this->data['isAjax'])){
				echo $this->obj_session->get('custompopup_message');
				$this->obj_session->unsetVar('custompopup_message');
			}else{
				if(isset($this->data['profile']))
					ToolMan::redirect('/profile.php?travelerID='.$this->data['fID']);
				else
					ToolMan::redirect('/friend.php?page='.$this->data['page']);
			}		
		}
	
		protected function _cancel(){
			$obj_traveler = $this->file_factory->getClass('Traveler', array( $this->obj_session->get('travelerID') ));
			$obj_traveler->cancelFriendRequest( $this->data['fID'] );
			// edited by ianne - 11/20/2008
			$this->_success();
			if(isset($this->data['isAjax'])){
				echo $this->obj_session->get('custompopup_message');
				$this->obj_session->unsetVar('custompopup_message');
			}else{
				$action = (0 < $this->data['locationID'])	?	"CurrentlyIn"	:	NULL;
				$action = ("" == $this->data['keywords'])	?	$action			:	"search";
				if( isset($this->data['travelers']) ){
					if(isset($action))
						ToolMan::redirect('/travelers.php?page='.$this->data['page'].'&locationID='.$this->data['locationID'].'&keywords='.$this->data['keywords'].'&searchby='.$this->data['searchby'].'&action='.$action); 
					else
						ToolMan::redirect('/travelers.php?page='.$this->data['page'].'&locationID='.$this->data['locationID'].'&keywords='.$this->data['keywords'].'&searchby='.$this->data['searchby']); 
				}else if(isset($this->data['profile'])){
					if(isset($this->data['travID']))
						ToolMan::redirect('/profile.php?travelerID='.$this->data['travID']);
					else
						ToolMan::redirect('/profile.php?travelerID='.$this->data['fID']);
				}else
					ToolMan::redirect('/friend.php');
			}	
		}
	
		protected function _delete(){
			$traveler = $this->file_factory->getClass('Traveler', array( $this->obj_session->get('travelerID') ));
			$traveler->removeFriend( $this->data['fID'] );

			// Invalidate caching			
			$cache = ganetCacheProvider::instance()->getCache();    	
			if (isset($cache)) {	  
				$hashes = $cache->get('Traveler_Friends_hashes_' . $traveler->getTravelerID());	   
				if (is_array($hashes)) {
					foreach ($hashes as $hash_key) {	   
			 			$cache->delete($hash_key);	   
			 		}
					$cache->set('Traveler_Friends_hashes_' . $traveler->getTravelerID(), array());	   
				}
				
				$hashes = $cache->get('Traveler_Friends_hashes_' . $this->data['fID']);	    
				if (is_array($hashes)) {
					foreach ($hashes as $hash_key) {	  
						$cache->delete($hash_key);	
					}	
					$cache->set('Traveler_Friends_hashes_' . $this->data['fID'], array());	    
				}	       	    	
			} 
			// end of caching invalidation

			$this->data['page'] = ( isset($this->data['page']))	?	$this->data['page']	:	1;
			// edited by ianne - 11/20/2008
			$this->_success();
			if(isset($this->data['isAjax'])){
				echo $this->obj_session->get('custompopup_message');
				$this->obj_session->unsetVar('custompopup_message');
			}else{
				if(isset($this->data['profile']))
					ToolMan::redirect('/profile.php?travelerID='.$this->data['fID']);
				else
					ToolMan::redirect('/friend.php');
			}		
		}
	
		protected function _reject(){
			$obj_traveler = $this->file_factory->getClass('Traveler', array( $this->obj_session->get('travelerID') ));
			$obj_traveler->rejectFriendRequest( $this->data['fID'] );
			// edited by ianne - 11/20/2008
			$this->_success();
			if(isset($this->data['isAjax'])){
				echo $this->obj_session->get('custompopup_message');
				$this->obj_session->unsetVar('custompopup_message');
			}else{
				if(isset($this->data['profile']))
					ToolMan::redirect('/profile.php?travelerID='.$this->data['fID']);
				elseif( isset( $this->data['page'] ) && strlen($this->data['page']) )
					ToolMan::redirect('/friend.php?page='.$this->data['page']);
				else
					ToolMan::redirect('/friend.php');
			}		
		}
	
		protected function _request(){
			$obj_traveler     = $this->file_factory->getClass('Traveler', array( $this->obj_session->get('travelerID') ));
			$obj_friend       = $this->file_factory->getClass('Traveler', array( $this->data['fID'] ));
	        $obj_template     = $this->file_factory->getClass('Template');
			$obj_inc_template = clone($obj_template);
		
		
			$obj_navigation = $this->file_factory->getClass('SubNavigation');
			$obj_navigation->setContext('TRAVELER');
			$obj_navigation->setContextID( $this->obj_session->get('travelerID') );
			$obj_navigation->setLinkToHighlight('MY_FRIENDS');
			$isFriend = $obj_traveler->isFriend( $this->data['fID'] );
			if ( !$isFriend && !$obj_traveler->isBlocked( $this->data['fID'] ) ){
				//require_once("class.phpmailer.php");
				$obj_traveler->requestFriendship ( $this->data['fID'] );
				
				// added by chris: use notification system to send this notification
				require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
				RealTimeNotifier::getInstantNotification(RealTimeNotifier::REQUEST_FRIEND, 
					array(
						'FRIEND'=> $obj_friend,
						'DOER'	=> $obj_traveler
					))->send();
				// end add
				
				/*$msg = "Hi ".$obj_friend->getTravelerProfile()->getFirstname()."\n\n" .
					   $obj_traveler->getUserName() ." has requested you as a friend on GoAbroad Network. To \n".  
					   "confirm or deny this request, please click on the link below: \n\n" .
					   "http://www.goabroad.net/login.php?redirect=friend.php \n\n".
					   "Happy travels! \n\n" .
					   "GoAbroad Network \n" .
					   "http://www.GoAbroad.net";

				$mail = new PHPMailer();
				$mail->IsSMTP();
				$mail->IsHTML(false);
				$mail->From     = 'admin@goabroad.net';
				$mail->FromName = 'GoAbroad Network';
				$mail->Subject  = 'You have a new friend request on GoAbroad Network!';
				$mail->Body     = $msg;
				$mail->AddAddress($obj_friend->getTravelerProfile()->getEmail());
				$mail->Send();*/
			}
			// edited by ianne 11/20/2008 -- changed implementation after confirm
			$this->_success();
			ob_end_clean();
			
			$action = (array_key_exists('locationID', $this->data) && 0 < $this->data['locationID'])	?	"CurrentlyIn"	:	NULL;
			$action = (array_key_exists('keywords', $this->data) && "" == $this->data['keywords'])	?	$action			:	"search";
			if( isset($this->data['travelers']) ){
				if(isset($action))
					ToolMan::redirect('/travelers.php?page='.$this->data['page'].'&locationID='.$this->data['locationID'].'&keywords='.$this->data['keywords'].'&searchby='.$this->data['searchby'].'&action='.$action); 
				else
					ToolMan::redirect('/travelers.php?page='.$this->data['page'].'&locationID='.$this->data['locationID'].'&keywords='.$this->data['keywords'].'&searchby='.$this->data['searchby']); 
			}else if(isset($this->data['profile'])){
				if(isset($this->data['travID'])) // travelerID = ID of the traveler whose profile is currently viewed
					ToolMan::redirect('/profile.php?travelerID='.$this->data['travID']); 
				else
					ToolMan::redirect('/profile.php?travelerID='.$this->data['fID']);
			}else
				ToolMan::redirect('/friend.php');
			
			/*$isOwner = false;
		
			if( $this->obj_session->getLogin() ){
				if( $this->obj_session->get('travelerID') == $this->data['fID'] ) $isOwner = true; 
			}
		
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain'));
			Template::setMainTemplateVar('page_location', 'My Passport');
		
			$obj_inc_template->set('friend'   , $obj_friend->getUserName() );
			$obj_inc_template->set('isFriend' , $isFriend );
			$obj_inc_template->set('travID'   , $obj_friend->getTravelerID() );
			
			$factory      = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array());
			$profile_comp = $factory->create( ProfileCompFactory::$TRAVELER_CONTEXT );
			$profile_comp->init($this->obj_session->get('travelerID'));
			$obj_view     = $profile_comp->get_view();
		
			$obj_template->set('title'        , 'Friends Confirmation' );
			$obj_template->set('contents'     , $obj_inc_template->fetch( $this->file_factory->getTemplate('ViewSuccessInvite')) );
			$obj_template->set("subNavigation", $obj_navigation);
			$obj_template->set("isOwner"      , $isOwner);
			$obj_template->set( "obj_view"     , $obj_view);
			$obj_template->out( $this->file_factory->getTemplate('MainTemplate') );*/
		}
	
		protected function _unblock(){
			$obj_traveler = $this->file_factory->getClass('Traveler', array( $this->obj_session->get('travelerID') ));
			$obj_traveler->unblockUser( $this->data['fID'] );
			// edited by ianne - 11/20/2008
			$this->_success();
			if(isset($this->data['isAjax'])){
				echo $this->obj_session->get('custompopup_message');
				$this->obj_session->unsetVar('custompopup_message');
			}else{
				if(isset($this->data['profile']))
					ToolMan::redirect('/profile.php?travelerID='.$this->data['fID']);
				else
					ToolMan::redirect('/friend.php');
			}		
		}
		
		protected function _view(){
			if( !isset($this->data['tID']) ){
				$this->data['tID'] = $this->obj_session->get('travelerID');
			}

			// START -- added by: cheryl ivy q. go    25 March 2008
			if (!is_numeric($this->data['tID']) || 0 >= $this->data['tID']){
				header("location:index.php");
				exit;
			}
			// END
			
			$factory      = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array());
			$profile_comp = $factory->create( ProfileCompFactory::$TRAVELER_CONTEXT );
			$profile_comp->init($this->data['tID']);
			$obj_view     = $profile_comp->get_view();
			
			$obj_template     = $this->file_factory->getClass('Template');
			$obj_inc_template = clone($obj_template);
		
			$obj_navigation   = $this->file_factory->getClass('SubNavigation');
			$obj_navigation->setContext('TRAVELER');
			$obj_navigation->setContextID( $this->data['tID'] );
			$obj_navigation->setLinkToHighlight('MY_FRIENDS');
		
			$isOwner       = false;
			$obj_traveler  = $this->file_factory->getClass('Traveler', array($this->data['tID']) );
			$trav_name     = $obj_traveler->getUserName();
			$count_friends = count($obj_traveler->getFriends());
		
			if( $this->obj_session->getLogin() ){
				if( $this->obj_session->get('travelerID') == $this->data['tID'] ){
					$isOwner = true;
				}
			}
			
			//added to enforce privacy preference
			if( !$isOwner && !$obj_traveler->getPrivacyPreference()->canViewFriends($this->obj_session->get('travelerID')) ){
				header("location: /".$trav_name);
				exit;
			}
			
			$this->data['hasPendingRequests'] = false;
			//$querystring = ( isset($this->data['fID']) )? 'action=ViewFriendsList&tID='.$this->data['tID'].'&fID='.$this->data['fID'].'&page=1' : 'action=ViewFriendsList&tID='.$this->data['tID'].'&page=1';
			$this->data['page'] = ( isset($this->data['page']) )? $this->data['page'] : 1;
			if ( $isOwner ){
				$obj_inc_template->set( 'friends_requests' , $obj_traveler->getFriendRequests()   );
				// $obj_inc_template->set( 'pending_requests' , $obj_traveler->getPendingRequests()  );
				$obj_inc_template->set( 'pending_requests' , $this->__viewPendingRequests($obj_traveler));
				// $obj_inc_template->set( 'blocked_users'    , $obj_traveler->getBlockedUsers()     );
				$obj_inc_template->set( 'blocked_users', $this->__viewBlockedTravelers($obj_traveler));
				//$obj_inc_template->set( 'suspended_requests' , $obj_traveler->getHasSuspendedFriendRequests());
				$all_friend_requests = array_merge($obj_traveler->getFriendRequests(),$obj_traveler->getSuspendedFriendRequests());
				$obj_inc_template->set( 'all_friend_requests' , $all_friend_requests);
			}
			$obj_inc_template->set( 'hasPendingRequests', $this->data['hasPendingRequests']);
			$obj_inc_template->set( 'friends_list' , $this->__viewFriendsList()     );
			$obj_inc_template->set( 'IsLogin'      , $this->obj_session->getLogin() );
			$obj_inc_template->set( "isOwner"      , $isOwner                       );
			$obj_inc_template->set( "trav_name"    , $trav_name                     );
			$obj_inc_template->set( "count_friends", $count_friends                 );
			
			$obj_inc_template->set('success',NULL);
			if($this->obj_session->get('custompopup_message')){
				$obj_inc_template->set('success',$this->obj_session->get('custompopup_message'));
				$this->obj_session->unsetVar('custompopup_message');
			}
			
			$obj_inc_template->set( "username"     , $obj_traveler->getUserName()   );
			$code= '';
			
	 		$username = ( strtolower(substr($obj_traveler->getUsername(), -1)) == 's' )? $obj_traveler->getUsername()."'" : $obj_traveler->getUsername()."'s";
	 		if( isset($GLOBALS['CONFIG']) ){
	 			if( $isOwner )
					Template::setMainTemplateVar('title', 'My Friends - ' . $GLOBALS['CONFIG']->getSiteName());
				else
					Template::setMainTemplateVar('title', $username.' Friends - ' . $GLOBALS['CONFIG']->getSiteName() );
	 		} 
			else{
				if( $isOwner )
					Template::setMainTemplateVar('title', 'My Friends - GoAbroad Network: Online Community for Travelers');
				else
					Template::setMainTemplateVar('title', $username.' Friends - GoAbroad Network: Online Community for Travelers');
			}
		
			Template::includeDependentCss("/css/vsgStandardized.css");
			Template::includeDependentCss("/css/modalBox.css");
		
			if( $isOwner )
				Template::setMainTemplateVar('page_location', 'My Passport');
			else{
				Template::includeDependentCss("/css/Thickbox.css");
				Template::includeDependentJs("/js/thickbox.js");	
				Template::setMainTemplateVar('page_location', 'Travelers');
			}
			Template::setMainTemplateVar('layoutID', 'main_pages');
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain'));
			
			$obj_template->set( 'title'        , 'Friends Lists'                                                      );
			$obj_template->set( 'contents'     , $obj_inc_template->fetch($this->file_factory->getTemplate('Friend')) );
			$obj_template->set( "subNavigation", $obj_navigation                                                      );
			$obj_template->set( "isOwner"      , $isOwner                                                             );
			$obj_template->set( "obj_view"     , $obj_view                                                            );
			
			//if friend requests come from suspended users, allow them to show only ONCE, and remove display afterwards
			$obj_traveler->removeUnavailableFriendRequests();
			 
			$obj_template->out( $this->file_factory->getTemplate('MainTemplate') );
			
			
		}
	
		protected function __viewFriendsList(){
			if( !isset($this->data['tID']) && $this->obj_session->getLogin() ){
				$this->data['tID'] = $this->obj_session->get('travelerID');
			}
			$this->data['page'] = ( isset($this->data['page']) )? $this->data['page'] : 1;
			$obj_template       = $this->file_factory->getClass('Template');
			//$canView            = true;
		 
			if ( isset($this->data['fID']) ){
				$obj_traveler   = $this->file_factory->getClass('Traveler', array($this->data['fID']) );
			}
			else{
				$obj_traveler   = $this->file_factory->getClass('Traveler', array($this->data['tID']) );
			}
			
			if( is_object($obj_traveler) && ($obj_traveler->isSuspended() || $obj_traveler->isDeactivated()) ){
				header("location: /travelers.php");
				exit;
			}
		
			//$querystring        = ( isset($this->data['fID']) )? 'action=ViewFriendsList&tID='.$this->data['tID'].'&fID='.$this->data['fID'] : 'action=ViewFriendsList&tID='.$this->data['tID'];
			$querystring        = ( isset($this->data['fID']) )? 'action=View&tID='.$this->data['tID'].'&fID='.$this->data['fID'] : 'action=View&tID='.$this->data['tID'];
			
			$isOwner            = ( $this->data['tID'] == $this->obj_session->get('travelerID') )? true : false;
		
			/**if ( $this->obj_session->getLogin() && isset($this->data['fID']) ){ 
				$obj_pref       = new PrivacyPreference( $this->data['fID'] );
				$canView        = $obj_pref->canViewOnlineStatus( $this->data['tID'] );
			}*/
		
			$friends_lists = $obj_traveler->getFriends(); 
			$arr_lists     = array();
			$trav_name     = $obj_traveler->getUserName();  
			// recreate array of objects without index
			if( count($friends_lists) ){
				foreach($friends_lists as $friends_list){ 
					$arr_lists[] = $friends_list; 
				}
			} 
			
			$paging        = $this->file_factory->getClass('Paging', array( count($arr_lists), $this->data['page'],$querystring,12) );
			$iterator      = $this->file_factory->getClass('ObjectIterator', array( $arr_lists, $paging ) );
			//$paging->setOnClick('getFriendList');
			$obj_template->set( 'paging'         , $paging                        );
			$obj_template->set( 'iterator'       , $iterator                      );
			$obj_template->set( 'IsLogin'        , $this->obj_session->getLogin() );
			//$obj_template->set( 'canView'        , $canView                       );
			$obj_template->set( 'trav_name'      , $trav_name                     );
			$obj_template->set( 'count_friends'  , count($arr_lists)              );
			$obj_template->set( 'isOwner'        , $isOwner                       );
			$obj_template->set( 'allow_controls' , $isOwner                       );
			$obj_template->set( 'current_page'   , $this->data['page']            );

			if(isset($this->data['isAjax']))
				$obj_template->out( $this->file_factory->getTemplate('ViewFriendsList') );
			else	
				return $obj_template->fetch( $this->file_factory->getTemplate('ViewFriendsList') ); 	
		}
	
		protected function __viewPendingRequests($obj_traveler=null){
			if( !isset($this->data['tID']) && $this->obj_session->getLogin() ){
				$this->data['tID'] = $this->obj_session->get('travelerID');
			}
			$showN = (isset($this->data['qty'])) ? $this->data['qty'] : 3;
			$showAll = (isset($this->data['showAll'])) ? true : false;
			
			$pending_request_template = $this->file_factory->getClass('Template');
			
			if(!$obj_traveler){
				$obj_traveler = $this->file_factory->getClass('Traveler', array($this->data['tID']));
			}
			
			$pendingRequests = $obj_traveler->getPendingRequests();
			$this->data['hasPendingRequests'] = count($pendingRequests) ? true : false;
			$pending_request_template->set('pending_requests', $pendingRequests);
			$pending_request_template->set('showN', $showN);
			$pending_request_template->set('showAll', $showAll);
			if(isset($this->data['isAjax'])){
				$pending_request_template->out('travellog/views/tpl.PendingRequests.php');exit;
			}else{
				return $pending_request_template->fetch('travellog/views/tpl.PendingRequests.php');
			}	
		}
		
		protected function __viewBlockedTravelers($obj_traveler=null){
			if( !isset($this->data['tID']) && $this->obj_session->getLogin() ){
				$this->data['tID'] = $this->obj_session->get('travelerID');
			}
			$showN = (isset($this->data['qty'])) ? $this->data['qty'] : 3;
			$showAll = (isset($this->data['showAll'])) ? true : false;
			
			$blocked_travelers_template = $this->file_factory->getClass('Template');

			if(!$obj_traveler){
				$obj_traveler = $this->file_factory->getClass('Traveler', array($this->data['tID']));
			}
		
			$blocked_travelers_template->set('blocked_users', $obj_traveler->getBlockedUsers());
			$blocked_travelers_template->set('showN', $showN);
			$blocked_travelers_template->set('showAll', $showAll);
			if(isset($this->data['isAjax'])){
				$blocked_travelers_template->out('travellog/views/tpl.BlockedTravelers.php');exit;
			}else{
				return $blocked_travelers_template->fetch('travellog/views/tpl.BlockedTravelers.php');
			}
		}
		
		protected function __applyRules(){ 
			if( in_array( strtoupper($this->data['action']), AbstractFriendController::$REQUIRES_ACTION_RULES) ){
				// TODO: If some actions requires a more complex rules use a rule object or method instead
				switch( strtolower($this->data['action']) ){   
					case 'accept':	
						if( !array_key_exists('fID', $this->data) || !$this->obj_session->getLogin() ){
							if( $this->obj_session->getLogin() )
								ToolMan::redirect('/friend.php');
							else
								ToolMan::redirect('/travelers.php');
							exit;
						}
						$obj_traveler = $this->file_factory->getClass('Traveler', array($this->data['fID']));
						//if( $obj_traveler->isAdvisor() ) ToolMan::redirect('/travelers.php');
					break; 
					case 'block':
						if( !array_key_exists('fID', $this->data) || !$this->obj_session->getLogin() ){
							if( $this->obj_session->getLogin() )
								ToolMan::redirect('/friend.php');
							else
								ToolMan::redirect('/travelers.php');
							exit;
						}
						$obj_traveler = $this->file_factory->getClass('Traveler', array($this->data['fID']));
						//if( $obj_traveler->isAdvisor() ) ToolMan::redirect('/travelers.php');
						$obj_traveler = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')));
						/*if( !$obj_traveler->isFriend( $this->data['fID'] ) ){				//editted by neri so as to block a traveler even if he's not a friend': 11-12-08
							if( $this->obj_session->getLogin() )
								ToolMan::redirect('/friend.php');
							else
								ToolMan::redirect('/travelers.php');
							exit;
						}*/
					break;
					case 'cancel':	
						if( !array_key_exists('fID', $this->data) || !$this->obj_session->getLogin() ){
							if( $this->obj_session->getLogin() )
								ToolMan::redirect('/friend.php');
							else
								ToolMan::redirect('/travelers.php');
							exit;
						}
						$obj_traveler = $this->file_factory->getClass('Traveler', array($this->data['fID']));
						//if( $obj_traveler->isAdvisor() ) ToolMan::redirect('/travelers.php');
					break;
					case 'delete': 
						if( !array_key_exists('fID', $this->data) || !$this->obj_session->getLogin() ){
							if( $this->obj_session->getLogin() )
								ToolMan::redirect('/friend.php');
							else
								ToolMan::redirect('/travelers.php');
							exit;
						}
						$obj_traveler = $this->file_factory->getClass('Traveler', array($this->data['fID']));
						//if( $obj_traveler->isAdvisor() ) ToolMan::redirect('/travelers.php');
						$obj_traveler = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')));
						if( !$obj_traveler->isFriend( $this->data['fID'] ) ){
							if( $this->obj_session->getLogin() )
								ToolMan::redirect('/friend.php');
							else
								ToolMan::redirect('/travelers.php');
							exit;
						}
					break;
					case 'reject':
						if( !array_key_exists('fID', $this->data) || !$this->obj_session->getLogin() ){
							if( $this->obj_session->getLogin() )
								ToolMan::redirect('/friend.php');
							else
								ToolMan::redirect('/travelers.php');
							exit;
						}
						$obj_traveler = $this->file_factory->getClass('Traveler', array($this->data['fID']));
						//if( $obj_traveler->isAdvisor() ) ToolMan::redirect('/travelers.php');
					break;
					case 'request':
						if( !array_key_exists('fID', $this->data) || !$this->obj_session->getLogin() ){
							ToolMan::redirect('/travelers.php');
							exit;
						}
						$obj_traveler = $this->file_factory->getClass('Traveler', array($this->data['fID']));
						//if( $obj_traveler->isAdvisor() ) ToolMan::redirect('/travelers.php');
					break;
					case 'unblock':
						if( !array_key_exists('fID', $this->data) || !$this->obj_session->getLogin() ){
							if( $this->obj_session->getLogin() )
								ToolMan::redirect('/friend.php');
							else
								ToolMan::redirect('/travelers.php');
							exit;
						}
						$obj_traveler = $this->file_factory->getClass('Traveler', array($this->data['fID']));
						//if( $obj_traveler->isAdvisor() ) ToolMan::redirect('/travelers.php');
					break;
					case 'view':
					case 'viewfriendslist':
						if( !array_key_exists('tID', $this->data) && !$this->obj_session->getLogin() ){
							if( $this->obj_session->getLogin() )
								ToolMan::redirect('/friend.php');
							else
								ToolMan::redirect('/travelers.php');
							exit; 
						}
					
						// if( !is_numeric($this->data['tID']) ) ToolMan::redirect('/travelers.php'); 
					
						$obj_traveler = ( !array_key_exists('tID', $this->data) && $this->obj_session->getLogin() )? $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID'))):$this->file_factory->getClass('Traveler', array($this->data['tID']));
						//if( $obj_traveler->isAdvisor() ) ToolMan::redirect('/travelers.php');
					break;
				}     
			}
		}
	
		protected function beforePerformAction(){
			if( $this->obj_session->getLogin() ){
				require_once('gaLogs/Class.GaLogger.php');
				GaLogger::create()->start($this->obj_session->get('travelerID'),'TRAVELER');  
			}
		}
	}
?>