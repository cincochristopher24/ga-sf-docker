<?php
	/**
	 * @(#) Class.AbstractSubGroupsController.php
	 * 
	 * @author Daphne Dolina
	 * @version 1.0
	 * @version 1.1 Feb. 9, 2008 Modified by: Antonio Pepito Cruda Jr.
	 */

	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/controller/Class.IController.php");
	require_once("travellog/factory/Class.FileFactory.php");
	require_once("travellog/components/Class.SubGroupComponent.php");
	require_once("Class.StringFormattingHelper.php");
	require_once("travellog/model/Class.SubGroupFactory.php");
	require_once("travellog/model/Class.SubGroupSortType.php");
	require_once("travellog/model/Class.CategoryRankHandler.php");
	require_once("travellog/model/Class.SubGroupRankHandler.php");
	require_once "Class.dbHandler.php";
	require_once("Class.HtmlHelpers.php");
	require_once("Class.Paging.php");
	
	abstract class AbstractSubGroupsController implements iController{
		
		protected 

		$obj_session  = NULL,

		$file_factory = NULL,
		
		$loggedUser  = NULL,

		$isLogged	  = false,

		$isMemberLogged = false,

		$isAdminLogged = false,

		$group 			= NULL,
		
		$subNav		=	NULL,
		
		$mNumRowsRetrieve = 50,
		
		$profile = NULL,  // the group profile that shows above the subnavigation
				
		$tpl		=	NULL;
	
		function __construct(){
			$this->obj_session = SessionManager::getInstance();
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template',array('path'=>''));
		
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('Condition');
			$this->file_factory->registerClass('FilterCriteria2');
			$this->file_factory->registerClass('FeaturedSubGroup');
			$this->file_factory->registerClass('CustomizedSubGroupCategory');
			$this->file_factory->registerClass('AdminGroup');
			$this->file_factory->registerClass('ViewSubGroupsCategory', array('path' => 'travellog/views'));
			$this->file_factory->registerClass('CategoriesWithSubGroupsViewHandler', array('path' => 'travellog/views/subgroups'));
			$this->file_factory->registerClass('SubGroupsViewHandler', array('path' => 'travellog/views/subgroups'));
			$this->file_factory->registerClass('SubGroup');
			
			$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerClass('BackLink' , array('path' => 'travellog/model/navigator'));			
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('ViewSubGroupsPanel', array('path' => 'travellog/views'));
			$this->file_factory->registerClass('ViewFeaturedSubGroups', array('path' => 'travellog/views/subgroups'));
			
			$this->file_factory->registerTemplate('LayoutMain');
			
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			$this->tpl = $this->file_factory->getClass('Template');
			$this->tpl->set_path("travellog/views/subgroups/");
			
		}
		
		/**
		 * Checks whether the current logged in is an admin user or not. If it
		 * is not an admin user it redirects the page to custom group normal view mode.
		 * 
		 * @param boolean $_privilege
		 * @return void
		 */
		function allowAdminPrivileges(){
			if (!$this->isAdminLogged){
				header("location: /subgroups.php?gID=".$this->group->getGroupID());
				exit;
			}
		}
		
		/**
		 * Handles the saving of the given subgroup's parent category. Exclusive for admin account only.
		 * 
		 * @param integer $subgroupID The ID of the subgroup to be assign to a category.
		 * @param integer $categoryID The ID of the new category for the subgroup.
		 * @param array $params The request data. This comes from fetchGPCValues method.
		 */
		function assignSubGroupToCategory($subgroupID, $categoryID, array $params){
			$this->allowAdminPrivileges();
			
			$new_category = $this->file_factory->getClass('CustomizedSubGroupCategory',array($categoryID));
			$subgroup = $this->file_factory->getClass('SubGroup', array($subgroupID));
			if ($new_category->getParentGroupID() == $subgroup->getParentID() OR 0 == $categoryID) {
				$old_category = $subgroup->getCategory();
				if (!is_null($old_category)) {
					$old_category->removeSubGroups($subgroupID);
				}
				
				if ($new_category->getParentGroupID() == $subgroup->getParentID()) {
					$new_category->addSubGroups($subgroupID);
				}
				
				$this->obj_session->set('custompopup_message', "You have successfully added the subgroup <strong>".$subgroup->getName()."</strong> to category <strong>".$new_category->getName()."</strong>.");
			}
			
			$back = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : "";
			
			header("Location: ".$back);
		}
		
		/**
		 * Handles the featuring of the given subgroup.
		 * 
		 * @param integer $subgroupID The ID of the subgroup to be featured.
		 * @param array $params The data from fetchGPC.
		 * @return void
		 */
		function featureGroup($subgroupID, array $params){
			$this->allowAdminPrivileges();
			$featuredSubGroup = $this->file_factory->getClass('FeaturedSubGroup' , array($subgroupID));
			$featuredSubGroup->setSubGroupFeatured();
			if (isset($_REQUEST['show'])) {
				$params['iterator'] = ($this->isAdminLogged) ? $this->group->getFeaturedSubGroups() : null;
				echo ($this->isAdminLogged) ? SubGroupComponent::fetchFeaturedSubGroupsTpl($params) : "";  				
			}
			exit;
		}
		
    /**
     * Fetches or retrieves the values of $_GET, $_POST, $_REQUEST, $_SESSION and $_COOKIE
     * 
     * @return array Array of values from gpc
     */
    function fetchGPCValues(){
			$params = array();
			$params['mode'] = ( isset($_GET['mode']))? $_GET['mode'] : 'view';
			$params['mode'] = ( isset($_POST['mode']))? $_POST['mode'] : $params['mode'];
			$params['gID']  = ( isset($_POST['gID']))? $_POST['gID'] : 0 ;
			$params['gID']  = ( isset($_GET['gID']))? $_GET['gID'] : $params['gID'];
	
			$params['searchKey']  = (isset($_REQUEST['txtGrpName']))? $_REQUEST['txtGrpName'] : '';
			$params['category_name'] = (isset($_REQUEST['category_name'])) ? $_REQUEST['category_name'] : '';
			$params['numRows'] = (isset($_REQUEST['numPerPage'])) ? $_REQUEST['numPerPage'] : $this->mNumRowsRetrieve;
			$params['category'] = ( isset($_REQUEST['category']) )? $_REQUEST['category'] : null;
			$params['categoryID'] = ( isset($_REQUEST['categoryID']) )? $_REQUEST['categoryID'] : 0;
			$params['search_by'] = isset($_REQUEST['search_by']) ? $_REQUEST['search_by'] : 'default';
			$params['action']  = ( isset($_GET['action']))? $_GET['action'] : 'ViewList';
			$params['action']  = ( isset($_POST['action']))? $_POST['action'] : $params['action'];
			
			$params['tabAction'] = (isset($_REQUEST['tabAction'])) ? $_REQUEST['tabAction'] : 'showActive';
			
			$params['sgID']  = ( isset($_GET['sgID']))? $_GET['sgID'] : 0 ;
			$params['radSort']  = ( isset($_REQUEST['radSort']))? $_REQUEST['radSort'] : 0 ;
			
			$params['fromMode'] = (isset($_REQUEST['fromMode'])) ? $_REQUEST['fromMode'] : "view";
			
			$params['page'] = (isset($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
			$params['toPage'] = (isset($_REQUEST['toPage'])) ? $_REQUEST['toPage'] : 1;
			
			$params['message'] = ($this->obj_session->get('custompopup_message')) ? $this->obj_session->get('custompopup_message') : NULL;
			$this->obj_session->unsetVar('custompopup_message');
			
			return $params;  	
    }
    
		/**
		 * Instantiates common variables used in performing an action. The actions in this 
		 * controller are feature, unfeature and sort the subgroups.
		 * 
		 * @param array $params The request data where the common variables is dependent on its instantiation.
		 * @return void
		 */
		function initializeActionCommonAttributes(&$params){
			(0 == $params['gID']) ? $this->redirectToIndex(true) : "";
			
			$mGroup = $this->file_factory->invokeStaticClass('GroupFactory','instance')->create(array($params['gID']));
			if( !count($mGroup) ){
				header("Location: /group.php");
				exit;
			}
			$this->group = $mGroup[0];
			($this->group->isParent()) ? "" : $this->redirectToIndex(true);
			$this->isAdminLogged	=	$this->group->getAdministratorID() == $this->obj_session->get('travelerID') || $this->group->isStaff($this->obj_session->get('travelerID')) ? true : false;			
			
			$params['access'] = ("showInActive" == $params['tabAction']) ? SubGroup::INACTIVE : SubGroup::ACTIVE;
      $params['isAdminLogged'] = $this->isAdminLogged;
			$params['parent'] = $this->group;
			$params['categoryCount'] = $this->group->getSubGroupCategoriesCount(null, true);
		}
		
		/**
		 * Instantiates the common variables used in view methods. Example of
		 * view method is viewArrangeSubGroups method.
		 * 
		 * @param array $params
		 * @return void
		 */
		function initializeViewCommonAttributes(&$params){
			$this->subNav = $this->file_factory->getClass('SubNavigation');
			$this->subNav->setContext('GROUP');
			$this->subNav->setContextID($params['gID']);
			$this->subNav->setLinkToHighlight('SUBGROUPS');
			
			$this->profile = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
			$this->profile->init($this->group->getGroupID());
			
			$handler = new dbHandler();
			$params['searchKey'] = stripslashes(preg_replace("/^'|'$/","",$handler->makeSqlSafeString($params['searchKey'])));
			$params['searchKey'] = htmlentities($params['searchKey'],ENT_QUOTES);
		}
		
		/**
		 * Serves as the entry point for this class
		 * 
		 * @return void
		 */
		function performAction(){
	    $params = $this->fetchGPCValues();
			$this->initializeActionCommonAttributes($params);
			
			switch ($params['action']) {
				case "setFeatured":
					$this->featureGroup($params['sgID'], $params);
				break;
				case "unsetFeatured":
					$this->unFeatureGroup($params['sgID'], $params);
				break;
				case "customsortgroups":
					$this->sortGroups($params);
				break;
				case "transfer_page_list":
					$this->transferPageInListMode($params['sgID'], $params);
				break;
				case "transfer_page_category":
					$this->transferPageInCategoryMode($params['sgID'], $params);
				break;
				case "custom_sort_groups_and_categories":
					$this->sortGroupsAndCategories($params);
				break;
				case "assign_category":
					$this->assignSubGroupToCategory($params['sgID'], $params['categoryID'], $params);
				break;
				case "search":
					if ('group' == $params['search_by']) {
						$params['mode'] = "list";
					}
					else {
						$params['mode'] = "default";
						$params['category_name'] = $params['searchKey'];
						$params['searchKey'] = "";
					}
				break;
			}
			
			$this->initializeViewCommonAttributes($params);
			
			switch ($params['mode']) {
				case "list":
				  $this->viewSubGroupsInListMode($params);
				break;
				case "ajax_list":
					$this->viewActiveInActiveInListMode($params);
				break;
				case "ajax_category":
					$this->viewActiveInActiveInCategoryMode($params);
				break;
				case "ajax_category_subgroups":
					$this->viewSubGroupsOfACategory($params['categoryID'], $params);
				break;
				default:
					$this->viewSubGroupsInCategoryMode($params);				
			}
		}
		
		/**
		 * Redirects the page to index.php when the value of $param is true.
		 * 
		 * @param boolean $param
		 * @return void
		 */
		function redirectToIndex($param){
			if ($param) {
				header("location: /index.php");
				exit;				
			}
		}
		
		/**
		 * Sorts the group of the given parent group. This functionality is
		 * limited only to admin privilege account (this includes the staff account).
		 * 
		 * @param integer $sortType The type of sorting that will be made to the subgroups of the parent group.
		 * @param array   $params  The data from GPC obtained from calling the fetchGPCValues method.
		 * @return void
		 */
		function sortGroups($params){
			$this->allowAdminPrivileges();
			SubGroupRankHandler::arrangeRankOfSubGroups(explode(",", $_REQUEST['sort_subgroup_ids']), (($params['page']-1)*$this->mNumRowsRetrieve)+1);
			exit;
		}
		
		/**
		 * Handles the sorting of groups and categories.
		 * 
		 * @param array $params The request data. The data from fetchGPC method.
		 * @return void
		 */
		function sortGroupsAndCategories(array &$params){
			$array = explode(",", $_REQUEST['sort_category_ids']);
			CategoryRankHandler::arrangeRankOfCategories($array);
			foreach ($array as $id) {
				CategoryRankHandler::arrangeRankOfSubGroups(explode(",", $_REQUEST['sort_subgroup_ids'.$id]), (($_REQUEST['subgroups_page'.$id]-1)*$params['numRows'])+1);
			}
			exit;
		}
		
		/**
		 * Handles the transfer of a group to other page. Limited to admin privilege account only.
		 * 
		 * @param integer $sgID The ID of the subgroups to be transfer to other page. 
		 * @param array $params The request data. The data from fetchGPC method.
		 * @return void
		 */
		function transferPageInCategoryMode($sgID, array &$params){
			$subgroup = $this->file_factory->getClass('SubGroup', array($sgID));
			$category = $subgroup->getCategory();
			if ($this->isAdminLogged AND isset($_REQUEST['toPage']) AND !is_null($category) AND 0 < $category->getCategoryID()) {
				$category->updateNewRankOfSubGroup($subgroup, (($_REQUEST['toPage']-1)*$params['numRows']) + 1, $params['tabAction']);
				$params['mode'] = "ajax_category_subgroups";
				$params['categoryID'] = $category->getCategoryID();
			}
			else {
				exit;
			}
		}
		
		/**
		 * Handles the transfer of a group to other page. Limited to admin privilege account only.
		 * 
		 * @param integer $sgID The ID of the subgroups to be transfer to other page.
		 * @param array $params The request data. The data from fetchGPC method.
		 * @return void
		 */
		function transferPageInListMode($sgID, array &$params){
			if ($this->isAdminLogged AND isset($_REQUEST['toPage'])) {
				$this->group->updateNewRankOfSubGroup($sgID, (($_REQUEST['toPage']-1)*$this->mNumRowsRetrieve) + 1, $params['tabAction']);
			}
			$params['mode'] = "ajax_list";
		}	
		
    /**
     * Unfeatures a given subgroup. Limited to admin account only.
     * 
     * @param integer $subgroupID The ID of the subgroup to be unfeatured.
		 * @param array $params The request data. The data from fetchGPCValues.
     * @return void
     */
		function unFeatureGroup($subgroupID, array $params){
			$this->allowAdminPrivileges();
			$featuredSubGroup = $this->file_factory->getClass('FeaturedSubGroup' , array($subgroupID));
			$featuredSubGroup->unsetSubGroupFeatured();
			if (isset($_REQUEST['show'])) {
				$params['iterator'] = ($this->isAdminLogged) ? $this->group->getFeaturedSubGroups(new RowsLimit(5, 0)) : null;
				echo ($this->isAdminLogged) ? SubGroupComponent::fetchFeaturedSubGroupsTpl($params) : "";  				
			}
			exit;
		}
		
		/**
		 * Handles the viewing of subgroups in category mode using ajax request.
		 * 
		 * @param array $params the request data
		 * @return void
		 */
		function viewActiveInActiveInCategoryMode(array $params){
			$params['subgroups_count'] = $this->group->getMySubGroupsCount("%".$params['searchKey']."%", $params['access']);
			$params['is_searching'] = ("" == trim($params['searchKey']) && "" == trim($params['category_name'])) ? false : true;
			$params['is_category_searching'] = ("" != trim($params['category_name']));
			$params['iterator'] = $this->group->getSubGroupCategories("%".$params['category_name']."%", true);
			$params['numRows'] = (2 < $params['iterator']->size()) ? 5 : 10;
			
			echo preg_replace("/(\s)+/", " ", SubGroupComponent::fetchCategoriesWithSubGroupsTpl($params));
		}
		
    /**
     * Handles the viewing of subgroups that has no main template.
     * 
     * @param array $params The data from fetchGPC.
     * @return void
     */
    function viewActiveInActiveInListMode(array $params){
      $params['is_searching'] = ("" == trim($params['searchKey'])) ? false : true;
      $params['iterator'] = $this->group->getMySubGroups("%".$params['searchKey']."%", $params['access'], "subgrouprank", new RowsLimit($this->mNumRowsRetrieve, (($params['page']-1)*$this->mNumRowsRetrieve)));
      $params['subgroups_count'] = $this->group->getMySubGroupsCount("%".$params['searchKey']."%", $params['access']);
      
  		echo preg_replace("/(\s)+/", " ", SubGroupComponent::fetchSubGroups($params));
    }
		
		/**
		 * Shows the subgroups page in normal mode
		 * 
		 * @param array $params the request data
		 * @return void
		 */
		function viewSubGroupsInCategoryMode(array $params){
			SubGroupComponent::saveUncategorized($this->group);    
      $categories = $this->group->getSubGroupCategories("%".$params['category_name']."%", true);
		  $params['numRows'] = (2 < $categories->size()) ? 5 : 10;
		  $params['subgroups_count'] = $this->group->getMySubGroupsCount("%".$params['searchKey']."%", $params['access']);
			$params['is_searching'] = ("" == trim($params['searchKey']) && "" == trim($params['category_name'])) ? false : true;
			$params['is_category_searching'] = ("" != trim($params['category_name']));
			if (!$params['is_searching']) {
				$params['iterator'] = ($this->isAdminLogged) ? $this->group->getFeaturedSubGroups(new RowsLimit(5, 0)) : null;
				$featured_groups = ($this->isAdminLogged) ? SubGroupComponent::fetchFeaturedSubGroupsTpl($params) : "";      
			}
			else {
				$featured_groups = "";
			}
      $params['iterator'] = $categories;
      
			
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain'))); 
			
			$tpl = $this->file_factory->getClass('Template'); 
			$tpl->set("subGroupsTemplate", SubGroupComponent::fetchCategoriesWithSubGroupsTpl($params));
			$tpl->set("featuredSubGroupsTemplate", $featured_groups);
			$tpl->set("searchKey", $params['searchKey']);
			$tpl->set("search_by", $params['search_by']);
			$tpl->set("category_name", $params['category_name']);
			$tpl->set("grpID", $this->group->getGroupID());
			$tpl->set("cnt_active", $this->group->getMySubGroupsCount("%".$params['searchKey']."%", SubGroup::ACTIVE));
			$tpl->set("cnt_inactive", $this->group->getMySubGroupsCount("%".$params['searchKey']."%", SubGroup::INACTIVE));
			$tpl->set("profile", $this->profile->get_view());
			$tpl->set("cntCategory", $params['categoryCount']);
			$tpl->set("subNavigation",$this->subNav);
			$tpl->set("assigner", ($this->isAdminLogged) ? SubGroupComponent::fetchCategoryAssignerTpl($categories) : "");
			$tpl->set("isAdminLogged",$this->isAdminLogged);
			$tpl->set("tab_action", $params['tabAction']);
			$tpl->set("is_searching", $params['is_searching']);
			$tpl->set("success", $params['message']);
			$tpl->set("mode", $params['mode']);
			$tpl->set("numRows", $params['numRows']);
			$tpl->set("txtGroupName",$params['searchKey']);
			$tpl->out("travellog/views/subgroups/tpl.GroupManagement.php"); 
		}
		
		/**
		 * Shows the subgroups page in list mode view or undetailed mode view.
		 *  
		 * @param array $params request data
		 * @return void
		 */
    function viewSubGroupsInListMode($params) {
      $cnt_active = $this->group->getMySubGroupsCount("%".$params['searchKey']."%", SubGroup::ACTIVE);
      $cnt_inactive = $this->group->getMySubGroupsCount("%".$params['searchKey']."%", SubGroup::INACTIVE);
      $params['iterator'] = $this->group->getMySubGroups("%".$params['searchKey']."%", $params['access'], "subgrouprank", new RowsLimit($this->mNumRowsRetrieve, (($params['page'] - 1)*$this->mNumRowsRetrieve)));
      $params['is_searching'] = ("" == trim($params['searchKey'])) ? false : true;
      $params['subgroups_count'] = (SubGroup::ACTIVE == $params['access']) ? $cnt_active : $cnt_inactive;
	
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			
			$tpl = $this->file_factory->getClass('Template');
			$tpl->set("subgroups", SubGroupComponent::fetchSubGroups($params));
			$tpl->set("searchKey", $params['searchKey']);
			$tpl->set("grpID", $this->group->getGroupID());
			$tpl->set("profile", $this->profile->get_view());
			$tpl->set("subNavigation", $this->subNav);
			$tpl->set("search_by", $params['search_by']);
			$tpl->set("isAdminLogged", $this->isAdminLogged);
			$tpl->set("assigner", ($this->isAdminLogged) ? SubGroupComponent::fetchCategoryAssignerTpl($this->group->getSubGroupCategories()) : "");
			$tpl->set("page", $params['page']);
			$tpl->set("is_searching", $params['is_searching']);
			$tpl->set("cnt_active", $cnt_active);
			$tpl->set("cnt_groups", ($cnt_active + $cnt_inactive));
			$tpl->set("cnt_inactive", $cnt_inactive);
			$tpl->set("tabAction", $params['tabAction']);
			$tpl->set("success", $params['message']);
	    $tpl->out("travellog/views/subgroups/tpl.ViewSubGroupList.php");	    
    }
    
    /**
     * Handles the viewing of subgroups of a given category.
     * 
     * @param integer $categoryID The ID of the given category.
     * @param array $params The request data.
     * @return void
     */
    function viewSubGroupsOfACategory($categoryID, array $params){
    	$category = $this->file_factory->getClass('CustomizedSubGroupCategory',array($categoryID));
			$groups_count = $category->getSubGroupsCount("%".$params['searchKey']."%", $params['access']);
			
			$paging = new Paging($groups_count, $params['page'], "gID=".$this->group->getGroupID()."&mode=ajax_category_subgroups&txtGrpName=".$params['searchKey']."&tabAction=".$params['tabAction']."&numPerPage=".$params['numRows']."&categoryID=".$category->getCategoryID()."',".$category->getCategoryID().",'", $params['numRows']);
			$paging->setOnclick("manager.paging");    	
    	
			$tpl = $this->file_factory->getClass('Template');
			$tpl->set("iterator", $category->getSubGroups("%".$params['searchKey']."%", $params['access'], " categorysubgrouprank", new RowsLimit($params['numRows'], (($params['page']-1)*$params['numRows']))));
			$tpl->set("keyword", $params['searchKey']);
			$tpl->set("grpID", $this->group->getGroupID());
			$tpl->set("parent", $this->group);
			$tpl->set("categoryID", $category->getCategoryID());
			$tpl->set("isAdminLogged", $this->isAdminLogged);
			$tpl->set("groups_count", $groups_count);
			$tpl->set("categoryCount", $params['categoryCount']);
			$tpl->set("page", $params['page']);
			$tpl->set("tabAction", $params['tabAction']);
			$tpl->set("paging", $paging);
			$tpl->set("numPerPage", $params['numRows']);
	    echo preg_replace("/(\s)+/", " ", $tpl->fetch("travellog/views/subgroups/tpl.IncCategorySubGroups.php"));	    	
    	exit;
    }
    
    /**
     * Activates the __call magic function.
     * 
     * @return void
     */
    function __call($method, $arguments){
			header ("HTTP/1.0 404 Not Found");
			header ("Status 404 Not Found"); 
			require_once("FileNotFound.php");
			exit;
    }
	}
?>