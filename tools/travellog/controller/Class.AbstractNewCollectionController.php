<?php

	require_once 'travellog/factory/Class.FileFactory.php';
	require_once 'travellog/controller/Class.IController.php';
	require_once 'Class.Template.php';
	require_once("travellog/UIComponent/NewCollection/interface/PhotoCollectionConstants.php");
    
	// Added by: Adelbert
	require_once('travellog/controller/socialApps/Class.fbNeedToUpdateAccountController.php');
	
	abstract class AbstractNewCollectionController implements iController, PhotoCollectionConstants{
		protected $mSession		=	NULL;
		protected $file_factory	=	NULL;
		protected $mTemplate	=	NULL;
		protected $mSubNavigation	=	NULL;
		protected $mContextInfo	=	NULL;
		
		protected $mType		=	NULL;
		protected $mID			=	NULL;
		protected $mContext		=	NULL;
		protected $mGenID		=	NULL;
		
		protected $mLoggedID	=	0;
		protected $mUserLevel	=	0; //0=not logged, 1=owner, 2=staff 3=superstaff, 4=admin
		
		protected $mTraveler	=	NULL;
		protected $mGroup 		= 	NULL;
		
		protected $mParams		=	array();
		protected $mCollectionHelper	=	NULL;
		protected $mCollectionLoader	=	NULL;
		
		protected static $instance = NULL;
		
		function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template', array('path' => ''));
			$this->file_factory->registerClass('SessionManager');
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('PhotoAlbum');
			$this->file_factory->registerClass('Photo');
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('PathManager');
			
			$this->file_factory->registerClass('PhotoCollectionHelper',				array('path' => 'travellog/UIComponent/NewCollection/helper/'));
			$this->file_factory->registerClass('PhotoCollectionUploadHelper',		array('path' => 'travellog/UIComponent/NewCollection/helper/'));
			$this->file_factory->registerClass('PhotoCollectionLoaderFactory',		array('path' => 'travellog/UIComponent/NewCollection/factory/'));
			$this->file_factory->registerClass('PhotoCollectionDTOFactory',			array('path' => 'travellog/UIComponent/NewCollection/factory/'));
			$this->file_factory->registerClass('PhotoCollectionUploadedPhotoFactory',	array('path' => 'travellog/UIComponent/NewCollection/factory/'));
			
			$this->file_factory->registerClass('ViewPhotoCollectionList',			array('path' => 'travellog/UIComponent/NewCollection/views/'));
			$this->file_factory->registerClass('ViewPhotoCollectionItemContent',	array('path' => 'travellog/UIComponent/NewCollection/views/'));
			$this->file_factory->registerClass('ViewPhotoCollectionRearrangePhotos',array('path' => 'travellog/UIComponent/NewCollection/views/'));
			$this->file_factory->registerClass('ViewPhotoCollectionImageUploader',	array('path' => 'travellog/UIComponent/NewCollection/views/'));
			$this->file_factory->registerClass('ViewPhotoCollectionGroupMembers',	array('path' => 'travellog/UIComponent/NewCollection/views/'));
			$this->file_factory->registerClass('ViewPhotoCollectionGroupMemberPhotoList',	array('path' => 'travellog/UIComponent/NewCollection/views/'));
			$this->file_factory->registerClass('ViewPhotoCollectionThumbnailCropper',		array('path' => 'travellog/UIComponent/NewCollection/views/'));
			$this->file_factory->registerClass('ViewPhotoCollectionManageUploadedPhotos',	array('path' => 'travellog/UIComponent/NewCollection/views/'));
			
			$this->file_factory->registerClass('ViewManageFeaturedAlbums',			array('path' => 'travellog/UIComponent/NewCollection/views/'));
			$this->file_factory->registerClass('ViewPopupGallery',					array('path' => 'travellog/UIComponent/NewCollection/views/'));
			
			$this->file_factory->registerClass('RandomTravelBioView',	array('path' => 'travellog/views/'));
			$this->file_factory->registerClass('ProfileCompFactory',	array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerTemplate('LayoutMain');

			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			$this->mSession = $this->file_factory->invokeStaticClass('SessionManager', 'getInstance');
			$this->mLoggedID = $this->mSession->get('travelerID');
		
			self::$instance = $this;
		}
		
		function performAction(){
			
			$this->mParams = array(	"type"		=>	NULL,
									"ID"		=>	NULL,
									"context"	=>	NULL,
									"genID"		=>	NULL,
									"tlogID"	=>	NULL,
									"mode"		=>	NULL,
									"action"	=>	self::VIEW_COLLECTION );
			$this->mParams = array_merge($this->mParams,$this->extractParameters());
			
			$this->mType = $this->mParams["type"];
			$this->mID = $this->mParams["ID"];

			// temporary fix
			//$this->mGenID = str_replace('?______array', '', $this->mParams["genID"]); // $this->mParams["genID"];
			$this->mGenID = $this->mParams["genID"];
			$this->mContext = $this->mParams["context"];
			
			switch($this->mParams["action"]){
				case self::CREATE_ALBUM					:	$this->_createAlbum();
															break;
				case self::LOAD_UPLOADER				:	$this->_loadUploader();
															break;											
				case self::EDIT_TITLE					:	$this->_editTitle();
															break;
				case self::VIEW_MEMBERS					:	$this->_viewMembers();
															break;
				case self::FETCH_MEMBERS				:	$this->_fetchMembers();
															break;
				case self::VIEW_MEMBER_PHOTOS			:	$this->_viewMemberPhotos();
															break;
				case self::MANAGE_PHOTOS				:	$this->_managePhotos();
															break;
				case self::SAVE_CHANGES					:	$this->_saveChanges();
															break;
				case self::GRAB_PHOTO					:	$this->_grabPhoto();
															break;
				case self::REMOVE_MEMBER_PHOTO			:	$this->_removeMemberPhoto();
															break;
				case self::UPLOAD						:	$this->_upload();
															break;
				case self::GET_UPLOAD_ERRORS			:	$this->_getUploadErrors();
															break;
				case self::MANAGE_PHOTOS				:	$this->_managePhotos();
															break;
				case self::BATCH_DELETE					:	$this->_batchDelete();
															break;
				case self::DELETE_PHOTO					:	$this->_deletePhoto();
															break;
				case self::REMOVE_GRABBED				:	$this->_removeGrabbed();
															break;
				case self::EDIT_CAPTION					:	$this->_editCaption();
															break;
				case self::ROTATE						:	$this->_rotate();
															break;
				case self::SET_PRIMARY					:	$this->_setAsPrimary();
															break;
				case self::SET_JOURNAL_PRIMARY			: 	$this->_setAsJournalPrimary();
															break;
				case self::LOAD_CROPPER					:	$this->_loadCropper();
															break;
				case self::SAVE_THUMBNAIL				:	$this->_saveThumbnail();
															break;
				case self::DELETE_ALBUM					:	$this->_deleteAlbum();
															break;
				case self::LOAD_REARRANGE_VIEW			:	$this->_loadRearrangeView();
															break;
				case self::SAVE_PHOTO_ORDER				:	$this->_savePhotoOrder();
															break;
				case self::SAVE_FEATURED_ALBUMS			:	$this->_saveFeaturedAlbums();
															break;
				case self::FEATURE_ALBUM				:	$this->_featureAlbum();
															break;
				case self::UPDATE_MANAGE_FEATURED_ALBUMS_NOTICE	:	$this->_updateManageFeaturedAlbumsNotice();
																	break;
				case self::VIEW_COLLECTION				:	$this->_renderCollection();
															break;
				case self::LOAD_POPUP_GALLERY			:	$this->_loadPopupGallery();
															break;
				case self::FETCH_HEADER_AFTER_DELETE_ALBUM	:	$this->_fetchHeaderAfterDeleteAlbum();
																break;
				default									:	$this->_redirectPageNotFound();
															break;
			}
			
		}
		
		function _defineCommonAttributes(){
			$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			
			$this->mTemplate = $this->file_factory->getClass('Template');
			$this->mTemplate->set_path('travellog/UIComponent/NewCollection/views/');

			$this->_checkUserLevel();
			
			if( self::TRAVELER_TYPE == $this->mType ){
				Template::setMainTemplateVar('page_location', $this->mLoggedID == $this->mID ? 'My Passport' : 'Travelers');
			}
			else if (self::GROUP_TYPE == $this->mType) {
				if( isset($GLOBALS["CONFIG"]) ){
					Template::setMainTemplateVar('page_location', 'Home');
				}else{
					Template::setMainTemplateVar('page_location', 'Groups / Clubs');
				}
			}
			
			if (self::GROUP_TEMPLATE_TYPE == $this->mType) {
				$group_template = GanetGroupTemplatePeer::retrieveByPK($this->mID);
				if (null !== $group_template) {
					$this->mContextInfo = new GanetGroupTemplateProfileComponent($group_template);
					$this->mSubNavigation	= new GanetGroupTemplateNavigationComponent($group_template, GanetGroupTemplateModulePeer::TYPE_PHOTO);
				}
				else {
					header('Location: /index.php');
					exit;
				}
			}
			else {
				$this->mSubNavigation = $this->file_factory->getClass('SubNavigation');
				if (self::GROUP_TYPE == $this->mType && 0 < $this->mID) {
					$this->mSubNavigation->setContext('GROUP');
					$this->mSubNavigation->setContextID($this->mID);
					$this->mSubNavigation->setLinkToHighlight('PHOTOS');
					$this->mSubNavigation->setGroupType('ADMIN_GROUP');
					
					$mProfileFactory = $this->file_factory->invokeStaticClass('ProfileCompFactory', 'getInstance', array());
					$mProfileComp	= $mProfileFactory->create( ProfileCompFactory::$GROUP_CONTEXT );
	
					if(2 == $this->mGroup->getDiscriminator() && (1 < $this->mGroup->getParentID()))
						$mProfileComp->init($this->mGroup->getParentID());
					else
						$mProfileComp->init($this->mID);
				}
				else if (self::TRAVELER_TYPE == $this->mType && 0 < $this->mID) {
					$this->mSubNavigation->setContext('TRAVELER');
					$this->mSubNavigation->setContextID($this->mID);
					$this->mSubNavigation->setLinkToHighlight('MY_PHOTOS');
					
					$mProfileFactory = $this->file_factory->invokeStaticClass('ProfileCompFactory', 'getInstance', array());
					$mProfileComp	= $mProfileFactory->create( ProfileCompFactory::$TRAVELER_CONTEXT );
					$mProfileComp->init($this->mID);
				}
				else {
					header("location: index.php");
					exit;
				}
	
				$this->mContextInfo = $mProfileComp->get_view();
			}
		}
		
		function _checkUserLevel(){
			// check if user is currently logged in
			switch($this->mType){
				case self::TRAVELER_TYPE	:	$this->mUserLevel = ($this->mLoggedID==$this->mID)? self::OWNER : 0;
												$this->mTraveler = $this->file_factory->getClass('Traveler', array(intval($this->mID)));
												if( !($this->mTraveler instanceof Traveler) ){
													header("location: /FileNotFound.php");
													exit;
												}
												if($this->mTraveler->isSuspended() || $this->mTraveler->isDeactivated()){
													header("location: /travelers.php");
													exit;
												}
												break;
				case self::GROUP_TYPE		:	$group = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array(intval($this->mID)));
												$this->mGroup = $group[0];
												if( !($this->mGroup instanceof AdminGroup) ){
													header("Location: /FileNotFound.php");
													exit;
												}
												if( 0 < $this->mLoggedID && !$this->mGroup->isSuspended() && $this->mGroup->getDiscriminator() == GROUP::ADMIN){
													if( $this->mGroup->isSubGroup() ){
														$parent = $this->mGroup->getParent();
														if ($parent->getAdministratorID() == $this->mLoggedID){
															$this->mUserLevel = self::ADMIN;
														}else if ($parent->isSuperStaff($this->mLoggedID)){
															$this->mUserLevel = self::SUPERSTAFF;
														}else if( $this->mGroup->isStaff($this->mLoggedID)){
															$this->mUserLevel = self::STAFF;
														}
													}else{
														if ($this->mGroup->getAdministratorID() == $this->mLoggedID){
															$this->mUserLevel = self::ADMIN;
														}else if ($this->mGroup->isSuperStaff($this->mLoggedID)){
															$this->mUserLevel = self::SUPERSTAFF;
														}else if($this->mGroup->isStaff($this->mLoggedID)){
															$this->mUserLevel = self::STAFF;
														}
													}
												}
												break;
				case self::GROUP_TEMPLATE_TYPE:
					$group_template = GanetGroupTemplatePeer::retrieveByPK($this->mID);
					if (null !== $group_template) {
						$groups = GroupFactory::instance()->create(array($group_template->getParentGroupID()));
						if (0 < count($groups)) {
							$this->mGroup = $groups[0];
							if (GanetSessionService::getInstance()->isGroupAdminLogged($groups[0])) {
								$this->mUserLevel = self::ADMIN;
							}
						}
					}
					else {
						header('Location: /index.php');
						exit;
					}
				break;
			}
			
			if ( self::VIEWER < $this->mUserLevel ) {
				// start GA Logger
				require_once 'gaLogs/Class.GaLogger.php';
				try{
					GaLogger::create()->start($this->mLoggedID, 'TRAVELER');
				}
				catch(exception $e){
				}
			}
		}
		
		function _prepareDependencies(){
			Template::setMainTemplateVar('layoutID', 'page_photos');
			
			Template::includeDependentCss("/css/vsgStandardized.css");
			Template::includeDependentCss("/css/modalBox.css");
			
			if( 0 < $this->mUserLevel ){
				Template::includeDependentCss("/min/g=PhotoCollectionPrivilegedCss");
				Template::includeDependentJs("/min/g=PhotoCollectionPrivilegedJs");
				Template::includeDependentJs("/js/prototype.js");
				Template::includeDependentJs("/min/g=PhotoCollectionPrivilegedJs2");
			}else{
				Template::includeDependentCss("/css/Thickbox.css");
				Template::includeDependentJs("/js/thickbox.js");
			}
			
			Template::includeDependentJs("/min/g=PhotoCollectionJs");
		}
		
		function _prepareCollectionHelper(){
			$this->mCollectionHelper = $this->file_factory->invokeStaticClass('PhotoCollectionHelper', 'getInstance');
			$this->mCollectionHelper->setType($this->mType);
			$this->mCollectionHelper->setID($this->mID);
			$this->mCollectionHelper->setContext($this->mContext);
			$this->mCollectionHelper->setGenID($this->mGenID);
			$this->mCollectionHelper->setUserLevel($this->mUserLevel);
			$this->mCollectionHelper->setLoggedID($this->mLoggedID);
			$this->mCollectionHelper->setGroup($this->mGroup);
			$this->mCollectionHelper->setTraveler($this->mTraveler);
			$this->mCollectionHelper->setTravellogID($this->mParams["tlogID"]);
			$this->mCollectionHelper->setMode($this->mParams["mode"]);
			$this->mCollectionHelper->setAction($this->mParams["action"]);
		}
		
		function _prepareCollection(){
			if( is_null($this->mCollectionHelper) ){
				$this->_prepareCollectionHelper();
			}
			$collectionLoaderFactory = $this->file_factory->invokeStaticClass('PhotoCollectionLoaderFactory', 'getInstance');
			$this->mCollectionLoader = $collectionLoaderFactory->create($this->mCollectionHelper);
		
			$activeCollectionItem = $this->mCollectionLoader->getCollectionItem($this->mCollectionHelper->getContext(),$this->mCollectionHelper->getGenID());
			
			$this->mCollectionHelper->setCollection($this->mCollectionLoader->getCollection());
			$this->mCollectionHelper->setActiveCollectionItem($activeCollectionItem);
		}
		
		function extractParameters(){
			$params = $_REQUEST;
			$paramArray = array();

			if( isset($params) ){
				foreach( $params as $key => $value){
					$paramArray[$key] = $value;
				}
			}
			return $paramArray;
		}
		
		function _redirectPageNotFound(){
			header("location: /FileNotFound.php");
			exit;
		}
		
		function _redirectIfNotAllowed($allowed=TRUE){
			if( !$allowed ){
				$this->_redirectPageNotFound();
			}
		}
		
		function _restrictIfNotPrivileged($privileged=FALSE){
			if( !$privileged ){
				echo "RESTRICTED";
				exit;
			}
		}
		
		function _stopOnError($error=TRUE){
			if( $error ){
				echo "ERROR";
				exit;
			}
		}
		
		function _renderCollection(){
			$this->_checkUserLevel();
			$this->_defineCommonAttributes();
			$this->_prepareDependencies();
			$this->_prepareCollectionHelper();
			$this->_prepareCollection();
							
			if( !$this->mCollectionHelper->getActiveCollectionItem() && self::VIEWER == $this->mUserLevel 
				|| is_null($this->mCollectionHelper->getType()) || is_null($this->mCollectionHelper->getID()) ){
				$this->_redirectPageNotFound();
			}	
			$this->_redirectIfNotAllowed(!$this->mCollectionHelper->_isAjaxRequest());
				
			$mCollectionList = $this->file_factory->getClass('ViewPhotoCollectionList');
			$mCollectionList->setCollectionHelper($this->mCollectionHelper);
			
			$mCollectionContent = $this->file_factory->getClass('ViewPhotoCollectionItemContent');
			$mCollectionContent->setCollectionHelper($this->mCollectionHelper);
			
			if( self::GROUP_TYPE == $this->mCollectionHelper->getType() ){
				$this->mTemplate->set("copying_notice_component", new GanetGroupCopyingNoticeComponent($this->mCollectionHelper->getGroup()));
			}
			if( !is_null($this->mCollectionHelper->getGenID()) || !is_null($this->mCollectionHelper->getActiveCollectionItem()) ){ 
				$collectionItem = $this->mCollectionHelper->getActiveCollectionItem();
				if( is_object($collectionItem) ){
					$photoIDs = $collectionItem->getPhotoIDs();
					if( count($photoIDs) ){
						$contents = array(	"label"		=>	"Share this Collection:",
											"permalink"	=>	"http://".$_SERVER["SERVER_NAME"].$collectionItem->getLink()."#".$photoIDs[0],
											"title"		=>	$collectionItem->getTitle()." photos",
											"target_new_window"	=> TRUE	);
					
						require_once("travellog/views/Class.SharebarView.php");
						$sharebar = new SharebarView();
						$sharebar->setContents($contents);
						$this->mTemplate->set("sharebar",$sharebar);
					}
				}
			}
			if( !is_null($this->mCollectionHelper->getGenID()) || !is_null($this->mCollectionHelper->getActiveCollectionItem()) ){ 
				$collectionItem = $this->mCollectionHelper->getActiveCollectionItem();
				if( $collectionItem && !is_null($collectionItem) && is_object($collectionItem) ){
					$photoIDs = $collectionItem->getPhotoIDs();
					if( count($photoIDs) ){
						require_once('Class.StringFormattingHelper.php');
						$this->file_factory->getClass("Template");
						$possessive_name = StringFormattingHelper::possessive($this->mCollectionHelper->getOwner()->getName());
						$siteContext = SiteContext::getInstance();
						$site_name = isset($GLOBALS["CONFIG"]) ? $siteContext->getSiteName()." Network" : "GoAbroad Network";
						$fbMetaTags = array(	'<meta name="title" content="'.$possessive_name.' '.$collectionItem->getTitle().' photo collection"/>',
												'<meta property="og:title" content="'.$possessive_name.' '.$collectionItem->getTitle().' photo collection"/>',
												'<meta property="og:site_name" content="'.$site_name.'"/>');
						Template::clearFBRequiredMetaTags();
						Template::hideProfileFBLike();
						Template::setFBRequiredMetaTags($fbMetaTags);
						
						$contents = array(	"permalink"	=>	"http://".$_SERVER["SERVER_NAME"].$collectionItem->getLink(),
											"title"		=>	$collectionItem->getTitle()." photo collection",
											"name"		=>	"photo");

						require_once("travellog/views/Class.FBLikeControlView.php");
						$fbLike = new FBLikeControlView();
						$fbLike->setContents($contents);
						$this->mTemplate->set("fbLike",$fbLike);
					
						require_once("travellog/views/Class.BookmarkControlsView.php");
						$sharebar = new BookmarkControlsView();
						$sharebar->setContents($contents);
						$sharebar->prepareURLParameters();
						$this->mTemplate->set("sharebar",$sharebar);
					}
					if( self::GROUP_TYPE == $this->mCollectionHelper->getType() || self::GROUP_TEMPLATE_TYPE == $this->mCollectionHelper->getType() ){
						Template::setMainTemplateVar("title",$this->mCollectionHelper->getOwner()->getName().": ".$collectionItem->getTitle()." photo collection");
					}else{
						Template::setMainTemplateVar("title",$this->mCollectionHelper->getOwner()->getUsername().": ".$collectionItem->getTitle()." photo collection");
					}
				}
			}
			
			$this->mTemplate->set("collection",		$mCollectionList);
			$this->mTemplate->set("content",		$mCollectionContent);
			$this->mTemplate->set("collectionControl", $mCollectionContent->getCollectionControlView());
			$this->mTemplate->set("subNavigation",	$this->mSubNavigation);
			$this->mTemplate->set("contextInfo",	$this->mContextInfo);
			$this->mTemplate->set("type",			$this->mType);
			$this->mTemplate->set("ID",				$this->mID);
			$this->mTemplate->set("context",		$this->mContext);
			$this->mTemplate->set("genID",			$this->mGenID);
			$this->mTemplate->set("isPrivileged",	(self::VIEWER < $this->mUserLevel)? 1 : 0);
			$this->mTemplate->set("isAdmin",		$this->mUserLevel == self::ADMIN ? 1 : 0);
			$this->mTemplate->out("tpl.Collection.php");
		}
		
		function _createAlbum(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			
			if (self::GROUP_TEMPLATE_TYPE == $this->mCollectionHelper->getType()) {
				$_POST['context_id'] = $this->mCollectionHelper->getID();
				$_POST['context_type'] = 3;
				$_POST['album_title'] = $this->mParams["txtAlbumName"];
				$request = new GanetWebRequest();
				$request->setParameter('success_redirect_url', "/collection.php?type=group_template&ID=".$this->mCollectionHelper->getID()."&context=group_template&genID={album_id}&action=3");
				
				$action = new GanetPhotoSavePhotoAlbumAction($request);
				if ($action->validate()) {
					$action->execute();
				}
				else {
					$action->onValidationFailure();
				}
			}
			else {
				$this->_redirectIfNotAllowed(self::VIEWER < $this->mCollectionHelper->getUserLevel() 
					&& !is_null($this->mCollectionHelper->getType()) 
					&& !is_null($this->mCollectionHelper->getID())
					&& isset($this->mParams["txtAlbumName"]) 
					&& "" != trim($this->mParams["txtAlbumName"])
					&& !$this->mCollectionHelper->_isAjaxRequest());

				require_once("travellog/model/Class.PhotoAlbum.php");
				$album = new PhotoAlbum();
				if( self::GROUP_TYPE == $this->mCollectionHelper->getType() ){
					$album->setGroupID($this->mCollectionHelper->getID());
					$this->mCollectionHelper->setContext(self::ALBUM_CONTEXT);
				}else if( self::TRAVELER_TYPE == $this->mCollectionHelper->getType() ){
					$album->setGroupID(0);
					$this->mCollectionHelper->setContext(self::TRAVALBUM_CONTEXT);
				}
				$album->setTitle($this->mParams["txtAlbumName"]);
				$album->setCreator($this->mCollectionHelper->getLoggedID());
				$album->Create();
				
				$this->mCollectionHelper->setGenID($album->getPhotoAlbumID());
				
				require_once("travellog/model/Class.ActivityFeed.php");
				try{
					ActivityFeed::create($album);
				}catch(exception $e){}
				
				$this->invalidateCache();
				header("location: collection.php?type=".$this->mCollectionHelper->getType()."&ID=".$this->mCollectionHelper->getID()."&context=".$this->mCollectionHelper->getContext()."&genID=".$this->mCollectionHelper->getGenID()."&action=3");
			}
		}
		
		function _loadUploader(){
			$this->_checkUserLevel();
			$this->_defineCommonAttributes();
			$this->_prepareDependencies();
			$this->_prepareCollectionHelper();
			$this->_prepareCollection();
			
			$this->_redirectIfNotAllowed(self::VIEWER < $this->mCollectionHelper->getUserLevel() 
				&& $this->mCollectionHelper->validContext()
				&& $this->mCollectionHelper->validGenID() 
				&& !$this->mCollectionHelper->_isAjaxRequest());
				
			$collectionDTO = $this->_getCollectionDTO();
			$this->mCollectionHelper->setActiveCollectionItem($collectionDTO);
			
			$mCollectionList = $this->file_factory->getClass('ViewPhotoCollectionList');
			$mCollectionList->setCollectionHelper($this->mCollectionHelper);
			
			$mCollectionContent = $this->file_factory->getClass('ViewPhotoCollectionImageUploader');
			$mCollectionContent->setCollectionHelper($this->mCollectionHelper);
			
			$this->mTemplate->set("collection",		$mCollectionList);
			$this->mTemplate->set("content",		$mCollectionContent);
			$this->mTemplate->set("subNavigation",	$this->mSubNavigation);
			$this->mTemplate->set("contextInfo",	$this->mContextInfo);
			$this->mTemplate->set("type",			$this->mType);
			$this->mTemplate->set("isPrivileged",	(self::VIEWER < $this->mUserLevel)? 1 : 0);
			$this->mTemplate->set("isAdmin",		$this->mUserLevel == self::ADMIN ? 1 : 0);
			$this->mTemplate->out("tpl.Collection.php");
		}
		
		function _editTitle(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			
			if (self::GROUP_TEMPLATE_TYPE == $this->mCollectionHelper->getType()) {
				$_POST['context_id'] = $this->mCollectionHelper->getID();
				$_POST['context_type'] = 3;
				$_POST['album_title'] = $this->mParams["txtAlbumName"];
				$_POST['album_id'] = $this->mCollectionHelper->getGenID();
				$request = new GanetWebRequest();	
				ob_start();
				$action = new GanetPhotoSavePhotoAlbumAction($request);
				if ($action->validate()) {
					$action->execute();
				}
				else {
					$action->onValidationFailure();
				}
				ob_end_clean();
			}
			else {
				$this->_redirectIfNotAllowed($this->mCollectionHelper->_isAjaxRequest());
				$this->_restrictIfNotPrivileged(self::VIEWER < $this->mCollectionHelper->getUserLevel());
				
				$this->_stopOnError(self::ALBUM_CONTEXT != $this->mCollectionHelper->getContext()
					&& self::TRAVALBUM_CONTEXT != $this->mCollectionHelper->getContext() && self::GROUP_TEMPLATE_CONTEXT != $this->mCollectionHelper->getContext()
					|| !isset($this->mParams["txtAlbumName"]) 
					|| !$this->mCollectionHelper->validGenID());
				
				$collectionDTO = $this->_getCollectionDTO();
				$mAlbum = $collectionDTO->getContextObject();
				
				if( $mAlbum instanceof PhotoAlbum ){
					$this->_restrictIfNotPrivileged($mAlbum->getCreator() == $this->mLoggedID || $mAlbum->getGroupID() == $this->mCollectionHelper->getID());
					$mAlbum->setTitle($this->mParams["txtAlbumName"]);
					$mAlbum->Update();
				}else{
					$this->_stopOnError(TRUE);
				}
			}
		}
		
		function _viewMembers(){
			$this->_checkUserLevel();
			$this->_defineCommonAttributes();
			$this->_prepareDependencies();
			$this->_prepareCollectionHelper();
			$this->_prepareCollection();
			
			$this->_redirectIfNotAllowed(self::VIEWER < $this->mCollectionHelper->getUserLevel() 
				&& ((self::GROUP_TYPE == $this->mCollectionHelper->getType() 
					&& self::ALBUM_CONTEXT == $this->mCollectionHelper->getContext())
				|| (self::GROUP_TEMPLATE_TYPE == $this->mCollectionHelper->getType() 
					&& self::GROUP_TEMPLATE_CONTEXT == $this->mCollectionHelper->getContext()))
				&& $this->mCollectionHelper->validGenID()
				&& !$this->mCollectionHelper->_isAjaxRequest());
			
			$collectionDTO = $this->_getCollectionDTO();
			$this->mCollectionHelper->setActiveCollectionItem($collectionDTO);
			
			$mCollectionList = $this->file_factory->getClass('ViewPhotoCollectionList');
			$mCollectionList->setCollectionHelper($this->mCollectionHelper);
			
			$mCollectionContent = $this->file_factory->getClass('ViewPhotoCollectionGroupMembers');
			$mCollectionContent->setCollectionHelper($this->mCollectionHelper);
			
			$this->mTemplate->set("collection",		$mCollectionList);
			$this->mTemplate->set("content",		$mCollectionContent);
			$this->mTemplate->set("subNavigation",	$this->mSubNavigation);
			$this->mTemplate->set("contextInfo",	$this->mContextInfo);
			$this->mTemplate->set("type",			$this->mType);
			$this->mTemplate->set("isPrivileged",	(self::VIEWER < $this->mUserLevel)? 1 : 0);
			$this->mTemplate->set("isAdmin",		$this->mUserLevel == self::ADMIN ? 1 : 0);
			$this->mTemplate->out("tpl.Collection.php");
		}
		
		function _fetchMembers(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			$this->_redirectIfNotAllowed($this->mCollectionHelper->_isAjaxRequest());
			$this->_restrictIfNotPrivileged(self::VIEWER < $this->mCollectionHelper->getUserLevel());
			$this->_stopOnError(!(self::GROUP_TYPE == $this->mCollectionHelper->getType()
				&& self::ALBUM_CONTEXT == $this->mCollectionHelper->getContext()
				&& $this->mCollectionHelper->validGenID()));
				
			$collectionDTO = $this->_getCollectionDTO();
			$this->mCollectionHelper->setActiveCollectionItem($collectionDTO);
			
			$mCollectionContent = $this->file_factory->getClass('ViewPhotoCollectionGroupMembers');
			$mCollectionContent->setCollectionHelper($this->mCollectionHelper);
			$mCollectionContent->setPage($this->mParams["page"]);
			$mCollectionContent->renderFetchedMembers();
		}
		
		function _viewMemberPhotos(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			$this->_redirectIfNotAllowed($this->mCollectionHelper->_isAjaxRequest());
			$this->_restrictIfNotPrivileged(self::VIEWER < $this->mCollectionHelper->getUserLevel());
			$this->_stopOnError((self::GROUP_TYPE != $this->mCollectionHelper->getType() 
								&& self::GROUP_TEMPLATE_TYPE != $this->mCollectionHelper->getType())
				|| (self::ALBUM_CONTEXT != $this->mCollectionHelper->getContext() 
					&& self::GROUP_TEMPLATE_CONTEXT != $this->mCollectionHelper->getContext())
				|| !$this->mCollectionHelper->validGenID()
				|| !isset($this->mParams["tID"])
				|| !is_numeric($this->mParams["tID"]) );
			
			$collectionDTO = $this->_getCollectionDTO();
			
			$member = $this->file_factory->getClass('Traveler', array(intval($this->mParams["tID"])));
			
			$mCollectionContent = $this->file_factory->getClass('ViewPhotoCollectionGroupMemberPhotoList');
			$mCollectionContent->setMember($member);
			$mCollectionContent->setGroup($this->mCollectionHelper->getGroup());
			$mCollectionContent->setGroupPhotoAlbum($collectionDTO->getContextObject());
			$mCollectionContent->render();
		}
		
		function _grabPhoto(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			$this->_redirectIfNotAllowed($this->mCollectionHelper->_isAjaxRequest());
			$this->_restrictIfNotPrivileged(self::VIEWER < $this->mCollectionHelper->getUserLevel());
			$this->_stopOnError((self::GROUP_TYPE != $this->mCollectionHelper->getType() 
								&& self::GROUP_TEMPLATE_TYPE != $this->mCollectionHelper->getType())
				|| (self::ALBUM_CONTEXT != $this->mCollectionHelper->getContext() 
					&& self::GROUP_TEMPLATE_CONTEXT != $this->mCollectionHelper->getContext())
				|| !$this->mCollectionHelper->validGenID()
				|| !isset($this->mParams["photoID"])
				|| !is_numeric($this->mParams["photoID"]) );
			
			$collectionDTO = $this->_getCollectionDTO();

			$mGroupAlbum = $collectionDTO->getContextObject();
			
			if( self::ALBUM_CONTEXT == $this->mCollectionHelper->getContext() ){
				$mPhoto = $this->file_factory->getClass('Photo', array($mGroupAlbum,$this->mParams["photoID"]));
			
				$this->_stopOnError( !($mPhoto instanceof Photo) );
			
				$mGroupAlbum->addPhoto($mPhoto,$grabbed=TRUE);
				if( is_null($mGroupAlbum->getPrimary()) || !$mGroupAlbum->getPrimary() ){
					$mGroupAlbum->setAsPhotoAlbumPrimary($mPhoto->getPhotoID());
				}
			}else if( self::GROUP_TEMPLATE_CONTEXT == $this->mCollectionHelper->getContext() ){
				$oldPhoto = $mPhoto = $this->file_factory->getClass('Photo', array(NULL,$this->mParams["photoID"]));
				$pathManager =$this->file_factory->getClass('PathManager',array($oldPhoto->getContext()));
				
				$mPhoto = GanetPhotoPeer::retrieveByPk($this->mParams["photoID"]);
				$this->_stopOnError(is_null($mPhoto));
				
				$this->_stopOnError( !$mGroupAlbum->grabPhoto($mPhoto,$pathManager) );
			}
			
			$mTpl = $this->file_factory->getClass('Template');
			$mTpl->set("title",htmlspecialchars(stripslashes($mGroupAlbum->getTitle()),ENT_QUOTES));
			$mTpl->set("mode","ADD");
			$mTpl->set("message","Successfully added");
			$mTpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionMemberPhotoGrabNotice.php");
		
			$this->invalidateCache();
		}
		
		function _removeMemberPhoto(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			$this->_redirectIfNotAllowed($this->mCollectionHelper->_isAjaxRequest());
			$this->_restrictIfNotPrivileged(self::VIEWER < $this->mCollectionHelper->getUserLevel());
			$this->_stopOnError((self::GROUP_TYPE != $this->mCollectionHelper->getType() 
								&& self::GROUP_TEMPLATE_TYPE != $this->mCollectionHelper->getType())
				|| (self::ALBUM_CONTEXT != $this->mCollectionHelper->getContext() 
					&& self::GROUP_TEMPLATE_CONTEXT != $this->mCollectionHelper->getContext())
				|| !$this->mCollectionHelper->validGenID()
				|| !isset($this->mParams["photoID"])
				|| !is_numeric($this->mParams["photoID"]) );
			
			$collectionDTO = $this->_getCollectionDTO();
			
			$mGroupAlbum = $collectionDTO->getContextObject();
			
			if( self::ALBUM_CONTEXT == $this->mCollectionHelper->getContext() ){
				$mPhoto = $this->file_factory->getClass('Photo', array($mGroupAlbum,$this->mParams["photoID"]));
			
				$this->_stopOnError( !($mPhoto instanceof Photo) );
			
				$mGroupAlbum->removeGrabbedPhoto($mPhoto->getPhotoID());
			}else if( self::GROUP_TEMPLATE_CONTEXT == $this->mCollectionHelper->getContext() ){
				$mPhoto = GanetPhotoPeer::retrieveByPk($this->mParams["photoID"]);
				
				$this->_stopOnError(is_null($mPhoto));
				
				$this->_stopOnError( !$mGroupAlbum->removeGrabbedPhoto($mPhoto) );
			}
			
			$mTpl = $this->file_factory->getClass('Template');
			$mTpl->set("title",htmlspecialchars(stripslashes($mGroupAlbum->getTitle()),ENT_QUOTES));
			$mTpl->set("mode","REMOVE");
			$mTpl->set("message","Successfully removed");
			$mTpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionMemberPhotoGrabNotice.php");
		
			$this->invalidateCache();
		}
		
		function _upload(){
			$this->_checkUserLevel();
			ob_clean();
			$this->_prepareCollectionHelper();
			//$this->_redirectIfNotAllowed($this->mCollectionHelper->_isAjaxRequest());
			$this->_stopOnError(!$this->mCollectionHelper->validContext()
				|| !$this->mCollectionHelper->validGenID());

			/*$server = $_SERVER["SERVER_NAME"];
			$tolerate = FALSE;
			if( 0 < strpos($server,".local") || 0 === strpos($server,"dev.goabroad.net") || 0 < strpos($server,"dev.goabroad.net") ){//is local or dev
				$tolerate = TRUE;
			}*/
			
			$this->_restrictIfNotPrivileged(self::VIEWER < $this->mCollectionHelper->getUserLevel());
			
			ob_clean();
			$collectionDTO = $this->_getCollectionDTO();
			$uploadHelper = $this->file_factory->invokeStaticClass('PhotoCollectionUploadHelper', 'getInstance');
			$uploadHelper->setCollectionDTO($collectionDTO);
			$uploadHelper->setUploadMode($this->mParams["mode"]);
			$uploadHelper->setFiles($_FILES);
			$uploadHelper->setData($this->mParams);
			$uploadHelper->setBatchUploadID(isset($_REQUEST["batchUploadID"]) ? $_REQUEST["batchUploadID"] : NULL);
			$uploadHelper->processFiles();
			ob_clean();
			
			$hasInvalidFile = FALSE;
			if( isset($_REQUEST["mode"]) && 1 == $_REQUEST["mode"] ){
				$collectionDTO = $this->_getCollectionDTO();
				$collectionName = "_".$collectionDTO->getContext()."_".$collectionDTO->getGenID();
				if( isset($_SESSION["InvalidFiles$collectionName"]) ){
					unset($_SESSION["InvalidFiles$collectionName"]);
					unset($_SESSION["LargeFiles$collectionName"]);
					echo "UNRECOGNIZED_FILE_TYPE";
					$hasInvalidFile = TRUE;
				}
				if( isset($_SESSION["ValidFiles$collectionName"]) ){
					unset($_SESSION["UploadError$collectionName"]);
					unset($_SESSION["ValidFiles$collectionName"]);
				}
			}
			
			if( !$hasInvalidFile ){
				$this->invalidateCache();	
			}
		}
		
		function _getUploadErrors(){
			$this->_checkUserLevel();
			ob_clean();
			$this->_prepareCollectionHelper();
			$this->_redirectIfNotAllowed($this->mCollectionHelper->_isAjaxRequest());
			$this->_restrictIfNotPrivileged(self::VIEWER < $this->mCollectionHelper->getUserLevel());
			$this->_stopOnError(!$this->mCollectionHelper->validContext()
				|| !$this->mCollectionHelper->validGenID());
			
			$collectionDTO = $this->_getCollectionDTO();
			$collectionName = "_".$collectionDTO->getContext()."_".$collectionDTO->getGenID();
			
			$validUploads = 0;
			$success = "";
			if( isset($_SESSION["ValidFiles$collectionName"]) ){
				$success = implode(",",$_SESSION["ValidFiles$collectionName"]);
				$validUploads = count($_SESSION["ValidFiles$collectionName"]);
				unset($_SESSION["ValidFiles$collectionName"]);
			}
			
			$failed = "";
			if( isset($_SESSION["UploadError$collectionName"]) && $_SESSION["UploadError$collectionName"] ){
				$failed = implode(",",$_SESSION["LargeFiles$collectionName"]);
				$failed .= "?".implode(",",$_SESSION["InvalidFiles$collectionName"]);
				unset($_SESSION["LargeFiles$collectionName"]);
				unset($_SESSION["InvalidFiles$collectionName"]);
				unset($_SESSION["UploadError$collectionName"]);
			}
			
			if( "" == $failed && 0 < $validUploads ){//no failed uploads
				echo "UPLOADED?".$validUploads;
				exit;
			}
			
			echo $validUploads."?".$failed;
		}
		
		function _managePhotos(){//after uploading files
			$this->_checkUserLevel();
			$this->_defineCommonAttributes();
			$this->_prepareDependencies();
			$this->_prepareCollectionHelper();
			
			$this->_redirectIfNotAllowed(self::VIEWER < $this->mCollectionHelper->getUserLevel()
				&& self::JOURNAL_CONTEXT != $this->mCollectionHelper->getContext()
				&& $this->mCollectionHelper->validGenID()
				&& !$this->mCollectionHelper->_isAjaxRequest());
			
			$this->_prepareCollection();
			
			$mCollectionList = $this->file_factory->getClass('ViewPhotoCollectionList');
			$mCollectionList->setCollectionHelper($this->mCollectionHelper);
			
			$mCollectionContent = $this->file_factory->getClass('ViewPhotoCollectionManageUploadedPhotos');
			$mCollectionContent->setCollectionHelper($this->mCollectionHelper);
			
			$this->mTemplate->set("collection",		$mCollectionList);
			$this->mTemplate->set("content",		$mCollectionContent);
			$this->mTemplate->set("subNavigation",	$this->mSubNavigation);
			$this->mTemplate->set("contextInfo",	$this->mContextInfo);
			$this->mTemplate->set("type",			$this->mType);
			$this->mTemplate->set("ID",				$this->mID);
			$this->mTemplate->set("context",		$this->mContext);
			$this->mTemplate->set("genID",			$this->mGenID);
			$this->mTemplate->set("isPrivileged",	(self::VIEWER < $this->mUserLevel)? 1 : 0);
			$this->mTemplate->set("isAdmin",		$this->mUserLevel == self::ADMIN ? 1 : 0);
			$this->mTemplate->out("tpl.Collection.php");
		}
		
		function _saveChanges(){
			$this->_checkUserLevel();
			$this->_defineCommonAttributes();
			$this->_prepareDependencies();
			$this->_prepareCollectionHelper();
			
			$this->_redirectIfNotAllowed(self::VIEWER < $this->mCollectionHelper->getUserLevel()
				&& self::JOURNAL_CONTEXT != $this->mCollectionHelper->getContext()
				&& $this->mCollectionHelper->validGenID()
				&& !$this->mCollectionHelper->_isAjaxRequest());
						
			$mCollection = $this->_getCollectionDTO();
			
			$photoContext = $mCollection->getContextObject();
			$photos = $photoContext->getPhotos();
			
			foreach($photos as $photo){
				if (self::GROUP_TEMPLATE_TYPE == $this->mCollectionHelper->getType()) {
					$caption = (isset($this->mParams["txtCaption_".$photo->getPhotoID()])) ? trim(strip_tags($this->mParams["txtCaption_".$photo->getPhotoID()])) : $photo->getCaption();
					$photo->setCaption($caption);
					$photo->save();
					
					$primary_id = (isset($this->mParams["radPrimary"]) && 0 < $this->mParams["radPrimary"]) ? $this->mParams["radPrimary"] : $photoContext->getPrimaryPhotoID();
					$photoContext->setPrimaryPhotoID($primary_id);
					$photoContext->save();
				}
				else {
					if( isset($this->mParams["txtCaption_".$photo->getPhotoID()]) 
						&& $photo->getCaption() != trim(strip_tags($this->mParams["txtCaption_".$photo->getPhotoID()])) ){
						$photo->setCaption(trim(strip_tags($this->mParams["txtCaption_".$photo->getPhotoID()])));
						$photo->updateCaption();
					}
					if( isset($this->mParams["radPrimary"]) && $photo->getPhotoID() == $this->mParams["radPrimary"] ){
						if( self::ALBUM_CONTEXT == $mCollection->getContext() ){
							require_once("travellog/model/Class.PhotoAlbum.php");
							$album = new PhotoAlbum($mCollection->getGenID());
							$photo->setContext($album);
							$photo->setPhotoTypeID(7);
						}
						$photo->setAsPrimaryPhoto();
					}	
				}
				
				$this->invalidateCache();
			}
			
			if( self::TRAVELLOG_CONTEXT == $mCollection->getContext() ){
				header("location: ".$mCollection->getJournalCollectionLink());
			}else{
				header("location: /collection.php?type=".$mCollection->getType()."&ID=".$mCollection->getID()."&context=".$mCollection->getContext()."&genID=".$mCollection->getGenID());	
			}
		}
		
		function _batchDelete(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			
			if (self::GROUP_TEMPLATE_TYPE == $this->mCollectionHelper->getType()) {
				$_POST['photo_ids'] = isset($this->mParams["photoID"]) ? $this->mParams["photoID"] : array();
				$_POST['album_id'] = $this->mCollectionHelper->getGenID();
				$action = new GanetPhotoDeletePhotosAction(new GanetWebRequest());
				if ($action->validate()) {
					$action->execute();
				}
				else {
					$action->onValidationFailure();
				}
			}
			else {		
				$this->_redirectIfNotAllowed($this->mCollectionHelper->_isAjaxRequest());
				$this->_restrictIfNotPrivileged(self::VIEWER < $this->mCollectionHelper->getUserLevel());
				$this->_stopOnError(!$this->mCollectionHelper->validContext()
					|| !$this->mCollectionHelper->validGenID());
				
				$mCollection = $this->_getCollectionDTO();
				
				$context_obj = $mCollection->getContextObject();
				
				if( !is_null($context_obj) && is_array($this->mParams["photoID"]) ){
					foreach($this->mParams["photoID"] as $photoID){
						//check if photo is only a grabbed photo
						if( self::ALBUM_CONTEXT == $mCollection->getContext() && $context_obj->isGrabbedPhoto($photoID) ){
							$context_obj->removeGrabbedPhoto($photoID);
						}else{
							$photo_obj = $this->file_factory->getClass('Photo', array($context_obj,$photoID));
							$photo_obj->Delete();
							if( self::TRAVELLOG_CONTEXT == $mCollection->getContext() ){
								$photo_obj->DeleteBackup($mCollection->getGenID());
							}
						}
					}
				
					$this->invalidateCache();
				}
			}
		}
		
		function _deleteAlbum(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			
			if (self::GROUP_TEMPLATE_TYPE == $this->mCollectionHelper->getType()) {
				$_POST['album_id'] = $this->mCollectionHelper->getGenID();
				$request = new GanetWebRequest();
				$request->setParameter('success_redirect_url', "/collection.php?type=group_template&ID=".$this->mCollectionHelper->getID()."&context=group_template&genID={album_id}&action=3");
				
				ob_start();
				$action = new GanetPhotoDeletePhotoAlbumAction($request);
				if ($action->validate()) {
					$action->execute();
				}
				else {
					$action->onValidationFailure();
				}
				ob_end_clean();
			}
			else {
				$this->_redirectIfNotAllowed($this->mCollectionHelper->_isAjaxRequest());
				$this->_restrictIfNotPrivileged(self::VIEWER < $this->mCollectionHelper->getUserLevel());
				
				$this->_stopOnError(self::ALBUM_CONTEXT != $this->mCollectionHelper->getContext()
					&& self::TRAVALBUM_CONTEXT != $this->mCollectionHelper->getContext()
					|| !$this->mCollectionHelper->validGenID());
				
				$collectionDTO = $this->_getCollectionDTO();
				$mAlbum = $collectionDTO->getContextObject();
				
				if( $mAlbum instanceof PhotoAlbum ){
					$this->_restrictIfNotPrivileged($mAlbum->getCreator() == $this->mLoggedID || $mAlbum->getGroupID() == $this->mCollectionHelper->getID());
					$mAlbum->Delete();
					
					$this->invalidateCache();
				}else{
					$this->_stopOnError(TRUE);
				}
			}
		}
		
		function _deletePhoto(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			
			if (self::GROUP_TEMPLATE_TYPE == $this->mCollectionHelper->getType()) {
				$_POST['photo_ids'] = array($this->mParams["photoID"]);
				$_POST['album_id'] = $this->mCollectionHelper->getGenID();
				$action = new GanetPhotoDeletePhotosAction(new GanetWebRequest());
				if ($action->validate()) {
					$action->execute();
				}
				else {
					$action->onValidationFailure();
				}
			}
			else {
				$this->_redirectIfNotAllowed($this->mCollectionHelper->_isAjaxRequest());
				$this->_restrictIfNotPrivileged(self::VIEWER < $this->mCollectionHelper->getUserLevel());
				
				$this->_stopOnError(self::JOURNAL_CONTEXT == $this->mCollectionHelper->getContext() 
					&& is_null($this->mCollectionHelper->getTravellogID())
				 	|| !isset($this->mParams["photoID"]) 
					|| !is_numeric($this->mParams["photoID"])
					|| !$this->mCollectionHelper->validContext()
					|| !$this->mCollectionHelper->validGenID());
				
				$collectionDTO = $this->_getCollectionDTO();
				$contextObject = $collectionDTO->getContextObject();
				
				$photo = $this->file_factory->getClass("Photo",array($contextObject,intval($this->mParams["photoID"])));
				$photo->Delete();
				if( self::JOURNAL_CONTEXT == $this->mContext || self::TRAVELLOG_CONTEXT == $this->mContext ){
					$photo->DeleteBackup($this->mCollectionHelper->getTravellogID());
				}
				
				$this->invalidateCache();
				
				// Added by: Adelbert Silla
				// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
				//           This is a preperation for updating application static views in facebook
				// Date added: aug. 10, 2009
				
				// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
				fbNeedToUpdateAccountController::saveNewlyEditedAccount($_SESSION['travelerID'],4);
			}
		}
		
		function _removeGrabbed(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			$this->_redirectIfNotAllowed($this->mCollectionHelper->_isAjaxRequest());
			$this->_restrictIfNotPrivileged(self::VIEWER < $this->mCollectionHelper->getUserLevel());
			
			$this->_stopOnError(self::ALBUM_CONTEXT != $this->mCollectionHelper->getContext() 
				|| !$this->mCollectionHelper->validGenID()
			 	|| !isset($this->mParams["photoID"]) 
				|| !is_numeric($this->mParams["photoID"]));
			
			$collectionDTO = $this->_getCollectionDTO();
			$album = $collectionDTO->getContextObject();
			
			$this->_restrictIfNotPrivileged($album->getGroupID() == $this->mCollectionHelper->getID());
			
			$album->removeGrabbedPhoto($this->mParams["photoID"]);
			
			$this->invalidateCache();
		}
		
		function _editCaption(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			$this->_redirectIfNotAllowed($this->mCollectionHelper->_isAjaxRequest());
			$this->_restrictIfNotPrivileged(self::VIEWER < $this->mCollectionHelper->getUserLevel());
			
			$this->_stopOnError(self::JOURNAL_CONTEXT == $this->mCollectionHelper->getContext()
			 	&& is_null($this->mCollectionHelper->getTravellogID())
			 	|| !isset($this->mParams["photoID"]) 
				|| !is_numeric($this->mParams["photoID"])
				|| !$this->mCollectionHelper->validGenID()
				|| !$this->mCollectionHelper->validContext() );
			
			$collectionDTO = $this->_getCollectionDTO();
			$contextObject = $collectionDTO->getContextObject();
			
			$photo = $this->file_factory->getClass("Photo",array($contextObject,intval($this->mParams["photoID"])));
			
			if( !($photo instanceof Photo) ){
				$this->_stopOnError(TRUE);
			}
			
			$photo->setCaption($this->mParams["caption"]);
			$photo->updateCaption();
		}
		
		function _rotate(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			$this->_redirectIfNotAllowed($this->mCollectionHelper->_isAjaxRequest());
			$this->_restrictIfNotPrivileged(self::VIEWER < $this->mCollectionHelper->getUserLevel());
			$this->_stopOnError(self::JOURNAL_CONTEXT == $this->mCollectionHelper->getContext()
		 		&& is_null($this->mCollectionHelper->getTravellogID())
		 		|| !isset($this->mParams["photoID"]) 
				|| !is_numeric($this->mParams["photoID"])
				|| !$this->mCollectionHelper->validGenID()
				|| !$this->mCollectionHelper->validContext()
				|| !isset($this->mParams["angle"]) );
			
			if( 0 == $this->mParams["angle"] ){
				exit;
			}
			
			$mCollection = $this->_getCollectionDTO();
			$mPhotoContext = $mCollection->getContextObject();
			
			if (self::GROUP_TEMPLATE_TYPE == $this->mCollectionHelper->getType()) {
				$photo = GanetPhotoPeer::retrieveByPK($this->mParams["photoID"]);
				$this->_stopOnError(is_null($photo));
				$uploaded = $this->file_factory->invokeStaticClass('PhotoCollectionUploadedPhotoFactory', 'getInstance')->create($mCollection->getContext());
				$uploaded->rotatePhoto2($mPhotoContext, $photo, $this->mParams["angle"]);	
				echo "SUCCESS?".$photo->getFilename();					
			}
			else {
				$mPhoto = $this->file_factory->getClass('Photo', array($mPhotoContext,$this->mParams["photoID"]));
				$this->_stopOnError(!($mPhoto instanceof Photo));
				
				$pathManager =$this->file_factory->getClass('PathManager',array($mPhotoContext));
				$uploaded = $this->file_factory->invokeStaticClass('PhotoCollectionUploadedPhotoFactory', 'getInstance')->create($mCollection->getContext());
				$uploaded->rotatePhoto($pathManager,$mPhotoContext,$mPhoto,$this->mParams["angle"]);
			
				echo "SUCCESS?".$mPhoto->getFilename();
			}
		}
		
		function _setAsPrimary(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			if (self::GROUP_TEMPLATE_TYPE == $this->mCollectionHelper->getType()) {
				$_GET['photo_id'] = $this->mParams["photoID"];
				$action = new GanetPhotoSetPrimaryPhotoAction(new GanetWebRequest());
				if ($action->validate()) {
					$action->execute();
				}
				else {
					$action->onValidationFailure();
				}
			}
			else {
				$this->_redirectIfNotAllowed($this->mCollectionHelper->_isAjaxRequest());
				$this->_restrictIfNotPrivileged(self::VIEWER < $this->mCollectionHelper->getUserLevel());
				
				$this->_stopOnError(self::JOURNAL_CONTEXT == $this->mCollectionHelper->getContext() 
					&& is_null($this->mCollectionHelper->getTravellogID())
				 	|| !isset($this->mParams["photoID"]) 
					|| !is_numeric($this->mParams["photoID"])
					|| !$this->mCollectionHelper->validGenID()
					|| !$this->mCollectionHelper->validContext() );
				
				$collectionDTO = $this->_getCollectionDTO();
				$contextObject = $collectionDTO->getContextObject();
				
				$photo = $this->file_factory->getClass("Photo",array($contextObject,intval($this->mParams["photoID"])));
			
				if( !($photo instanceof Photo) ){
					$this->_stopOnError(TRUE);
				}
				
				if ( self::ALBUM_CONTEXT == $this->mCollectionHelper->getContext() || self::TRAVALBUM_CONTEXT == $this->mCollectionHelper->getContext() ){
					$contextObject->setAsPhotoAlbumPrimary($photo->getPhotoID());
				}else{
					$photo->setAsPrimaryPhoto();
				}
	
				$this->invalidateCache();
	
				// Added by: Adelbert Silla
				// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
				//           This is a preperation for updating application static views in facebook
				// Date added: Aug. 10, 2009
				// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
				fbNeedToUpdateAccountController::saveNewlyEditedAccount($_SESSION['travelerID'],4);
			}
		}
		
		function _setAsJournalPrimary(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			$this->_redirectIfNotAllowed($this->mCollectionHelper->_isAjaxRequest());
			$this->_restrictIfNotPrivileged(self::VIEWER < $this->mCollectionHelper->getUserLevel());
			
			$this->_stopOnError(self::TRAVELLOG_CONTEXT != $this->mCollectionHelper->getContext()
				|| !isset($this->mParams["photoID"]) 
				|| !is_numeric($this->mParams["photoID"])
				|| !$this->mCollectionHelper->validGenID()
				|| !$this->mCollectionHelper->validContext() );
			
			$collectionDTO = $this->_getCollectionDTO();
			$travellogObject = $collectionDTO->getContextObject();
			
			$travelObject = $travellogObject->getTravel();
			
			$photo = $this->file_factory->getClass("Photo",array($travellogObject,intval($this->mParams["photoID"])));
		
			if( !($photo instanceof Photo) ){
				$this->_stopOnError(TRUE);
			}
			
			$travelObject->setPrimaryphoto($photo->getPhotoID());
			$travelObject->Update();

			$this->invalidateCache();

			// Added by: Adelbert Silla
			// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
			//           This is a preperation for updating application static views in facebook
			// Date added: Aug. 10, 2009
			// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
			fbNeedToUpdateAccountController::saveNewlyEditedAccount($_SESSION['travelerID'],4);
		}
		
		function _loadCropper(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			$this->_redirectIfNotAllowed($this->mCollectionHelper->_isAjaxRequest());
			
			$collectionDTO = $this->_getCollectionDTO();
			$this->mCollectionHelper->setActiveCollectionItem($collectionDTO);
			$this->mCollectionHelper->setParams($this->mParams);
			
			$cropper = $this->file_factory->getClass('ViewPhotoCollectionThumbnailCropper');
			$cropper->setCollectionHelper($this->mCollectionHelper);
			$cropper->render();
		}
		
		function _saveThumbnail(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			$this->_redirectIfNotAllowed($this->mCollectionHelper->_isAjaxRequest());
			$this->_restrictIfNotPrivileged(self::VIEWER < $this->mCollectionHelper->getUserLevel());
			
			$mCollectionDTO = $this->_getCollectionDTO();
			$mPhotoContext = $mCollectionDTO->getContextObject();
			
			$mPhoto = $this->file_factory->getClass('Photo', array($mPhotoContext,$this->mParams["photoID"]));
			$pathManager =$this->file_factory->getClass('PathManager',array($mPhotoContext));
			
			$uploaded = $this->file_factory->invokeStaticClass('PhotoCollectionUploadedPhotoFactory', 'getInstance')->create($this->mCollectionHelper->getContext());
			$uploaded->setNewThumbnail($pathManager,$mPhotoContext,$mPhoto,$this->mParams["x"],$this->mParams["y"],$this->mParams["width"],$this->mParams["height"]);
			
			echo "SUCCESS?".$mPhoto->getFilename();
		}
		
		function _loadRearrangeView(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			$this->_redirectIfNotAllowed($this->mCollectionHelper->_isAjaxRequest());
			
			$collectionDTO = $this->_getCollectionDTO();
			$this->mCollectionHelper->setActiveCollectionItem($collectionDTO);
			
			$rearrangeView = $this->file_factory->getClass('ViewPhotoCollectionRearrangePhotos');
			$rearrangeView->setCollectionHelper($this->mCollectionHelper);
			$rearrangeView->render();
		}
		
		function _savePhotoOrder(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			
			if (self::GROUP_TEMPLATE_TYPE == $this->mCollectionHelper->getType()) {
				$action = new GanetPhotoReArrangePhotosAction(new GanetWebRequest());
				if ($action->validate()) {
					$action->execute();
				}
				else {
					$action->onValidationFailure();
				}
			}
			else {
				$this->_redirectIfNotAllowed($this->mCollectionHelper->_isAjaxRequest());
				$this->_restrictIfNotPrivileged(self::VIEWER < $this->mCollectionHelper->getUserLevel());
	
				$this->_stopOnError(self::JOURNAL_CONTEXT == $this->mCollectionHelper->getContext() 
					&& is_null($this->mCollectionHelper->getTravellogID())
					|| !$this->mCollectionHelper->validGenID()
					|| !$this->mCollectionHelper->validContext() );
				
				$collectionDTO = $this->_getCollectionDTO();
				$collectionDTO->getContextObject()->reArrangePhoto($this->mParams["photoID"]);
			}
		}
		
		private function _updateManageFeaturedAlbumsNotice(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			$this->_restrictIfNotPrivileged(self::VIEWER < $this->mCollectionHelper->getUserLevel()
				&& $this->mCollectionHelper->_isAjaxRequest() );
			$this->_stopOnError( self::GROUP_TYPE != $this->mCollectionHelper->getType()
				|| is_null($this->mCollectionHelper->getID())
				|| !is_numeric($this->mCollectionHelper->getID()) );
			
			$featuredAlbumsView = $this->file_factory->getClass('ViewManageFeaturedAlbums');
			$featuredAlbumsView->setGroup($this->mCollectionHelper->getOwner());
			$featuredAlbumsView->render();
		}
		
		private function _saveFeaturedAlbums(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			$this->_restrictIfNotPrivileged(self::VIEWER < $this->mCollectionHelper->getUserLevel()
				&& $this->mCollectionHelper->_isAjaxRequest() );
			$this->_stopOnError( self::GROUP_TYPE != $this->mCollectionHelper->getType()
				|| is_null($this->mCollectionHelper->getID())
				|| !is_numeric($this->mCollectionHelper->getID())
				|| !isset($this->mParams["albums"]) );
			
			$albumTokens = explode(",",$this->mParams["albums"]);
			foreach($albumTokens AS $token){
				$tokens = explode("_",$token);
				if( 2 != count($tokens) ){
					continue;
				}
				$albumID = $tokens[0];
				$feature = 1 == $tokens[1] ? TRUE : FALSE;
				try{
					$album = $this->file_factory->getClass('PhotoAlbum',array($albumID));
					if( $album instanceof PhotoAlbum && $album->getGroupID() == $this->mID ){
						$album->Feature($feature,$this->mGroup);
					}	
				}catch(exception $e){
				}
			}
		}
		
		private function _featureAlbum(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			$this->_restrictIfNotPrivileged(self::VIEWER < $this->mCollectionHelper->getUserLevel()
				&& $this->mCollectionHelper->_isAjaxRequest() );
			$this->_stopOnError( self::GROUP_TYPE != $this->mCollectionHelper->getType()
				|| is_null($this->mCollectionHelper->getID())
				|| !is_numeric($this->mCollectionHelper->getID())
				|| !isset($this->mParams["feature"]) 
				|| !$this->mCollectionHelper->validGenID() 
				|| !is_numeric($this->mCollectionHelper->getGenID())
				|| self::ALBUM_CONTEXT != $this->mCollectionHelper->getContext() );
				
			try{
				$album = $this->file_factory->getClass('PhotoAlbum',array($this->mCollectionHelper->getGenID()));
				if( $album instanceof PhotoAlbum ){
					$album->Feature(1==$this->mParams["feature"],$this->mGroup);
				}
			}catch(exception $e){
			}
		}
		
		function _loadPopupGallery(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			$this->_redirectIfNotAllowed($this->mCollectionHelper->_isAjaxRequest());
			
			$collectionDTO = $this->file_factory->invokeStaticClass('PhotoCollectionDTOFactory','getInstance')->create($this->mCollectionHelper->getContext());
			$collectionDTO->setGenID($this->mCollectionHelper->getGenID());
			/*
				$isISV = false;
			*/
			if (isset($GLOBALS['CONFIG'])){
				$config = $GLOBALS['CONFIG'];
				//$isISV = $config->getGroupID() == 1760 ? true : false;
				if ($collectionDTO->getType() == PhotoCollectionConstants::GROUP_TYPE){
					$collectionDTO->setID($config->getGroupID());
				}else{
					$collectionDTO->setID($this->mCollectionHelper->getID());
				}
			}
			else{
				$collectionDTO->setType($this->mCollectionHelper->getType());
				$collectionDTO->setID($this->mCollectionHelper->getID());
			}
			
			$popupView = $this->file_factory->getClass('ViewPopupGallery');
			$popupView->setCollectionDTO($collectionDTO);
			$photos = $collectionDTO->getContextObject()->getPhotos();
			$popupView->setPhotos($photos);
			$currentPhotoID = 0;
			if( isset($this->mParams["photoID"]) ){
				$mPhoto = $this->file_factory->getClass('Photo', array($collectionDTO->getContextObject(),$this->mParams["photoID"]));
				if( $mPhoto instanceof $mPhoto ){
					$popupView->setDefaultPhoto($mPhoto);
					$currentPhotoID = $this->mParams["photoID"];
				}
			}else{
				$popupView->setDefaultPhoto($photos[0]);
			}
			$link = "http://".$_SERVER["SERVER_NAME"].$collectionDTO->getLink();
			if ($currentPhotoID){
		    	$link .= "#".$currentPhotoID;
		    }
			$fbLike = urlencode($link);
			$popupView->setFbLikeView($fbLike);
			$popupView->render();
		}
		
		private function _fetchHeaderAfterDeleteAlbum(){
			$this->_checkUserLevel();
			$this->_prepareCollectionHelper();
			$this->_redirectIfNotAllowed($this->mCollectionHelper->_isAjaxRequest());
			$this->_restrictIfNotPrivileged(self::VIEWER < $this->mCollectionHelper->getUserLevel());
			
			$this->_prepareCollectionHelper();
			$this->_prepareCollection();
			
			$tpl = new Template();
			$tpl->set("collectionCount",count($this->mCollectionHelper->getCollection()));
			$tpl->set("photocount",$this->mCollectionHelper->getAllCollectionPhotoCount());
			$tpl->set("type",$this->mCollectionHelper->getType());
			$tpl->set("ID",$this->mCollectionHelper->getID());
			$tpl->out("travellog/UIComponent/NewCollection/views/tpl.ViewPhotoCollectionContentSummaryHeader.php");
		}
		
		//note: _prepareCollectionHelper() must have been invoked before calling this function
		private function _getCollectionDTO($retainJournalContext=FALSE){
			$collectionDTOFactory = $this->file_factory->invokeStaticClass('PhotoCollectionDTOFactory', 'getInstance');
			if( self::JOURNAL_CONTEXT == $this->mCollectionHelper->getContext() && !$retainJournalContext ){
				$collectionDTO = $collectionDTOFactory->create(self::TRAVELLOG_CONTEXT);
				$collectionDTO->setGenID($this->mCollectionHelper->getTravellogID());
			}else{
				$collectionDTO = $collectionDTOFactory->create($this->mCollectionHelper->getContext());
				$collectionDTO->setGenID($this->mCollectionHelper->getGenID());
			}
			$collectionDTO->setType($this->mCollectionHelper->getType());
			$collectionDTO->setID($this->mCollectionHelper->getID());
			
			return $collectionDTO;
		}
		
		private function invalidateCache(){
			require_once('Cache/ganetCacheProvider.php');
			$cache	=	ganetCacheProvider::instance()->getCache();
			if ($cache != null) {
				if( self::TRAVELER_TYPE == $this->mType ){
					$cache->delete("Photo_Collection_Traveler_Owner_".$this->mID);
					$cache->delete("Photo_Collection_Traveler_Public_".$this->mID);
					$cache->delete("Photo_Collection_CBTraveler_Public_".$this->mID);
				}else if( self::GROUP_TYPE == $this->mType ){			
					$cache->delete("Photo_Collection_Group_Admin_".$this->mID);
					$cache->delete("Photo_Collection_Group_Public_".$this->mID);
				}
			}
		}
		
		public static function resolveCollectionHelper(){
			self::$instance->_prepareCollectionHelper();
		}
	}