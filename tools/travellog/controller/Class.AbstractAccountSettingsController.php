<?php

	require_once("travellog/factory/Class.FileFactory.php");
 
	class AbstractAccountSettingsController{
		
		protected $session	= NULL;
		protected $file_factory = NULL;
		protected $config = NULL;
		protected $traveler = NULL;
		
		public function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template', array('path' => ''));
			$this->file_factory->registerClass('SessionManager');
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('Traveler');
			
			$this->file_factory->registerClass('ProfileCompFactory', array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
			$this->file_factory->registerTemplate('LayoutMain');

			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			$this->session = $this->file_factory->invokeStaticClass('SessionManager', 'getInstance');
			
			if( $this->isAjaxRequest() ){
				$this->redirect(404);
			}
			
			if( !$this->session->getLogin() ){
				$this->redirect("/login.php?redirect=/account-settings.php");
			}
			
			$this->traveler = $this->file_factory->getClass("Traveler",array($this->session->get("travelerID")));
		}
		
		public function performAction(){
			$action = isset($_POST["action"]) ? $_POST["action"] : "";
			switch($action){
				case "update":
					$this->update();
					break;
				default:
					$success_message = $this->session->get("ACCOUNT_CHANGE_SUCCESS_NOTICE");
					$success_message = $success_message ? $success_message : "";
					$this->session->unsetVar("ACCOUNT_CHANGE_SUCCESS_NOTICE");
					
					$errors = $this->session->get("ACCOUNT_CHANGE_ERRORS_ARRAY");
					$errors = $errors ? $errors : array();
					$this->session->unsetVar("ACCOUNT_CHANGE_ERRORS_ARRAY");
					
					$this->_default($errors,$success_message);
					break;
			}
		}
		
		protected function update(){
			require_once('travellog/rules/Class.TravelerAccountRules.php');
			
			$travelerID = $this->session->get("travelerID");
			
			$err = array();
				
	 		if(!isset($_GET['ref'])) {
				// read and verify username entry
				if(isset($_POST['username'])){
					if( $this->traveler->getUsername() != $_POST['username'])
						$tempUsernameErr = TravelerAccountRules::getUsernameErrorMessage($_POST['username']);
					if(!empty($tempUsernameErr))
						$err[0] = array_pop($tempUsernameErr); 
				}
			
				// read and verify email entry
			
				// check if two emails are equal
				if(isset($_POST['newemail']) && strlen(trim($_POST['newemail'])) > 0){
					if(trim($_POST['newemail']) != trim($_POST['newemail2'])){
						$err[1]="The retyped email is not equal to the new email!";
					}
					//Replaced eregi with preg_match
					//else if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $_POST['newemail'])) {
					else if(!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i', $_POST['newemail'])) {
				 		$err[1]="Invalid email address in New Email field.";
					}
					else{
						$email 		= $_POST['newemail'];
						if($this->traveler->getEmail() != trim($email)){
							if(!TravelerAccountRules::validateTravelerEmail($email)){
								$err[1]="New Email Address is taken already";
							}
						}
					}
				}
			}
			if (!isset($_POST['newPassword']) || ( !isset($_POST['currentPassword']) && 0 == strlen(trim($_GET['ref'])) ) ){
	 			$this->redirect("/travelerdashboard.php");
			}
			
	 		// check if the typed current password equal to the password stored in traveler object
			
	 		if ((isset($_POST['currentPassword']) && strlen(trim($_POST['currentPassword'])) > 0) && $_POST['currentPassword']!= $this->traveler->getPassword() && !isset($_REQUEST['ref'])) {
	 			$err[3] = "The current password is not valid!";
	 		}
			$success_message = "";
			if(count($err) <= 0){
		 		try {
					// insert code for logger
					require_once('gaLogs/Class.GaLogger.php');
					GaLogger::create()->start($travelerID,'TRAVELER');
					
					$success = "Your password has been changed.";
					if($_POST['newPassword'] != ""){
						$this->traveler->setPassword($_POST['newPassword']);
					 	$this->traveler->updatePassword();
					}
					
					$travelerprofile = $this->traveler->getTravelerProfile();
					$updated = FALSE;
					if( $this->traveler->getUsername() != $_POST['username'] ){
						$travelerprofile->setUserName($_POST['username']);
						$updated = TRUE;
					}
					if($_POST['newemail'] != "" && $_POST['newemail2'] != ""){
						$travelerprofile->setEmail($_POST['newemail']);
						$updated = TRUE;
					}
					if( $updated ){
						$travelerprofile->UpdateAccountSettings();
						$success_message = "Your account settings has been updated.";
					}
		 		}catch (Exception $exp) {
					$err[4] = $exp->getMessage();
		 		}
			}
			
			$this->session->set("ACCOUNT_CHANGE_SUCCESS_NOTICE",$success_message);
			$this->session->set("ACCOUNT_CHANGE_ERRORS_ARRAY",$err);
			
			$this->redirect("/account-settings.php");
		}
		
		protected function _default($errors=array(),$success_message=""){
			$travelerID = $this->session->get("travelerID");
			
			$subNavigation = $this->file_factory->getClass('SubNavigation');
		 	$subNavigation->setContext('TRAVELER');	
		 	$subNavigation->setContextID($travelerID);
		 	$subNavigation->setLinkToHighlight('MY_PROFILE');

			$profile_comp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$TRAVELER_CONTEXT );
			$profile_comp->init($travelerID, AbstractProfileCompController::$EDIT_ACCOUNT_SETTINGS);

			$template = $this->file_factory->getClass("Template");
		
			Template::setMainTemplate($this->file_factory->getTemplate('LayoutMain'));
			Template::setMainTemplateVar('page_location', 'My Passport');
			Template::includeDependentJs('/min/f=js/prototype.js');
			Template::includeDependentJs('/js/interactive.form.js');
			Template::includeDependentJs('/js/account-settings.js');
		
			$twitter_oauth_api = OAuthAPIFactory::getInstance()->create("twitter");
			$twitter_linked = $twitter_oauth_api->isTravelerProfileLinked($travelerID);
			
			$google_oauth_api = OAuthAPIFactory::getInstance()->create("google");
			$google_linked = $google_oauth_api->isTravelerProfileLinked($travelerID);
			
			$facebook_oauth_api = OAuthAPIFactory::getInstance()->create("facebook");
			$facebook_linked = $facebook_oauth_api->isTravelerProfileLinked($travelerID);
		
			$template->set('profile', $profile_comp->get_view());
			$template->set("subNavigation",$subNavigation);
			$template->set("config",$this->config);
			$template->set("traveler",$this->traveler);
			$template->set("errors",$errors);
			$template->set("success_message",$success_message);
			$template->set("twitter_linked",$twitter_linked);
			$template->set("google_linked",$google_linked);
			$template->set("facebook_linked",$facebook_linked);
			$template->out("travellog/views/tpl.AccountSettings.php");
		}
		
		protected function isAjaxRequest(){
			if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) 
				&& strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ){
				return TRUE;
			}
			if(	!empty($_SERVER['HTTP_X_REQUESTED_WITH']) 
				&& strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
				return TRUE;
			}
			return FALSE;
		}
		
		public function redirect($location){
			if( 404 == $location ){
				header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
			}else{
				header("location: ".$location);
			}
			exit;
		}
	}