<?php
require_once("travellog/model/Class.SessionManager.php");
require_once("Class.ToolMan.php");
require_once("travellog/controller/Class.IController.php");
require_once("travellog/factory/Class.FileFactory.php");
//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
abstract class AbstractPostCardController implements IController{
	
	protected static $REQUIRES_ACTION_RULES = array('VIEW', 'SEND');
	
	protected  
	
	$obj_session  = NULL,
	
	$file_factory = NULL;
	
	
	function __construct(){
		$this->obj_session  = SessionManager::getInstance();
		$this->file_factory = FileFactory::getInstance(); 
		$this->file_factory->registerClass( 'TravelLog'        );
		$this->file_factory->registerClass( 'Traveler'         );
		$this->file_factory->registerClass( 'AddressBookEntry' );
		$this->file_factory->registerClass('SubNavigation');
		$this->file_factory->registerClass('GroupApprovedJournals', array('path' => 'travellog/dao'));
		
		
		$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
		$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
		$this->file_factory->registerClass('GroupFactory');
		
		$this->file_factory->registerClass( 'PostCardModel' );
		$this->file_factory->registerClass( 'PhotoTravellog', array('path' => 'travellog/UIComponent/Photo/model/') );
		$this->file_factory->registerClass( 'MainLayoutView', array('path' => 'travellog/views/') );
		$this->file_factory->registerClass( 'PostCardView'  , array('path' => 'travellog/views/') );
		$this->file_factory->registerClass( 'Template'      , array('path' => '')                 );  
		$this->file_factory->registerTemplate( 'LayoutMain'     );
		
		$this->file_factory->setPath('CSS', '/min/f=css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');  
	}
	
	function performAction(){
		//$this->beforePerformAction();
		switch( strtolower($this->data['action']) ){
			case "send":
				$this->__validateForm();
				$this->_send();
			break;
			default:
				$this->__getData();
				$this->_view();
		}
	}
	
	protected function _view(){
		$arr_contents      = array();
		$obj_navigation    = $this->file_factory->getClass('SubNavigation');
		$obj_traveler      = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')) );
		$factory           = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array());
		$address           = '[';
		$this->file_factory->getClass('Template');
		$obj_photo                 = $this->file_factory->getClass('PhotoTravellog', array($this->data['travellogID']));
		$obj_address_book          = $this->file_factory->getClass('AddressBookEntry');
		$arr_contents['col_address_book'] = $obj_address_book->getAddressBookEntriesByOwnerTravelerID($this->obj_session->get('travelerID'));
		$arr_contents['col_friends']      = $obj_traveler->getFriends(); 
		$arr_contents['props']     = $this->data; 
		$arr_contents['photos']    = $obj_photo->getPhotos(); 
		$arr_contents['obj_entry'] = $this->file_factory->getClass('TravelLog', array($this->data['travellogID']));
		$obj_view                  = $this->file_factory->getClass('MainLayoutView');
		$travellogID               = $this->data['travellogID'];	
		if( get_class($arr_contents['obj_entry']->getOwner()) == 'Traveler' ){
			$obj_navigation->setContext('TRAVELER');
			$obj_navigation->setContextID($this->obj_session->get('travelerID'));
			$profile_comp = $factory->create( ProfileCompFactory::$TRAVELER_CONTEXT );
			$profile_comp->init($this->obj_session->get('travelerID'));
		}
		else{
			$obj_navigation->setContext('GROUP');
			$obj_navigation->setGroupType('ADMIN_GROUP'); 
			$obj_navigation->setContextID($arr_contents['obj_entry']->getOwner()->getGroupID());
			$profile_comp = $factory->create( ProfileCompFactory::$GROUP_CONTEXT );
			$profile_comp->init($arr_contents['obj_entry']->getOwner()->getGroupID());
		}
		
		
		$obj_navigation->setLinkToHighlight('MY_JOURNALS');

		$arr_contents['obj_profile_view'] = $profile_comp->get_view();       
		$arr_contents['obj_navigation'] = $obj_navigation;
		
		/*$ctr  = 1;
		foreach($col_address_book as $obj_address){
			$address .= '{ name: "' . $obj_address->getName() . '", to: "'. $obj_address->getEmailAddress() . '" }';
			if( count($col_address_book) != $ctr ) $address .= ', ';
			$ctr++;  
		}
		$address .= '];';*/
		
		$code = <<<BOF
		
		<script type="text/javascript">

		jQuery(document).ready(function() {
		    jQuery('#mycarousel').jcarousel({
		        vertical: true,
		        scroll: 2
		    });
		});
		
		</script>
		
		<script type="text/javascript">   
		//<![CDATA[
			//var addresses   = $address;
			var travellogID = $travellogID; 
			(function($){
				$(document).ready(function(){
					
					
					var popUIEntry = new PopUI(); 
					popUIEntry.set_url                ( '/ajaxpages/journal_entry_page.php' );	   
					popUIEntry.set_form_label_elements( ['contentLabel']                    );
					popUIEntry.set_form_elements      ( ['content','TEXTAREA']              );
					
					
					jQuery('#EntryTitleLink').click(function(){
						popUIEntry.set_form_label_values  ( ['Entry Title']                       );
						popUIEntry.set_hidden_field_value ( 'ENTRYTITLE'                          );
						popUIEntry.set_uses_tinymce       ( false                                 );
						popUIEntry.set_querystring        ( 'tID='+travellogID+'&type=ENTRYTITLE' );
						popUIEntry.set_height             ( ['14px']                              );
						popUIEntry.set_on_save_callback   ( function(field, content){jQuery('#NEW'+field).val(content)} );
						
						popUIEntry.render();
					});
			
					jQuery('#EntryCalloutLink').click(function(){
						popUIEntry.set_form_label_values  ( ['Entry Callout']                       );
						popUIEntry.set_hidden_field_value ( 'ENTRYCALLOUTCONTENT'                   );
						popUIEntry.set_uses_tinymce       ( false                                   );
						popUIEntry.set_querystring        ( 'tID='+travellogID+'&type=ENTRYCALLOUT' );
						popUIEntry.set_height             ( ['14px']                                );
						popUIEntry.set_on_save_callback   ( function(field, content){jQuery('#NEW'+field).val(content)} );
						
						popUIEntry.render();
					});
					
					/*$("#mytextarea").autocomplete(addresses, {
				 		multiple     : true,
				 		width        : 400,
				 		minChars     : 0,
						matchContains: true,
						autoFill     : true,
				 		formatItem   : function(row, i, max) {
							return '"' + row.name + '" <' + row.to + '>'; 
						},
						formatResult : function(row) {
							return  row.name + ' <' + row.to + '>';  
						}
					});*/
					
					$.showCallout = function(){
						$('#editable-callout-form').attr('style', 'display:block');
						$('#editable-callout').attr('style', 'display:none');
					},
					
					$.updateCallout = function(){
						$('#editable-callout-form').attr('style', 'display:none');
						$('#editable-callout').attr('style', 'display:block');
						$('#jeditable-entry-callout').html($('#callout').val());
					}
				});
			})(jQuery);
			
			
			function html_entity_decode(str) {
			  return str.replace(/</g,"&lt;").replace(/>/g,"&gt;");
			}
			
			function html_entity_encode(str) {
			  return str.replace(/&lt;/g,"<").replace(/&gt;/g,">");
			}
			
		//]]>  
		</script>
BOF;
		//PREPARE POSTCARD VIEW
		$obj_postcard_view = $this->file_factory->getClass('PostCardView');
		$obj_postcard_view->setContents( $arr_contents );
		//END POSTCARD VIEW
		

		
		Template::includeDependentCss( "/min/f=css/Thickbox.css"                      );
		Template::includeDependentCss( '/min/f=css/postcard.css'                      ); 
		Template::includeDependentCss( '/min/f=css/jquery.jcarousel2.css'             ); 
		Template::includeDependentCss( '/js/autocomplete/jquery.autocomplete.css'     ); 		
		
		Template::setMainTemplateVar ( 'title', $obj_traveler->getUsername().' - Journals');
		Template::setMainTemplateVar ( 'metaDescription', ''                          );	
		Template::setMainTemplate    ( $this->file_factory->getTemplate('LayoutMain') );
		Template::setMainTemplateVar ( 'layoutID', 'journals'                         );
		Template::includeDependent   ( $code                                          );
		Template::includeDependentJs ( '/min/g=PostcardJs' );		
		//Template::includeDependentJs ( '/js/jquery.jcarousel.pack.js'                 );		
		//Template::includeDependentJs ( '/js/autocomplete/lib/jquery.bgiframe.min.js'  );
		//Template::includeDependentJs ( '/js/autocomplete/lib/jquery.dimensions.js'    );
		//Template::includeDependentJs ( '/js/autocomplete/lib/jquery.ajaxQueue.js'     ); 
		//Template::includeDependentJs ( '/js/autocomplete/jquery.autocomplete.js'      );
		//Template::includeDependentCss( '/js/autocomplete/jquery.autocomplete.css'     ); 
		//Template::includeDependentJs ( "/js/date.js"              , array('bottom'=> true));
		//Template::includeDependentJs ( "/js/jquery.datePicker.js" , array('bottom'=> true));
		//Template::includeDependentJs ( "/js/jquery.checkbox.js"   , array('bottom'=> true));
		//Template::includeDependentJs ( "/js/entry_location.js"    , array('bottom'=> true));
		//Template::includeDependentJs ( "/js/thickbox.js"          , array('bottom'=> true));
		//Template::includeDependentJs ( "/js/tiny_mce/tiny_mce.js" , array('bottom'=> true));
		//Template::includeDependentJs ( "/js/popUI.js"             , array('bottom'=> true));
		//Template::includeDependentJs ( "/js/custompopup-1.2.js"   , array('bottom'=> true));
		
		
		$arr_contents['contents'] = $obj_postcard_view;
		
		$obj_view->setContents( $arr_contents );
		
		echo $obj_view->render();
	}
	
	protected function _send(){
		$heightAttribute = '';
		$widthAttribute = '';
		if( strlen(trim($this->data['photolink'])) ){
			$size = list($width, $height, $type, $attr) = @getimagesize(trim($this->data['photolink']));
			$targetSize = 420;
			if ( $size ){		
				//get proper rescaling ratio to find proper adjusted dimension
				$ratio = 1;
				if ( $width > $targetSize && $width == $height ){//square
					$ratio = $targetSize / $width;
				}else if ( $width > $targetSize && $width > $height ){//landscape
					$ratio = $targetSize / $width;
				}else if ( $height > $targetSize && $height > $width && $width > $targetSize){//portrait, rescale only if width exceeds targetsize
					$ratio = $targetSize / $width;
				}
				$height = round($height*$ratio);
				$width = round($width*$ratio);
				
				$heightAttribute = 'height = "'.$height.'"';
				$widthAttribute = 'width = "'.$width.'"';
			}
		}
		$photolink       = ( strlen(trim($this->data['photolink'])) )? '<img src="'.$this->data['photolink'].'" border="0" align="right" '.$heightAttribute.' '.$widthAttribute.'/>' : '';
		$title           = $this->data['title'];
		$message         = $this->data['message'];
		$callout         = $this->data['callout'];
		$country_id      = isset($this->data['countryID']) ? $this->data['countryID'] : 0; 				// if condition added by te daphne: 12-04-08
		$to              = $this->data['to'];  
		$addresses       = ( isset($this->data['addresses']) )? $this->data['addresses'] : '';
		$travellogID     = $this->data['travellogID'];
		$obj_entry       = $this->file_factory->getClass('TravelLog', array($this->data['travellogID']));
		$obj_trip        = $obj_entry->getTrip();
		$obj_location    = $obj_trip->getLocation();
		
		if( !is_array($addresses) &&  strlen(trim($to)) == 0 ){
			ToolMan::redirect('/postcards.php?travellogID=' . $travellogID);
			exit;
		}
		
		if( get_class($obj_location) == 'Country' ){
			$location    = $obj_location->getName();

			// added by bert (Dec. 22, 2008)
			$country_id = $obj_location->getCountry()->getCountryID();
						
		}
		else{
			$country_id = $obj_location->getCountry()->getCountryID();
			$country    = $obj_location->getCountry()->getName();
			$city       = ( $obj_location->getName() != $country )? $obj_location->getName() : '';
			
			$location   = ( strlen(trim($city)) )? $city . ', ': '';
			$location  .= $country;
		} 
		
		$arrival         = $obj_trip->getArrival();
		$obj_traveler    = $obj_entry->getTravel()->getTraveler();
		$obj_journalowner= $obj_entry->getTravel()->getOwner();
		$obj_profile     = $obj_traveler->getTravelerProfile();
		$obj_location    = $obj_traveler->getPresentLocation();
		$username        = $obj_traveler->getUsername();
		$travelerID      = $obj_traveler->getTravelerID();

		if ($obj_location){
			if( get_class($obj_location) == 'Country' ){
				$curr_location   = $obj_location->getName(); 
				$curr_country_id = $obj_location->getCountryID();
			}
			else{
				$curr_location   = $obj_location->getCountry()->getName(); 
				$curr_country_id = $obj_location->getCountry()->getCountryID();
			}
		}
		
		$user_photo      = $obj_profile->getPrimaryPhoto()->getPhotoLink('thumbnail');
		$obj_journal     = $obj_entry->getTravel();
		$arr_obj_owner   = $this->file_factory->invokeStaticClass('GroupApprovedJournals',
							'getRelatedGroups',array($obj_journal->getTravelID()));
		$site            = false;
		$group_id        = '';
		$group_name      = '';
		$group_photo     = '';
		$more_journals   = 0;
		$group_contents    = '';
		
		// if this postcard is sent from main .net and
		//  entry is owned by traveler and there's any related groups, show group bug(s)
		if (!isset($GLOBALS['CONFIG']) && 'Traveler' == get_class($obj_journalowner) && 0 < count($arr_obj_owner)):
			$group_contents .= '<div class="groupside"><ul>';
			foreach($arr_obj_owner as $owner):	
				$site 			= $owner->getServerName();
				$group_id      = $owner->getGroupID();
				$group_name    = $owner->getName();
				$group_photo   = $owner->getGroupPhoto()->getPhotoLink('thumbnail');
				$more_journals = $this->file_factory->invokeStaticClass('GroupApprovedJournals',
									'hasMoreThanOneJournal',array($group_id));

				if( $site != false ):		
					$siteURL = "http://".$site;
					$journalsURL = "http://".$site."/journal.php";
				else:		
					$siteURL = "/group.php?gID=".$group_id;
					$journalsURL = "journal.php?action=groupJournals&gID=".$group_id;
				endif;
				
				$group_contents .= '<li style="clear: both;float:right;font-size:13px;margin-top:10px;list-style-image:none;list-style-position:outside;list-style-type:none;">';
				$group_contents .= '<img src="'.$group_photo.'" border="0" align="right" width="37" height="37" style="float:right;padding-left:10px;"/>';
				$group_contents .= '<div style="margin:0;padding:0;float:right;text-align:right;"><a href="'.$siteURL.'" style="color:#999;text-decoration:none;">'.$group_name.'</a><br/>';
				if( $more_journals ) $group_contents .= '<a href="'.$journalsURL.'" style="color:#aaa;font-size:11px;text-decoration:none;">view other journals</a></div>';
				$group_contents .= '</li>';
				
			endforeach;
			$group_contents .= '</ul></div>';
		endif;
	
		// if this is a traveler journal and currlocation is available, show current location flag
		$currloc_img  = '';		
		if( 'Traveler' == get_class($obj_journalowner) && isset($curr_country_id) && 0 < $curr_country_id) {	
			$currloc_img = '<img src="http://images.goabroad.com/images/flags/flag'.$curr_country_id.'.gif" alt="Travel notes from '. $curr_location . '" width="22" height="11" style="margin:0;padding:0;float:right;"/>';			
		}
		
		require_once("class.phpmailer.php");
		$obj_traveler = $this->file_factory->getClass('Traveler', array( $this->obj_session->get('travelerID') )); 


		$emailBodyArgs = array(
			'photolink' => $photolink,
			'title'		=> $title,
			'location' => $location,
			'arrival'  => $arrival,
			'callout'  => $callout,
			'travelerID' => $travelerID,
			'message' => $message,
			'user_photo' => $user_photo,
			'username' => $username,
			'country_id' => $country_id,			
			'curr_location' => (isset($curr_location)) ? $curr_location : '',
			'curr_country_id' => (isset($curr_country_id)) ? $curr_country_id : 0,
			'group_contents' => $group_contents,
			'currloc_img' => $currloc_img,
			'entry' => $obj_entry
		);
		
		//if( $obj_traveler->isAdvisor() ){
		if( 'AdminGroup' == get_class($obj_journalowner)) {	
			$group = $this->file_factory->invokeStaticClass ('GroupFactory','instance')->create(array($obj_traveler->getAdvisorGroup()));
			$emailBodyArgs = array_merge($emailBodyArgs,array('group_name' => $group[0]->getName(),'group_id' => $obj_traveler->getAdvisorGroup(),'user_photo' => $group[0]->getGroupPhoto()->getPhotoLink('thumbnail')));	
		}	
		
			try{	
				$mail = new PHPMailer();
				$mail->IsSMTP();
				$mail->IsHTML(true);
				$mail->From      = ( isset($GLOBALS['CONFIG']) && is_object($GLOBALS['CONFIG']) ) ? $GLOBALS['CONFIG']->getAdminEmail():  'admin@goabroad.net';
				$mail->FromName  = ( isset($GLOBALS['CONFIG']) && is_object($GLOBALS['CONFIG']) ) ? $GLOBALS['CONFIG']->getEmailName(): 'The GoAbroad Network Team';
				$subject_name 	  = (isset($emailBodyArgs['group_name'])) ? $emailBodyArgs['group_name'] : $obj_traveler->getTravelerProfile()->getFirstname() . ' ' . $obj_traveler->getTravelerProfile()->getLastname();
			
				// if traveler context and empty firstname/lastname (for advisor turned travelers), use username instead
				if (!isset($emailBodyArgs['group_name']) && 0 == strlen(trim($subject_name)) )
					$subject_name = $obj_traveler->getUserName();
				$site_name		  = ( isset($GLOBALS['CONFIG']) && is_object($GLOBALS['CONFIG']) ) ? $GLOBALS['CONFIG']->getSiteName(): 'GoAbroad Network';
				$mail->Subject   = $subject_name . ' has sent you a Postcard on '.$site_name;
				$user_email      = $obj_traveler->getTravelerProfile()->getEmail();
				$username        = $obj_traveler->getUsername();
				$email_addresses = array();
				$mail->AddReplyTo(trim($user_email));
				
				if( strlen(trim($to)) ) 
					$email_addresses = explode(',', $to);
				
				if( is_array($addresses) ) 
					$email_addresses = array_merge($addresses,$email_addresses);
				
				
				foreach( $email_addresses as $email_address ){ 
					
					if( strpos($email_address, '^') ) 
						$arr_address = explode('^', $email_address);
					else{
						$arr_address[0] = '';
						$arr_address[1] = $email_address;
					}	
						  
					$email = $arr_address[1];
					$name  = $arr_address[0];
					
					$mail->Body = self::getMailBody($emailBodyArgs,$email);
					$mail->AddAddress(trim($email));
					$succ = $mail->Send();
					$mail->ClearAddresses();
				} 
			}
			catch(exception $e){
				$succ=false;
			}
			//if( $succ )
				$this->obj_session->set('custompopup_message',"<h3>Postcard has successfully been sent.</h3>");
			//else
			//	$this->obj_session->set('custompopup_message',"<h3>Postcard has successfully been sent.</h3>");
			ToolMan::redirect('/postcards.php?travellogID=' . $travellogID);
	}
	
	static function getMailBody($args = array(), $email = '') {		

		$name        = Traveler::getFullNameByEmail($email) != '' && $args['message'] != '' ? "Dear ".ucwords(Traveler::getFullNameByEmail($email))."," : 'Hi!';
		$server_name = $_SERVER['SERVER_NAME'];
		$photolink = $args['photolink'];
		$title = $args['title'];
		$location = $args['location']; 		
		$arrival = date('M d, Y',strtotime($args['arrival']));
		$callout = $args['callout'];
		$travelerID = $args['travelerID'];
		$entry_link = "http://".$server_name."/journal-entry.php?action=view&amp;travellogID=" . $args['entry']->getTravelLogID();
		$message = $args['message'];
		$user_photo = $args['user_photo'];
		$username = (isset($args['group_name'])) ? $args['group_name'] : $args['username'];
		$country_id = $args['country_id'];
		$curr_location = $args['curr_location'];
		$curr_country_id = $args['curr_country_id'];
		$group_contents = $args['group_contents'];
		$currloc_img = $args['currloc_img'];		
		$callout_all = (strlen(trim($callout))) ? '<p style="color:#BBBBBB;font-size:14px;padding:10px 0;">"' . $callout . '"</p>' : '';
		
		$link = (isset($args['group_id'])) ? 'group.php?gID='.$args['group_id'] : $args['username'];
		
		$footerImage = 'http://images.goabroad.net/images/bottom_imagelogo.jpg';

		if( isset($GLOBALS['CONFIG']) && is_object($GLOBALS['CONFIG']) ){
			require_once('Class.ToolMan.php');
			$cobrandFooterImage = ToolMan::getAbsolutePath('../../../goabroad.net/custom/'.$GLOBALS['CONFIG']->getFolderName().'/images/bottom_imagelogo.jpg');			
			$footerImage = file_exists($cobrandFooterImage) ? 'http://www.goabroad.net/custom/'.$GLOBALS['CONFIG']->getFolderName().'/images/bottom_imagelogo.jpg' : $footerImage;
		}
		
		$html = <<<BOF
					<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
					<html>
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
					<title>Email Postcard Version</title>
					</head>
					
					<style type="text/css">
						body {
							background-color:#e5e6e7;
						}
					</style>

					<body bgcolor="#e5e6e7" style="background-color:#e5e6e7">
					
			<div style="background-color:#e5e6e7;padding:10px;">
			
			<table cellpadding="0" cellspacing="0" bgcolor="#e5e6e7" style="background-color:#e5e6e7;margin:0 auto;padding:10px;color:#000000;line-height:normal;font-family: lucida sans unicode,lucida grande,verdana,sans-serif;font-weight:normal;font-size: 12px;line-height:20px;">
				<tr>
					<td style="margin:0 auto;width:726px;">			

						<table cellpadding="0" cellspacing="0">
							<tr>			
								<td style="height:8px;overflow:hidden;width:726px;">
									<img src="http://images.goabroad.net/images/top_imagepos.jpg" border="0" align="left" height="10">
								</td>
							</tr>
						</table>	


						<table cellpadding="0" cellspacing="0" style="margin:0 auto;width:726px;" bgcolor="#ffffff">
							<tr>
								<td style="background-color:#ffffff;height:100%;margin:0 auto;overflow:auto;padding:14px 20px;width:726px">
								
									<span style="float:right;padding:0 0 0 10px;">$photolink</span>	
									
									<h1 style="font-size:2em;font-weight:normal;line-height:normal;margin:20px 0 5px;padding:0;">$title</h1>
									
									<p style="color:#777777;margin:0;padding:0;"><img width="22" height="11" alt="$location" src="http://images.goabroad.com/images/flags/flag$country_id.gif" style="margin:0;padding-top:3px;float:left;"/>&nbsp;$location | $arrival</p>

									$callout_all

									<a style="text-decoration:none;color:#1387c4;" href="$entry_link">
										<img src="http://images.goabroad.net/images/mid_connectpos.jpg" border="0" align="left" alt="You can view my full journal entry here">
									</a>
									
									<br clear="all">

									<h2 style="font-size:2em;font-weight:normal;line-height:normal;padding:0;color:#ff8e00;margin:20px 0 15px;">$name</h2>
							
									<p style="float:left;line-height:24px;margin:0;padding:0;">$message</p>
									
									<br clear="all">

									<span style="float:right;font-size:13px;font-weight:bold;margin-top:30px;width:300px;">
										<img src="$user_photo" border="0" align="right" width="37" height="37" style="float:right;
margin:0;padding:0 0 0 10px;"/>
										<p style="float:right;padding:0;margin:0;text-align:right;"><a style="color:#1387C4;text-decoration:none;" href="http://$server_name/$link">$username</a><br/>
										$currloc_img
										</p>
									</span>

									$group_contents
									
									<br clear="all">
									<img src="$footerImage" border="0" align="left" height="153" width="679">
								
								</td>
							</tr>
						</table>
						
							<table cellpadding="0" cellspacing="0">
								<tr>
									<td class="bottomsection" style="overflow:hidden;width:726px;">
										<img src="http://images.goabroad.net/images/bottom_imagepos.jpg" border="0" align="left" height="10">
									</td>
								</tr>
							</table>


					</td>

				</tr>			

		</table>

</div>

					</body>


					</html>
BOF;

	return $html;

	}
	
	protected function __getData(){
		$this->data['to']        = ( isset($this->data['to'])        )? $this->data['to']        : '';
		$this->data['addresses'] = ( isset($this->data['addresses']) )? $this->data['addresses'] : '';
		$this->data['message']   = ( isset($this->data['message'])   )? $this->data['message']   : '';  	
	}
	
	protected function __validateForm(){
		
	}
	
	protected function __applyRules(){
		if( in_array( strtoupper($this->data['action']), AbstractPostCardController::$REQUIRES_ACTION_RULES) && !$this->obj_session->isSiteAdmin() ){
			// TODO: If some actions requires a more complex rules use a rule object or method instead  
			switch( strtolower($this->data['action']) ){
				case 'view':
					if( !$this->obj_session->getLogin() || !isset($this->data['travellogID']) ) ToolMan::redirect('/journal.php');
					$obj_entry = $this->file_factory->getClass('TravelLog', array($this->data['travellogID']));
					if( $obj_entry->getTrip()->getTravelerID() != $this->obj_session->get('travelerID') ) ToolMan::redirect('/journal.php');
				break;
				case 'send':
					if( !$this->obj_session->getLogin() || !isset($this->data['travellogID']) ) ToolMan::redirect('/journal.php');
					$obj_entry = $this->file_factory->getClass('TravelLog', array($this->data['travellogID']));
					if( $obj_entry->getTrip()->getTravelerID() != $this->obj_session->get('travelerID') ) ToolMan::redirect('/journal.php');
				break;
			}
		}
	}
}
?>