<?php
require_once('travellog/controller/Class.AbstractPagesController.php');
class GANETPagesController extends AbstractPagesController{
	
	function performAction(){
		$this->data           = array_merge($_GET, $_POST);
		$this->data['action'] = ( isset($this->data['action']))? $this->data['action'] : 'view'; 
		parent::performAction();   
	}
}
?>
