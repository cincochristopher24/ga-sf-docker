<?php
require_once('vendor/autoload/GanetAutoloader.php');
require_once("travellog/model/Class.SessionManager.php");
class TwitterWidgetPostObserver {
	
	const CONTEXT = 'twitter';
	
	/* $url - format must be e.g.: /journal-entry.php?travelerID=123&journalID=123&entryID=123
	 * 
	 * 
	 */ 
	public static function post($url){
		GanetAutoloader::getInstance()->register();
		GanetPropelRuntime::getInstance()->initPropel();
		
		$travelerID = SessionManager::getInstance()->get('travelerID');
	//	echo $travelerID; exit;
		$trueURL = 'http://'.$_SERVER['SERVER_NAME'].$url;
		$apiObj = OAuthAPIFactory::getInstance()->create(self::CONTEXT);
		$apiObj->post($trueURL, $travelerID);
	}
}