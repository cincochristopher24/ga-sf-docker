<?php
	
require_once("travellog/controller/Class.AbstractMessagesController.php");
	 
class GANETMessagesController extends AbstractMessagesController{
	
	protected function setGroupPageLocation() {
		Template::setMainTemplateVar('page_location', 'Groups / Clubs');
	}
	
	protected function getMainTemplate() {
		return 'travellog/views/tpl.LayoutMain.php';
	}
	
	protected function getSiteName() {
		return "GoAbroad Network";
	}
	
	protected function loadMessagesManager($id) {
		return gaMessagesManagerRepository::load($id, 1);
	}
	
	protected function wrapSentMessage(gaPersonalMessage $message) {
		return new gaMainPersonalMessageAdapter($message);
	}
	
	protected function loadGroupMessagesManager($id, gaGroup $group) {
		return gaMessagesManagerRepository::loadGroup($id, 1, $group);
	}
}