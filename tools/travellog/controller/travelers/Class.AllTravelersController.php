<?php
require_once("travellog/controller/travelers/Class.AbstractTravelersController.php");
require_once("travellog/views/travelers/Class.TravelersView.php");
require_once('travellog/model/Class.TravelerSearchCriteria2.php');
require_once("Class.Criteria2.php");
class AllTravelersController extends AbstractTravelersController{
	
	function retrieve( &$props ){
		$obj_traveler_search = new TravelerSearchCriteria2;
		$c                   = new Criteria2();
		$query_string        = 'action=all';
		
		if( $this->obj_session->get('isLogin') ) $c->mustNotBeEqual( 'travelerID', $this->obj_session->get('travelerID') ); 
		$c->setOrderBy( 'ranking DESC'                                        );
		$c->setLimit  ( $props['offset'] . ' , ' . $props['rows'] );
		
		$this->props['col_travelers']= $obj_traveler_search->getAllTravelers($c);
		$this->props['obj_paging']   = new Paging( 
													$obj_traveler_search->getTotalRecords(), 
													$props['page'],
													$query_string, 
													$props['rows'] 
								   	 		     );
		$props['obj_iterator'] = new ObjectIterator( $props['col_travelers'], $props['obj_paging'], true );
	}
	
	function retrieveView( $props ){
		
		$this->beforeRender();
		
		$this->retrieve( $props );
		$this->props = $props;
		
		$this->obj_view = new TravelersView;
		$this->obj_view->setContent( $props );
		
		$this->afterFilter();
		
		//$this->main_tpl->set( 'mainContent', $this->main_tpl->fetch("travellog/views/tpl.ViewTravelersList.php") );
		//$this->main_tpl->out("travellog/views/tpl.Travelers.php");	
		return $this->obj_view; 
	}
		
}
?>
