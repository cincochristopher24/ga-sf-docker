<?php
	
require_once("travellog/controller/Class.AbstractSPSyncAccountController.php");
	 
class GANETSPSyncAccountController extends AbstractSPSyncAccountController{
	
	public function __construct(){
		parent::__construct();
		
		$this->file_factory->registerClass('Traveler', array("path" => "travellog/model"));
	}
	
	protected function autoLogin($travelerID=0){
		if( !$travelerID ){
			return FALSE;
		}
		$traveler = $this->file_factory->getClass("Traveler",array($travelerID));
		
		$this->file_factory->registerClass('GANETSPOAuthLoginController', array('path' => 'travellog/controller/'));
		$controller = $this->file_factory->getClass("GANETSPOAuthLoginController");
		
		$this->redirectAfterLogin = "TRAVELER_DASHBOARD";
		
		return $controller->startTravelerSession($travelerID);
	}
}