<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */

require_once('travellog/service/PhotoService.php');
require_once('Class.Template.php');
require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
require_once('travellog/model/Class.Photo.php');

class PhotoRequestSHOWROTATE{
	
	function __construct(FileFactory $FFactory){
		
		$template 		= $FFactory->getClass('Template');
		$Subnavigation 	= $FFactory->getClass('SubNavigation');
		
		new PhotoFactory($_REQUEST['genID'],$_REQUEST['cat']);
		
		$sessMan =	$FFactory->getClass('SessionManager');
		$travelerID = $sessMan->get('travelerID');
		
		
		$_REQUEST['travelerID'] = $travelerID;
		
		
		if(!PhotoFactory::create()->isOwner($travelerID))
			return header("Location:http://".$_SERVER['SERVER_NAME']."/index.php");
		
		$Photo =  new Photo(PhotoFactory::create()->getPHOTOcontext(),$_REQUEST['photoID']);
		
		$template->set_vars(array(
				'genID'			=>$_REQUEST['genID'],	
				'context'		=>$_REQUEST['cat'],
				'loginID'		=>$travelerID,
				'uploadphoto'		=>"photomanagement.php?action=add&cat=".$_REQUEST['cat']."&genID=".$_REQUEST['genID'],
				'viewallphotolink'	=>"photomanagement.php?action=vfullsize&cat=".$_REQUEST['cat']."&genID=".$_REQUEST['genID']."&photoID=".$_REQUEST['photoID'],
				'owner'			=>PhotoFactory::create()->isOwner($travelerID),
				'photolink'		=>$Photo->getPhotoLink(),
				'photoid'		=>$_REQUEST['photoID'],
				'headercaption'	=>PhotoFactory::create()->getHeaderCaption()));				
				
				$template->set('subNavigation',PhotoFactory::create()->getSUBNavigation($Subnavigation,$travelerID));
			
		echo $template->out('tpl.RotatePhoto.php');		
	}
	
}


?>
