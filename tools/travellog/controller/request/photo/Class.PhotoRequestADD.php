<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */

require_once('travellog/model/Class.Traveler.php');
require_once('travellog/service/PhotoService.php');
require_once('Class.Template.php');
require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
require_once("travellog/model/Class.UploadManager.php");
require_once('travellog/model/Class.PathManager.php');
require_once("travellog/UIComponent/Photo/model/Class.ParsePERLUploadParams.php");
require_once("travellog/UIComponent/Photo/model/Class.PhotoManager.php");
require_once("travellog/UIComponent/Photo/command/Class.PhotoJournalLogCommand.php");
require_once("travellog/UIComponent/Photo/command/Class.PhotoTravelLastUpdatedCommand.php");
require_once("travellog/UIComponent/Photo/command/Class.PhotoNotifierCommand.php");

class PhotoRequestADD{
	
	function __construct(FileFactory $FFactory){
		
		
		//ini_set("display_errors", true);
		//error_reporting(E_ALL);
		
		
		$template 		= $FFactory->getClass('Template');
		$Subnavigation 	= $FFactory->getClass('SubNavigation');
			
		new PhotoFactory($_REQUEST['genID'],$_REQUEST['cat']);
		
		$sessMan =	$FFactory->getClass('SessionManager');
		$travelerID = $sessMan->get('travelerID');
		
		
		$_REQUEST['travelerID'] = $travelerID;
		
		
		if(!PhotoFactory::create()->isOwner($travelerID))
			return header("Location:http://".$_SERVER['SERVER_NAME']."/index.php");
				
		
		$error 	= NULL;
		$new  	= false;
		$submit = false;	
		
		if($_REQUEST['cat'] == 'group' && isset($_REQUEST['new']))
			$new = true;
		
		$obj_traveler = new Traveler($travelerID);
		
		if(isset($_REQUEST['tmp_sid'])){								
			
			new ParsePERLUploadParams($_REQUEST['tmp_sid']);
			
			// insert code for logger system
			//require_once('gaLogs/Class.GaLogger.php');
			//GaLogger::create()->start($travelerID,'TRAVELER');
			
			$path = new PathManager(PhotoFactory::create()->getPHOTOcontext());	
			
			$U_Manager = new UploadManager();
			$U_Manager->setContext($_REQUEST['cat']);			
			$U_Manager->setDestination($path);
			$U_Manager->setFiles(ParsePERLUploadParams::getFiles());
			$U_Manager->setTypeUpload('image');
			$U_Manager->upload();		
			
			$P_Manager = new PhotoManager();
			$P_Manager->setLoginID($travelerID);
			$P_Manager->setGenID($_REQUEST['genID']);
			$P_Manager->setContext($_REQUEST['cat']);
			$P_Manager->setIsGroupChecked(ParsePERLUploadParams::isGroupNotified());
			$P_Manager->setIsAddressChecked(ParsePERLUploadParams::isAddressBookNotified());
			
			//set Command to execute after new Photo Uploaded
			$P_Manager->addCommand(new PhotoJournalLogCommand);			//Log to Journal
			$P_Manager->addCommand(new PhotoTravelLastUpdatedCommand);	//update Travel
			$P_Manager->addCommand(new PhotoNotifierCommand);			//Send Notice
				
			$P_Manager->Save($U_Manager);

			
			if($P_Manager->getValidFiles())	
				
				if($_REQUEST['cat']== "group"){										
					header("Location:http://".$_SERVER['SERVER_NAME']."/group.php?gID=".$_REQUEST['genID']);																
				}elseif($_REQUEST['cat']== "resume"){
					header("Location:http://".$_SERVER['SERVER_NAME']."/resume.php?action=view");																
				}else{
					header("Location:http://".$_SERVER['SERVER_NAME']."/photomanagement.php?cat=".$_REQUEST['cat']."&action=view&genID=".$_REQUEST['genID']);																
					
				}
			else
				$error = $P_Manager->getInvalidFiles();
		}
		
											
		$template->set_vars(array('error' =>$error,
								  'submit' =>$submit,
								  'newgroup'=>$new,
								  'varpic'=>'pictures[]',
								  'defaultnumberofpic'=>PhotoFactory::create()->NUMPHOTOSTOUPLOAD,
								  'cat' =>$_REQUEST['cat'],
								  'action' =>'add',
								  'genID'  =>$_REQUEST['genID'],
								  'loginID'		=>$travelerID,
								  'isAdvisor'	=>$obj_traveler->isAdvisor(),
								  'isAdministrator' =>$obj_traveler->isAdministrator(),
								  'photocount' => PhotoFactory::create()->getCOUNTPhotos(),
								  'backlink'=>PhotoFactory::create()->getBackLink(),	
								  'backcaption'=>PhotoFactory::create()->getBackCaption()	
								  )); 
								  
		$template->set('subNavigation',PhotoFactory::create()->getSUBNavigation($Subnavigation,$travelerID));
		
		echo $template->out('tpl.AddPhoto.php');	
		
		
			
	}
	
}


?>
