<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */

require_once('travellog/service/PhotoService.php');
require_once('Class.Template.php');
require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");

class PhotoRequestARRANGE{
	
	function __construct(FileFactory $FFactory){
		
		$template 		= $FFactory->getClass('Template');
		$Subnavigation 	= $FFactory->getClass('SubNavigation');
		
		$sessMan =	$FFactory->getClass('SessionManager');
		$travelerID = $sessMan->get('travelerID');
		
		
		$_REQUEST['travelerID'] = $travelerID;
		
		
		$request = array();
		$request['context'] 	= $_REQUEST['cat'];
		$request['loginid'] 	= $travelerID;
		$request['genid'] 		= $_REQUEST['genID'];
		
		$PhotoService = new PhotoService($request);
		$PhotoService->setTemplateViews($template->getPath());
		
		$Pfactory = new PhotoFactory($request['genid'],$request['context']);
		
		if(!PhotoFactory::create()->isOwner($travelerID))
			return header("Location:http://".$_SERVER['SERVER_NAME']."/index.php");
		
		
		if(isset($_REQUEST['rearrange'])){	
			require_once("travellog/UIComponent/Photo/model/Class.PhotoManager.php");										
			$photoIDs = explode(",",$_POST['txtposition']);											
			PhotoManager::rearrange($Pfactory,array_reverse($photoIDs));
			header("Location:photomanagement.php?cat=".$_REQUEST['cat']."&action=view&genID=".$_REQUEST['genID']);
		}
		
		$template->set_vars(array(
						'uploadphoto'		=> "photomanagement.php?action=add&cat=".$_REQUEST['cat']."&genID=".$_REQUEST['genID'],
						'rearrangephoto'	=> "photomanagement.php?action=arrange&cat=".$_REQUEST['cat']."&genID=".$_REQUEST['genID'],	
						'photo'				=> PhotoFactory::create()->getPhotos(),
						'reccount'			=> PhotoFactory::create()->getCOUNTPhotos(),
						'owner'				=> PhotoFactory::create()->isOwner($request['loginid']),
						'headercaption'		=> PhotoFactory::create()->getHeaderCaption()));	
		
		$template->set('subNavigation',PhotoFactory::create()->getSUBNavigation($Subnavigation,$request['loginid']));
		
		$template->set('rearrangephoto_action',"photomanagement.php?action=arrange&cat=".$_REQUEST['cat']."&genID=".$_REQUEST['genID']);
		echo $template->out('tpl.ReArrangePhoto.php');	
					
	}
	
}


?>
