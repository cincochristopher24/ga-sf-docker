<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */

require_once('travellog/model/Class.Traveler.php');
require_once('travellog/service/PhotoService.php');
require_once('Class.Template.php');
require_once("travellog/UIComponent/Photo/factory/Class.PhotoFactory.php");
require_once("travellog/model/Class.UploadManager.php");
require_once('travellog/model/Class.PathManager.php');
require_once("travellog/UIComponent/Photo/model/Class.ParsePERLUploadParams.php");
require_once("travellog/UIComponent/Photo/model/Class.PhotoManager.php");
require_once("travellog/UIComponent/Photo/command/Class.PhotoJournalLogCommand.php");
require_once("travellog/UIComponent/Photo/command/Class.PhotoTravelLastUpdatedCommand.php");
require_once("travellog/UIComponent/Photo/command/Class.PhotoNotifierCommand.php");

class PhotoRequestEDIT{
	
	function __construct(FileFactory $FFactory){
		
		$template 		= $FFactory->getClass('Template');
		$Subnavigation 	= $FFactory->getClass('SubNavigation');
		
		new PhotoFactory($_REQUEST['genID'],$_REQUEST['cat']);
		
		$sessMan =	$FFactory->getClass('SessionManager');
		$travelerID = $sessMan->get('travelerID');
		
		
		$_REQUEST['travelerID'] = $travelerID;
		
		
		if(!PhotoFactory::create()->isOwner($travelerID))
			return header("Location:http://".$_SERVER['SERVER_NAME']."/index.php");
				
		
		$error = false;
		$photoid = isset($_REQUEST['photoID'])?$_REQUEST['photoID']:$_REQUEST['id'];
								  
		$template->set('subNavigation',PhotoFactory::create()->getSUBNavigation($Subnavigation,$travelerID));
		
		$photo = new Photo(PhotoFactory::create()->getPHOTOcontext(),$photoid);	
		
		if(isset($_REQUEST['tmp_sid'])){										
			new ParsePERLUploadParams($_REQUEST['tmp_sid']);
			
			$path = new PathManager(PhotoFactory::create()->getPHOTOcontext());	
			
			$U_Manager = new UploadManager();
			$U_Manager->setContext($_REQUEST['cat']);			
			$U_Manager->setDestination($path);		
			$U_Manager->setFiles(ParsePERLUploadParams::getFiles());
			$U_Manager->setTypeUpload('image');
			$U_Manager->upload();		
			
			$P_Manager = new PhotoManager();
			$P_Manager->setPhotoID($photoid);
			$P_Manager->setLoginID($travelerID);
			$P_Manager->setGenID($_REQUEST['genID']);
			$P_Manager->setContext($_REQUEST['cat']);
			$P_Manager->setIsGroupChecked(ParsePERLUploadParams::isGroupNotified());
			$P_Manager->setIsAddressChecked(ParsePERLUploadParams::isAddressBookNotified());
			//set Command to execute after new Photo Uploaded
			$P_Manager->addCommand(new PhotoJournalLogCommand());			//Log to Journal
			$P_Manager->addCommand(new PhotoTravelLastUpdatedCommand());	//update Travel
			$P_Manager->addCommand(new PhotoNotifierCommand());
			
			$P_Manager->change($U_Manager);
			
			if($P_Manager->getValidFiles())
				if($_GET['cat']== "group"){										
					header("Location:http://".$_SERVER['SERVER_NAME']."/group.php?gID=".$_GET['genID']);																
				}elseif($_GET['cat']== "resume"){
					header("Location:http://".$_SERVER['SERVER_NAME']."/resume.php?action=view");																
				}else{
					header("Location:photomanagement.php?cat=".$_GET['cat']."&action=vfullsize&genID=".$_GET['genID']."&photoID=".$photoid);													
				}	
				
			else
				$error = $P_Manager->getInvalidFiles();
		}
																				
		$template->set_vars(array(
							'error'		=>$error,
							'photoid'	=>$photoid,
							'photo'	 	=>$photo->getPhotoLink("featured"),
							'caption'	=>$photo->getCaption(),
							'genID'		=>$_REQUEST['genID'],
							'backlink'	=>PhotoFactory::create()->getBackLink(),	
							'context'	=>$_REQUEST['cat']));																			
		echo $template->out('tpl.EditPhoto.php');		
	}
	
	
		
}


?>
