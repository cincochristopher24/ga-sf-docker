<?php


class PhotoRequestFactory{
	
	private $file_factory = NULL;
	
	function __construct(){
		$this->file_factory = FileFactory::getInstance();
		$this->create();
	}
	
	function create(){
		require_once("travellog/controller/request/photo/Class.PhotoRequest".strtoupper($_REQUEST['action']).".php");
	 	$Photo = new ReflectionClass("PhotoRequest".strtoupper($_REQUEST['action']));
	 	return $Photo->newInstance($this->file_factory);
	}
		
}

?>
