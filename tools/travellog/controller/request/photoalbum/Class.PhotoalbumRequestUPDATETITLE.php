<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */



class PhotoalbumRequestUPDATETITLE{
	
	function __construct(FileFactory $FFactory){
		
		$template 		= $FFactory->getClass('Template');
		
		$sessMan 		= $FFactory->getClass('SessionManager');
		$travelerID 	= $sessMan->get('travelerID');
		
		$_REQUEST['travelerID'] = $travelerID;
		
		require_once('travellog/model/Class.PhotoAlbum.php');
			
		$photoalbum 	= new PhotoAlbum($_POST['albumid']);			
		$datecreated	= getdate();
		$datecreated 	= $datecreated['year']."-".$datecreated['mon']."-".$datecreated['mday']." ".$datecreated['hours'].":".$datecreated['minutes'].":".$datecreated['seconds'];
							
		$photoalbum->setTitle($_POST['albumtitle']);
		//$photoalbum->setLastUpdate($datecreated);																	
		$photoalbum->Update();	
		
		$photoalbum = new PhotoAlbum($_POST['albumid']);
		$template->set_vars(array('palbum'	=>$photoalbum));																																									
		
		echo $template->fetch('tpl.per_album_carousel.php');
			
			
	}
	
}


?>
