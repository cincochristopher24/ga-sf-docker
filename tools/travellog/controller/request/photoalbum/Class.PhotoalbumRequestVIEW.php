<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */

require_once('travellog/service/PhotoalbumService.php');

class PhotoAlbumRequestVIEW{
	
	function __construct(FileFactory $FFactory){
		
		$template 		= $FFactory->getClass('Template');
		$Subnavigation 	= $FFactory->getClass('SubNavigation');
		
		$sessMan 		=	$FFactory->getClass('SessionManager');
		$travelerID 		= $sessMan->get('travelerID');
		
		$_REQUEST['travelerID'] = $travelerID;
		
		$Subnavigation->setContext('GROUP');
		$Subnavigation->setContextID($_REQUEST['groupID']);
		$Subnavigation->setGroupType('ADMIN_GROUP');
		//$Subnavigation->setLinkToHighlight('GROUP_NAME');
		$Subnavigation->setLinkToHighlight('PHOTOS');
		
		$PhotoAlbumService = new PhotoAlbumService();
		$PhotoAlbumService->setTemplateViews($template->getPath());
		
		$group = new AdminGroup($_REQUEST['groupID']);
		
		$isOwner = false;
		
		if(in_array($travelerID,$group->getStaff(true)) || $group->getAdministrator()->gettravelerID() == $travelerID){
				$isOwner = true;
		}	
		 
		
		$factory = $FFactory->getClass('ProfileCompFactory');
		$profile_comp = $factory->create(ProfileCompFactory::$GROUP_CONTEXT);
		$profile_comp->init($_GET['groupID']);
		$obj_view     = $profile_comp->get_view();
		
		$template->set('profile_header',$obj_view);
		
		
		
		$template->set('subNavigation',$Subnavigation);
		
		$template->set_vars(array(
					'groupID' 			=>$_REQUEST['groupID'],
					'isOwner' 			=>$isOwner,	
					'photoalbumservice'	=>$PhotoAlbumService,
					'loginID'			=>$travelerID));																																									
		echo $template->out('tpl.ViewPhotoAlbum.php');
		
	}
	
}


?>
