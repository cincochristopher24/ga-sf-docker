<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */


class PhotoalbumRequestDEL{
	
	function __construct(FileFactory $FFactory){
		
		$template 		= $FFactory->getClass('Template');
		
		$sessMan 		= $FFactory->getClass('SessionManager');
		$travelerID 	= $sessMan->get('travelerID');
		
		require_once('travellog/model/Class.PhotoAlbum.php');
		
		$palbum = new PhotoAlbum($_GET['photoalbumid']);
		$palbum->Delete();																
		
		require_once('travellog/model/Class.AdminGroup.php');
		
		$group = new AdminGroup($_GET['groupID']);
		
		require_once('travellog/model/Class.PhotoAlbumAdapter.php');
		$PhotoAlbumAdapter = new PhotoAlbumAdapter();
		
		$grpPhotoAlbums = $PhotoAlbumAdapter->getAlbums($group);
		
		$template->set_path("travellog/views/");
		$template->set("isAdminLogged",true);
		$template->set("grpID",$_GET['groupID']);
		$template->set("loggedUserID",$travelerID);
		$template->set("grpPhotoAlbums",$grpPhotoAlbums);
		
		echo $template->fetch('tpl.IncGroupPhotoAlbums.php');
		
	}
	
}


?>
