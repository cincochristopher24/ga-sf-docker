<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */

class PhotoalbumRequestTOGPUBLISH{
	
	function __construct(FileFactory $FFactory){
		
		$template 		= $FFactory->getClass('Template');
		
		$sessMan 		=	$FFactory->getClass('SessionManager');
		$travelerID 	= $sessMan->get('travelerID');
		
		require_once("Class.Connection.php");
		require_once("Class.Recordset.php");
		
		$mConn = new Connection;
		$mRs   = new Recordset($mConn);
		
		$travelID	= $_GET['travelid'];
		$publish 	= $_GET['publish'];
		$mRs->Execute("UPDATE tblGroupApprovedJournals set isphotorelate = '$publish' WHERE travelID = '$travelID'");
		
		$publish = $publish?0:1;
		
		echo "<a href='javascript:void(0)' onclick='GROUPPhotoAlbum.TOGpublish($travelID,$publish)'>";
		if(!$publish)
			echo "Unpublish";
		else
			echo "Publish";
		
		echo "</a>";
		
	}
	
}


?>
