<?php
/*
 * Created on Nov 14, 2007
 * Class.PhotoRequestVIEW.php
 * 
 * @author Joel C. Llano <joel.llano@goabroad.com> 
 * @version: 
 * @package: 
 */

require_once('travellog/service/PhotoalbumService.php');

class PhotoAlbumRequestDELETE{
	
	function __construct(FileFactory $FFactory){
		
		$template 		= $FFactory->getClass('Template');
		
		$sessMan 		=	$FFactory->getClass('SessionManager');
		$travelerID 	= $sessMan->get('travelerID');
		
		$_REQUEST['travelerID'] = $travelerID;
		
		$PhotoAlbumService = new PhotoAlbumService();
		$PhotoAlbumService->setTemplateViews($template->getPath());
		$PhotoAlbumService->Delete();
		
	}
	
}


?>
