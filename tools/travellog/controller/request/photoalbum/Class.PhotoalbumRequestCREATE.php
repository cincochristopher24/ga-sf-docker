<?php

class PhotoalbumRequestCREATE{
	
	function __construct(FileFactory $FFactory){
		
		$template 		= $FFactory->getClass('Template');
		$sessMan 		= $FFactory->getClass('SessionManager');
		$travelerID 	= $sessMan->get('travelerID');
		
		
		$datecreated	=getdate();
		$datecreated = $datecreated['year']."-".$datecreated['mon']."-".$datecreated['mday']." ".$datecreated['hours'].":".$datecreated['minutes'].":".$datecreated['seconds'];
		
		require_once('travellog/model/Class.PhotoAlbum.php');
		
		$PhotoAlbum = new PhotoAlbum();
		$PhotoAlbum->setTitle($_POST['albumtitle']);
		$PhotoAlbum->setLastUpdate($datecreated);
		$PhotoAlbum->setGroupID($_POST['groupID']);
		$PhotoAlbum->setCreator($travelerID);
		$PhotoAlbum->Create();																	
		
		require_once('travellog/model/Class.AdminGroup.php');
		
		$group = new AdminGroup($_POST['groupID']);
		/*
		$photoalbum = $group->getPhotoAlbums();	
						 
		$template->set("isAdminLogged",true);
		$template->set("grpID",$_POST['groupID']);
		$template->set("grpPhotoAlbums",$photoalbum);
		
		echo $template->fetch('tpl.IncGroupPhotoAlbum.php');
		*/
		
		require_once('travellog/model/Class.PhotoAlbumAdapter.php');
		$PhotoAlbumAdapter = new PhotoAlbumAdapter();
		
		$grpPhotoAlbums = $PhotoAlbumAdapter->getAlbums($group);
		
		$template->set_path("travellog/views/");
		$template->set("isAdminLogged",true);
		$template->set("grpID",$_POST['groupID']);
		$template->set("loggedUserID",$travelerID);
		$template->set("grpPhotoAlbums",$grpPhotoAlbums);
		
		echo $template->fetch('tpl.IncGroupPhotoAlbums.php');
		
	}
	
}


?>
