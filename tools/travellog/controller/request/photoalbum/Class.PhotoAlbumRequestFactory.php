<?php


class PhotoAlbumRequestFactory{
	
	private $file_factory = NULL;
	
	function __construct(){
		$this->file_factory = FileFactory::getInstance();
		$this->create();
	}
	
	function create(){
		require_once("travellog/controller/request/photoalbum/Class.PhotoalbumRequest".strtoupper($_REQUEST['action']).".php");
	 	$PALBUM = new ReflectionClass("PhotoalbumRequest".strtoupper($_REQUEST['action']));
	 	return $PALBUM->newInstance($this->file_factory);
	}
		
}

?>