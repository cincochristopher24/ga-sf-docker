<?php
	/*
	 * Class.AdvisorRegistrationController.php
	 * Created on Nov 20, 2007
	 * created by marc
	 */
	 
	require_once("travellog/controller/Class.AbstractRegisterController.php");
	
	class AdvisorRegistrationController extends AbstractRegisterController{
		
		function __construct(){
			
			parent::__construct();
			$this->file_factory->registerClass('AdminGroup', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('GroupPrivacyPreference', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass("Client",array("path" => "travellog/model/"));
			
		}		
		
		function validateUserDetails( $content = array() ){
			
			$conn = $this->file_factory->getClass("Connection");
			$rs = $this->file_factory->getClass("Recordset",array($conn));
			
			$arrErrorMessage = parent::validateUserDetails($content);
			// advisor name
			if( 0 == strlen($content["instname"]) )
				$arrErrorMessage[constants::EMPTY_GROUPNAME] = "Institution/Professor Name must be filled out.";
			elseif( !preg_match('/^[a-zA-Z0-9\-\s\.]*$/',$content["instname"]) )
				$arrErrorMessage[constants::INVALID_GROUPNAME_FORMAT] = "Invalid format for group name. (Note: Only numbers, letters, spaces, period and hyphen(-) are allowed.)";
			else{
				// check if username is already used and push in error array
				$sqlquery = "SELECT groupID FROM tblGroup " .
						"WHERE name='" . addslashes($content["instname"]) . "' "; 
				$myquery = $rs->Execute($sqlquery);
				if ( mysql_num_rows($myquery) )
					$arrErrorMessage[constants::GROUPNAME_ALREADY_USED] = "'" . $content["instname"] . "' already exists, please create another.";		
			}
			
			// first name
			if( 0 == strlen($content["firstname"]) )
				$arrErrorMessage[constants::EMPTY_FIRSTNAME] = "First Name must be filled out.";
			// last name
			if( 0 == strlen($content["lastname"]) )
				$arrErrorMessage[constants::EMPTY_LASTNAME] = "Last Name must be filled out.";
		
			return $arrErrorMessage;
			
		}
		
		function viewRegistrationForm( $content = array() ){
			
			$ht = $this->file_factory->getClass("HelpText");
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			$security = $this->file_factory->invokeStaticClass("Captcha","createImage");
			
			$nContent = array(
				"arrErrorMessages"	=>	isset($content["arrErrorMessages"]) ? $content["arrErrorMessages"] : array(),		
				"instname"			=>	isset($content["instname"]) ? trim($content["instname"]) : "",
				"firstname"			=>	isset($content["firstname"]) ? trim($content["firstname"]) : "",
				"lastname"			=>	isset($content["lastname"]) ? trim($content["lastname"]) : "",
				"advisorUsrname"	=>	isset($content["username"]) ? trim($content["username"]) : "",
				"password"			=>	isset($content["password"]) ? trim($content["password"]) : "",
				"advisorEmailAdd"	=>	isset($content["emailAdd"]) ? trim($content["emailAdd"]) : "",
				"subscribeNL"		=>	isset($content["subscribeNL"]) ? $content["subscribeNL"] : 0,
				"regKey"			=>	isset($content["regKey"]) ? trim($content["regKey"]) : "",
				"regID"				=>	isset($content["regID"]) ? trim($content["regID"]) : "",
				"gcID"				=>	isset($content["gcID"]) ? $content["gcID"] : 0,
				"ref"				=>	isset($content["ref"]) ? $content["ref"] : "",				
				"strErrorMessages"	=>	isset($content["strErrorMessages"]) ? $content["strErrorMessages"] : "",
				"redirectPage"	    =>	isset($content["redirectPage"]) ? $content["redirectPage"] : $this->register_helper->getRedirectPage(constants::REGISTER_PAGE),				
				"security"			=>	$security,
				"advisorQuestion"	=>	$ht->getHelpText('advisor-register-help-head'),
				"advisorAnswer"		=>	$ht->getHelpText('advisor-register-help-content'),
				"agreeTerm"			=>	isset($content["agreeTerm"]) ? $content["agreeTerm"] : false
			);
			Template::includeDependentJs('/min/f=js/prototype.js', array('top' => true));
			Template::includeDependentJs('/js/interactive.form.js');
			Template::includeDependentJs('js/tools/common.js');
			Template::includeDependentJs('js/utils/json.js');
			Template::includeDependentJs('captcha/captcha.js');
			$code = <<<STYLE
			<script type="text/javascript">
				var Captcha = new CaptchaLoader({					
					imageTagID	: 'captchaImg',
					onLoad		: function(params){			
						$('encSecCode').value  = params['code'];
					},
					imgAttributes : {
						width : '200',
						height: '50'	
					}
				});
			</script>
STYLE;

			Template::includeDependent($code);
			Template::setMainTemplateVar('layoutID', 'Register');
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );
			Template::includeDependentJs('/js/popup.js');
			Template::setMainTemplateVar('title', 'Register as Travel Advisor/Planner - Join the GoAbroad Network!');
			Template::setMainTemplateVar('metaDescription', 'Universities, study abroad offices, travel agents, tour operators or just about any group of travel enthusiasts can become Online Advisors and create special groups where they can post alerts, updates, and photos as well as send email broadcasts to their traveling members.');
			
			$fcd = $this->file_factory->getClass('RegisterFcd', $nContent, false);
			$obj_view->setContents( array('contents' => $fcd->retrieveGroupRegistrationForm()) );
			$obj_view->render();
			
		} 
		
		function submitDetails(){
			
			$advisorData = $this->register_helper->readAdvisorData();
			$advisorData["arrErrorMessages"] = $this->validateUserDetails($advisorData);
			
			if( count($advisorData["arrErrorMessages"]) ){
				$this->viewRegistrationForm($advisorData);
			}
			else{
				$this->saveDetailsToTempTable($advisorData);
				$this->showRegistrationDetails($advisorData);
			}
				
		}
		
		function saveDetailsToTempTable( &$advisorData = array() ){
			
			$conn = $this->file_factory->getClass("Connection");
			$rs = $this->file_factory->getClass("Recordset",array($conn));
			
			if( $advisorData["regID"] )
				$sqlquery = "UPDATE tblTmpRegister SET ";
			else	 
				$sqlquery = "INSERT INTO tblTmpRegister SET ";
			$sqlquery .= "genName = '" . addslashes($advisorData["instname"]) . "',
					lastName = '" . addslashes($advisorData["lastname"]) . "',	
					username = '" . addslashes($advisorData["username"]) . "',		 
					password = '" . addslashes($advisorData["password"]) . "',	
					email = '" . addslashes($advisorData["emailAdd"]) . "',
					subscribeNL = ". $advisorData["subscribeNL"];
			if( 0 == $advisorData["regID"] )
				$sqlquery .= " ,regKey = '" . mktime() . "'"; 
			if( $advisorData["regID"])
				$sqlquery .= " WHERE registerID = " . addslashes($advisorData["regID"]) . 
					" AND regKey = '" . addslashes($advisorData["regKey"]) . "'";
			$myquery = $rs->Execute($sqlquery);
			if( 0 == $advisorData["regID"] ){
				$sqlquery = "SELECT registerID, regKey FROM tblTmpRegister WHERE registerID = LAST_INSERT_ID()";
				$myquery = $rs->Execute($sqlquery);
				$row = mysql_fetch_array($myquery);
				$advisorData["regKey"] = $row["regKey"];
				$advisorData["regID"] = $row["registerID"];		
			}
			$advisorData["user"] = constants::GROUP;
		
		}
		
		function modifyDetails(){
			
			$conn = $this->file_factory->getClass("Connection");
			$rs = $this->file_factory->getClass("Recordset",array($conn));
			$advisorData = $this->register_helper->readAdvisorData();
			
			$sqlquery = "SELECT * FROM tblTmpRegister " .
				"WHERE registerID = '" . addslashes($advisorData["regID"]) . "' " .  
				" AND regKey = '" . addslashes($advisorData["regKey"]) . "'";
			$myquery = $rs->Execute($sqlquery);
			if( $tmpRow = mysql_fetch_array($myquery) ){
				$advisorData["instname"] = $tmpRow["genName"];
				$advisorData["firstName"] = $tmpRow["genName"];
				$advisorData["lastName"] = $tmpRow["lastName"];
				$advisorData["username"] = $tmpRow["username"];			
				$advisorData["emailAdd"] = $tmpRow["email"];
				$advisorData["subscribeNL"] = $tmpRow["subscribeNL"];
				$this->viewRegistrationForm($advisorData);
			}
			else{
				ob_end_clean();
				header("location: " . $this->register_helper->getRedirectPage(constants::REGISTER_PAGE));
				exit;
			}
		}
		
		function saveUserDetails($tmpRow){
			$advisorData = $this->register_helper->readAdvisorData();
			$tmpRow["firstName"] = (isset($advisorData["firstname"]) && "" != $advisorData["firstname"]) ? $advisorData["firstname"] : $tmpRow["genName"];
			
			$conn = $this->file_factory->getClass("Connection");
			$rs = $this->file_factory->getClass("Recordset",array($conn));
			
			$rs->execute("DELETE FROM tblTmpRegister " .
				"WHERE registerID = '" . addslashes($tmpRow["registerID"]) . "' " .  
				"AND regKey = '" . addslashes($tmpRow["regKey"]) . "'");
			$sqlquery = "INSERT INTO tblSendable(sendabletype) VALUES(1)";
			$myquery = $rs->Execute($sqlquery); 
			$sendableID = mysql_insert_id($conn->GetConnection()); 
			$sqlquery = "INSERT INTO tblTraveler SET " .
					"firstname = '" . $tmpRow["firstName"] . "', " .
					"lastname = '" . $tmpRow["lastName"] . "', " . 
					"username = '" . $tmpRow["username"] . "', " .
					"password = '" . $tmpRow["password"] . "', " .
					"email = '" . $tmpRow["email"] . "', " . 
					"sendableID = $sendableID, " . 
					"isadvisor = 1, " . 
					"dateregistered = now()"; // dateregistered added by ianne - 11/25/2008
			$myquery = $rs->Execute($sqlquery);
			
			/**
			 * get the travelerID of the user
			 * insert needed information in database for forums
			 */
			$travelerID = mysql_insert_id($conn->GetConnection());
			
			// create group
			$group = $this->file_factory->getClass("AdminGroup");
			$group->setDiscriminator(2);
			$group->setName($tmpRow["genName"]);
			$group->setGroupAccess(2);
			$group->setAdministratorID($travelerID);
			$group->setAdministratorClientID($tmpRow["clientID"]);		
			$group->Create();
			
			// create traveler object
			$traveler = $this->file_factory->getClass("Traveler",array($travelerID));
			
			$gID = $group->getGroupID();
			$group->addMember($traveler);
			
			// >>>> SET DEFAULT GROUP PRIVACY PREFERENCES
			$groupPref = $this->file_factory->getClass("GroupPrivacyPreference");
			$groupPref->setGroupID($gID);
			$groupPref->setViewAlerts(1);
			$groupPref->setDownloadFiles(1);
			$groupPref->setViewGroups(1);
			$groupPref->setViewBulletin(1);
			$groupPref->setViewCalendar(1);
			$groupPref->update();		
			
			$rs->Execute("INSERT INTO tblGrouptoAdvisor SET 
							travelerID = " . $travelerID . ", 
							clientID = " . $tmpRow["clientID"] . ", 
							groupID = " . $gID);
							
			$traveler->transferAdvisorMessages();
			
			$this->saveCommonUserDetails($travelerID,$tmpRow["email"]);
			
			if($tmpRow["subscribeNL"])
				$this->subscribeNL($tmpRow["email"],8);
				
			return array("travelerID" => $travelerID,"groupID" => $gID);
		}
		
	}
	  
?>
