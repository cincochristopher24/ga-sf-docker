<?php
	/*
	 * Class.AbstractForgotPasswordController.php
	 * Created on Nov 28, 2007
	 * created by marc
	 */
	require_once("travellog/controller/Class.IController.php"); 
	require_once("travellog/factory/Class.FileFactory.php");
	require_once("Class.Constants.php");
	 
	class AbstractForgotPasswordController implements IController{
		
		static $file_factory = NULL;
		static $fpHelper = NULL;
		
		/**********************************************************************************
		 * edits of neri:
		 * 		set fpData as third parameter of the MailGenerator class if 
		 * 			it's a cobrand, otherwise, do not set any third parameter:	01-07-09											01-07-09
		 * 		replaced cobrandName with cobrandEmailName and cobrandSiteName 
		 * 			and set these variables to null if they do not exist in 
		 * 			the array in sendPasswordEmail():							01-07-09
		 *		set value of network to cobrand name if it's a cobrand,
		 * 			otherwise, set it to 'GoAbroad.net' as it's default value
		 * 			and added it on the array content in 
		 * 			viewForgotPasswordForm():									01-06-09
		 * 		set value of cobrand to false in constructor:					01-06-09
		 * 		set cobrandName into null if it does not exists in the array 
		 * 			in sendPasswordEmail(): 									12-23-08
		 **********************************************************************************/
		
		function __construct(){
			
			$this->file_factory = FileFactory::getInstance();
			
			$this->file_factory->registerClass("MainLayoutView", array("path" => "travellog/views/"));
			$this->file_factory->registerClass("TravelerProfile", array("path" => "travellog/model/"));
			$this->file_factory->registerClass("phpmailer");
			$this->file_factory->registerClass("Crypt");
			$this->file_factory->registerClass("Captcha", array("path" => "captcha/"));
			$this->file_factory->registerClass("MailGenerator", array("path" => "travellog/model/"));
			$this->file_factory->registerClass("ForgotPasswordHelper", array("path" => "travellog/helper/"));
			$this->file_factory->registerClass("ForgotPasswordFcd", array("path" => "travellog/facade/"));
			$this->file_factory->registerClass("Traveler", array("path" => "travellog/model"));
			
			$this->fpHelper = $this->file_factory->invokeStaticClass("ForgotPasswordHelper","getInstance");
			
			// views
			$this->file_factory->registerTemplate("LayoutMain");
			$this->file_factory->setPath("CSS", "/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/views/");	
			
			$this->cobrand = false;			
		}	
		
		function performAction(){
			
			switch( $this->fpHelper->getAction()){
				case constants::SUBMIT_DETAILS:
					return $this->sendpasswordEmail();
				default:
					return $this->viewForgotPasswordForm();		
			}
			
		}
		
		function validate(){
			
			$fpData = $this->fpHelper->readForgotpasswordData();
			
			if( !strlen($fpData["email"]) )
				$fpData["arrError"][] = constants::EMPTY_EMAIL_ADDRESS;
			elseif( !$fpData["travelerID"] = $this->file_factory->invokeStaticClass("Traveler","exist",array($fpData["email"])) )
				$fpData["arrError"][] = constants::TRAVELER_DOES_NOT_EXIST;
			if( !$this->file_factory->invokeStaticClass("Captcha","validate",array($fpData["code"],$fpData["secCode"])))
				$fpData["arrError"][] = constants::SECURITY_CODE_MISMATCH;
			
			return $fpData;
		}
		
		function viewForgotPasswordForm( $fpData = array() ){
			
			if ($this->cobrand) 
				$network = $GLOBALS['CONFIG']->getAdminGroup()->getName(); 
			else
				$network = 'GoAbroad.net';
			
			$security = $this->file_factory->invokeStaticClass("Captcha","createImage");
			
			$content = array(
				"email"		=>	isset($fpData["email"]) ? trim($fpData["email"]) : "",
				"code"		=>	isset($fpData["code"]) ? trim($_POST['encSecCode']) : "",
				"arrError"	=>	isset($fpData["arrError"]) ? $fpData["arrError"] : array(),
				"src"		=> 	$security["src"],
				"code"		=>	$security["code"],
				"network"	=>  $network
			);	
			
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			
			Template::includeDependentJs('/min/f=js/prototype.js', array('top'=>true));
			Template::includeDependentJs('js/tools/common.js');
			Template::includeDependentJs('js/utils/json.js');
			Template::includeDependentJs('captcha/captcha.js');
						
			$script = <<<SCRIPT
			<script type="text/javascript">
			//<[!CDATA[
			
				function doSubmitOnce(currentForm){
					// Run through elements and disable submit buttons
					for(i=0;i<currentForm.length;i++)
					{
						var currentElement = currentForm.elements[i];
						if(currentElement.type.toLowerCase() == "submit")
						currentElement.disabled = true;
					}
				}
			
				// We need to register our submit once function on all forms
				if(document.all||document.getElementById) 
				{	
					// Run through all forms on page
					for(i=0;i<document.forms.length;i++)
					{
						var currentForm = document.forms[i];
						// Register event handler
						// Use quirksmode idea for flexible registration by copying existing events
						var oldEventCode = (currentForm.onsubmit) ? currentForm.onsubmit : function () {};
						currentForm.onsubmit = function () {oldEventCode(); doSubmitOnce(currentForm)};
					}
				}
				
				var Captcha = new CaptchaLoader({					
					imageTagID	: 'captchaImg',
					onLoad		: function(params){			
						$('encSecCode').value  = params['code'];
					},
					imgAttributes : {
						width : '200',
						height: '50'	
					}
				});
							
				//]]>
			</script>	
SCRIPT;

			Template::includeDependent($script);
			Template::setMainTemplateVar('layoutID', 'page_default');
			Template::includeDependentJs('js/interactive.form.js');
			Template::setMainTemplateVar('title', 'Password Recovery Assistance - GoAbroad Network');
			Template::setMainTemplateVar('metaDescription',' We can help you get back your account by changing the forgotten password here.');
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );
			
			Template::includeDependentCss("/css/vsgStandardized.css");
			Template::includeDependentCss("/css/modalBox.css");
			Template::includeDependentCss('/css/Thickbox.css');
			Template::includeDependentJs('/js/thickbox.js');
			
			$fcd = $this->file_factory->getClass('ForgotPasswordFcd', $content, false);
			$obj_view->setContents( array('contents' => $fcd->retrieveForgotPasswordFormPage()) );
			$obj_view->render();
		}
		
		function sendPasswordEmail( ){
			
			$fpData = $this->validate();
			
			if( count($fpData["arrError"]) )
				return $this->viewForgotPasswordForm($fpData);
			else{
				if (!array_key_exists('cobrandEmailName', $fpData))
					$fpData['cobrandEmailName'] = null;
					
				if (!array_key_exists('cobrandSiteName', $fpData))
					$fpData['cobrandSiteName'] = null;
				
				if (!$this->cobrand)
					$mailGenerator = $this->file_factory->getClass("MailGenerator",array($fpData["travelerID"],5));
				else
					$mailGenerator = $this->file_factory->getClass("MailGenerator",array($fpData["travelerID"],5,$fpData));
				
				$content["sent"] = true;
				
				/**
				* prompt programmer if error while sending email occurred
				*/
				if(!$mailGenerator->Send()){
				   $content["sent"] = false;
				}
			}	
			
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			
			Template::setMainTemplateVar('layoutID', 'main_pages');
			Template::setMainTemplateVar('title', 'Password Recovery Assistance - GoAbroad Network');
			Template::setMainTemplateVar('metaDescription',' We can help you get back your account by changing the forgotten password here.');
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') ); 
			
			Template::includeDependentCss("/css/vsgStandardized.css");
			Template::includeDependentCss("/css/modalBox.css");
			Template::includeDependentCss('/css/Thickbox.css');
			Template::includeDependentJs('/js/thickbox.js');
			
			$fcd = $this->file_factory->getClass('ForgotPasswordFcd', $content, false);
			$obj_view->setContents( array('contents' => $fcd->retrieveForgotPasswordConfirmationPage()) );
			$obj_view->render();
		}
	}
	
?>