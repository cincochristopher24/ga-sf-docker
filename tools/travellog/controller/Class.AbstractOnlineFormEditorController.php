<?php
	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/factory/Class.FileFactory.php");
	require_once('JSON.php');
	
	abstract class AbstractOnlineFormEditorController
	{
		protected $file_factory = NULL;
		protected $obj_session  = NULL;
		
		public function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/views/'));
			$this->obj_session  = SessionManager::getInstance();
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			$this->file_factory->registerClass('ToolMan', array('path'=>''));
			$this->file_factory->registerClass('AdminGroup');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('Template',array('path'=>''));
			$this->file_factory->registerClass('GaOnlineForm',array('path'=>'travellog/model/gaOnlineForms/'));
		}
		
		public function performAction(){
			$travelerID = $this->obj_session->get('travelerID');
			if(!$travelerID){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/login.php'));
			}
			//check if the user is an advisor
			$oTraveler = $this->file_factory->getClass('Traveler',array($travelerID));
			if(!$oTraveler->isAdvisor()){		
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			
  			$frmID = (array_key_exists('frmID',$_GET) && is_numeric($_GET['frmID']) )?$_GET['frmID']:0;echo '--------';exit;
  			$gID = $this->file_factory->invokeStaticClass('AdminGroup','getAdvisorGroupID',array($travelerID));
			$formData = 'null';
			$refresh = 'true';
			$extParticipants = array();
			if(0!=$frmID){
				$onlineForm = $this->file_factory->invokeStaticClass('GaOnlineForm','getFormByOnlineFormID',array($frmID));
				if(is_null($onlineForm) || $onlineForm->getGroupID() != $gID) $this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
				if(array_key_exists('duplicate',$_GET)){
					$onlineForm = $onlineForm->duplicate();
				}
				else $refresh = 'false';
				$formData = $onlineForm->getJsonFormData();
				$formParticipants = $onlineForm->getParticipants();
				//map the existing participants with the groupID as key
				foreach($formParticipants as $iFormParticipant){
					$extParticipants[$iFormParticipant->getGroupID()] = $iFormParticipant;
				}
			}
			
			$gFactory =  $this->file_factory->invokeStaticClass('GroupFactory','instance');
			try{$oGroup  =  $gFactory->create($gID);}
			catch(exception $e){				
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			$grps = $oGroup->getSubGroups();
			$participants = array();	
			$participants[] = "{'id':{$oGroup->getGroupID()},'label':'{$oGroup->getName()}','checked':true}";
			if(is_array($grps)){
				foreach($grps as $iGroup){
					$checked = array_key_exists($iGroup->getGroupID(),$extParticipants) ? 'true' : 'false';
					$label = htmlentities(addslashes($iGroup->getName()));
					$participants[] = "{'id':{$iGroup->getGroupID()},'label':'$label','checked':$checked}";
				}
			}

			$jsonParticipants = '['.implode(",\n",$participants).']';
			
			$gFactory =  $this->file_factory->invokeStaticClass('GroupFactory','instance');
			try{ $oGroup  =  $gFactory->create($gID);}
			catch(exception $e){				
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			
			$subNavigation = $this->file_factory->getClass('SubNavigation');
			$subNavigation->setContext('GROUP');	
			$subNavigation->setContextID($oGroup->getGroupID());
			$subNavigation->setGroupType('ADMIN_GROUP');
			$subNavigation->setLinkToHighlight(' ONLINE_FORMS');
  			
  			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			$tpl = $this->file_factory->getClass('Template');
			$tpl->set_path("travellog/views/onlineForms/");
			$tpl->set("gID",$gID);
			$tpl->set("frmID",$frmID);		
			$tpl->set("formData",$formData);
			$tpl->set("jsonParticipants",$jsonParticipants);
			$tpl->set("refresh",$refresh);
			$tpl->set("subNavigation",$subNavigation);
			$tpl->out("tpl.OnlineFormEditor.php");		
		}
		
	}
?>