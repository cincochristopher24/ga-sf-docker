<?php
  /**
   * @(#) Class.SubGroupCategoriesController.php
   * 
   * Note: methods are arranged in alphabetical order.
   * 
   * @author Antonio Pepito Cruda Jr.
   * @version 1.0 - 01 6, 09
   */
   
  require_once("travellog/model/Class.SessionManager.php");
  require_once("travellog/controller/Class.IController.php");
  require_once("travellog/factory/Class.FileFactory.php");
  
  class SubGroupCategoriesController implements IController {
  	
  	/**
  	 * Attributes
  	 */
  	protected 
  	
  	$subNav = NULL,	// the variable the holds the subnavigation object
  	
  	$isAdminLogged = false, // a flag variable whether an admin is logged or not
  	
  	$profileHandler = NULL // the variable that handles the group profile that shows above the subnavigation.
  	;	
  	
  	/**
  	 * constructor
  	 */
  	function __construct(){
			$this->obj_session = SessionManager::getInstance();
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template',array('path'=>''));
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('CustomizedSubGroupCategory');
			
			$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerClass('SubGroupCategoriesViewHandler', array('path' => 'travellog/views/subgroups'));
			$this->file_factory->registerClass('SubNavigation');
			
			$this->file_factory->registerTemplate('LayoutMain');
			
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			$this->tpl = $this->file_factory->getClass('Template');
			$this->tpl->set_path("travellog/views/");		
  	}
  	
		/**
		 * Checks whether the current logged in is an admin user(staff is consider as an admin user) or not. If it
		 * is not an admin user it redirects the page to custom group normal view mode.
		 * 
		 * @param boolean $_privilege
		 * @return void
		 */
		function allowAdminPrivileges(){
			if (!$this->isAdminLogged){
				header("location:subgroups.php?gID=".$this->group->getGroupID());
				exit;
			}
		}
		
		/**
		 * Arranges in the database the categoryRank of the given categories.
		 * 
		 * @param array $params The data where where the categories to be arranged will be get.
		 */
		function arrangeCategories(&$params){
			$array = explode(",", $params['categoryIDs']);
			$this->group->sortCategoryRanks($array);
			$this->group->saveSubGroupCategoriesRankSettings(SubGroupSortType::CUSTOM);
			
			$params['message'] = "SubGroup Category Order Saved";
			$params['mode'] = "viewCategoriesWithoutMainTemplate";
		}
		
		/**
		 * Deletes the given category(category ID) from the database.
		 * 
		 * @param array $params The request data.
		 * @return void
		 */
		public function deleteCategory($params) {
			$category = $this->file_factory->getClass('CustomizedSubGroupCategory',array($params['sgcID']));
			$category->emptyCategory();
			$category->delete();
			
			$this->obj_session->set('custompopup_message', "You have successfully deleted the category <strong>".$category->getName()."</strong>.");
			
			if ("subgroups" == trim($params['fromMode'])) {
				header("location: /subgroups.php?gID=".$this->group->getGroupID());
			}
			else {
				header("location: /subgroupcategories.php?gID=".$this->group->getGroupID());
			}
		}
  	
  	/**
  	 * Fetches the values passed through post, request, get,
  	 * session and cookie. 
  	 * 
  	 * @return array
  	 */
  	private function fetchGPCData(){
			$params = array();
			$params['mode'] = ( isset($_GET['mode']))? $_GET['mode'] : 'view';
			$params['mode'] = ( isset($_POST['mode']))? $_POST['mode'] : $params['mode'];
			$params['gID']  = ( isset($_POST['gID']))? $_POST['gID'] : 0 ;
			$params['gID']  = ( isset($_GET['gID']))? $_GET['gID'] : $params['gID'];
			
			$params['category'] = ( isset($_REQUEST['category']) )? $_REQUEST['category'] : null;
			
			$params['action']  = ( isset($_GET['action']))? $_GET['action'] : 'ViewList';
			$params['action']  = ( isset($_POST['action']))? $_POST['action'] : $params['action'];
      		
      $params['sgcID'] = ( isset($_POST['hidcID']) )? $_POST['hidcID'] : 0;
      $params['sgcID'] = ( isset($_REQUEST['sgcID']) )? $_REQUEST['sgcID'] : $params['sgcID'];
			$params['sgcName'] = ( isset($_POST['txtcName']) )? $_POST['txtcName'] : null;
			
			$params['fromMode'] = (isset($_REQUEST['fromMode'])) ? $_REQUEST['fromMode'] : "subgroups";
			$params['categoryIDs'] = (isset($_REQUEST['categoryIDs'])) ? $_REQUEST['categoryIDs'] : ""; 
			
			$params['message'] = ($this->obj_session->get('custompopup_message')) ? $this->obj_session->get('custompopup_message') : NULL;
			$this->obj_session->unsetVar('custompopup_message');
			
			return $params;  
  	}
  	
  	/**
  	 * Initializes the common variables used in adding, updating and deletion.
  	 * 
  	 * @param array $params The data used to initialize the common variables.
  	 * @return void
  	 */
  	function initializeActionCommonAttributes(&$params){
			if (0 == $params['gID']){
				header("location: /index.php");
				exit;
			}
			
			$mGroup = $this->file_factory->invokeStaticClass('GroupFactory','instance')->create(array($params['gID']));
			$this->group = $mGroup[0];
			
			$this->loggedUser = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')));
				
			$this->isLogged		=	$this->obj_session->get('travelerID') > 0 ? true : false ;
			$this->isMemberLogged	=	$this->group->isMember($this->loggedUser);
			$this->isAdminLogged	=	$this->group->getAdministratorID() == $this->obj_session->get('travelerID') || $this->group->isStaff($this->obj_session->get('travelerID')) ? true : false;			
			$this->category = (is_numeric($params['category'])) ? new CustomizedSubGroupCategory($params['category']) : $params['category'];
  	}
  	
  	/**
  	 * Initializes the common variables that are used in viewing.
  	 * 
  	 * @param array $params The data used to initialize the common variables.
  	 * @return void
  	 */
  	function initializeViewCommonAttributes(&$params){
			$this->subNav = $this->file_factory->getClass('SubNavigation');
			$this->subNav->setContext('GROUP');
			$this->subNav->setContextID($params['gID']);
			$this->subNav->setLinkToHighlight('SUBGROUPS');	
			
			$this->profileHandler = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
			$this->profileHandler->init($this->group->getGroupID());
  	}
  	
  	/**
  	 * The entry point of this class or shall I say this is the one called 
  	 * to use this class.
  	 * 
  	 * @return void
  	 */
  	function performAction(){
  		$params = $this->fetchGPCData();
  		$this->initializeActionCommonAttributes($params);
  		
			switch ($params['action']) {
				case 'arrangeCategories':
					$this->arrangeCategories($params);
				break;
				case 'updateCategory':
					$this->updateCategory($params);
				break;
				case 'deleteCategory':
					$this->deleteCategory($params);
				break;
			}
  		
			$this->initializeViewCommonAttributes($params);
			
			switch ($params['mode']) {
				case 'viewAllCategories':
				  $this->viewAllCategories($params);
				break;
				case 'viewAddCategory':
				case 'viewEditCategory':
					$this->viewUpdateCategory($params);
				break;
				case 'viewCategoriesWithoutMainTemplate':
					$this->viewCategoriesListWithoutMainTemplate($params);
				break;
				default:
					$this->viewAllCategories($params);		
			}
  	}
  	
		/**
		 * Updates/Add a new category. 
		 * 
		 * @param array $_params The data of the new category will be get from here. This must be the request data.
		 * @return void 
		 */
		public function updateCategory($_params) {
			if (empty($_params['sgcName'])) {
				header("location: subgroups.php?gID=".$_params['gID']."&mode=viewAddCategory");
				exit;	
			}
			
			$category = $this->file_factory->getClass('CustomizedSubGroupCategory',array($_params['sgcID']));
			$category->setName($_params['sgcName']);
			$category->setParentID($_params['gID']);
		  $category->save();
			$category->emptyCategory();
			
			if (isset($_POST["chkCategory"])) {
				try {
				  $category->addSubgroup($_POST["chkCategory"]);
				}
				catch(exception $ex){}
			}
			
			// create messages
			if (0 == $_params['sgcID']) {
				$this->obj_session->set('custompopup_message', "You have successfully added the category <strong>".$category->getName()."</strong>.");
			}
			else {
				$this->obj_session->set('custompopup_message', "You have successfully updated the category <strong>".$category->getName()."</strong>.");
			}
			// end of messages
			
			if ("subgroups" == trim($_params['fromMode'])) {
				header("location: /subgroups.php?gID=".$this->group->getGroupID());
			}
			else {
				header("location: /subgroupcategories.php?gID=".$this->group->getGroupID());
			}
		}
		
		/**
		 * Shows the categories list in a non-main template form in other words it has
		 * no main template. This is actually for ajax requests.
		 * 
		 */
		function viewCategoriesListWithoutMainTemplate($params){
			$categories = $this->group->getSubGroupCategories();
			$categ_count = count($categories);
		
			$handler = $this->file_factory->getClass('SubGroupCategoriesViewHandler');
			$handler->setCategories($categories);
  		$handler->setCategoriesCount($categ_count);
  		$handler->setMessage($params['message']);
  		$handler->setParentGroupID($this->group->getGroupID());
  		
  		echo $handler->renderListOfCategories();	
		}
  	
    /**
     * Shows all the subgroup categories of the parent group (admin group).
     * Exclusive for admin account only.
     * 
     * @param array $params The request data.
     */
    function viewAllCategories($params){
			$this->allowAdminPrivileges();
			
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			
			$categories = $this->group->getSubGroupCategories();
			
			$this->tpl->set_path("travellog/views/subgroups/");
			$this->tpl->set('categories',$categories);
			$this->tpl->set('categoryCount',count($categories));
			$this->tpl->set('profile', $this->profileHandler->get_view());
			$this->tpl->set('subNavigation',$this->subNav);
			$this->tpl->set('isAdminLogged',$this->isAdminLogged);
			$this->tpl->set('success', $params['message']);
			$this->tpl->set('grpID', $this->group->getGroupID());
			
	    $this->tpl->out('tpl.ViewAllSubGroupCategories.php');	 	    	
    }
    
		/**
		 * Shows the form in Adding/Editing a subgroup category.
		 * 
		 * @param array $_params the request data
		 * @return void
		 */
		public function viewUpdateCategory($_params) {
			$this->allowAdminPrivileges();
			
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			$category = $this->file_factory->getClass('CustomizedSubGroupCategory',array($_params['sgcID']));
			
			$this->tpl->set_path("travellog/views/");
			$this->tpl->setVars( array(
				'subNavigation' => $this->subNav,
				'profile' => $this->profileHandler->get_view(),
				'id' => $_params['sgcID'],
				'name' => $category->getName(),
				'parentID' => $_params['gID'],
				'category_groups' => $category->getAllSubGroups(),
				'non_category_groups' =>$this->group->getSubGroupsWithCategory(),
				'fromMode' => $_params['fromMode']
			) );
			$this->tpl->setTemplate('tpl.FrmCustomizedSubGroupCategory.php');
			$this->tpl->out(); 
		}
  }
