<?php
/*
	Filename:	  Class.TravelPlansController.php formerly Class.TravelWishController.php
	Module:		  My Travel Plans (Travel Scheduler/Travel ToDo/Go Abroad)
	Author:		  Jonas Tandinco
	Purpose:	  module controller for travel item list, add, edit, and delete operations
	Date created: Jul/11/2007

	EDIT HISTORY:
  
	Jul/12/2007 by Jonas Tandinco
		1.	finished the add/edit/delete functionality for travel items
		2.	fixed the travel wish list

	Jul/13/2007 by Jonas Tandinco
		1.	fixed a bug on edit travel item
		2.	created displayForm for code reuse
		3.	fixed issues on Date To field value when saving to the database

	Jul/14/2007 by Jonas Tandinco
		1.	added sub navigation but temporarily commented due to unknown side effects

	Jul/18/2007 by Jonas Tandinco
		1.	enabled sub navigation

	Jul/19/2007 by Jonas Tandinco
		1.	changed the order of countries to Alphabetic Ascending and scrapped the use of regions

	Aug/21/2007 by Jonas Tandinco
		1.	added validation to make sure the user selects unique countries

	Aug/28/2007 by Jonas Tandinco
		1.	added validation so that a user cannot edit a travel linked to a journal even if he
			tries to type the travelWishID as part of the URL

	Aug/30/2007 by Jonas Tandinco
		1.	setLinkToHighlight in subnavigation disabled

	Sep/05/2007 by Jonas Tandinco
		1.	disabled the use of status for a travel
		2.	changed date validation since date range for travel is now optional
		3.	changed validation for destinations since it's now optional

	Sep/07/2007 by Jonas Tandinco
		1.	added validation for destination form entries start date, and end date
		2.	travel details limited to 100 from 1000
		3.	implemented description, travel start and end dates for destinations

	Sep/13/2007 by Jonas Tandinco
		1.	allowed redirection to profile when actions are called from profile

	Sep/14/2007 by Jonas Tandinco
		1.	removed unnecesarry commented code
		2.	added more comments to make code more readable
		3.	modified executeList() to reorder the list of travels => present & future travels followed by past travels

	Sep/19/2007 by Jonas Tandinco
		1.	cleaned external variables before save to prevent XSS (cross-site scripting) attacks

	Sep/20/2007 by Jonas Tandinco
		1.	removed travel dates from top-level of a travel
		2.	added validation since at least one destination is now required
		3.	optimized code in add and edit travels

	Sep/21/2007 by Jonas Tandinco
		1.	added validation for dates since it's now made up of 3 dropdown combo boxes
		2.	refactored code so it adheres to best OOP practices

	Sep/24/2007 by Jonas Tandinco
		1.	changed help text
		2.	refactored code to make it more object-oriented and eliminate redundant code

	Oct/01/2007 by Jonas Tandnco
		1.	refactored code to just save a start date and end date for a travel instead of separating them into 3 fields each

	Oct/09/2007 by Jonas Tandinco
		1.	refactored code to transfer some logic to models

	Oct/15/2007 by Jonas Tandinco
		1.	removed checking for travel plans linked to journals

	Nov/06/2007 by Jonas Tandinco
		1.	fixed sub navigation context
		
	Nov/13/2007 by Jonas Tandinco
		1.	renamed class to TravelPlansController as well as the filename
		2.	changed executeCreate() to default a travel destination start date to today
		
	Nov/14/2007 by Jonas Tandinco
		1.	removed code referencing a deleted member of class TravelDestination
		2.	added additional check for each description of a travel destination
		
	Nov/27/2007 by Jonas Tandinco
		1.	moved sub navigation instantiation to Class.AbstractTravelPlansController.php
		2.	modified constructor to accept traveler ID and the sub navigation object
*/

require_once('travellog/model/Class.TravelSchedule.php');
require_once('travellog/model/Class.TravelPlan.php');
require_once('travellog/model/Class.TravelDestination.php');

require_once('Class.Template.php');
require_once('travellog/model/Class.Country.php');

class TravelPlansController {
	private $errors = array();
	private $subNavigation;

	public function __construct() {
		// get session manager
		$this->obj_session   = SessionManager::getInstance();
		$this->template      = new Template();
	}
	
	public function initialize($travelerID, $subNavigation) {
		$this->travelerID    = $travelerID;
		$this->subNavigation = $subNavigation;		
	}

	// add error message for later display
	private function setError($message) {
		$this->errors[] = $message;
	}
	
	// retrieve array of error messages
	private function getErrors() {
		return $this->errors;
	}

	// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<     L I S T    >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	// display user travels
	public function executeList($isOwner) {
		$userID = TravelSchedule::getUserIDWithTravelerID($this->travelerID);

		// get the list of travel plans ordered with present travels first followed by
		// ongoing travels, then future travels (no dates)
		$travels = TravelSchedule::getOrderedTravels($userID);
		// get past travels
		$pastTravels = TravelSchedule::getTravelItems($userID, TravelSchedule::$PAST);

		// get list of countries
		$countryList = Country::getCountryList();
		// get countries the user wants to travel
		$countriesInterested = TravelSchedule::getCountriesToTravel($this->travelerID);
		
		$this->template->set_vars( array(
			'subNavigation'       => $this->subNavigation,
			'travel_wishes'       => $travels,
			'pastTravels'         => $pastTravels,
			'owner'               => $isOwner,
			'countryList'         => $countryList,
			'countriesInterested' => $countriesInterested,
			'callingTemplate'     => 1
		));

		$this->template->out('travellog/views/tpl.ViewTravelPlansMain.php');
	}
	// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<     END LIST    >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	
	public function executeCreate() {
		$travel = new TravelPlan();
		
		// default for a travel is one destination
		$travelDestination = new TravelDestination();
		
		// set default start date to today
		$travelDestination->setStartDate(date('Y-m-d', time()));
		
		$travel->addDestination($travelDestination);	   

		$this->displayForm($travel);
	}

	public function executeEdit() {
		// edit permission should be granted only to the owner
		if (!TravelSchedule::isValid(trim($_GET['travelwishid']), TravelSchedule::getUserIDWithTravelerID($this->obj_session->get('travelerID')))) {
			header('location: /travelplans.php');
		}

		// retrieve the travel plan
		$travel = TravelSchedule::retrieveByPk($_GET['travelwishid']);

		$this->displayForm($travel);
	}
	
	// displays the the form for adding or modifying travel items
	public function displayForm($travel) {
		$this->template->set_vars( array(
			'countryList'	=> Country::getCountryList(),
			'errors'		=> $this->getErrors(),
			'travel'		=> $travel,
			'subNavigation' => $this->subNavigation
		));
		
		$this->template->out('travellog/views/tpl.FrmTravelSchedule.php');
	}
	
	public function executeSave() {
		// check to make sure this page is only accessed through post method
		if ($_SERVER['REQUEST_METHOD'] != 'POST') {
			header('location: /travelplans.php');
		}
		
		$travel = new TravelPlan();
		
		// set values
		$travel->setTravelWishID($_POST['hdnTravelWishID']);
		$travel->setUserID(TravelSchedule::getUserIDWithTravelerID($this->obj_session->get('travelerID')));
		$travel->setTitle(trim($_POST['title']));
		$travel->setDetails(trim($_POST['details']));

		// transform destinations into objects
		$arrDestinations = array();
		for ($i = 0; $i < count($_POST['selCountries']); $i++) {
			$destination = new TravelDestination();
			
			$destination->setTravelWishID($travel->getTravelWishID());

			// use country object
			$country = new Country($_POST['selCountries'][$i]);
			$destination->setCountry($country);

			$destination->setDescription(trim($_POST['dest_descriptions'][$i]));
			
			// compose start date in 2007-01-01 format
			$startDate = !$_POST['start_years'][$i] ? '000' : '';
			$startDate .= $_POST['start_years'][$i] . '-';
			$startDate .= strlen($_POST['start_months'][$i]) == 1 ? '0' : '';
			$startDate .= $_POST['start_months'][$i] . '-';
			$startDate .= strlen($_POST['start_days'][$i]) == 1 ? '0' : '';
			$startDate .= $_POST['start_days'][$i];

			$destination->setStartDate($startDate);
			
			// compose end date in 2007-01-01 format
			$endDate = !$_POST['end_years'][$i] ? '000' : '';
			$endDate .= $_POST['end_years'][$i] . '-';
			$endDate .= strlen($_POST['end_months'][$i]) == 1 ? '0' : '';
			$endDate .= $_POST['end_months'][$i] . '-';
			$endDate .= strlen($_POST['end_days'][$i]) == 1 ? '0' : '';
			$endDate .= $_POST['end_days'][$i];

			$destination->setEndDate($endDate);
			
			$arrDestinations[] = $destination;
		}
		
		$travel->setDestinations($arrDestinations);

		if ($this->checkForErrors()) {
			$this->displayForm($travel);
			exit;
		} else {
			$travel->save();
		}

		// redirect back to the calling template, src = 2 => profile page
		if ($_POST['src'] == 2) {
			header("location: /profile.php?travelerID=" . $this->obj_session->get('travelerID'));
		} elseif ($_POST['src'] == 3) {
			header('location: /passport.php');
		} else {
			header('location: /travelplans.php');
		}
	}

	// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<      D E L E T E     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	// delete a travel item
	public function executeDelete() {
		$travel = TravelSchedule::retrieveByPk(trim($_GET['travelwishid']));

		if (TravelSchedule::isValid(trim($_GET['travelwishid']), TravelSchedule::getUserIDWithTravelerID($this->obj_session->get('travelerID')))) {
			$travel->delete();
		}
		
		// return to profile if src=2
		if (isset($_GET['src']) && $_GET['src'] == 2) {
			header("location: /profile.php?travelerID=" . $this->obj_session->get('travelerID'));
		} elseif (isset($_GET['src']) && $_GET['src'] == 3) {
			header("location: /passport.php");
		} else {
			header('location: /travelplans.php');	
		}

	}
	// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<      END DELETE     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<      D E L E T E   D E S T I N A T I O N    >>>>>>>>>>>>>>>>>>>>>>>>>>
	// delete a travel destination
	public function executeDelDest() {
		require_once 'travellog/model/Class.TravelDestination.php';
		require_once 'travellog/model/Class.TravelDestinationPeer.php';

		$destination = new TravelDestination;
		
		if (TravelSchedule::isValid(trim($_GET['travelwishid']), TravelSchedule::getUserIDWithTravelerID($this->obj_session->get('travelerID')))) {
			if (TravelDestinationPeer::destinationExists($_GET['travelwishid'], $_GET['countryid'])) {
				// delete a travel destination
				TravelDestinationPeer::deleteDestination($_GET['travelwishid'], $_GET['countryid']);
			}
		}		
		
		// return to profile if src=2
		if (isset($_GET['src']) && $_GET['src'] == 2) {
			header("location: /profile.php?travelerID=" . $this->obj_session->get('travelerID'));
		} elseif (isset($_GET['src']) && $_GET['src'] == 3) {
			header("location: /passport.php");
		} else {
			header('location: /travelplans.php');	
		}		
	}
	// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<      END DELETE DESTINATION     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<      EDIT COUNTRIES TO TRAVEL     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	// edit the countries the user wants to travel
	public function executeEditCountriesToTravel() {
		echo 'EditCountriesToTravel'; exit;
		$selCountries = array();
		
		if (count($_POST['sel_countries'])) {
			foreach ($_POST['sel_countries'] as $countryID) {
				$selCountries[] = $countryID;
			}
		}

		TravelSchedule::editCountriesToTravel($selCountries, $this->obj_session->get('travelerID'));

		if (isset($_GET['src'])) {
			switch($_GET['src']) {
				case '2':	// profile
					header('location: /profile.php?travelerID=' . $this->obj_session->get('travelerID'));
					exit;
					
				case '3':	// passport
					header('location: /passport.php');
					exit;
			}	
		}
		
		header('location: /travelplans.php');	   		
		exit;
	}
	// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<      EDIT COUNTRIES TO TRAVEL     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	// validate form data
	public function checkForErrors() {
		$errors = 0;
		
		// The title field is required
		if (!trim($_POST['title'])) {
			$this->setError('Please name this trip');
	
			$errors++;
		}
		
		// Travel Details is required
		if (!trim($_POST['details'])) {
			$this->setError('Please write a few details about this trip');
			
			$errors++;
		}
		
		// make sure the details doesn't exceed 100 characters
		if (strlen(trim($_POST['details'])) > 100) {
			$this->setError('Details must not exceed 100 characters');
			
			$errors++;
		}

		// make sure a country is selected
		for ($i = 0; $i < count($_POST['selCountries']); $i++) {
			if ($_POST['selCountries'][$i] == 0) {
				// destination added without selected country
				$this->setError('All added destinations must have a chosen country');
				
				$errors++;
				break;
			}
		}

		// make sure no duplicate countries are selected
		for ($i = 0; $i < count($_POST['selCountries']); $i++) {
			for ($j = 0; $j < count($_POST['selCountries']); $j++) {
				if ($i == $j) {
					continue;
				}
				
				// duplicate country detected
				if ($_POST['selCountries'][$i] != 0 && $_POST['selCountries'][$i] == $_POST['selCountries'][$j]) {
					$this->setError('Please select unique destinations');
					
					$errors++;
					break 2;
				}
			}
		}
		
		// make sure descriptions for destinations do not exceed 250 characters
		for ($i = 0; $i < count($_POST['selCountries']); $i++) {
			if (strlen(trim($_POST['dest_descriptions'][$i])) > 250) {
				$this->setError('One or more destination description exceeds the 250-character limit');
				
				$errors++;
				break;
			}
		}
		
		// check to make sure all start dates are valid
		for ($i = 0; $i < count($_POST['selCountries']); $i++) {
			if (!$this->isValidDate($_POST['start_months'][$i], $_POST['start_days'][$i], $_POST['start_years'][$i])
				&& !$this->isValidEmptyDate($_POST['start_months'][$i], $_POST['start_days'][$i], $_POST['start_years'][$i])) {
				
				$this->setError('
					Please check the start dates you have chosen for each destination. Accepted dates are
					<ul>
						<li>month day and year</li>
						<li>month & year</li>
						<li>year</li>
					</ul>'
				);
				
				$errors++;
				break;
			}
		}

		// check to make sure all end dates are valid
		for ($i = 0; $i < count($_POST['selCountries']); $i++) {
			if (!$this->isValidDate($_POST['end_months'][$i], $_POST['end_days'][$i], $_POST['end_years'][$i])
				&& !$this->isValidEmptyDate($_POST['end_months'][$i], $_POST['end_days'][$i], $_POST['end_years'][$i])) {
				
				$this->setError('
					Please check the end dates you have chosen for each destination. Accepted dates are
					<ul>
						<li>month day and year</li>
						<li>month & year</li>
						<li>year</li>
					</ul>'
				);
				
				$errors++;
				break;
			}
		}
		
		// check to make sure that all end dates have a start date
		for ($i = 0; $i < count($_POST['selCountries']); $i++) {
			if ($this->isValidDate($_POST['end_months'][$i], $_POST['end_days'][$i], $_POST['end_years'][$i])
				&& $this->isValidEmptyDate($_POST['start_months'][$i], $_POST['start_days'][$i], $_POST['start_years'][$i])) {
			
				$this->setError('All end dates must have valid start dates');
				
				$errors++;
				break;
			}
		}

		// all start dates must be earlier than their end dates
		for ($i = 0; $i < count($_POST['selCountries']); $i++) {
			if ($this->isValidDate($_POST['start_months'][$i], $_POST['start_days'][$i], $_POST['start_years'][$i])
				&& $this->isValidDate($_POST['end_months'][$i], $_POST['end_days'][$i], $_POST['end_years'][$i])) {
				
				$theStartDate = $this->createDummyDate($_POST['start_months'][$i], $_POST['start_days'][$i], $_POST['start_years'][$i]);
				$theEndDate   = $this->createDummyDate($_POST['end_months'][$i], $_POST['end_days'][$i], $_POST['end_years'][$i]);
				
				if ($theEndDate < $theStartDate) {
					$this->setError('One or more end date is earlier than its start date');
					
					$errors++;
					break;
				}
			}
		}

		return $errors;
	}
	
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> H E L P E R S <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
	// check to whether or not the date input is accepted
	private function isValidDate($month, $day, $year) {
		// valid dates format Jan 1 2007, Jan 2007, 2007
		if ($year && !($day && !$month)) {
			if ($month && $day) {
				if (!checkdate($month, $day, $year)) {
					return false;
				}
			}
			
			return true;
		}
		
		return false;
	}
	
	// check whether or not the date is a valid empty date
	private function isValidEmptyDate($month, $day, $year) {
		if (!$month && !$day && !$year) {
			return true;
		}
		
		return false;
	}
	
	// creates a formatted date accepted by date() and fills in incomplete acceptable dates
	private function createDummyDate($month, $day, $year) {
		if ($year && !$day && !$month) {				// 2007 becomes 01-01-2007
			return mktime(0, 0, 0, 1, 1, $year);
		} elseif ($year && $month && !$day) {			// March 2007 becomes 03-01-2007
			return mktime(0, 0, 0, $month, 1, $year);
		} elseif ($year && $month && $day) {			// April 17, 2007 becomes 04-17-2007
			return mktime(0, 0, 0, $month, $day, $year);
		}
	}
}
?>