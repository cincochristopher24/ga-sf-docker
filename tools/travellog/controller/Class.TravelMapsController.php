<?php
/*
	Filename:	  Class.TravelMaps.php
	Author:		  Jonas Tandinco
	Purpose:	  module controller for travel maps actions
	Date created: Dec/21/2007

	EDIT HISTORY:
*/
require_once 'travellog/model/Class.SessionManager.php';
require_once 'travellog/model/socialApps/mytravelmap/Class.TravelMapPeer.php';

class TravelMapsController {
	// CONSTRUCTOR
	public function __construct() {
		$this->obj_session  = SessionManager::getInstance();
	}

	public function executeEditCountriesTraveled() {
		$newCountries = isset($_POST['sel_countries']) ? $_POST['sel_countries'] : array();
		TravelMapPeer::setCountriesTraveled($this->obj_session->get('travelerID'), $newCountries);
		
		// redirect to referer
		header("location: {$_SERVER['HTTP_REFERER']}");
	}
}