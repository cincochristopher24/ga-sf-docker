<?php
/*
	Filename:		Class.AbstractTravelPlansController.php
	Author:			Jonas Tandinco
	Date Created:	Nov/12/2007
	Putpose:		contains default behavior for travel plans features 

	EDIT HISTORY:
	
	Nov/27/2007 by Jonas Tandinco
		1.	subnavigation object is transferred here so it can be overridden by co branded sites
		2.	all authentication for user allowed actions are all done here
		
	Dec/19/2007 by Jonas Tandinco
		1.	modified call to canvass page to always use url-provided travelerID. If none is present, use
			the session
*/

require_once("travellog/model/Class.SessionManager.php");
require_once("travellog/controller/Class.IController.php");
require_once("travellog/factory/Class.FileFactory.php");

//abstract class AbstractTravelPlansController implements IController {
abstract class AbstractTravelPlansController {
	protected $file_factory = NULL;
	
	public function __construct() {
		//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');

		$this->obj_session  = SessionManager::getInstance();
		$this->file_factory = FileFactory::getInstance();
		
		$this->file_factory->registerClass('Template' , array( 'path' => '' ));
		
		// register classes to be used
		$this->file_factory->registerClass('TravelPlansController', array('path' => 'travellog/controller/'));
		$this->file_factory->registerClass('SubNavigation', array('path' => 'travellog/model/'));
		
		// set default path for templates, css, and header footer views
		$this->file_factory->registerTemplate('LayoutMain');
		$this->file_factory->setPath('CSS', '/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
	}
	
	public function performAction(){
		//Temporary redirect HAM 14Nov2008//////////////
		$sessMan = $this->file_factory->invokeStaticClass('SessionManager','getInstance',array());
		$travelerID = $sessMan->get('travelerID');

		if(0 == $travelerID) header('Location: index.php');
		header('location: /widget.php?travelerID='.$travelerID.'&action=editMTB');
		exit;
		////////////////////////////////////////////////	
		
		if (isset($_GET['travelerID'])) {
			$isOwner = false;
			$travelerID = $_GET['travelerID'];
		} elseif ($this->obj_session->getLogin()) {
			$isOwner = true;
			$travelerID = $this->obj_session->get('travelerID');
			
			// insert code for logger system
			/**
			require_once('gaLogs/Class.GaLogger.php');
			GaLogger::create()->start($travelerID,'TRAVELER');
			**/
			
		} else {
			// redirect to home page
			header("location: /login.php?failedLogin&redirect={$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}");
			exit;
		}
/*
		if ($this->obj_session->getLogin()) {
			$isOwner = true;
			$travelerID = $this->obj_session->get('travelerID');
		} elseif (isset($_GET['travelerID'])){
			$isOwner = false;
			$travelerID = $_GET["travelerID"];
		} else {
			// redirect to home page
			header("location: /login.php?failedLogin&redirect={$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}");
			exit;
		}
*/		
		$subNavigation = $this->file_factory->getClass('SubNavigation');
		$subNavigation->setContext("TRAVELER");
		$subNavigation->setContextID($travelerID);

		// determine desired action from query string
		if (isset($_GET['action'])) {
			if (strtolower($_GET['action']) == 'add') {
				$action = 'add';
			} elseif (strtolower($_GET['action']) == 'edit' && isset($_GET['travelwishid'])) {
				$action = 'edit';
			} elseif (strtolower($_GET['action']) == 'save') {
				$action = 'save';
			} elseif (strtolower($_GET['action']) == 'delete' && isset($_GET['travelwishid'])) {
				$action = 'delete';
			} elseif (strtolower($_GET['action']) == 'deldest' && isset($_GET['travelwishid']) && isset($_GET['countryid'])) {
				$action = 'deldest';
			} elseif (strtolower($_GET['action']) == 'setcountries') {
				$action = 'setcountries';
			}
		} else {
			$action = 'view';
		}
		
		$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain'))); 
		
		// execute action
		$travelPlansController = $this->file_factory->getClass('TravelPlansController');
		$travelPlansController->initialize($travelerID, $subNavigation);

		switch ($action) {
		case 'add': // add travel item
			$travelPlansController->executeCreate();
			break;
		case 'edit': // modify travel item entry
			$travelPlansController->executeEdit();
			break;
		case 'save': // save add/edit
			$travelPlansController->executeSave();
			break;
		case 'delete': // delete travel item
			$travelPlansController->executeDelete();
			break;
		case 'deldest':
			$travelPlansController->executeDelDest(); // delete travel destination 
			break;
		case 'setcountries':
			$travelPlansController->executeEditCountriesToTravel();
		default: // list the traveler's travel schedule
			$travelPlansController->executeList($isOwner, $subNavigation);
			break;
		}
	}
}
?>
