<?php
/*
	Filename:		Class.AbstractTravelMapsController.php
	Author:			Jonas Tandinco
	Date Created:	Dec/21/2007
	Putpose:		contains default behavior for mytravelmaps features 

	EDIT HISTORY:
*/

require_once("travellog/model/Class.SessionManager.php");
require_once("travellog/controller/Class.IController.php");
require_once("travellog/factory/Class.FileFactory.php");

//abstract class AbstractTravelPlansController implements IController {
abstract class AbstractTravelMapsController {
	protected $file_factory = NULL;
	
	public function __construct() {
		//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');

		$this->obj_session  = SessionManager::getInstance();
		$this->file_factory = FileFactory::getInstance();
		
		// register classes to be used
		$this->file_factory->registerClass('TravelMapsController', array('path' => 'travellog/controller/'));
	}
	
	public function performAction(){
		if (!$this->obj_session->getLogin()) {
			// redirect to home page
			header("location: /login.php?failedLogin&redirect={$_SERVER['PHP_SELF']}?{$_SERVER['QUERY_STRING']}");
			exit;
		}

		if (isset($_GET['action']) && trim($_GET['action'] == '1')) {
			$action = 'editcountriestraveled';
		} else {
			// TODO: redirect to file not found page
		}
		
		// execute action
		$travelMapsController = $this->file_factory->getClass('TravelMapsController');

		switch ($action) {
		case 'editcountriestraveled': // add travel item
			$travelMapsController->executeEditCountriesTraveled();
			break;
		}
	}
}
?>
