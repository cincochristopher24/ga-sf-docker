<?php
	require_once("travellog/controller/Class.IController.php");
	require_once("travellog/factory/Class.FileFactory.php");

	abstract class AbstractProfileController implements IController{

		protected $properties = array('showAds' => true, 'groupCount' => 0);
		
		public $obj_session  = NULL, $file_factory = NULL;

		function __construct(){

			$this->file_factory = FileFactory::getInstance();

			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('TravelerProfile');
			$this->file_factory->registerClass('CommentsView' , array('path' => 'travellog/views/'));
			$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerClass('AddressBookEntry');			
			$this->file_factory->registerClass('Calendar');
			$this->file_factory->registerClass('TravelJournalRSSFeeder2');
			$this->file_factory->registerClass('HelpText');
			$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
			$this->file_factory->registerClass('SessionManager', array("path" => "travellog/model/"));
			$this->file_factory->registerClass('JournalsComp', array('path' => 'travellog/views/'));
			$this->file_factory->registerClass('ArticleComp', array('path' => 'travellog/views/'));		
//			$this->file_factory->registerClass('MessagesPanel', array('path' => 'travellog/views/'));		
//			$this->file_factory->registerClass('MessageSpace');
			$this->file_factory->registerClass('TravelMapPeer', array('path' => 'travellog/model/socialApps/mytravelmap/'));
			$this->file_factory->registerClass('Country');
			$this->file_factory->registerClass('TravelSchedule');
			$this->file_factory->registerClass('Advertisement');
			$this->file_factory->registerClass('Template',array('path'=>''));
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('ProfileQuestion', array('path' => 'travellog/model/travelbio/'));
			$this->file_factory->registerClass('FeaturedAlbumsView',array('path' => 'travellog/views/'));
		
			// message center
			$this->file_factory->registerClass('gaMessageCenterMapper', array('path' => 'travellog/model/messageCenter/'));
			$this->file_factory->registerClass('gaTravelerMapper', array('path' => 'travellog/model/messageCenter/'));
			
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/views'));		
			$this->file_factory->registerTemplate('IncMyGroups', array('path'=>'travellog/views/','file'=>'IncTravelerGroups'));

			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');		

			//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
		}

		function performAction() {
		
		
			$this->properties['travelerID'] = $this->file_factory->invokeStaticClass('SessionManager','getInstance',array())->get('travelerID');
			
			if (isset($_GET['travelerID'])) {
				try {
					$this->properties['traveler'] = $this->file_factory->getClass('Traveler', array($_GET['travelerID']));
					
					if ($this->properties['traveler']->isDeactivated() || $this->properties['traveler']->isBlocked($this->properties['travelerID'])) {//NOTE: isBlocked can accept IDs not related to the containing object(traveler). though this shouldn't be the case
						header("location: travelers.php");
						exit;
					}						
				} catch (Exception $e) {
					header("location : Logout.php");
					exit;
				}
			} else {
				header("Location:http://".$_SERVER['SERVER_NAME']."/index.php");
				exit;						
			}
			
			$this->properties['travelerprofile'] = $this->file_factory->getClass('TravelerProfile',array($_GET['travelerID']));
			if ($this->properties['travelerprofile']->getIssuspended() 
				&& !$this->file_factory->invokeStaticClass('SessionManager','getInstance',array())->isSiteAdmin()) {
				header('Location: index.php');
				exit;
			}

			$this->properties['isLogin'] = (0 != $this->properties['travelerID']);
			$this->properties['owner'] = ($this->properties['travelerID'] == $_GET['travelerID']);
			$this->__incrementViews();
			
			$start	=	microtime(true);
			$this->prepareMainTemplate();
			$end	= 	microtime(true);
			//echo 'AbstractProfileController->prepareMainTemplate() took ' . ($end - $start) . ' sec <br/>';
			
			$start	=	microtime(true);
			$this->prepareDependencies();
			$end	= 	microtime(true);
			//echo 'prepareDependencies() took ' . ($end - $start) . ' sec <br/>';

			//Main profile 
			$start	=	microtime(true);
			$profile_comp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$TRAVELER_CONTEXT );
			$profile_comp->init($_GET['travelerID']);

			$this->properties['template'] = $this->file_factory->getClass('Template');			
			$this->properties['template']->set_path('travellog/views/');		
			$this->properties['template']->set('profile_comp', $profile_comp->get_view());
			$end	= 	microtime(true);
			//echo 'Profile Component took ' . ($end - $start) . ' sec <br/>';
			
			
			//Template properties shared by the components 
			$this->properties['template']->set('owner', $this->properties['owner']);
      		$this->properties['template']->set('travelerID', $this->properties['travelerID']); 
			$this->properties['template']->set('isLogin', $this->properties['isLogin']);
			
			
			$start	=	microtime(true);
			$this->prepareComponent('SubNavigation');
			$end	= 	microtime(true);
			//echo 'prepareSubnavigation took ' . ($end - $start) . ' sec <br/>';
			
			$start	=	microtime(true);
			$this->prepareComponent('PhotoUIFacade');
			$end	= 	microtime(true);
			//echo 'prepare PhotoUIFacade took ' . ($end - $start) . ' sec <br/>';
			
			$start = microtime(true);
			$this->prepareComponent('JournalsComp');
			$end	= 	microtime(true);
			//echo 'prepare Journals component took ' . ($end - $start) . ' sec <br/>';
			
			
			// if(strpos($_SERVER['HTTP_HOST'],'dev.') >= 0 || strpos($_SERVER['HTTP_HOST'],'.local') >= 0)
			// 	$this->prepareComponent('ArticleComp');
			$start	=	microtime(true);
			$this->prepareComponent('ArticleComp');
			$end	= 	microtime(true);
			//echo 'prepare Article component took ' . ($end - $start) . ' sec <br/>';

			$this->prepareComponent('Comments');
			$end	= 	microtime(true);
			//echo 'prepare Comments took ' . ($end - $start) . ' sec <br/>';
			
			$start	=	microtime(true);
			$this->prepareComponent('MyTravelMap');
			$end	= 	microtime(true);
			//echo 'prepare MyTravelMap took ' . ($end - $start) . ' sec <br/>';
			
			$start	=	microtime(true);
			$this->prepareComponent('MyTravelPlans');
			$end	= 	microtime(true);
			//echo 'prepare MyTravelPlans took ' . ($end - $start) . ' sec <br/>';
			
			$start	=	microtime(true);
			$this->prepareComponent('TravelJournalRssFeeder');
			$end	= 	microtime(true);
			//echo 'prepare TravelJournalRssFeeder took ' . ($end - $start) . ' sec <br/>';
			
			$start	=	microtime(true);
			$this->prepareComponent('Advertisement');
			$end	= 	microtime(true);
			//echo 'prepare Advertisement took ' . ($end - $start) . ' sec <br/>';
			
			$start	=	microtime(true);
			$this->prepareComponent('Videos');
			$end	= 	microtime(true);
			
			//Optional components
			$privacyPreferences = $this->properties['traveler']->getPrivacyPreference();
		
			if ($this->properties['owner'] || $privacyPreferences->canViewCalendar($this->properties['travelerID'])) {
				$start = microtime(true);
				$this->prepareComponent('Events');
				$end	= 	microtime(true);
				//echo 'prepare Events took ' . ($end - $start) . ' sec <br/>';
			}
			
			
			if ($this->properties['owner'] || $privacyPreferences->canViewFriends($this->properties['travelerID'])) {
				$start = microtime(true);
				$this->prepareComponent('Friends');
				$end	= 	microtime(true);
				//echo 'prepare Friends took ' . ($end - $start) . ' sec <br/>';				
			}
			if ($this->properties['owner'] || $privacyPreferences->canViewGroups($this->properties['travelerID'])) {
				$start = microtime(true);
				$this->prepareComponent('Groups');
				$end	= 	microtime(true);
				//echo 'prepare Groups took ' . ($end - $start) . ' sec <br/>';
			}
			
			//remove steps in profile page since it's now in dashboard
			/*if ($this->properties['owner'] && 
					! (!$this->properties['traveler']->getShowSteps() ||
				  		$this->properties['traveler']->isProfileCompleted() ||
				  		$this->properties['groupCount'] ||
				  		$this->properties['travelPlansCount'] ||
				  		$this->properties['travelMapCount'] ||
				  		$this->properties['traveler']->getCountTravelJournals() || 
				  		$this->file_factory->invokeStaticClass('ProfileQuestion', 'getNumberofQuestionsWithAnswer', array($this->properties['travelerID'])) ||
			      		count($this->file_factory->invokeStaticClass('AddressBookEntry', 'getAddressBookEntriesByOwnerTravelerID', array ($this->properties['travelerID']))))) {
				$this->prepareComponent('Steps');//NOTE: Steps needs the TravelPlans, TravelBio, TravelMap and Groups components initialized first
		    }*/
		   
		   //remove message center mini in profile page since it's now in dashboard
		    /*if ($this->properties['owner']) { 
				$start	=	microtime(true);
		    	$this->prepareComponent('MessageCenter');
		    	$end	= 	microtime(true);
				//echo 'prepare MessageCenter took ' . ($end - $start) . ' sec <br/>';
							
			}*/
			
			$this->prepareComponent('TravelerFeaturedAlbums');
			
			Template::includeDependentCss("/css/vsgStandardized.css");
			Template::includeDependentCss("/css/modalBox.css");
			
			$start	=	microtime(true);
			$this->properties['template']->out('tpl.ViewTravelerProfile.php');
			$end	= 	microtime(true);
			//echo 'output template took ' . ($end - $start) . ' sec <br/>';
		}

		private function prepareComponent($componentName) {
			$this->{'prepare'.$componentName}();
		}
		
		private function prepareSteps() {
			//TODO: get rid of these libraries
			//Template::includeDependentJs("/js/jquery-1.1.4.pack.js");
			Template::includeDependentJs("/js/jquery.jcarousel.js");
			Template::includeDependentJs("/js/jquery.ready.js");
			Template::includeDependentCss("/css/jquery.jcarousel.css");
 			$jscode = <<<BOF
 				<script type="text/javascript">
					function removeSteps(travelerID){
						new Ajax.Request('/ajaxpages/unshowSteps.php',{method : 'get',parameters: {travelerid : travelerID}  });
						document.getElementById('stepmaincontainer').style.display = 'none';
					}
					
					jQuery.noConflict();
					jQuery.elementReady('steps_carousel', function() {
    					jQuery('#steps_carousel').jcarousel();
					});
				</script>
BOF;
			Template::includeDependent($jscode);
			
			/** added by neri - 06/11/09 **/
			if (isset($GLOBALS['CONFIG']))
				$this->properties['template']->set('siteName', $GLOBALS['CONFIG']->getSiteName());
			else
				$this->properties['template']->set('siteName', 'GoAbroad.net');
				
			$this->properties['template']->set('steps',$this->properties['template']->fetch("tpl.Steps3.php"));	 
		}
		
		private function prepareJournalsComp(){
			$jCompContextAr = $this->file_factory->invokeStaticClass('JournalsComp','convertURLParamToSetupArray');
			
			if (count($jCompContextAr) == 0) {// setup for viewing all journals
				$jCompContextAr['VIEWMODE'] = 'PROFILE';
				if ($this->properties['travelerID']) $jCompContextAr['SECURITY_CONTEXT'] = 'LOGIN';			
			}
			
			// flag if viewing profile from a cobrand
			if(isset($GLOBALS['CONFIG']))
				$jCompContextAr['COBRAND_PROFILE'] = true;
					
			$this->properties['template']->set('journalsComp',$this->file_factory->getClass('JournalsComp',array($jCompContextAr,$this->properties['traveler']->getTravelerID(),$this->properties['travelerID']))); 
		}
		
		private function prepareArticleComp(){
			$setup = array(
				'OWNER' => $this->properties['owner'],
				'CONTEXT' => "author",
				'DATA' => $this->properties['traveler']->getTravelerID(),
				'SUBJECT' => $this->properties['traveler'],
				'VIEWMODE' => "PROFILE"
			);
			
			$this->properties['template']->set('articleComp',$this->file_factory->getClass('ArticleComp',array($setup)));
		}
		
		private function prepareMainTemplate() {
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));		
		
			$travelerprofile = $this->properties['travelerprofile'];
			$username = $travelerprofile->getUsername();
			$travelertype = $travelerprofile->getTravType()->gettravelertype();
		
			$hometown = NULL; $country = NULL; $currhometown = NULL; $currcountry = NULL;
			// edits: checked if locations are valiv
			if ($travelerprofile->getHTLocationID()) {
				$city = $travelerprofile->getCity();
				$ctry = (!is_null($city)) ? $city->getCountry() : null;
				if(!is_null($city) && !is_null($ctry)){	
					$hometown = $city->getName();
					$country = $ctry->getName();
				}
			}
		
			if ($travelerprofile->getCurrLocationID()) {
				$currentLocation = $travelerprofile->getCurrLocation();
				$ctry = (!is_null($currentLocation)) ? $currentLocation->getCountry() : null;
				if(!is_null($currentLocation) && !is_null($ctry)){
					$currhometown = $currentLocation->getName();
					$currcountry = $ctry->getName();
				}
			}
				
			if ($this->properties['owner']) {
				Template::setMainTemplateVar('title',  'My Profile - GoAbroad Network: Online Community for Travelers');
				Template::setMainTemplateVar('page_location', 'My Passport');
			} else {
				$tmpTitlePhrase = (strlen($username)-1 == strripos($username,'s'))?$username."'":$username."'s"; //TODO: don't know if this is still used elsewhere - icasimpan Aug 13, 2007
				$tmpUsernamePossessive = (strlen($username)-1 == strripos($username,'s'))?$username."'":$username."'s";
				$tmpBirthPlace = ($hometown!=$country)?"$hometown, $country":$country;
				$tmpDescriptionPhrase = ($travelertype)?$travelertype:'traveler';

				Template::setMainTemplateVar('title',  $tmpUsernamePossessive . ' Travel Journals - Travel Blogs - Travel Reviews');
		 		Template::setMainTemplateVar('metaDescription', $tmpUsernamePossessive . ' Travel Profile on GoAbroad Network. Check out the  wonderful travel experiences of this ' . $tmpDescriptionPhrase . ' from ' . $tmpBirthPlace);
				Template::setMainTemplateVar('metaKeywords', 'travel review, travel journal, travel blog, travel experiences, travel advice, travel photos, travel maps');
				Template::setMainTemplateVar('page_location', 'Travelers');
			}

			Template::setMainTemplateVar('layoutID', 'page_profile');		
		}
	
		/**
		 * Inline scripts and css, referenced CSS and JS files. Only files that are
		 * shared by the included components or are germane to the profile page 
		 * should be included here, otherwise they can be included in the handlers 
		 * for the specific component.
		 * 
		 * Note that the order and/or position of the script in the document could be significant.
		 * 
		 * @return void
		 */
		private function prepareDependencies() {
			Template::includeDependent('<!--[if lt IE 7.]><script defer="defer" type="text/javascript" src="/js/pngfix.js"></script><![endif]-->');
		}
	
		private function prepareSubNavigation() {
			$subNavigation = $this->file_factory->getClass('SubNavigation');		
			$subNavigation->setContext('TRAVELER');
			$subNavigation->setContextID($_GET['travelerID']);
			$subNavigation->setLinkToHighlight('MY_PROFILE');		
		
			$this->properties['template']->set("subNavigation",$subNavigation);
		}
	   
	 
		private function preparePhotoUIFacade(){
			require_once("travellog/UIComponent/Photo/facade/PhotoUIFacade.php");
			$Photo = new PhotoUIFacade();
			$Photo->setContext('profile');
			$Photo->setGenID($_GET['travelerID']);
			$Photo->setLoginID($this->properties['travelerID']);
			$Photo->build();		
		}
	   
		protected function prepareMessageCenter() {
			require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
			require_once 'travellog/model/messageCenter/manager/Class.gaMessagesManagerRepository.php';
			require_once('travellog/model/Class.SiteContext.php');
			
			$traveler = gaTravelerMapper::getTraveler($this->properties['travelerID']);
			
			$numMessagesDisplayed = 5;
			
			$siteContext = SiteContext::getInstance();
			$siteAccessType = $siteContext->isInCobrand() ? 2 : 1;
			
			$messagesManager = gaMessagesManagerRepository::load($traveler->getID(), $siteAccessType);
			
			$messages     = $messagesManager->getInboxWihtoutShoutOutMessages();
			$sentMessages = $messagesManager->getSentMessages();
			
			Template::includeDependentJs("/js/messageCenter/messages.js", array("bottom" => true));
			$template = $this->properties['template'];
			
			$template->set ( 'owner',           	 $traveler );
			$template->set ( 'isPowerful',           $this->properties['owner'] ? true : false );
			$template->set ( 'thereAreMoreMessages', count($messages) > $numMessagesDisplayed ? true : false );
			$template->set ( 'messages',             array_slice($messages, 0, $numMessagesDisplayed) );
			$template->set ( 'outboxMessages',       array_slice($sentMessages, 0, $numMessagesDisplayed) );
			$template->set ( 'snippetLimit',         100 );
			
			$template->set ( 'showReadStatus',       true  );
			$template->set ( 'showRealName',         false );
			
			$template->set ( 'canBeSelected',        false );
			$template->set ( 'canBeDeleted',         true  );
			
			$template->set ( 'canOutboxBeSelected',  false );
			$template->set ( 'canOutboxBeDeleted',   false );
			
			$template->set ( 'messageCenterView',    $template->fetch('messageCenter/tpl.ViewTravelerMessageCenterMini.php') );
		}
		
		private function prepareComments() {
			$commentsview = $this->file_factory->getClass('CommentsView');
			$commentsview->setContext(1, $this->properties['traveler']->getTravelerID());
			$commentsview->setTravelerID($this->properties['travelerID']);
			$commentsview->setOwnerID( $this->properties['traveler']->getTravelerID());
			$commentsview->setEnvURL($_SERVER['REQUEST_URI']."^comment_form");
			$commentsview->readComments();
			
			$this->properties['template']->set('commentsview',$commentsview);
		
		}
		
		private function prepareVideos() {
			$cobrandGroupID = (isset($GLOBALS['CONFIG'])) ? $GLOBALS['CONFIG']->getGroupID() : 0;
			$videos = $this->properties['traveler']->getTravelerVideos($cobrandGroupID, $this->properties['travelerID']);
			
			if (0 < count($videos)) {
				$recentVideosTemplate = $this->file_factory->getClass('Template');
				$recentVideosTemplate->set_path('travellog/views/');
				
				$recentVideosTemplate->set('travelerID', $this->properties['traveler']->getTravelerID());
				$recentVideosTemplate->set('logged', $this->properties['owner']);
				$recentVideosTemplate->set('videos', $videos);
				
				/*if ($this->properties['owner'])
					$recentVideosTemplate->set('label', 'Manage Videos');
				else*/
					if (3 < count($videos))
						$recentVideosTemplate->set('label', 'View All Videos');
				
				$template = $this->properties['template'];
				$template->set('videos', $recentVideosTemplate->fetch('tpl.IncTravelerRecentVideos.php'));
			}
		}
	
		private function prepareGroups() {
			$groups = $this->properties['traveler']->getGroups();
			$groupCount = count($groups);
			
			$this->properties['groupCount'] = $groupCount;//This is also used by the Steps component
			
			if (!$groupCount && !$this->properties['owner']) return;			
			
			$showGroupCount = false;
			// reduce the groups to display to 5 if groups are more than 5
			if (5 < $groupCount) {
				for ($i = 1; $i <= $groupCount - 5 ;$i++ ) {
			 		array_pop($groups);
			 	}
			 	$showGroupCount = true;
			}

			$groupsTemplate = $this->file_factory->getClass('Template');
			$groupsTemplate->set('groups',	$groups);
			$groupsTemplate->set('helpTextprofile',	true);
			$groupsTemplate->set('helpText',		nl2br($this->file_factory->getClass('HelpText')->getHelpText('profile-group')));
			$groupsTemplate->set('isowner',			$this->properties['owner']);
			$groupsTemplate->set('groupCount',			$groupCount);
			$groupsTemplate->set('showGroupCount',  $showGroupCount);
			$groupsTemplate->set('logged',			$this->properties['owner']);
			$groupsTemplate->set('traveler',		$this->properties['traveler']);

			$this->properties['template']->set("groups", $groupsTemplate->fetch($this->file_factory->getTemplate("IncMyGroups")));
		}
	
		private function prepareEvents() {
			$viewer = $this->file_factory->getClass('Traveler',array($this->properties['travelerID']));
			$traveler_log = $this->file_factory->getClass('Traveler',array($_GET['travelerID']));

			$cal = $this->file_factory->getClass('Calendar', array($traveler_log));
	
			$cal->setPrivacyContext($viewer);// set the current user logged in the system

			$events = $cal->getEvents(null,date('Y-m-d'));

			$eventCount = count($events);

			if (10 < $eventCount) {// reduce the events to 10 if there are more than 10 events on the calendar
				for ($i=1;$i <= $eventCount - 10 ;$i++ ) {
					array_pop($events);
				}
			}
			
			$calTemplate =$this->file_factory->getClass('Template');
			$calTemplate->set_path('travellog/views/');			
			$calTemplate->set("owner",$this->properties['owner']);
			$calTemplate->set("eventCount",$eventCount);
			$calTemplate->set("calEvents",$events);
			$calTemplate->set("link_view",'tID='.$_GET['travelerID']);

			$template = $this->properties['template'];			
			$template->set("calendar",$calTemplate->fetch("tpl.IncCalendar.php"));
			$template->set("eventCount",$eventCount);

		}
	
		//TODO: clean up code
		private function prepareFriends() {
			$template = $this->properties['template'];		
		
			$travelerID = $this->properties['travelerID'];
			$traveler = $this->properties['traveler'];
			$travelerprofile = $this->properties['travelerprofile'];
			$isOwner = $this->properties['owner'];
		
			$friendsTemplate = $this->file_factory->getClass('Template');
			$friendsTemplate->set_path('travellog/views/');
			$friendCount = 0;

			$friends = $traveler->getFriends($_filtercriteria = NULL, $_rowslimit = NULL, $prioritizeFriendsWithPhoto=TRUE);
			$friendCount = count($friends);
			
			if (0 == $friendCount){
				if (!$isOwner) return;
				
				//TODO: move to performAction() as this is used by "group" too 
				$ht = $this->file_factory->getClass('HelpText');
				$friendsTemplate->set('helpText', nl2br($ht->getHelpText('dashboard-friends')));
			}
			
			$showFriendsCount = false;
			if (12 < $friendCount) {
				//usort($friends, array('Traveler','orderByLastLogin'));
				for ($i = 1; $i <= $friendCount - 12 ; $i++ ) {
			 		array_pop($friends);
				}
				$showFriendsCount = true;
				//usort($friends, array('Group','orderByName'));
			}

			$friendsTemplate->set('friends', $friends);
			$friendsTemplate->set('count',			$friendCount);
			$friendsTemplate->set('showFriendsCount', $showFriendsCount);
			$friendsTemplate->set('traveler',		$travelerprofile);
			$friendsTemplate->set('islogin',		$travelerID?true:false);
			$friendsTemplate->set('isowner',		$isOwner);
			$friendsTemplate->set('ptravelerid',	$_GET['travelerID']);
			$traveler_frnd = $this->file_factory->getClass('Traveler', array($travelerID));
		
			// check if traveler is an advisor
			$isAdvisor = $traveler_frnd->isAdvisor();

			$friendsTemplate->set('isfriend', $traveler_frnd->isFriend($_GET['travelerID']));
			$friendsTemplate->set('isAdvisor', $isAdvisor);

			$friendsTemplate->set('viewAllFriendsLink', $isOwner ? '/friend.php' : '/friend.php?tID='.$travelerprofile->getTravelerID());
		
			if(0 != $travelerID){
				$traveler_log = $this->file_factory->getClass('Traveler',array($travelerID));
				$friendsTemplate->set('loggedUser',$traveler_log);
			}

			if($travelerID){

				/**
				 * Instantiate new traveler for Add as friends link
				 */

				$tra_veler		= $this->file_factory->getClass('Traveler',array($travelerID));
				$isfriend 		= $tra_veler->isFriend($_GET['travelerID']);
				$isblock  		= $tra_veler->isBlocked($_GET['travelerID']);
				$is_friend      = $tra_veler->isFriend($_GET['travelerID']);
				$pendingrequest = $tra_veler->getPendingRequests();
				$friendrequest	= $tra_veler->getFriendRequests();

				$pendingrequesttravID 	= array();
				$friendrequesttravID 	= array();

				if(count($pendingrequest)){
					foreach($pendingrequest as $i => $pending_request){
						$pendingrequesttravID[$pending_request->getTravelerID()]	= $pending_request->getTravelerID();
					}
				}
				$ispendingrequest = array_search($_GET['travelerID'],$pendingrequesttravID);

				if(count($friendrequest)){
					foreach($friendrequest as $i => $friend_request){
						$friendrequesttravID[$friend_request->getTravelerID()]	= $friend_request->getTravelerID();
					}
				}

				$isfriendrequest = array_search($_GET['travelerID'],$friendrequesttravID);


				$template->set('ispendingrequest',$ispendingrequest);
				$template->set('isfriendrequest',$isfriendrequest);
				$template->set('is_friend',$is_friend);

				/* to friends templete**/
				$friendsTemplate->set('ispendingrequest',$ispendingrequest);
				$friendsTemplate->set('isfriendrequest',$isfriendrequest);
				
				/*end to friends templete **/

				if(!$ispendingrequest && !$isfriendrequest && !$isfriend && !$isblock && !$tra_veler->isAdvisor()){
					$template->set('addasfriend',true);
				}

				if(!$isblock){
					$template->set('block',true);
				}
			}
			$template->set("friends",$friendsTemplate->fetch("tpl.IncTravelerFriends.php") );
		}

		private function prepareAdvertisement() {
			require_once('travellog/model/Class.SiteContext.php');
			if(!SiteContext::getInstance()->isInCobrand()){
				$this->properties['template']->set('advertisement', $this->file_factory->invokeStaticClass('Advertisement','getAd'));
			}
		}

		private function prepareTravelJournalRSSFeeder() {
			$TravelJournalRSSFeeder = $this->file_factory->getClass('TravelJournalRSSFeeder2');
			$this->properties['template']->set('hasTravelupdate', $TravelJournalRSSFeeder->hasTravelupdate($this->properties['traveler']));
		}

		private function prepareMyTravelMap() {
			$template = $this->properties['template'];
			$userMap = $this->file_factory->invokeStaticClass('TravelMapPeer', 'getTravelMap', array($this->properties['owner'] ? $this->properties['travelerID'] : $_GET['travelerID']));
			$this->properties['travelMapCount'] = count($userMap->getCountriesTraveled());//NOTE: used the by Steps component
			
			$template->set('travelerID', $_GET['travelerID']);
			$template->set('userMap', $userMap);
			$template->set('travelMapCount', $this->properties['travelMapCount']);
			$template->set('countryList', $this->file_factory->invokeStaticClass('Country', 'getCountryList'));
			$template->set('travelMap',   $template->fetch('tpl.ViewTravelMapsMini.php'));
			
			Template::includeDependentJs('http://maps.google.com/maps?file=api&v=2&key=' . $userMap->getGMapKey(), array('minify'=>false,'bottom'=>true));
		}

		private function prepareMyTravelPlans() {
			$template = $this->properties['template'];
		
			$travelPlansUser = $this->file_factory->invokeStaticClass('TravelSchedule',	'getTravelPlansUserWithTravelerID', array($_GET['travelerID']));
			$itemsToDisplay = isset($travelPlansUser['travelPlansDisplayed']) ? $travelPlansUser['travelPlansDisplayed'] : 5;
			$includePastTravels = isset($travelPlansUser['travelPlansIncludePast']) ? $travelPlansUser['travelPlansIncludePast'] : false;			
			
			$travelItems = $this->file_factory->invokeStaticClass('TravelSchedule', 'getOrderedTravels', array($travelPlansUser['userID']));
			
			if ($includePastTravels && (count($travelItems) < $itemsToDisplay)) {
				$travelItems = array_merge($travelItems, $this->file_factory->invokeStaticClass('TravelSchedule', 'getPastTravels', array($travelPlansUser['userID'])));
			}

			$totalNumItems = count($travelItems);
			$this->properties['travelPlansCount'] = $totalNumItems;//NOTE: used the by Steps component
			
			if ($totalNumItems > $itemsToDisplay) {
				for ($i = 1, $j=$totalNumItems - $itemsToDisplay; $i <= $j; $i++ ) {
					array_pop($travelItems);
				}
			}			

			$template->set('showMyTravelPlans', $this->properties['owner'] ? true : ($totalNumItems > 0));
			$template->set('totalNumItems', $totalNumItems);
			$template->set('travelItems', $travelItems);

			// TRAVEL WISH LIST
			$template->set('countryList',         $this->file_factory->invokeStaticClass('Country',        'getCountryList'));
			$template->set('countriesInterested', $this->file_factory->invokeStaticClass('TravelSchedule', 'getCountriesToTravel', array($_GET['travelerID'])));
			
			$template->set('incTravelplans', $template->fetch("tpl.ViewTravelPlansMini.php"));
		}

		private function prepareTravelerFeaturedAlbums(){
			$featuredAlbumsView = $this->file_factory->getClass('FeaturedAlbumsView',array($this->properties['traveler'],$this->properties['owner']));
			$this->properties['template']->set('featuredAlbumsView',$featuredAlbumsView);
		}
		
		private function __incrementViews(){
			if(!$this->properties['owner']){
				$profile = $this->properties['travelerprofile'];
				$profile->addViews();
			}
		}

		//TODO: do we still need this? can anyone confirm? hello? anybody out there?
//		protected function performAjaxAction() {
//		
//			$sessMan = $this->file_factory->invokeStaticClass('SessionManager', 'getInstance', array ());
//			$travelerID = $sessMan->get('travelerID');
//		
//			isset ($_REQUEST['action']) ? $action = $_REQUEST['action'] : $action = '';
//		
//			if (!$sessMan->getLogin()) {
//				// allow loadjournalentries action to be able to run even if user is not logged in
//				if ($action != 'loadjournalentries') {
//					echo 'Session already expired. Click my passport tab to relogin.';
//					return;
//				}
//			}
//	
//			/*********************************************
//			 *  for Message Center (display of messages)
//			 */
//			if ($action == 'showmessages') {
//				$viewType = $_POST['viewType'];
//
//				$obj_traveler = $this->file_factory->getClass('Traveler', array (
//					$travelerID
//				));
//				$obj_message_panel = $this->file_factory->getClass('MessagesPanel');
//				$obj_message_panel->setContext($obj_traveler);
//			
//				/******** last edited by: cheryl go			28 august 2008 ***********/
//				/******* purpose: remove view all link if there is no message ********/
//
//				if ($viewType == MessagesPanel::$BULLETIN)
//					$obj_message_panel->getBulletinsForMessagesPanel();
//				else if ($viewType == MessagesPanel::$PERSONAL_MESSAGE)
//					$obj_message_panel->getPersonalMessagesForMessagesPanel();
//
//				switch ($viewType) {
//					case 1:
//						echo '<div class="header_actions"><a class="add compose_msg" title="Send your friends private messages." href="messages.php?compose">Compose Message</a>&nbsp;|&nbsp;<a class="add" href="bulletinmanagement.php?method=compose">New Bulletin Post</a></div>';
//
//						break;
//					case 2 : // Personal messages add link
//						echo '<div class="header_actions">
//								<a class="add compose_msg" title="Send your friends private messages." href="messages.php?compose">Compose new message</a>';
//						if (0 < $obj_message_panel->getTotalNumberOfMessages())		
//							echo '&nbsp;|&nbsp;<a href="messages.php" class="more">View all</a>';
//						echo '</div>';
//
//						break;
//					case 3 :
//						echo '<div class="header_actions">
//								<a class="add bulletin_post" title="Post news or updates you want to share with your friends." href="bulletinmanagement.php?method=compose">Post new bulletin</a>';
//						if (0 < $obj_message_panel->getTotalNumberOfMessages())
//							echo '&nbsp;|&nbsp;<a href="bulletinmanagement.php" class="more">View all</a>';
//						echo '</div>';
//
//						break;
//					default :
//						echo '';
//
//				}
//				echo $obj_message_panel->render($viewType);
//
//				/************************ end of edit ****************************/
//				if(1==$viewType){
//					echo '<script>jQuery("#msgTotal").html("'.$obj_message_panel->getTotalNumberOfNewMessages().'");</script>';
//				}
//			}
//		
//			if ($action == 'loadjournalentries'){
//				require_once('travellog/views/Class.TravelsView.php');
//				$tid = $_REQUEST['travelid'];			
//				$tv = new TravelsView($this->file_factory->getClass('Traveler', array ($travelerID)));
//				echo $tv->renderAjTravelEntries($tid);
//			}
//		
//			if ($action == 'deletejournalentry'){
//				require_once('travellog/model/Class.TravelLog.php');
//				$tlogid = $_REQUEST['travellogID'];
//				$tl = new TravelLog($tlogid);
//				try {
//					$tl->Delete();
//					$t = 1;
//				}catch (Exception $e){
//					header("HTTP/1.0 500 Internal Server Error");
//					exit;
//				}
//				header("HTTP/1.0 200 OK");			
//			}
//		
//			if ($action == 'deleteAlert'){
//				try{	
//					require_once('travellog/model/Class.AlertMessage.php');
//					$msg = new AlertMessage($_POST['attrID']);
//				}
//				catch(exception $e){
//					header("HTTP/1.0 500 Internal Server Error");
//					exit;
//				}
//			
//				$messageSpace = $this->file_factory->getClass('MessageSpace');
//				$messageSpace->delete($_POST['attrID'], $msg->getDiscriminator(), $msg->getMessageId());
//				$_REQUEST['action'] = 'showmessages';
//				$this->performAction();
//			}
//		}
	}
?>