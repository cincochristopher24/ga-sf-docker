<?php
	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/controller/Class.IController.php");
	require_once("travellog/factory/Class.FileFactory.php");
	require_once("Class.StringFormattingHelper.php");
	require_once "Class.dbHandler.php";
	require_once("Class.HtmlHelpers.php");
	require_once('Class.Paging.php');
	
	abstract class AbstractCustomizedSubGroupCategoryController implements iController{
		protected 

		$obj_session  = NULL,

		$file_factory = NULL,
		
		$loggedUser  = NULL,

		$isLogged	  = false,

		$isMemberLogged = false,

		$isAdminLogged = false,

		$group 			= NULL,
		
		$subNav		=	NULL,
		
		$sGroupList = array(),
		
		$sGroupsCnt_Active = 0,   // count for active subgroups 
		
		$sGroupsCnt_InActive = 0, // count for inactive subgroups
		
		$sGroupsCnt = 0,          // count for all subgroups that are not is custom groups section
		
		$curPage = 1,
		
		$mNumRowsRetrieve = 6,
				
		$tpl		=	NULL;
		
		function __construct(){
			$this->obj_session = SessionManager::getInstance();
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template',array('path'=>''));
		
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('Condition');
			$this->file_factory->registerClass('FilterCriteria2');
			$this->file_factory->registerClass('FeaturedSubGroup');
			
			$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerClass('BackLink' , array('path' => 'travellog/model/navigator'));			
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('', array('path' => 'travellog/views'));;
			
			$this->file_factory->registerTemplate('LayoutMain');
			
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			$this->tpl = $this->file_factory->getClass('Template');
			$this->tpl->set_path("travellog/views/");
			
		}
		
		/**
	     * Fetches or retrieves the values of $_GET, $_POST, $_REQUEST, $_SESSION and $_COOKIE
	     * 
	     * @return array Array of values from gpc
	     */
	    function fetchGPCValues(){
			$params = array();
			$params['mode'] = ( isset($_GET['mode']))? $_GET['mode'] : 'view';
			$params['mode'] = ( isset($_POST['mode']))? $_POST['mode'] : $params['mode'];
			$params['gID']  = ( isset($_POST['gID']))? $_POST['gID'] : 0 ;
			$params['gID']  = ( isset($_GET['gID']))? $_GET['gID'] : $params['gID'];
	
			$params['searchKey']  = ( isset($_REQUEST['txtGrpName']))? $_REQUEST['txtGrpName'] : '';
			
			$params['action']  = ( isset($_GET['action']))? $_GET['action'] : 'ViewList';
			$params['action']  = ( isset($_POST['action']))? $_POST['action'] : $params['action'];
			
			$params['tabAction'] = (isset($_REQUEST['tabAction'])) ? $_REQUEST['tabAction'] : 'showActive';
			$params['tabAction'] = ("showActive" != $params['tabAction'] AND "showInActive" != $params['tabAction']) ? "showActive" : $params['tabAction'];
			
			$params['sgID']  = ( isset($_GET['sgID']))? $_GET['sgID'] : 0 ;
			$params['radSort']  = ( isset($_REQUEST['radSort']))? $_REQUEST['radSort'] : 0 ;
			
			$params['curPage'] = (isset($_POST['page'])) ? $_POST['page'] : 1;
      		$params['curPage'] = (isset($_GET['page']))  ? $_GET['page']  : $params['curPage'];
			
			$params['message'] = ($this->obj_session->get('custompopup_message')) ? $this->obj_session->get('custompopup_message') : NULL;
			$this->obj_session->unsetVar('custompopup_message');
			
			return $params;  	
	    }
	    
	    /**
		 * Serves as the entry point for this class
		 * 
		 * @return void
		 */
		function performAction(){
		    $params = $this->fetchGPCValues();
				
			
			switch ($params['mode']) {
				case 'view':
					//$this->_viewSubGroups($params);	
					break;
			}
			//$this->initializeActionCommonAttributes($params);
			
			switch ($params['action']) {
				case 'add':
					$this->_addCategory($params);
					break;
				case 'edit':
					$this->_editCategory($params);
					break;
				default:
					$this->_displayAddForm($params);
			}
			
			//$this->initializeViewCommonAttributes($params);
			
			
		}
		
		function _allowAdminPrivileges(){
			if (!$this->isAdminLogged){
				header("location:subgroups.php?gID=".$this->group->getGroupID());
				exit;
			}
		}
		
		/**
		 * Add a group category and assigns subgroups to it
		 * 
		 * @return object CustomizedSubGroupCategory
		 */
		function _addCategory($_params){
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain'))); 
			$this->_allowAdminPrivileges($this->isLogged); // if not logged in, redirect
		}
		
		function _displayAddForm($_params){
			require_once("travellog/model/Class.AdminGroup.php");
			require_once("travellog/model/Class.CustomizedSubGroupCategory.php");
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			//$this->tpl = $this->fetchSubGroupMainTemplate($_params);
			$adminGroup = new AdminGroup(245);
			$vars = array(
						'category' => new CustomizedSubGroupCategory(1),
						'category_groups' => $adminGroup->getSubGroupsWithCategory(new CustomizedSubGroupCategory(1)),
						'non_category_groups' => $adminGroup->getSubGroupsWithCategory(),
					);
			$this->tpl->setVars($vars);
			$this->tpl->out('tpl.FrmCustomizedSubGroupCategory.php');
		}
		
	}
?>
