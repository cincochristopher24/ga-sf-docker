<?php

	require_once 'travellog/factory/Class.FileFactory.php';
	require_once 'travellog/controller/Class.IController.php';
	require_once 'Class.Template.php';
	
	require_once("travellog/model/Class.CustomPageHeader.php");
	require_once("travellog/model/Class.CustomPageHeaderPhoto.php");
	require_once("travellog/model/Class.PathManager.php");
	require_once("travellog/model/Class.TravelLog.php");
	require_once("travellog/model/Class.Photo.php");
	require_once("travellog/model/Class.Config.php");
	require_once("travellog/UIComponent/NewCollection/factory/Class.PhotoCollectionUploadedPhotoFactory.php");

	abstract class AbstractCustomPageHeaderController implements iController{
		
		protected $mSession		=	NULL;
		protected $file_factory	=	NULL;
		protected $mTemplate	=	NULL;
		protected $mSubNavigation	=	NULL;
		protected $mContextInfo	=	NULL;
		
		protected $mLoggedID	=	0;
		protected $mIsPrivileged	=	FALSE;
		
		protected $mTraveler	=	NULL;
		protected $mGroup 		= 	NULL;
		
		protected $mCustomPageHeader	=	NULL;
		
		protected $mParams		=	array( 	"gID"				=>	0,
											"action"			=>	"VIEW",
											"step"				=>	0,
											"home_welcome_text"	=>	"",
											"home_welcome_title"=>	"",
											"photo_options"		=>	"UPLOAD",
											"signUp_options"	=>	0,
											"ID"				=>	0,
											"page"				=>	1,
											"mode"				=>	'ALL' //photoID
										);
		
		function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template', array('path' => ''));
			$this->file_factory->registerClass('SessionManager');
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('Config');
			$this->file_factory->registerClass('ProfileCompFactory',array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerTemplate('LayoutMain');
			
			$this->file_factory->registerClass('PhotoCollectionUploadedPhotoFactory',array('path'=>'travellog/UIComponent/NewCollection/factory/'));

			$this->file_factory->registerClass('ViewCustomPageHeader',array('path' => 'travellog/views/custom_pageheader/'));
			$this->file_factory->registerClass('ViewUploadedPhotos',array('path' => 'travellog/views/custom_pageheader/'));

			$this->file_factory->setPath('CSS', '/min/f=css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			$this->mSession = $this->file_factory->invokeStaticClass('SessionManager', 'getInstance');
			$this->mLoggedID = $this->mSession->get('travelerID');
		}
		
		function performAction(){
			$this->mParams = array_merge($this->mParams,$this->extractParameters());
			/*$this->_checkIfCobrand();
			$this->_checkPrivileges();
			$this->_defineCommonAttributes();
			$this->_prepareDependencies();*/
			//var_dump($this->mParams);exit;
			switch( $this->mParams["action"] ){
				case "VIEW"					:	$this->_render();
												break;
				case "SAVETEXT"				:	$this->_saveText();
												break;
				case "DIRECTSAVE"			:	$this->_directSave();
												break;
				case "PREVIEWSAVE"			:	$this->_previewSave();
												break;
				case "PROCEED_UPLOAD_STEP3"	:	$this->_proceedUploadStep3();
												break;
				case "PROCEED_FILTERUSED_STEP3" : 	$this->_proceedFilterUsedStep3();
												break;
				case "UPLOAD"				:	$this->_upload();
												break;
				case "FETCH_UPLOADED"		:	$this->_fetchUploaded();
												break;
				case "DELETE_UPLOADED"		:	$this->_deleteUploaded();
												break;
				case "CROP_PHOTO"			:	$this->_cropPhoto();
												break;
				case "FETCH_RANDOM"			:	$this->_fetchRandom();
												break;
				case "FETCH_RANDOM_FILTERED":	$this->_fetchRandomFiltered();
												break;
				case "SAVE_CUSTOMIZATION"	:	$this->_saveUploaded();
												break;
				case "DIRECTSAVE_HANDPICKED":	$this->_directSaveHandpicked();
												break;
				case "CANCEL_CUSTOMIZATION"	:	$this->_cancelCustomization();
												break;
				case "VIEW_PHOTOS_TO_FETCH"	:	$this->_viewPhotosToFetch();
												break;
				case "SAVE_FETCHED"			:	$this->_saveFetched();
												break;
				case "VIEWHANDPICKED"		:	$this->_viewHandpicked();
												break;
				case "SAVE_FILTERED_CUSTOMIZATION"	: 	$this->_saveFiltered();
														break;
				default						:	$this->_render();
												break;
			}
			
		}
		
		function _checkIfCobrand($redirect=FALSE){
			if( isset($GLOBALS['CONFIG']) ){
				return TRUE;
			}
			if( $redirect){
				header("Location: /index.php");
				exit;
			}else{
				return FALSE;
			}
		}
		
		function _checkPrivileges($redirect=FALSE){
			// check if user is currently logged in
			$this->mTraveler = $this->file_factory->getClass('Traveler', array($this->mLoggedID));
			
			/*if( $this->mTraveler->isAdvisor() ){
				$group = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array($this->mTraveler->getAdvisorGroup()));
				$this->mGroup = $group[0];
				$this->mIsPrivileged = TRUE;
			}else{
				try{
					$config = new Config($_SERVER["SERVER_NAME"]);
					if( $config instanceof Config ){
						$group = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array($config->getGroupID()));
						$this->mGroup = $group[0];
						if( $this->mGroup->isSuperStaff($this->mLoggedID) ){
							$this->mIsPrivileged = TRUE;
						}
					}
				}catch(exception $e){
				}
			}*/
			
			if( isset($GLOBALS["CONFIG"]) ){
				$group = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array($GLOBALS["CONFIG"]->getGroupID()));
				$this->mGroup = $group[0];
				if( $this->mGroup->getAdministratorID() == $this->mLoggedID || $this->mGroup->isSuperStaff($this->mLoggedID) ){
					$this->mIsPrivileged = TRUE;
					$this->mSession->set("gID",$this->mGroup->getGroupID());
				}
			}
			
			if ( $this->mIsPrivileged ) {
				try{
					$this->mCustomPageHeader = new CustomPageHeader($this->mGroup->getGroupID());
					$this->mCustomPageHeader->setGroup($this->mGroup);
					
					if( $this->mCustomPageHeader->isCustomizing() && $this->mParams["step"] > 0 
						&& $this->mParams["step"] > $this->mCustomPageHeader->getLastStep()+1 && $redirect ){
						header("Location: /custom_pageheader.php");
					}else if( 0 == $this->mParams["step"] ){
						$this->mParams["step"] = 1;
					}
				}catch(exception $e){
				}
				
				// start GA Logger
				require_once 'gaLogs/Class.GaLogger.php';
				try{
					GaLogger::create()->start($this->mLoggedID, 'TRAVELER');
				}
				catch(exception $e){
				}
			}elseif($redirect){
				header("Location: /index.php");
				exit;
			}
		}
		
		function _defineCommonAttributes(){
			$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			
			$this->mTemplate = $this->file_factory->getClass('Template');
			$this->mTemplate->set_path('travellog/views/custom_pageheader/');
			
			Template::setMainTemplateVar('page_location','Home');
			
			$this->mSubNavigation = $this->file_factory->getClass('SubNavigation');
			$this->mSubNavigation->setContext('GROUP');
			$this->mSubNavigation->setContextID($this->mGroup->getGroupID());
			$this->mSubNavigation->setLinkToHighlight('HOME');
			$this->mSubNavigation->setGroupType('ADMIN_GROUP');
			
			$mProfileFactory = $this->file_factory->invokeStaticClass('ProfileCompFactory', 'getInstance', array());
			$mProfileComp	= $mProfileFactory->create( ProfileCompFactory::$GROUP_CONTEXT );
			$mProfileComp->init($this->mGroup->getGroupID());

			$this->mContextInfo = $mProfileComp->get_view();
		}
		
		function _prepareDependencies(){
			//Template::includeDependentCss("/min/f=css/cobrand_customhome.css");
			if( 3 > $this->mParams["step"] ){
				Template::includeDependentCss("/min/f=css/Thickbox.css");
				Template::includeDependentJs("/js/thickbox.js");
			}
			if( 1 == $this->mParams["step"] ){
				Template::includeDependentJs("/js/contentSlider.js");
				Template::includeDependentJs("/js/collectionPopup.js");
			}
			if( 3 > $this->mParams["step"] ){
				Template::includeDependentCss("/css/Thickbox.css");
				Template::includeDependentJs("/js/thickbox.js");
			}
			if( 1 == $this->mParams["step"] ){
				Template::includeDependentJs("/js/contentSlider.js");
				Template::includeDependentJs("/js/collectionPopup.js");
			}
			if( 2 == $this->mParams["step"] ){
				Template::includeDependentJs("/min/f=js/prototype.js");
				Template::includeDependentJs("/js/ajaxfileupload.js");
				Template::includeDependentCss('/js/cropper/cropper.css');
				Template::includeDependentJs("js/cropper/lib/builder.js");
				Template::includeDependentJs("js/cropper/lib/dragdrop.js");
				Template::includeDependentJs("js/cropper/cropper.js");
			}
			Template::includeDependentJs("/js/custom_pageheader-1.2.js");
		}
		
		function extractParameters(){
			$params = $_REQUEST;
			$paramArray = array();

			if( isset($params) ){
				foreach( $params as $key => $value){
					$paramArray[$key] = $value;
				}
			}
			return $paramArray;
		}
		
		function _render(){
			$this->_checkIfCobrand(TRUE);
			$this->_checkPrivileges(TRUE);
			$this->_defineCommonAttributes();
			$this->_prepareDependencies();
			
			$mContent = $this->file_factory->getClass('ViewCustomPageHeader');
			$mContent->setParams($this->mParams);
			$mContent->setCustomPageHeader($this->mCustomPageHeader);
			
			$this->mTemplate->set("content",		$mContent);
			$this->mTemplate->set("subNavigation",	$this->mSubNavigation);
			$this->mTemplate->set("contextInfo",	$this->mContextInfo);
			$this->mTemplate->out("tpl.ViewCustomPageHeader.php");
		}
		
		function _saveText(){
			
			$this->_checkIfCobrand(TRUE);
			$this->_checkPrivileges(TRUE);
			
			if( !($this->mCustomPageHeader instanceof CustomPageHeader) ){
				$this->mCustomPageHeader = new CustomPageHeader();
				$this->mCustomPageHeader->setGroup($this->mGroup);
			}
			
			$this->mCustomPageHeader->setGroupID($this->mGroup->getGroupID());
			$this->mCustomPageHeader->setWelcomeTitleTemp(trim($this->mParams["home_welcome_title"]));
			$this->mCustomPageHeader->setWelcomeTextTemp(trim($this->mParams["home_welcome_text"]));
			
			if( "FILTER_USED" == $this->mParams["photo_options"] ){
				$this->mCustomPageHeader->setPhotoOptionTemp( $this->mCustomPageHeader->getPhotoOption() );
			}else{
				$this->mCustomPageHeader->setPhotoOptionTemp( "UPLOAD"==$this->mParams["photo_options"] ? 0 : 1 );
			}
			
			$this->mCustomPageHeader->setDirectSignupTemp($this->mParams["signUp_options"]);
			$this->mCustomPageHeader->setLastStep($this->mParams["step"]);
			
			$this->mCustomPageHeader->Save(FALSE);//finalStep=FALSE
			
			$params = array("customizing"	=>	FALSE,
							"deleted"		=>	FALSE,
							"initial"		=>	TRUE	);
			$customPhotos = $this->mCustomPageHeader->getCustomPageHeaderPhotos($params);
			foreach($customPhotos as $customPhoto){
				$customPhoto->UpdateForCustomization();
			}
			
			$step = $this->mParams["step"]+1;
			$filterused = "SELECT_UPLOADED"==$this->mParams["photo_options"] ? "&filterused" : "";
			header("Location: /custom_pageheader.php?action=VIEW&step=$step".$filterused);
			exit;
		}
		
		function _directSave(){
			$this->_checkIfCobrand(TRUE);
			$this->_checkPrivileges(TRUE);
			
			if( !($this->mCustomPageHeader instanceof CustomPageHeader) ){
				header("Location: /index.php");
				exit;
			}
			
			$this->mCustomPageHeader->setWelcomeTitleTemp(trim($this->mParams["home_welcome_title"]));
			$this->mCustomPageHeader->setWelcomeTextTemp(trim($this->mParams["home_welcome_text"]));
			$this->mCustomPageHeader->setPhotoOptionTemp( "UPLOAD"==$this->mParams["photo_options"]? 0 : 1 );
			$this->mCustomPageHeader->setDirectSignupTemp($this->mParams["signUp_options"]);
			
			$this->mCustomPageHeader->DeleteUnusedPhotos();
			
			$params = array(	"customizing"	=>	TRUE,
								"deleted"		=>	FALSE,
								"photoOption"	=>	"UPLOAD"==$this->mParams["photo_options"]? 0 : 1 );
			$customPhotos = $this->mCustomPageHeader->getCustomPageHeaderPhotos($params);//customizing=TRUE
			
			foreach($customPhotos as $customPhoto){
				$customPhoto->Save(TRUE);
			}
			
			$this->_saveCustomization();
		}
		
		function _upload(){
			if( !$this->_checkIfCobrand(FALSE) ){
				exit;
			}
			$this->_checkPrivileges(FALSE);
			if( !$this->mIsPrivileged ){
				exit;
			}
			
			$id = $this->mParams["ID"];
			
			$pathManager = new PathManager($this->mGroup,'image');
			$rel = $pathManager->GetPathrelative();
			if( !is_dir($rel) ){
				$pathManager->CreatePath();
			}
			
			$customPhoto = new CustomPageHeaderPhoto();
			$customPhoto->setGroupID($this->mGroup->getGroupID());
			$customPhoto->setPhotoOption($this->mCustomPageHeader->getPhotoOptionTemp());
			$customPhoto->setGroup($this->mGroup);
			$photo = FALSE;
			foreach($_FILES AS $file){
				$photo = $customPhoto->Upload($file);
				if( $photo ){
					$_SESSION["CustomPhotoUploaded_".$id] = TRUE;
					$_SESSION["CustomPhotoUploadedID_".$id] = $photo->getPhotoID();
					$_SESSION["CustomPhotoUploadedLink_".$id] = $photo->getPhotoLink("customheaderstandard");
				}else{
					$_SESSION["CustomPhotoUploaded_".$id] = FALSE;
				}
				break;
			}
		}
		
		function _fetchUploaded(){
			if( !$this->_checkIfCobrand(FALSE) ){
				exit;
			}
			$this->_checkPrivileges(FALSE);
			if( !$this->mIsPrivileged ){
				exit;
			}
			
			$id = $this->mParams["ID"];
			if( $_SESSION["CustomPhotoUploaded_".$id] ){
				echo "QUALIFIED?".$_SESSION["CustomPhotoUploadedID_".$id]."?".$_SESSION["CustomPhotoUploadedLink_".$id];
				unset($_SESSION["CustomPhotoUploaded_".$id]);
				unset($_SESSION["CustomPhotoUploadedID_".$id]);
				unset($_SESSION["CustomPhotoUploadedLink_".$id]);
			}else{
				echo "NOT_QUALIFIED";
				unset($_SESSION["CustomPhotoUploaded_".$id]);
			}
		}
		
		function _deleteUploaded(){
			if( !$this->_checkIfCobrand(FALSE) ){
				exit;
			}
			$this->_checkPrivileges(FALSE);
			if( !$this->mIsPrivileged ){
				exit;
			}
			
			$id = $this->mParams["ID"];
			try{
				$customPhoto = new CustomPageHeaderPhoto($id);
				if( $customPhoto instanceof CustomPageHeaderPhoto && $customPhoto->getGroupID() == $this->mGroup->getGroupID() ){
					$customPhoto->setGroup($this->mGroup);
					$customPhoto->Delete(TRUE);//customizing=TRUE
				}
			}catch(exception $e){}
		}
		
		function _cropPhoto(){
			if( !$this->_checkIfCobrand(FALSE) ){
				echo "RESTRICTED";
				exit;
			}
			$this->_checkPrivileges(FALSE);
			if( !$this->mIsPrivileged ){
				echo "RESTRICTED";
				exit;
			}
			
			$id = $this->mParams["ID"];
			try{
				$customPhoto = new CustomPageHeaderPhoto($id);
				if( $customPhoto instanceof CustomPageHeaderPhoto && $customPhoto->getGroupID() == $this->mGroup->getGroupID() ){
					$customPhoto->setGroup($this->mGroup);
					$photo = $customPhoto->getPhotoInstance();
					
					$pathManager = new PathManager($this->mGroup,'image');
					
					$upPhoto = $this->file_factory->invokeStaticClass('PhotoCollectionUploadedPhotoFactory', 'getInstance')->create('custompageheader');
					$upPhoto->cropCustomPageHeader($pathManager,$photo,$this->mParams["x"],($this->mParams["y"]*2));
					
					echo "CROPPED?".$photo->getPhotoLink("customheaderstandard");
				}
			}catch(exception $e){
				echo "RESTRICTED";
				exit;
			}
		}
		
		function _previewSave(){
			$this->_checkIfCobrand(TRUE);
			$this->_checkPrivileges(TRUE);
			
			if( !($this->mCustomPageHeader instanceof CustomPageHeader) ){
				$this->mCustomPageHeader = new CustomPageHeader();
				$this->mCustomPageHeader->setGroup($this->mGroup);
			}
			
			//$this->mCustomPageHeader->UndoChanges();
			
			$this->mCustomPageHeader->setGroupID($this->mGroup->getGroupID());
			$this->mCustomPageHeader->setWelcomeTitleTemp(trim($this->mParams["home_welcome_title"]));
			$this->mCustomPageHeader->setWelcomeTextTemp(trim($this->mParams["home_welcome_text"]));
			$this->mCustomPageHeader->setPhotoOptionTemp( "UPLOAD"==$this->mParams["photo_options"]? 0 : 1 );
			$this->mCustomPageHeader->setDirectSignupTemp($this->mParams["signUp_options"]);
			$this->mCustomPageHeader->setLastStep($this->mParams["step"]);
			
			//$this->mCustomPageHeader->setIsActive(0);//always set to inactive when editing
			
			$this->mCustomPageHeader->Save(FALSE);//finalStep=FALSE
			
			$params = array("customizing"	=>	FALSE,
							"deleted"		=>	FALSE	);
			$customPhotos = $this->mCustomPageHeader->getCustomPageHeaderPhotos($params);
			foreach($customPhotos as $customPhoto){
				$customPhoto->UpdateForCustomization();
			}
			
			$this->mCustomPageHeader->setLastStep(2);
			$this->mCustomPageHeader->UpdateLastStep();
			header("Location: /custom_pageheader.php?action=VIEW&step=3");
		}
		
		function _proceedUploadStep3(){
			
			$this->_checkIfCobrand(TRUE);
			$this->_checkPrivileges(TRUE);
			
			$params = array("customizing"	=>	TRUE,
							"deleted"		=>	FALSE);
			$customPhotos = $this->mCustomPageHeader->getCustomPageHeaderPhotos($params);//customizing
			foreach($customPhotos as $customPhoto){
				if( isset($this->mParams["home_photo_description_".$customPhoto->getPhotoID()]) ){
					if( $customPhoto->getCaptionTemp() != $this->mParams["home_photo_description_".$customPhoto->getPhotoID()] ){
						$customPhoto->setCaptionTemp(trim($this->mParams["home_photo_description_".$customPhoto->getPhotoID()]));
						$customPhoto->updateCaptionTemp();
					}
				}
			}
			$this->mCustomPageHeader->setLastStep(2);
			$this->mCustomPageHeader->UpdateLastStep();
			header("Location: /custom_pageheader.php?action=VIEW&step=3");
		}
		
		function _proceedFilterUsedStep3(){
			$this->_checkIfCobrand(TRUE);
			$this->_checkPrivileges(TRUE);
			
			$params = array("deleted"			=>	FALSE,
							"ignorePhotoOption" =>	TRUE,
							"isSaved"			=>	TRUE);
			$customPhotos = $this->mCustomPageHeader->getCustomPageHeaderPhotos($params);
			if( !count($customPhotos) ){
				header("Location: /custom_pageheader.php?action=VIEW&step=1");
				exit;
			}
			
			$temp = array();
			foreach( explode(",",$this->mParams["selection"]) as $idstr ){
				if( "" != trim($idstr) ){
					$temp[] = $idstr;
				}
			}
			
			foreach($customPhotos as $customPhoto){
				if( in_array($customPhoto->getPhotoID(),$temp) ){
					$customPhoto->Filter($include=TRUE);
					if( isset($this->mParams["hdnCaption_".$customPhoto->getPhotoID()]) ){
						$photoInstance = $customPhoto->getPhotoInstance();
						if( $customPhoto->getCaptionTemp() != $this->mParams["hdnCaption_".$customPhoto->getPhotoID()] 
							|| $photoInstance->getCaption() != $customPhoto->getCaptionTemp() ){
							$customPhoto->setCaptionTemp(trim($this->mParams["hdnCaption_".$customPhoto->getPhotoID()]));
							$customPhoto->updateCaptionTemp();
						}
					}
				}else{
					$customPhoto->Filter($include=FALSE);
				}
			}
			
			$this->mCustomPageHeader->setLastStep(2);
			$this->mCustomPageHeader->UpdateLastStep();
			header("Location: /custom_pageheader.php?action=VIEW&step=3&filterused");
		}
		
		function _fetchRandom(){
			
			if( !$this->_checkIfCobrand(FALSE) ){
				echo "FAILED";
				exit;
			}
			$this->_checkPrivileges(FALSE);
			if( !$this->mIsPrivileged ){
				echo "FAILED";
				exit;
			}
			
			$customPhoto = $this->mCustomPageHeader->getRandomPhoto(TRUE);//customizing=TRUE
			if( $customPhoto ){
				echo $customPhoto->getPhotoInstance()->getPhotoLink("customheaderstandard")."?".htmlspecialchars(stripslashes(strip_tags($customPhoto->getCaptionTemp())),ENT_QUOTES);
			}else{
				echo "FAILED";
			}
		}
		
		function _fetchRandomFiltered(){
			
			if( !$this->_checkIfCobrand(FALSE) ){
				echo "FAILED";
				exit;
			}
			$this->_checkPrivileges(FALSE);
			if( !$this->mIsPrivileged ){
				echo "FAILED";
				exit;
			}
			
			$customPhoto = $this->mCustomPageHeader->getRandomPhoto($customizing=NULL,$filtered=TRUE);
			if( $customPhoto ){
				echo $customPhoto->getPhotoInstance()->getPhotoLink("customheaderstandard")."?".htmlspecialchars(stripslashes(strip_tags($customPhoto->getCaptionTemp())),ENT_QUOTES);
			}else{
				echo "FAILED";
			}
		}
		
		function _saveUploaded(){
			$this->_checkIfCobrand(TRUE);
			$this->_checkPrivileges(TRUE);
			
			$this->mCustomPageHeader->DeleteUnusedPhotos();
			$this->mCustomPageHeader->InactivatePhotos();
			
			$params = array(	"customizing"	=>	TRUE,
								"deleted"		=>	FALSE);
			$customPhotos = $this->mCustomPageHeader->getCustomPageHeaderPhotos($params);//customizing=TRUE
			
			foreach($customPhotos as $customPhoto){
				$customPhoto->Save(TRUE);
			}
			
			$this->_saveCustomization();
		}
		
		function _saveFiltered(){
			$this->_checkIfCobrand(TRUE);
			$this->_checkPrivileges(TRUE);
			
			$this->mCustomPageHeader->DeleteUnusedPhotos();
			$this->mCustomPageHeader->InactivatePhotos();
			
			$params = array("deleted"	=>	FALSE,
							"isSaved"	=>	TRUE,
							"isFiltered"=>	TRUE,
							"ignorePhotoOption"	=>	TRUE);
			$customPhotos = $this->mCustomPageHeader->getCustomPageHeaderPhotos($params);
			
			foreach($customPhotos as $customPhoto){
				$customPhoto->Save(TRUE);
			}
			
			$this->_saveCustomization();
		}
		
		function _directSaveHandpicked(){
			$this->_checkIfCobrand(TRUE);
			$this->_checkPrivileges(TRUE);
			
			$this->mCustomPageHeader->DeleteUnusedPhotos();
			$this->mCustomPageHeader->InactivatePhotos();
			
			$params = array(	"customizing"	=>	TRUE,
								"deleted"		=>	FALSE);
			$customPhotos = $this->mCustomPageHeader->getCustomPageHeaderPhotos($params);
			foreach($customPhotos as $customPhoto){
				if( isset($this->mParams["home_photo_description_".$customPhoto->getPhotoID()]) ){
					if( $customPhoto->getCaptionTemp() != $this->mParams["home_photo_description_".$customPhoto->getPhotoID()] ){
						$customPhoto->setCaptionTemp(trim($this->mParams["home_photo_description_".$customPhoto->getPhotoID()]));
					}
				}
				$customPhoto->Save(TRUE);
			}
			$this->_saveCustomization();
		}
		
		function _cancelCustomization(){
			$this->_checkIfCobrand(TRUE);
			$this->_checkPrivileges(TRUE);
			
			if( isset($_SESSION["random_custom_photo"]) ){
				unset($_SESSION["random_custom_photo"]);
				unset($_SESSION["last_index"]);
			}
			
			$this->mCustomPageHeader->UndoChanges();
			header("Location: /index.php");
		}
		
		function _saveCustomization(){
			
			$this->mCustomPageHeader->Save(TRUE);//finalStep=TRUE
			
			if( isset($_SESSION["COBRAND_HEADER_CUSTOMIZED"]) ){
				unset($_SESSION["COBRAND_HEADER_CUSTOMIZED"]);
			}
			$_SESSION["COBRAND_HEADER_CUSTOMIZED"] = TRUE;
			
			$this->mCustomPageHeader->UndoChanges();
			
			header("Location: /index.php");
		}
		
		function _viewPhotosToFetch(){
			
			$this->_checkIfCobrand(TRUE);
			$this->_checkPrivileges(TRUE);
			
			$_SESSION["FETCH_JOURNAL_SELECTION"] = $this->mParams["selection"];
			$this->mCustomPageHeader->setLastStep(2);
			$this->mCustomPageHeader->UpdateLastStep();
			header("Location: /custom_pageheader.php?action=VIEW&step=3"); 
		}
		
		function _saveFetched(){
			
			$this->_checkIfCobrand(TRUE);
			$this->_checkPrivileges(TRUE);
			
			if( isset($_SESSION["FETCH_JOURNAL_SELECTION"]) ){
				unset($_SESSION["FETCH_JOURNAL_SELECTION"]);
			}
			$temp = array();
			foreach( explode(",",$this->mParams["selection"]) as $idstr ){
				if( "" != trim($idstr) ){
					$temp[] = $idstr;
				}
			}
			$this->mParams["selection"] = $temp;
			
			$this->mCustomPageHeader->DeleteOldFetchedPhotos();//final=FALSE
			$this->mCustomPageHeader->InactivatePhotos();
			
			$photoIDs = $this->mParams["selection"];
			foreach( $photoIDs AS $photoID ){
				$travellog = TravelLog::getTravelLogContext($photoID);
				if( $travellog ){
					$photo = new Photo($travellog,$photoID);
					if( $photo instanceof Photo ){
						$this->mCustomPageHeader->SaveFetchedPhoto($photo);
					}
				}
			}
			
			$this->_saveCustomization();
		}
		
		function _viewHandpicked(){
			$privileged = TRUE;
			$privileged = $this->_checkIfCobrand(FALSE);
			$this->_checkPrivileges(FALSE);
			if( !$this->mIsPrivileged ){
				$privileged = FALSE;
			}
			
			$mContent = $this->file_factory->getClass('ViewUploadedPhotos');
			$mContent->setCustomPageHeader($this->mCustomPageHeader);
			$mContent->setIsPrivileged($privileged);
			$mContent->render();
		}
		
	}