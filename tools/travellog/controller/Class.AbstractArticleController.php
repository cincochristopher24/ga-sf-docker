<?php 
require_once("Class.ToolMan.php");
require_once("travellog/controller/Class.IController.php");
require_once("travellog/factory/Class.FileFactory.php");

abstract class AbstractArticleController implements IController{
	
	protected static $REQUIRES_ACTION_RULES = array('ADD', 'SAVE', 'EDIT', 'DELETE', 'VIEW');
	
	protected 
	
	$data         = array(),
	
	$obj_session  = NULL,
	
	$profile_view = NULL,
	
	$obj_navigation = NULL,
	
	$file_factory = NULL;
	
	function __construct(){
		$this->obj_session  = SessionManager::getInstance();
		$this->file_factory = FileFactory::getInstance(); 
		
		$this->file_factory->registerClass('MainLayoutView'      , array('path' => 'travellog/views/'));
		$this->file_factory->registerClass('LocationFactory');
		$this->file_factory->registerClass('CommentsView' , array('path' => 'travellog/views/'));
		$this->file_factory->registerClass('EmailNotification');
		$this->file_factory->registerClass('NewCity');
		$this->file_factory->registerClass('Coordinates');
		$this->file_factory->registerClass('MailImplController'  , array('path' => 'travellog/controller/'));
		$this->file_factory->registerClass('gaSubject'           , array('path' => 'travellog/controller/'));
		$this->file_factory->registerClass('gaAbstractEvent'     , array('path' => 'travellog/controller/'));
		$this->file_factory->registerClass('gaActionType'        , array('path' => 'travellog/controller/'));
		
		$this->file_factory->registerClass('Article');
		$this->file_factory->registerClass('TravelLog');
		$this->file_factory->registerClass('Tags');
		$this->file_factory->registerClass('Trip');
		$this->file_factory->registerClass('Traveler'); 
		$this->file_factory->registerClass('Map');
		$this->file_factory->registerClass('ArticleFcd', array('path' => 'travellog/facade/'));
		
		
		$this->file_factory->registerClass('ViewArticle', array('path' => 'travellog/views/'));
		
		
		$this->file_factory->registerClass('BackLink'               , array('path' => 'travellog/model/navigator/'));
		
		$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
		$this->file_factory->registerClass('AdminGroup');
		$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
		
		$this->file_factory->registerClass('SubNavigation');
		$this->file_factory->registerClass('Template'               , array('path' => ''));
		$this->file_factory->registerClass('GaDateTime'               , array('path' => ''));
		$this->file_factory->registerClass('StringFormattingHelper' , array('path' => ''));
		$this->file_factory->registerClass('HtmlHelpers'            , array('path' => ''));
		$this->file_factory->registerClass('Criteria2'               , array('path' => ''));
		$this->file_factory->registerClass('GroupFactory');
				
		
		$this->file_factory->registerTemplate('FrmArticle');
		
		$this->file_factory->registerTemplate('LayoutMain');
		$this->file_factory->registerTemplate('TravelLog');
		$this->file_factory->registerClass('HelpText');
		$this->file_factory->setPath('CSS', '/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');  
		
	}
	
	function performAction(){	
		try{	
			$this->data['action'] = (isset($this->data['context'])) ? "viewlist" : $this->data['action'];
			
			$this->beforePerformAction();
			switch( strtolower($this->data['action']) ){
				case "edit":
				case "add":
					$this->initProfileComp();
					$this->__getData();
					$this->_viewAddForm();
				break;
			
				case "delete":
					$this->initProfileComp();
					$this->_delete();
				break;
						
				case "save":
					$this->initProfileComp();
					$this->__validateForm();
					$this->_save();
				break;
			
				case "addcity":
					$this->initProfileComp();
					$this->_saveCity();
				break;
			
				case "viewlist":				
					$this->initProfileComp();
					$this->_viewArticlesList();
				break;
			
				case "view":
					$this->initProfileComp();
					$this->_viewArticle();
				break; 
					
				default: ToolMan::redirect('/journal.php'); exit;
			}
		}catch(Exception $e){
			ToolMan::redirect('/journal.php'); exit;
		}
	}
	
	protected function _saveCity(){
		$txtLat  	             = $this->data['lat'];
		$txtLong 	             = $this->data['long'];
		$locationID              = $this->data['locID'];
		$txtCity 	             = $this->data['cityName'];
		$this->data['countryID'] = $this->data['locID'];
		
		$location = $this->file_factory->invokeStaticClass('LocationFactory');// LocationFactory::instance(); 
		$Country  = $location-> create($locationID);

		$City = $this->file_factory->getClass('NewCity'); //new NewCity();
		$City-> setCountryID($Country->getCountryID());
		$City-> setTravelerID( $this->obj_session->get('travelerID') );
		$City-> setName($txtCity);
		$City-> Save();
		$this->data['cityID'] = $City->getLocationID(); 
		 
		$Coords = $this->file_factory->getClass('Coordinates', array($City->getLocationID())); //new Coordinates($City->getLocationID());
		$Coords-> setX($txtLong);
		$Coords-> setY($txtLat);
		$Coords-> save(); 

		$path = "http://" . $_SERVER['SERVER_NAME'] . "/admin/approveCity.php?countryLocID=" . $locationID;
		$EmailNotice = $this->file_factory->getClass('EmailNotification', array(3)); //new EmailNotification(3);
		$EmailNotice-> setTo('admin@goabroad.net');
		$EmailNotice-> setToName('GoAbroad.net Administrator');
		$EmailNotice-> setFrom('admin@goabroad.net');
		$EmailNotice-> setFromName('GoAbroad.net');
		$EmailNotice-> setSubject('New city added in GoAbroad.Net');
		$EmailNotice-> setMessage('<br /> Hi! <br /> <br /> A new city, <strong>' . $txtCity . '</strong> was added to the country <strong>' . 
				$Country-> getName() . '</strong> through the <strong>Add City Tool</strong>. Please confirm if this is a valid city by ' .
				'clicking on the link below: <br /> <br /> <a href = "'.$path.'"> Approve New City </a> <br /> <br /> <br /> <br /> Thanks, ' .
				'<br /> <strong>The GoAbroad Tech Team</strong>');
		$EmailNotice-> Send();
		
		 
		$this->_getCities();
	}
	
	protected function _getCities(){
		$html = '';
		try{
			$obj_factory     = $this->file_factory->invokeStaticClass('LocationFactory'); 
			$obj_location    = $obj_factory->create($this->data['countryID']);
			$col_cities      = $obj_location->getCities($this->obj_session->get('travelerID'));
			if( count($col_cities) ){
				foreach( $col_cities as $obj_city ){
					$selected = ( $obj_city->getLocationID() == $this->data['cityID'] )? 'selected=true' : '';
					$html    .= '<option value="'.$obj_city->getLocationID().'" '.$selected.'>'.$obj_city->getName().'</option>';
				}	
			}
		}catch(Exception $e){
			
		}
		$type = ($this->data['countryID'] == 7758)	?	"State"	:	"City";
		$selected = ( 0 == $this->data['cityID'] )? 'selected=true' : '';
		$html    .= '<option value="0" '.$selected.'>Choose a '.$type.'</option>'; 
		$html    .= '<option value="">-Other City-</option>';    
		echo $html . '|' . $this->data['cityID'];    
	}
	
	protected function _viewArticle(){
		$obj_map		= $this->file_factory->getClass('Map');
		$obj_article 	= $this->file_factory->getClass('Article', array($this->data['articleID'])); 
		$obj_view     	= $this->file_factory->getClass('MainLayoutView');
		
		//tags
		$article_tag = $this->file_factory->getClass('Tags');
		$article_tag->setContext(Tags::ARTICLE);
		$article_tag->getTagsByContextID($this->data['articleID']);
		
		// retrieve article sub views
		$fcd = $this->file_factory->getClass('ArticleFcd');
		$fcd->setArticleID($this->data['articleID']);
		$isAdmin = $fcd->enableControls();
		
		ob_start();
			$obj_map->includeJs();
			$google_map_include = ob_get_contents(); 
		ob_end_clean();
		
		$articleID = $obj_article->getArticleID();
		
		if( $isAdmin ){
		
		$code = <<<BOF
<script type="text/javascript">   
tinyMCE_GZ.init({
	plugins : 'style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,'+ 
        'searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras',
	themes : 'simple,advanced',
	languages : 'en',
	disk_cache : true,
	debug : false
});
</script>
		
<script type="text/javascript">   
//<![CDATA[
	jQuery.noConflict();
	articleID = $articleID;
	isAdmin     = $isAdmin;
	is_tinymce  = 0;
	
	
	jQuery(document).ready(function(){
		
		
		var popUIArticle = new PopUI(); 
		popUIArticle.set_url                ( '/ajaxpages/article_page.php' );	   
		popUIArticle.set_form_label_elements( ['contentLabel']                    );
		popUIArticle.set_form_elements      ( ['content','TEXTAREA']              );
		//popUIArticle.set_cancel_element     ( 'cancel'                            ); 
		
		
		jQuery('#ArticleTitleLink').click(function(){
			popUIArticle.set_form_label_values  ( ['Article Title']                       );
			popUIArticle.set_hidden_field_value ( 'ARTICLETITLE'                          );
			popUIArticle.set_uses_tinymce       ( false                                 );
			popUIArticle.set_querystring        ( 'aID='+articleID+'&type=ARTICLETITLE' );
			popUIArticle.set_height             ( ['28px']                              );
			popUIArticle.set_on_save_callback   ( 
				function(field, content){
					jQuery('#ENTRYTITLE').html(content);
				} 
			);
			
			popUIArticle.set_before_save_validation ( 
				function(field){ 
					if(!(jQuery.trim(field)).length){
						alert('Invalid title. It must not be empty!');
						return false;
					}
					return true;
				}
			);
			popUIArticle.set_character_count_callback( function(){jQuery('.charcount').characterCount(250); jQuery('.charcount').keyup();}, "character" );
			popUIArticle.render();
		});

		jQuery('#ArticleCalloutLink').click(function(){
			jQuery('#ENTRYDESCRIPTION').trigger('editcallout');
		});
		
		jQuery('#ArticleDescriptionLink').click(function(){ 
			jQuery('#ENTRYDESCRIPTION').trigger('editdesc');
		}); 		
		
		jQuery('#ENTRYDESCRIPTION').bind("editdesc", function(){
			popUIArticle.set_form_label_values  ( ['Article Description']                       );
			popUIArticle.set_hidden_field_value ( 'ARTICLEDESCRIPTION'                          );
			popUIArticle.set_uses_tinymce       ( true                                        );
			popUIArticle.set_tinymce_custom_cleanup( 'custom_callback' );
			popUIArticle.set_querystring        ( 'aID='+articleID+'&type=ARTICLEDESCRIPTION' );
			popUIArticle.set_height             ( ['300px']                                   );
			popUIArticle.set_on_save_callback   ( 
				function(field, content){
					jQuery('#ENTRYDESCRIPTION').html(content);
					jQuery('#ArticleDescriptionLink').bind('click', function(){ 
						jQuery('#ENTRYDESCRIPTION').trigger('editdesc');
					}); 
					jQuery('#ArticleCalloutLink').click(function(){
						jQuery('#ENTRYDESCRIPTION').trigger('editcallout');
					});
				} 
			);

			popUIArticle.set_before_save_validation ( 
				function(field){
					if(!(jQuery.trim(field.replace(/(<p>(\&nbsp;)+<\/p>)/g,"")).length)){
						alert('Invalid title. It must not be empty!');
						return false;
					}
					return true;
				}
			);
			popUIArticle.set_character_count_callback( null, null );

			popUIArticle.render();
		});
		
		jQuery('#ENTRYDESCRIPTION').bind("editcallout", function(){
			popUIArticle.set_form_label_values  ( ['Article Callout']                       );
			popUIArticle.set_hidden_field_value ( 'ARTICLECALLOUT'                          );
			popUIArticle.set_uses_tinymce       ( false                                   );
			popUIArticle.set_querystring        ( 'aID='+articleID+'&type=ARTICLECALLOUT' );
			popUIArticle.set_height             ( ['28px']                                );
			popUIArticle.set_on_save_callback   ( 
				function(field, content){
					html = 	'<img border="0" align="0" src="/images/calloutquotefirst.gif"/>'+
							content+
							'<img border="0" align="0" src="/images/calloutquotelast.gif"/>';
					jQuery('#ENTRYCALLOUT').html(html);
				} 
			);
			popUIArticle.set_character_count_callback( function(){jQuery('.charcount').characterCount(250); jQuery('.charcount').keyup();}, "character" );
			
			popUIArticle.render();
		});
		
		jQuery('#ArticleEditorsNoteLink').click(function(){
			popUIArticle.set_form_label_values  ( ['Editors Note']                       );
			popUIArticle.set_hidden_field_value ( 'EDITORSNOTE'                          );
			popUIArticle.set_uses_tinymce       ( false                                   );
			popUIArticle.set_querystring        ( 'aID='+articleID+'&type=EDITORSNOTE' );
			popUIArticle.set_height             ( ['28px']                                );
			popUIArticle.set_on_save_callback   ( 
				function(field, content){
					jQuery('#EDITORSNOTE').html(content);
				} 
			);
			popUIArticle.set_character_count_callback( null,null );
			
			popUIArticle.render();
		});
		
		jQuery('#ArticleTagsLink').click(function(){
			popUIArticle.set_form_label_values  ( ['Article Tags']                       );
			popUIArticle.set_hidden_field_value ( 'ARTICLETAGS'                          );
			popUIArticle.set_uses_tinymce       ( false                                   );
			popUIArticle.set_querystring        ( 'aID='+articleID+'&type=ARTICLETAGS' );
			popUIArticle.set_height             ( ['28px']                                );
			popUIArticle.set_on_save_callback   ( 
				function(field, content){
					var html = "";
					if(content != null){
						var tags = content.split(',');
						for(i=0;i<tags.length;i++){
							html += '<a href="/journal.php?action=All&amp;tag='+tags[i]+'">'+tags[i]+'</a>';
							if((i+1) < (tags.length)) html += ' , ';
						}
					}
					jQuery('#ARTICLESTAG').html(html);
				} 
			);
			popUIArticle.set_character_count_callback( null,null );

			popUIArticle.render();
		});
	});	
	
	function custom_callback(type, value){
		switch (type) {
			case "get_from_editor":
				value = value.replace(/(<p>([<br\/>]|[&nbsp;]|\s)*<\/p>)/gi,'');
				break;
			case "insert_to_editor":
				value = value.replace(/(<p>([<br\/>]|[&nbsp;]|\s)*<\/p>)/gi,'');
				break;
			case "submit_content":
				break;
			case "get_from_editor_dom":
				break;
			case "insert_to_editor_dom":
				break;
			case "setup_content_dom":
				break;
			case "submit_content_dom":
				break;
		}

		return value;
	}

	jQuery(document).ready(function(){  
		initmb();
	});
	
//]]>  
</script>
BOF;
		
		}
		
		$dom = new DOMDocument;
	    @$dom->loadHTML(trim($obj_article->getDescription())); 
	    $dom->preserveWhiteSpace = false; 
		$ps = $dom->getElementsByTagName('p');
		$metaDesc = $ps ? htmlentities($ps->item(0)->nodeValue, ENT_QUOTES) : strip_tags(substr($obj_article->getDescription(), 0, 140));
		
		//$pageLocation = ($obj_article->getGroupID() != 0) ? 'Groups / Clubs'  : 'Travelers';
		if( isset($GLOBALS["CONFIG"]) ){
			if( isset($_GET["gID"]) || (0 < $obj_article->getGroupID() && 0 < $this->obj_session->get('travelerID')) ){
				Template::setMainTemplateVar ( 'page_location', 'Home' );
			}elseif( 0 == $obj_article->getGroupID() && $obj_article->getAuthorID() == $this->obj_session->get('travelerID') ){
				Template::setMainTemplateVar ( 'page_location', 'My Passport' );
			}elseif( 0 < $this->obj_session->get('travelerID') ){
				Template::setMainTemplateVar ( 'page_location', 'Travelers' );
			}else{
				Template::setMainTemplateVar ( 'page_location', 'Journals' );
			}
		}else{
			if( isset($_GET["gID"]) || 0 < $obj_article->getGroupID() ){
				Template::setMainTemplateVar ( 'page_location', 'Groups / Clubs' );
			}elseif( 0 == $obj_article->getGroupID() && $obj_article->getAuthorID() == $this->obj_session->get('travelerID') ) {
				Template::setMainTemplateVar ( 'page_location', 'My Passport' );
			}else{
				Template::setMainTemplateVar ( 'page_location', 'Travelers' );
			}
		}
		//Template::setMainTemplateVar ( 'page_location', $pageLocation ); 
		Template::setMainTemplateVar ( 'title', strip_tags($obj_article->getTitle()) . " - GoAbroad Network");
		Template::setMainTemplateVar ( 'metaDescription', strip_tags($metaDesc));	
		Template::setMainTemplateVar ('layoutID', 'journals');
		Template::setMainTemplate    ( $this->file_factory->getTemplate('LayoutMain')                               );
		Template::includeDependent   ( $google_map_include, array('bottom'=> true));
		
		if( $this->obj_session->getLogin() ){			
			Template::includeDependentJs ( "/min/g=JournalEntryLoginJs"   , array('bottom'=> true));
			//Template::includeDependentJs ( "/js/addnewcity.js", array('bottom'=> true));
			//Template::includeDependentJs ( "/js/modaldbox.js"   , array('bottom'=> true));
		}
		
		if( $isAdmin ){
			Template::includeDependentJs ( "/min/g=ArticleAdminJs" , array('bottom'=> true));
			Template::includeDependentJs ( "/js/tiny_mce/tiny_mce_gzip.js" , array('bottom'=> true));
			//Template::includeDependentJs ( "/js/jquery.characterCount.js" , array('bottom'=> true));
			//Template::includeDependentJs ( "/js/popUI.js" , array('bottom'=> true));
			//Template::includeDependentJs("/js/jquery.checkbox.js", array('bottom'=> TRUE));
			//Template::includeDependentJs("/js/article-location.js", array('bottom'=> TRUE));
			Template::includeDependent($code,array('bottom'=> true) );
		}
		
		Template::includeDependentJs (array("/min/g=ArticleJs"),array('bottom'=> true) );
		//Template::includeDependentJs (array("/js/jcarousel-lite.js","/js/PhotoUILayout.js"),array('bottom'=> true) );
			
		$context 	= 'article';
		$loginid 	= 0;
		$genid 		= $obj_article->getArticleID();
		
		require_once("travellog/UIComponent/Photo/facade/PhotoUIFacade.php");
		$Photo = new PhotoUIFacade();
		$Photo->setContext($context);
		$Photo->setGenID($genid);
		$Photo->setLoginID($loginid);
		$Photo->build();
		
		$articlejs =<<<BOF
		<script type = "text/javascript">
			jQuery.noConflict();
		
			jQuery(document).ready(function(){
				
				
				jQuery("#expand_author").click(function(){
					jQuery("#more_author").slideToggle("slow");
					if(jQuery(this).hasClass('expand')){
						jQuery(this).addClass('collapse');
						jQuery(this).removeClass('expand');
						jQuery(this).html('less -');
					}else{
						jQuery(this).addClass('expand');
						jQuery(this).removeClass('collapse');
						jQuery(this).html('more +');
					}
				});
				jQuery("#expand_group").click(function(){
					jQuery("#more_group").slideToggle("slow");
					if(jQuery(this).hasClass('expand')){
						jQuery(this).addClass('collapse');
						jQuery(this).removeClass('expand');
						jQuery(this).html('less -');
					}else{
						jQuery(this).addClass('expand');
						jQuery(this).removeClass('collapse');
						jQuery(this).html('more +');
					}
				});
				jQuery("#expand_related").click(function(){
					jQuery("#more_related").slideToggle("slow");
					if(jQuery(this).hasClass('expand')){
						jQuery(this).addClass('collapse');
						jQuery(this).removeClass('expand');
						jQuery(this).html('less -');
					}else{
						jQuery(this).addClass('expand');
						jQuery(this).removeClass('collapse');
						jQuery(this).html('more +');
					}
				});
			});
		</script>
BOF;
		Template::includeDependent($articlejs,array('bottom'=> true) );
		
		Template::includeDependentCss( "/min/f=css/styles.css");
		
		Template::includeDependentCss("/css/vsgStandardized.css");
		Template::includeDependentCss("/css/modalBox.css");
		
		$arr_filter = array();
		$arr_filter['group_id'] 	= (isset($this->data['gID'])) ? $this->data['gID'] : null;
		$arr_filter['travelerID'] 	= $this->obj_session->get('travelerID');
		$arr_filter['articleID'] 	= $obj_article->getArticleID();
		$arr_filter['authorID'] 	= $obj_article->getAuthorID();
		$arr_filter['profile']  	= $this->profile_view->get_view();
		$arr_filter['subnav']		= $this->obj_navigation;
		$arr_filter['tags']			= $article_tag->getTagArray();
				
		$arr_contents['contents'] = $fcd->retrieveArticle( $arr_filter );
		$obj_view->setContents( $arr_contents );
		
		return $obj_view->render();
	}

	protected function _viewArticlesList(){
		require_once("Class.StringFormattingHelper.php");
		// get articles
		$obj_article  = $this->file_factory->getClass('Article'); 
		
		$this->data['context'] = (isset($this->data['context'])) ? $this->data['context'] : "";
		$this->data['value'] = (isset($this->data['value'])) ? $this->data['value'] : "";
		
		$articleslist = $obj_article->getArticles($this->data['context'],$this->data['value']);
		
		// setting template
		$obj_template = $this->file_factory->getClass('Template');
		$obj_template->set('articles', $articleslist);
		//$obj_template->set('profile', $this->profile_comp->get_view());
		//$obj_template->set('sub_navigation', $this->obj_navigation);
		
		Template::setMainTemplate($this->file_factory->getTemplate('LayoutMain'));
		Template::setMainTemplateVar ( 'page_location', 'Journals' );
		Template::setMainTemplateVar ( 'title', 'Articles by '.$this->data['context'] );
		Template::setMainTemplateVar ( 'layoutID', 'basic'         );
		
		$obj_template->out('travellog/views/tpl.ViewArticlesList.php');
	}
	
	protected function _viewAddForm(){
		$travelerID                     = $this->obj_session->get('travelerID');
		$obj_traveler                   = $this->file_factory->getClass('Traveler', array($travelerID));
		$this->data['is_advisor']       = $obj_traveler->isAdvisor();
		$this->data['is_administrator'] = $obj_traveler->isAdministrator();
		$this->data['isAdmin']          = $this->obj_session->isSiteAdmin();
		$this->data['gID']				= isset($this->data['gID']) ? "?gID=".$this->data['gID'] : "";
//		$this->data['obj_back_link'] 	= $this->file_factory->invokeStaticClass('BackLink','instance');
		$obj_template                   = $this->file_factory->getClass('Template');
		$obj_inc_template               = clone($obj_template);
		$col_countries                  = Country::getCountryList();
		$obj_map                        = $this->file_factory->getClass('Map'); 
		
		$obj_template->set('profView', $this->profile_view->get_view());
		
		$obj_inc_template->set( 'props'        , $this->data    );
		$obj_inc_template->set( 'col_countries', $col_countries );
		
		
		$obj_template->set_vars(
			array(
				'title'         => "Create Article",
				'map'			=> $obj_map,
				'form'          => $obj_inc_template->fetch($this->file_factory->getTemplate('FrmArticle')),
				'subNavigation' => $this->obj_navigation
			)
		);
		 
		$cityID = $this->data['cityID'];
		
		$code = <<<BOF
<script type="text/javascript">   
tinyMCE_GZ.init({
	plugins : 'style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,'+ 
        'searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras',
	themes : 'simple,advanced',
	languages : 'en',
	disk_cache : true,
	debug : false
});
</script>
		
<script type="text/javascript">   
//<![CDATA[
	tinyMCE.init({
		mode: "none",
	  	theme: "advanced",
	  	theme_advanced_toolbar_location: "top",
	  	theme_advanced_toolbar_align: "left",
	  	theme_advanced_buttons1: "bold,italic,underline",
	  	theme_advanced_buttons2: "", 
	  	theme_advanced_buttons3: "",
		valid_elements : "b,strong,i,em,u,p,-p,-br,style",
		inline_styles: false,
		cleanup_on_startup: true,
		force_p_newlines: true,
		force_br_newlines: false,
		cleanup_callback: custom_callback,
		plugins: "safari"
	});
	jQuery.noConflict();
	
	function custom_callback(type, value){
		switch (type) {
			case "get_from_editor":
				value = value.replace(/(<p>([<br\/>]|[&nbsp;]|\s)*<\/p>)/gi,'');
				break;
			case "insert_to_editor":
				value = value.replace(/(<p>([<br\/>]|[&nbsp;]|\s)*<\/p>)/gi,'');
				break;
			case "submit_content":
				break;
			case "get_from_editor_dom":
				break;
			case "insert_to_editor_dom":
				break;
			case "setup_content_dom":
				break;
			case "submit_content_dom":
				break;
		}

		return value;
	}
	
	cityID = $cityID; 
	
	jQuery(document).ready(function(){  
		jQuery('#countryID').bind('change', function(){ jQuery.getCities(this); });
		jQuery('#countryID').trigger('change'); 	
		jQuery("#citylist #cityID").ajaxStart(function(){
		   jQuery(this).empty();
		   jQuery(this).html('<option value="0" selected>Loading. . .</option>');
		   jQuery(this).attr('disabled', 'true');
		});
		jQuery("#citylist #cityID").ajaxStop(function(){
		   jQuery(this).attr('disabled', ''); 
		});

		tinyMCE.execCommand( 'mceAddControl', false, 'description' );
	});
//]]>  
</script>
BOF;
		
		Template::setMainTemplate($this->file_factory->getTemplate('LayoutMain'));
		Template::includeDependent   ( $obj_map->getJsToInclude()     );
		Template::includeDependentJs ( '/min/f=js/prototype.js', array('top' => true));
		Template::includeDependentJs ( "/js/addnewcity.js"            );
		Template::includeDependentJs ( "/js/utils/Class.Request.js"   );
		Template::includeDependentJs ( "/js/modaldbox.js"             );
		// Template::includeDependentCss( "/css/modaldbox.css"           ); File does not exist. Is this being used anyway? Please fix!
		Template::includeDependentCss( "/min/f=css/basic.css"        );
		Template::includeDependentJs ( '/js/jquery.checkbox.js'       );      
		Template::includeDependentJs ( '/js/jquery.entry.js'          );
		Template::includeDependentJs ( '/js/tiny_mce/tiny_mce_gzip.js');
		Template::includeDependent   ( $code                          );
		Template::includeDependentCss( "/min/f=css/styles.css"              );
		if( isset($GLOBALS["CONFIG"]) ){
			if( isset($_GET["gID"]) ){
				Template::setMainTemplateVar ( 'page_location', 'Home' );
			}else{
				Template::setMainTemplateVar ( 'page_location', 'My Passport' );
			}
		}else{
			if( isset($_GET["gID"]) ){
				Template::setMainTemplateVar ( 'page_location', 'Groups / Clubs' );
			}else{
				Template::setMainTemplateVar ( 'page_location', 'My Passport' );
			}
		}
		//Template::setMainTemplateVar ( 'page_location', 'My Passport' );
		Template::includeDependentJs ( '/js/interactive.form.js'      );
		Template::setMainTemplateVar ( 'layoutID', 'journals'         );
		
		$obj_template->out($this->file_factory->getTemplate('TravelLog'));
	}
	
	protected function __getData(){
		if( array_key_exists('articleID', $this->data) && $this->data['articleID'] && strtolower($this->data['action']) != 'save' ){
			$obj_article                  = $this->file_factory->getClass('Article', array($this->data['articleID']));
			$obj_tags 					  = $this->file_factory->getClass('Tags');
			$obj_traveler                 = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')) );     
			$obj_factory                  = $this->file_factory->invokeStaticClass('LocationFactory');
			$obj_location                 = $obj_factory->create($obj_article->getLocID());
			
			if( get_class($obj_location) == 'Country' ){
				$this->data['countryID']  = $obj_article->getLocID();
				$this->data['cityID']     = 0;
				$locationID               = $obj_article->getLocID();
			}
			elseif( get_class($obj_location) == 'City' ){
				$this->data['countryID']  = $obj_location->getCountry()->getLocationID();
				$this->data['cityID']     = $obj_trip->getLocationID();
				$locationID               = $obj_trip->getLocationID();
			}
			$curr_locationID               = $obj_traveler->getTravelerProfile()->getCurrLocationID();
			$this->data['current_location']= ( $locationID == $curr_locationID )? 1 : 0; 
			
			$this->data['authorID']		  = $obj_article->getAuthorID();
			$this->data['groupID']		  = $obj_article->getGroupID();
			$this->data['title']          = $obj_article->getTitle();
			$this->data['publish']        = $obj_article->getPublish();
			$this->data['description']    = $obj_article->getDescription();
			$this->data['callout']        = $obj_article->getCallout();
			//$this->data['date']			  = $obj_article->getArticleDate();
			$this->data['editorsnote']	  = $obj_article->getEditorsNote();	
			$this->data['tags']			  = $obj_tags->tagsToString($obj_tags->getTagsByContextID($this->data['articleID']));
			$this->data['errors']         = array();
			$this->data['action']         = '/journal.php'; 
			$this->data['mode']           = 'edit';
			
		}
		else{
			
			$this->data['countryID']           = ( isset($this->data['countryID'])           )? $this->data['countryID']           	: 0;
			$this->data['cityID']              = ( isset($this->data['cityID'])              )? $this->data['cityID']              	: 0;
			
			$this->data['groupID']		       = ( isset($this->data['groupID'])         	 )? $this->data['groupID']	         	: 0;
			$this->data['authorID']		       = ( isset($this->data['authorID'])         	 )? $this->data['authorID']         	: 0;
			$this->data['articleID']    	   = ( isset($this->data['articleID'])         	 )? $this->data['articleID']         	: 0;
			$this->data['title']               = ( isset($this->data['title'])               )? $this->data['title']               	: '';
			$this->data['publish']             = ( isset($this->data['publish'])             )? $this->data['publish']             	: 1;
			$this->data['description']         = ( isset($this->data['description'])         )? $this->data['description']         	: '';
			$this->data['callout']             = ( isset($this->data['callout'])             )? $this->data['callout']             	: '';
			$this->data['tags']	               = ( isset($this->data['tags'])                )? $this->data['tags']	               	: '';
			$this->data['editorsnote']         = ( isset($this->data['editorsnote'])         )? $this->data['editorsnote']         	: '';
			//$this->data['date']				   = ( isset($this->data['date'])             	 )? $this->data['date']             	: date('Y-m-d');
			$this->data['current_location']    = ( isset($this->data['current_location'])    )? $this->data['current_location']    	: 0;
			$this->data['errors']              = array();
			$this->data['action']              = '/journal.php';
			$this->data['mode']                = 'add';
		}	
	}
	
	protected function _save(){
		$locationID = ( $this->data['cityID'] )? $this->data['cityID']: $this->data['countryID'];
		
		//$this->data['description'] = preg_replace("#((&lt;!--)(.|\n)*(--&gt;))#","",strip_tags($this->data['description'],'<p><b><strong><i><em><u><span>'));
		$this->data['description'] = preg_replace("#<style[^>]*?>(.|\n)*?</style>#","",$this->data['description']);
		$this->data['description'] = preg_replace("#&lt;(.|\n)*?&gt;#","",strip_tags($this->data['description'],'<p><b><strong><i><em><u><span>'));

		if( $this->data['articleID'] ){
			
			$obj_article  = $this->file_factory->getClass('Article', array($this->data['articleID']));
			$obj_tags	  = $this->file_factory->getClass('Tags');
			
			$obj_article->setLocID($locationID);
			$obj_traveler = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')) );
			
			$obj_article->setAuthorID    ( $this->obj_session->get('travelerID') );
			$obj_article->setTitle       ( str_replace('/', '', $this->data['title']) );
			$obj_article->setDescription ( $this->data['description']                 );
			$obj_article->setPublish     ( $this->data['publish']                     );
			$obj_article->setCallout     ( $this->data['callout']                     );
			$obj_article->setEditorsNote ( $this->data['editorsnote']				  );
			//$obj_article->setArticleDate ( $this->data['date']						  );
			$obj_article->Update();
			
			// save tags
			$obj_tags->setContextID($this->data['articleID']);
			$obj_tags->setContext(Tags::ARTICLE);
			$obj_tags->setTagString($this->data['tags']);
			$obj_tags->Save();
		}	
		else{
			$this->file_factory->getClass('gaActionType');
			$obj_article  = $this->file_factory->getClass('Article');
			$obj_tags	  = $this->file_factory->getClass('Tags');
			$obj_article->setLocID( $locationID );
			
			$obj_article->setAuthorID    ( $this->obj_session->get('travelerID') );
			//$obj_article->setTitle       ( ereg_replace("[&\/\'\"]", "", strip_tags($this->data['title'])) );
			$obj_article->setTitle       ( preg_replace("/[&\/\'\"]/", "", strip_tags($this->data['title'])) );
			$obj_article->setDescription ( $this->data['description']                 );
			$obj_article->setPublish     ( $this->data['publish']                     );
			$obj_article->setCallout     ( $this->data['callout']                     );
			$obj_article->setEditorsNote ( $this->data['editorsnote']				  );
			//$obj_article->setArticleDate ( $this->data['date']						  );
			
			$param = "";
			if(isset($this->data['gID'])){
				$obj_article->setGroupID($this->data['gID']);
				$param = "&gID=".$this->data['gID'];
			}
			
			$obj_article->Create();
			
			$this->data['articleID'] = $obj_article->getArticleID();
			
			// save tags
			$obj_tags->setContextID($this->data['articleID']);
			$obj_tags->setContext(Tags::ARTICLE);
			$obj_tags->setTagString($this->data['tags']);
			$obj_tags->Save();
			
			$obj_factory                      = $this->file_factory->invokeStaticClass('LocationFactory');
			$obj_location                     = $obj_factory->create($locationID);
			$props                            = array();
			$obj_subject                      = $this->file_factory->getClass('gaSubject');
			$obj_mail                         = $this->file_factory->getClass('MailImplController');
			
			$props['locationID']              = $locationID;  
			$props['description']             = $this->data['description'];
			//$props['date']		              = $this->data['date'];
			$props['articleID']	              = $obj_article->getArticleID();
			$props['countryID']               = $obj_location->getCountry()->getCountryID();
			$props['action']                  = gaActionType::$SAVE;
			
			$obj_event = $this->file_factory->getClass('gaAbstractEvent', array(null, $props));
			$obj_subject->registerObserver($obj_mail);
			$obj_subject->notifyObservers($obj_event);
		}
		
		// added by nash - March 22, 2010
		// widgets observer
		// modified by adelbert - May 12, 2010
		require_once('travellog/controller/twitterWidget/Class.WidgetPostObserver.php');
		$url = '/article.php?action=view&articleID='.$this->data['articleID'].$param;
		WidgetPostObserver::post($url);
		ToolMan::redirect($url);
	}
	
	protected function _delete(){
		$obj_article = $this->file_factory->getClass('Article',array($this->data['articleID']));
		$obj_article->Delete();
		
		// For site admin only
		if( $this->obj_session->isSiteAdmin() ){
			require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
			RealTimeNotifier::getInstantNotification(RealTimeNotifier::ADMIN_DELETE_ARTICLE_ENTRY, 
				array(
					'ARTICLE_ENTRY' => $obj_article
				))->send();
			
			if( $obj_article->getOwner() instanceof Traveler )   
				ToolMan::redirect('/'.$obj_article->getTraveler()->getUsername());
			else
				ToolMan::redirect('/travel.php?action=groupJournals&gID='.$obj_article->getOwner()->getGroupID());
		}
		
		if( isset($this->data['gID']) )
			ToolMan::redirect('/journal.php?action=GroupJournals');
		else
			ToolMan::redirect('/journal.php?action=MyJournals');
	}
	
	protected function __applyRules(){
		$this->data['articleID'] = intval($this->data['articleID']); 
		if( in_array( strtoupper($this->data['action']), AbstractArticleController::$REQUIRES_ACTION_RULES) && !$this->obj_session->isSiteAdmin() ){
			// TODO: If some actions requires a more complex rules use a rule object or method instead  
			switch( strtolower($this->data['action']) ){
				case 'view':
					if(!isset($this->data['articleID']) || !is_numeric($this->data['articleID'])){
						ToolMan::redirect('/journal.php'); 
						exit;
					}
				break;
				case 'save':
					if( !$this->obj_session->getLogin() || !isset($_POST['action']) ){	
						ToolMan::redirect('/journal.php'); 
						exit;
					}
				break;	  	 
				case 'delete':  			  
				case 'edit':
					if( !$this->obj_session->getLogin() ) ToolMan::redirect('/journal.php');
					$obj_article  = $this->file_factory->getClass('Article', array($this->data['articleID']));
					$travelerID = $obj_article->getAuthorID();
					if(  $travelerID != $this->obj_session->get('travelerID') ){
						ToolMan::redirect('/journal.php');    
						exit;
					}	
				break;
				case 'add':
					if( !$this->obj_session->getLogin() ) ToolMan::redirect('/journal.php');
					if(isset($this->data['gID'])){
						$obj_group = $this->file_factory->invokeStaticClass ('GroupFactory','instance')->create(array($this->data['gID']));
						if(!count($obj_group)){ // if group !exists
							ToolMan::redirect('/journal.php');
							exit;
						}						
						$obj_traveler = $this->file_factory->getClass("Traveler",array($this->obj_session->get("travelerID")));
						$obj_group = $obj_group[0];
						switch(get_class($obj_group)){
							case "AdminGroup":
								if( $obj_group->isStaff($obj_traveler->getTravelerID()) ){
									//allow
								}elseif( isset($this->data['gID']) ){	
									$gIDarr = $obj_traveler->getGroupsAdministered(true);
									if(!in_array($this->data['gID'], $gIDarr)){	
										ToolMan::redirect('/journal.php');
										exit;
									}	
								}else{ // check if staff
									$sIDs = $obj_group->getAllGroupStaff();
									$staff = false;
									foreach($sIDs as $sID){
										if($sID->getTravelerID() == $obj_traveler->getTravelerID()){
											$staff = true;
											break;
										}	
									}
									if(!$staff){
										ToolMan::redirect('/journal.php');
										exit;
									}
								}
							break;
							case "FunGroup":
								// check if admin
								if( $obj_traveler->getTravelerID() != $obj_group->getAdministratorID() ){
										ToolMan::redirect('/journal.php');
										exit;
								}		
							break;
						}					
					}
				break;
			}
		}
	}
	
	protected function __validateForm(){
		$this->__getData();
		$errors = array();
		if( !preg_match('/.+/', $this->data['title']) ){
			$errors[] = 'Title is a required field!';
		}
		if( !preg_match('/.+/', $this->data['description']) ){
			$errors[] = 'Description is a required field!';
		}
		if( $this->data['countryID'] == 0 && $this->data['cityID'] == 0 ){
			$errors[] = 'Location of Article is a required field!';   
		}
		
		$this->data['errors'] = $errors;
		
		if( count($this->data['errors']) ){ 
			$this->_viewAddForm();
			exit;	
		}
	}
	
	protected function initProfileComp(){
		$this->obj_navigation = $this->file_factory->getClass('SubNavigation');
		
		$travelerID = $this->obj_session->get('travelerID');
		switch($this->data['action']){
			case "add":
				$this->data['gID'] = (isset($_GET['gID'])) ? $_GET['gID'] : null;
				break;
			case "view":
				$obj_article = $this->file_factory->getClass('Article',array($this->data['articleID']));
				$this->data['gID'] = ($obj_article->getGroupID() != 0) ? $obj_article->getGroupID() : null;
				/*$this->data['gID'] = (isset($_GET['gID'])) ? $_GET['gID'] : $obj_article->getGroupID();
				$this->data['gID'] = ($this->data['gID'] != 0) ? $this->data['gID'] : null;*/
				$travelerID = $obj_article->getAuthorID();
				
				$traveler = $this->file_factory->getClass('Traveler',array($travelerID));
				if(is_object($traveler) && ($traveler->isSuspended()||$traveler->isDeactivated())){
					header("location: /journal.php");
					exit;
				}
				
				break;
		}
		if(!isset($this->data['gID'])){
			$this->obj_navigation->setContext('TRAVELER');
			$this->obj_navigation->setContextID($travelerID);

			// set up owner component
			$factory      = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array());
			$profile_comp = $factory->create( ProfileCompFactory::$TRAVELER_CONTEXT );
			$profile_comp->init($travelerID);
			$this->profile_view = $profile_comp;
		}
		else{
			$this->obj_navigation->setContext('GROUP');
			$this->obj_navigation->setContextID($this->data['gID']);
			
			$factory      = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array());
			$profile_comp = $factory->create( ProfileCompFactory::$GROUP_CONTEXT );
			$profile_comp->init($this->data['gID']);
			$this->profile_view = $profile_comp;
		}
		$this->obj_navigation->setLinkToHighlight( 'MY_JOURNALS' );	
	}
	
	protected function beforePerformAction(){
		if( $this->obj_session->getLogin() ){ 
			require_once('gaLogs/Class.GaLogger.php'); 
			GaLogger::create()->start($this->obj_session->get('travelerID'),'TRAVELER');   
		}
	}
}
?>
