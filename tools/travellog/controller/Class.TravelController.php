<?
/**
* <b>Travel Controller</b> class.
* @author Aldwin S. Sabornido
* @version 1.0 PHP5
* @see http://www.GoAbroad.com/
* @copyright 2006-2007
*/


require_once("travellog/model/Class.LocationFactory.php");
require_once("travellog/model/Class.Trip.php");
require_once("travellog/model/Class.TravelLog.php");
require_once('Class.FormHelpers.php');
require_once('travellog/model/Class.Country.php');
require_once('Class.Template.php');
require_once('Class.ObjectIterator.php');
require_once('Class.Paging.php');
require_once('Class.GaDateTime.php');
//require_once('travellog/model/Class.Notifier.php');
require_once("travellog/model/Class.SubNavigation.php");
require_once('travellog/model/navigator/Class.BackLink.php');
require_once('travellog/model/Class.HelpText.php');
require_once('travellog/model/admin/Class.FeaturedSection.php');

// Added by: Adelbert Silla
require_once('travellog/controller/socialApps/Class.fbNeedToUpdateAccountController.php');

class TravelController{
	
	private static $travelID;

	public static function ViewList( $_travelerID ){
		echo 'hey11';exit;
		$obj_traveler = new Traveler($_travelerID);
		if ($obj_traveler->isAdvisor()){
			 $groupID         = AdminGroup::getAdvisorGroupID($_travelerID);
			 $obj_admin_group = new AdminGroup($groupID);
			 $travels         = $obj_admin_group->getTravels();
		}else
			 $travels         = $obj_traveler->getTravels();
		 
		$maintpl  = new Template();
		$maintpl->set_vars
		( array(
			'title'   => 'View Travels',
			'travels' => $travels
		));
		
		Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
		$maintpl->out('travellog/views/tpl.TravelView.php');
	}
	
	//public static function ShowEdit( $_travelerID,$_travelID,$_description='',$_title='',$errors = array() ){
	public static function ShowEdit( $formvars,$errors = array() ){
		//require_once('travellog/model/Class.TravelSchedule.php');
		
		$inctraveltpl     = new Template();
		$maintpl          = new Template();
		$facebook_userID  = TravelSchedule::getUserIDWithTravelerID  ( $formvars["travelerID"] );
		$is_facebook_user = false; 
		$status           = 0;
		
		/*if( $facebook_userID ){
			$obj_travel_sched       = new TravelSchedule;
			$facebook_travel_wishID = TravelSchedule::getTravelWishIDByTravelID( $formvars["travelID"]   );	
			$obj_travel_item        = $obj_travel_sched->getTravelItem( $facebook_travel_wishID );
			$status                 = $obj_travel_item->getStatusID();
			$is_facebook_user       = true;       
		} */
		$subNavigation    = new SubNavigation();
		$subNavigation->setContext('TRAVELER');
		$subNavigation->setContextID($formvars["travelerID"]);
		$subNavigation->setLinkToHighlight('MY_JOURNALS');	
		
		$obj_backlink    = BackLink::instance();
		$obj_backlink->Create();
		
		if(!count($errors)){
			$travel                  = new Travel($formvars["travelID"]);
			$_locationID             = $travel->getLocationID();
			$formvars["description"] = $travel->getDescription();
			$formvars["title"]       = $travel->getTitle();
			$formvars["privacypref"] = $travel->getPrivacyPref();
		}

		$inctraveltpl->set('action'           , "http://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']."?action=update");
		$inctraveltpl->set('title'            , $formvars["title"]                         );			
		$inctraveltpl->set('description'      , $formvars["description"]                   );
		$inctraveltpl->set('privacypref'      , $formvars["privacypref"]                   );
		$inctraveltpl->set('travelID'         , $formvars["travelID"]                      );
		$inctraveltpl->set('context'          , $formvars["context"]                       );
		$inctraveltpl->set('is_facebook_user' , $is_facebook_user                          );
		$inctraveltpl->set('status'           , $status                                    );
		$inctraveltpl->set('mode'             , 'edit'                                     );
		$inctraveltpl->set('errors'           , $errors                                    );
		$inctraveltpl->set('obj_backlink'     , $obj_backlink                              );
		$inctraveltpl->set('hasJournals'      , self::hasJournals($_SESSION['travelerID']) );
		
		if ($formvars["context"] == 2){
			$inctraveltpl->set('gID'           , $formvars["gID"]);
			$inctraveltpl->set('isSubGroup'    , $formvars["isSubGroup"]);
			$inctraveltpl->set('col_subgroups' , $formvars["col_subgroups"]);
		}	
		
		$maintpl->set_vars
		( array(
			'title'         => "Edit Travel",
			'form'          => $inctraveltpl->fetch('travellog/views/tpl.FrmTravel.php'),
			'context'       => $formvars["context"],
			'subNavigation' => $subNavigation
		));
		Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
		$maintpl->out('travellog/views/tpl.Travel.php');
	}	


	public static function ShowAdd($formvars,$errors = array()){  	
		$cs              = Country::getCountryList();
		$inctraveltpl    = new Template();
		$maintpl         = new Template();
		$subNavigation   = new SubNavigation();
		$subNavigation->setContext('TRAVELER');
		$subNavigation->setContextID($_SESSION['travelerID']);
		$subNavigation->setLinkToHighlight('MY_JOURNALS');	
		
		$is_facebook_user = false; 
		$status           = 0;
		
		
		$obj_backlink    = BackLink::instance();
		$obj_backlink->Create();
				
		$obj_help_text = new HelpText();
		
		$inctraveltpl->set('action'        , "http://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']."?action=save");			
		$inctraveltpl->set('description'      , $formvars["description"]                   );
		$inctraveltpl->set('title'            , $formvars["title"]                         );
		$inctraveltpl->set('privacypref'      , $formvars["privacypref"]                   );
		$inctraveltpl->set('context'          , $formvars["context"]                       );
		$inctraveltpl->set('is_facebook_user' , $is_facebook_user                          );
		$inctraveltpl->set('status'           , $status                                    );
		$inctraveltpl->set('travelID'         , ''                                         );
		$inctraveltpl->set('errors'           , $errors                                    );
		$inctraveltpl->set('mode'             , 'add'                                      );
		$inctraveltpl->set('obj_backlink'     , $obj_backlink                              );
		$inctraveltpl->set('obj_help_text'    , $obj_help_text                             );
		$inctraveltpl->set('hasJournals'      , self::hasJournals($_SESSION['travelerID']) );
		
		if ($formvars["context"] == 2){
			$inctraveltpl->set('gID'           , $formvars["gID"]);
			$inctraveltpl->set('isSubGroup'    , $formvars["isSubGroup"]);
			$inctraveltpl->set('col_subgroups' , $formvars["col_subgroups"]);
			
		}
		
		$maintpl->set_vars
		( array(
			'title'         => "Add Travel",
			'form'          => $inctraveltpl->fetch('travellog/views/tpl.FrmTravel.php'),
			'context'       => $formvars["context"],
			
			'subNavigation' => $subNavigation
		));
		Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
		$maintpl->out('travellog/views/tpl.Travel.php');
	}
	
	public static function Delete($_travelerID,$_travelID){
		$travel  = new Travel($_travelID);
		if ($travel->getTravelerID() == $_travelerID){ 
			require_once('travellog/model/Class.JournalLog.php');
			
			$travel->Delete();
			
			$objJournalLog = new JournalLog;
			$objJournalLog->setLogType ( 3          );
			$objJournalLog->setTravelID( $_travelID );
			$objJournalLog->Delete();	

			// Added by: Adelbert Silla
			// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
			//           This is a preperation for updating application static views in facebook
			// Date added: Aug. 10, 2009
			// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
			fbNeedToUpdateAccountController::saveNewlyEditedAccount($_travelerID,4);


		}
	}	
	
	public static function Save($formvars = array()){	
		$obj_traveler = new Traveler($formvars["travelerID"]);
		$refID        = $formvars["travelerID"];
		$refType      = 1;
		
		if ($obj_traveler->isAdvisor()){
			$refID    = $formvars["gID"];
			$refType  = 2; 	
		}
		
		$obj_travel  = new Travel();
		$obj_travel->setRefID      ( $refID                   );
		$obj_travel->setRefType    ( $refType                 );
		$obj_travel->setTitle      ( $formvars["title"]       );
		$obj_travel->setTravelerID ( $formvars["travelerID"]  );
		$obj_travel->setDescription( $formvars["description"] );
		$obj_travel->setPrivacyPref( $formvars["privacypref"] ); 
		$obj_travel->Create();

		TravelController::$travelID = $obj_travel->getTravelID();

		// Added by: Adelbert Silla
		// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
		//           This is a preperation for updating application static views in facebook
		// Date added: Aug. 10, 2009
		// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
		fbNeedToUpdateAccountController::saveNewlyEditedAccount($formvars["travelerID"],4);

	}	
	

	public static function Update($formvars){
		require_once('travellog/model/Class.JournalLog.php');
		require_once('travellog/model/Class.JournalLogType.php');
		
		
		$obj_travel  = new Travel($formvars["travelID"]);
		if ($obj_travel->getTravelerID() == $formvars["travelerID"]){
			$obj_traveler = new Traveler($formvars["travelerID"]);
			$refID        = $formvars["travelerID"];
			$refType      = 1;
			if ($obj_traveler->isAdvisor()){
				$refID    = $formvars["gID"];
				$refType  = 2; 	
			}
			$obj_travel->setRefID      ( $refID                   );
			$obj_travel->setRefType    ( $refType                 );
			$obj_travel->setTitle      ( $formvars["title"]       );
			$obj_travel->setTravelerID ( $formvars["travelerID"]  );
			$obj_travel->setDescription( $formvars["description"] );
			$obj_travel->setPrivacyPref( $formvars["privacypref"] ); 
			$obj_travel->Update();
			
			$obj_log = new JournalLog;
			$obj_log->setTravelID( $formvars["travelID"]            );
			$obj_log->setLogType ( JournalLogType::$UPDATED_JOURNAL );
			$obj_log->setLinkID  ( $formvars["travelID"]            );
			$obj_log->Save();

			// Added by: Adelbert Silla
			// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
			//           This is a preperation for updating application static views in facebook
			// Date added: Aug. 10, 2009
			// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
			fbNeedToUpdateAccountController::saveNewlyEditedAccount($formvars["travelerID"],4);
	
		}
	}	
	
	public static function ViewAllList( $formvars ){
		//viewed,rating,lastupdated
		$paging              = NULL;
		$outtravels          = NULL;
		$country_name        = NULL;        
		$groupID             = 0;
		$link                = NULL;
		$objObjectIterator   = NULL;
		$objPaging           = NULL;
		$objMainTemplate     = new Template();
		$cond                = new Condition();
		$d                   = new GaDateTime();
		$cond1               = new Condition();
		$filter              = new FilterCriteria2();
		$objSubNavigation    = new SubNavigation();
		$objFeaturedSection  = new FeaturedSection(9);
		$objHelpText         = new HelpText();
		$formvars['groupID'] = 0;
		
		if ( $formvars['travelerID'] ){
			$traveler = new Traveler($formvars['travelerID']);
			require_once('travellog/model/Class.JournalPrivacyPreference.php');
			$obj_privacy_pref  = new JournalPrivacyPreference;
			if ($traveler->isAdvisor()){
				$formvars['groupID'] = AdminGroup::getAdvisorGroupID($formvars['travelerID']);
				$obj_admin_group     = new AdminGroup($formvars['groupID']);
				$collectionTravels   = $obj_admin_group->getTravels( $obj_privacy_pref->getPrivacyCriteria2s($formvars['curr_travelerID'], $formvars['travelerID']) );
			}
			else{
				$collectionTravels = $traveler->getTravels( $obj_privacy_pref->getPrivacyCriteria2s($formvars['curr_travelerID'], $formvars['travelerID']) );
			}
		}
		elseif ($formvars['locationID'] > 0 )		// added by daf feb.9.06
			$collectionTravels = Travel::getAllTravelsByCountryID($formvars['locationID']);
		else{
			
			$cond1->setAttributeName('locationID');
			$cond1->setOperation(FilterOp::$EQUAL);
			$cond1->setValue($formvars['locationID']);
			
			$cond->setAttributeName($formvars['sort_by']);
			$cond->setOperation(FilterOp::$ORDER_BY_DESC);
			
			$filter->addCondition($cond);
			$filter->addCondition($cond1);
			$collectionTravels = Travel::getFilteredTravels($filter);
		}
		
		if( $formvars['locationID'] > 0 ){
			$factory      = LocationFactory::instance();
			$location     = $factory->create($formvars['locationID']);
			$country_name = $location->getName();
		}
		
		$collectionPopularTravels = Travel::getMostPopularTravel();
		$collectionCountry        = Travellog::getCountriesWithLogs();
					
		if( count($collectionTravels) ){
			if( $formvars['locationID'] ){ 
				$formvars['query_string'] = '&countryID='.$formvars['locationID'];
			}				
			if( $formvars['travelerID'] )
				$objPaging = new Paging( count($collectionTravels), $formvars['page'], 'action=viewmytravels&sortby='.$formvars['sort_by'].$formvars['query_string'], 5);
			else
				$objPaging = new Paging( count($collectionTravels), $formvars['page'], 'sortby='.$formvars['sort_by'].$formvars['query_string'], 5);
			
			$objObjectIterator = new ObjectIterator($collectionTravels, $objPaging);
		} 
		
		if( $formvars['groupID'] == 0 ){
			$objSubNavigation->setContext        ( 'TRAVELER'              );
			$objSubNavigation->setContextID      ( $formvars['travelerID'] );
		}
		else{
			$objSubNavigation->setContext        ( 'GROUP'      );
			$objSubNavigation->setContextID($formvars['groupID']);
			$objSubNavigation->setGroupType('ADMIN_GROUP');
		}	
		$objSubNavigation->setLinkToHighlight( 'MY_JOURNALS'           );
		if( $formvars['travelerID'] == 0 ) $objSubNavigation->setDoNotShow(true);
		
		if(array_key_exists('journal_page_owner', $formvars)) {
			$objMainTemplate['journal_page_owner'] = $formvars['journal_page_owner'];
		}
		 
		$objMainTemplate->set_vars
		( array(
			'objPaging'                => $objPaging,
			'objObjectIterator'        => $objObjectIterator,
			'formvars'                 => $formvars,
			'objSubNavigation'         => $objSubNavigation,
			'collectionPopularTravels' => $collectionPopularTravels,
			'collectionFeaturedSection'=> $objFeaturedSection->getFeaturedItems(),
			'collectionCountry'        => $collectionCountry,
			'objHelpText'              => $objHelpText,
			'country_name'             => $country_name,       
			'd'                        => $d,
		));
		
		Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
		if( !$formvars['mytravels'] )
			$objMainTemplate->out('travellog/views/tpl.TravelView.php');
		else{
			$objMainTemplate->out('travellog/views/tpl.MainTravel.php');
		}	  
	}
	
	public static function hasJournals($travelerID){
		$traveler = new Traveler($travelerID);
		if ($traveler->isAdvisor()){
			$groupID           = AdminGroup::getAdvisorGroupID($travelerID);
			$obj_admin_group   = new AdminGroup($groupID);
			$collectionTravels = $obj_admin_group->getTravels();
		}else
			$collectionTravels   = $traveler->getTravels();
		return ( count($collectionTravels) )? true: false;
	}
		
	public static function getTravelID(){
		return TravelController::$travelID;
	}
}

?>
