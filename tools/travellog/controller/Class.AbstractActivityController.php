<?php
require_once("travellog/model/Class.SessionManager.php");
require_once("Class.ToolMan.php");
require_once("travellog/controller/Class.IController.php");
require_once("travellog/factory/Class.FileFactory.php");
//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
abstract class AbstractActivityController implements IController{ 
	
	protected static $REQUIRES_ACTION_RULES = array('ADD', 'SAVE', 'EDIT', 'DELETE', 'REMOVE', 'CREATE');
	
	protected  
	
	$obj_session  = NULL,
	
	$file_factory = NULL;
	
	function __construct(){
		$this->obj_session  = SessionManager::getInstance();
		$this->file_factory = FileFactory::getInstance(); 
		$this->file_factory->registerClass('Program');
		$this->file_factory->registerClass('Activity'); 
		$this->file_factory->registerClass('AdminGroup');
		$this->file_factory->registerClass('Template'       , array('path' => ''));
		$this->file_factory->registerClass('HtmlHelpers'    , array('path' => ''));
		$this->file_factory->registerClass('GaDateTime'       , array('path' => ''));
		
		$this->file_factory->registerTemplate('LayoutMain');
		$this->file_factory->registerTemplate('ActivityContent');
		$this->file_factory->registerTemplate('ViewProgramActivity');
		$this->file_factory->registerTemplate('Activity');
		$this->file_factory->registerTemplate('FrmActivity');
		
		$this->file_factory->setPath('CSS', '/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
	}
	
	function performAction(){
		$this->beforePerformAction();
		switch( strtolower($this->data['action']) ){
			case "edit":
			case "create":
				$this->__getData();
				$this->_viewAddForm();
			break;
			
			case "save":
				$this->__validateForm();
				$this->_save();
			break;
			
			case "activitylists":
				$this->_viewActivityLists();
			break;
			
			case "eventdescription": 
			case "activitydescription":
				$this->_viewActivityDescription();
			break;
			
			case "remove":
				$this->_remove();
			break;
			
			case "delete":
				$this->_delete();
			break;
			
			case "add":
			default:
				$this->_viewActivities();
		}
	}	
	
	protected function _viewAddForm(){
		$obj_template     = $this->file_factory->getClass('Template');
		$obj_inc_template = clone($obj_template);
		$obj_inc_template->set('props', $this->data);
		$obj_template->set('form' , $obj_inc_template->fetch($this->file_factory->getTemplate('FrmActivity')) );
		$obj_template->set('props', $this->data);
		
		echo $obj_template->fetch( $this->file_factory->getTemplate('Activity') ); 
	}
	
	protected function _viewActivityDescription(){
		$obj_template                  = $this->file_factory->getClass('Template');
		$this->data['obj_event']       = $this->file_factory->getClass('Activity', array($this->data['eID']));
		$this->data['obj_program']     = $this->file_factory->getClass('Program', array($this->data['obj_event']->getProgramID()));
		$this->data['gID']             = $this->data['obj_program']->getGroupID();         
		$obj_group                     = $this->file_factory->getClass('AdminGroup', array($this->data['gID']));
		$this->data['is_owner']        = ( $this->obj_session->get('travelerID') == $obj_group->getAdministratorID() )? true: false;
		$this->data['is_login']        = $this->obj_session->getLogin();    
		$obj_template->set( 'props', $this->data );        
		
		echo $obj_template->fetch($this->file_factory->getTemplate('ActivityContent')); 
	}
	
	protected function _save(){
		if( $this->data['eID'] ){
			$obj_activity = $this->file_factory->getClass('Activity', array($this->data['eID']));
			$obj_activity->setTitle         ( $this->data['title']       );
			$obj_activity->setTheDate       ( $this->data['event_date']  );			
			$obj_activity->setDescription   ( $this->data['description'] );
			$obj_activity->setVenue         ( $this->data['venue']       );
			$obj_activity->setDisplayPublic ( $this->data['display']     );  
			$obj_activity->Update();
		}
		else{
			$obj_activity = $this->file_factory->getClass('Activity');
			$obj_activity->setProgramID     ( $this->data['pID']         );
			$obj_activity->setTitle         ( $this->data['title']       );
			$obj_activity->setTheDate       ( $this->data['event_date']  );			
			$obj_activity->setDescription   ( $this->data['description'] );
			$obj_activity->setVenue         ( $this->data['venue']       );
			$obj_activity->setDisplayPublic ( $this->data['display']     );
			$obj_activity->Create();
		}	
	}
	
	protected function _delete(){
		$obj_event = $this->file_factory->getClass('Activity', array($this->data['eID']));
		$obj_event->Delete();
	}
	
	protected function _remove(){
		$this->_delete();
		$link = $this->data['from_where'];
		ToolMan::redirect('/'.$link.'.php');  
	}
	
	
	
	protected function _viewActivityLists(){
		echo $this->__getActivityLists();
	}
	
	protected function __getActivityLists(){ 
		$obj_template           = $this->file_factory->getClass('Template');
		$obj_program            = $this->file_factory->getClass('Program', array($this->data['pID']));
		$this->data['gID']      = $obj_program->getGroupID();  
		$obj_group              = $this->file_factory->getClass('AdminGroup', array($this->data['gID']));
		$col_activities         = $this->__filter($obj_program->getActivities());
		$this->data['is_login'] = $this->obj_session->getLogin();
		$this->data['is_owner'] = ( $obj_group->getAdministratorID() == $this->obj_session->get('travelerID') )? true : false;
		
		$obj_template->set( 'props'         , $this->data     ); 
		$obj_template->set( 'col_activities', $col_activities );
		
		return $obj_template->fetch( $this->file_factory->getTemplate('ViewProgramActivity') ); 
	}
	
	protected function __filter($col_activities){
		$temp_activity = array();
		if( !$this->obj_session->getLogin() && count($col_activities) ){
			foreach ( $col_activities as $obj_activity){
				if ( $obj_activity->getDisplayPublic() == 1 ) $temp_activity[] = $obj_activity;  
			}
		} 
		elseif( count($col_activities) ){
			$temp_activity = $col_activities;
		}
		return $temp_activity; 
	}
	
	protected function __getData(){
		if( array_key_exists('eID', $this->data) && $this->data['eID'] && strtolower($this->data['action']) != 'save' ){
			$obj_activity              = $this->file_factory->getClass('Activity', array($this->data['eID']));
			$obj_program               = $this->file_factory->getClass('Program', array($obj_activity->getProgramID()));
			$this->data['title']       = $obj_activity->getTitle();
			$this->data['description'] = $obj_activity->getDescription();
			$this->data['display']     = $obj_activity->getDisplayPublic();
			$this->data['venue']       = $obj_activity->getVenue();
			$this->data['pID']         = $obj_activity->getProgramID();
			$this->data['event_date']  = $obj_activity->getTheDate();
			$this->data['start_date']  = $obj_program->getStart();
			$this->data['finish_date'] = $obj_program->getFinish();
			$this->data['errors']      = array();
			$this->data['mode']        = 'edit';
		}
		else{    
			$obj_program               = $this->file_factory->getClass('Program', array($this->data['pID']));
			$this->data['title']       = ( isset($this->data['title'])       )? $this->data['title']       : '';
			$this->data['description'] = ( isset($this->data['description']) )? $this->data['description'] : '';
			$this->data['display']     = ( isset($this->data['display'])     )? $this->data['display']     : 1;
			$this->data['venue']       = ( isset($this->data['venue'])       )? $this->data['venue']       : '';
			$this->data['event_date']  = ( isset($this->data['event_date'])  )? $this->data['event_date']  : '';
			$this->data['start_date']  = ( isset($this->data['start_date'])  )? $this->data['start_date']  : '';
			$this->data['finish_date'] = ( isset($this->data['finish_date']) )? $this->data['finish_date'] : '';
			$this->data['eID']         = ( isset($this->data['eID'])         )? $this->data['eID']         : 0;  
			$this->data['errors']      = array();
			$this->data['mode']        = 'add';     
		}	
		
	}
	
	protected function __validateForm(){ 
		$this->__getData(); 
		$errors = array();
		if( !preg_match('/.+/', $this->data['title']) ){
			$errors[] = 'Title is a required field!';
		}
		if( !preg_match('/.+/', $this->data['description']) ){
			$errors[] = 'Description is a required field!';
		}
		if( !preg_match('/.+/', $this->data['venue']) ){
			$errors[] = 'Venue is a required field!';
		}
		if( !preg_match('/.+/', $this->data['event_date']) ){ 
			$errors[] = 'Date is a required field!';
		}
		
		$this->data['errors'] = $errors;
		
		if( count($this->data['errors']) ){ 
			$this->_viewAddForm();
			exit;	
		}
	} 
	
	protected function __applyRules(){ 
		if( in_array( strtoupper($this->data['action']), AbstractActivityController::$REQUIRES_ACTION_RULES) ){
			// TODO: If some actions requires a more complex rules use a rule object or method instead
			switch( strtolower($this->data['action']) ){
				case 'add':	
					if( !$this->obj_session->getLogin() ){
						ToolMan::redirect('/group.php?gID='.$this->data['gID']);
						exit;
					}
				break; 
				
				case 'save':
					if( !isset($_POST['action']) ){	
						echo '<h2>Requires a post method.</h2>';
						exit;
					}	
				break;
				
				case 'remove':
					if( !$this->obj_session->getLogin() ){
						ToolMan::redirect('/index.php');
						exit;
					}
					$link = $this->data['from_where'];     
					if( !isset($this->data['eID']) ){
						ToolMan::redirect('/'.$link.'.php'); 
						exit; 
					}
					$obj_event         = $this->file_factory->getClass('GroupEvent', array($this->data['eID']));
					$obj_group_factory = $this->file_factory->invokeStaticClass('GroupFactory');

					$mGroup		= $obj_group_factory->create(array($obj_event->getGroupID()));
					$obj_group	= $mGroup[0];
					$obj_event	= $this->file_factory->getClass('Activity', array($this->data['eID']));
					$obj_program = $this->file_factory->getClass('Program', array($obj_event->getProgramID()));
					$obj_group	 = $this->file_factory->getClass('AdminGroup', array($obj_program->getGroupID()));
					if(  $obj_group->getAdministratorID() != $this->obj_session->get('travelerID') ){
						ToolMan::redirect('/'.$link.'.php'); 
						exit;
					} 	
				break;
				 
				case 'delete':	
					if( !isset($_POST['action']) ){	
						echo '<h2>Requires a post method.</h2>';
						exit;
					}			  
				case 'edit': 
					if( !isset($this->data['eID']) ){
						echo '<h2>Invalid eventID!!!</h2>';
						exit;
					}  
					$obj_event   = $this->file_factory->getClass('Activity', array($this->data['eID']));
					$obj_program = $this->file_factory->getClass('Program', array($obj_event->getProgramID()));
					$obj_group   = $this->file_factory->getClass('AdminGroup', array($obj_program->getGroupID()));
					if(  $obj_group->getAdministratorID() != $this->obj_session->get('travelerID') ){
						echo '<h2>You are not allowed to '. $this->data['action'] .' this event.</h2>';
						exit;
					} 	
				case 'create':
					if( !$this->obj_session->getLogin() ){
						echo '<h2>You are not allowed to add.</h2>';
						exit;
					}
				break; 
			}
		}
	}
	
	protected function beforePerformAction(){
		if( $this->obj_session->getLogin() ){
			require_once('gaLogs/Class.GaLogger.php');
			GaLogger::create()->start($this->obj_session->get('travelerID'),'TRAVELER');  
		}
	}
} 
?>   
