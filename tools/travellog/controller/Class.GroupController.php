<?

	require_once("Class.Template.php");
	require_once('Class.FormHelpers.php');
	require_once('Class.Connection.php');
	require_once('Class.Recordset.php');
	require_once('Class.Paging.php');
	require_once('Class.ObjectIterator.php');
	require_once('travellog/model/Class.Traveler.php');
	require_once('travellog/model/Class.GroupCategory.php');
	require_once('travellog/model/Class.MessageSpace.php');
	require_once('travellog/model/Class.RowsLimit.php');
	require_once('travellog/model/Class.Calendar.php');
	require_once('travellog/model/Class.Map.php');
	require_once('travellog/model/Class.Condition.php');
	require_once('travellog/model/Class.FilterCriteria2.php');
	require_once('travellog/model/Class.Group.php');
	require_once('travellog/model/Class.GroupAccess.php');
	require_once('travellog/model/Class.GroupFactory.php');
	require_once('travellog/model/navigator/Class.BackLink.php');
	require_once('travellog/model/Class.SubNavigation.php');
	require_once('travellog/model/Class.GroupMessagePanel.php');
	require_once('travellog/model/Class.OnlineSurvey.php');
	require_once('travellog/views/Class.TravelsView.php');
  require_once('travellog/components/profile/controller/Class.ProfileCompFactory.php');
	require_once 'travellog/views/Class.ViewImportantLinks.php';

	require_once('Class.HtmlHelpers.php');
	
	/*******************************************************************************************
	 * edits: neri - 01/22/09
	 * 		set the value of viewFeaturedJournals to true if group has featured journals
	 * 			or if admin is logged
	 * 		displayed the journals template only if viewFeaturedJournals is true in viewGroup()
	 * 		added function for displaying the recent videos of the group in homepage: 06/10/09
	 *******************************************************************************************/

	class GroupController
	{
	
		protected
	
		$group 					= NULL,
		$isMemberLogged 		= FALSE,	
		$isAdminLogged 			= FALSE,
		$main_tpl 				= FALSE,
		$subNavigation			= NULL,
		$IncMessageCenter		= NULL,
		$isSuperStaffLogged		= FALSE,
	
		$groupID,	
		$grpDiscrim,	
		$grpAccess,	
	
		$loggedUser 			= NULL,	
		$loggedUserID 			= 0,	
		$isLogged 				= FALSE,	
	
		$viewGreetings			= FALSE,	
		$viewCalendar  			= FALSE,
		$viewResourceFiles 		= FALSE,	
		$viewSubGroups 			= FALSE,	
		$viewCustomSection		= FALSE,		
		$viewGroupJournals		= FALSE,
		$viewFeaturedJournals	= FALSE,
		$viewRecentVideos 		= FALSE,
	
		$viewPrefCalendar	 	= FALSE,	
		$viewPrefResourceFiles	= FALSE,	
		$viewPrefGroups			= FALSE,
		$viewImportantLinks		= FALSE,
		$viewGroupSteps			= FALSE,
		$isMessageCenterPublic	= FALSE,
		
		$showGroupSteps			= 0,
				
		$IncGroupInfo 			= NULL,	
		$IncGroupMembers 		= NULL,	
		$IncGroupMap 			= NULL,	
		$IncObjMap	 			= NULL,
		$IncGroupGreetings		= NULL,
		$IncGroupCalendar		= NULL,
		$IncGroupFiles 			= NULL,	
		$IncGroupSubGroups		= NULL,
		$IncGroupCustomSection	= NULL, 
		$IncGroupJournals		= NULL,
		$IncGroupSurveys		= NULL,
		$IncGroupLogin          = NULL,
		$IncGroupAlbums         = NULL,
		$IncGroupImportantLinks = NULL,
		$IncGroupPhotoAlbums    = NULL, //added by Joel - 03-25-2008
		$IncGroupDiscussions	= NULL,
		$IncGroupArticles       = NULL, // added by ianne 05/08/2009
		$IncGroupRecentVideos	= NULL, // added by neri - 06/09/2009
	
		$isShowDependentjScript = true,
		
		$file_factory			= NULL,
		$mainGroupInfo			= NULL, //added by nash - Nov. 06, 2008
		
		$hasResourceFiles		= FALSE,
		$hasSubGroups			= FALSE,
		$hasStaff				= FALSE,
		$hasEvents				= FALSE,
		$hasLinks				= FALSE,
		$hasDiscussions			= FALSE,
		$hasMembers				= FALSE,
		$hasPhotos				= FALSE,
		$hasVideos				= FALSE,
		
		$isParent				= FALSE,
		
		$IncGroupTagCloud		= NULL; //added by naldz - 01-22-2010
		
		function setGroupDetails($group)	{
			$this->group = $group;
		
			$this->groupID = $group->getGroupID();
			$this->grpDiscrim = $group->getDiscriminator();
			$this->grpAccess = $group->getGroupAccess();
			$this->showGroupSteps = $group->showGroupSteps();
			
			$this->isParent = $group->isParent();
		}
	
		function setLoggedUser($loggedUser)	{
			$this->loggedUser = $loggedUser;
			$this->loggedUserID = $loggedUser->getTravelerID();
		}
	
		function setIsLogged($isLogged)	{
			$this->isLogged = $isLogged;
		}
	
		function setIsMemberLogged($isMemberLogged)	{
			$this->isMemberLogged = $isMemberLogged;
		}
	
		function setIsAdminLogged($isAdminLogged)	{
			$this->isAdminLogged = $isAdminLogged;
		}
	
		function setViewNews($_viewNews)	{
			$this->viewNews = $_viewNews;
		}
	
		function setFileFactory($fileFactory) { // added by nash - Nov. 06, 2008
			$this->file_factory = $fileFactory;
		}
	
		function __construct()
		{
	
			$this->main_tpl = new Template();
			$this->main_tpl->set_path("travellog/views/");
		
		}
	

		function preparePreferences(){
			try {
				$privacypref 	        		= new GroupPrivacyPreference($this->group->getGroupID());
				$this->viewPrefCalendar	 		= $privacypref->getViewCalendar();	
				$this->viewPrefGroups			= $privacypref->getViewGroups();
				$this->viewPrefResourceFiles	= $privacypref->getDownloadFiles();
				$this->isMessageCenterPublic    = $privacypref->getViewBulletin();					
			}
		
			catch (Exception $e) {			
			
				$this->viewPrefCalendar	 		= true;	
				$this->viewPrefResourceFiles	= true;	
				$this->viewPrefGroups			= true;
			
			}
		
		}
	
		function prepareMainGroupInfo(){	// function added by nash - Nov. 07, 2008
			$factory = ProfileCompFactory::getInstance();
			$profile_comp = $factory->create( ProfileCompFactory::$GROUP_CONTEXT );
			
			if((2 == $this->group->getDiscriminator()) AND (1 < $this->group->getParentID())) { // if subgroup 
				$profile_comp->init($this->group->getParentID());
			}
			else {
				$profile_comp->init($this->groupID);
			}
			
			$this->mainGroupInfo = $profile_comp->get_view();
			
		}
	
		function prepareGroupInfo(){
			
			$grpInfoTemplate  = clone $this->main_tpl;
			$grpInfoTemplate->set_path("travellog/views/");
		
			$grpInstInfoTemplate  = clone $this->main_tpl;
			$grpInstInfoTemplate->set_path("travellog/views/");
		
			$grpInfoTemplate->set("loggedUser",$this->loggedUser);	
			$grpInfoTemplate->set("isLogged",$this->isLogged);	
			$grpInfoTemplate->set("isMemberLogged",$this->isMemberLogged);	
			$grpInfoTemplate->set("isAdminLogged",$this->isAdminLogged);
			
			$grpInfoTemplate->set("is_parent_group",$this->isParent);
		
			$grpInfoTemplate->set("group",$this->group);
			$grpInfoTemplate->set("grpID",$this->groupID);
			$grpInfoTemplate->set("grpDiscriminator",$this->grpDiscrim);
			$grpInfoTemplate->set("grpAccess",$this->grpAccess);
		
			$grpInfoTemplate->set("grpName",$this->group->getName());
			$grpInfoTemplate->set("grpDesc",$this->group->getDescription());
		
			$grpInfoTemplate->set("grpCategoryList",$this->group->getGroupCategory(true));
		
			$grpInfoTemplate->set("grpLabelName", $this->group->getLabel());
			
			//  Added by Antonio Pepito Cruda Jr. for view more or less in group info
	      $desc_len = strlen($this->group->getDescription());
	      $truncated_description_right = $this->group->getDescription();
	      $truncated_description_left = "";
	      $show_collapse = false;
  
	      if (200 < $desc_len) {
	  	    $show_collapse = true;
	  	    $truncate_index = 200;
	        // truncate from the nearest space starting from the position 200
	        for (;$truncate_index < $desc_len; $truncate_index++) {
	    	    if (" " == substr($this->group->getDescription(),$truncate_index,1)) {
	    		    break;
	    	    }
	        }
  
	  	    $truncated_description_right = substr($this->group->getDescription(),0,$truncate_index);
	        $truncated_description_left = substr($this->group->getDescription(),$truncate_index,5000);
	      }	

			$grpInfoTemplate->set("show_collapse",$show_collapse);
			$grpInfoTemplate->set("truncated_description_right",$truncated_description_right); 
			$grpInfoTemplate->set("truncated_description_left",$truncated_description_left);     
      		
			// end of added lines
			
		
			$grpPhotoID = $this->group->getGroupPhotoID();
				
			$grpInfoTemplate->set("grpPhoto", $this->group->getGroupPhoto());
		  
		   if (0 == $grpPhotoID){		
				$grpLabel = ($this->isParent) ? 'Group' : 'Subgroup';
				$grpInfoTemplate->set("grpPhotoLabel", 'Upload '.$grpLabel. ' Logo' );
				$grpInfoTemplate->set("grpPhotoLink", 'photomanagement.php?cat=group&action=add&genID=' . $this->groupID );
		
			}
			else {
				$grpLabel = ($this->isParent) ? 'Group' : 'Subgroup';
				$grpInfoTemplate->set("grpPhotoLabel", 'Change '.$grpLabel. ' Logo' );
				$grpInfoTemplate->set("grpPhotoLink", 'photomanagement.php?cat=group&action=edit&genID=' . $this->groupID . '&photoID=' . $grpPhotoID );				
			
				$grpInfoTemplate->set("grpDelPhotoLabel", 'Remove '.$grpLabel. ' Logo' );
				$grpInfoTemplate->set("grpDelPhotoLink", 'photomanagement.php?cat=group&action=delete&genID=' . $this->groupID . '&photoID=' . $grpPhotoID );
			
			}
		
		
			if (GROUP::ADMIN == $this->group->getDiscriminator() ){
			
				$grpInfoTemplate->set("isParent",$this->group->isParent());
			
				if (TRUE== $this->group->isSubGroup())
					$grpInfoTemplate->set("parentGrp",$this->group->getParent());
			
				// >>>>  GET INSTITUTION INFO  >>>> 
				$grpInstInfoTemplate->set("grpInstitutionInfo",$this->group->getInstitutionInfo());	
				$grpInfoTemplate->set("grpWebURL", $this->group->getInstitutionInfo()->getWebURL() );
				$grpInfoTemplate->set("grpInstitutionInfo", $grpInstInfoTemplate->fetch("tpl.IncGroupInstInfo.php") );
			
			
				// >>>>  GET NEW INQUIRIES  >>>> 
				$inquirybox = new InquiryBox($this->group->getSendableID());
				$newmessages = $inquirybox->getNumberofNewMessages() ;
			
				if (1 == $newmessages )
					$inquiryStr = 'You have ' . $newmessages . ' new inquiry.';
			    elseif (1 < $newmessages)
			   		$inquiryStr = 'You have ' . $newmessages . ' new inquiries.';
		    
			    if (isset($inquiryStr) && 0 <  AdminGroup::getAdvisorClientID($this->group->getAdministrator()->getTravelerID()))		// if there's an inquiry and this is account of a GA advisor
	    			$grpInfoTemplate->set("grpInquiryStr", $inquiryStr);	
    		
				// >>>> IF ADMIN GROUP, SET LINK FOR ADD and VIEW PHOTO ALBUMS
			
				$grpInfoTemplate->set("grpAddPhotoLabel", "Add Photo Album" );
				$grpInfoTemplate->set("grpAddPhotoLink", "photoalbum.php?action=add&groupID=" . $this->groupID );
				if (count($this->group->getPhotoAlbums())) {
					$grpInfoTemplate->set("grpViewPhotoLabel", "View Photo Albums" );
					$grpInfoTemplate->set("grpViewPhotoLink", "photoalbum.php?action=view&groupID=" . $this->group->getGroupID() .  "&travelerID=" . $this->loggedUserID);
				}
			
				if (NULL != $this->group->getOnlineForm()) {
					if ($this->isAdminLogged){
						$grpInfoTemplate->set("grpSurveyLabel", "Online Survey Form" );
						$grpInfoTemplate->set("grpSurveyLink", "onlineform.php?formID=" . $this->group->getOnlineForm()->getOnlineFormID());
					}
					else if ($this->isMemberLogged){
						$arrParticipants = OnlineSurvey::getParticipants($this->group->getOnlineForm()->getOnlineFormID());
						$arrParticipantID = OnlineSurvey::getarrParticipantID();
						if (!in_array($this->loggedUserID, $arrParticipantID)){
							$grpInfoTemplate->set("grpSurveyLabel", "Online Survey" );
							$grpInfoTemplate->set("grpSurveyLink", "onlineform.php?formID=" . $this->group->getOnlineForm()->getOnlineFormID());
						}
					}
				}
			
			}
			else {
			
				$grpInfoTemplate->set("grpAdmin", $this->group->getAdministrator());
			
				// >>>> IF FUN GROUP, SET LINK FOR ADD and VIEW PHOTOS
				$grpInfoTemplate->set("grpAddPhotoLabel", "Add Photos" );
				$grpInfoTemplate->set("grpAddPhotoLink", "photomanagement.php?cat=fungroup&action=add&genID=" . $this->groupID );
				if (NULL != $this->group->getPhotos()) {
					$grpInfoTemplate->set("grpViewPhotoLabel", "View Photos" );
					$grpInfoTemplate->set("grpViewPhotoLink", "photomanagement.php?cat=fungroup&action=view&travelerID=" . $this->loggedUserID . "&genID=" . $this->groupID );
				}
			
			}
			
			$this->IncGroupInfo = $grpInfoTemplate->fetch("tpl.IncGroupInfo.php");	
		
		}
	
		function prepareGroupMembers(){
			require_once('travellog/model/Class.SqlCriteria2.php');
			require_once('travellog/model/Class.MemberPeer.php');
			
			$criteria = new SqlCriteria2();
			$criteria->addSelectTable('tblTraveler');
			$criteria->add(MemberPeer::TRAVELER_ID, 'tblTraveler.travelerID');
			//$criteria->add(MemberPeer::TRAVELER_ID, 'SELECT travelerID FROM tblGrouptoAdvisor', SqlCriteria2::NOT_IN);
			$criteria->add('tblTraveler.active', 1);
			$criteria->add('tblTraveler.isSuspended', 0);
			$criteria->add('tblTraveler.deactivated', 0);
			//$criteria->add('tblTraveler.travelerID',$this->group->getAdministratorID(),SqlCriteria2::NOT_EQUAL);
			
			$props = array();
			$props['gID'] = $this->groupID;
			$props['is_admin_logged'] = $this->isAdminLogged;
			$props['logged_user_id'] = $this->loggedUserID;
			
			if (GROUP::ADMIN == $this->grpDiscrim AND $this->group->isParent()) {
				$arrAdmin = $this->group->getStaffofSubGroups($_list = false,$_vars = array(),$prioritizeStaffWithPhotos=TRUE);
				// merge administrator to the superstaff/admin list
				$arrAdmin = array_merge(array($this->group->getAdministrator()), $arrAdmin);
				$props['staff'] = $arrAdmin;
			}
			else if (GROUP::ADMIN == $this->grpDiscrim) {
				$arrAdmin = $this->group->getStaff($_list=false,$_subgrouponly=false,$_prioritizeStaffWithPhotos=TRUE);
				// merge administrator to the superstaff/admin list
				$arrAdmin = array_merge(array($this->group->getAdministrator()), $arrAdmin);
				$props['staff'] = $arrAdmin;
			}
			
			//include advisor as staff +1 since advisor is not in tblGrouptoFacilitator
			$props['staff_count'] = (GROUP::ADMIN == $this->grpDiscrim) ? count($props['staff']) : 0;
			$props['members_count'] = ($this->group->getMembersCount($criteria) - $props['staff_count']);
			$props['show_members_count'] = (12 < $props['members_count']) ? true : false;
			$props['show_staff_count'] = (6 < $props['staff_count']) ? true : false;

			$props['is_parent_group'] = $this->isParent;
			
			$this->hasStaff = ($props['staff_count']) ? TRUE : FALSE;
			$this->hasMembers = ($props['members_count']) ? TRUE : FALSE;
			
			$members_tpl  = new Template();
			$members_tpl->set_path('travellog/views/');
				
			if (0 < $props['members_count'] || $this->isAdminLogged) {		
				$cond = new Condition;
				$cond->setAttributeName("tblTraveler.latestlogin");
				$cond->setOperation(FilterOp::$ORDER_BY_DESC);
				$order = new FilterCriteria2();
				$order->addCondition($cond);
				
				$filter = NULL;
				
				$limit  = new RowsLimit(12, 0);
				$props['members'] = $this->group->getMembers($limit,$filter,$order,$count=false,$prioritizeMembersWithPhotos=TRUE);
	 
				$props['members'] = array_slice($props['members'], 0, 12);
				//usort($props['members'], array('Group','orderByName'));	// sort the members by name
				
 		    	$member_tpl = new Template();
 		    	$member_tpl->setTemplate('travellog/views/group_home/tpl.IncMembers.php');
 		    	$member_tpl->setVars($props);				
				$members_tpl->set('members_template', $member_tpl);
			}
				
			if (GROUP::ADMIN == $this->grpDiscrim && ($this->isAdminLogged || $props['staff_count']) ) {
				/*require_once("travellog/model/Class.Advisor.php");
				$props['staff'] = array_merge($props['staff'],array(new Advisor($this->group->getAdministratorID())));*/
					 
				$props['staff'] = array_slice($props['staff'], 0, 6);
				//usort($props['staff'], array('Group','orderByName')); // sort staff by name
				
				$staff_tpl = new Template();
				$staff_tpl->setTemplate('travellog/views/group_home/tpl.IncStaff.php');
				$staff_tpl->setVars($props);
				$members_tpl->set('staff_template', $staff_tpl);
			}
			
			$this->IncGroupMembers = $members_tpl->fetch('tpl.IncGroupMembers.php');	
		}
		
		function prepareGroupSurveys(){
			$grpSurveyTemplate = clone $this->main_tpl;
			$grpSurveyTemplate->set_path("travellog/views/");
			//if admin is logged, show all the surveys of the group.
			require_once('travellog/model/formeditor/Class.SurveyParticipant.php');
			require_once('travellog/model/formeditor/Class.SurveyForm.php');
			//if the logged traveler is not the administrator, show only the surveys that the traveler is allowed to participate relative to the current
			//group being viewed.
			$viewableSurveys = ( $this->isAdminLogged 
				? SurveyForm::getGroupSurveyForms($this->group->getGroupID()) 
				: SurveyForm::getUnparticipatedSurveyFormsOfGroupByTraveler($this->group->getGroupID(),isset($this->loggedUser)? $this->loggedUser->getTravelerID() : 0)
			);
			$grpSurveyTemplate->set('isAdminLogged',$this->isAdminLogged);
			$grpSurveyTemplate->set('group',$this->group);
			$grpSurveyTemplate->set('surveys',$viewableSurveys);
			$grpSurveyTemplate->set('is_parent_group',$this->isParent);
			if(count($viewableSurveys) || ($this->isAdminLogged && $this->group->isParent()) ){
				$this->IncGroupSurveys = $grpSurveyTemplate->fetch("tpl.IncGroupSurveys.php");
			}
		}
		
		function prepareGroupCalendar(){
			//Template::includeDependentJs('/min/f=js/jquery.groupEvent.js',array('bottom'=>true));
			$rowslimitnum = 5;
			$rowslimit = new RowsLimit($rowslimitnum, 0);
		
			//original code	
			$cal = new Calendar($this->group);
			if (TRUE ==  $this->isLogged)
				$cal->setPrivacyContext($this->loggedUser);							// set the current user logged in the system
		
			$events = $cal->getEvents(null,date("Y-m-d"));
			$hasRetriedGetEvents = $cal->hasRetriedGetEvents();
			
			$this->hasEvents = count($events) ? TRUE : FALSE;
			
			if ( (TRUE == $this->isMemberLogged || TRUE ==  $this->viewPrefCalendar ) &&  ( (0 < count($events) && !$hasRetriedGetEvents) || $this->isAdminLogged ) ) {
			
				$grpCalendarTemplate = new Template();
				$grpCalendarTemplate->set_path("travellog/views/");
				$grpCalendarTemplate->set("isAdminLogged",$this->isAdminLogged);
				$grpCalendarTemplate->set("grpID",$this->groupID);
				
				$countAllEvents = count($events);
			
				if (count($events)){
				
					if (count($events) > $rowslimitnum){
						
						$events = array_slice($events, 0, $rowslimitnum);					
					}
				
					$grpCalendarTemplate->set("grpCalendarViewLink",true);
				}
			
				$grpCalendarTemplate->set("grpCalendar",$events);
				$grpCalendarTemplate->set("countAllEvents",$countAllEvents);
				$grpCalendarTemplate->set("is_parent_group",$this->isParent);
				$grpCalendarTemplate->set("hasRetriedGetEvents",$hasRetriedGetEvents);
			
			
				$this->IncGroupCalendar = $grpCalendarTemplate->fetch("tpl.IncGroupCalendar.php");
			
				$this->viewCalendar = true;
			}	
					
		}
		
		/**
		 * Handles the preparation for discussion board.
		 * 
		 * Rules for viewing discussion board in the group home page.
		 * 	General rules:
		 * 		1. Archive discussions are viewable to admin(administrator, staff) only.
		 * 		2. Archive discussions are not replyable.		
		 * 
		 * 	SubGroup:
		 * 		1. Topic::PUBLIC_SETTING
		 * 			1.1 If there are no discussions, only the members of the root group can view.
		 * 			1.2 Only the members of the root group can reply.
		 * 		2. Topic::SUBGROUP_PUBLIC_SETTING
		 * 			2.1 If there are no discussions, only the members of the subgroup can view.
		 * 			2.2 Only the members of the subgroup can reply.
		 * 		3. Topic::MEMBERS_ONLY_SETTING
		 * 			3.1 Only the members of the root group can view and add reply.
		 * 		4. Topic::SUBGROUP_MEMBERS_ONLY_SETTING
		 * 			4.1 Only the members of the subgroup can view and add reply.
		 * 	
		 * 	Root Group:
		 * 		1. Archive topics are viewable to admin(administrator, staff) only.
		 * 		2. If the members(admin, member) is not logged in, show only topics with public setting
		 * 			privacy settings.
		 * 		3. If there are no topics, only the admin can view.
		 * 		4. If there are topics but there are no discussions, only the members(admin, member) can view. 
		 * 		5. Only the members(admin, members) can reply.		
		 * 
		 * @return void
		 */
		function prepareGroupDiscussions(){
			if(Group::FUN <> $this->group->getDiscriminator()){
				require_once('travellog/model/Class.DiscussionPeer.php');
				require_once('travellog/model/Class.TopicPeer.php');
				require_once('travellog/helper/Class.DiscussionHelper.php');
				require_once('travellog/model/Class.PosterPeer.php');
				
				// root_member_logged - means the current logged user is a member of the root/parent group.
				// first_member_logged - means the current logged user is a direct or first degree member of the subgroup.				
				$params = array();
				$params['group'] = $this->group;
				$params['is_subgroup'] = ($this->group->isParent()) ? false : true;		
				$params['is_admin_logged'] = $this->isAdminLogged;
				$params['group_id'] = $this->group->getGroupID();
				$params['parent_group'] = ($params['is_subgroup']) ? $this->group->getParent() : $this->group;
				$params['is_member_logged'] = ($this->isLogged AND ($this->isAdminLogged OR $this->group->isMember($this->loggedUser))) ? true : false;		
				$params['is_root_member_logged'] = ($this->isLogged AND ($params['is_member_logged'] OR ($params['is_subgroup'] AND $params['parent_group']->isMember($this->loggedUser)))) ? true : false;				
				$params['can_reply'] = $params['is_root_member_logged'];
				$params['discussions_count'] = DiscussionPeer::getGroupDiscussionsCount($this->group, $this->loggedUserID);			
				$params['topics_count'] = TopicPeer::getTopicsCount($this->group, $this->loggedUserID);
				
				$this->hasDiscussions = $params['discussions_count'] ? TRUE : FALSE;
							
				if (!$params['is_admin_logged']) {
					if (0 == $params['discussions_count']) {
						if (!$params['is_member_logged']) {
							return;
						}
						else if (0 == $params['topics_count']) {
							return;
						}
					}
				}
				
				$criteria = new SqlCriteria2();
				$criteria->add(TopicPeer::GROUP_ID, $this->group->getGroupID());
				
				$criteria2 = new SqlCriteria2();
				$criteria2->addLimit(3);
				$criteria2->addOrderByColumn(DiscussionPeer::DATE_UPDATED, SqlCriteria2::DESC);
				if (!$this->isAdminLogged) {
					$criteria2->add(DiscussionPeer::STATUS, DiscussionPeer::ACTIVE);
				}
				
				// Check rules for subgroup
				if ($params['is_subgroup']) {
					$topics = TopicPeer::doSelect($criteria);
					$topic = (0 == count($topics)) ? TopicPeer::createTopicForGroup($this->group) : $topics[0];	
					switch ($topic->getPrivacySetting()) {						
						case TopicPeer::SUBGROUP_PUBLIC_SETTING:
						case TopicPeer::SUBGROUP_MEMBERS_ONLY_SETTING:
							if ($params['is_member_logged']) {
								$params['can_reply'] = true;
							}
						break;
					}						
					
					$params['topic_id'] = $topic->getID();
					$criteria2->addLimit(5);
					$params['recent_discussions'] = array();
					$params['discussions'] = $topic->getDiscussions($criteria2);		
					$params['discussion_helper'] = new DiscussionHelper($this->isAdminLogged);
					$params['discussion_helper']->setTemplate('tpl.IncDiscussion3.php');
					foreach ($params['discussions'] as $discussion) {
						$params['discussion_helper']->addDiscussion($discussion);
						
						if (1 < $discussion->getPostsCount()) {
							$params['recent_discussions'][] = $discussion;
						}
					}
					
					$tpl = new Template;
					$tpl->setTemplate('travellog/views/discussionboard/tpl.ViewAjaxSubGroupHomePageDiscussions.php');
					$tpl->set('discussion_helper', $params['discussion_helper']);
					$tpl->doNotUseMainTemplate();
					$params['discussions_template'] = $tpl;
				} // end of subgroup rules checking
				
				// Validates the rules for root group
				else {
					$criteria->addLimit(3);
					$criteria->addOrderByColumn(TopicPeer::DATE_UPDATED, SqlCriteria2::DESC);
					if (!$params['is_admin_logged']) {
						$criteria->add(TopicPeer::STATUS, TopicPeer::ACTIVE);
						if (!$params['is_member_logged']) {
							$criteria->add(TopicPeer::PRIVACY_SETTING, TopicPeer::PUBLIC_SETTING, SqlCriteria2::IN);
						}
					}
					$topics = TopicPeer::doSelect($criteria);						
					
					$params['discussion_helpers'] = array();
					$params['recent_discussions'] = array();
					$params['topics'] = array();
					foreach ($topics as $topic) {
						$discussions = $topic->getDiscussions($criteria2);
						if (0 < count($discussions)) {
							$params['topics'][] = $topic; 
							$params['discussion_helpers'][$topic->getId()] = new DiscussionHelper($this->isAdminLogged);
							$params['discussion_helpers'][$topic->getId()]->addDiscussions($discussions);
							
							foreach ($discussions as $discussion) {
								if (1 < $discussion->getPostsCount()) {
									$params['recent_discussions'][] = $discussion;
								}
							}
						}
					}
					
					$params['added_discussions'] = DiscussionPeer::getDiscussionsAddedToKnowledgeBase($params['group']);
					$params['recent_discussions'] = array_merge($params['recent_discussions'], $params['added_discussions']);
					usort($params['recent_discussions'], array("DiscussionPeer", "compareDiscussion"));	
				} // end of root group rules checking		
				
				$tpl_file = ($params['is_subgroup']) ? 'tpl.IncSubGroupMiniDiscussionBoard.php' : 'tpl.IncRootGroupMiniDiscussionBoard.php';
				
				$tpl = new Template;
				$tpl->setPath('travellog/views/discussionboard/');
				$tpl->setVars($params);
				
				$this->IncGroupDiscussions = $tpl->fetch($tpl_file);
			}
		}
	
		function prepareGroupFiles(){
		
			$priavcyPref = $this->isMemberLogged ? 1 : 2;
			$privacyPref = $this->isAdminLogged ? 0 : $priavcyPref;
			$grpResourceFiles = $this->group->getResourceFiles(NULL, $privacyPref);
			
			$this->hasResourceFiles = $grpResourceFiles ? TRUE : FALSE;
			
			if ( (TRUE == $this->isMemberLogged || TRUE == $this->viewPrefResourceFiles ) &&  (0 < count($grpResourceFiles) || $this->isAdminLogged ) ) {
			
				$grpFilesTemplate = new Template();
				$grpFilesTemplate->set_path("travellog/views/");
				$grpFilesTemplate->set("isAdminLogged",$this->isAdminLogged);
				$grpFilesTemplate->set("grpID",$this->groupID);
				
				$grpFilesTemplate->set("grpResourceFiles",$grpResourceFiles);
				
				$grpFilesTemplate->set("is_parent_group",$this->isParent);
			
				$this->IncGroupFiles = $grpFilesTemplate->fetch("tpl.IncGroupFiles.php");	
				$this->viewResourceFiles = true;
			}			
		
		}
	
		function prepareGroupPhotoAlbums(){
			
			require_once('travellog/model/Class.PhotoAlbumAdapter.php');
			$PhotoAlbumAdapter = new PhotoAlbumAdapter();
			$PhotoAlbumAdapter->setLoguserID($this->loggedUserID);
		
			$grpPhotoAlbums = $PhotoAlbumAdapter->getAlbums($this->group);
			
			$featuredAlbums = $PhotoAlbumAdapter->getFeaturedAlbums($this->group);
			if( count($featuredAlbums) ){
				$grpPhotoAlbums = $featuredAlbums;
			}
			
			$this->hasPhotos = count($grpPhotoAlbums) ? TRUE : FALSE;
			
			//$grpPhotoAlbums = $this->group->getPhotoAlbums();
			$grpPhotoAlbumsTemplate = new Template();
			$grpPhotoAlbumsTemplate->set_path("travellog/views/");
			$grpPhotoAlbumsTemplate->set("isAdminLogged",$this->isAdminLogged);
			$grpPhotoAlbumsTemplate->set("grpID",$this->groupID);
			$grpPhotoAlbumsTemplate->set("loggedUserID",$this->loggedUserID);
			$grpPhotoAlbumsTemplate->set("grpPhotoAlbums",$grpPhotoAlbums);

			$grpPhotoAlbumsTemplate->set("is_parent_group",$this->isParent);
		
			/*
			$photos = array();
			if(count($grpPhotoAlbums)>0)
				foreach($grpPhotoAlbums as $album)
					if(count($album->getPhotos())>0)
						$photos[] = $album->getPhotos();	
			
			if($this->isAdminLogged || count($photos) >0)
				$this->IncGroupPhotoAlbums = $grpPhotoAlbumsTemplate->fetch("tpl.IncGroupPhotoAlbum.php");	
			*/
		
			$photos = array();
			if($grpPhotoAlbums){
				foreach($grpPhotoAlbums as $i =>$album){
					if($album['context']=="photoalbum"){
						if($album['numphotos'] > 0){
							//if ($this->loggedUserID == $album['creator'] || $album['publish'])
								$photos[] = count($photos)+$album['numphotos'];	
						}
					}else{
						if ($this->loggedUserID == $album['creator'] || $album['publish']){
								$photos[] = count($photos)+$album['numphotos'];	
						}
					}
				}
			}
					
			if($this->isAdminLogged || count($photos) >0){
				$this->IncGroupPhotoAlbums = $grpPhotoAlbumsTemplate->fetch("tpl.IncGroupPhotoAlbums.php");	
				
				$this->isShowDependentjScript = count($photos) >0?true:false;
				
				require_once("travellog/UIComponent/Photo/facade/PhotoUIFacade.php");
				$Photo = new PhotoUIFacade();
				$Photo->setContext('group');
				$Photo->setGenID($this->groupID);
				//$Photo->setLoginID($this->loggedUser?$this->loggedUser->getTravelerID():0);
				$Photo->setLoginID(0);
				$Photo->setGroupHasPhotosInAlbum($this->isShowDependentjScript);
				$Photo->build();
				
			}else{
				$this->isShowDependentjScript = false;
			}
		}
		
		function prepareGroupVideos() {
			if (GROUP::ADMIN == $this->group->getDiscriminator()) {
				if(!Template::isExistDependent('/min/g=GroupPhotoUIOwnAlbJs')){
					Template::includeDependentJs('/min/f=js/thickbox.js', array('bottom'=> true));
					Template::includeDependentCss('/min/f=css/Thickbox.css');
				}
				
				$recentVideos = $this->group->getGroupVideos();
				$this->hasVideos = count($recentVideos) ? TRUE : FALSE;
				
				if ($this->isAdminLogged || $this->isMemberLogged || 0 < count($recentVideos)) {
					$tpl = new Template();
					$tpl->set('grpID', $this->groupID);
					$tpl->set('videos', $recentVideos);
					$tpl->set('isAdminLogged', ($this->isAdminLogged || $this->isMemberLogged));
					
					$tpl->set('is_parent_group', $this->isParent);
					
					if ($this->isAdminLogged || $this->isMemberLogged){
						$tpl->set('label', 'Manage Videos');
					}
					else{
						if (4 < count($recentVideos)){
							$tpl->set('label', 'View All Videos');
						}
					}
		
					$this->IncGroupRecentVideos = $tpl->fetch('travellog/views/tpl.IncGroupRecentVideos.php');
					$this->viewRecentVideos = true;
				}
			}
		}
		
		function prepareSubGroups(){
			$origCountSubGroups = $this->group->getMySubGroupsCount();
			$this->hasSubGroups = $origCountSubGroups ? TRUE : FALSE;
			if ( TRUE == $this->group->isParent() && (TRUE == $this->isMemberLogged || TRUE == $this->viewPrefGroups ) &&  (0 < $origCountSubGroups  || $this->isAdminLogged ) ) {
				/*require_once("travellog/views/subgroups/Class.CategoryViewHandler.php");
				$categs = array();
				$groups =  $this->group->getFeaturedSubGroups();
				$categories = $this->group->getFeaturedSubGroupCategories();
				while($categories->hasNext()) {
					$category = $categories->next();
					$categs[$category->getCategoryID()] = new CategoryViewHandler($category);
				}
				while($groups->hasNext()){
					$group = $groups->next();
					$categs[$group->getCategoryID()]->addSubGroup($group, $this->group);
				}*/
				require_once("travellog/model/Class.GroupPeer.php");
				require_once("travellog/model/Class.SubGroup.php");
				$featuredGroups = GroupPeer::getFeaturedGroupsAsSubGroup($this->group);
				$origFeaturedCount = count($featuredGroups);
				if( !$origFeaturedCount ){
					$recentN = $this->isAdminLogged ? 6 : 5;
					$featuredGroups = GroupPeer::getRecentSubgroupsAsSubGroup($this->group, $includeFeatured=FALSE, $recentN);
				}
				
				$link_label = ($origCountSubGroups > 5) ? "View All ". $origCountSubGroups." Subgroups" : "";
				$link_label = ($this->isAdminLogged) ? "Manage Subgroups" : $link_label;
					
				$tpl = new Template();
				$tpl->set("isAdminLogged",$this->isAdminLogged);
				$tpl->set("grpID",$this->groupID);
				$tpl->set("origCountSubGroups",$origCountSubGroups);
				//$tpl->set("featuredCount", $groups->size());
				$tpl->set("origFeaturedCount", $origFeaturedCount );
				$tpl->set("featuredGroups", $featuredGroups);
				$tpl->set("loggedUser", $this->loggedUser);
				//$tpl->set("categories", implode(" ", $categs));
				$tpl->set("link_label", $link_label);
				$tpl->set("parent", $this->group);
				$tpl->set("loggedUser", $this->loggedUser);
				$tpl->set('isLogged', $this->isLogged);
				if( $this->isAdminLogged ){
					$this->IncGroupSubGroups = $tpl->fetch("travellog/views/tpl.IncGroupProfileSubGroupList2.php");
				}else{
					$this->IncGroupSubGroups = $tpl->fetch("travellog/views/tpl.IncGroupProfileSubGroupList.php");
				}
				$this->viewSubGroups = true;
			}
		
		}
	
		function prepareGroupJournals(){

			require_once('travellog/views/Class.JournalsComp.php');
			require_once('travellog/model/Class.Featured.php');
			require_once('travellog/dao/Class.GroupApprovedJournals.php');
			
			$vm = array('VIEWMODE'=>'GROUP','SECURITY_CONTEXT'=>'PUBLIC');			//default view mode
			
			$jCompContextAr =	JournalsComp::convertURLParamToSetupArray();
			
			if (count($jCompContextAr) == 0){
				$jCompContextAr = array('VIEWMODE'=>'GROUP','SECURITY_CONTEXT'=>'PUBLIC');			//default view mode
			
				if ($this->loggedUserID > 0)
					$jCompContextAr['SECURITY_CONTEXT'] = 'LOGIN';
			}	
			
			if(isset($jCompContextAr['TYPE']) && $jCompContextAr['TYPE'] == 'ARTICLES')
				$jCompContextAr['CURRENT_PAGE'] = NULL;
			
			$jCompContextAr['SUB_FEATURED'] = false;
			if($this->group->getParentID() > 0){
				$featured = Featured::getFeaturedTravelIDsInGroup($this->group->getGroupID());
				$approved = GroupApprovedJournals::getApprovedTravelIDsInGroup($this->group->getGroupID());
				if(count($featured) <= 0){ // if there are no featured journals show approved journals
					$jCompContextAr['VIEW_TAB'] = "STAFF_ADMIN_APPROVED";
					$jCompContextAr['SUB_FEATURED'] = true;
				}
			
				$journalComp = new JournalsComp($jCompContextAr,$this->group->getGroupID(),$this->loggedUserID);
							
				$this->IncGroupJournals = $journalComp;
				$this->viewFeaturedJournals = (0 < (count($featured) || count($approved)) || $this->isAdminLogged) ? true : false;
			}else{
				$journalComp = new JournalsComp($jCompContextAr,$this->group->getGroupID(),$this->loggedUserID);

				$this->IncGroupJournals = $journalComp;
				//$this->viewGroupJournals = (0 < count($this->group->getTravels()) || $this->isAdminLogged ) ? true : false;
				$this->viewFeaturedJournals = (0 < count(Featured::getFeaturedTravelIDsInGroup($this->group->getGroupID())) || $this->isAdminLogged) ? true : false;
			}
		}
		
		function prepareGroupArticles(){
			require_once('travellog/views/Class.ArticleComp.php');
			require_once('travellog/views/Class.JournalsComp.php');
			
			$setup = array(
				'OWNER' => $this->isAdminLogged,
				'CONTEXT' => "group",
				'DATA' => $this->group->getGroupID(),
				'SUBJECT' => $this->group,
				'VIEWMODE' => "GROUP"
			);
			
			$arr_setup = ArticleComp::convertURLParamToSetupArray();
			if (count($arr_setup) == 0){
				$arr_setup = array('VIEWMODE'=>'GROUP','SECURITY_CONTEXT'=>'PUBLIC');			//default view mode
			
				if ($this->loggedUserID > 0)
					$arr_setup['SECURITY_CONTEXT'] = 'LOGIN';
			}	
			
			if(!isset($arr_setup['TYPE']))
				$arr_setup['PAGE'] = NULL;
				
			$setup = array_merge($setup,$arr_setup);							
			
			$articleComp = new ArticleComp($setup);
			$this->IncGroupArticles = $articleComp;		
			$this->viewGroupArticles = ($articleComp->getArticlesCount() > 0) ? true : false;
		}
	
		public function prepareMessageCenter() {
			require_once 'travellog/model/messageCenter/manager/Class.gaMessagesManagerRepository.php';
			require_once('travellog/model/Class.SiteContext.php');

			$group = gaGroupMapper::getGroup($this->group->getGroupID());
			
			$siteContext = SiteContext::getInstance();
			$siteAccessType = $siteContext->isInCobrand() ? 2 : 1;
			
			//$messagesManager = gaMessagesManagerRepository::load($group->getAdministratorID(), $siteAccessType);
			$messagesManager = gaMessagesManagerRepository::loadGroup($group->getAdministratorID(), $siteAccessType, $group);
			
			if ($this->isLogged && in_array($_SESSION['travelerID'], $group->getOverseerIDs()))
				$isPowerful = true;
			else
				$isPowerful = false;
				
			$isMessageCenterPublic = $this->isMessageCenterPublic;

			// retrieve messages based on what is to be shown
			$showMessageCenter = false;
			$messages = $messagesManager->getNewsOnly();
			
			if ($isPowerful || (($this->isMemberLogged || $isMessageCenterPublic) && count($messages) > 0))
				$showMessageCenter = true;
			else
				$showMessageCenter = false;

			$numMessagesDisplayed = 5;
			
			//Template::includeDependentJs("/min/f=js/messageCenter/messages.js", array("bottom" => true));

			$template = $this->main_tpl;

			$template->set ( 'isPowerful',           $isPowerful        );
			$template->set ( 'showMessageCenter',    $showMessageCenter );
			$template->set ( 'thereAreMoreMessages', count($messages) > $numMessagesDisplayed ? true : false );
			$template->set ( 'messages',             array_slice($messages, 0, $numMessagesDisplayed) );
			$template->set ( 'snippetLimit',         100                );		// value experimental

			$template->set ( 'showRealName',   $isPowerful );
			$template->set ( 'showReadStatus', $isPowerful );
			
			$template->set ( 'canBeSelected', false );
			$template->set ( 'canBeDeleted',  false );
			
			$template->set ( 'group', $group );
			$template->set ( 'thisIsAClub',  $group->isClub() );

			$template->set ( 'messageCenterView',    $template->fetch('messageCenter/tpl.ViewGroupMessageCenterMini.php') );
		}
	
		function prepareImportantLinks(){

			$grpImportantLinksTemplate = new ViewImportantLinks();
			$grpImportantLinksTemplate->setIsAdminLogged($this->isAdminLogged);
			$grpImportantLinksTemplate->setGroupId($this->group->getGroupID());
			$grpImportantLinksTemplate->retrieve();
			$this->IncGroupImportantLinks = $grpImportantLinksTemplate;

			$hasContent = (0 < count($grpImportantLinksTemplate->getArrLink())) ? true : false;		
			$this->viewImportantLinks = ($hasContent || $this->isAdminLogged ) ? true : false;
			$this->hasLinks = $hasContent;
		}
		
		/**
		 * initializes the subnavigation to be used
		 */
		function prepareSubNavigation(){
			if (!isset($this->subNavigation))
			  $this->subNavigation = new SubNavigation();
			$this->subNavigation->setContext('GROUP');
			$this->subNavigation->setContextID($this->groupID);
			$this->subNavigation->setLinkToHighlight('GROUP_NAME');
		
			if (GROUP::ADMIN == $this->grpDiscrim )		
				$this->subNavigation->setGroupType('ADMIN_GROUP');
			else
				$this->subNavigation->setGroupType('FUN_GROUP');			
		}
		
		function prepareGroupSteps(){
			require_once('travellog/views/Class.ViewGroupSteps.php');
			$this->viewGroupSteps = new ViewGroupSteps();
			$this->viewGroupSteps->setGroup($this->group);
			$this->viewGroupSteps->setPhotosVideosLink($this->hasPhotos);
			$this->viewGroupSteps->setSteps(array(
				'step1' => $this->group->isProfileComplete(),
				'step2' => $this->hasSubGroups,
				'step3' => $this->hasStaff,
				'step4' => $this->hasSubGroups,
				'step5' => ($this->hasVideos && $this->hasPhotos),
				'step6' => $this->hasResourceFiles,
				'step7' => $this->hasEvents,
				'step8' => $this->hasLinks,
				'step9' => $this->hasDiscussions,
				'step10' => $this->hasMembers
			));
		}
	
		/***
			Added by naldz(01-22-2010)
		***/
		public function prepareGroupTags(){
			if($this->group->isParent()){
				require_once(dirname(__FILE__).'/../../../lib/models/pageTag/GanetPageTagPeer.php');
				if(count(GanetPageTagPeer::retrieveMostPopularGroupTags($this->group->getGroupID(), 1)) > 0){
					Template::includeDependentJs('/min/g=GroupPagesJs');
					Template::includeDependentCss('/min/g=GroupPagesJs');
					$tpl = new Template();
					$tpl->set('groupID', $this->group->getGroupID());
					$this->IncGroupTagCloud = $tpl->fetch("travellog/views/tpl.IncGroupPageTags.php");
				}
			}
		}
			
		function viewGroup(){
			$this->main_tpl->set("grpID"			  ,  $this->groupID);
			$this->main_tpl->set("group"			  ,  $this->group);
			$this->main_tpl->set("grpDiscriminator"	  ,	 $this->grpDiscrim);
			$this->main_tpl->set('copying_notice_component', new GanetGroupCopyingNoticeComponent($this->group));	
			$this->main_tpl->set("isLogged"           ,  $this->isLogged);	
			$this->main_tpl->set("isMemberLogged"     ,  $this->isMemberLogged);	
			$this->main_tpl->set("isAdminLogged"      ,  $this->isAdminLogged);	
		
			$this->main_tpl->set("loggedUser",$this->loggedUser);
			$this->main_tpl->set('mainGroupInfo_view' , $this->mainGroupInfo); // added by nash
			$this->main_tpl->set('grpInfoTemplate'    ,  $this->IncGroupInfo);
			$this->main_tpl->set('memberTemplate'     ,  $this->IncGroupMembers);
			$this->main_tpl->set('grpDiscussionsTemplate', $this->IncGroupDiscussions);
			$this->main_tpl->set('grpCalendarTemplate',  $this->IncGroupCalendar);
			$this->main_tpl->set('grpMsgCenterTemplate',  $this->IncMessageCenter);
			$this->main_tpl->set("isParent", true);
			
			$this->main_tpl->set('groupTagCloud', $this->IncGroupTagCloud);
			
			if (GROUP::ADMIN == $this->grpDiscrim ){		
				$this->main_tpl->set("isParent"            ,  $this->group->isParent());
				$this->main_tpl->set('grpSurveyTemplate',  $this->IncGroupSurveys);
				$this->main_tpl->set('grpFilesTemplate'    ,  $this->IncGroupFiles);
				$this->main_tpl->set('grpSubGroupsTemplate',  $this->IncGroupSubGroups);
				$this->main_tpl->set('grpCustomSectionTemplate',  $this->IncGroupCustomSection);
				
				if ($this->viewFeaturedJournals)
					$this->main_tpl->set('grpJournalsTemplate',  $this->IncGroupJournals);
				
				$this->main_tpl->set('grpPhotoAlbumsTemplate',  $this->IncGroupPhotoAlbums);
				$this->main_tpl->set('grpRecentVideosTemplate',  $this->IncGroupRecentVideos);
				$this->main_tpl->set('grpImportantLinksTemplate',  $this->IncGroupImportantLinks);
				$this->main_tpl->set('viewGroupSteps',  $this->viewGroupSteps);
			}
			
			$obj_session = SessionManager::getInstance();
			
			$this->main_tpl->set('success',NULL);
			if($obj_session->get('custompopup_message')){
				$this->main_tpl->set('success',$obj_session->get('custompopup_message'));
				$obj_session->unsetVar('custompopup_message');
			}
			$this->main_tpl->set('viewGroupArticles',  $this->viewGroupArticles);
			$this->main_tpl->set('grpArticlesTemplate',  $this->IncGroupArticles);
			$this->main_tpl->set("viewCalendar"		 , $this->viewCalendar);	
			$this->main_tpl->set("viewResourceFiles" , $this->viewResourceFiles);
			$this->main_tpl->set("viewSubGroups"	 , $this->viewSubGroups);
			$this->main_tpl->set("viewCustomSection"	 , $this->viewCustomSection);	
			$this->main_tpl->set("viewGroupJournals" , $this->viewGroupJournals);
			$this->main_tpl->set("viewImportantLinks" , $this->viewImportantLinks);
			$this->main_tpl->set("isShowDependentjScript",  $this->isShowDependentjScript);
			$this->main_tpl->set("subNavigation",$this->subNavigation);
			$this->main_tpl->out("tpl.GroupHomepage2.php");	
		
		}
	
	}