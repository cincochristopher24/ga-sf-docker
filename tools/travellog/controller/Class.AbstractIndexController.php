<?php

	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/controller/Class.IController.php");
	require_once("travellog/factory/Class.FileFactory.php");
	
	abstract class AbstractIndexController implements iController{
		
		protected
		
		$obj_session  = NULL,
		
		$file_factory = NULL,
		
		$tplHomePage  = NULL;
		
		function __construct(){
			
			$this->obj_session = SessionManager::getInstance();
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template' , array ( 'path' => '' ));
			
			$this->file_factory->registerClass('RotatingPhoto');
			
			$this->file_factory->registerClass('Featured');
			
			$this->file_factory->registerClass('FeaturedSection'  , array ( 'path' => 'travellog/model/admin/' ));
			
			$this->file_factory->registerClass('GAMapView'        , array('path' => 'travellog/views/'));
			$this->file_factory->registerClass('Map');
			$this->file_factory->registerClass('LocationMarker');
			$this->file_factory->registerClass('TravelLog');
			$this->file_factory->registerClass('Location');
			$this->file_factory->registerClass('MapFrame'         , array('file' => 'Map', 'class' => 'MapFrame'));
			$this->file_factory->registerClass('CurrentTravelers' , array('path' => 'travellog/model/travelers/', 'file' => 'CurrentTravelerModel'));
			
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
		}
		
		function performAction(){
			
			$this->_showHomePage();				
	
		}
		
		
		function _showHomePage(){
			
			$this->file_factory->invokeStaticClass('Template','setMainTemplateVar',array('title', 'GoAbroad Network: Travel Journals, Travel Blogs &amp; Travel Notes from the Travel Community'));
			
			$this->file_factory->invokeStaticClass('Template','setMainTemplateVar',array('metaDescription', 'Travel journals and blogs from the travel community. You too can create FREE travel journals with photos and share them with your families, friends and a growing network of diverse travelers.'));
			
	       $this->file_factory->invokeStaticClass('Template','setMainTemplateVar',array('metaKeywords', '	travel community, travel network, travel online, travel website, travel web site, travel blog, travel note, free travel blogs, travel blogs, free travel blog, personal travel blogs, travel notes, travel journal, travel community website, travel notes blog, free travel journal, free online travel journal, travel journal online, blogs, journals, travel diaries, travel journals, travel diary, traveler, travelers community, travelers community, traveler community, free travel journals, free online travel journals, online travelers community, travel review, 
	travel comments, travel observations, trip journals,  travelogues, travel reviews, travel guide, travel advice, travel photos, travel maps'));
			
			$this->file_factory->invokeStaticClass('Template','setMainTemplateVar',array('isInHomepage', true));
			
			$this->tplHomePage = $this->file_factory->getClass('Template');
			$this->tplHomePage->set_path("travellog/views/");
			
			// ---------  rotating photo  ------------ //
			require_once("travellog/model/Class.RotatingPhoto.php");
			
			$rotatingPhotoClass = $this->file_factory->getClass('RotatingPhoto');
			$rotatingPhoto = $rotatingPhotoClass->getRandomRotatingPhoto();
			
			$this->tplHomePage->set('rotatingPhoto', $rotatingPhoto);
			// ---------  rotating photo  ------------ //
			
			//temporary
			require_once("travellog/model/admin/Class.FeaturedSection.php");
			
			// ---------  featured photo ------------- //
			$featPhotoSection = $this->file_factory->getClass('FeaturedSection', array(FeaturedSection::$PHOTO));
			$featPhoto = $featPhotoSection->getFeaturedItems();
			
			$this->tplHomePage->set('featPhoto', $featPhoto);
			$this->tplHomePage->set('featPhotoSection',$featPhotoSection);
			// ---------  featured photo ------------- //


			// --------- featured group ------------- //
			$featGroupSection =  $this->file_factory->getClass('FeaturedSection', array(FeaturedSection::$GROUP));
			$featGroup = $featGroupSection->getFeaturedItems();
		
			$this->tplHomePage->set('featGroup', $featGroup);
			$this->tplHomePage->set('featGroupSection',$featGroupSection);
			// --------- featured group ------------- //


			// --------- featured travel ------------- //
			$featTravelSection = $this->file_factory->getClass('FeaturedSection', array(FeaturedSection::$JOURNAL));
			$featTravel = $featTravelSection->getFeaturedItems();
		
			$this->tplHomePage->set('featTravel', $featTravel);
			$this->tplHomePage->set('featTravelSection',$featTravelSection);

				// get locations of featured travels
				$featTravelLoc = array();
				for ($idxfeattravel=0; $idxfeattravel < count($featTravel); $idxfeattravel++) {
					$eachfeattravel = $featTravel[$idxfeattravel];
					$locationname = array();
					if (count($eachfeattravel->getTrips())):
						$trips = $eachfeattravel->getTrips();
						foreach($trips as $trip):
							if (null != $trip->getLocation())
								$locationname[] = $trip->getLocation()->getCountry()->getName();
							continue;
						endforeach;				
					endif;
					$featTravelLoc[$idxfeattravel] = $locationname;
				}

				$this->tplHomePage["featTravelLoc"] = $featTravelLoc;
			// --------- featured travel ------------- //
			
			
			// --------- latest log  ----------- //
		    $featEntrySection = $this->file_factory->getClass('FeaturedSection', array(FeaturedSection::$JOURNAL_ENTRY));
		    $latestLog = $featEntrySection->getFeaturedItems();
		    $this->tplHomePage->set('latestLog', $latestLog);
			$this->tplHomePage->set('featEntrySection', $featEntrySection);
			// --------- latest log  ----------- //
			
			
			// ------- featured traveler ------------ //
			$featTravelerSection = $this->file_factory->getClass('FeaturedSection', array(FeaturedSection::$TRAVELER));
			$featTraveler = $featTravelerSection->getFeaturedItems();
			$this->tplHomePage->set('featTraveler', $featTraveler);
			$this->tplHomePage->set('featTravelerSection', $featTravelerSection);
			// ------- featured traveler ------------ //
			
			
			// if not logged in, manage Login and LogOut 
			if (0 == $this->obj_session->get('travelerID')) {
				$this->_manageLogin();
				$this->_manageLogout();
				
			} else { // show newest members instead of login box
				$this->_showNewestMembers();
			}	
			
			// GET MAP
			//$obj_current_traveler = $this->file_factory->getClass('CurrentTravelers');
			//$arr_locations        = $obj_current_traveler->GetTravelersCountryWithCurrentLocation();
			//$arr_locations = $this->file_factory->invokeStaticClass( 'TravelLog', 'getCountriesWithLogs' );
			//foreach( $arr_locations as $location ){
			//	$obj_location = $this->file_factory->getClass('Location');
			//	$obj_location->setLocationID($location['locationID']);
			//	$obj_location->setName($location['name']);
			//	$arr[] = $obj_location;
			//}
			/** Removed By: daf Nov14.2008 ; GoogleMaps no longer needed in homepage based on mockup
			 
			$arr                           = $this->file_factory->invokeStaticClass( 'TravelLog', 'getCountriesWithLogs' );
			$arr_contents                  = array(); 

			$arr_contents['height']        = 280;
			$arr_contents['width']         = 264;
			$arr_contents['title']         = 'Travel Destinations';

			$arr_contents['col_locations'] = $arr;
			$obj_map_view                  = $this->file_factory->getClass('GAMapView');
			$obj_map_view->setContents($arr_contents);
			
			$this->tplHomePage->set('obj_map_view', $obj_map_view);
			
			*/
			
			require_once("travellog/UIComponent/Photo/facade/PhotoUIFacade.php");
			$Photo = new PhotoUIFacade();
			$Photo->setContext('home');
			$Photo->setGenID(0);
			$Photo->setLoginID(0);
			$Photo->build();		
			$this->tplHomePage->set('loggedIn', $this->obj_session->getLogin());
			$this->tplHomePage->out("tpl.Homepage.php");
			
		}
		
		function _manageLogin(){
			
			$obj_factory = ControllerFactory::getInstance();
			$this->tplHomePage['qryLogin'] = $obj_factory->createController('Login')->performAction();
		
		}
		
		function _manageLogout(){
			
			if( 0 == strcmp($_SERVER["SERVER_NAME"],"www.goabroad.net") )
				$this->tplHomePage["serve_dir"] = "www";
			else
				$this->tplHomePage["serve_dir"] = "dev";	 
		
			if( isset($_GET["logout"]) && strlen(substr(trim($_GET["logout"]),6,strlen($_GET["logout"]))) )
				$this->tplHomePage["logoutMe"] = substr(trim($_GET["logout"]),6,strlen($_GET["logout"]));
			else
				$this->tplHomePage["logoutMe"] = 0;
				
		}
		
		function _showNewestMembers(){
			
			$this->file_factory->registerClass('RowsLimit');
			$this->file_factory->registerClass('Traveler');
			
			$rowslimit = $this->file_factory->getClass('RowsLimit', array(3,0));
			$newestmembers = $this->file_factory->invokeStaticClass('Traveler' , 'getNewestMembers' , array ( $rowslimit, $this->obj_session->get('travelerID') ));					// neri added logged travelerID: 11-14-08
			
			$this->tplHomePage->set('newestMembers', $newestmembers);
			
		}
		
	}
?>