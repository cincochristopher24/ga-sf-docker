<?php
	
require_once("travellog/controller/Class.AbstractSPOAuthLoginController.php");

class GANETSPOAuthLoginController extends AbstractSPOAuthLoginController{
	
	public function __construct(){
		parent::__construct();
		
		$this->file_factory->registerClass('Traveler', array("path" => "travellog/model"));
	}
	
	public function loginUser($travelerID=0,$oauth_api=NULL){
		if( $this->startSession($travelerID) ){
			$oauth_api->login();
			$oauth_api->clearCredentials();
			header("location: /passport.php");
			exit;
		}
		return FALSE;
	}
}