<?php
	require_once('travellog/factory/Class.FileFactory.php');
	
	abstract class AbstractDiscussionController {
		
		function __construct(){
			$file_factory = FileFactory::getInstance();
			$file_factory->registerClass('SubNavigation');
			$file_factory->registerClass('Traveler');
			$file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
			$file_factory->registerClass('Template', array('path' => ''));
			$file_factory->registerTemplate('LayoutMain');
			$file_factory->setPath('CSS', '/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			$file_factory->getClass("Template");
			Template::includeDependentCss("/css/vsgStandardized.css");
			Template::includeDependentCss("/css/modalBox.css");
			Template::includeDependentCss('/css/Thickbox.css');
			Template::includeDependentJs('/js/thickbox.js');		
		}
		
		/**
		 * Shows the add form of discussion.
		 * 
		 * @param int $id The ID of the topic in which the discussion is made for.
		 * 
		 * @return void
		 */
		function add($id = 0){
			require_once('travellog/action/discussion/Class.Add.php');
			
			$params = array();
			$params['topic_id'] = $id;
			
			$action = new Add($params);
			$action->execute();			
		}
		
		/**
		 * Shows the posts of a discussion.
		 * 
		 * @param int $id The ID of the discussion.
		 * 
		 * @return void
		 */
		function posts($id = 0){
			require_once('travellog/action/discussion/Class.Posts.php');
			
			$params = array();
			$params['discussion_id'] = $id;
			
			$action = new Posts($params);
			$action->execute();
		}
		
		/**
		 * Saves the newly created discussion.
		 * 
		 * @return void
		 */
		function save(){
			require_once('travellog/action/discussion/Class.Save.php');
			
			$action = new Save();
			$action->execute();
			exit;			
		}
		
		/**
		 * Activates the given discussion.
		 * 
		 * @param int $id The ID of the discussion to be activated.
		 * 
		 * @return void
		 */
		function activate($id = 0){
			require_once('travellog/action/discussion/Class.Activate.php');
			
			$params = array();
			$params['discussion_id'] = $id;
			$action = new Activate($params);
			$action->execute();
			exit;
		}
		
		/**
		 * Archives the given discussion.
		 * 
		 * @param integer $discussionID The ID of the discussion to be archived.
		 * 
		 * @return void
		 */
		function archive($id = 0){
			require_once('travellog/action/discussion/Class.Archive.php');
			
			$params = array();
			$params['discussion_id'] = $id;
			$action = new Archive($params);
			$action->execute();
			exit;
		}
		
		/**
		 * Adds the given discussion to knowledge base.
		 * 
		 * @param int $id The ID of the discussion to be added to the knowledge base.
		 * 
		 * @return void
		 */
		function addToKnowledgeBase($id = 0){
			require_once('travellog/action/discussion/Class.AddToKnowledgeBase.php');
			
			$params = array();
			$params['discussion_id'] = $id;
			$action = new AddToKnowledgeBase($params);
			$action->execute();
			exit;
		}
		
		/**
		 * Removes the given discussion from the knowledge base.
		 * 
		 * @param int $id 	The ID of the discussion to be removed from the knowledge base.
		 * @param int $mode The confirmation type mode
		 * 
		 * @return void
		 */
		function removeFromKnowledgeBase($id = 0, $mode = 0){
			require_once('travellog/action/discussion/Class.RemoveFromKnowledgeBase.php');
			
			$params = array();
			$params['discussion_id'] = $id;
			$params['mode'] = $mode;
			$action = new RemoveFromKnowledgeBase($params);
			$action->execute();
			exit;
		}
		
		/**
		 * Features the given discussion.
		 * 
		 * @param int 	 $discussionID	The ID of the discussion to be featured.
		 * @param int 	 $mode					The response view mode of the action.
		 * 
		 * @return void
		 */
		function feature($discussion_id = 0, $mode = 0){
			require_once('travellog/action/discussion/Class.Feature.php');
			
			$params = array();
			$params['discussion_id'] = $discussion_id;
			$params['mode'] = $mode;
			$action = new Feature($params);
			$action->execute();
			exit;
		}
		
		/**
		 * Features the given discussion.
		 * 
		 * @param int 	 $discussionID	The ID of the discussion to be featured.
		 * @param int 	 $mode					The response view mode of the action.
		 * 
		 * @return void
		 */
		function unfeature($discussion_id = 0, $mode = 0){
			require_once('travellog/action/discussion/Class.Unfeature.php');
			
			$params = array();
			$params['discussion_id'] = $discussion_id;
			$params['mode'] = $mode;
			$action = new Unfeature($params);
			$action->execute();
			exit;
		}
		
		function edit($discussion_id = 0) {
			require_once('travellog/action/discussion/Class.Edit.php');
			
			$params = array();
			$params['discussion_id'] = $discussion_id;
			$action = new Edit($params);
			$action->execute();
			exit;			
		}
		
		/**
		 * Activates the __call magic function. 
		 */
		function __call($method, $arguments){
			require_once('travellog/action/Class.ControllerAction.php');
			
			ControllerAction::showFileNotFound();
		}
		
	}