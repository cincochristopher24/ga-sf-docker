<?php
require_once("travellog/factory/Class.FileFactory.php");

abstract class AbstractGroupTemplateController {
	
	protected $file_factory = null;
	
	public function __construct() {
		$this->file_factory = FileFactory::getInstance();
		$this->file_factory->registerTemplate('LayoutMain');
		$this->file_factory->setPath('CSS', '/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
	}
	
	/**
	 * Activates the __call magic function.
	 * 
	 * @return void
	 */
	public function __call($method, $arguments) {
		$actions = include(realpath(dirname(__FILE__).'/../../../').'/lib/services/group_template/actions/action.info.php');
		if (isset($actions[$method])) {
			require_once(realpath(dirname(__FILE__).'/../../../').'/lib/services/group_template/actions/'.$actions[$method]['file']);
			
			$action = new $actions[$method]['class']($arguments[0]);
			if ($action->validate()) {
				$action->execute();
			}
			else {
				$action->onValidationFailure();
			}
		}
		else {
			include_once('FileNotFound.php');
			exit;
		}
	}
}