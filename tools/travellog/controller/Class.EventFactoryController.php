<?php
/**
 * <b>Event</b> factory controller.
 * @author Aldwin S. Sabornido
 * @version 2.0 PHP5
 * @see http://www.GoAbroad.com/
 * @copyright 2006-2007
 */

require_once("travellog/controller/Class.TravelerEventController.php");
require_once("travellog/controller/Class.GroupEventController.php");
require_once("travellog/controller/Class.ActivityController.php");


class EventFactoryController
{
	
	private static 
	
	$TRAVELER = 'TravelerEvent',
	
	$GROUP    = 'GroupEvent',
	
	$ACTIVITY = 'Activity',
	
	$instance = NULL ;
	
	public static function instance()
 	{
 		if (self::$instance == null ) {
 			self::$instance = new EventFactoryController();
 		}
 		
 		return self::$instance;	
 	}

	public static function create( $context )
	{
		
		$event = NULL;
		
		switch( $context )
		{
			case self::$TRAVELER:
				$event = new TravelerEventController();
				break;
			case self::$GROUP:
				$event = new GroupEventController();
				break;	
			/*case self::$ACTIVITY:
				$event = new ActivityController();
				break;*/	
		}
		
		return $event;		
	}
	
	public static function SplitAction( $action )
	{
		$param  = array();
		if ( substr_compare($action,'add',0,2) == 0 ):
			$param[] = 'add';
			$param[] = substr($action,3,strlen($action)-3);
		elseif ( substr_compare($action,'edit',0,3) == 0 ):
			$param[] = 'edit';
			$param[] = substr($action,4,strlen($action)-4) ;
		elseif ( substr_compare($action,'delete',0,5) == 0 ):
			$param[] = 'delete';
			$param[] = substr($action,6,strlen($action)-5) ;
		elseif ( substr_compare($action,'update',0,5) == 0 ):
			$param[] = 'update';
			$param[] = substr($action,6,strlen($action)-5) ;
		elseif ( substr_compare($action,'save',0,3) == 0 ):
			$param[] = 'save';
			$param[] = substr($action,4,strlen($action)-4) ;
		elseif ( substr_compare($action,'view',0,3) == 0 ):
			$param[] = 'view';
			$param[] = substr($action,4,strlen($action)-4) ;
		endif;
		
		return $param;
	}
	
}
?>
