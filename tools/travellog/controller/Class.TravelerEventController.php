<?php
/**
 * <b>Traveler Event</b> controller.
 * @author Aldwin S. Sabornido
 * @version 2.0 PHP5
 * @see http://www.GoAbroad.com/
 * @copyright 2006-2007
 */

require_once("travellog/controller/Class.AbstractEvent.php");
require_once("travellog/model/Class.Traveler.php");
require_once("Class.Template.php");
require_once("Class.Paging.php");
require_once("Class.ObjectIterator.php");
require_once("Class.Date.php");

class TravelerEventController extends AbstractEvent
{
	private 
	
	$primaryID  = NULL;

	
	function isValidForm( $formvars )
	{
		if ( !STRLEN(TRIM($formvars['txtTitle']))       ) array_push( $this->errors,'Title is a requred field!'       );
		if ( !STRLEN(TRIM($formvars['txtDate']))        ) array_push( $this->errors,'Date is a requred field!'        );
		if ( !STRLEN(TRIM($formvars['txaDescription'])) ) array_push( $this->errors,'Description is a requred field!' );
		return ( count($this->errors) )? true : false;
	}
	
	function getData()
	{
		$arr_values = array();
		$obj_gevent = new TravelerEvent ( $this->primaryID );
		if ( count($obj_gevent) ):
			$arr_values['txtTitle']       = $obj_gevent->getTitle();
			//$arr_values['txtDate']        = $this->obj_date->FormatSqlDate($obj_gevent->getTheDate(),'-');
			$arr_values['txtDate']        = FormHelpers::CreateCalendar($obj_gevent->getTheDate());
			$arr_values['txaDescription'] = $obj_gevent->getDescription();
			$arr_values['radPublic']      = $obj_gevent->getDisplayPublic();
			$arr_values['hidGenID']       = $obj_gevent->getTravelerEventID();
			$arr_values['hidPrimaryID']   = $this->primaryID;
		endif;
		return $arr_values;
	}
	
	function displayForm( $formvars = array() )
	{
		$form_tpl = clone $this->main_tpl;
		$this->obj_backlink->Create();
		$form_tpl->set( 'post'              , $formvars                                                     );
		$form_tpl->set( 'obj_backlink'      , $this->obj_backlink                                           );
		$form_tpl->set( 'url_action'        , 'event.php?action='.$this->mode                               );
		
		$this->main_tpl->set( 'title'        , 'Traveler Event Form'                                        );
		$this->main_tpl->set( 'errors'       , $this->errors                                                );		
		$this->main_tpl->set( 'form_tpl'     , $form_tpl->fetch('travellog/views/tpl.FrmTravelerEvent.php') );
		Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
		$this->main_tpl->out( 'travellog/views/tpl.Event.php' );
	}
	
	function getForm($formvars = array())
	{
		$this->obj_backlink->Create();
		$form_tpl = clone $this->main_tpl;
		$form_tpl->set( 'post'              , $formvars                                              );
		$form_tpl->set( 'mode'              , $this->mode                                            );
		$form_tpl->set( 'context'           , 2                                                      );
		$form_tpl->set( 'action'            , 'manager.saveTravelerEvent()'                          );
		$form_tpl->set( 'obj_backlink'      , $this->obj_backlink                                    );
		if(strcasecmp($this->mode,'update') == 0):
			$form_tpl->set( 'cancel'        , 'manager.toggleEvent('.$formvars['hidPrimaryID'].',2)' );
		else:
			$form_tpl->set( 'cancel'        , 'manager.refreshPage()'                                );
		endif;
		
		$form_tpl->out('travellog/views/tpl.FrmGroupEvent.php'  );
		//$form_tpl->out('travellog/views/tpl.FrmTravelerEvent.php'  );
	}
	
	function addEntry( $formvars )
	{
		$obj_event = new TravelerEvent();
		$obj_event->setTravelerID    ( $formvars['hidGenID']                                       );
		$obj_event->setTitle         ( $formvars['txtTitle']                                       );
		$obj_event->setTheDate       ( $this->obj_date->CreateODBCDate($formvars['txtDate'], '/' ) );			
		$obj_event->setDescription   ( $formvars['txaDescription']                                 );
		$obj_event->setDisplayPublic ( $formvars['radPublic']                                      );
		$obj_event->Create();
	}
	
	function updateEntry( $formvars )
	{
		$obj_event = new TravelerEvent  ( $this->primaryID                                              );
		$obj_event->setTitle         ( $formvars['txtTitle']                                       );
		$obj_event->setTheDate       ( $this->obj_date->CreateODBCDate($formvars['txtDate'], '/')  );			
		$obj_event->setDescription   ( $formvars['txaDescription']                                 );
		$obj_event->setDisplayPublic ( $formvars['radPublic']                                      );
		$obj_event->Update();
	}
	
	function removeEntry()
	{
		$obj_event = new TravelerEvent( $this->primaryID );
		$obj_event->Delete();
	}
		
	public function View( $page )
	{
		exit;
		$obj_traveler                 = new Traveler( $this->genericID );
		$arr_obj_traveler_events      = array();
		$arr_obj_traveler_events_temp = $obj_traveler->getTravelerEvents();
		
		if ( !$this->IsLogin && count($arr_obj_traveler_events_temp) ):
			foreach ( $arr_obj_traveler_events_temp as $arr_obj_traveler_event):
				if ( $arr_obj_traveler_event->getDisplayPublic() ) $arr_obj_traveler_events[] = $arr_obj_traveler_event; 
			endforeach;
		elseif ( count($arr_obj_traveler_events_temp) ):
			foreach ( $arr_obj_traveler_events_temp as $arr_obj_traveler_event):
				$arr_obj_traveler_events[] = $arr_obj_traveler_event; 
			endforeach;
		endif;
				
		if ( count($arr_obj_traveler_events) ):
			
			$main_tpl       = new Template();
			$inc_tpl        = clone $main_tpl;
			$obj_paging     = new Paging( count($arr_obj_traveler_events),$page,'action=view&parentID='.$this->genericID,20 );
			$obj_iterator   = new ObjectIterator( $arr_obj_traveler_events,$obj_paging );
			$obj_paging->setOnclick('changeContents');
			
			$obj_iterator->rewind();
			$group_title = $obj_traveler->getName();
			if( $this->primaryID == NULL ):
				$this->primaryID = $obj_iterator->current()->getTravelerEventID();
			endif;
			
			$inc_tpl->set( 'obj_iterator' , $obj_iterator                          );
			$inc_tpl->set( 'obj_paging'   , $obj_paging                            );
			$inc_tpl->set( 'primaryID'    , $this->primaryID                       );
			$inc_tpl->set( 'context'      , 2                                      );
			
			$code = <<<BOF
<script type="text/javascript">
//<![CDATA[
	var COLLAPSABLE_CLASS_NAME = "viewevents";
	var COLLAPSABLE_ELEMENT    = "tr";
	
	var changeColor = function(param) 
	{
	    if(document.getElementById && document.createTextNode) 
	    {
	        var entries = document.getElementsByTagName(COLLAPSABLE_ELEMENT);
	        document.getElementById(param).style.backgroundColor = 'yellow';
	        
	        for(i=0;i < entries.length; i++) 
	            if (entries[i].className==COLLAPSABLE_CLASS_NAME)
					if ( param != entries[i].getAttribute('id') ) document.getElementById(entries[i].getAttribute('id')).style.backgroundColor = 'white';
	    }
	} 
	
	var toggleEvent = function(param,param1)
	{
		new ajax( 'ajaxpages/ajaxevent.php', {postBody: 'eventID='+param+'&context='+param1, update: $('view'), afterUpdate: changeColor(param)});	
	}
	
	var toggleView = function()
	{
		toggleEvent(parseInt($('eventID').innerHTML),parseInt($('context').innerHTML));
	}
	
	var changeContents = function(param)
	{
		new ajax( 'ajaxpages/event-list.php', {postBody: param, update: $('contents'), afterUpdate: toggleView} );
	}
	
	window.onload = function() 
	{
		toggleView();
	}
//]]>
</script>		
BOF;
		
			Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
			//Template::includeDependentJs("/js/prototype1.lite.js");
			Template::includeDependentJs("/min/f=js/prototype.js", array('top' => true));
			Template::includeDependentJs("/js/moo.ajax.js");
			Template::includeDependentJs("/js/moo.fx.js");
			Template::includeDependentJs("/js/moo.fx.pack.js");
			Template::includeDependentJs("/js/Validation.js");
			Template::includeDependent($code);
			
			$main_tpl->set( 'main_title'  , 'Traveler Events'                                            );
			$main_tpl->set( 'group_title' , $group_title                                                 );
			$main_tpl->set( 'action_new'  , "manager.displayTravelerForm('mode=add')"                    );
			$main_tpl->set( 'contents'    , $inc_tpl->fetch('travellog/views/tpl.ViewTravelerEventsContent.php') );
			$main_tpl->out( 'travellog/views/tpl.ViewEvents.php'                                         );
			
		endif;
	}
	
	/**
	 * Setters and Getters
	 */
	public function setPrimaryID( $primaryID )
	{
		$this->primaryID = $primaryID;
	}
	public function getPrimaryID()
	{
		return $this->primaryID;
	}
	
}
?>
