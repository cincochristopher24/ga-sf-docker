<?php
	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/factory/Class.FileFactory.php");
	require_once('class.phpmailer.php');
	
	abstract class AbstractConfirmNotificationController
	{
		protected $file_factory 	= NULL;
		protected $obj_session  	= NULL;
		protected $mLoggedTraveler 	= NULL;
		protected $mSubjectGroup	= NULL;	
		protected $mFilter			= NULL;
		protected $mMethod			= NULL;
		
		public function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/views'));
			$this->obj_session  = SessionManager::getInstance();
			$this->file_factory->registerClass('ToolMan', array('path'=>''));
			$this->file_factory->registerClass('GaString', array('path'=>''));
			$this->file_factory->registerClass('HelperGlobal', array('path'=>''));
			$this->file_factory->registerClass('Template', array('path'=>''));
			$this->file_factory->registerClass('GaDateTime', array('path'=>''));
			$this->file_factory->registerClass('AddressBook');
			$this->file_factory->registerClass('AddressBookEntry');
			$this->file_factory->registerClass('TravelerProfile');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('AdminGroup');
			$this->file_factory->registerClass('ConfirmationData');
			$this->file_factory->registerClass('Crypt', array('path'=>''));
			
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
		}
		
		public function performAction(){
			ob_start();
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			$tmp = $this->file_factory->getClass('Template'); 
			$tmp->set_path('travellog/views/');
			//check if all the parameters is present	
			if(isset($_GET['oID']) && isset($_GET['eID']) && isset($_GET['cnf']) && isset($_GET['ds'])){
				//check if confirmation message is still valid with the ds get variable
				$crypt = $this->file_factory->getClass('Crypt');
				$dateSent = $crypt->decrypt(str_replace(' ','+',$_GET['ds']));
				$isValidDate = $this->file_factory->invokeStaticClass('GaDateTime','isValidDate',array($dateSent)); 
				if(!$isValidDate){
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
				}
				$dateSum = GaDateTime::dateAdd( $dateSent,'m',5 );
				$isDateRangeValid = $this->file_factory->invokeStaticClass('GaDateTime','dateCompare',array($dateSum,time()));
				if(-1 == $isDateRangeValid){
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
				}
				$ownerID = $this->file_factory->invokeStaticClass('ConfirmationData','getDecryptedTravelerID',array($_GET['oID'])); 
				if(null == $ownerID){
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
				}
				$entryID = $this->file_factory->invokeStaticClass('ConfirmationData','getDecryptedAddressBookEntryID',array($_GET['eID']));
				if(null == $entryID){
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
				}
				//check if the value of the conf has valid
				if(0!=$_GET['cnf'] && 1!=$_GET['cnf']){			
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
				}
				
				//check if the entryID is owned by the TravelerID
				$addressBook = $this->file_factory->getClass('AddressBook',array($ownerID)); 
				if(!$this->file_factory->invokeStaticClass('AddressBookEntry','travelerHasEntry',array($ownerID,$entryID))){
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
				}
				$addressBookEntry = $this->file_factory->getClass('AddressBookEntry',array($entryID));
				$ownerTraveler = $this->file_factory->getClass('Traveler',array($ownerID));
				//$travelerProfile = $this->file_factory->getClass('TravelerProfile',array($ownerID));
				
				/*if($ownerTraveler->isAdvisor()){
					$groupID = $this->file_factory->invokeStaticClass('AdminGroup','getAdvisorGroupID',array($ownerID));
					$ownerGroup = new AdminGroup($groupID);
					$travelerUserName = ucwords($ownerGroup->getName());
					$profileLink = "http://".$_SERVER['SERVER_NAME'].$ownerGroup->getFriendlyUrl();
					$userNameOwn = $travelerUserName;
				}
				else{
					$travelerUserName = ucwords($travelerProfile->getUserName());
					$profileLink = "http://".$_SERVER['SERVER_NAME']."/$travelerUserName";
					$ar = str_split($travelerUserName);
					//$userNameOwn = ('s' == strtolower($ar[count($ar)-1]))?$travelerUserName."'":$travelerUserName."'s";
					$userNameOwn = $this->file_factory->invokeStaticClass('GaString','makePossessive',array($travelerUserName));
					
					if (null == $travelerProfile->getGender()){
						$gender1 = 'his/her';
						$gender2 = 'he/she';
					}
					else{
						$gender1 = ('M' == $travelerProfile->getGender())?'his':'her';
						$gender2 = ('M' == $travelerProfile->getGender())?'he':'she';
					}
					
				}*/
				
				//check if the request has been denied or accepted			
				if(1 == $_GET['cnf']){//the request has been approved
					$addressBookEntry->setStatus(AddressBookEntry::ACCEPTED);						
					?>
					<?/*
						Dear <?= $addressBookEntry->getName() ?>,<br /></br />
						<?php if($ownerTraveler->isAdvisor()): ?>
							<p>
								Thank you for confirming your consent. You have agreed to receive updates about <?= $userNameOwn ?>
								group's activities on GoAbroad Network.<br /> 
								The group will be able to send you messages. You will also find out if the <?= $userNameOwn ?> group:
							</p>	
							<p>	
								<ul style="list-style-type:disc;list-style-position: inside;">
									<li>wrote new travel journals</li>
									<li>edited group travel journals</li>
									<li>and uploaded new photos.</li>
								</ul>
							</p>
							<p>	To view <?= $userNameOwn ?> group's home page, please click the following link:</p>
							
						<?php else: ?>	
							<p>
								Thank you for confirming your consent. You have agreed to receive updates about <?= $userNameOwn ?>
								activities on GoAbroad Network.<br /> 
								<?= ucwords($gender2) ?> will be able to send you messages. You will also find out if <?= $travelerUserName ?>:
							</p>	
							<p>	
								<ul style="list-style-type:disc;list-style-position: inside;">
									<li>wrote new travel journals</li>
									<li>edited <?= $gender1 ?> travel journals</li>
									<li>and uploaded new photos.</li>
								</ul>
							</p>
							<p>	To view <?= $userNameOwn ?> profile, please click the following link:</p>
						<?php endif; ?>
						<p><a href="<?= $profileLink ?>"><?= $profileLink ?></a></p>
						<p>	If you're not yet registered on the GoAbroad Network, you may sign up by clicking on the following link:	</p>
						
						<p><a href="http://<?=$_SERVER['SERVER_NAME'] ?>/register.php">http://<?=$_SERVER['SERVER_NAME'] ?>/register.php</a></p>	
						<p>
							Have a great day!<br /><br /><br />
							
							
							Many thanks,<br />
							The Goabroad Network Team
						</p>
						*/?>
						
					<?php
				}
				else{ //the request has been denied
					$addressBookEntry->setStatus(AddressBookEntry::DENIED);				
					?>
					
					<?/*
						<p>	Dear <?= $addressBookEntry->getName() ?>,</p >
						<?php if($ownerTraveler->isAdvisor()): ?>
							<p>
								You have chosen not to receive updates about <?= $userNameOwn ?>group's activities on the GoAbroad Network. <br />
								You can still view <?= $userNameOwn ?> group's travel journals and photos by viewing its groups home page at
							</p>
						<?php else: ?>
							<p>
								You have chosen not to receive updates about <?= $userNameOwn ?> activities on the GoAbroad Network. <br />
								You can still view <?= $gender1 ?> travel journals and photos by viewing <?= $gender1 ?> profile at
							</p>
						<?php endif; ?>
						<p><a href="<?= $profileLink ?>"><?= $profileLink ?></a></p>
						
						<p>	If you're not yet registered on the GoAbroad Network, you may sign up by clicking on the following link:</p>
						
						<p><a href="http://<?=$_SERVER['SERVER_NAME'] ?>/register.php">http://<?=$_SERVER['SERVER_NAME'] ?>/register.php</a></p>		
						<p>
							Have a great day!<br /><br /><br />					
							
							Many thanks,<br />
							The Goabroad Network Team
						</p>
					*/?>	
					
					<?php
								
				}	
				$addressBookEntry->save();
				
				// added by chris: use notification system to send this notification
				require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
				RealTimeNotifier::getInstantNotification(RealTimeNotifier::CONFIRM_ADDRESSBOOK_ENTRY, 
					array(
						'ADDRESSBOOK_ENTRY'	=> $addressBookEntry,
						'OWNER'				=> $ownerTraveler,
						'CONFIRMED'			=> 1 == $_GET['cnf']
					))->send();
				// end add
			}
			else{
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			
			$content = ob_get_clean();
			/*$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->IsHTML(true);
			$mail->From = 'admin@goabroad.net';					
			$mail->FromName = 'The GoAbroad Network';
			$mail->Subject  = 'Notification Confirmation';
			$mail->Body = $content;
			$mail->AddAddress(trim($addressBookEntry->getEmailAddress()));
			if($mail->Send()){
				$content1 = '<p>Thank You for your confirmation. An email was sent to your email account regarding this matter. </p>';
			}
			else{
				$content1 = '<p>Thank You for your confirmation. </p>';
			}
			if($ownerTraveler->isAdvisor()){
				$content1 .= 	'<br />'."<a href='$profileLink'>Visit $userNameOwn group's home page.</a>";
			}
			else{
				$content1 .= 	'<br />'."<a href='$profileLink'>Visit $userNameOwn GoAbroad.net profile</a>";
			}*/
			
			$profileLink 	= $ownerTraveler->getFriendlyUrl();
			$username		= $ownerTraveler->getUsername();
			$gID 			= $ownerTraveler->getAdvisorGroup();
			$extra 			= '<br />'."<a href='$profileLink'>Visit " . GaString::makePossessive($username) . " GoAbroad.net profile</a>";
			
			if( 0 < $gID ){
				try{
					$group = GroupFactory::instance()->create(array($gID));
					$group = $group[0];
					$username = $group->getName();
					$profileLink = $group->getFriendlyUrl();
					$extra 			= '<br />'."<a href='$profileLink'>Visit " . $username . " group's home page</a>";
				}
				catch(exception $e){}
			}
			$content1 	= '<p>Thank You for your confirmation. An email was sent to your email account regarding this matter. </p>' . $extra;
			
			$tmp->set_vars(array(
							'title' => 'Notification Confirmation',
							'content' => $content1,
							'travelerID' => $ownerID,
							'name' =>  $username,
							));
				
			$tmp->out('tpl.DefaultView.php');
		}
	}
?>
