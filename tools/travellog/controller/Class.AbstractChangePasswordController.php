<?php
	require_once('travellog/model/Class.SessionManager.php');
	require_once('travellog/controller/Class.IController.php');
	require_once('travellog/factory/Class.FileFactory.php');
	require_once('travellog/model/Class.Traveler.php');
	require_once('travellog/rules/Class.TravelerAccountRules.php');
	
	abstract class AbstractChangePasswordController implements IController {
		
		/****************************************************************************************
		 * edits of neri:	
		 * 		checked if ref has been set instead of getting its length and 
		 * 			checking if it's equal to 0:									01-05-09
		 * 		set value of cobrand to false in the constructor: 					01-06-09
		 * 		set value of network to cobrand name if it's a cobrand,			
		 * 			otherwise, set it to it's default value "GoAbroad Network"
		 * 			and declared it as a template variable in performAction():		01-06-09									01-06-09
		 ****************************************************************************************/
		
		function __construct () {
			$this->file_factory = FileFactory::getInstance(); 
			$this->file_factory->registerClass('Template', array('path' => ''));
			$this->file_factory->registerTemplate('LayoutMain');
			$this->file_factory->registerTemplate('LayoutPopup');
			
			$this->file_factory->registerClass('TravelerProfile');
			$this->file_factory->registerClass('GroupFactory');
			
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');  
			
			$this->cobrand = false;	
		}
		
		function performAction () {
			if ($this->cobrand) 
				$network = $GLOBALS['CONFIG']->getAdminGroup()->getName().' Network'; 
			else
				$network = 'GoAbroad Network'; 
			
			$this->initialize();
			
			switch ($_GET['action']) {
			 	case 'update':
			 					 		
				 	$traveler = new Traveler($this->travelerID);
					$err = array();
						
			 		if(!isset($_GET['ref'])) {
						// read and verify username entry
						if(!isset($_GET['group'])){
							if(isset($_POST['username'])){
								/*if(0 == strlen(trim($_POST['username']))){
									$err[0]="Username is Empty. Please fill in username.";
								}else if(strlen($_POST['username']) != strlen(ereg_replace("[^A-Za-z0-9_]", "", $_POST['username']))){
									$err[0]="Invalid format for username.(Note: username must be one word only)";
								}else{
									$conn = new Connection();
									$rs    = new Recordset($conn);
									$sql = "SELECT username FROM tblTraveler WHERE travelerID='$this->travelerID'";
									$rs->Execute($sql);

									$travelerobj 	= $rs->FetchAsObject();
								
									if( $travelerobj->username !== $_POST['username'] ){
										$uname = $_POST['username'];
										$sql = "SELECT travelerID FROM tblTraveler WHERE username='$uname'";
										$rs->Execute($sql);
										if( $rs->RecordCount() ){						
											$err[0]="Username is Already Taken.";
										}
									}
								}*/
								if( $traveler->getUsername() != $_POST['username'])
									$tempUsernameErr = TravelerAccountRules::getUsernameErrorMessage($_POST['username']);
								if(!empty($tempUsernameErr))
									$err[0] = array_pop($tempUsernameErr); 
							}
						}
					
						// read and verify email entry
					
						// check if two emails are equal
						if(isset($_POST['newemail']) && strlen(trim($_POST['newemail'])) > 0){
							if(trim($_POST['newemail']) != trim($_POST['newemail2'])){
								$err[1]="The retyped email is not equal to the new email!";
							}
							//else if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $_POST['newemail'])){
							else if(!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i', $_POST['newemail'])){
						 		$err[1]="Invalid email address in New Email field.";
							}
							else{
								$email 		= $_POST['newemail'];
								if($traveler->getEmail() != trim($email)){
									if(!TravelerAccountRules::validateTravelerEmail($email)){
										$err[1]="New Email Address is taken already";
									}
								}
							}
						}
					}
					if (!isset($_POST['newPassword']) || ( !isset($_POST['currentPassword']) && 0 == strlen(trim($_GET['ref'])) ) ) 
			 			exit();
					
			 		// check if the typed current password equal to the password stored in traveler object
					
			 		if ((isset($_POST['currentPassword']) && strlen(trim($_POST['currentPassword'])) > 0) && $_POST['currentPassword']!= $traveler->getPassword() && !isset($_REQUEST['ref'])) {
			 			$err[3] = "The current password is not valid!";
						//$this->template->set("error","The current password is not valid!");
			 			//$this->template->out("Tpl.ChangePassword.php");
			 			//exit;
			 			//break;
			 		}
			
					if(count($err) <= 0){
				 		try {
							// insert code for logger
							require_once('gaLogs/Class.GaLogger.php');
							GaLogger::create()->start($this->travelerID,'TRAVELER');
							
							$success = "Your password has been changed!";
							if(!isset($_GET['ref'])) {
								if(!isset($_GET['group']))
									$this->travelerprofile->setUserName($_POST['username']);
								if($_POST['newemail'] != "" && $_POST['newemail2'] != "")
									$this->travelerprofile->setEmail($_POST['newemail']);
								
								$this->travelerprofile->UpdateAccountSettings();
								$success = "Your account settings has been updated.";
							}
							if($_POST['newPassword'] != ""){
								$traveler->setPassword($_POST['newPassword']);
							 	$traveler->updatePassword();
							}
							$this->template->set("uname",$this->travelerprofile->getUserName());
							$this->template->set("success_message",$success);
					 		$this->template->set("network", $network);
				 		} catch (Exception $exp) {
							$err[4] = $exp->getMessage();
				 			$this->template->set("error",$err);
				 		}
					}else{
				 		$this->template->set("error",$err);
					}
				
			 		$this->template->out("Tpl.ChangePassword.php");
			 		break;
			 		
			 	case 'entry':
			 		$this->template->out("Tpl.ChangePassword.php");
					break;
			}
		}
		
		function initialize () {
			$sm = SessionManager::getInstance();
			if(!isset($_GET['group']))
			 	$this->travelerID = $sm->get('travelerID');
			else{
				$obj_group = $this->file_factory->invokeStaticClass ('GroupFactory','instance')->create(array($_GET['group']));
				$this->travelerID = (count($obj_group)) ? $obj_group[0]->getAdministratorID() : 0;
			}	
		 	$ref = "";
		 	
		 	if (isset($_GET['ref'])) {
			 	require_once("Class.Crypt.php");
			 	
			 	$crypt = new Crypt();
			 	
			 	try{
			 		$this->travelerID = $crypt->decrypt(preg_replace("/ /","+",$_GET['ref']));
							 		
			 		if( !is_numeric($this->travelerID) ){
			 			header("location: register.php");
			 			exit;
			 		}	
			 		
			 		$traveler = new Traveler($this->travelerID);
			 	}	
			 	catch( Exception $e){
			 		header("location: register.php");
			 	}
			 	
			 	$ref = "&ref=".$_GET['ref']; 
			 }
			 
			 elseif ($this->travelerID == 0)
			 	exit();
				
			 if (isset($_GET['ref'])) 
			 	$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			 else 
			 	$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutPopup')));
			 
				
			$this->template = $this->file_factory->getClass('Template');
			$this->template->set_path('travellog/views/');
			$this->template->set("ref",$ref);
			
			
			$this->travelerprofile 	= $this->file_factory->getClass('TravelerProfile',array($this->travelerID));
			// traveler profile settings
			if (!isset($_GET['ref'])){
				$username = $this->travelerprofile->getUserName();
				$email = $this->travelerprofile->getEmail();
				
				$this->template->set("username",$username);
				$this->template->set("email",$email);
			}
		}
	}
?>
