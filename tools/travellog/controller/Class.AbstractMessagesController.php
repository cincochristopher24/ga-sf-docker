<?php
	
require_once ("travellog/factory/Class.FileFactory.php");
require_once("travellog/model/messageCenter/data_mapper/Class.gaMessageDM.php");
require_once("travellog/model/messageCenter/data_mapper/Class.gaNewsDM.php");
require_once("travellog/model/messageCenter/data_mapper/Class.gaPersonalMessageDM.php");
require_once ("travellog/model/messageCenter/manager/Class.gaMessagesManagerRepository.php");	
require_once ("travellog/controller/Class.IController.php");
require_once ("travellog/model/messageCenter/Class.gaMessage.php");
require_once ("travellog/model/messageCenter/Class.gaGroup.php");
require_once ("travellog/model/messageCenter/Class.gaGroupMapper.php");
require_once ("travellog/model/messageCenter/Class.gaTraveler.php");
require_once ("travellog/model/messageCenter/Class.gaTravelerMapper.php");
require_once ("Class.Template.php");
require_once ("Class.GaString.php");

abstract class AbstractMessagesController implements IController{
	
	private static $publicActions = array("view");
	private static $groupActions = array("messageMembers", "messageGroupStaff");
	private $mloggedTraveler = NULL;
	private $mRecipientType = '';//values: Member, Staff
	
	protected $profile_comp;
	protected $file_factory;
	
	abstract protected function getMainTemplate();
	abstract protected function setGroupPageLocation();
	abstract protected function getSiteName();
	abstract protected function loadMessagesManager($id);
	abstract protected function wrapSentMessage(gaPersonalMessage $message);
	abstract protected function loadGroupMessagesManager($id, gaGroup $group);
	
	public function __construct(){
		$this->file_factory = FileFactory::getInstance();
		
		// models
		$this->file_factory->registerClass('Traveler', array('path' => 'travellog/model/'));
		$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
		
		$this->file_factory->registerClass('MainLayoutView', array('path' => 'travellog/views/'));
		$this->file_factory->registerClass('Template', array('path' => ''));
		
		// views
		$this->file_factory->setPath('CSS', '/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
		
		//start logger
		if(isset($_SESSION['travelerID'])){
			require_once('gaLogs/Class.GaLogger.php');
			GaLogger::create()->start($_SESSION['travelerID'],'TRAVELER');
		}
	}
	
	public function performAction() {
		if (isset($_GET['act'])) {
			$action = $_GET['act'];
		} elseif (isset($_GET['action'])) {
			$action = $_GET['action'];
		} else {
			$action = 'view';
		}

		// require login for non public actions
		if (!in_array($action, self::$publicActions)) {
			$this->requireAuthentication();
		}
	//	var_dump($action);
		if (in_array($action, self::$groupActions)) {
			$this->requireGroupID();
		}
		
		switch ($action) {
			case "messageMembers":
				$this->mRecipientType = 'Members';
				$this->handleMessageMembers();
				
				break;
			
			case "messageGroupStaff":
				$this->mRecipientType = 'Staff';
				$this->handleMessageGroupStaff();
				break;
				
			case "messageTraveler":
				$this->handleMessageTraveler();
				break;
				
			case "messageStaffOfManyGroups":
				$this->mRecipientType = 'Staff';
				$this->handleMessageStaffOfManyGroups();
				break;
				
			case "messageManyTravelers":
				$this->handleMessageManyTravelers();
				break;
			
			case "markAsRead":
				$this->markAsRead();
				break;
					
			case "markAsUnread":
				$this->markAsUnread();
				break;
				
			case "delete":
				$this->doDeleteMessage();
				break;
				
			case "deleteOutbox":
				$this->doDeleteOutboxMessage();
				break;
				
			case "batchDelete":
				$this->batchDelete();
				break;
				
			case "batchDeleteOutbox":
				$this->batchDeleteOutbox();
				break;
				
			case "batchMarkAsRead":
				$this->batchMarkAsRead();
				break;
				
			case "batchMarkAsUnread":
				$this->batchMarkAsUnread();
				break;
				
			case "replyToMessage" :
				$this->doReplyToMessage();
				break;
				
			case "view":
				$this->listMessages();
				break;
		}
	}
	
	protected function requireGroupID() {
		if (!isset($_GET['gID']) || !($group = gaGroupMapper::getGroup(trim($_GET['gID'])))) {
			header("location: /messages.php");
			exit;
		}
	}
	
	protected function handleMessageMembers() {
		$group = gaGroupMapper::getGroup($_GET['gID']);

		if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
			// handle form submission
			$errors = $this->validateComposeMessageForm();
			if (count($errors) > 0) {
				$this->showMessageGroupStaffForm($group, $errors);
			} else {
				$this->sendMessageToMembers($group);
				
				$_SESSION['flashMessage'] = 'Message sent to ' . $group->getName() . ' members';
				//header("location: /messages.php?gID=".$group->getID());
				header("location: /messages.php?outbox");
				exit;
			}
		} else {
			if(!in_array($group->getStaffIDs(),$this->loggedTraveler->getID()) && $group->getAdministratorID() != $this->loggedTraveler->getID()){
				header("location: /messages.php");
			}
			$this->showMessageGroupStaffForm($group);
		}
	}
	
	protected function sendMessageToMembers(gaGroup $group) {
		$newMessage = new gaToGroupStaffPersonalMessage();
			
		$newMessage->setSenderID     ( $_SESSION['travelerID']     );
		$newMessage->setSubject      ( "[{$group->getName()}] " . $_POST['txtSubject'] );
		$newMessage->setBody         ( $_POST['txaMsgText']        );
		$newMessage->setRecipientIDs ( $this->excludeSenderInRecipientList($this->loggedTraveler->getID(),$group->getMemberIDs())      );
		
		$recipientSettings = array();
		foreach ($newMessage->getRecipientIDs() as $recipientID) {
			if( $this->isLoggedTravelerSpammerToRecipient($recipientID) ){
				$recipientSettings[$recipientID] = array('isSpam' => 1);
			}else{
				$recipientSettings[$recipientID] = array('isSpam' => 0);
			}
		}
		$newMessage->setRecipientSettings($recipientSettings);
		
		$newMessage->setGroupIDs     ( array($group->getID())      );
		$newMessage->setGroups       ( array($group)               );
		
		$newMessage->send();
		self::sendEmailNotification($newMessage);
	}
	
	protected function showLoginUnless($condition) {
		if (!$condition) {
			header("location: login.php?failedLogin&redirect=messages.php?" . $_SERVER['QUERY_STRING']);
			exit;
		}
	}
	
	protected function showFileNotFound() {
		header ("HTTP/1.0 404 Not Found");
		header ("Status 404 Not Found"); 
		require("travellog/error/FileNotFound.html");
		exit;
	}
	
	protected function showFileNotFoundUnless($condition) {
		if (!$condition) {
			$this->showFileNotFound();
		}
	}
	
	protected function showGroupHomePageUnless($condition){
		if (!$condition) {
			if(isset($_GET['gID']))
				header("Location: /group.php?gID=".$_GET['gID']);
			else
				header("Location: http://".$_SERVER['SERVER_NAME']);
		}
	}
	
	/**
	 * displays all types of messages (personal, news, shout outs)
	 * @author	Jonas Tandinco (Nov/11/2008)
	 */
	public function listMessages() {
		if (isset($_GET['gID']) && !is_null($group = gaGroupMapper::getGroup(intval($_GET['gID'])))) {
			$this->showGroupMessageCenter($group);
		} else {
			$this->showTravelerMessageCenter();
		}
	}
	
	protected function showTravelerMessageCenter() {
		if (!isset($_SESSION['travelerID'])) {
			$this->showLoginUnless(false);
		}
		
		$messagesManager = $this->loadMessagesManager($_SESSION['travelerID']);
		
		$messagesPerPage = 50;
		
		$messages = $messagesManager->getInboxMessages();
		
		require_once 'Class.Paging.php';
		
		$pageNumber = isset($_GET['page']) && isset($_GET['inbox']) ? $_GET['page'] : 1;
		$pager = new Paging(count($messages), $pageNumber, 'inbox', $messagesPerPage);

		// limit number of messages displayed
		$messages = array_slice($messages, ($pageNumber - 1) * $messagesPerPage, $messagesPerPage);
		$sentMessages = $messagesManager->getSentMessages();

		$outboxPageNumber = isset($_GET['page']) && isset($_GET['outbox']) ? $_GET['page'] : 1;
		$outboxPager = new Paging(count($sentMessages), $outboxPageNumber, 'outbox', $messagesPerPage);
		$sentMessages = array_slice($sentMessages, ($outboxPageNumber - 1) * $messagesPerPage, $messagesPerPage);
		
		// SUBNAVIGATION
		require_once 'travellog/model/Class.SubNavigation.php';
		
		$subNavigation = new SubNavigation();
		$subNavigation->setContext         ( 'TRAVELER'              );
		$subNavigation->setContextID       ( $_SESSION['travelerID'] );
		$subNavigation->setLinkToHighlight ( 'MESSAGES'              );
		
		$this->setupTravelerMainTemplate();
		Template::includeDependentJs('/min/g=MessageControllerJs');

		// profile component
		require_once 'travellog/components/profile/controller/Class.ProfileCompFactory.php';
		
		$profileComponent = ProfileCompFactory::create(ProfileCompFactory::$TRAVELER_CONTEXT);
		$profileComponent->init($_SESSION['travelerID']);
		
		$template = $this->file_factory->getClass('Template');
		$template->set_path('travellog/views/messageCenter/');

		$template->set ( 'prof_comp',      $profileComponent->get_view());
		$template->set ( 'subNavigation',  $subNavigation  );
		
		$template->set ( 'showRealName',   false );
		$template->set ( 'showReadStatus', true  );
		
		$template->set ( 'canPostNews',       FALSE        );
		$template->set ( 'canComposeMessage', TRUE         );
		$template->set ( 'canUseTopActions',  TRUE         );
		$template->set ( 'canBeSelected',     TRUE         );
		$template->set ( 'canBeDeleted',      TRUE         );
		
		$template->set ( 'canOutboxBeSelected', TRUE       );
		$template->set ( 'canOutboxBeDeleted',  TRUE       );
		
		$template->set ( 'pager',          $pager          );
		$template->set ( 'outboxPager',    $outboxPager    );
		
		$template->set ( 'isPowerful',     TRUE            );
		$template->set ( 'showOutbox',     true	           );
		$template->set ( 'snippetLimit',   100             );
		$template->set ( 'messages',       $messages       );
		$template->set ( 'outboxMessages', $sentMessages );
		
		$template->set ( 'highlightOutbox', isset($_GET['show_outbox']) ? true : false );
		
		$template->out ( 'tpl.ViewMessageCenterMain.php' );
	}
	
	protected function setupGroupMainTemplate() {
		Template::setMainTemplate    ( $this->getMainTemplate() );
		$this->setGroupPageLocation();
		Template::setMainTemplateVar ( 'layoutID', 'basic'     );
		
		Template::includeDependentCss("/css/vsgStandardized.css");
		Template::includeDependentCss("/css/modalBox.css");
		Template::includeDependentCss('/css/Thickbox.css');
		Template::includeDependentJs('/js/thickbox.js');
	}
	
	protected function setupTravelerMainTemplate() {
		Template::setMainTemplate    ( $this->getMainTemplate()       );
		Template::setMainTemplateVar ( 'page_location', 'My Passport' );
		Template::setMainTemplateVar ( 'layoutID', 'basic'           );
	}

	protected function showGroupMessageCenter(gaGroup $group) {
		require_once 'travellog/model/Class.GroupPrivacyPreference.php';
		
		$privacypref = new GroupPrivacyPreference($group->getID());
		$isMessageCenterPublic = $privacypref->getViewBulletin();;

		if( !(isset($_SESSION['travelerID']) || $isMessageCenterPublic)){
			$this->showGroupHomePageUnless(false);
		}
		
		//$messagesManager = $this->loadMessagesManager($group->getAdministratorID());
		$messagesManager = $this->loadGroupMessagesManager($group->getAdministratorID(),$group);
		
		if (isset($_SESSION['travelerID']) && in_array($_SESSION['travelerID'], $group->getOverseerIDs()))
			$isPowerful = true;
		else
			$isPowerful = false;

		// retrieve messages based on what is to be shown
		$showOutbox = FALSE;
		
		if ($group->isParent() && $isPowerful)
			$messages = $messagesManager->getMessages(array('NEWS', 'SHOUTOUT'));
		else
			$messages = $messagesManager->getNewsOnly();
		
		require_once 'Class.Paging.php';
		
		// PAGING
		$messagesPerPage = 50;
		
		$pageNumber = isset($_GET['page']) ? $_GET['page'] : 1;
		$pager = new Paging(
					count($messages),
					$pageNumber,
					'gID=' . $_GET['gID'],
					$messagesPerPage
				);

		$messages = array_slice($messages, ($pageNumber - 1) * $messagesPerPage, $messagesPerPage);

		$this->setupGroupMainTemplate();
		Template::includeDependentJs('/min/g=MessageControllerJs');
		
		$subNavigation = $this->getGroupSubNavigation($group);
		
		// profile component
		require_once 'travellog/components/profile/controller/Class.ProfileCompFactory.php';
		
		$profileComponent = ProfileCompFactory::create(ProfileCompFactory::$GROUP_CONTEXT);
		$profileComponent->init($_GET['gID']);

		$template = new Template();
		$template->set_path('travellog/views/messageCenter/');

		// prof_comp added by ianne - 12-3-2008
		$template->set ( 'prof_comp',      $profileComponent->get_view());
		$template->set ( 'subNavigation',  $subNavigation );
		
		$template->set ( 'showRealName',        $isPowerful );
		$template->set ( 'showReadStatus',      $isPowerful );
		
		$template->set ( 'canPostNews',         $isPowerful );
		$template->set ( 'canComposeMessage',   $isPowerful );
		$template->set ( 'canUseTopActions',    $isPowerful );
		$template->set ( 'canBeSelected',       $isPowerful );
		$template->set ( 'canBeDeleted',        $isPowerful );
		
		$template->set ( 'canOutboxBeSelected', $isPowerful );
		$template->set ( 'canOutboxBeDeleted',  $isPowerful );
		
		$template->set ( 'pager',          $pager           );
		
		$template->set ( 'isPowerful',     $isPowerful      );
		$template->set ( 'snippetLimit',   100              );
		$template->set ( 'messages',       $messages        );
		$template->set ( 'isClub',         $group->isClub() );
		
		$template->set ( 'outboxMessages', array()  );
		$template->set ( 'showOutbox',     $showOutbox      );		// used for selecting the outbox tab
		
		$template->set ( 'highlightOutbox', isset($_GET['show_outbox']) ? true : false );		// used to show whether the outbox messages should be shown or not

		$template->out ( 'tpl.ViewMessageCenterMain.php' );
	}
	
	public function markAsRead() {
		if (!isset($_POST['isAjax']) && $_POST['isAjax'] != 1) {
			$this->showFileNotFound();
		}
		
		require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
		
		$traveler = gaTravelerMapper::getTraveler($_SESSION['travelerID']);
		if (!$traveler) {
			echo 'Access Denied';
			exit;
		}
		
		$group = null;
		if (isset($_POST['gID'])) {
			require_once 'travellog/model/messageCenter/Class.gaGroupMapper.php';
			$group = gaGroupMapper::getGroup($_POST['gID']);
		}
		
		if (!is_null($group) && in_array($_SESSION['travelerID'], $group->getOverseerIDs())) {
			$ownerID = $group->getAdministratorID();
		} else {
			$ownerID = $_SESSION['travelerID'];
		}

		require_once 'travellog/model/messageCenter/Class.gaMessage.php';
		
		switch ($_POST['contentType']) {
			case gaMessage::SHOUTOUT:
				// TODO: set shoutout as read
				require_once 'travellog/model/messageCenter/data_mapper/Class.gaMessageDM.php';
				gaMessageDM::markCopyAsRead($ownerID, $_POST['messageID']);
				
				break;
			
			case gaMessage::PERSONAL:
				//$this->markPersonalMessageAsRead();
				require_once 'travellog/model/messageCenter/data_mapper/Class.gaPersonalMessageDM.php';
				gaPersonalMessageDM::markCopyAsRead($ownerID, $_POST['messageID']);
				
				break;
				
			case gaMessage::NEWS:
				require_once 'travellog/model/messageCenter/data_mapper/Class.gaNewsDM.php';
				gaNewsDM::markCopyAsRead($_POST['messageID'], $ownerID);
				
				break;
		}
		
		echo 1;
	}
	
	public function doDeleteMessage() {
		if (!isset($_POST['isAjax']) && $_POST['isAjax'] != 1) {
			$this->showFileNotFound();
		}
		
		require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
		
		$traveler = gaTravelerMapper::getTraveler($_SESSION['travelerID']);
		if (!$traveler) {
			echo 'Access Denied';
			exit;
		}
		
		$group = null;
		if (isset($_POST['gID'])) {
			require_once 'travellog/model/messageCenter/Class.gaGroupMapper.php';
			$group = gaGroupMapper::getGroup($_POST['gID']);
		}
		
		if (!is_null($group) && in_array($_SESSION['travelerID'], $group->getOverseerIDs())) {
			$ownerID = $group->getAdministratorID();
		} else {
			$ownerID = $_SESSION['travelerID'];
		}
		

		require_once 'travellog/model/messageCenter/Class.gaMessage.php';
		
		switch ($_POST['contentType']) {
			case gaMessage::SHOUTOUT:
				require_once 'travellog/model/Class.Comment.php';
				
				$comment = new Comment($_POST['messageID']);
				$comment->Delete();
				
				break;
			
			case gaMessage::PERSONAL:
				require_once 'travellog/model/messageCenter/data_mapper/Class.gaPersonalMessageDM.php';
				gaPersonalMessageDM::deleteCopy($ownerID, $_POST['messageID']);
			
				break;
				
			case gaMessage::NEWS:
				require_once 'travellog/model/messageCenter/data_mapper/Class.gaNewsDM.php';
				gaNewsDM::deleteCopy($ownerID, $_POST['messageID']);
				
				break;
		}
		
		echo 'Message deleted';
	}
	
	public function doDeleteOutboxMessage() {
		if (!isset($_POST['isAjax']) && $_POST['isAjax'] != 1) {
			$this->showFileNotFound();
		}
		
		require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
		$traveler = gaTravelerMapper::getTraveler($_SESSION['travelerID']);

		if (!$traveler) {
			echo 'Access Denied';
			exit;
		}

		require_once 'travellog/model/messageCenter/Class.gaGroupMapper.php';
		
		if (isset($_POST['gID'])) {
			$group = gaGroupMapper::getGroup($_POST['gID']);
		} else {
			$group = null;
		}
		
		if (is_null($group)) {
			$ownerID = $_SESSION['travelerID'];
		} else {
			$ownerID = $group->getAdministratorID();
		}
		
		require_once 'travellog/model/messageCenter/data_mapper/Class.gaPersonalMessageDM.php';
		
		gaPersonalMessageDM::delete($ownerID, $_POST['messageID']);
		
		echo 'Message deleted';
	}
	
	public function batchDelete() {
		if (!isset($_POST['isAjax']) && $_POST['isAjax'] != 1) {
			$this->showFileNotFound();
		}
		
		require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
		
		$traveler = gaTravelerMapper::getTraveler($_SESSION['travelerID']);
		if (!$traveler) {
			echo 'Access Denied';
			exit;
		}

		require_once 'travellog/model/messageCenter/Class.gaMessage.php';
		
		$messageIDs   = explode ( ',', $_POST['messageIDs']   );
		$contentTypes = explode ( ',', $_POST['contentTypes'] ); 
		$groupID      = isset($_POST['gID']) ? $_POST['gID'] : 0;
		
		$group = null;
		if (isset($_POST['gID'])) {
			require_once 'travellog/model/messageCenter/Class.gaGroupMapper.php';
			$group = gaGroupMapper::getGroup($_POST['gID']);
		}
		
		if (!is_null($group) && in_array($_SESSION['travelerID'], $group->getOverseerIDs())) {
			$ownerID = $group->getAdministratorID();
		} else {
			$ownerID = $_SESSION['travelerID'];
		}
		
		$shoutOutIDs        = array();
		$newsIDs            = array();
		$personalMessageIDs = array();
		
		for ($i = 0; $i < count($messageIDs); $i++) {
			switch ($contentTypes[$i]) {
				case gaMessage::SHOUTOUT:
					$shoutOutIDs[] = $messageIDs[$i];
				
					break;
					
				case gaMessage::PERSONAL:
					$personalMessageIDs[] = $messageIDs[$i];
				
					break;
					
				case gaMessage::NEWS:
					$newsIDs[] = $messageIDs[$i];
				
					break;
			}
		}
		
		if (count($shoutOutIDs) > 0) {
			require_once 'travellog/model/Class.Comment.php';
			
			foreach ($shoutOutIDs as $id) {
				$shoutOut = new Comment($id);
				$shoutOut->Delete();
			}
		}
		
		if (count($newsIDs) > 0) {
			require_once 'travellog/model/messageCenter/data_mapper/Class.gaNewsDM.php';
			gaNewsDM::batchDeleteCopy($ownerID, $newsIDs);
		}
		
		if (count($personalMessageIDs) > 0) {
			require_once 'travellog/model/messageCenter/data_mapper/Class.gaPersonalMessageDM.php';
			gaPersonalMessageDM::batchDeleteCopy($ownerID, $personalMessageIDs);
		}

		echo 'Selected messages deleted';
	}
	
	public function batchDeleteOutbox() {
		if (!isset($_POST['isAjax']) && $_POST['isAjax'] != 1) {
			$this->showFileNotFound();
		}
		
		require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
		$traveler = gaTravelerMapper::getTraveler($_SESSION['travelerID']);

		if (!$traveler) {
			echo 'Access Denied';
			exit;
		}

		$personalMessageIDs   = explode ( ',', $_POST['messageIDs']   );
		$contentTypes = explode ( ',', $_POST['contentTypes'] ); 
		
		require_once 'travellog/model/messageCenter/Class.gaGroupMapper.php';
		
		if (isset($_POST['gID'])) {
			$group = gaGroupMapper::getGroup($_POST['gID']);
		} else {
			$group = null;
		}
		
		if (is_null($group)) {
			$ownerID = $_SESSION['travelerID'];
		} else {
			$ownerID = $group->getAdministratorID();
		}
		
		require_once 'travellog/model/messageCenter/data_mapper/Class.gaPersonalMessageDM.php';
		
		gaPersonalMessageDM::batchDelete($ownerID, explode(',', $_POST['messageIDs']));
		
		echo 'Selected messages deleted';
	}
	
	public function batchMarkAsUnread() {
		if (!isset($_POST['isAjax']) && $_POST['isAjax'] != 1) {
			$this->showFileNotFound();
		}
		
		require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
		$traveler = gaTravelerMapper::getTraveler($_SESSION['travelerID']);

		if (!$traveler) {
			echo 'Access Denied';
			exit;
		}

		require_once 'travellog/model/messageCenter/Class.gaMessage.php';
		
		$messageIDs   = explode ( ',', $_POST['messageIDs']   );
		$contentTypes = explode ( ',', $_POST['contentTypes'] );

		$group = null;
		if (isset($_POST['gID'])) {
			require_once 'travellog/model/messageCenter/Class.gaGroupMapper.php';
			$group = gaGroupMapper::getGroup($_POST['gID']);
		}
		
		if (!is_null($group) && in_array($_SESSION['travelerID'], $group->getOverseerIDs())) {
			$ownerID = $group->getAdministratorID();
		} else {
			$ownerID = $_SESSION['travelerID'];
		}

		$shoutOutIDs        = array();
		$newsIDs            = array();
		$personalMessageIDs = array();
		
		for ($i = 0; $i < count($messageIDs); $i++) {
			switch ($contentTypes[$i]) {
				case gaMessage::SHOUTOUT:
					$shoutOutIDs[] = $messageIDs[$i];
				
					break;
					
				case gaMessage::PERSONAL:
					$personalMessageIDs[] = $messageIDs[$i];
				
					break;
					
				case gaMessage::NEWS:
					$newsIDs[] = $messageIDs[$i];
				
					break;
			}
		}
		
		if (count($shoutOutIDs) > 0) {
			require_once 'travellog/model/Class.Comment.php';
			
			foreach ($shoutOutIDs as $id) {
				$shoutOut = new Comment($id);
				
				$shoutOut->setIsRead(FALSE);
				$shoutOut->UpdateIsRead();
			}
		}
		
		if (count($newsIDs) > 0) {
			require_once 'travellog/model/messageCenter/data_mapper/Class.gaNewsDM.php';
			gaNewsDM::batchMarkAsUnread($ownerID, $newsIDs);
		}
		
		if (count($personalMessageIDs) > 0) {
			require_once 'travellog/model/messageCenter/data_mapper/Class.gaPersonalMessageDM.php';
			gaPersonalMessageDM::batchMarkCopyAsUnread($ownerID, $personalMessageIDs);
		}

		echo 1;
	}
	
	public function batchMarkAsRead() {
		if (!isset($_POST['isAjax']) && $_POST['isAjax'] != 1) {
			$this->showFileNotFound();
		}
		
		require_once 'travellog/model/messageCenter/Class.gaTravelerMapper.php';
		$traveler = gaTravelerMapper::getTraveler($_SESSION['travelerID']);
		
		if (!$traveler) {
			echo 'Access Denied';
			exit;
		}

		require_once 'travellog/model/messageCenter/Class.gaMessage.php';
		
		$messageIDs   = explode ( ',', $_POST['messageIDs']   );
		$contentTypes = explode ( ',', $_POST['contentTypes'] );
		
		$group = null;
		if (isset($_POST['gID'])) {
			require_once 'travellog/model/messageCenter/Class.gaGroupMapper.php';
			$group = gaGroupMapper::getGroup($_POST['gID']);
		}
		
		// identify whose copy are we going to modify
		if (!is_null($group) && in_array($_SESSION['travelerID'], $group->getOverseerIDs())) {
			$ownerID = $group->getAdministratorID();
		} else {
			$ownerID = $_SESSION['travelerID'];
		}
		
		$shoutOutIDs        = array();
		$newsIDs            = array();
		$personalMessageIDs = array();
		
		for ($i = 0; $i < count($messageIDs); $i++) {
			switch ($contentTypes[$i]) {
				case gaMessage::SHOUTOUT:
					$shoutOutIDs[] = $messageIDs[$i];
				
					break;
					
				case gaMessage::PERSONAL:
					$personalMessageIDs[] = $messageIDs[$i];
				
					break;
					
				case gaMessage::NEWS:
					$newsIDs[] = $messageIDs[$i];
				
					break;
			}
		}
		
		if (count($shoutOutIDs) > 0) {
			require_once 'travellog/model/Class.Comment.php';
			
			foreach ($shoutOutIDs as $id) {
				$shoutOut = new Comment($id);
				
				$shoutOut->setIsRead(TRUE);
				$shoutOut->UpdateIsRead();
			}
		}
		
		if (count($newsIDs) > 0) {
			require_once 'travellog/model/messageCenter/data_mapper/Class.gaNewsDM.php';
			gaNewsDM::batchMarkAsRead($ownerID, $newsIDs);
		}
		
		if (count($personalMessageIDs) > 0) {
			require_once 'travellog/model/messageCenter/data_mapper/Class.gaPersonalMessageDM.php';
			gaPersonalMessageDM::batchMarkCopyAsRead($ownerID, $personalMessageIDs);
		}

		echo 1;
	}
	
	public function doReplyToMessage() {
		if (!isset($_POST['isAjax']) && $_POST['isAjax'] != 1) {
			echo 'Access Denied';
			exit;
		}
		
		if (isset($_POST['gID'])) {
			require_once 'travellog/model/messageCenter/Class.gaGroupMapper.php';
			$group = gaGroupMapper::getGroup($_POST['gID']);
		} else {
			$group = null;
		}
		
		if (!is_null($group)) {
			$newMessage = $this->sendReplyToGroupStaff($group);
		} else {
			$newMessage = $this->sendReplyToTraveler();
		}

		foreach($newMessage->getRecipientIDs() as $recipientID){
			$recipient = gaTravelerMapper::getTraveler($recipientID);
		}

		require_once 'travellog/helper/messageCenter/messageCenterHelper.php';  // restored by nash - July 16, 2009 ** causing an error in dev
		$message = $this->wrapSentMessage($newMessage);

		if (!is_null($group) && in_array($_SESSION['travelerID'], $group->getOverseerIDs()))
			$showRealName = TRUE;
		else
			$showRealName = FALSE;
		
		includeView('singleOutboxMessage', array(
												'message' => $message,
												'snippetLimit' => 100,
												'canBeSelected' => $_POST['caller'] == 1,
												'canBeDeleted' => TRUE,
												'showRealName' => $showRealName,
											));
	}
	
	protected function sendReplyToGroupStaff(gaGroup $group) {
		require_once 'travellog/model/messageCenter/personal_message/Class.gaToGroupStaffPersonalMessage.php';
		
		$newMessage = new gaToGroupStaffPersonalMessage();
		
		$newMessage->setSenderID($_SESSION['travelerID']);
		$newMessage->setSubject($_POST['subject']);
		$newMessage->setBody($_POST['message']);
		
		if ($group->isParent()) {
			$newMessage->setRecipientIDs($this->excludeSenderInRecipientList($this->loggedTraveler->getID(),array_merge(array($group->getAdministratorID()), $group->getSuperStaffIDs())));
		} else {
			$newMessage->setRecipientIDs($this->excludeSenderInRecipientList($this->loggedTraveler->getID(),array_merge(array($group->getAdministratorID()), $group->getStaffIDs())));
		}
		
		$newMessage->setRecipients(gaTravelerMapper::retrieveByPks($newMessage->getRecipientIDs()));
		
		$recipientSettings = array();
		foreach ($newMessage->getRecipientIDs() as $recipientID) {
			if( $this->isLoggedTravelerSpammerToRecipient($recipientID) ){
				$recipientSettings[$recipientID] = array('isSpam' => 1);
			}else{
				$recipientSettings[$recipientID] = array('isSpam' => 0);
			}
		}
		
		$newMessage->setRecipientSettings($recipientSettings);
		
		$newMessage->setGroupID($group->getID());
		$newMessage->setGroup($group);
		
		$newMessage->send();
		
		return $newMessage;
	}
	
	protected function sendReplyToTraveler() {
		require_once 'travellog/model/messageCenter/personal_message/Class.gaToTravelerPersonalMessage.php';
		
		$newMessage = new gaToTravelerPersonalMessage();
		
		$newMessage->setSenderID($_SESSION['travelerID']);
		$newMessage->setSubject($_POST['subject']);
		$newMessage->setBody($_POST['message']);
		$newMessage->setDateCreated(date('Y-m-d H:i:s'));
		
		$newMessage->setRecipientIDs(array($_POST['recipientID']));
		$newMessage->setRecipients(gaTravelerMapper::retrieveByPks($newMessage->getRecipientIDs()));
		//$newMessage->setRecipientSettings(array($_POST['recipientID'] => array('isSpam' => 0)));
		
		$isSpam = $this->isLoggedTravelerSpammer(gaTravelerMapper::getTraveler($_POST['recipientID'])) ? 1 : 0;
		$newMessage->setRecipientSettings(array($_POST['recipientID'] => array('isSpam' => $isSpam)));
		
		$newMessage->send();
		
		self::sendEmailNotification($newMessage);
		
		return $newMessage;
	}
	
	// function added by ianne - init profile component for group/traveler profile
	function initProfileComponent(){
		require_once 'travellog/components/profile/controller/Class.ProfileCompFactory.php';
		
		if (isset($_GET['gID'])) {
			$this->profile_comp = ProfileCompFactory::create(ProfileCompFactory::$GROUP_CONTEXT);
			$this->profile_comp->init($_GET['gID']);
		}else{
			if( isset($_SESSION['gID']) ) { // logged in as advisor
				$this->profile_comp = ProfileCompFactory::create(ProfileCompFactory::$GROUP_CONTEXT);
				$this->profile_comp->init($this->session_manager->get('gID'));
			}else{
				$this->profile_comp = ProfileCompFactory::create(ProfileCompFactory::$TRAVELER_CONTEXT);
				$this->profile_comp->init($this->session_manager->get('travelerID'));
			}
		}
	}
	
	/**
	 * REQUIREMENTS: for a traveler to send messages to staff of many different groups in behalf of the group,
	 * he must be logged on and have admin privileges (advisor, super staff, staff)
	 */
	protected function handleMessageStaffOfManyGroups() {
		$group = gaGroupMapper::getGroup(trim($_GET['gID']));
		if (!isset($_GET['gID']) || !$group || !in_array($_SESSION['travelerID'], $group->getOverseerIDs())) {
			header("location: /messages.php");
			exit;
		}
		
		if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
			// handle form submission
			$errors = $this->validateComposeMessageForm();
			if (!isset($errors["recipientList"]) && !$this->validateGroupNameOfStaffMessageRecipients()) {
				$errors["recipientList"] = "Please check to make sure you have spelled the recipient's name correctly.";
			}
			
			if (count($errors) > 0) {
				$this->showMessageStaffOfManyGroupsForm($group, $errors);
			} else {
				$groupNames = $this->sendMessageToGroupStaffOfManyGroups($group);
				
				$_SESSION['flashMessage'] = 'Message sent to staff of '.$groupNames;
				//header("location: /messages.php?gID=" . $group->getID());
				header("location: /messages.php?outbox");
				exit;
			}
		} else {
			$this->showMessageStaffOfManyGroupsForm($group);
		}
	}
	
	protected function validateGroupNameOfStaffMessageRecipients() {
		$groupNames = explode(',', trim($_POST['txtDestination']));
		foreach ($groupNames as $key => $groupName) {
			$groupNames[$key] = trim($groupName);
		}
		
		$groups = gaGroupMapper::retrieveGroupsByGroupNames($groupNames);
		
		if (count($groups) > 0) {
			return true;
		}
		
		return false;
	}
	
	protected function showMessageStaffOfManyGroupsForm(gaGroup $group, array $errors = array()) {
		$profileComponent = $this->getGroupProfileComponent($group);
		$subnavigation    = $this->getGroupSubNavigation($group);
		
		$inboxLink  = "/messages.php?gID=" . $group->getID();
		$outboxLink = "/messages.php?gID=" . $group->getID() . "&show_outbox";
		
		$siteDisplayName = $this->getSiteName();
		
		$notifyByEmail = true;
		if (isset($_POST['btnSend']) && !isset($_POST['chkSendNotification'])) {
			$notifyByEmail = false;
		}
		
		$suggestions = gaGroupMapper::retrieveCoveredGroupNames($group);
		foreach ($suggestions as $key => $suggestion) {
			$suggestions[$key] = "'$suggestion'";
		}
		
		// auto suggest
		$this->setupGroupMainTemplate();
		Template::includeDependentJs("/min/g=AutoCompleteJs");
		//Template::includeDependentJs("/js/actb.js");
		//Template::includeDependentJs("/js/commons.js");
		
		$formTemplate = new Template();

		$formTemplate->set_vars(array(
				'recipientsListLocked' => false,
				'recipientDisplayName' => isset($_POST['txtDestination']) ? $_POST['txtDestination'] : $group->getName(),
				'subject'              => isset($_POST['txtSubject']) ? $_POST['txtSubject'] : '',
				'body'                 => isset($_POST['txaMsgText']) ? $_POST['txaMsgText'] : '',
				'notifyByEmail'        => $notifyByEmail ? 'checked' : '',
				'profileComponent'     => $profileComponent->get_view(),
				'subnavigation'        => $subnavigation,
				'inboxLink'            => $inboxLink,
				'outboxLink'           => $outboxLink,
				'siteDisplayName'      => $siteDisplayName,
				'errors'               => $errors,
				'autoSuggestSetting'   => 'on',
				'suggestions'          => $suggestions,
				'helpText'             => 'Send messages to your <strong>groups</strong> - just type in the name of the group. To send the same message to many groups, ' .
										  'just separate their names with commas.<br /><br />' .
										  'To send a message to a <strong>member</strong>, please go to the member\'s profile and click Send a Message.',
				'recipientType'		   => $this->mRecipientType
			));
		
		$formTemplate->out('travellog/views/messageCenter/tpl.FrmComposeMessage.php');
	}
	
	protected function sendMessageToGroupStaffOfManyGroups(gaGroup $sendingGroup) {
		$groupNames = explode(',', trim($_POST['txtDestination']));
		foreach ($groupNames as $key => $groupName) {
			$groupNames[$key] = trim($groupName);
		}
		
		$groups = gaGroupMapper::retrieveGroupsByGroupNames($groupNames);
		
		$collRecipientIDs = array();
		$collGroupIDs = array();
		
		$groupNames = array();
		foreach ($groups as $group) {
			$collGroupIDs[] = $group->getID();
			
			$groupNames[] = $group->getName();
			
			if ($group->isParent()) {
				$collRecipientIDs = array_merge($collRecipientIDs, array($group->getAdministratorID()), $group->getSuperStaffIDs());
			} else {
				$collRecipientIDs = array_merge($collRecipientIDs, array($group->getAdministratorID()), $group->getStaffIDs());
			}
		}
		
		// make sure a recipient doesnt' receive the message more than once since it's possible
		// that a certain traveler is a staff of many groups and those groups could possibly be
		// included as recipients of this message
		$collRecipientIDs = array_unique($collRecipientIDs);
		$collGroupIDs = array_unique($collGroupIDs);
		
		$collRecipientIDs = $this->excludeSenderInRecipientList($this->loggedTraveler->getID(),$collRecipientIDs);
		
		$newMessage = new gaToGroupStaffPersonalMessage();
			
		$newMessage->setSenderID     ( $_SESSION['travelerID'] );
		$newMessage->setSubject      ( "[{$sendingGroup->getName()}] " . $_POST['txtSubject']    );
		$newMessage->setBody         ( $_POST['txaMsgText']    );
		
		$newMessage->setRecipientIDs ( $collRecipientIDs       );
		
		$recipientSettings = array();
		foreach ($newMessage->getRecipientIDs() as $recipientID) {
			if( $this->isLoggedTravelerSpammerToRecipient($recipientID) ){
				$recipientSettings[$recipient] = array('isSpam' => 1);
			}else{
				$recipientSettings[$recipientID] = array('isSpam' => 0);
			}
		}
		$newMessage->setRecipientSettings($recipientSettings);
		
		$newMessage->setGroupIDs     ( $collGroupIDs           );
		$newMessage->setGroups       ( $groups                 );
		
		$newMessage->setIsMessageIncludedInNotif(isset($_POST['chkSendNotification']) ? 1 : 0);
		
		$newMessage->send();
		self::sendEmailNotification($newMessage);
		
		return join(", ",$groupNames);
	}
	
	protected function handleMessageManyTravelers() {
		if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
			// handle form submission
			$errors = $this->validateComposeMessageForm();
			if (!isset($errors["recipientList"]) && !$this->validateTravelerMessageRecipients()) {
				$errors["recipientList"] = "Please check to make sure you have spelled the recipient's name correctly.";
			}
			
			if (count($errors) > 0) {
				$this->showMessageManyTravelersForm($errors);
			} else {
				$this->sendMessageToManyTravelers();
				
				$_SESSION['flashMessage'] = 'Message sent';
				header("location: /messages.php?outbox");
				exit;
			}
		} else {
			$this->showMessageManyTravelersForm();
		}
	}
	
	protected function validateTravelerMessageRecipients() {
		$userNames = explode(',', trim($_POST['txtDestination']));
		foreach ($userNames as $key => $userName) {
			$userNames[$key] = trim($userName);
		}
		
		$recipientIDs = gaTravelerMapper::getIDsByUserNames($userNames);
		
		if (count($recipientIDs) > 0) {
			return true;
		}

		return false;
	}
	
	protected function sendMessageToManyTravelers() {
		$userNames = explode(',', trim($_POST['txtDestination']));
		foreach ($userNames as $key => $userName) {
			$userNames[$key] = trim($userName);
		}
		$usernames = array_unique($userNames);
		
		$recipientIDs = gaTravelerMapper::getIDsByUserNames($userNames);
		//$sender = gaTravelerMapper::getTraveler($_SESSION['travelerID']);
		
		$recipientSettings = array();
		foreach ($recipientIDs as $id) {
			/*if (in_array($id, $sender->getFriendIDs()))
				$isSpam = 0;
			else
				$isSpam = 1;
			
			$recipientSettings[$id] = array('isSpam' => $isSpam);*/
			
			$recipientSettings[$id] = $this->isLoggedTravelerSpammer(gaTravelerMapper::getTraveler($id)) ? array('isSpam' => 1) : array('isSpam' => 0);
		}
		
		$newMessage = new gaToTravelerPersonalMessage();
			
		$newMessage->setSenderID     ( $_SESSION['travelerID']   );
		$newMessage->setSubject      ( $_POST['txtSubject']      );
		$newMessage->setBody         ( $_POST['txaMsgText']      );
		$newMessage->setRecipientIDs ( $recipientIDs );
		
		$newMessage->setRecipientSettings ( $recipientSettings);
		
		$newMessage->send();
		
		self::sendEmailNotification($newMessage);
	}
	
	protected function showMessageManyTravelersForm(array $errors = array()) {
		$traveler = gaTravelerMapper::getTraveler($_SESSION['travelerID']);
		$profileComponent = $this->getTravelerProfileComponent($traveler);
		$subnavigation    = $this->getTravelerSubNavigation($traveler);
		
		$inboxLink  = "/messages.php";
		$outboxLink = "/messages.php?show_outbox";
		
		$siteDisplayName = $this->getSiteName();
		
		if (isset($_POST['btnSend']) && !isset($_POST['chkSendNotification'])) {
			$notifyByEmail = false;
		} else {
			$notifyByEmail = true;
		}
		
		$suggestions = gaTravelerMapper::getFriendUserNames($_SESSION['travelerID']);
		foreach ($suggestions as $key => $suggestion) {
			$suggestions[$key] = "'$suggestion'";
		}
		
		// auto suggest
		$this->setupTravelerMainTemplate();
		Template::includeDependentJs("/min/g=AutoCompleteJs");
		//Template::includeDependentJs("/js/actb.js");
		//Template::includeDependentJs("/js/commons.js");
		
		
		$formTemplate = new Template();
		
		$formTemplate->set_vars(array(
				'recipientsListLocked' => false,
				'recipientDisplayName' => isset($_POST['txtDestination']) ? $_POST['txtDestination'] : '',
				'subject'              => isset($_POST['txtSubject']) ? $_POST['txtSubject'] : '',
				'body'                 => isset($_POST['txaMsgText']) ? $_POST['txaMsgText'] : '', 
				'notifyByEmail'        => $notifyByEmail ? 'checked' : '',
				'profileComponent'     => $profileComponent->get_view(),
				'subnavigation'        => $subnavigation,
				'inboxLink'            => $inboxLink,
				'outboxLink'           => $outboxLink,
				'siteDisplayName'      => $siteDisplayName,
				'errors'               => $errors,
				'autoSuggestSetting'   => 'on',
				'suggestions'          => $suggestions,
				'helpText'             => 'Send a message to your friends and other travelers on GoAbroad Network. To send the same message to many recipients, just separate their names with commas.',
				'recipientType'	   	   => $this->mRecipientType
			));
		
		$formTemplate->out('travellog/views/messageCenter/tpl.FrmComposeMessage.php');
	}
	
	protected function handleMessageTraveler() {
		if (!isset($_GET['id']) || !($traveler = gaTravelerMapper::getTraveler(trim($_GET['id'])))) {
			header("location: /messages.php");
			exit;
		}
		
		if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
			// handle form submission
			$errors = $this->validateComposeMessageForm();
			
			if (count($errors) > 0) {
				$this->showMessageTravelerForm($traveler, $errors);
			} else {
				$this->sendMessageToTraveler($traveler);
				
				$_SESSION['flashMessage'] = 'Message sent to ' . $traveler->getUserName();
				header("location: /messages.php?outbox");
				exit;
			}
		} else {
			$this->showMessageTravelerForm($traveler);
		}
	}
	
	protected function sendMessageToTraveler(gaTraveler $traveler) {
		// if the sender and recipients are not friends, set message as spam
		/*if (in_array($_SESSION['travelerID'], $traveler->getFriendIDs())) {
			$isFriend = TRUE;
		} else {
			$isFriend = FALSE;
		}
		
		$recipientSettings["isSpam"] = $isFriend ? 0 : 1; */ 
		
		$recipientSettings["isSpam"] = $this->isLoggedTravelerSpammer($traveler)? 1 : 0;
		
		$newMessage = new gaToTravelerPersonalMessage();
			
		$newMessage->setSenderID     ( $_SESSION['travelerID']   );
		$newMessage->setSubject      ( $_POST['txtSubject']      );
		$newMessage->setBody         ( $_POST['txaMsgText']      );
		$newMessage->setRecipientIDs ( array($traveler->getID()) );
		
		$newMessage->setRecipientSettings ( array($traveler->getID() => $recipientSettings) );
		
		$newMessage->setIsMessageIncludedInNotif(isset($_POST['chkSendNotification']) ? 1 : 0);
		
		$newMessage->send();
		
		self::sendEmailNotification($newMessage);
	}
	
	protected function showMessageTravelerForm(gaTraveler $traveler, array $errors = array()) {
		$profileComponent = $this->getTravelerProfileComponent($traveler);
		$subnavigation    = $this->getTravelerSubNavigation($traveler);
		
		$inboxLink  = "/messages.php";
		$outboxLink = "/messages.php?show_outbox";
		
		$siteName = $this->getSiteName();
		
		$notifyByEmail = true;
		if (isset($_POST['btnSend']) && !isset($_POST['chkSendNotification'])) {
			$notifyByEmail = false;
		}

		$this->setupTravelerMainTemplate();
		$formTemplate = new Template();
		
		$formTemplate->set_vars(array(
				'recipientsListLocked' => true,
				'recipientDisplayName' => $traveler->getUserName(),
				'subject'              => isset($_POST['txtSubject']) ? $_POST['txtSubject'] : '',
				'body'                 => isset($_POST['txaMsgText']) ? $_POST['txaMsgText'] : '',              
				'notifyByEmail'        => $notifyByEmail ? 'checked' : '',
				'profileComponent'     => $profileComponent->get_view(),
				'subnavigation'        => $subnavigation,
				'inboxLink'            => $inboxLink,
				'outboxLink'           => $outboxLink,
				'siteDisplayName'      => $siteName,
				'errors'               => $errors,
				'autoSuggestSetting'   => 'off',
				'suggestions'          => '',
				'helpText'             => '',
				'recipientType'	   	   => $this->mRecipientType
			));
		
		$formTemplate->out('travellog/views/messageCenter/tpl.FrmComposeMessage.php');
	}
	
	protected function handleMessageGroupStaff() {
		$group = gaGroupMapper::getGroup(trim($_GET['gID']));
		if (!isset($_GET['gID']) || !$group) {
			header("location: /messages.php");
			exit;
		}

		if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
			// handle form submission
			$errors = $this->validateComposeMessageForm();
			if (count($errors) > 0) {
				$this->showMessageGroupStaffForm($group, $errors);
			} else {
				// RULE: if a subgroup doesn't have staff, message is sent to super staff and admin
				if ($group->isParent() || count($group->getStaffIDs()) <= 0) {
					$this->sendMessageToGroupStaff($group, array_merge(array($group->getAdministratorID()), $group->getStaffIDs()));
				} else {
					$this->sendMessageToGroupStaff($group, array_merge(array($group->getAdministratorID()), $group->getStaffIDs()));
				}
				
				$_SESSION['flashMessage'] = 'Message sent to ' . $group->getName() . ' staff';
				//header("location: /messages.php?gID=" . $group->getID());
				header("location: /messages.php?outbox");
				exit;
			}
		} else {
			if(!in_array($this->loggedTraveler->getID(), $group->getStaffIDs()) 
				&& $group->getAdministratorID() != $this->loggedTraveler->getID()
				&& !in_array($this->loggedTraveler->getID(), $group->getMemberIDs()) ){
				header("location: /messages.php");
			}
			$this->showMessageGroupStaffForm($group);
		}
	}
	
	protected function sendMessageToGroupStaff(gaGroup $group, array $recipientIDs) {
		$recipientIDs = $this->excludeSenderInRecipientList($this->loggedTraveler->getID(),$recipientIDs);
		
		$newMessage = new gaToGroupStaffPersonalMessage();
			
		$newMessage->setSenderID     ( $_SESSION['travelerID']     );
		$newMessage->setSubject      ( "[{$group->getName()}] " . $_POST['txtSubject'] );
		$newMessage->setBody         ( $_POST['txaMsgText']        );
		$newMessage->setRecipientIDs ( array_unique($recipientIDs) );
		
		$recipientSettings = array();
		foreach ($newMessage->getRecipientIDs() as $recipient) {
			// set non-spam if sender is group member or administrator of the group
			if( in_array($_SESSION['travelerID'],$group->getMemberIDs())
				|| $group->getAdministratorID() == $_SESSION['travelerID']
				|| ($group->isSubGroup() && in_array($_SESSION['travelerID'],$group->getParentGroup()->getMemberIDs())) ){
				if( $this->isLoggedTravelerSpammerToRecipient($recipientID=$recipient) ){
					$recipientSettings[$recipient] = array('isSpam' => 1);
				}else{
					$recipientSettings[$recipient] = array('isSpam' => 0);
				}
			}else{
				$recipientSettings[$recipient] = array('isSpam' => 1);
			}
		}
		$newMessage->setRecipientSettings($recipientSettings);
		
		$newMessage->setGroupIDs     ( array($group->getID())      );
		$newMessage->setGroups       ( array($group)               );
		
		$newMessage->setIsMessageIncludedInNotif(isset($_POST['chkSendNotification']) ? 1 : 0);
		
		$newMessage->send();
		self::sendEmailNotification($newMessage);
	}
		
	protected function validateComposeMessageForm() {
		$errors = array();
		
		// make sure there is a recipient if necessary
		if (isset($_POST['txtDestination']) && trim($_POST['txtDestination']) == '') {
			$errors["recipientList"] = "Please write the recipient(s) of your message.";
		}
		
		// make sure subject is provided
		if (trim($_POST['txtSubject']) == '') {
			$errors['subject'] = "Please write a subject of your message.";
		}
		
		// make sure message content is provided
		if (trim($_POST['txaMsgText']) == '') {
			$errors['body'] = 'You have not yet written a message.';
		}
		
		// SMART ERROR MESSAGING HERE
		if (trim($_POST["txtSubject"]) == '' && trim($_POST["txaMsgText"]) == '') {
			$errors["subjectAndBody"] = "Please write your message and its subject.";

			unset($errors["subject"], $errors["body"]);
		}

		return $errors;
	}
	
	protected function showMessageGroupStaffForm(gaGroup $group, array $errors = array()) {
		$profileComponent = $this->getGroupProfileComponent($group);
		$subnavigation    = $this->getGroupSubNavigation($group);

		$inboxLink  = "/messages.php";
		$outboxLink = "/messages.php?show_outbox";
		
		$siteDisplayName = $this->getSiteName();
		
		$notifyByEmail = true;
		if (isset($_POST['btnSend']) && !isset($_POST['chkSendNotification'])) {
			$notifyByEmail = false;
		}

		$this->setupGroupMainTemplate();
		$formTemplate = new Template();

		$formTemplate->set_vars(array(
				'recipientsListLocked' => true,
				'recipientDisplayName' => $group->getName(),
				'subject'              => isset($_POST['txtSubject']) ? $_POST['txtSubject'] : '',
				'body'                 => isset($_POST['txaMsgText']) ? $_POST['txaMsgText'] : '',
				'notifyByEmail'        => $notifyByEmail ? 'checked' : '',
				'profileComponent'     => $profileComponent->get_view(),
				'subnavigation'        => $subnavigation,
				'inboxLink'            => $inboxLink,
				'outboxLink'           => $outboxLink,
				'siteDisplayName'      => $siteDisplayName,
				'errors'               => $errors,
				'autoSuggestSetting'   => 'off',
				'suggestions'          => '',
				'helpText'             => '',
				'recipientType'	   	   => $this->mRecipientType
			));
		
		$formTemplate->out('travellog/views/messageCenter/tpl.FrmComposeMessage.php');
	}
	
	//######################### M I S C E L L A N E O U S ##################################

	protected function getGroupSubNavigation(gaGroup $group) {
		require_once 'travellog/model/Class.SubNavigation.php';
		$subNavigation = new SubNavigation();
		
		$subNavigation->setContext("GROUP");
		$subNavigation->setContextID($group->getID());
		
		if ($group->isParent()) {
			$subNavigation->setGroupType("ADMIN_GROUP");
		} else {
			$subNavigation->setGroupType("FUN_GROUP");
		}
		
		$subNavigation->setLinkToHighlight('MESSAGE_CENTER');
			
		return $subNavigation;
	}
	
	protected function getGroupProfileComponent(gaGroup $group) {
		require_once 'travellog/components/profile/controller/Class.ProfileCompFactory.php';
		$component = ProfileCompFactory::getInstance()->create(ProfileCompFactory::$GROUP_CONTEXT);
		$component->init($group->getID());
		
		return $component;
	}
	
	protected function getTravelerProfileComponent() {
		require_once 'travellog/components/profile/controller/Class.ProfileCompFactory.php';
		
		$component = ProfileCompFactory::getInstance()->create(ProfileCompFactory::$TRAVELER_CONTEXT);
		$component->init($_SESSION["travelerID"]);
		
		return $component;
	}
	
	protected function getTravelerSubNavigation() {
		require_once 'travellog/model/Class.SubNavigation.php';
		$subNavigation = new SubNavigation;
		
		$subNavigation->setContext("TRAVELER");
		$subNavigation->setContextID($_SESSION['travelerID']);
		$subNavigation->setLinkToHighlight("MESSAGES");
			
		return $subNavigation;
	}
	
	protected function requireAuthentication() {
		if (!(isset($_SESSION['travelerID']) && $_SESSION['travelerID'] != 0)) {
			header("location: /login.php?failedLogin&redirect=/messages.php?" . $_SERVER['QUERY_STRING']);
			exit;
		}
		$this->loggedTraveler = gaTravelerMapper::getTraveler($_SESSION['travelerID']);
	}
	
	protected function isLoggedTravelerSpammer(gaTraveler $recipient){
		if( is_null($recipient) ){
			return TRUE;
		}
		
		if( $this->isSiteAdminLogged() ){
			return FALSE;
		}
		
		if( gaTravelerMapper::isUserBlockedByTraveler($userID=$this->loggedTraveler->getID(),$travelerID=$recipient->getID()) ){
			return TRUE;
		}
		
		if( in_array($this->loggedTraveler->getID(), $recipient->getFriendIDs()) ){
			return FALSE;
		}
		
		$recipientGroupsJoined = $recipient->getMainGroupsJoined();
		foreach($recipientGroupsJoined AS $rj_group){
			// advisor/administrator to member, NOT SPAM
			if( $this->loggedTraveler->getID() == $rj_group->getAdministratorID() ){
				return FALSE;
			}
			// sender and recipient are group mates, NOT SPAM
			if( in_array($this->loggedTraveler->getID(),$rj_group->getMemberIDs())
				&& in_array($recipient->getID(),$rj_group->getMemberIDs()) ){
				return FALSE;
			}
		}
		
		$recipientAdministeredGroups = $recipient->getAdministeredGroups();
		foreach( $recipientAdministeredGroups AS $ra_group ){
			// member to advisor/administrator, NOT SPAM
			if( in_array($this->loggedTraveler->getID(),$ra_group->getMemberIDs())
			 	&& $ra_group->getAdministratorID() == $recipient->getID() ){
				return FALSE;
			}
		}
		
		return TRUE;
	}
	
	protected function isLoggedTravelerSpammerToRecipient($recipientID=0){
		if( $this->isSiteAdminLogged() ){
			return FALSE;
		}
		if( gaTravelerMapper::isUserBlockedByTraveler($userID=$this->loggedTraveler->getID(),$travelerID=$recipientID) ){
			return TRUE;
		}
		return FALSE;
	}
	
	protected function excludeSenderInRecipientList($senderID=0,$recipientIDs=array()){
		$tempRecipients = array();
		foreach($recipientIDs AS $recipientID){
			if( $recipientID != $senderID ){
				$tempRecipients[] = $recipientID;
			}
		}
		return $tempRecipients;
	}
	
	// ################# EMAIL NOTIFICATIONS ####################
	
	protected static function sendEmailNotification($newMessage){
		require_once("travellog/helper/Class.MessageHelper.php");
		require_once('travellog/model/notifications/Class.Notification.php');
		require_once('travellog/model/notifications/Class.NotificationComposer.php');
		require_once('travellog/model/notifications/Class.NotificationSender.php');
				
		$domainConfig 	= SiteContext::getInstance();
		$emailName		= $domainConfig->getEmailName();
		$siteName 		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
		$siteUrl 		= $domainConfig->getServerName();
		
	//	get groups to which the message is sent for notification footer variable
		$groupRecipients = gaPersonalMessageDM::getMessageGroupRecipients($newMessage->getID(), $returnObjects=TRUE);
	
	//	if there are group recipients, get the proper domain context for cobranded sites
		if( 0 < count($groupRecipients) ){
			$toGroup = $groupRecipients[0]->isSubgroup() ? $groupRecipients[0]->getParentGroup() : $groupRecipients[0];
			try{
				$siteConfig = Config::findByGroupID($toGroup->getID());
				if( $siteConfig instanceof Config ){
					$domainConfig   = $siteConfig;
					$emailName		= $domainConfig->getEmailName();
					$siteName 		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
					$siteUrl 		= $domainConfig->getServerName();
					if( FALSE === strpos($siteUrl,"dev.")
						&& (strpos($_SERVER["SERVER_NAME"],"dev.") || 0 === strpos($_SERVER["SERVER_NAME"],"dev.")) ){
						$siteUrlTokens = explode(".",$siteUrl);
						$temp = array();
						foreach($siteUrlTokens AS $token){
							if( "goabroad" == strtolower($token) ){
								$temp[] = "dev";
							}		
							$temp[] = strtolower($token);
						}
						$siteUrl = join(".",$temp);
					}
				}
			}catch(exception $e){
			}
		}

	//	$includeMessage = (isset($mData['sendNotification']) ? TRUE : FALSE);
		$footerVars = array(
			'siteName'	=> $siteName,
			'siteUrl'	=> $siteUrl
		);
		

	//	var_dump($newMessage);
		$sender = gaTravelerMapper::getTraveler($newMessage->getSenderID());
		$includeMessage = ( isset($_POST['chkSendNotification']) && 1 == $_POST['chkSendNotification'] ) ? true : false;
		
		$mNotifications = array();
			
		foreach($newMessage->getRecipientSettings() as $recipientID => $setting){
			//if( $setting["isSpam"] ){
			//	continue;
			//}
			
			$recipient = gaTravelerMapper::getTraveler($recipientID);
			
			if( $setting["isSpam"] ){
				$vars = array(	
					'recipient' 	=> $recipient->getUserName(),
					'message' 		=> $newMessage,
					'sender' 		=> $sender,
					'siteName' 		=> $siteName,
					'siteUrl' 		=> $siteUrl
				);
				$notification = new Notification;
				$notification->setMessage(NotificationComposer::compose($vars,'tpl.NewMessageCenterSpamPersonalMessageNotificationTemplate.php'));
				$notification->setSender($domainConfig->getAdminEmail());
				$notification->setSenderName($emailName);
				$notification->setSubject('Message marked as spam from ' . $sender->getName() );
				$notification->addRecipient('admin@goabroad.net');
			}else{
				//set proper footer variable, check if the recipient is the admin/staff of the group recipient 
				$footerVars = array(
					'siteName'	=> $siteName,
					'siteUrl'	=> $siteUrl
				);
				foreach($groupRecipients AS $groupRecipient){
					if( $groupRecipient->getAdministratorID() == $recipientID ){//admin
						$temp = array("group"=>$groupRecipient, "isAdvisor"=>TRUE);
						$footerVars = array_merge($footerVars,$temp);
						break;
					}elseif( in_array($recipientID,$groupRecipient->getStaffIDs()) ){//staff
						$temp = array("group"=>$groupRecipient, "isStaff"=>TRUE, "isSubgroup"=>$groupRecipient->isSubGroup());
						$footerVars = array_merge($footerVars,$temp);
						break;
					}
				}
				
				$vars = array_merge($footerVars, array(
					'newMessage'	=> $newMessage,
					'sender'		=> $sender,
					'recipient'		=> $recipient,
					'emailName'		=> $emailName,
					'isMessageIncluded' => $includeMessage,
					'notificationFooter'=> NotificationComposer::compose($footerVars,'tpl.IncNotificationFooter.php')
				));
			
				$body = NotificationComposer::compose($vars, 'tpl.NewMessageCenterNotificationTemplate.php');
			
				$notification = new Notification;
				$notification->setMessage($body);
				$notification->setSubject('You have a new message on '.$siteName);
				$notification->setSender($domainConfig->getAdminEmail());
				$notification->setSenderName($emailName);
				$notification->addRecipient($recipient->getEMailAddress());
			}
			$mNotifications[] = $notification;
			
		}
	//	var_dump($sender->getName());
	//	var_dump($sender->getUsername());
	//	foreach($mNotifications as $iNotif){
	//		var_dump($iNotif);
	//	}
		if( 0 < count($mNotifications) ){
			NotificationSender::getInstance()->send($mNotifications,true);
		}
		
	}
	
	private function isSiteAdminLogged(){
		if( isset($_SESSION["isSiteAdminLogin"]) && $_SESSION["isSiteAdminLogin"] ){
			return TRUE;
		}
		return FALSE;
	}
	
}