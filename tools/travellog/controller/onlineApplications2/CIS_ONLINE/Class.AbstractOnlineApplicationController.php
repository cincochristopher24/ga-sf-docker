<?php
/***
 * REMINDER TO SELF:
 * 				REFACTOR CODE - NRL July 22, 2010
 *
 *
 ****/
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.CISOnlineAppFieldsMapper.php');
require_once('travellog/factory/Class.FileFactory.php');

class AbstractOnlineApplicationController {

	protected $file_factory 	= NULL;
	protected $mTemplate		= null;
	protected $selectedStep     = 1;
	protected $lastInsertedPersonalInfoID;
	protected $action_class_names = array();


	const VIEW_INTRO				= 'introductionPage',
		  VIEW_PROGRAM_INFO_FORM  = 'programInformationForm',
		  VIEW_AGREEMENT_FORM     = 'agreementAndWaiverForm',
		  VIEW_PERSONAL_INFO_FORM   = 'personalInformationForm2';


	private static $steps = array(
								1 => self::VIEW_PERSONAL_INFO_FORM,
								2 => self::VIEW_PROGRAM_INFO_FORM
								);

	private static $class_names = array(
								self::VIEW_PERSONAL_INFO_FORM => array('PersonalInfo',
																		'AddressInfo',
																		'PassportDetails',
																		'EmergencyContact',
																		'SchoolInfo',
																		'AdditionalInfo',
																		'BillingInfo'),

								self::VIEW_PROGRAM_INFO_FORM => array('ProgramInformation', 'HousingOptions', 'AdditionalInfo'),
								self::VIEW_AGREEMENT_FORM => array('Agreement'),
								);




	public function __construct(){
		$this->file_factory = FileFactory::getInstance();

		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CIS/views/onlineApp'));
		$this->file_factory->setPath('CSS', '/custom/CIS/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CIS/views/onlineApp/');
		$this->file_factory->registerClass('ToolMan', 			array('path'=>''));
		$this->file_factory->registerClass('Template', 			array('path'=>''));
		$this->file_factory->registerClass('Comments', 			  array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('Agreement',            array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('UserCoupon',            array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));

		$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
		$this->file_factory->invokeStaticClass('Template','setMainTemplateVar', array('layoutID', 'online_application'));
		$this->file_factory->registerClass('State');
	}



	public function performAction(){
		$tplOnlineApplication = $this->file_factory->getClass('Template');
		//var_dump('ec');exit;
 		$this->action = (isset($_GET['action']) ? $_GET['action'] : 'view');
 		$this->selectedStep = (isset($_GET['step']) ? $_GET['step'] : '');
 		$this->lastInsertedPersonalInfoID = array('PersonalInfoID' => (isset($_GET['pID']) ? $_GET['pID'] : ''));

 	/*	if(!isset($_GET['step']) && !isset($_GET['action'])) {
			//var_dump("dsfsdf--s");
			$this->mTemplate = 'tpl.'.self::VIEW_INTRO.'.php';

			$tplOnlineApplication->set('pageTitle', 'Online Application');
			$tplOnlineApplication->out('travellog/views/onlineApplications/cis/'.$this->mTemplate);
		}
	*/

		//step=1&action=view
		if(!isset($_GET['action']) || $this->action == 'view' ) {

			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.TermsPeer.php');
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.ProgramsPeer.php');
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.OptionsPeer.php');
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.HousingOptionsPeer.php');
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.DurationOptionsPeer.php');
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.UserCouponPeer.php');
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.SchoolInfo.php');

			//var_dump('here i\he ');exit;
			//var_dump($this->lastInsertedPersonalInfoID);exit;
			$this->mTemplate = 'tpl.'.self::VIEW_PERSONAL_INFO_FORM.'.php';
			//var_dump($this->mTemplate = 'tpl.'.self::VIEW_PERSONAL_INFO_FORM.'.php');exit;
			//$applicationProgressMenu = $this->prepareApplicationProgressMenu();

	// FOR PROGRAM NFO
			$terms = TermsPeer::retrieveAll();

			$programs = ProgramsPeer::retrieveAllWithTerms();

			$options = OptionsPeer::retrievelOptionsOnProgramsAndTerms();

			$housingOptions = HousingOptionsPeer::retrieveHousingOptionsPerProgram();

			//var_dump($terms);exit;
			$jsPrograms = array();
			foreach($programs as $program){
				$jsProgramsWithTerms = array(
					'id' => $program->getID(),
					'name' => $program->getName(),
					'termID' => $program->getTerm(),
					'hasStartDate' => $program->hasStartDate()
					);
				$jsPrograms[] = $jsProgramsWithTerms;
			}
		$jsHousingOptions = array();
			foreach($housingOptions as $hOption){
				$jsHousingOptions[] = array('id' => $hOption->getID(),
											'programID' => $hOption->getProgramID(),
											'termID' => $hOption->getTermID(),
											'name' => $hOption->getName());
			}

		$jsOptions = array();
			foreach($options as $option){
				$jsOptionsWithProgramAndTerm = array(
						'id' => $option->getID(),
						'programID' => $option->getProgramID(),
						'termID' => $option->getTermID(),
						'name' => $option->getName()
						);

				$jsOptions[] = $jsOptionsWithProgramAndTerm;
			}

		for($x=1; $x<=12; $x++)
			{
				$months[] = date('F', mktime(0,0,0, $x, 1));
			}



			$pID = $this->lastInsertedPersonalInfoID['PersonalInfoID'];
			//$couponCode = null;
			//if ($pID) {
				$couponCode = UserCouponPeer::retrieveByPersonalInfoID($pID);
			//}



			//$schoolInfo = new SchoolInfo();
			//$cpaSchools = $schoolInfo->getCpaSchools();

			$tplOnlineApplication = $this->file_factory->getClass('Template');
			//var_dump(json_encode($cpaSchools));exit;
			//$tplOnlineApplication->set('cpaSchools', json_encode($cpaSchools));
			$tplOnlineApplication->set('pageTitle', 'Online Application');
			$states = $this->prepareStateList();
			$tplOnlineApplication->set('states',$states);

	// FOR PROGRAM INFO

			$tplOnlineApplication->set('terms',$terms);
			$tplOnlineApplication->set('comment','');
			//$tplOnlineApplication->set('pID',$pID);
			$tplOnlineApplication->set('jsPrograms',$jsPrograms);
			$tplOnlineApplication->set('jsOptions',$jsOptions);
			$tplOnlineApplication->set('programs',$programs);
			$tplOnlineApplication->set('startMonths',$months);
			$tplOnlineApplication->set('jsHousingOptions',$jsHousingOptions);
			$tplOnlineApplication->set('couponCode',$couponCode);

			//$tplOnlineApplication->set('applicationProgressMenu', $applicationProgressMenu);
			$tplOnlineApplication->out('travellog/views/onlineApplications/cis/'.$this->mTemplate);

		}
	/*	else if($this->action == 'view' && isset($_GET['pID'])){

			if(empty($_GET['backTrack'])){
			//var_dump('dsf');
			}
		}
		*/
		else if ($this->action == 'save') {
				 	if ($this->selectedStep == 1) {



				 //$personalInfo = $this->save();

				 $this->save();


				 require_once 'travellog/model/onlineApplications/CIS_onlineApplication/Class.ProgramInformation2.php';

	 			$personalId = new ProgramInformation2();
				$persId = $personalId->getPersonalId();
				$pID = $persId['id'];

				 require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.UserCouponPeer.php');
				 $couponCode = UserCouponPeer::retrieveByPersonalInfoID($pID);
				 // $approvedCoupons = UserCouponPeer::getApprovedCouponCodes();



				 // COUPON CODE
                 /*
				if($couponCode AND in_array($couponCode->getName(),$approvedCoupons)){
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('OnlineApplication.php?step=2&action=thankyoucpa'));
				}
                  * 
                  */
				
				if($_POST['ProgramInfo']['FirstProgramID'] == 300)
				{
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('OnlineApplication.php?step=2&action=thankyoucpa'));
				}
				
				 if(isset($_POST['SchoolInfo']['OtherSchoolName1']))
				 {

				 	$schoolcpa = $_POST['SchoolInfo']['SchoolID'];
				 	//var_dump($schoolcpa);
				 	$pos = strrpos($schoolcpa, "partner");
						if ($pos === false) { // note: three equal signs
						    // not found...
							$this->createCSV();
							$this->file_factory->invokeStaticClass('ToolMan','redirect',array('http://www.cisabroad.com/redirect-to-deposit'));
						    //$this->file_factory->invokeStaticClass('ToolMan','redirect',array('OnlineApplication.php?step=2&action=paypal'));
						}
						else{
							// $this->file_factory->invokeStaticClass('ToolMan','redirect',array('OnlineApplication.php?step=2&action=thankyoucpa'));
							$this->createCSV();
							$this->file_factory->invokeStaticClass('ToolMan','redirect',array('http://www.cisabroad.com/payment/thank-you?deposit=true'));
						}
				 }
				 else {
					$this->createCSV();
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('http://www.cisabroad.com/redirect-to-deposit'));
				 	//$this->file_factory->invokeStaticClass('ToolMan','redirect',array('OnlineApplication.php?step=2&action=paypal'));
				 }

				//if(isset($_POST['waiveme'])){
					//$this->file_factory->invokeStaticClass('ToolMan','redirect',array('OnlineApplication.php?step=2&action=thankyoucpa'));
				//}else {
				//$this->file_factory->invokeStaticClass('ToolMan','redirect',array('OnlineApplication.php?step=2&action=paypal'));
				//}
		 		//$this->file_factory->invokeStaticClass('ToolMan','redirect',array('OnlineApplication.php?step=2&action=view&pID='.$personalInfo->getId()));

		 	}



		}
		else if ($this->action == 'paypal'){

			$this->createCSV();
			$this->mTemplate = 'tpl.paypall.php';
			$tplOnlineApplication->set('pageTitle', 'Online Application');
			$tplOnlineApplication->out('travellog/views/onlineApplications/cis/'.$this->mTemplate);
           
		}

	else if ($this->action == 'thankyoucpa'){
			//var_dump('hi');exit;
			$this->createCSV();
			$this->mTemplate = 'tpl.thankYouCpa.php';
			$tplOnlineApplication->set('pageTitle', 'Online Application');
			$tplOnlineApplication->out('travellog/views/onlineApplications/cis/'.$this->mTemplate);


		}
     else if ($this->action == 'paypal_submitted'){
          $this->mTemplate = 'tpl.paypalThankYou.php';
          $tplOnlineApplication->set('pageTitle', 'Online Application');
		  $tplOnlineApplication->out('travellog/views/onlineApplications/cis/'.$this->mTemplate);
     }
     else if ($this->action == 'paypal_return'){
          $this->mTemplate = 'tpl.paypalReturn.php';
          $tplOnlineApplication->set('pageTitle', 'Online Application');
		  $tplOnlineApplication->out('travellog/views/onlineApplications/cis/'.$this->mTemplate);
     }
        
	}
private function createCSV(){

	require_once 'travellog/model/onlineApplications/CIS_onlineApplication/Class.PersonalInfo2.php';
	 require_once 'travellog/model/onlineApplications/CIS_onlineApplication/Class.ProgramInformation2.php';

	 $personalId = new ProgramInformation2();
			$persId = $personalId->getPersonalId();
			$pID = $persId['id'];

		//$learnfromOthers =  $_POST['AdditionalInfo']['Others'];
		//var_dump($learnfromOthers);


	//	$this->lastInsertedPersonalInfoID['PersonalInfoID'] = 61;
		//$pID = /*$_GET['pID'];*/$this->lastInsertedPersonalInfoID['PersonalInfoID'];
		 //echo $pID; exit;

		$objArr = array();
		foreach(self::$class_names as $key => $values){
			foreach($values as $cls){
				$clsName = 'Class.'.$cls.'Mapper.php';
				$file = dirname(__FILE__).'/../../../model/onlineApplications/CIS_onlineApplication/Mapper/'.$clsName;
				if(file_exists($file)){
					require_once($file);
					$objArr[$cls] = call_user_func(array($cls.'Mapper', 'retrieveByPersonalInfoID'), $pID);

				}
			}
		}



		// get the comment of the user if there is

		$userID = $pID;
		$com = $this->file_factory->getClass('Comments');

		$comment = $com->retrieveCommentByUserId($userID);


		// prepare applicant name to be used as filename for the files to be created
		$name = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getName();


		$id = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getID();

		$applicantName = str_replace(' ','_',$name);

		// prepare list for csv file

		require_once('travellog/helper/onlineApplications/Class.CIS_CSVListCreator.php');

		$csvList = CIS_CSVListCreator::create($objArr);


		// creating csv file

		$csvFile = 'application_'.$applicantName.'_'.$id.'.csv';


		$csvName  = dirname(__FILE__).'/../../../../../goabroad.net/onlineapplication/tmp/'.$csvFile;


		$csvFileHandle = fopen($csvName, 'w') or die("cannot open file");
		foreach ($csvList as $line) {
		    fputcsv($csvFileHandle, $line);
		}
		fclose($csvFileHandle);


		/*
		header('Content-type: application/csv');
		header("Content-Disposition: inline; filename=".$csvFile);
		*/



		// prepare template for text file
		$txtFileTemplate = $this->file_factory->getClass('Template');
		$txtFileTemplate->set('objArr', $objArr);
		$txtFileTemplate->set('comment', $comment);
		$txtTmplate = $txtFileTemplate->fetch('travellog/views/onlineApplications/cis/tpl.txtFile.php');


		// creating text file

		$txtFile = 'application_'.$applicantName.'_'.$id.'.txt';
		$filename = dirname(__FILE__).'/../../../../../goabroad.net/onlineapplication/tmp/'.$txtFile;

		$ourFileHandle = fopen($filename, 'w') or die("cannot open file");

		fwrite($ourFileHandle, $txtTmplate);
		fclose($ourFileHandle);




		// for debugging purpose only

		$remoteAddr = Getenv("REMOTE_ADDR");

	//	if($remoteAddr == '121.96.35.154'){
	//		var_dump(file_exists($csvName));
	//		var_dump(file_exists($filename));
	//	}


		require_once('class.phpmailer.php');

			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->IsHTML(false);

			//$mail->From = "jpalm@cisabroad.com";
		//	$mail->From = "nash.lesigon@goabroad.com";
			$mail->From = "aheddens@cisabroad.com";


			//$mail->From = "admissions@cisabroad.com";
			$mail->FromName = "Online Application";
			$mail->Subject = "Online Application - $name";
			$mail->Body = "Application";

			$mail->Addattachment($csvName, $csvFile);
			$mail->Addattachment($filename, $txtFile);
			
			
			// edits : NASH - Oct. 18, 2011
			//		   added bon in address
			
			$mail->AddAddress("bon.lariosa@goabroad.com");
			// $mail->AddAddress("info@cisabroad.com");
			// $mail->AddAddress("thea.moraleta@goabroad.com");
			//$mail->AddAddress("jonathan.antivo@goabroad.com");
			//$mail->AddBCC("jonathan.antivo@goabroad.com");
			// $mail->AddBCC("nash.lesigon@goabroad.com"); 
			$mail->AddBCC("jonathan.antivo@goabroad.com");



			//$this->saveDoneApplication();

			if($mail->Send()){

				// temporarily dont delete files for backup since mail server is unstable - Oct 30, 2009
				unlink($filename);
				unlink($csvName);
			//	if($remoteAddr == '121.96.35.154'){
			//		echo 'sent'; exit;
			//	}
			}
			else{

			//	if($remoteAddr == '121.96.35.154'){
			//		echo 'not sent<br/>';
			//		var_dump($mail);
			//		var_dump($mail->smtp->error);
					//$mail->smtp->error['error']

			//		exit;
			//	}
			//	echo "waray kasend";
			//	echo "<pre>";
			//	var_dump($mail->smtp->error);
			//	exit;
			}


	//	echo "<br/>end";
	}


public function save() {

	$FriendGrant =  $_POST['AdditionalInfo']['FriendGrant'];
	$CampusRep =  $_POST['AdditionalInfo']['CampusRep'];


	require_once 'travellog/model/onlineApplications/CIS_onlineApplication/Class.PersonalInfo2.php';
	 require_once 'travellog/model/onlineApplications/CIS_onlineApplication/Class.ProgramInformation2.php';



	//$_POST['PersonalInfo]['FirstName'];
//	$newClass = new NameOfClass();
	//$newClass->setName($_POST['PersonalInfo]['FirstName']);
	//$newClass->save();
//var_dump($_POST['PersonalInfo']);exit;

		//var_dump($_POST);exit;

		 $fname =  $_POST['PersonalInfo']['FirstName'];
		 $LastName =  $_POST['PersonalInfo']['LastName'];
		 $Gender =  $_POST['PersonalInfo']['Gender'];
		 $mm =  $_POST['PersonalInfo']['mm'];
		 $dd =  $_POST['PersonalInfo']['dd'];
		 $yyyy =  $_POST['PersonalInfo']['yyyy'];
		 $birthdate = $yyyy.'-'.$mm.'-'.$dd;
		 $Email =  $_POST['PersonalInfo']['Email'];
		 $AltEmail =  $_POST['PersonalInfo']['AltEmail'];
		 $BillingEmail =  $_POST['PersonalInfo']['BillingEmail'];
		 $ShirtSize = $_POST['PersonalInfo']['ShirtSize'];

		// for address
		$Street1 = $_POST['AddressInfo']['Street1'];
		$City1 = $_POST['AddressInfo']['City1'];
		$State1 = $_POST['AddressInfo']['State1'];
		$Zip1 = $_POST['AddressInfo']['Zip1'];
		$PhoneNumber1 = $_POST['AddressInfo']['PhoneNumber1'];
		$Cellphone = $_POST['AddressInfo']['Cellphone'];

		// for school
		$SchoolID = $_POST['SchoolInfo']['SchoolID'];
		$schoolInfo = $_POST['SchoolInfo'];
		$schooltype = null;
		//var_dump($schoolInfo['SchoolID']);exit;
		//var_dump($schoolInfo); exit;

		//$OtherSchoolName1 = $_POST['SchoolInfo']['OtherSchoolName1'];
		if(!isset($schoolInfo['Hs']) AND !isset($schoolInfo['Other'])){

			if($schoolInfo['SchoolID'] == '0'){
				$schoolInfo['OtherSchoolName'] = $schoolInfo['OtherSchoolName1'];
				//var_dump('adi ak'); exit;
			}
			else{
				$id = explode("_", $schoolInfo['SchoolID']);
				$schoolInfo['SchoolID'] = $id[1];
				$schooltype = ("usu" == $id[0]) ? 0 : 1;
				$schoolInfo['OtherSchoolName'] = (0 < $id[1]) ? $schoolInfo['OtherSchoolName'] : $schoolInfo['OtherSchoolName1'];
			}

		}

		else {
				if(!isset($schoolInfo['Hs'])){
					$schoolInfo['SchoolID'] = 0;
									$schooltype = 3;
								}
								else {
									$schoolInfo['SchoolID'] = 0;
									$schooltype = 2;
								}

			}


		//$Other = $_POST['SchoolInfo']['Other'];
		//$OtherSchoolName = $_POST['SchoolInfo']['OtherSchoolName'];
		$Major = $_POST['SchoolInfo']['Major'];
		$YearInSchool = $_POST['SchoolInfo']['YearInSchool'];
		$GenAverage = $_POST['SchoolInfo']['GenAverage'];
		$school_contact = $_POST['SchoolInfo']['Contact'];
		$school_email = $_POST['SchoolInfo']['Email'];


		$insert = new PersonalInfo2();
		 // $insert->setID($fname);
		  $insert->setFirstName($fname);
		  $insert->setLastName($LastName);
		  $insert->setGender($Gender);
		  $insert->setBirthday($birthdate);
		  $insert->setEmail($Email);
		  $insert->setAltEmail($AltEmail);
		  $insert->setBillingEmail($BillingEmail);
		  $insert->setShirtSize($ShirtSize);

		//insert to address
		 $insert->setStreet1($Street1);
		 $insert->setCity1($City1);
		 $insert->setState1($State1);
		 $insert->setZip1($Zip1);
		 $insert->setPhoneNumber1($PhoneNumber1);
		 $insert->setCellphone($Cellphone);

		 // insert school
		 $insert->setSchoolID($schoolInfo['SchoolID']);
		 $insert->setOtherSchoolName($schoolInfo['OtherSchoolName']);
		 $insert->setSchoolType($schooltype);
		 $insert->setMajor($Major);
		 $insert->setYearInSchool($YearInSchool);
		 $insert->setGenAverage($GenAverage);
		 $insert->setSchoolContact($school_contact);
		 $insert->setSchoolEmail($school_email);


		  $insert->save();


		  //return $insert;



		  //  program information *************************


		// program info
		$personalId = new ProgramInformation2();
			$perId = $personalId->getPersonalId();

		$Term =  $_POST['ProgramInfo']['Term'];
		$Year =  $_POST['ProgramInfo']['Year'];
		$FirstProgramID =  $_POST['ProgramInfo']['FirstProgramID'];
		$FirstOptionID =  $_POST['ProgramInfo']['FirstOptionID'];
		$HousingOptionID =  $_POST['ProgramInfo']['HousingOptionID'];
		$DurationOptionID =  $_POST['ProgramInfo']['DurationOptionID'];
		$StartMonth =  $_POST['ProgramInfo']['StartMonth'];
		$StartDay =  $_POST['ProgramInfo']['StartDay'];
		// $Interest =  $_POST['ProgramInfo']['Interest'];

		  # -- AGREEMENT
		 $agreementDate = array(CISOnlineAppFieldsMapper::APPLICATION_DATE => Date("Y-m-d H:i:s"));
		 $signature = $_POST['Agreement']['Signature'];

   		 	// addtional info

		$LearnProgramFrom =  $_POST['AdditionalInfo']['LearnProgramFrom'];
		$Others =  $_POST['AdditionalInfo']['Others'];
		$FinancialAid =  $_POST['AdditionalInfo']['FinancialAid'];
		$CampusRep =  $_POST['AdditionalInfo']['CampusRep'];

		$FriendGrant =  $_POST['AdditionalInfo']['FriendGrant'];
		$goAgainGrant = $_POST['goAgainGrant'];

		//$CampusRep =  $_POST['CampusRep'];
		//$FriendGrant =  $_POST['AdditionalInfo']['FriendGrant'];
		$txtAcomment =  $_POST['txtAcomment'];


		//var_dump($_POST['CouponCode']);exit;
		//var_dump($_POST);exit;

		$insertprog = new ProgramInformation2();
		$insertprog->setPersonalInfoID($perId['id']);
		$insertprog->setTerm($Term);
		$insertprog->setYear($Year);
		$insertprog->setFirstProgramID($FirstProgramID);
		$insertprog->setFirstOptionID($FirstOptionID);
		$insertprog->setHousingOptionID($HousingOptionID);
		$insertprog->setDurationOptionID($DurationOptionID);
		$insertprog->setStartMonth($StartMonth);
		$insertprog->setStartDay($StartDay);
		// $insertprog->setInterest($Interest);


		// additonal info
		$insertprog->setLearnProgramFrom($LearnProgramFrom);
		$insertprog->setOthers($Others);
		$insertprog->setFinancialAid($FinancialAid);
		$insertprog->setCampusRep($CampusRep);
		$insertprog->setFriendGrant($FriendGrant);
		$insertprog->setCampusRep($CampusRep);
		$insertprog->setGoAgainGrant($goAgainGrant == 0 ? 'No' : 'Yes');

		# AGREEMENT
		$insertprog->setPersonalInfoID($perId['id']);
		$insertprog->setSignature($signature);
		$insertprog->setApplicationDate($agreementDate['ApplicationDate']);
		# comment
		$insertprog->setMessage($txtAcomment);


		$insertprog->save();


		// coupon code
		$CouponCode =  $_POST['CouponCode'];
		if(isset($_POST['CouponCode'])){
			$this->processCouponCode();
			}


}
		// ####### PROCESS COUPON CODE #######

		public function processCouponCode(){
		require_once 'travellog/model/onlineApplications/CIS_onlineApplication/Class.ProgramInformation2.php';
		$personalId = new ProgramInformation2();
		$perId = $personalId->getPersonalId();

		$pID = $perId['id'];



		$coupon = $this->file_factory->getClass('UserCoupon');
		$coupon->setPersonalInfoID($pID);

		if(!empty($_POST['CouponCode'])){
			$code = $_POST['CouponCode'];
			$coupon->setName($code);
			$coupon->save();
		}
		else {
			if($coupon->hasCode()){
				$coupon->delete();
			}
		}
	}



	public function prepareCountryList(){
		$countryList = $this->file_factory->invokeStaticClass('Country','getCountryList');
		return $countryList;
	}

	public function prepareStateList(){
		$stateList = $this->file_factory->invokeStaticClass('State','getStateList');
		return $stateList;
	}


}
?>