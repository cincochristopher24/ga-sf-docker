<?php
/*** 
 * REMINDER TO SELF: 
 * 				REFACTOR CODE - NRL July 22, 2010
 * 
 * 
 ****/ 
require_once('travellog/model/onlineApplications/CIS_onlineApplication/Class.CISOnlineAppFieldsMapper.php');
require_once('travellog/model/Class.SessionManager.php');
require_once('travellog/factory/Class.FileFactory.php');


require_once('travellog/web_services/Class.gaCISOnlineAppWebService.php');

class AbstractOnlineApplicationController {
	
	protected $file_factory 	= NULL;
	protected $obj_session  	= NULL;
	protected $loggedTraveler 	= NULL;
	protected $pageTitle		= 'Online Application';
	protected $mTemplate		= null;
	protected $selectedStep     = 1;
	protected $lastInsertedPersonalInfoID;
	protected $action_class_names = array();
	protected $hasUnfinishedStep = false;
	protected $procID = null;
	
	const	LAST_STEP = 6;
	
	const	VIEW_FORM               = 'viewOnlineAppForm',
			// form names
			VIEW_INTRO				= 'introductionPage',
			VIEW_CHECKLIST			= 'checklistPage',
			VIEW_PERSONAL_INFO_FORM = 'personalInformationForm',
			VIEW_PROGRAM_INFO_FORM  = 'programInformationForm',
			VIEW_PERSONAL_STATEMENT_FORM = 'personalStatementForm',
			VIEW_DOCUMENTS_FORM     = 'documentsToMailForm',
			VIEW_AGREEMENT_FORM     = 'agreementAndWaiverForm',
			VIEW_BILLING_FORM       = 'billingAndPaymentForm',
			
			// function names
			SAVE_PERSONAL_INFO      = 'savePersonalInfo',
			SAVE_PROGRAM_INFO       = 'saveProgramInfo',
			SAVE_PERSONAL_STATEMENT = 'savePersonalStatement',
			SAVE_AGREEMENT          = 'saveAgreement',
			SAVE_BILLING_INFO       = 'saveBillingInfo',
			
			APOLOGY_PAGE = 'apologypage',
			THANKYOU_PAGE = 'thankyou',
			PAYPALL_PAGE = 'paypall';
	
	
	private static $steps = array(
								1 => self::VIEW_PERSONAL_INFO_FORM,
								2 => self::VIEW_PROGRAM_INFO_FORM,
								3 => self::VIEW_PERSONAL_STATEMENT_FORM,
								4 => self::VIEW_DOCUMENTS_FORM,
								5 => self::VIEW_AGREEMENT_FORM,
								6 => self::VIEW_BILLING_FORM);
								
	private static $save = array(1 => self::SAVE_PERSONAL_INFO,
								2 => self::SAVE_PROGRAM_INFO,
								3 => self::SAVE_PERSONAL_STATEMENT,
								4 => null,
								5 => self::SAVE_AGREEMENT,
								6 => self::SAVE_BILLING_INFO);
	
	private static $class_names = array(
								self::VIEW_PERSONAL_INFO_FORM => array('PersonalInfo',
																		'AddressInfo',
																		'PassportDetails',
																		'EmergencyContact',
																		'SchoolInfo',
																		'AdditionalInfo',
																		'BillingInfo'),
												
								self::VIEW_PROGRAM_INFO_FORM => array('ProgramInformation', 'HousingOptions', 'AdditionalInfo'),
								self::VIEW_PERSONAL_STATEMENT_FORM => array('PersonalStatement'),
								self::VIEW_DOCUMENTS_FORM => array(),
								self::VIEW_AGREEMENT_FORM => array('Agreement'),
								self::VIEW_BILLING_FORM   => array('BillingInfo', 'AddressInfo')
								);
	
	/**
	 TODO::::: REFACTOR CODE ASAP - nash	  
	 **/ 
	
	public function __construct(){
		
		$this->file_factory = FileFactory::getInstance();
		$this->obj_session  = SessionManager::getInstance();
		
		$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/custom/CIS/views/onlineApp'));
		$this->file_factory->setPath('CSS', '/custom/CIS/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/custom/CIS/views/onlineApp/');		
		
		$this->file_factory->registerClass('ToolMan', array('path'=>''));
		$this->file_factory->registerClass('HelperGlobal', array('path'=>''));
		$this->file_factory->registerClass('Template', array('path'=>''));
		$this->file_factory->registerClass('Traveler');
		$this->file_factory->registerClass('AdminGroup');
		$this->file_factory->registerClass('SubNavigation');
		
	//	$this->file_factory->registerClass('CISOnlineAppFieldsMapper',      array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('PersonalInfo',         array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('AddressInfo',          array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('PassportDetails',      array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('EmergencyContact',     array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('SchoolInfo',           array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('AdditionalInfo',       array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('ProgramInformation',   array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('PersonalInfoToProgram',array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('BillingInfo',          array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('PersonalStatement',    array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('Agreement',            array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		
		$this->file_factory->registerClass('HousingOptions',            array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		
		$this->file_factory->registerClass('DoneProcess',         array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('Steps',         array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		
		$this->file_factory->registerClass('Comments',            array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		$this->file_factory->registerClass('UserCoupon',            array('path'=>'travellog/model/onlineApplications/CIS_onlineApplication'));
		
		$this->file_factory->registerClass('Country');
		$this->file_factory->registerClass('State');
	}
	
	public function performAction(){
	//	echo 'two';
	//	var_dump($_GET); exit;
	//	
		$this->action = (isset($_GET['action']) ? $_GET['action'] : 'view');
	//	echo "action : ".$this->action; exit;
		if($this->action == "createCSV" AND isset($_GET['nash'])){
			$_GET['pID'] = 1540;
			$this->createCSV(); exit;
		}
		
		$this->selectedStep = (isset($_GET['step']) ? $_GET['step'] : '');
		// this variable is also like the process id
		$this->lastInsertedPersonalInfoID = array('PersonalInfoID' => (isset($_GET['pID']) ? $_GET['pID'] : ''));
		$onlineAppUserID = $this->obj_session->get('onlineAppUserID');
		
		if(!$onlineAppUserID){
			$this->file_factory->invokeStaticClass('ToolMan','redirect',array('login.php'));
		}
		if(isset($_GET['logout'])){
			$this->endSession();
			$this->file_factory->invokeStaticClass('ToolMan','redirect',array('login.php'));
		}
		// if a user manually enters the url
		if($this->selectedStep > 1 AND !isset($_GET['pID'])){
			$this->file_factory->invokeStaticClass('ToolMan','redirect',array('OnlineApplication.php?step=1&action=view'));
		}
		
		
		if (isset($_GET['pID'])){
			
			if($this->isApplicationDone()){
				$this->endSession();
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('login.php'));
			//	$this->file_factory->invokeStaticClass('ToolMan','redirect',array('OnlineApplication.php?step=1&action=view'));
			}
			
			else if (!$this->isOwner()){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('OnlineApplication.php?step=1&action=view'));
			}
			
		}
		
	//	echo "here";
		// check if the user has unfinished application
		// if TRUE, redirect to unfinished application
		$doneStep = $this->checkStepsDone();
		$cnt = $doneStep;
		
		if(0 < $doneStep){
			// we only redirect the user when he is accessing the step 1
			// and when the backTrack is not present in the url
			// 
			if($this->selectedStep == 1 and !isset($_GET['backTrack']) and ($this->action == 'view')){
				// if our counter is less than 7, we use the counter as the redirect page;
				//var_dump('here');
				if(7 > $cnt){
				//	var_dump('a');
					$step = $cnt;
					$personalInfoID = $this->lastInsertedPersonalInfoID['PersonalInfoID'];
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('OnlineApplication.php?step='.$step.'&action=view&pID='.$personalInfoID.'&backTrack'));
				}
				// but if not, we use the $doneStep value
				else {
				//	var_dump('b');
					$step = $doneStep;
					$personalInfoID = $this->lastInsertedPersonalInfoID['PersonalInfoID'];
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('OnlineApplication.php?step='.$step.'&action=view&pID='.$personalInfoID.'&backTrack'));
				}
			}
		}
	//	var_dump($_GET);
	//	echo 'didi na';
		if($this->selectedStep == 4){
			$this->saveStepsDone();
		}
		if(0 < $this->selectedStep){
		//	var_dump('greater than');
			if($this->selectedStep > 7){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('OnlineApplication.php'));
			}
			
			if($this->action == 'view'){
			//	var_dump('show this template dapat');
				$this->mTemplate = 'tpl.'.self::$steps[$this->selectedStep].'.php';
				$this->action_class_names = self::$class_names[self::$steps[$this->selectedStep]];
				$this->showTemplate1();
			}
			else if ($this->action == 'save'){
			//	echo self::$save[$this->selectedStep]; exit;
				if(!empty($_POST)){
					self::$save[$this->selectedStep];
					$this->saveValues();
				}
				else {
					$step = $this->selectedStep;
					$personalInfoID = $this->lastInsertedPersonalInfoID['PersonalInfoID'];
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('OnlineApplication.php?step='.$step.'&action=view&pID='.$personalInfoID));
				}
			}
		}
		
		else {
			if($this->action == 'viewChecklist'){
				$this->mTemplate = 'tpl.'.self::VIEW_CHECKLIST.'.php';
			}
			else if ($this->action == 'almostDone'){
			//	$this->mTemplate = 'tpl.'.self::APOLOGY_PAGE.'.php';
				$this->mTemplate = 'tpl.'.self::PAYPALL_PAGE.'.php';
			}
			else if ($this->action == 'processComplete'){
				$this->mTemplate = 'tpl.'.self::THANKYOU_PAGE.'.php';
			}
			else {
				$this->mTemplate = 'tpl.'.self::VIEW_INTRO.'.php';
			}
			
			$this->showTemplate1();
		}
	
	}
	
	public function redirect1(){
		if($this->selectedStep == 6 AND !isset($_GET['saveAndExit'])){
			//header("Location: https://www.ecsi.net/cgi-bin/webx.exe");
			if($_POST['Submit'] == 'SUBMIT'){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('OnlineApplication.php?action=processComplete'));
			}
			else {
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('OnlineApplication.php?action=almostDone'));
			}
			
		}
		else if(!isset($_GET['saveAndExit'])){
			$personalInfoID = $this->lastInsertedPersonalInfoID['PersonalInfoID'];
			$back = "";
			if(isset($_GET['backTrack'])){ 
				$back = '&backTrack';
				$step = $this->selectedStep - 1;}
			else { $step = $this->selectedStep + 1; }
			
			$this->file_factory->invokeStaticClass('ToolMan','redirect',array('OnlineApplication.php?step='.$step.'&action=view&pID='.$personalInfoID.$back));
		}
		else{
		//	$location = 'http://secure.goabroad.net/cis/mysessionhandler.php?logout';
			
		//	header("location: $location");
		//	exit;
			$this->file_factory->invokeStaticClass('ToolMan','redirect', array('login.php'));
		}
	}
	
	public function saveValues(){
	//	$remoteAddr = Getenv("REMOTE_ADDR");
	//	if($remoteAddr == '121.96.35.154'){
	//		var_dump($_POST);
	//		exit;
	//	} 
		$this->doPreparePersonalInfoValues();
		$this->doPrepareProgramInfoValues();
		$this->doPreparePersonalStatementValues();
		$this->doPrepareAgreementValues();
	//	$this->doPrepareBillingInfoValues();
		
		$this->doPrepareAdditionalInfos();
		
		
		$this->saveStepsDone();
		
	//	echo 'adi na';
	//	var_dump($this->isApplicationDone());
		
		$this->updateStudentDB();
	
		if($this->selectedStep == self::LAST_STEP AND $this->isApplicationDone() == false AND !isset($_GET['saveAndExit'])){
			$this->createCSV();
		}
		$this->redirect1();
	}
	
	public function doPreparePersonalInfoValues(){
		// first page is divided into 5 parts
		
		// save first the personalInfo part ** note: passport details is included here
		$personalInfo = (isset($_POST['PersonalInfo']) ? $_POST['PersonalInfo'] : array());
		$this->doSavePersonalInfo($personalInfo);
		
		// save student address details ** note: i separated this for more convenience
		$addressInfo = (isset($_POST['AddressInfo']) ? $_POST['AddressInfo'] : array());
		$this->doSaveAddressInfo($addressInfo);
		
		// save passport details
	//	$passport = (isset($_POST['PassportInfo']) ? $_POST['PassportInfo'] : array());
	//	$this->doSavePassportDetails($passport);
		
		// save emergency contact
	//	$emergencyContact = (isset($_POST['EmergencyContact']) ? $_POST['EmergencyContact'] : array());
	//	$this->doSaveEmergencyContact($emergencyContact);
		
		// save last attended school details/information
		$schoolInfo = (isset($_POST['SchoolInfo']) ? $_POST['SchoolInfo'] : array());
		$this->doSaveSchoolInfo($schoolInfo);
		
		// save additional information
	//	$addInfo = (isset($_POST['AdditionalInfo']) ? $_POST['AdditionalInfo'] : array());
	//	$needs = (isset($_POST['SpecialNeedsInfo']) ? $_POST['SpecialNeedsInfo'] : array());
	//	$additionalInfo = array_merge($addInfo, $needs);
	//	$this->doSaveAdditionalInfo($additionalInfo);
		
	}
	
	public function doSavePersonalInfo(array $personalInfo){
		if(!empty($personalInfo)){
			$birthday = array('Birthday' => $personalInfo['yyyy'].'-'.$personalInfo['mm'].'-'.$personalInfo['dd']);
			$onlineAppUserID = array('OnlineAppUserID' => $this->obj_session->get('onlineAppUserID'));
			$personalInfoMerged = array_merge($birthday, $onlineAppUserID, $personalInfo);
			$personalInfoObj = $this->file_factory->getClass('PersonalInfo');
			$personalInfoObj->setValues($personalInfoMerged);
			
			if (isset($_POST['Submit'])){
				$personalInfoObj->save();
			}
			else if (isset($_POST['Edit'])){
				$personalInfoObj->setID($_POST['pID']);
				$personalInfoObj->update();
			}
		//	TODO: store process id in session	
		//	$this->obj_session->get('onlineAppUserID');
		//	$this->procID = 
			$this->lastInsertedPersonalInfoID = array('PersonalInfoID' => $personalInfoObj->getID());
			
		}
		
	}
	
	public function doSaveAddressInfo(array $addressInfo){
		if(!empty($addressInfo)){
			$validUntil = array();
			$addressInfo = array_filter($addressInfo);
			
			if(isset($addressInfo['yyyy']) and isset($addressInfo['mm']) and isset($addressInfo['dd'])){
				$validUntil = array('ValidUntil' => $addressInfo['yyyy'].'-'.$addressInfo['mm'].'-'.$addressInfo['dd']);
			}
			
			$addressInfoMerged = array_merge($this->lastInsertedPersonalInfoID, $addressInfo, $validUntil);
		//	var_dump($_POST['pID']); exit;
			$addressInfoObj = (isset($_POST['pID']) AND (int)$_POST['pID'] > 0) ? AddressInfoMapper::retrieveByPersonalInfoIDWithType($_POST['pID']) : null;
		//	var_dump($addressInfoObj); exit;
			if(is_null($addressInfoObj)){
				$addressInfoObj = $this->file_factory->getClass('AddressInfo');
				$addressInfoObj->setValues($addressInfoMerged);
				$addressInfoObj->save();
			}
			else {
				$addressInfoObj->setValues($addressInfoMerged);
				$addressInfoObj->update();
			}
			
			
			
			// if(isset($_POST['Submit'])){
			// 	$addressInfoObj->save();
			// }
			// else if (isset($_POST['Edit'])){
			// 	$addressInfoObj->update();
			// }
			
		}
		
	}
	
	public function doSavePassportDetails(array $passportInfo){
		if(!self::isEmpty($passportInfo)){
			$expiryDate = array('ExpiryDate' => $passportInfo['yyyy'].'-'.$passportInfo['mm'].'-00');

			$passportInfoMerged = array_merge($this->lastInsertedPersonalInfoID, $passportInfo, $expiryDate);
			$passportDetailsObj = $this->file_factory->getClass('PassportDetails');
			$passportDetailsObj->setValues($passportInfoMerged);
			
			if(isset($_POST['Submit'])){
				$passportDetailsObj->save();
			}
			else if (isset($_POST['Edit'])){
				$passportDetailsObj->update();
			}
		}
		
	}
	
	public function doSaveEmergencyContact(array $emergencyContact){
		if(!empty($emergencyContact)){
			$emergencyContactMerged = array_merge($this->lastInsertedPersonalInfoID, $emergencyContact);
			
			$addressInfo = array_merge(array(CISOnlineAppFieldsMapper::ADDRESS_TYPE => 2,
											CISOnlineAppFieldsMapper::PERMANENT_ADDRESS => 1), $emergencyContact );


			$this->doSaveAddressInfo($addressInfo);
			$emergencyContactObj = $this->file_factory->getClass('EmergencyContact');
			$emergencyContactObj->setValues($emergencyContactMerged);
			
			if(isset($_POST['Submit'])){
				$emergencyContactObj->save();
			}
			else if (isset($_POST['Edit'])){
				$emergencyContactObj->update();
			}
		}
		
	}
	
	public function doSaveSchoolInfo(array $schoolInfo){
		if(!empty($schoolInfo)){
			/* School types: 
			 * 		0 - non partner
			 * 		1 - partner school
			 * 		2 - high school
			 * 		3 - other school
			 */ 
		//	var_dump($schoolInfo); exit;
			if(!isset($schoolInfo['Hs']) AND !isset($schoolInfo['Other'])){
				
				if($schoolInfo['SchoolID'] == '0'){
					$schoolInfo['OtherSchoolName'] = $schoolInfo['OtherSchoolName1'];
					
				}
				else{
					$id = explode("_", $schoolInfo['SchoolID']);
					$schoolInfo['SchoolID'] = $id[1];
					$schoolInfo['Type'] = ("usu" == $id[0]) ? 0 : 1;
					$schoolInfo['OtherSchoolName'] = (0 < $id[1]) ? $schoolInfo['OtherSchoolName'] : $schoolInfo['OtherSchoolName1'];
				}
				
				
				
			}
			else {
				if(!isset($schoolInfo['Hs'])){
					$schoolInfo['SchoolID'] = 0;
					$schoolInfo['Type'] = 3;
				}
				else {
					$schoolInfo['SchoolID'] = 0;
					$schoolInfo['Type'] = 2;
				}
				
			}
			
			$schoolInfoMerged = array_merge($this->lastInsertedPersonalInfoID, $schoolInfo);
			$schoolInfoObj = $this->file_factory->getClass('SchoolInfo');
			$schoolInfoObj->setValues($schoolInfoMerged);

			if(isset($_POST['Submit'])){
				$schoolInfoObj->save();
			}
			else if (isset($_POST['Edit'])){
				$schoolInfoObj->update();
			}
		}
	}
	
	public function doPrepareAdditionalInfos(){
	//	echo "<pre>";
	//	var_dump($_POST);
	//	exit;
		
		
		
		
		$addInfo = (isset($_POST['AdditionalInfo']) ? $_POST['AdditionalInfo'] : array());
		$needs = (isset($_POST['SpecialNeedsInfo']) ? $_POST['SpecialNeedsInfo'] : array());
		
		if(isset($_POST['CampusRep']) AND $_POST['CampusRep'] == 0){
			$addInfo['CampusRep'] = "";
		}
		
		if(isset($_POST['FriendGrant']) AND $_POST['FriendGrant'] == 0){
			$addInfo['FriendGrant'] = "";
		}
		
		
		
		$additionalInfo = array_merge($addInfo, $needs);
		$this->doSaveAdditionalInfo($additionalInfo);
	}
	
	public function doSaveAdditionalInfo(array $additionalInfo){
		if(!empty($additionalInfo)){
			$additionalInfoMerged = array_merge($this->lastInsertedPersonalInfoID, $additionalInfo);
			$additionalInfoObj = $this->file_factory->getClass('AdditionalInfo');
			$additionalInfoObj->setValues($additionalInfoMerged);
			
			if(isset($_POST['Submit'])){
				$additionalInfoObj->save();
			}
			else if (isset($_POST['Edit'])){
				$additionalInfoObj->update();
			}
		}
	//	exit;
	}
	// ######## END OF PERSONAL INFORMATION SECTION #########
	
	// ######## PROGRAM INFORMATION SECTION ######	
	public function doPrepareProgramInfoValues(){
		// NOTE: when next is clicked in the in PERSONAL INFORMATION page, PersonalInfoID must be posted in url 
		// 		
		if(isset($_POST['txtAcomment'])){
			$this->processComment();
		}
		
		$programInfo = (isset($_POST['ProgramInfo']) ? $_POST['ProgramInfo'] : array());
		$this->doSaveProgramInfo($programInfo);
		
		if(isset($_POST['CouponCode'])){
			$this->processCouponCode();
		}
		
		//echo '<p>done program info</p>';
	}
	// TODO:
	public function doSaveProgramInfo(array $programInfo){
		if(!empty($programInfo)){
		//	var_dump($programInfo); exit;
			$programInfoMerged = array_merge($this->lastInsertedPersonalInfoID,$programInfo);
			$progInfo = $this->file_factory->getClass('ProgramInformation');
			$progInfo->setValues($programInfoMerged);
		//	exit;
			if (isset($_POST['Submit'])){
				$progInfo->save();
			}
			else if (isset($_POST['Edit'])){
				$progInfo->update();
			}
			
		}
		
	}
	// ######## END PROGRAM INFORMATION SECTION ######
	
	// ####### PROCESS COUPON CODE #######
	public function processCouponCode(){
		$pID = $this->lastInsertedPersonalInfoID['PersonalInfoID'];

		$coupon = $this->file_factory->getClass('UserCoupon');
		$coupon->setPersonalInfoID($pID);
		
		if(!empty($_POST['CouponCode'])){
			$code = $_POST['CouponCode'];
			$coupon->setName($code);
			$coupon->save();
		}
		else {
			if($coupon->hasCode()){
				$coupon->delete();
			}
		}
	}
	
	// ######## PERSONAL STATEMENT SECTION ######
	public function doPreparePersonalStatementValues(){
		$personalStatement = (isset($_POST['PersonalStatement']) ? $_POST['PersonalStatement'] : array());
		$this->doSavePersonalStatemet($personalStatement);
	}
	public function doSavePersonalStatemet(array $personalStatement){
		if(!self::isEmpty($personalStatement)){
			$statementMerged = array_merge($this->lastInsertedPersonalInfoID, $personalStatement);
			$statement = $this->file_factory->getClass('PersonalStatement');
			if(!isset($statementMerged['chckStatement'])){
				$statement->setValues($statementMerged);
			}
			else {
				// if $statementMerged['chckStatement'] is set - pass 1 to lateOption
				$statement->setPersonalInfoID($this->lastInsertedPersonalInfoID['PersonalInfoID']);
				$statement->setLateOption(1);
			}
			
			if(isset($_POST['Submit'])){
				$statement->save();
			}
			else if(isset($_POST['Edit'])){
				$statement->update();
			}
			
		}
		
	}
	// ######## END PERSONAL STATEMENT SECTION ######
	
	// ###### AGREEMENT AND WAIVER SECTION ######
	public function doPrepareAgreementValues(){
		$agreement = (isset($_POST['Agreement']) ? $_POST['Agreement'] : array());
		$this->doSaveAgreement($agreement);
	}
	
	public function doSaveAgreement(array $agreement){
		if(!empty($agreement)){
			$agreementDate = array(CISOnlineAppFieldsMapper::APPLICATION_DATE => Date("Y-m-d H:i:s"));
			$agreementMerged = array_merge($this->lastInsertedPersonalInfoID, $agreement, $agreementDate);
			$agreementObj = $this->file_factory->getClass('Agreement');
			$agreementObj->setValues($agreementMerged);
		//	$agreementObj->setSignature('True');
		//	$agreementObj->setApplicationDate(Date("Y-m-d H:i:s"));
		//	var_dump($agreement);
		//	var_dump($agreementObj);exit;
			if (isset($_POST['Submit'])){
			//	$agreementObj->save();
			}
			else if (isset($_POST['Edit'])){
				$agreementObj->setLastEdited(Date("Y-m-d H:i:s"));
			//	$agreementObj->update();
			}
			
			$agreementObj->update();
			
		}
		
	}
	// ###### AGREEMENT AND WAIVER SECTION ######
	
	// ###### BILLING AND PAYMENT SECTION #####
	public function doPrepareBillingInfoValues(){
//		$CISOnlineAppFieldsMapper = $this->file_factory->invokeStaticVar('CISOnlineAppFieldsMapper', 'billingInfoFieldMap');
		
		$billingInfo = (isset($_POST['BillingInfo']) ? $_POST['BillingInfo'] : array());
 		if(!empty($billingInfo)){
			$cnt = 0;

			if(isset($billingInfo[CISOnlineAppFieldsMapper::PAYING_PERSONS])){
				foreach($billingInfo[CISOnlineAppFieldsMapper::PAYING_PERSONS] as $person):
					$cnt = $cnt + (int)$person;
				endforeach;
			}
			else {
				$cnt = 1;
			}
			

			$billingInfo[CISOnlineAppFieldsMapper::PAYING_PERSONS] = $cnt;
			
			$this->doSaveBillingInfo($billingInfo);
			
		}
		
	}
	
	public function doSaveBillingInfo(array $billingInfo){
		if(!empty($billingInfo)){
			$billingInfoMerged = array_merge($this->lastInsertedPersonalInfoID, $billingInfo);
			
			$addressInfo = array_merge(array(CISOnlineAppFieldsMapper::ADDRESS_TYPE => 3,
											CISOnlineAppFieldsMapper::PERMANENT_ADDRESS => 1), $billingInfoMerged );
			
			$this->doSaveAddressInfo($addressInfo);
			$billing = $this->file_factory->getClass('BillingInfo');
			$billing->setValues($billingInfoMerged);
			
			if(isset($_POST['Submit'])){
				$billing->save();
			}
			else if (isset($_POST['Edit'])){
				$billing->update();
			}
			
		}
		
	}
	
	/**
	start
	*/
	
	public function showTemplate1(){
		$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
		$this->file_factory->invokeStaticClass('Template','setMainTemplateVar', array('layoutID', 'online_application'));
		$tplOnlineApplication = $this->file_factory->getClass('Template');
		$applicationProgressMenu = $this->prepareApplicationProgressMenu();
		$countries = $this->prepareCountryList();
		if(isset($_GET['pID'])){
			$objArr = array();
			$personalInfoID = $_GET['pID'];
			foreach($this->action_class_names as $cls){
				$clsName = 'Class.'.$cls.'Mapper.php';
				$file = dirname(__FILE__).'/../../../model/onlineApplications/CIS_onlineApplication/Mapper/'.$clsName;
				if(file_exists($file)){
					require_once($file);
					$func = call_user_func(array($cls.'Mapper', 'retrieveByPersonalInfoID'), $personalInfoID);
					if($func != null){
						$objArr[$cls] = $func;
					}
					
				}				
			}
			// 0 when not empty
			// 1 when empty
			
			if(!empty($objArr)){
				$tplOnlineApplication->set('buttonName','Edit');
				$tplOnlineApplication->set('objArr',$objArr);
				
			}
		}
		
	//	echo "here";
		if($this->selectedStep == 2 and $this->action == 'view'){
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.TermsPeer.php');
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.ProgramsPeer.php');
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.OptionsPeer.php');
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.HousingOptionsPeer.php');
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.DurationOptionsPeer.php');
			
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.UserCouponPeer.php');
		//	echo "start<br/><pre>";
			$terms = TermsPeer::retrieveAll();
		//	var_dump($terms);
			$programs = ProgramsPeer::retrieveAllWithTerms();
		//	var_dump($programs);
			$options = OptionsPeer::retrievelOptionsOnProgramsAndTerms();
			
			$housingOptions = HousingOptionsPeer::retrieveHousingOptionsPerProgram();
		//	$durationOptions = DurationOptionsPeer::retrieveDurationOptionsPerProgram();
			// prepare all programs
			$jsPrograms = array();
			foreach($programs as $program){
				$jsProgramsWithTerms = array(
					'id' => $program->getID(),
					'name' => $program->getName(),
					'termID' => $program->getTerm(),
					'hasStartDate' => $program->hasStartDate()
					);
				$jsPrograms[] = $jsProgramsWithTerms;
			}
		//	echo "<pre>";
		//	var_dump($jsPrograms); exit;
			// prepare all program options
			$jsOptions = array();
			foreach($options as $option){
				$jsOptionsWithProgramAndTerm = array(
						'id' => $option->getID(),
						'programID' => $option->getProgramID(),
						'termID' => $option->getTermID(),
						'name' => $option->getName()
						);
						
				$jsOptions[] = $jsOptionsWithProgramAndTerm;
			}
		//	echo "<pre>";
		//	var_dump($jsOptions);
			$jsHousingOptions = array();
			foreach($housingOptions as $hOption){
				$jsHousingOptions[] = array('id' => $hOption->getID(),
											'programID' => $hOption->getProgramID(),
											'termID' => $hOption->getTermID(),
											'name' => $hOption->getName());
			}
			
		//	echo "<pre>";
		//	var_dump($jsHousingOptions); exit;
			
			for($x=1; $x<=12; $x++)
			{
				$months[] = date('F', mktime(0,0,0, $x, 1));
			}
			
			
			$userID = $this->obj_session->get('onlineAppUserID');

			$com = $this->file_factory->getClass('Comments');
			$comment = $com->retrieveCommentByUserId($userID);
			
			$pID = $this->lastInsertedPersonalInfoID['PersonalInfoID'];
			$couponCode = UserCouponPeer::retrieveByPersonalInfoID($pID);
			
			$tplOnlineApplication->set('housingOptions',$housingOptions);
			$tplOnlineApplication->set('comment',$comment);
			
			$tplOnlineApplication->set('jsOptions',$jsOptions);
			$tplOnlineApplication->set('jsPrograms',$jsPrograms);
			$tplOnlineApplication->set('jsHousingOptions',$jsHousingOptions);
			
			$tplOnlineApplication->set('terms',$terms);
			$tplOnlineApplication->set('programs',$programs);
			
			$tplOnlineApplication->set('startMonths',$months);
			$tplOnlineApplication->set('couponCode',$couponCode);
			
		//	exit;
		}
		
		if($this->selectedStep == 1 and $this->action == 'view'){
		//	require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.CISPartnerSchoolsPeer.php');
		//echo '1';
		//	$schools = $this->prepareSchoolsAndUniversities(); //CISPartnerSchoolsPeer::retrieveAll();
		//echo '2';	
			$states = $this->prepareStateList();
		//echo '3';	
			$tplOnlineApplication->set('states',$states);
		//	$tplOnlineApplication->set('schools',$schools);
		}
		
		if($this->selectedStep == 6 and $this->action == 'view'){
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.CISPartnerSchoolsPeer.php');
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.ProgramsPeer.php');
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.SchoolInfoMapper.php');
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.ProgramInformationMapper.php');
			
			require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.UserCouponPeer.php');
			
			$pID = $this->lastInsertedPersonalInfoID['PersonalInfoID'];
			
			$schoolInfo = SchoolInfoMapper::retrieveByPersonalInfoID($pID);
			$programInformation = ProgramInformationMapper::retrieveByPersonalInfoID($pID);
			
			$partnerSchools = CISPartnerSchoolsPeer::retrieveAllInArray();
			//$discountedPrograms = ProgramsPeer::retrieveDiscountedProgramsInArray();
			$discountedPrograms = array();
			$couponCode = UserCouponPeer::retrieveByPersonalInfoID($pID);
			$approvedCoupons = UserCouponPeer::getApprovedCouponCodes();
			/*
			echo "<pre>";
			
			
		//	var_dump($schoolInfo->getSchoolName());
		//	var_dump($schoolInfo->getSchoolID());
		//	var_dump($schoolInfo->getType());
			var_dump($partnerSchools);
			
		//	var_dump($programInformation->getFirstProgramID());
			
		//	var_dump($discountedPrograms);
		
		exit;*/

			if(array_key_exists($schoolInfo->getSchoolID(),$partnerSchools) AND $schoolInfo->getType() == 1){
				if(array_key_exists($programInformation->getFirstProgramID(),$discountedPrograms)){
					$msg = 'You\'re almost there...time to pay the application fee and submit your application. 
							Your application fee is reduced from $150 to $100 because you attend a college or 
							university who is a member of our Cooperating Partner Alliance (CPA) program.';
					$proceedBtnName = 'PROCEED';
				}
				else {
					$msg = 'You\'re almost there...all you need to do is submit your application. Because you 
							attend a college or university who is a member of our Cooperating Partner Alliance (CPA) 
							program your application fee is waived.';
					$proceedBtnName = 'SUBMIT';
				}
			}
			else {
				if($couponCode AND in_array($couponCode->getName(),$approvedCoupons)){
					$msg = 'You\'re almost there...all you need to do is submit your application. Because you 
							have a coupon code, you do not need to pay the application fee.';
					$proceedBtnName = 'SUBMIT';					
				}
				else {
					$msg = 'You\'re almost there...time to pay the application fee and submit your application. The 
							application fee is $50.';
					$proceedBtnName = 'PROCEED';
				}
				
			}
			
		//	var_dump($msg);
			
			
		//	exit;
			$tplOnlineApplication->set('proceedBtnName', $proceedBtnName);
			$tplOnlineApplication->set('msg', $msg);
		}
		//echo 'test';
		$remoteAddr = Getenv("REMOTE_ADDR");
		
		$tplOnlineApplication->set('remoteAddr',$remoteAddr);
		// FOR DEBUGGING PURPOSE ONLY
		$tplOnlineApplication->set('nash',$this->obj_session->get('onlineAppUserID'));
		
		
		$tplOnlineApplication->set('pageTitle',$this->pageTitle);
		$tplOnlineApplication->set('pID',$this->lastInsertedPersonalInfoID['PersonalInfoID']);
		
		$tplOnlineApplication->set('applicationProgressMenu',$applicationProgressMenu);
		$tplOnlineApplication->set('countries',$countries);
		$tplOnlineApplication->out('travellog/views/onlineApplications/cis/'.$this->mTemplate);
		
	}
	
	/**
	end
	*/
	
	public function prepareCountryList(){
		$countryList = $this->file_factory->invokeStaticClass('Country','getCountryList');
		return $countryList;
	}
	
	public function prepareStateList(){
		$stateList = $this->file_factory->invokeStaticClass('State','getStateList');
		return $stateList;
	}
	
	public function prepareApplicationProgressMenu(){
		$progressMenu = $this->file_factory->getClass('Template');
		$progressMenu->set('selectedStep', $this->selectedStep);
		return $progressMenu->fetch('travellog/views/onlineApplications/cis/tpl.applicationProgressMenu.php');

	}
	
	public function prepareSchoolsAndUniversities(){
		
		require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.USUniversitiesPeer.php');		
		require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.CISPartnerSchoolsPeer.php');
		echo "<pre>";
		$schools = CISPartnerSchoolsPeer::retrieveAll();
	//	var_dump($schools);
		$usUniversities = USUniversitiesPeer::retrieveUSUniversities();
		var_dump($usUniversities);
		$partnerSchools = array();
		$usuSchools = array();
		$combinedSchools = array();
		$foundSameName = false;
		$scFirstrun = true;
		$cnt = 1;
		foreach($usUniversities as $key => $obj){
			foreach($schools as $sKey => $school){
				if($obj->getName() == $school->getName()){
					$foundSameName = true;
					$cnt++;
				}				
				if($scFirstrun == true){
					$pKey = 'partner_'.$school->getID();
					$partnerSchools[$pKey] = $school->getName();
				}
			}
			
			if($foundSameName == false){
				$cKey = 'usu_'.$obj->getID();
				$usuSchools[$cKey] = $obj->getName();
			}			
			$foundSameName = false;
			$scFirstrun = false;
		}
		
		$combinedSchools = array_merge($usuSchools, $partnerSchools);
		array_multisort($combinedSchools,SORT_ASC);
		return $combinedSchools;
	}
		
	private function checkStepsDone(){
		require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.PersonalInfoMapper.php');
		$userID = $this->obj_session->get('onlineAppUserID');
		
		$personalInfoObj = PersonalInfoMapper::retrieveByUserID($userID);
		if($personalInfoObj != null){
			$doneSteps = $this->file_factory->getClass('Steps');
			$step = $doneSteps->getStepByPersonalID($personalInfoObj->getID());

			if(0 < $step){
				$this->lastInsertedPersonalInfoID = array('PersonalInfoID' => $personalInfoObj->getID());
			}
			
			return $step;
		}
		return null;
		
	}
	
	private function saveStepsDone(){
		$doneSteps = $this->file_factory->getClass('Steps');
		$step = $doneSteps->getStepByPersonalID($this->lastInsertedPersonalInfoID['PersonalInfoID']);
		
		if ($this->selectedStep == 6 AND !isset($_GET['saveAndExit'])){
			$doneSteps->setPersonalInfoID($this->lastInsertedPersonalInfoID['PersonalInfoID']);
			$doneSteps->delete();
		}
		else if ($this->selectedStep > $step){
			$doneSteps->setPersonalInfoID($this->lastInsertedPersonalInfoID['PersonalInfoID']);
			$doneSteps->setDoneStep($this->selectedStep);
			$doneSteps->save();
		}
	}
	
	// miscellaneous functions
	private static function isEmpty(array $array ){
		$my_not_empty = create_function('$v', 'return strlen($v) > 0;');
		return (count(array_filter($array, $my_not_empty)) == 0) ? 1 : 0;
		
	}
	
	private function endSession(){
		$this->obj_session->unsetVar('onlineAppUserID');
	//	$location = 'http://secure.goabroad.net/cis/mysessionhandler.php?logout';
		
	//	header("location: $location");
	//	exit;
	}
	
	private function isApplicationDone(){
	//	var_dump('is application done?');
	//	require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.DoneProcessPeer.php');
		require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.PersonalInfoMapper.php');
		$pID = $this->lastInsertedPersonalInfoID['PersonalInfoID'];
		
		$isDone = PersonalInfoMapper::isProcessDone($pID);
		
		return $isDone;
		
	}
	
	private function saveDoneApplication(){
		$pID = $this->lastInsertedPersonalInfoID['PersonalInfoID'];
		$personalInfoObj = $this->file_factory->getClass('PersonalInfo');
		$personalInfoObj->setID($pID);
		$personalInfoObj->saveProcessAsDone();
	//	$doneProcess = $this->file_factory->getClass('DoneProcess');
	//	$doneProcess->setProcessID($pID);
	//	$doneProcess->save();
		
	}
	
	private function isOwner(){
		$onlineAppUserID = $this->obj_session->get('onlineAppUserID');
		
		require_once('travellog/model/onlineApplications/CIS_onlineApplication/Mapper/Class.PersonalInfoMapper.php');
		$pID = $this->lastInsertedPersonalInfoID['PersonalInfoID'];
		
		$owner = PersonalInfoMapper::isViewerOwner($pID, $onlineAppUserID);
		return $owner;
		
	}
	
	private function createCSV(){
	//	$this->lastInsertedPersonalInfoID['PersonalInfoID'] = 61;
		$pID = /*$_GET['pID'];*/$this->lastInsertedPersonalInfoID['PersonalInfoID'];
		//echo $pID; exit;
		$objArr = array();
		foreach(self::$class_names as $key => $values){
			foreach($values as $cls){
				$clsName = 'Class.'.$cls.'Mapper.php';
				$file = dirname(__FILE__).'/../../../model/onlineApplications/CIS_onlineApplication/Mapper/'.$clsName;
				if(file_exists($file)){
					require_once($file);
					$objArr[$cls] = call_user_func(array($cls.'Mapper', 'retrieveByPersonalInfoID'), $pID);
				}
			}			
		}
		
		// get the comment of the user if there is
		$userID = $this->obj_session->get('onlineAppUserID');
		$com = $this->file_factory->getClass('Comments');
		$comment = $com->retrieveCommentByUserId($userID);

		// prepare applicant name to be used as filename for the files to be created
		$name = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getName();
		$id = $objArr[CISOnlineAppFieldsMapper::PERSONAL_INFO]->getID();
		$applicantName = str_replace(' ','_',$name);
		
		// prepare list for csv file  
		
		require_once('travellog/helper/onlineApplications/Class.CIS_CSVListCreator.php');
		$csvList = CIS_CSVListCreator::create($objArr);
		
		// creating csv file
		$csvFile = 'application_'.$applicantName.'_'.$id.'.csv';
		$csvName  = dirname(__FILE__).'/../../../../../goabroad.net/onlineapplication/tmp/'.$csvFile;
		$csvFileHandle = fopen($csvName, 'w') or die("cannot open file");
		foreach ($csvList as $line) {
		    fputcsv($csvFileHandle, $line);
		}
		fclose($csvFileHandle);
		
		// prepare template for text file  
		$txtFileTemplate = $this->file_factory->getClass('Template');
		$txtFileTemplate->set('objArr', $objArr);
		$txtFileTemplate->set('comment', $comment);
		$txtTmplate = $txtFileTemplate->fetch('travellog/views/onlineApplications/cis/tpl.txtFile.php');
		
		// creating text file
		$txtFile = 'application_'.$applicantName.'_'.$id.'.txt';
		$filename = dirname(__FILE__).'/../../../../../goabroad.net/onlineapplication/tmp/'.$txtFile;
		$ourFileHandle = fopen($filename, 'w') or die("cannot open file");
		fwrite($ourFileHandle, $txtTmplate);
		fclose($ourFileHandle);
		
		// for debugging purpose only
		
		$remoteAddr = Getenv("REMOTE_ADDR");
		
	//	if($remoteAddr == '121.96.35.154'){
	//		var_dump(file_exists($csvName));
	//		var_dump(file_exists($filename));
	//	}
		
		
		require_once('class.phpmailer.php');
			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->IsHTML(false);
			//$mail->From = "jpalm@cisabroad.com";
		//	$mail->From = "nash.lesigon@goabroad.com";
			$mail->From = "aheddens@cisabroad.com";
			
			
			//$mail->From = "admissions@cisabroad.com";
			$mail->FromName = "Online Application";
			$mail->Subject = "Online Application - $name";
			$mail->Body = "Application";
			
			$mail->Addattachment($csvName, $csvFile);
			$mail->Addattachment($filename, $txtFile);

			$mail->AddAddress("admissions@cisabroad.com");
			//$mail->AddAddress("nash.lesigon@goabroad.com");
			$mail->AddBCC("reynaldo.castellano@goabroad.com");
			//$mail->AddBCC("virna.alvero@goabroad.com");
			$mail->AddBCC("nash.lesigon@goabroad.com");
		
			$this->saveDoneApplication();
			
			if($mail->Send()){
				// temporarily dont delete files for backup since mail server is unstable - Oct 30, 2009
				unlink($filename);
				unlink($csvName);
			//	if($remoteAddr == '121.96.35.154'){
			//		echo 'sent'; exit;
			//	}
			}
			else{
			//	if($remoteAddr == '121.96.35.154'){
			//		echo 'not sent<br/>';
			//		var_dump($mail);
			//		var_dump($mail->smtp->error);
					//$mail->smtp->error['error']
					
			//		exit;
			//	}
			//	echo "waray kasend";
			//	echo "<pre>";
			//	var_dump($mail->smtp->error);
			//	exit;
			}
			
	//	echo "<br/>end";
	}
	
	public function processComment(){
	//	var_dump($_POST);
		$userID = $this->obj_session->get('onlineAppUserID');
		$com = $this->file_factory->getClass('Comments');
		$com->setGenID($userID);
		
		if(!empty($_POST['txtAcomment'])){
			$message = $_POST['txtAcomment'];
			$com->setMessage($message);
			$com->save();
		}
		else {
	//		echo 'test';
			if($com->hasComment()){
				$com->delete();
			}
		}
		
	}
	
	/**
	 * @purpose - update student db in GoAbroadHQ
	 **/ 
	private function updateStudentDB(){ 
		$pID = $this->lastInsertedPersonalInfoID['PersonalInfoID'];
		$params = array('pID' => $pID, 'step' => $this->selectedStep);
		//var_dump($params); exit;
		//$params = array('pID' => 13, 'step' => 2); 
		//gaCISOnlineAppWebService::getInstance()->postToGaHQ($params);
	// 	exit;
	}
}
?>