<?php

require_once('travellog/web_services/Class.gaCISOnlineAppWebService.php');
require_once('Class.Crypt.php');
class AbstractOnlineAppGatewayController {
	
	protected $accepted_params = array(	
										'first_name', 'last_name', 'preferred_name', 'gender', 'date_of_birth', 'primary_email', 'secondary_email', 
										'student_home_address_street_address', 'student_home_address_city', 'student_home_address_state', 
										'student_home_address_zip', 'phone_number', 'cell_phone',
										'university', 'field_of_study', 'current_year_in_school', 'gpa', 'inquiry_related_inquiry_source', 'learnfrom_others',
										'term', 'year', 'program', 'option', 'duration', 'housing', 'start_month', 'start_day_month', 'full_year_interest',
										'cis_custom_academic_or_career_goals', 'cis_custom_cultural_understanding', 'cis_custom_personal_growth', 'shirt_size'
										);
										
	public function __construct(){}
	
	public function performAction(){
		
		$params = $_POST;
		/*
		
		$params = array("student_id" => "161",
						"ganet_profile" => "",
						"lead_source" => "",
		  				"field_rep" => "",
		  				"date_entered" => "",
		  				"creator_id" => "",
		  				"student_advisor" => "",
		  				"program_advisor" => "",
		  				"first_name" => "Nashy",
		  				"last_name" => "Lesigon",
		  				"preferred_name" => "nash",
		 				"gender" => "0",
		  				"cis_custom_password" => "",
		  				"status" => "1",
		  				"primary_email" => "nash.lesigon123@goabroad.com",
		  				"secondary_email" => "",
		  				"phone_number" => "",
		  				"cell_phone" => "",
		  "student_home_address_street_address" => "11th",
		  "student_home_address_city" => "Tacloban City",
		  "student_home_address_state" => "",
		  "student_home_address_country" => "0",
		  "student_home_address_zip" => "6500",
		  "university" => "Nashy State University",
		  "university_state" => "",
		  "field_of_study" => "tester",
		  "current_year_in_school" => "0",
		  "gpa" => "5.0",
		  "permanent_home_phone_number" => "",
		  "permanent_home_address_street_address" => "",
		  "permanent_home_address_city" => "",
		  "permanent_home_address_state" => "",
		  "permanent_home_address_country" => "0",
		  "permanent_home_address_zip" => "",
		  "inquiry_related_program_type" => "",
		  "inquiry_related_term" => "",
		  "inquiry_related_year_abroad" => "",
		  "inquiry_related_inquiry_source" => "CIS Alumni Referral",
		  "cis_custom_housing_option" => "",
		  "cis_custom_duration_option" => "",
		  "cis_custom_internship_start_month" => "",
		  "cis_custom_day_to_start_internship" => "",
		  "cis_custom_program_option" => "",
		  "cis_custom_arrival_date" => "",
		  "cis_custom_application_deadline" => "",
		  "cis_custom_payment_deadline" => "",
		  "cis_custom_deposit_deadline" => "",
		  "primary_program_interest" => "",
		  "secondary_program_interest" => "",
		  "primary_country_interest" => "",
		  "secondary_country_interest" => "",
		  "program_selection_program_name" => "",
		  "program_selection_program_term" => "",
		  "program_selection_program_year" => "",
		  "program_selection_program_type" => "",
		  "program_selection_program_start_date" => "",
		  "program_selection_program_end_date" => "",
		  "citizenship" => "",
		  "country_of_birth" => "",
		  "date_of_birth" => "1984-11-15 00:00:00"
		);
		*/
		
		
		gaCISOnlineAppWebService::getInstance()->processUpdateFromHQ($params);
	
		// post to HQ
	//	$post_params = array('pID' => 27, 'step' => 1); 
	//	var_dump($post_params); exit;
	//	gaCISOnlineAppWebService::getInstance()->postToGaHQ($post_params);
	}
	
}