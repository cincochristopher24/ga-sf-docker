<?php
	require_once('travellog/factory/Class.FileFactory.php');
	require_once("travellog/model/Class.SessionManager.php");
	/***
	 * RULES: 
	 * - The user must be logged and only advisors can visit this page.
	 * - A surveyformID must be present in the url.
	 * - That surveyFormID must be owned by the advisor.
	 */
	abstract class AbstractSurveyStatsController
	{
		protected $obj_session 		= NULL;
		protected $file_factory 	= NULL;
		protected $loggedTraveler 	= NULL;
		protected $group			= NULL;
		
		public function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/views'));
			$this->obj_session  = SessionManager::getInstance();
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			$this->file_factory->registerClass('ToolMan', array('path'=>''));
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('TravelerProfile');
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('AdminGroup');
			$this->file_factory->registerClass('SurveyForm', array('path'=>'travellog/model/formeditor/'));
			$this->file_factory->registerClass('SurveyFormAnswer', array('path'=>'travellog/model/formeditor/'));
			$this->file_factory->registerClass('Template', array('path'=>''));
			$this->file_factory->registerClass('SubNavigation');
			
			/*****************************************************
			 * added by neri: 11-06-08
			 * registers the class used by the profile component 
			 *****************************************************/
			 
			$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
			
			$this->file_factory->invokeStaticClass('Template','setMainTemplateVar',array('layoutID','survey'));
			
		}
		
		public function performAction(){
			//check if the user is logged
			//$travelerID = (isset($_SESSION) && array_key_exists('travelerID',$_SESSION) && $_SESSION['travelerID'] !=0 ? $_SESSION['travelerID']:0 );
			$travelerID = $this->obj_session->get('travelerID');
			if(!isset($travelerID) || 0==$travelerID){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/login.php?failedLogin&redirect=/surveystats.php?'.$_SERVER['QUERY_STRING']));
			}
			//check if the user is an advisor
			$this->loggedTraveler = $this->file_factory->getClass('Traveler',array($travelerID));
			if(!array_key_exists('gID',$_GET)){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			$oGroupFactory = $this->file_factory->invokeStaticClass('GroupFactory','instance');
			try{
				$groups = $oGroupFactory->create(array($_GET['gID']));
				$oGroup = count($groups) ? $groups[0] : null;
			}
			catch(exception $e){
				$oGroup = null;
			}
			if(is_null($oGroup) || $oGroup->getDiscriminator() != GROUP::ADMIN){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			if($oGroup->getAdministratorID() != $this->loggedTraveler->getTravelerID() && !$oGroup->isSuperStaff($this->loggedTraveler->getTravelerID())){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			
			/***
			if(!$this->loggedTraveler->isAdvisor()){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			***/
			
			//check if the surveyFormID is valid	
			if(!isset($_GET['frmID']) || !is_numeric($_GET['frmID'])){						
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			$frmID = $_GET['frmID'];
			
			//check if the frmID is a valid survey form ID
			if(!$this->file_factory->invokeStaticClass('SurveyForm','isExists',array($frmID))){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}

			//make sure that survey form is owned by the administered group 
			$oForm = $this->file_factory->getClass('SurveyForm',array($frmID));
			if($oForm->getGroupID()!=$oGroup->getGroupID()){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}

			$this->group = $oGroup;

			$surveyFormAnswers = $this->file_factory->invokeStaticClass('SurveyFormAnswer','getSurveyFormAnswersBySurveyFormID',array($oForm->getSurveyFormID()));
			$surveyFormAnswerGroups = array();
			foreach($surveyFormAnswers as $key =>$iSurveyFormAnswer){
				try{
					$surveyFormAnsGroup = $oGroupFactory->create( array($iSurveyFormAnswer->getGroupID()) );
					$surveyFormAnswerGroups[$iSurveyFormAnswer->getSurveyFormAnswerID()] = $surveyFormAnsGroup[0];
				}
				catch(exception $e){
					unset($surveyFormAnswers[$key]);				
				}
			}

			$view = (array_key_exists('view',$_GET) && 'entries'==$_GET['view'])?'entries':'stats';
			if('entries'==$view){
				if(!count($surveyFormAnswers)){
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/surveystats.php?frmID='.$frmID));
				}
				$tplEntry = $this->file_factory->getClass('Template');
				$tplEntry->set_path('travellog/views/');
				$participantInfo = array();
				foreach($surveyFormAnswers as $iFrmAns){
					$info = array();
					$fieldAnswers  = $iFrmAns->getSurveyFieldAnswers();
					$mappedSurveyFieldAnswers = array();
					foreach($fieldAnswers as $iFldAns){
						$fldAnswerValues = $iFldAns->getAnswerValues();
						$answerVal = array();
						foreach($fldAnswerValues as $iAnsVal){
							$answerVal[] = $iAnsVal->getValue();
						}
						$mappedSurveyFieldAnswers[$iFldAns->getSurveyFieldID()] = $answerVal; 
					}
					$tplEntry->set('mappedSurveyFieldAnswers',$mappedSurveyFieldAnswers);
					$tplEntry->set('surveyForm',$oForm);
					$info['formTpl'] 	= $tplEntry->fetch('tpl.SurveyStatsEntry.php');
					$info['formAnswer'] = $iFrmAns;
					$info['travelerProfile'] = $this->file_factory->getClass('TravelerProfile',array($iFrmAns->getTravelerID()));
					$info['group']		= $surveyFormAnswerGroups[$iFrmAns->getSurveyFormAnswerID()];
					$participantInfo[$iFrmAns->getSurveyFormAnswerID()] = $info;
				}
				$this->showEntryTemplate(array(
					'oGroup' 			=> $oGroup,
					'participantInfo'	=>$participantInfo,
					'surveyForm'		=>$oForm,
					'surveyFormAnswers'	=>$surveyFormAnswers 
				));
			}
			else{
				//map the surveyfields. Make the field id as the key
				$formFields = $oForm->getSurveyFormFields();
				$mappedFormFields = array();
				$surveyFieldAnswers = array();
				$orphanFieldAnswers 	= array();
				$orphanOptionAnswers 	= array();
				
				foreach($formFields as $iFld){
					$mappedFormFields[$iFld->getSurveyFieldID()] = $iFld;
					if($iFld->isAnswerable()){
						if($iFld->isWithOptions()){
							$options = $iFld->getOptions();
							$optionAnswers = array();
							foreach($options as $iOpt){
								$optionAnswers[$iOpt] = 0;
							} 
							$surveyFieldAnswers[$iFld->getSurveyFieldID()] = $optionAnswers;
						}
						else{
							$surveyFieldAnswers[$iFld->getSurveyFieldID()] = array();
						}
					}
				}
				
				foreach($surveyFormAnswers as $iFrmAns){
					$fieldAnswers = $iFrmAns->getSurveyFieldAnswers();
					foreach($fieldAnswers as $iFldAns){
						//if the surveyFieldID of $iFldAns is not present in the mappedFormFields as key, it is an orphan field answer
						if(!array_key_exists($iFldAns->getSurveyFieldID(),$surveyFieldAnswers)){
							$orphanFieldAnswers[] = $iFldAns;
						}
						else{
							//get the type of the surveyfield
							$mappedFormFld = $mappedFormFields[$iFldAns->getSurveyFieldID()];
							$fldType = $mappedFormFld->getFieldType();
							if($mappedFormFld->isAnswerable()){
								$iFldAnsVals = $iFldAns->getAnswerValues();
								if($mappedFormFld->isWithOptions()){
									foreach($iFldAnsVals as $iVal){
										if(array_key_exists($iVal->getValue(),$surveyFieldAnswers[$iFldAns->getSurveyFieldID()])){
											$surveyFieldAnswers[$iFldAns->getSurveyFieldID()][$iVal->getValue()]++;
										}
										else{
											if(!array_key_exists($iFldAns->getSurveyFieldID(),$orphanOptionAnswers)){
												$orphanOptionAnswers[$iFldAns->getSurveyFieldID()] = array();
											}
											if(!array_key_exists($iVal->getValue(), $orphanOptionAnswers[$iFldAns->getSurveyFieldID()])){
												$orphanOptionAnswers[$iFldAns->getSurveyFieldID()][$iVal->getValue()] = 0;
											}
											$orphanOptionAnswers[$iFldAns->getSurveyFieldID()][$iVal->getValue()]++;
										}
									}
								}
								else{
									/***
									if(isset($_GET['test']) && !isset($iFldAnsVals[0])){
										var_dump($iFldAns->getSurveyFieldID());
										echo '<br />';
										var_dump($iFldAns->getSurveyFieldAnswerID());
										echo '<br />';
										var_dump($iFldAns->getAnswerValues());
										echo '<br />==========<br />';
									}
									***/									
									if(isset($iFldAnsVals[0]) && strlen($iFldAnsVals[0]->getValue())){
										$surveyFieldAnswers[$iFldAns->getSurveyFieldID()][] = $iFldAnsVals[0]->getValue();
									}
								}
							}
						}
					}
				}
				
				$this->showOverViewTemplate(array(
					'oGroup' => $oGroup,
					'surveyForm'=>$oForm,
					'surveyFormAnswers'=>$surveyFormAnswers,
					'orphanFieldAnswers'=>$orphanFieldAnswers,
					'orphanOptionAnswers'=>$orphanOptionAnswers,
					'surveyFieldAnswers'=>$surveyFieldAnswers
				));
				
			}
		}

		protected function createTemplate(){
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			$tpl = $this->file_factory->getClass('Template');
			$tpl->set_path('travellog/views/');
			
			$subNavigation = $this->file_factory->getClass('SubNavigation');
			$subNavigation->setContext('GROUP');	
			$subNavigation->setContextID($this->group->getGroupID());
			$subNavigation->setGroupType('ADMIN_GROUP');
			$subNavigation->setLinkToHighlight('SURVEY_CENTER');
			$tpl->set('subNavigation',$subNavigation);
			
			/****************************************************
			 * edits by neri: 11-06-08
			 * added code for displaying the profile component
			 ****************************************************/
			
			$profile_comp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
			$profile_comp->init($this->group->getGroupID());
			$tpl->set('profile', $profile_comp->get_view());
			
			return $tpl;
		}

		
		protected function showOverviewTemplate($vars=array()){
			$tpl = $this->createTemplate();
			$tpl->set('oGroup',$vars['oGroup']);	
			$tpl->set('surveyForm',$vars['surveyForm']);	
			$tpl->set('surveyFormAnswers',$vars['surveyFormAnswers']);
			$tpl->set('orphanFieldAnswers',$vars['orphanFieldAnswers']);
			$tpl->set('orphanOptionAnswers',$vars['orphanOptionAnswers']);
			$tpl->set('surveyFieldAnswers',$vars['surveyFieldAnswers']);
			$tpl->set('totalParticipants',count($vars['surveyFormAnswers']));
			$tpl->out('tpl.SurveyStats.php');
		}
		
		protected function showEntryTemplate($vars=array()){
			$tpl = $this->createTemplate();
			$tpl->set('oGroup',$vars['oGroup']);	
			$tpl->set('participantInfo',$vars['participantInfo']);
			$tpl->set('surveyForm',$vars['surveyForm']);
			$tpl->set('totalParticipants',count($vars['surveyFormAnswers']));
			$tpl->out('tpl.SurveyStatsEntries.php');
		}
		
	}
 ?>