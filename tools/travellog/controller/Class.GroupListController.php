<?
	require_once("Class.Template.php");
	require_once('Class.FormHelpers.php');
	require_once('Class.Paging.php');
	require_once('Class.ObjectIterator.php');
	require_once('travellog/model/Class.Condition.php');
	require_once('travellog/model/Class.FilterCriteria2.php');
	require_once('travellog/model/Class.RowsLimit.php');
	require_once('travellog/model/Class.Group.php');
	require_once('travellog/model/Class.GroupAccess.php');
	require_once('travellog/model/Class.GroupFactory.php');
	require_once('travellog/model/Class.GroupCategory.php');
	require_once("travellog/model/Class.SubNavigation.php");
	require_once("travellog/model/Class.HelpText.php");
	require_once('Class.StringFormattingHelper.php');

	class GroupListController{
	
		protected
	
		$formHelpers = NULL,
	
		$mygroups  = FALSE,
	
		$traveler  = NULL,
	
		$page     = 1,

		$numrecords   = 6,
	
		$grpName = "",
	
		$loggedUser  = NULL,
	
		$loggedUserID  = NULL,
	
		$grpType = 'viewlist',
	
		$grpDiscrim = 'viewlist',

		$main_tpl = FALSE;
	
	
		function __construct(){
			$this->main_tpl = new Template();
			$this->main_tpl->set_path("travellog/views/");
		
			$this->formHelpers = new FormHelpers();
		}
	
	
		function setPage($page)	{
			$this->page = $page;
		}
	
		function setLoggedUser($loggedUser)	{
			$this->loggedUser = $loggedUser;
			if (NULL != $loggedUser)
				$this->loggedUserID = $loggedUser->getTravelerID();
		}
	
		function setTraveler($_traveler)	{
			$this->traveler = $_traveler;		
		}
	
		function setGroupType($grpType)	{
			$this->grpType = $grpType;
		}
	
		function setGroupDiscrim($grpDiscrim) {
			$this->grpDiscrim = $grpDiscrim;
		}
	
		function setGroupName($grpName) {
			$this->grpName = $grpName;
		}
	
		function setMyGroups($mygroups) {
			$this->mygroups = $mygroups;
		}
		
		/**
		 * Creates the filters for all groups based on the set properties of this object
		 * 
		 * @return FilterCriteria2
		 */
		public function fetchFilter(){
			$filter = new FilterCriteria2();
			
			switch ($this->grpDiscrim){ 
				case "fun":
				
					$cond = new Condition;
					$cond->setAttributeName("discriminator");
					$cond->setOperation(FilterOp::$EQUAL);
					$cond->setValue(Group::FUN);
			
					$filter->addCondition($cond);
					$filter->addBooleanOp('AND');
				  
					break;
				
				case "admin":
					//view all fun or view all admin
					$cond = new Condition;
					$cond->setAttributeName("discriminator");
					$cond->setOperation(FilterOp::$EQUAL);
					$cond->setValue(Group::ADMIN);
				
					$cond2 = new Condition;
					$cond2->setAttributeName("parentID");
					$cond2->setOperation(FilterOp::$EQUAL);
					$cond2->setValue(0);
				
					$filter->addCondition($cond);
					$filter->addBooleanOp('AND');
					$filter->addCondition($cond2);
					$filter->addBooleanOp('AND');
		
					break;
				
				case "viewlist":
		
					//view both sort by inverserank desc :: DEFAULT
					$cond = new Condition;
					$cond->setAttributeName("parentID");
					$cond->setOperation(FilterOp::$EQUAL);
					$cond->setValue(0);
				
					$filter->addCondition($cond);
					$filter->addBooleanOp('AND');
				
					break;
				
				default:
			
					//view both sort by inverserank desc :: DEFAULT
					$cond = new Condition;
					$cond->setAttributeName("parentID");
					$cond->setOperation(FilterOp::$EQUAL);
					$cond->setValue(0);
				
					$filter = new FilterCriteria2();
					$filter->addCondition($cond);
					$filter->addBooleanOp('AND');
				
					break;
			}
			
			$cond2 = new Condition;
			$cond2->setAttributeName("name");
			$cond2->setOperation(FilterOp::$LIKE);
			$cond2->setValue(addslashes($this->grpName));
		
			$filter->addCondition($cond2);
			$filter->addBooleanOp('AND');
			
			return $filter;
		}
		
		/**
		 * Creates the order of all groups based on the set properties of this object
		 * 
		 * @return FilterCriteria2
		 */
		public function fetchOrder(){
      $cnd = new Condition;
			$cnd->setAttributeName("inverserank");
			$cnd->setOperation(FilterOp::$ORDER_BY_DESC);
      
      $order = new FilterCriteria2();
			$order->addCondition($cnd);
			
			return $order;
		}
	
	  /**
	   * Creates the template of all groups
	   * 
	   * @return string
	   */
	  public function fetchAllGroupsTemplate(){
			$filter = $this->fetchFilter();
			$order  = $this->fetchOrder();
			
			if ('viewlist' == $this->grpType)
			  $grpFilteredGroups = Group::getFilteredGroups($filter, null, $order);		
	    else
	      $grpFilteredGroups = Group::getGroupsByCategory($this->grpType, null, $order);
			
			$discrim    = ("fun" != $this->grpDiscrim AND "admin" != $this->grpDiscrim) ? "" : "&cmbGrpDiscrim=".$this->grpDiscrim;
			$group_type = ("viewlist" == $this->grpType) ? "" : "&cmbGrpType=".$this->grpType;
			
			$paging    =  new Paging( count($grpFilteredGroups), $this->page, 'txtGrpName='.$this->grpName.$discrim.$group_type, $this->numrecords );
			$iterator  =  new ObjectIterator( $grpFilteredGroups,$paging    );
			
			$this->main_tpl->set('paging'   , $paging);
			$this->main_tpl->set('iterator' , $iterator);
					
			// set values for group categories in dropdown box
			$this->main_tpl->set('grpCategories',$this->formHelpers->CreateOptions(GroupCategory::getGroupCategoriesList(), $this->grpType ) );	
		
			$this->main_tpl->set('loggedUser',$this->loggedUser);
			$this->main_tpl->set('mygroups', $this->mygroups);
			$this->main_tpl->set('totalrec', count($grpFilteredGroups));
			$this->main_tpl->set('current_page', $this->page);
			
			return $this->main_tpl->fetch('tpl.ViewGroupList.php');	  	
	  }
	
		/**
		 * Creates the template of the traveler's groups
		 * 
		 * @param  Traveler $traveler    the traveler where the group list will be get
		 * @param  string   $queryString the query string for paging
		 * @return string
		 */
		public function fetchTravelerGroupListTemplate($traveler, $queryString = ""){
			$ordercond = new Condition;
			$ordercond->setAttributeName("adate");
			$ordercond->setOperation(FilterOp::$ORDER_BY_DESC);
		
			$order = new FilterCriteria2();
			$order->addCondition($ordercond);
			
			$limit = new RowsLimit($this->numrecords,(($this->page-1)*$this->numrecords));
					
			$grpFilteredGroups = $traveler->getGroups($order, $limit, $this->grpName);
			$total_rec = $traveler->getGroupsCount($this->grpName);
			$queryString = ("" != trim($this->grpName)) ? $queryString."&amp;txtGrpName=".$this->grpName : $queryString;
			$search = ("" != trim($this->grpName)) ? TRUE : FALSE;
			
			$paging = new Paging($total_rec, $this->page, $queryString, $this->numrecords);
			
			$this->main_tpl->set('current_page', $this->page);
			$this->main_tpl->set('paging', $paging);
			$this->main_tpl->set('search', $search);
			$this->main_tpl->set('group_list', $grpFilteredGroups);
			$this->main_tpl->set('loggedUser',$this->loggedUser);
			$this->main_tpl->set('mygroups', $this->mygroups);
			$this->main_tpl->set('totalrec', $total_rec);
		
			return $this->main_tpl->fetch('tpl.IncTravelerGroupList.php');			
		}
		
		/**
		 * Creates the template of search group form
		 * 
		 * @param  string|null|NULL  $mode      the mode whether (mygroups, moregroups or allgroups)
		 * @param  integer           $travID    the traveler ID for moregroups
		 * @param  boolean           $isMyGroup the flag whether this group search form is a form of the current logged in traveler or not
		 * @return string
		 */
		public function fetchGroupSearchFormTemplate($mode = "mygroups", $travID = 0, $isMyGroups = true){
			$this->main_tpl->set("mode", $mode);
			$this->main_tpl->set("travelerID", $travID);
			$this->main_tpl->set("mygroups", $isMyGroups);
			$this->main_tpl->set("grpName", $this->grpName);
			
			return $this->main_tpl->fetch('tpl.FrmSearchGrpName.php');
		}
		
	  /**
	   * Shows the all groups page or the Groups/Clubs tab
	   * 
	   * @return void
	   */
		function showAllGroups(){
			$tpl                 = clone $this->main_tpl;
			$filter_template     = NULL;
			$search_template     = NULL;
			$this->mygroups 		 = false;
		
	    $formHelpers = new FormHelpers();
	    $clubOptions = $formHelpers->CreateOptions(GroupCategory::getGroupCategoriesList(1), $this->grpType);		  
		  $adminOptions = $formHelpers->CreateOptions(GroupCategory::getGroupCategoriesList(2), $this->grpType);
		  
			$obj_session = SessionManager::getInstance();
			$success_message = ($obj_session->get('custompopup_message')) ? $obj_session->get('custompopup_message') : NULL;
			$obj_session->unsetVar('custompopup_message');
		  
			$tpl->set("grpCategories",$this->formHelpers->CreateOptions(GroupCategory::getGroupCategoriesList(), $this->grpType ) );	
			$tpl->set("grpType",$this->grpType);
			$tpl->set("grpDiscrim",$this->grpDiscrim);
			$tpl->set("grpName",$this->grpName);
			$tpl->set("clubOptions", $clubOptions);
			$tpl->set("adminOptions", $adminOptions);
		
			$filter_template = $tpl->fetch('tpl.FrmGroupFilters.php');
			$search_template = $this->fetchGroupSearchFormTemplate('allgroups', 0, false);
		  $group_list_tpl  = $this->fetchAllGroupsTemplate();
		  
		  /**if (0 == $this->loggedUserID) {
		    Template::includeDependentJs('/js/jquery-1.1.4.pack.js');
	      Template::includeDependent("<script type='text/javascript'>jQuery.noConflict();</script>");
		  }*/
		  Template::includeDependentJs('/js/allGroupsManager.js');
			
			Template::includeDependentCss("/css/vsgStandardized.css");
			Template::includeDependentCss("/css/modalBox.css");
			Template::includeDependentCss('/css/Thickbox.css');
			Template::includeDependentJs('/js/thickbox.js');
			
			$this->main_tpl->set('success',$success_message);
			$this->main_tpl->set('filter_template',$filter_template);
			$this->main_tpl->set('search_template',$search_template);
		  $this->main_tpl->set('group_list_tpl',$group_list_tpl);
		
			$this->main_tpl->out('tpl.GroupList.php');
		}
		
		/**
		 * Shows the current logged in traveler groups.
		 * main url - group.php?mode=mygroups
		 * 
		 * @param TravelerProfileCompController $profile_comp
		 * @return void
		 */
		function showMyGroupsPage($profile_comp){
			
			$search_template = $this->fetchGroupSearchFormTemplate();
		
			$subNavigation = new SubNavigation();
			$subNavigation->setContext('TRAVELER');
			$subNavigation->setContextID($this->loggedUserID);
			$subNavigation->setLinkToHighlight('MY_GROUPS');
			
			$group_list_tpl = $this->fetchTravelerGroupListTemplate($this->loggedUser,'mode=mygroups');
			
			$obj_session = SessionManager::getInstance();
			
			$success_message = ($obj_session->get('custompopup_message')) ? $obj_session->get('custompopup_message') : NULL;
			$obj_session->unsetVar('custompopup_message');
			
			$tpl = new Template();
			$tpl->set('profile',$profile_comp->get_view());
			$tpl->set('subNavigation',$subNavigation);
			$tpl->set( 'maintitle'  , 'My Groups' );
			$tpl->set('search_template',$search_template);
			$tpl->set('group_list_tpl', $group_list_tpl);
			$tpl->set('success',$success_message);
			$tpl->set('page_location', 'My Passport');
			$tpl->set('showBox', true);
			$tpl->out('travellog/views/tpl.MyGroupList.php');
		}
	
		/**
		 * Shows the groups of the this object's traveler attribute
		 * main url - group.php?travelerID=$travelerID
		 * 
		 * @param TravelerProfileCompController $profile_comp
		 * @return void
		 */	
		function showTravelerGroups($profile_comp){	

			$search_template = $this->fetchGroupSearchFormTemplate('moregroups', $this->traveler->getTravelerID(), false);
		
			$subNavigation = new SubNavigation();
			$subNavigation->setContext('TRAVELER');
			$subNavigation->setContextID($this->traveler->getTravelerID());
			$subNavigation->setLinkToHighlight('MY_GROUPS');
			
			$username = $this->traveler->getUserName();
			$username = strlen($username)-1 == strripos($username,'s')?$username."'":$username."'s";
			
			$group_list_tpl = $this->fetchTravelerGroupListTemplate($this->traveler, 'travelerID='.$this->traveler->getTravelerID());
			
			$tpl = new Template();
			$tpl->set('profile',$profile_comp->get_view());
			$tpl->set('subNavigation',$subNavigation);
			$tpl->set('maintitle'  , $username.' Groups' );
			$tpl->set('search_template',$search_template);
			$tpl->set('group_list_tpl', $group_list_tpl);
			$tpl->set('page_location', $this->traveler->getTravelerID() == SessionManager::getInstance()->get('travelerID') ? 'My Passport' : 'Travelers');
			$tpl->set('showBox', false);
			$tpl->out('travellog/views/tpl.MyGroupList.php');
		}
	
		function viewBySearchGroup()
		{
		
			//search group by name
			$cond = new Condition;
			$cond->setAttributeName("name");
			$cond->setOperation(FilterOp::$LIKE);
			$cond->setValue(addslashes($this->grpName));
		
			$filter = new FilterCriteria2();
			$filter->addCondition($cond);
			$filter->addBooleanOp('AND');
		
			if (TRUE == $this->mygroups || NULL != $this->traveler){
				// sort by date of membership desc  
				$ordercond = new Condition;
				$ordercond->setAttributeName("adate");
				$ordercond->setOperation(FilterOp::$ORDER_BY_DESC);
			
				$order = new FilterCriteria2();
				$order->addCondition($ordercond);
			}
			else {			
				// sort by inverserank of group desc 
				$ordercond = new Condition;
				$ordercond->setAttributeName("inverserank");
				$ordercond->setOperation(FilterOp::$ORDER_BY_DESC);
			
				$order = new FilterCriteria2();
				$order->addCondition($ordercond);
			
				$this->loggedUserID = 0;
			}
		
			if (NULL != $this->traveler)
				$filtertravID = $this->traveler->getTravelerID();
			else
				$filtertravID = $this->loggedUserID;
				
			if (0 == strlen($this->grpName)){			// if search key is empty string, since all groups are displayed, 
														// make sure only parent groups are displayed
				$cond2 = new Condition;
				$cond2->setAttributeName("parentID");
				$cond2->setOperation(FilterOp::$EQUAL);
				$cond2->setValue(0);
			
				$filter->addCondition($cond2);
				$filter->addBooleanOp('AND');	
			
				
				
				$grpFilteredGroups = Group::getFilteredGroups($filter, null, $order, null, $filtertravID);						
			}
		
			else {
			
				// first, get parent groups (non-sub groups)			
				$cond2 = new Condition;
				$cond2->setAttributeName("parentID");
				$cond2->setOperation(FilterOp::$EQUAL);
				$cond2->setValue(0);
			
				$filter->addCondition($cond2);
				$filter->addBooleanOp('AND');
			
				$grpFilteredGroups1 = Group::getFilteredGroups($filter, null, $order, null, $filtertravID);
			
				// next, get only sub groups; results will be merged later to avoid duplicates
				$cond2->setOperation(FilterOp::$NOT_EQUAL);
			
				$filter = new FilterCriteria2();
				$filter->addCondition($cond);
				$filter->addBooleanOp('AND');
				$filter->addCondition($cond2);
				$filter->addBooleanOp('AND');
			
				$condgrp = new Condition;
				$condgrp->setAttributeName("parentID");
				$condgrp->setOperation(FilterOp::$GROUP_BY);
			
				$groupsql = new FilterCriteria2();
				$groupsql->addCondition($condgrp);
			
				$grpFilteredGroups2 = Group::getFilteredGroups($filter, null, $order, $groupsql, $filtertravID);			
			
				// then merge both results to remove duplicates
				if (is_array($grpFilteredGroups1) && is_array($grpFilteredGroups2)){
					$grpFilteredGroups3 = array_merge($grpFilteredGroups1, $grpFilteredGroups2);
					//$grpFilteredGroups = array_unique($grpFilteredGroups);
					$grpFilteredGroups = array();
					$idlist = array();
					foreach($grpFilteredGroups3 as $eachgrp){
						if (!in_array($eachgrp->getGroupID() , $idlist )){
							$grpFilteredGroups[] = $eachgrp;
							$idlist[] = $eachgrp->getGroupID();
						}						
					}
				}
				elseif (is_array($grpFilteredGroups1))
					$grpFilteredGroups = $grpFilteredGroups1;
				elseif (is_array($grpFilteredGroups2))
					$grpFilteredGroups = $grpFilteredGroups2;
			
			}
		
			if (TRUE == $this->mygroups)
				$qrystr = 'mode=mygroups';
			else
				$qrystr ='';
			
			$paging        =  new Paging( count($grpFilteredGroups), $this->page, $qrystr , $this->numrecords );
			//$paging->setOnclick('showLoading();manager.filterSearchGroup');
			$iterator      =  new ObjectIterator( $grpFilteredGroups,$paging    );
		
			$this->main_tpl->set( 'paging'        , $paging         );
			$this->main_tpl->set( 'iterator'      , $iterator       );
					
			// set values for group categories in dropdown box
			$this->main_tpl->set("grpCategories",$this->formHelpers->CreateOptions(GroupCategory::getGroupCategoriesList(), $this->grpType ) );	
		
			$this->main_tpl->set("loggedUser",$this->loggedUser);
			$this->main_tpl->set('mygroups', $this->mygroups);
			$this->main_tpl->set('totalrec', count($grpFilteredGroups));
		
			$this->main_tpl->set('search', true);
		
			$this->main_tpl->out("tpl.ViewGroupList.php");	
		
		}
	
	}				
?>