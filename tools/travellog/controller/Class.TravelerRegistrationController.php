<?php
	/*
	 * Class.TravelerRegistrationController.php
	 * Created on Nov 20, 2007
	 * created by marc
	 */
	 
	require_once("travellog/controller/Class.AbstractRegisterController.php");
	require_once("utility/Class.EmailDomain.php");
	
	class TravelerRegistrationController extends AbstractRegisterController{
		
		protected $siteName = null;
		
		function __construct(){
			
			parent::__construct();
			$this->file_factory->registerClass('Group', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('AdminGroup', array('path' => 'travellog/model/'));
		}
		
		function performAction(){

			switch( $this->register_helper->getAction() ){
				
				case constants::FILL_UP:
					$myEmail = $this->register_helper->getInvitedEmail();
					$arrAttribute = $this->file_factory->invokeStaticClass('Group', 'getEmailInvitedByGroups', array($myEmail));
					
					if( !empty($arrAttribute) ){
						if( count($arrAttribute) > 1){
							$this->siteName = 'GoAbroad Network';
						}
						else{
							try{
								$group = $this->file_factory->getClass("AdminGroup", array($arrAttribute[0]['groupID']));
								$this->siteName = $group->getParentID() ? $group->getParent()->getName() : $group->getName();
							}
							catch(exception $e){
								$this->siteName = 'GoAbroad Network';
							}
						}
						
						return $this->viewRegistrationForm(array(
											"emailAdd" 	=> 	$arrAttribute[0]['email'],
											"firstname"	=>	$arrAttribute[0]['firstname'],
											"lastname"	=>	$arrAttribute[0]['lastname'],
											"fromEmailInvite" => TRUE ));
					}
					else{
						
						//check if login via service provider (twitter/google)
						$userData = $this->getServiceProviderAuthorization();
						if( $userData ){
							return $this->viewRegistrationForm(array(
												"emailAdd" 	=> 	$userData["email"],
												"username"	=>	TravelerPeer::composeUniqueUsername($userData["screen_name"]),
												"firstname"	=>	$userData["first_name"],
												"lastname"	=>	$userData["last_name"],
												"service_provider"	=>	$userData["service_provider"]	));
						}else{
							return $this->viewRegistrationForm();
						}
					}
					break;
				
				case constants::SUBMIT_DETAILS:
					return $this->submitDetails();
					break;
					
				case constants::CONFIRM_DETAILS:
					return $this->registerUser();
					break;		
					
				case constants::BACK:
					return $this->modifyDetails();
					break;
				
				default:
					//check if login via service provider (twitter/google)
					$userData = $this->getServiceProviderAuthorization();
					if( $userData ){
						return $this->viewRegistrationForm(array(
											"emailAdd" 	=> 	$userData["email"],
											"username"	=>	TravelerPeer::composeUniqueUsername($userData["screen_name"]),
											"firstname"	=>	$userData["first_name"],
											"lastname"	=>	$userData["last_name"],
											"service_provider"	=>	$userData["service_provider"] ));
					}else{
						return $this->viewRegistrationForm();
					}
			}
			
		}
		
		function validateUserDetails( $content = array() ){
			
			$arrErrorMessage = parent::validateUserDetails($content);
			if(EmailDomain::isBlackListed($content['emailAdd']))
				$arrErrorMessage[constants::EMAIL_BLACKLISTED] = "Registration is temporarily disabled. Please contact admin for inquiries.";
			
			// first name
			if( 0 == strlen($content["firstname"]) )
				$arrErrorMessage[constants::EMPTY_FIRSTNAME] = "First Name must be filled out.";
			// last name
			if( 0 == strlen($content["lastname"]) )
				$arrErrorMessage[constants::EMPTY_LASTNAME] = "Last Name must be filled out.";
			
			return $arrErrorMessage;
			
		}
		
		function viewRegistrationForm($content = array()){
			
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			$security = $this->file_factory->invokeStaticClass("Captcha","createImage");
			
			$travText = '<p>As a GoAbroad Network Traveler, you will be able to</p>
				<ul>
					<li><p><strong>connect with travelers</strong> from all over the world</p></li>
					<li><p><strong>blog</strong> about your travels in your very own travel journals</p></li>
					<li><p>post and share <strong>travel photos and videos</strong></p></li>
					<li><p><strong>join/create groups</strong> for your Universities, study abroad offices, travel agents, tour operators or just about any group of travel enthusiasts</p></li>
					<li><p><strong>share on Facebook</strong> - bring travel journals and travel map to Facebook, MySpace, Orkut and hi5</p></li>
				</ul>';
			$nContent = array(
				"arrErrorMessages"	=>	isset($content["arrErrorMessages"]) ? $content["arrErrorMessages"] : array(),		
				"lastname"			=>	isset($content["lastname"]) ? trim($content["lastname"]) : "",
				"firstname"			=>	isset($content["firstname"]) ? trim($content["firstname"]) : "",
				"usrname"			=>	isset($content["username"]) ? trim($content["username"]) : "",
				"password"			=>	isset($content["password"]) ? trim($content["password"]) : "",
				"emailAdd"			=>	isset($content["emailAdd"]) ? trim($content["emailAdd"]) : "",
				"subscribeNL"		=>	isset($content["subscribeNL"]) ? $content["subscribeNL"] : 0,
				"regKey"			=>	isset($content["regKey"]) ? trim($content["regKey"]) : "",
				"regID"				=>	isset($content["regID"]) ? trim($content["regID"]) : "",
				'university'		=>	isset($content['university']) ? trim($content['university']) : '',
				'chkStudent'		=>	isset($content['chkStudent']) ? trim($content['chkStudent']) : '',
				"security"			=>	$security,
				'travText'			=>	$travText,
				"agreeTerm"			=>	isset($content["agreeTerm"]) ? $content["agreeTerm"] : false,
				//'siteName'			=> 	'GoAbroad.com'
				'siteName'			=> $this->siteName,
				'redirect'			=> isset($_GET['redirect']) ? $_GET['redirect'] : '',
				'service_provider'	=> isset($content['service_provider']) ? $content['service_provider'] : NULL,
				'fromEmailInvite'	=> isset($content['fromEmailInvite']) ? $content['fromEmailInvite'] : FALSE,
			);
			Template::includeDependentCss('/min/g=RegistrationCss');
			Template::includeDependentJs('/min/g=RegistrationJs');
			Template::includeDependentJs('/min/f=js/prototype.js', array('top' => true));
			Template::includeDependentJs('/js/interactive.form.js');
			Template::includeDependentJs('js/tools/common.js');
			Template::includeDependentJs('js/utils/json.js');
			Template::includeDependentJs('captcha/captcha.js');
			$code = <<<STYLE
			<script type="text/javascript">
				var Captcha = new CaptchaLoader({					
					imageTagID	: 'captchaImg',
					onLoad		: function(params){			
						$('encSecCode').value  = params['code'];
					},
					imgAttributes : {
						width : '200',
						height: '50'	
					}
				});
				
				function changeStatusOnClick(){
					if( document.getElementById('chkStudent').checked )
						document.getElementById('liStudentBox').style.display = 'block';
					else
						document.getElementById('liStudentBox').style.display = 'none';
				}
				
			</script>
STYLE;

			Template::includeDependent($code);
			Template::setMainTemplateVar('layoutID', 'Register');
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );
			//Template::includeDependentJs('/js/popup.js');
			Template::setMainTemplateVar('title', 'Register as Traveler - Join the GoAbroad Network!');
			Template::setMainTemplateVar('metaDescription', 'Become friends with other travelers on the site, post messages for each other, join interesting forums and form clubs with those who share your interests.');
			
			Template::includeDependentCss("/css/vsgStandardized.css");
			Template::includeDependentCss("/css/modalBox.css");
			
			$fcd = $this->file_factory->getClass('RegisterFcd', $nContent, false);
			$obj_view->setContents( array('contents' => $fcd->retrieveTravelerRegistrationForm()) );
			$obj_view->render();
			
		}
		
		function submitDetails(){
			
			$travelerData = $this->register_helper->readTravelerData();
			$travelerData["arrErrorMessages"] = $this->validateUserDetails($travelerData);
			$travelerData["redirect"] = isset($_GET['redirect']) ? $_GET['redirect'] : '';
			
			if( count($travelerData["arrErrorMessages"]) ){
				foreach($travelerData as $key => $value){
					$travelerData[$key] = preg_replace("/\\\'/", "'", $value);
				}
				$this->viewRegistrationForm($travelerData);
			}
			else{
				$this->saveDetailsToTempTable($travelerData);
				foreach($travelerData as $key => $value){
					$travelerData[$key] = preg_replace("/\\\'/", "'", $value);
				}
				$this->showRegistrationDetails($travelerData);
			}
				
		}
		
		function saveDetailsToTempTable( &$travelerData = array() ){
			
			$conn = $this->file_factory->getClass("Connection");
			$rs = $this->file_factory->getClass("Recordset",array($conn));
			 
			if( $travelerData["regID"] )
				$sqlquery = "UPDATE tblTmpRegister SET ";
			else	 
				$sqlquery = "INSERT INTO tblTmpRegister SET ";
			$sqlquery .= "genName = '" . mysql_real_escape_string($travelerData["firstname"]) . "',  
					lastname = '" . mysql_real_escape_string($travelerData["lastname"]) . "', 
					username = '" . mysql_real_escape_string($travelerData["username"]) . "',		 
					password = '" . mysql_real_escape_string($travelerData["password"]) . "',	
					email = '" . mysql_real_escape_string($travelerData["emailAdd"]) . "',
					subscribeNL = ". $travelerData["subscribeNL"] . ",
					university = '" . mysql_real_escape_string(($travelerData["university"])) . "'";
			if( 0 == $travelerData["regID"] )
				$sqlquery .= " ,regKey = '" . mktime() . "'"; 
			if( $travelerData["regID"])
				$sqlquery .= " WHERE registerID = " . mysql_real_escape_string($travelerData["regID"]) . 
					" AND regKey = '" . mysql_real_escape_string($travelerData["regKey"]) . "'";
			$myquery = $rs->Execute($sqlquery);
			if( 0 == $travelerData["regID"] ){
				$sqlquery = "SELECT registerID, regKey FROM tblTmpRegister WHERE registerID = LAST_INSERT_ID()";
				$myquery = $rs->Execute($sqlquery);
				$row = mysql_fetch_array($myquery);
				$travelerData["regKey"] = $row["regKey"];
				$travelerData["regID"] = $row["registerID"];		
			}
			$travelerData["user"] = constants::TRAVELER;
		
		}
		
		function modifyDetails(){
			
			$conn = $this->file_factory->getClass("Connection");
			$rs = $this->file_factory->getClass("Recordset",array($conn));
			$travelerData = $this->register_helper->readTravelerData();
			
			$sqlquery = "SELECT * FROM tblTmpRegister " .
				"WHERE registerID = '" . mysql_real_escape_string($travelerData["regID"]) . "' " .  
				" AND regKey = '" . mysql_real_escape_string($travelerData["regKey"]) . "'";
			$myquery = $rs->Execute($sqlquery);
			if( $tmpRow = mysql_fetch_array($myquery) ){
				$travelerData["firstname"] = preg_replace("/\\\'/", "'", $tmpRow["genName"]); 
				$travelerData["lastname"] = preg_replace("/\\\'/", "'", $tmpRow["lastName"]);
				$travelerData["username"] = preg_replace("/\\\'/", "'", $tmpRow["username"]);			
				$travelerData["emailAdd"] = preg_replace("/\\\'/", "'", $tmpRow["email"]);
				$travelerData["subscribeNL"] = preg_replace("/\\\'/", "'", $tmpRow["subscribeNL"]);
				$travelerData["university"] = preg_replace("/\\\'/", "'", $tmpRow["university"]);
				$travelerData['chkStudent'] = strlen(trim(preg_replace("/\\\'/", "'", $tmpRow["university"]))) ? true : false; 
				$this->viewRegistrationForm($travelerData);
			}
			else{
				ob_end_clean();
				header("location: " . $this->register_helper->getRedirectPage(constants::REGISTER_PAGE));
				exit;
			}
		}
		
		protected function loginAuthorizedUser($registered_traveler_id=0,$oauth_api=NULL){
			$this->file_factory->registerClass('GANETSPOAuthLoginController', array('path' => 'travellog/controller/'));
			$controller = $this->file_factory->getClass("GANETSPOAuthLoginController");
			return $controller->loginUser($registered_traveler_id,$oauth_api);
		}
	} 
?>
