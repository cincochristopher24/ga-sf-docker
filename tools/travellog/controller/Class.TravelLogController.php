<?
require_once("travellog/model/Class.LocationFactory.php");
require_once("travellog/model/Class.Travel.php");
require_once("travellog/model/Class.TravelerProfile.php");
require_once("travellog/model/Class.Traveler.php");
require_once("travellog/model/Class.Trip.php");
require_once("travellog/model/Class.TravelLog.php");
require_once('Class.FormHelpers.php');
require_once('Class.Template.php');
require_once('Class.Date.php');
require_once("travellog/model/Class.SubNavigation.php");
require_once('travellog/model/navigator/Class.BackLink.php');

// Added by: Adelbert
require_once('travellog/controller/socialApps/Class.fbNeedToUpdateAccountController.php');

class TravelLogController
{
	 	public static function Add($_travelerID,$_travelID,$_title='',$_description='',$_arrivaldate = NULL,$errors=array(),$countryID=0,$current_locationID=0, $_callout = NULL, $_template_type=1)
	 	{
				require_once('travellog/model/Class.Map.php');
				require_once('travellog/model/Class.City.php');
				require_once('travellog/model/Class.Country.php');
				require_once('travellog/model/Class.TravelSchedule.php');
				
				$inctravellogtpl            = new Template();
				$maintpl                    = new Template();
				$subNavigation              = new SubNavigation();
				$arr_vars                   = array();
	 			$obj_travel                 = new Travel($_travelID);
	 			$param                      = NULL;
	 			$load_city                  = "manager.getCity();";
				$obj_trips                  = $obj_travel->getTrips();
				$obj_traveler               = new Traveler($_travelerID);
				$isAdvisor                  = $obj_traveler->isAdvisor(); 
				$isAdministrator            = $obj_traveler->isAdministrator();
				$map                        = new Map();
				
				
				if(count($obj_trips) && $countryID == 0){
					
					$current_locationID    = $obj_trips[count($obj_trips)-1]->getLocationID();
					$calendar              = FormHelpers::CreateCalendar($obj_trips[count($obj_trips)-1]->getArrival());
					$factory               = LocationFactory::instance();
					$location              = $factory->create($current_locationID);
					$countryID             = $location->getCountry()->getLocationID();
					
				}elseif ($_arrivaldate != NULL) 
					$calendar = FormHelpers::CreateCalendar($_arrivaldate);
				else 
					$calendar = FormHelpers::CreateCalendar();
				
				if ($current_locationID){	
					$param     = 'countryID='.$countryID.'&cityID='.$current_locationID;
					$load_city = "manager.getSelectedCity('$param')";
				}		
								
				$countries       = Country::getCountryList();
				$fh              = FormHelpers::CreateCollectionOption($countries,'getLocationID','getName',$countryID);
				
				$obj_backlink    = BackLink::instance();
				$obj_backlink->setReferer($_SERVER['HTTP_REFERER']);
				$obj_backlink->Create();
				
				$subNavigation->setContext('TRAVELER');
				$subNavigation->setContextID($_travelerID);
				$subNavigation->setLinkToHighlight('MY_JOURNALS');	
			
				$arr_vars['title']           = $_title; 
				$arr_vars['description']     = $_description;
				$arr_vars['callout']         = $_callout;
				$arr_vars['fh']              = $fh;
				$arr_vars['travelID']        = $_travelID;
				$arr_vars['travellogID']     = 0;
				$arr_vars['tripID']          = 0;
				$arr_vars['isAdministrator'] = $isAdministrator;
				$arr_vars['isAdvisor']       = $isAdvisor;
				$arr_vars['currentlocation'] = 0;
				$arr_vars['errors']          = $errors;
				$arr_vars['calendar']        = $calendar;
				$arr_vars['templatetype']    = $_template_type;
				$arr_vars['obj_backlink']    = $obj_backlink;
				$arr_vars['mode']            = 'Create';
				$arr_vars['journal_title']   = $obj_travel->getTitle();
				$arr_vars['action']          = "http://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']."?action=save";
				
				$inctravellogtpl->set("arr_vars" , $arr_vars);
				
				$maintpl->set_vars
				(array(
					'title'         => 'Add Travel Log',
					'map'			=> $map,
					'form'          => $inctravellogtpl->fetch('travellog/views/tpl.FrmTravelLog.php'),
					'subNavigation' => $subNavigation
				));

				// Added by: Adelbert Silla
				// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
				//           This is a preperation for updating application static views in facebook
				// Date added: Aug. 10, 2009
				// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
				fbNeedToUpdateAccountController::saveNewlyEditedAccount($formvars["travelerID"],4);

                $code = <<<BOF
<script type="text/javascript"> 
//<![CDATA[
	var manager = new TravellogManager(); 
	window.onload = function(){ 
		initmb();
		manager.useLoadingMessage();
		$load_city
	}
//]]>
</script>
BOF;
                				
	 			Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	 			Template::includeDependent($map->getJsToInclude());
	 			Template::includeDependentJs("/min/f=js/prototype.js", array('top' => true));
	 			Template::includeDependentJs("/js/scriptaculous/scriptaculous.js"); 
	 			Template::includeDependentJs("/js/addnewcity.js");
	 			Template::includeDependentJs("/js/moo1.ajax.js");
	 			Template::includeDependentJs("/js/travellogManager.js");
	 			Template::includeDependentJs("/js/utils/Class.Request.js");
	 			Template::includeDependentJs("/js/modaldbox.js");
	 			// Template::includeDependentCss("/css/modaldbox.css"); File does not exist. Is this being used anyway? Please fix!
	 			Template::includeDependentCss("/min/f=css/basic.css");
	 			Template::includeDependent($code);
	 			
	 			$maintpl->out('travellog/views/tpl.TravelLog.php');
	 	}
	 	
	 	
	 	public static function Edit($_travelerID,$_travellogID,$_title='',$_description='',$errors=array(),$locID=0,$current_locationID=0, $_callout = NULL,$_template_type=0)
	 	{
	 			require_once('travellog/model/Class.Map.php');
				require_once('travellog/model/Class.City.php');
				require_once('travellog/model/Class.Country.php');
				require_once('travellog/model/Class.TravelSchedule.php');
	 			
	 			$travellog        = new TravelLog($_travellogID);
				$travel           = new Travel($travellog->getTravelID());
				$trip             = new Trip($travellog->getTripID());
			
				$inctravellogtpl  = new Template();
				$maintpl          = new Template();
				$date             = ClassDate::instance();
				$factory          = LocationFactory::instance();
				$location         = $factory->create($trip->getLocationID());
				$countries        = Country::getCountryList();
				$map              = new Map();
				
				if ($locID == 0){	
					$locID              = $location->getCountry()->getLocationID();
					$param              = 'countryID='.$locID.'&cityID='.$trip->getLocationID();
					$current_locationID = $trip->getLocationID();
				}else
					$param              = 'countryID='.$locID.'&cityID='.$current_locationID;
					
				$fh                     = FormHelpers::CreateCollectionOption($countries,'getLocationID','getName',$locID);
				
								
				$subNavigation    = new SubNavigation();
				$subNavigation->setContext('TRAVELER');
				$subNavigation->setContextID($_travelerID);
				$subNavigation->setLinkToHighlight('MY_JOURNALS');	
				
				$obj_backlink     = BackLink::instance();
				$obj_backlink->Create();
				
				if( !count($errors) ){
					$_description   = $travellog->getDescription();
					$_title         = $travellog->getTitle();
					$_callout       = $travellog->getCallOut();
					$_template_type = $travellog->getTemplateType();
				}
				
				$obj_current_location = new Traveler($_travelerID);
				$location             = $obj_current_location->getPresentLocation();
 				$currentlocation      = 0;
 				if( $location != NULL ){
					if( $location->getLocationID() == $current_locationID ) 
						$currentlocation = 1;
	 			} 
				
				/*$facebook_userID              = TravelSchedule::getUserIDWithTravelerID( $_travelerID );
				$arr_vars["is_facebook_user"] = false;
				$arr_vars['status']           = 0;
				if( $facebook_userID ){
					$obj_travel_sched             = new TravelSchedule;
					$facebook_travel_wishID       = TravelSchedule::getTravelWishIDByTravelID( $travellog->getTravelID() );
					$obj_travel_item              = $obj_travel_sched->getTravelItem( $facebook_travel_wishID );
					$arr_vars['status']           = $obj_travel_item->getStatusID(); 
					$arr_vars["is_facebook_user"] = true;                   
				}*/ 
				
				$calendar = FormHelpers::CreateCalendar($trip->getArrival());
				
				$arr_vars['title']           = $_title; 
				$arr_vars['description']     = $_description;
				$arr_vars['callout']         = $_callout;
				$arr_vars['fh']              = $fh;
				$arr_vars['travelID']        = $travellog->getTravelID();
				$arr_vars['travellogID']     = $_travellogID;
				$arr_vars['tripID']          = $travellog->getTripID();
				$arr_vars['currentlocation'] = $currentlocation;
				$arr_vars['errors']          = $errors;
				$arr_vars['calendar']        = $calendar;
				$arr_vars['isAdministrator'] = 0;
				$arr_vars['isAdvisor']       = 0;
				$arr_vars['templatetype']    = $_template_type;
				$arr_vars['obj_backlink']    = $obj_backlink;
				$arr_vars['journal_title']   = $travellog->getTravel()->getTitle();
				$arr_vars['mode']            = 'Edit';
				$arr_vars['action']          = "http://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']."?action=update";
				
				$inctravellogtpl->set("arr_vars" , $arr_vars);
				
				$maintpl->set_vars
				(array(
					'title'         => 'Add Travel Log',
					'map'			=> $map,
					'form'          => $inctravellogtpl->fetch('travellog/views/tpl.FrmTravelLog.php'),
					'subNavigation' => $subNavigation
				));
				
				
				
				$code = <<<BOF
<script type="text/javascript"> 
//<![CDATA[
	var manager = new TravellogManager(); 
	window.onload = function(){ 
		initmb();
		manager.useLoadingMessage();
		manager.getSelectedCity('$param');
	}
//]]>
</script>
BOF;
				
				Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
				Template::includeDependent($map->getJsToInclude());
	 			Template::includeDependentJs("/js/addnewcity.js");
	 			Template::includeDependentJs("/js/moo1.ajax.js");
	 			Template::includeDependentJs("/js/travellogManager.js");
	 			Template::includeDependentJs("/js/utils/Class.Request.js");
	 			Template::includeDependentJs("/js/modaldbox.js");
	 			// Template::includeDependentCss("/css/modaldbox.css"); File does not exist. Is this being used anyway? Please fix!
	 			Template::includeDependentCss("/min/f=css/basic.css");
	 			Template::includeDependent($code);
				$maintpl->out('travellog/views/tpl.TravelLog.php');
	 	}		
	 	
	 	public static function Delete($_travelerID,$_travellogID){
 			if (isset($_travellogID)){
				require_once("travellog/controller/Class.FacebookAppImplController.php"); 
				require_once("travellog/controller/Class.gaSubject.php");
				require_once("travellog/controller/Class.gaAbstractEvent.php");
				
				$travellog = new TravelLog($_travellogID);
				$travellog->setTravelLogID($_travellogID);
				$_countryID  = $travellog->getTrip()->getLocation()->getCountry()->getCountryID();
				$_locationID = $travellog->getTrip()->getLocationID();
				$_travelID   = $travellog->getTravelID();
				$travellog->Delete();
												
				$props              = array();
				$obj_subject        = new gaSubject;
				$obj_facebook       = new FacebookAppImplController;
				$props['travelID']  = $_travelID;
				$props['type']      = 'entry';
				$props['locationID']= $_locationID;
				$props['countryID'] = $_countryID;
				$props['action']    = gaActionType::$DELETE; 
				
				$obj_event = new gaAbstractEvent(null, $props);
				
				$obj_subject->registerObserver($obj_facebook);
				
				$obj_subject->notifyObservers($obj_event); 
				
				
				require_once('travellog/model/Class.JournalLog.php');
				$objJournalLog = new JournalLog;
				$objJournalLog->setLogType( 1             );
				$objJournalLog->setLinkID ( $_travellogID );
				$objJournalLog->Delete();
				
				// Added by: Adelbert Silla
				// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
				//           This is a preperation for updating application static views in facebook
				// Date added: Aug. 10, 2009
				// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
				fbNeedToUpdateAccountController::saveNewlyEditedAccount($_travelerID,4);				

			}
	 	}	
	 	
	 	public static function Save($_travelerID,$_travelID,$_title,$_description,$_cityID,$_countryID,$_currentlocation,$_arrivaldate,$_groupmembers,$_addressbook, $_callout = NULL, $status, $_template_type=0,$_groupmembersaddressbook=0)
	 	{

				require_once('travellog/model/Class.JournalLog.php');
				require_once('travellog/model/Class.JournalLogType.php');
				require_once("travellog/controller/Class.FacebookAppImplController.php"); 
				require_once("travellog/controller/Class.MailImplController.php");
				require_once("travellog/controller/Class.gaSubject.php");
				require_once("travellog/controller/Class.gaAbstractEvent.php"); 

	 			$trip = new Trip();
				(intval($_cityID) > 0)? $locationID=intval($_cityID) : $locationID=intval($_countryID);
				
				$tripID = Trip::TripExists($_travelID,$locationID,$_arrivaldate); 
				if ($tripID == 0){
					$date        = ClassDate::instance();
					$arrivaldate = $_arrivaldate;
					$trip->setLocationID($locationID);
					$trip->setArrival($arrivaldate); 
					$trip->setTravelID($_travelID);
					$trip->Create();
					$tripID = $trip->getTripID();
				} 
				if($_currentlocation){
					$currentlocation = new Traveler($_travelerID);
					$currentlocation->setPresentLocation($locationID);
				}
				
				$travellog = new TravelLog();
				$travellog->setTitle       ( addslashes(str_replace('/', '', $_title)));
				$travellog->setTripID      ( $tripID                      );
				$travellog->setTravelID    ( $_travelID                   );
				$travellog->setDescription ( $_description                );
				$travellog->setCallOut     ( $_callout                    );
				$travellog->setTemplateType( $_template_type              );
				$travellog->Create();
				
				
				$obj_factory  = LocationFactory::instance();
				$obj_location = $obj_factory->create($locationID);
				$props        = array();
				$obj_subject  = new gaSubject;
				$obj_facebook = new FacebookAppImplController;
				$obj_mail     = new MailImplController;
				
				$props['travelID']                = $_travelID;
				$props['locationID']              = $locationID;  
				$props['travelerID']              = $_travelerID;
				$props['description']             = $_description;
				$props['arrivaldate']             = $_arrivaldate;
				$props['groupmembers']            = $_groupmembers;
				$props['addressbook']             = $_addressbook;
				$props['travellogID']             = $travellog->getTravelLogID();
				$props['groupmembersaddressbook'] = $_groupmembersaddressbook;
				$props['countryID']               = $obj_location->getCountry()->getCountryID();
				$props['action']                  = gaActionType::$SAVE;
				
				$obj_event = new gaAbstractEvent(null, $props);
				
				
				$obj_subject->registerObserver($obj_mail);
				
				$obj_subject->notifyObservers($obj_event);
				
				
				$obj_log = new JournalLog;
				$obj_log->setTravelID    ( $_travelID                       );
				$obj_log->setLogType     ( JournalLogType::$ADDED_NEW_ENTRY );
				$obj_log->setTravelLogID ( $travellog->getTravelLogID()     );
				$obj_log->Save();
				
				// Added by: Adelbert Silla
				// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
				//           This is a preperation for updating application static views in facebook
				// Date added: Aug. 10, 2009
				// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
				fbNeedToUpdateAccountController::saveNewlyEditedAccount($_travelerID,4);				
				
				/**
				 * Added by Naldz
				 * Date: Decemeber 20, 2006
				 * Purpose: Send an amail notification to the traveler's addressbook entries whenever he/she updates or add a Travel/Journal
				 */
				/*if ($_groupmembers || $_addressbook){ 
					require_once("travellog/model/notification/Class.Notifier.php");
					$traveler = new Traveler($_travelerID);
					$notifier = new Notifier($_travelerID);
					$notifier->setNotificationTag(Notifier::TRAVEL_JOURNAL_ENTRY_ADD);
					$notifier->setVar('travellog',$travellog);

					if ($_groupmembers) $notifier->includeGroupMembers(true);
					if ($_addressbook) $notifier->includeGroupAddressBook(true);	
					$notifier->sendNotification();
				}*/
				return $travellog->getTravelLogID();
	 	}
		 	
	 	public static function Update($_travelerID,$_travelID,$_travellogID,$_title,$_description,$_cityID,$_countryID,$_currentlocation,$_arrivaldate,$_tripID, $_callout = NULL,$status, $_template_type=0)
	 	{
	 			require_once('travellog/model/Class.JournalLog.php');
				require_once('travellog/model/Class.JournalLogType.php');
				require_once("travellog/controller/Class.FacebookAppImplController.php"); 
				require_once("travellog/controller/Class.gaSubject.php");
				require_once("travellog/controller/Class.gaAbstractEvent.php");
	 			
	 			$trip                       = new Trip($_tripID);
				$travellogID                = $_travellogID;
				$locationID                 = (intval($_cityID) > 0)?intval($_cityID) : intval($_countryID);
				$arrivaldate                = $_arrivaldate;		
				$tripID                     = Trip::TripExists($_travelID,$locationID,$arrivaldate);
				$date                       = ClassDate::instance(); 
								
				if ($tripID==0){
					
					$trip->setLocationID($locationID);
					$trip->setArrival($arrivaldate);
					if (count($trip->getTravelLogs()) > 1) 
						$trip->Create();
					else{
						$trip->setTripID($_tripID);
						$trip->Update();
					} 
					$tripID = $trip->getTripID();
				} 
				else{
					$trip->setLocationID($locationID);
					$trip->setTripID($_tripID);
					$trip->setArrival($arrivaldate);
					$tripID = $_tripID;
				}
				
				$objTraveler = new Traveler($_travelerID);
				
				if( $_currentlocation )
					$objTraveler->setPresentLocation( $locationID );
				elseif( $objTraveler->getPresentLocation() != NULL ){ 
					if( $objTraveler->getPresentLocation()->getLocationID() == $locationID )
						$objTraveler->setPresentLocation(0);
				}	
				
				$travellog = new TravelLog($travellogID);
				$travellog->setTripID      ( $tripID                       );
				$travellog->setTitle       ( str_replace('/', '', $_title) );
				$travellog->setDescription ( $_description                 );
				$travellog->setCallOut     ( $_callout                     );
				$travellog->setTemplateType( $_template_type               );
				$travellog->Update();
				
				$obj_factory  = LocationFactory::instance();
				$obj_location = $obj_factory->create($locationID);
				$props        = array();
				$obj_subject  = new gaSubject;
				$obj_facebook = new FacebookAppImplController;

				// Added by: Adelbert Silla
				// Purpose:  To save traveler to tblfbNeedToUpdateAccount. 
				//           This is a preperation for updating application static views in facebook
				// Date added: Aug. 10, 2009
				// params: $travelerID and $Application = ( MTB=1 or MTM=2 or MTP=3 or MTJ=4 )
				fbNeedToUpdateAccountController::saveNewlyEditedAccount($_travelerID,4);
				
				$props['travelID']    = $_travelID;
				$props['locationID']  = $locationID;
				$props['travelerID']  = $_travelerID;
				$props['description'] = $_description;
				$props['arrivaldate'] = $arrivaldate;
				$props['countryID']   = $obj_location->getCountry()->getCountryID();
				$props['action']      = gaActionType::$UPDATE;
				
				
				$obj_event = new gaAbstractEvent(null, $props);
				
				$obj_subject->registerObserver($obj_facebook);
				
				$obj_subject->notifyObservers($obj_event);
				
	 	}	
	 
	 	static function isOwner($travelerID,$primaryID,$context){
	 		$owner         = false; 
	 		switch($context){
	 			case 'journal':
	 				$obj_travel    = new Travel($primaryID);
	 				if ($obj_travel->getTravelerID()==$travelerID) $owner = true;
	 				break;
	 			case 'entry':
	 				$obj_travellog = new TravelLog($primaryID);
	 				$obj_travel    = $obj_travellog->getTravel();
	 				if ($obj_travel->getTravelerID()==$travelerID) $owner = true;
	 				break;
	 		}
	 		 
	 		return $owner;
	 	}
	 
	 	static function hasParameters( $req_vars ){
	 		return ( !isset($req_vars['travelID']) )? false: true;
	 	}
	 
	 	static function render( $req_vars ){
	 		require_once("Class.Criteria2.php");
	 		
	 		$obj_criteria          = new Criteria2;
	 		$tpl_main              = new Template; 
	 		$obj_travel            = new Travel( $req_vars['travelID'] );
	 		$arr_trips             = $obj_travel->getTrips();
	 		$arr_entry             = array();
	 		$current_pos           = 0;
	 		$ctr                   = 0;
	 		$req_vars['ownerID']   = $obj_travel->getTravelerID();  
	 		
	 		$obj_criteria->setOrderBy("travellogID"); 
	 		
	 		if( count($arr_trips) ){
	 			require_once("travellog/facade/Class.EntryTableOfContentsFcd.php");
	 			require_once("travellog/facade/Class.EntryHighlightsFcd.php");
	 			require_once("travellog/facade/Class.EntryTipsFcd.php");
	 			require_once("travellog/facade/Class.ProfileFcd.php");
	 			require_once("travellog/facade/Class.EntryFcd.php");
	 			require_once("travellog/facade/Class.MapFcd.php");
	 			
	 			$obj_template_content  = new TemplateContent;
	 			
	 				 			
	 			foreach( $arr_trips as $obj_trip ){
	 				$arr_temp_entry = $obj_trip->getTravelLogs($obj_criteria);
	 				
	 				if( count($arr_temp_entry) ){
	 					
	 					foreach( $arr_temp_entry as $obj_temp_entry ){
	 						$arr_entry[] = $obj_temp_entry;
	 					
	 						if( $obj_temp_entry->getTravelLogID() === $req_vars['travellogID'] ){
	 							$current_pos = $ctr;	
	 						}						
	 						$ctr++;
	 					}
	 				}	
	 			}
	 			
	 			$obj_current_entry = $arr_entry[$current_pos];
	 			$show_controls     = $this->showControls( $req_vars );
	 			
	 			$arr_vars['show_controls'] = $show_controls;
	 			$arr_vars['travellogID']   = $obj_current_entry->getTravelLogID();
	 			$arr_vars['template_type'] = $obj_current_entry->getTemplateType();
	 			$arr_vars['current_pos']   = $current_pos;
	 			$arr_vars['col_entry']     = $arr_entry;
	 			$arr_vars['travelerID']    = $req_vars['ownerID'];
	 			
	 			$obj_table_of_contents_fcd = new EntryTableOfContentsFcd;
	 			$obj_highlights_fcd        = new EntryHighlightsFcd;
	 			$obj_tips_fcd              = new EntryTipsFcd;
	 			$obj_profile_fcd           = new ProfileFcd;
	 			$obj_map_fcd               = new MapFcd;
	 			   
	 			
	 			$obj_table_of_contents_fcd->retrieveView ( $arr_vars );
	 				 			
	 			
	 			
	 			$obj_tips_fcd->retrieveByFilterView      ( $arr_vars );
	 			
	 			$obj_profile_fcd->retrieveByFilterView   ( $arr_vars );
	 			
	 			$obj_map_fcd->retrieveByFilterView       ( $arr_vars );
	 			
	 			$obj_entry_fcd1 = new EntryFcd( EntryViewType::$EntryTitleCalloutPhoto );
	 			$obj_entry_fcd1->retrieveByFilterView    ( $arr_vars );
	 			
	 			$obj_entry_fcd2 = new EntryFcd( EntryViewType::$EntryDescription );
	 			$obj_entry_fcd2->retrieveByFilterView    ( $arr_vars );
	 			
	 			$obj_highlights_fcd->retrieveByFilterView( $arr_vars );
	 			
	 		}else{
	 			
	 			$tpl_main->out("travellog/views/tpl.NoEntries.php");
	 		}	 			
	 		
	 	}
	 	
	 	function showControls( $req_vars ){
	 		return ( $req_vars['isLogin'] && $req_vars['travelerID'] == $req_vars['ownerID'] )? true: false;
	 	}
}
?>
