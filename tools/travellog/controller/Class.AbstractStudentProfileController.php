<?php
	/*
	 * Class.AbstractStudentProfileController.php
	 * Created on Nov 29, 2007
	 * created by marc
	 */
	
	require_once("travellog/factory/Class.FileFactory.php");
	require_once("Class.Constants.php");
	
	class AbstractStudentProfileController{
		
		static $file_factory = NULL;
		static $session_manager = NULL;
		static $spHelper=  NULL;
		static $studentProfile = NULL;
		static $traveler = NULL;
		
		function __construct(){
			
			$this->file_factory = FileFactory::getInstance();
			
			$this->file_factory->registerClass("SessionManager", array("path" => "travellog/model"));
			$this->session_manager = $this->file_factory->invokeStaticClass("SessionManager", "getInstance");
			
			if( !$this->session_manager->getLogin() ){
				header("location: login.php?failedLogin&redirect=student-profile.php");
			}
			
			$this->file_factory->registerClass("Listing", array("path" => "travellog/model"));
			$this->file_factory->registerClass("Client", array("path" => "travellog/model"));
			$this->file_factory->registerClass("StudentPreference", array("path" => "travellog/model"));
			$this->file_factory->registerClass("ListingProgram", array("path" => "travellog/model"));
			$this->file_factory->registerClass("ListingProgramType", array("path" => "travellog/model"));
			$this->file_factory->registerClass("StudentProfile", array("path" => "travellog/model"));
			$this->file_factory->registerClass("SubNavigation", array("path" => "travellog/model"));
			$this->file_factory->registerClass("RowsLimit", array("path" => "travellog/model"));
			$this->file_factory->registerClass("Country", array("path" => "travellog/model"));
			$this->file_factory->registerClass("Traveler", array("path" => "travellog/model"));
			$this->file_factory->registerClass("HelpText", array("path" => "travellog/model"));
			$this->file_factory->registerClass("StudentProfileHelper", array("path" => "travellog/helper"));
			$this->file_factory->registerClass("HybridPaging", array("path" => ""));
			$this->file_factory->registerClass("MainLayoutView", array("path" => "travellog/views/"));
			$this->file_factory->registerClass("AbstractView", array("path" => "travellog/views/"));
			$this->file_factory->registerClass("StudentProfileFcd", array("path" => "travellog/facade/"));
			$this->file_factory->registerClass("Connection", array("path" => ""));
			$this->file_factory->registerClass("Recordset", array("path" => ""));
			$this->file_factory->registerClass("FormHelpers", array("path" => ""));
			
			$this->spHelper = $this->file_factory->invokeStaticClass("StudentProfileHelper", "getInstance");
			$this->studentProfile = $this->file_factory->getClass("StudentProfile", array($this->session_manager->get("travelerID")));
			$this->traveler = $this->file_factory->getClass("Traveler", array($this->session_manager->get("travelerID")));
			
			// views
			$this->file_factory->registerTemplate('LayoutMain');
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
			
		}
		
		function performAction(){
			
			switch( $this->spHelper->getAction() ){
				case constants::EDIT:
					return $this->editStudentProfile();
					
				case constants::VIEW_REFERED_LISTINGS:
					return $this->viewReferedListings();	
				
				case constants::SUBMIT_DETAILS:
					return $this->updateStudentProfile();
				
				case constants::VIEW_STUDENT_COMMUNICATIONS:
					return $this->viewStudentCommunications();
					
				case constants::VIEW_LISTING:
					return $this->viewListing();	
				
				case constants::VIEW_CLIENT:
					return $this->viewClient();		
						
				default:
					$this->viewStudentProfile();	
			}
			
		}
		
		function editStudentProfile(){
			
			$studCountryChoices = $this->studentProfile->getCountryChoice();
			$countryChoices = array();
			for( $i=0; $i < 3; $i++ ){
				if ( isset($studCountryChoices[$i]) )
					$countryChoices[$i] = $studCountryChoices[$i]->getCountryID();
				else
					$countryChoices[$i] = 0;	 
			}
			
			$selcountry = array();
			$nConn = $this->file_factory->getClass("Connection");
			$nRecordset = $this->file_factory->getClass("Recordset", array($nConn));
			
			$sqlquery = "SELECT GoAbroad_Main.tbcountry.*
				FROM GoAbroad_Main.tbcountry,GoAbroad_Main.tblistclasstocountry,GoAbroad_Main.tblisting
				WHERE GoAbroad_Main.tbcountry.countryID = GoAbroad_Main.tblistclasstocountry.countryID
				AND GoAbroad_Main.tblisting.listingID = GoAbroad_Main.tblistclasstocountry.listingID
				AND GoAbroad_Main.tblisting.approved = 1
				AND GoAbroad_Main.tbcountry.approved = 1
				AND date_format(expiration,'%Y-%m-%d') > date_format('" . date('Y-m-d') ."','%Y-%m-%d')
				GROUP BY countryID
				ORDER BY country";

			$myquery = $nRecordset->Execute($sqlquery);
			while ($row = mysql_fetch_array($myquery)) {
		 			$country = $this->file_factory->getClass("Country");
		 			$country->setLocationID($row['locID']);
			 		$country->setContinentID($row['continentID']);
			 		$country->setCountryID($row['countryID']);
			 		$country->setName($row['country']);
			 		$selcountry[] = $country;
			}	
			
			$nContent = array(
				'title'				=>	"Edit Student Profile - " . $this->traveler->getTravelerProfile()->getFirstName(),
				'action'			=>	"student-profile.php?update",
				'preferences'		=>	$this->file_factory->getClass("StudentPreference", array(1))->getPreference(),
				'studentPreference'	=>	$this->studentProfile->getPreference(),
				'countryOption0'	=>	$this->file_factory->invokeStaticClass("FormHelpers","CreateCollectionOption", array($selcountry, 'getCountryId', 'getName',$countryChoices[0])),
				'countryOption1'	=>	$this->file_factory->invokeStaticClass("FormHelpers","CreateCollectionOption", array($selcountry, 'getCountryId', 'getName',$countryChoices[1])),
				'countryOption2'	=>	$this->file_factory->invokeStaticClass("FormHelpers","CreateCollectionOption", array($selcountry, 'getCountryId', 'getName',$countryChoices[2])),
				'subNavigation' 	=>	$this->getsubNavigation()
			);
			
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			
			Template::setMainTemplateVar('page_location', 'My Passport');
			Template::includeDependentJs("/js/interactive.form.js");
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') ); 
			
			$fcd = $this->file_factory->getClass('StudentProfileFcd', $nContent, false);
			$obj_view->setContents( array('contents' => $fcd->retrieveStudentProfileForm()) );
			$obj_view->render();
		}
		
		function viewStudentProfile(){
			
			$helpText = "";			
			if ( !$this->studentProfile->studentProfileExists($this->session_manager->get("travelerID")) )
				$helpText = nl2br($this->file_factory->getClass("HelpText")->getHelpText('inquiries'));
			
			$nContent = array( 
				"student" 			=>	$this->studentProfile,
				"traveler" 			=>	$this->traveler,
				"countryCol" 		=>  $this->studentProfile->getCountryChoice(),
				"countCountry" 		=>	count($this->studentProfile->getCountryChoice()),
				"preferenceList" 	=>	$this->studentProfile->getPreference()->getPreference(),
				"arrListings" 		=>	$this->file_factory->invokeStaticClass("Listing", "getStudentReferedListings", array($this->session_manager->get("travelerID",1,1))),
				"title"				=>	"Student Profile",
				"helpText"			=>	$helpText,
				"subNavigation" 	=>	$this->getSubNavigation(),
				"firstName" 		=>	$this->traveler->getTravelerProfile()->getFirstName(),
				"allowed" 			=>	strlen($this->traveler->getTravelerProfile()->getFirstName()),
				"obj_paging"		=>	$this->file_factory->getClass("HybridPaging", array( $this->file_factory->invokeStaticClass("Listing", "getCountStudentReferedListings", array($this->session_manager->get("travelerID"))), 1, '', 1, constants::MODE_AJAX	, 'listings', 'student-profile.php?refList'))
			);
			
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			
			Template::includeDependentJs('/js/prototype.js');
	   		Template::includeDependentJs('/js/moo.ajax.js');   
			Template::includeDependentJS('/js/student-communication.js');
			Template::includeDependentCss('/css/style_communication.css');
			Template::setMainTemplateVar('page_location', 'My Passport');
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );   
			
			$fcd = $this->file_factory->getClass('StudentProfileFcd', $nContent, false);
			$obj_view->setContents( array('contents' => $fcd->retrieveStudentProfilePage()) );
			$obj_view->render();
		}
		
		function updateStudentProfile(){
			
			$spData = $this->spHelper->readSPData();
			$countryChoices = array();
			if ( 0 < $spData['firstOption'] )
				$countryChoices[] = $spData['firstOption'];
			if ( 0 < $spData['secondOption'] )
				$countryChoices[] = $spData['secondOption'];
			if ( 0 < $spData['thirdOption'] )
				$countryChoices[] = $spData['thirdOption'];
			
			// update country choices
			$this->studentProfile->updateCountryChoices($countryChoices);
			
			$nPref = $this->file_factory->getClass('StudentPreference');
			$nProg = $this->file_factory->getClass('ListingProgram');
			
			foreach( $spData["programTypes"] as $key => $value){
				$prog = $this->file_factory->getClass("ListingProgram", array($key));
				$arrPTID = array();
				foreach( $value as $progTypeID ){
					$nProgType = $this->file_factory->getClass('ListingProgramType');
					$nProgType->setProgramTypeID($progTypeID);
					$arrPTID[] = $nProgType;
				}
				$prog->setProgramType($arrPTID);
				$nPref->addPreference($prog);
			}
			if( in_array(2,$spData["programs"]) ){
				$prog = $this->file_factory->getClass("ListingProgram", array(2));
				$nPref->addPreference($prog);
			}
			
			// update student preference	
			$this->studentProfile->updateStudentPreference($nPref);
			header ('location:student-profile.php');
		}
		
		/**
		 * start of misc functions
		 */
		
		function getSubNavigation(){
			
			$subNavigation = $this->file_factory->getClass("SubNavigation");
			$subNavigation->setContext("TRAVELER");
			$subNavigation->setContextID($this->session_manager->get("travelerID"));
			$subNavigation->setLinkToHighlight('STUDENT_SECTION');
			return $subNavigation;
		}	
		/**
		 * end of misc functions
		 */ 
		 
		/**
		 * start of ajax functions
		 */ 
		 
		function viewReferedListings(){
			
			$nContent = array( 
				"student" 			=>	$this->studentProfile,
				"traveler" 			=>	$this->traveler,
				"arrListings" 		=>	$this->file_factory->invokeStaticClass("Listing", "getStudentReferedListings", array($this->session_manager->get("travelerID"),$this->spHelper->getPage(),1)),
				"obj_paging"		=>	$this->file_factory->getClass("HybridPaging", array( $this->file_factory->invokeStaticClass("Listing", "getCountStudentReferedListings", array($this->session_manager->get("travelerID"))), $this->spHelper->getPage(), '', 1, constants::MODE_AJAX	, 'listings', 'student-profile.php?refList'))
			);
			
			$fcd = $this->file_factory->getClass('StudentProfileFcd', $nContent, false);
			echo $fcd->retrieveReferedListings()->render();
			
		}
		
		function viewStudentCommunications(){
			
			$viewCommData = $this->spHelper->readViewCommData();
			$rowsLimit = $this->file_factory->getClass('RowsLimit', array($viewCommData['rowsperpage'],$viewCommData['page']-1));
			
			$msgList = $this->traveler->getInquiryBox();
			$messages = $msgList->getStudentMessagesPerStudentAndListing($viewCommData['listingID'],$rowsLimit);
			
			$nContent = array( 
				"msgList"			=>	$this->traveler->getInquiryBox()->getStudentMessagesPerStudentAndListing($viewCommData['listingID'],$rowsLimit),
				"obj_paging"		=>	$this->file_factory->getClass("HybridPaging", array( $msgList->getTotalRecords(), $viewCommData['page'], 'listingID='.$viewCommData['listingID'], $viewCommData['rowsperpage'], constants::MODE_AJAX, 'inquirycomm_'.$viewCommData['listingID'], 'student-profile.php?viewComm'))
			);
			
			$fcd = $this->file_factory->getClass('StudentProfileFcd', $nContent, false);
			echo $fcd->retrieveStudentCommunications()->render();
			
		} 
		
		function viewListing(){
			
			$viewCommData = $this->spHelper->readViewCommData();
			$nContent = array(
				'listing'			=>	$this->file_factory->getClass('Listing',array($viewCommData['listingID'])),
			);
			
			$fcd = $this->file_factory->getClass('StudentProfileFcd', $nContent, false);
			echo $fcd->retrieveListing()->render();
		}
		
		function viewClient(){
			
			$viewCommData = $this->spHelper->readViewCommData();
			$nContent = array(
				'client'			=>	$this->file_factory->getClass('Client',array($viewCommData['clientID'])),
			);
			
			$fcd = $this->file_factory->getClass('StudentProfileFcd', $nContent, false);
			echo $fcd->retrieveClient()->render();
		}
		
		/**
		 * end of ajax calls
		 */ 
		
	}
	
?>
