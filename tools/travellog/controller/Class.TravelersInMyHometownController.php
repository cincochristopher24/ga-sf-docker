<?php
require_once('travellog/controller/Class.AbstractTravelerController.php');
require_once('travellog/model/Class.SearchFirstandLastnameService.php');
require_once('travellog/model/Class.TravelerSearchCriteria2.php');
require_once("travellog/model/Class.RowsLimit.php");
require_once("Class.Criteria2.php");
class TravelersInMyHometownController extends AbstractTravelerController{
	function viewContents(){
		$this->viewMain();
		$obj_travelers  = array();
		$obj_rows_limit = new RowsLImit($this->rows, $this->offset);
		$obj_traveler   = new Traveler($this->travelerID);
		$obj_travelers  = $obj_traveler->getOtherTravelersInMyHomeTown(NULL, $obj_rows_limit);
		$query_string   = 'action=inmyhometown';
		
		$obj_paging     = new Paging        ( $obj_traveler->getNumberOfTravelers(), $this->page,$query_string, $this->rows );
		$obj_iterator   = new ObjectIterator( $obj_travelers, $obj_paging, true                                             );
		
		$this->main_tpl->set( 'obj_paging'   , $obj_paging       );
		$this->main_tpl->set( 'obj_iterator' , $obj_iterator     );
		$this->main_tpl->set( 'selected'     , 2                 );
		$this->main_tpl->set( 'travelerID'   , $this->travelerID );
		$this->main_tpl->set( 'isLogin'      , $this->IsLogin    );
		$this->main_tpl->set( 'isAdvisor'    , $this->isAdvisor  );
		
		$this->main_tpl->set( 'mainContent', $this->main_tpl->fetch("travellog/views/tpl.ViewTravelersList.php") );
		$this->main_tpl->out("travellog/views/tpl.Travelers.php");		
	}
}
?>
