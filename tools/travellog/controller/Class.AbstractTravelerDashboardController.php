<?php

	require_once("travellog/factory/Class.FileFactory.php");
	require_once("travellog/controller/Class.IController.php");
	require_once("Class.Template.php");
	
	require_once("travellog/components/activity_feed/factory/Class.ActivityFeedAdapterFactory.php");
	require_once("travellog/model/Class.SiteContext.php");
	
	class AbstractTravelerDashboardController implements iController{
		protected $mSession			=	NULL;
		protected $file_factory		=	NULL;
		protected $mTemplate		=	NULL;
		protected $mSubNavigation	=	NULL;
		protected $mContextInfo		=	NULL;
		
		protected $mGroup			=	NULL;
		protected $mTraveler		=	NULL;
		
		protected $mParams			=	array(	"action"	=>	NULL,
												"mode"		=>	"RECENT",
		 										"limitID"	=>	0,
		 										"timezone"	=>	0	);
		
		function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template', array('path' => ''));
			$this->file_factory->registerClass('SessionManager');
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('Travel');
			
			$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
			$this->file_factory->registerClass('ViewTravelerDashboard', array('path' => 'travellog/views/traveler_dashboard/'));
			$this->file_factory->registerClass('ProfileCompFactory', array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerTemplate('LayoutMain');

			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			$this->mSession = $this->file_factory->invokeStaticClass('SessionManager', 'getInstance');
			$this->mLoggedID = $this->mSession->get('travelerID');
		}
		
		function performAction(){
			$this->mParams = array_merge($this->mParams,$this->extractParameters());
			
			switch(strtoupper(trim($this->mParams["action"]))){
				case "FETCH"				:	$this->_fetchFeeds();
												break;
				case "LOAD_SURVEY_LIST" 	:	$this->_loadSurveyList();
												break;
				default						:	$this->_render();
												break;
			}
		}
		
		function extractParameters(){
			$params = $_REQUEST;
			$paramArray = array();

			if( isset($params) ){
				foreach( $params as $key => $value){
					$paramArray[$key] = $value;
				}
			}
			return $paramArray;
		}
		
		function _defineCommonAttributes(){
			$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			
			$this->mTemplate = $this->file_factory->getClass('Template');
			$this->mTemplate->set_path('travellog/views/traveler_dashboard/');
			
			Template::setMainTemplateVar('page_location','My Passport');
			
			$this->mSubNavigation = $this->file_factory->getClass('SubNavigation');
			$this->mSubNavigation->setContext('TRAVELER');
			$this->mSubNavigation->setContextID($this->mTraveler->getTravelerID());
			$this->mSubNavigation->setLinkToHighlight('MY_DASHBOARD');
			
			$mProfileComp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$TRAVELER_CONTEXT );
			$mProfileComp->init($this->mTraveler->getTravelerID(),NULL,FALSE);
						
			$this->mContextInfo = $mProfileComp->get_view();
		}
		
		function _checkPrivileges($redirect=FALSE){
			// check if user is currently logged in
			$this->mTraveler = $this->file_factory->getClass('Traveler', array($this->mLoggedID));
			
			if ( 0 < $this->mLoggedID ) {
				// start GA Logger
				require_once 'gaLogs/Class.GaLogger.php';
				try{
					GaLogger::create()->start($this->mLoggedID, 'TRAVELER');
				}
				catch(exception $e){
				}
			}elseif($redirect){
				if( $this->_isAjaxRequest() ){
					exit;
				}
				header("Location: /login.php?failedLogin&redirect=/travelerdashboard.php");
				exit;
			}
		}
		
		function _isAjaxRequest(){
			if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) 
				&& strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ){
				return TRUE;
			}
			if(	!empty($_SERVER['HTTP_X_REQUESTED_WITH']) 
				&& strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
				return TRUE;
			}
			return FALSE;
		}
		
		function _prepareDependencies(){
			Template::setMainTemplateVar('layoutID', 'main_pages');
			Template::includeDependentCss("/min/f=css/Thickbox.css");
			Template::includeDependentJs("/min/g=DashboardMainJs");
		}
		
		function _render(){
			$this->_checkPrivileges(TRUE);
			$this->_defineCommonAttributes();
			$this->_prepareDependencies();
			
			$this->_updateFeatureNoticeVisibility();
			
			$mContent = $this->file_factory->getClass('ViewTravelerDashboard');
			$mContent->setParams($this->mParams);
			$mContent->setTraveler($this->mTraveler);
			
			$this->mTemplate->set("content",		$mContent);
			$this->mTemplate->set("subNavigation",	$this->mSubNavigation);
			$this->mTemplate->set("contextInfo",	$this->mContextInfo);
			$this->mTemplate->out("tpl.ViewTravelerDashboard.php");
		}
		
		function _fetchFeeds(){
			if( !$this->_isAjaxRequest() ){
				header("Location: /travelerdashboard.php");
			}
			$this->_checkPrivileges(TRUE);

			$feeds = array();
			$startFeedID = 0;
			$endFeedID = 0;
			
			$isInCobrand = FALSE;
			if( SiteContext::getInstance()->isInCobrand() ){
				$file_factory = FileFactory::getInstance();
				$file_factory->registerClass("GroupFactory");
				$group = $file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array(SiteContext::getInstance()->getGroupID()));
				$cobrandGroup = $group[0];
				$params = array("traveler"=>$this->mTraveler,"cobrandGroup"=>$cobrandGroup, "mode"=>$this->mParams["mode"]);
				$isInCobrand = TRUE;
			}else{
				$params = array("traveler"=>$this->mTraveler, "mode"=>$this->mParams["mode"]);
			}
			
			if( !is_null($this->mParams["limitID"]) ){
				$params = array_merge($params,array("limitFeedID"=>$this->mParams["limitID"]));
			}
			$activity_feeds = ActivityFeed::getTravelerNetworkFeeds($params);
			
			$feed_adapters = array();
			foreach($activity_feeds AS $feed){
				$tempFeed = ActivityFeedAdapterFactory::create($feed);
				if( $isInCobrand ){
					$travelID = 0;
					if( $feed->getFeedSection() == ActivityFeed::JOURNAL ){
						$travelID = $feed->getSectionID();
					}else if( $feed->getFeedSection() == ActivityFeed::JOURNALENTRY ){
						$travelID = $feed->getSectionRootID();
					}else if( ($feed->getFeedSection() == ActivityFeed::ENTRYVIDEO
						|| $feed->getFeedSection() == ActivityFeed::ENTRYPHOTO) 
						&& !is_null($tempFeed->getJournalEntry()) ){
						$travelID = $tempFeed->getJournalEntry()->getTravelID();
					}
					if( 0 < $travelID ){
						$status = $cobrandGroup->getGroupJournalStatus($travelID);
						if( !$status["approved"] ){
							$tempFeed->setItemExists(FALSE);
						}
					}
					if( !is_null($tempFeed->getGroupOwner()) ){
						if( $tempFeed->getGroupOwner()->getGroupID() != $cobrandGroup->getGroupID()
							&& $tempFeed->getGroupOwner()->getParentID() != $cobrandGroup->getGroupID() ){
							$tempFeed->setItemExists(FALSE);
						}
					}
				}
				$tempFeed->setTimezone(intval($this->mParams["timezone"]));
				$tempFeed->hideActionLinks();
				$feed_adapters[] = $tempFeed;
			}

			$tpl = new Template();
			$tpl->useMainTemplate(FALSE);
			$tpl->set("feeds",$feed_adapters);
			$tpl->set("previousFeeds", "PREVIOUS" == strtoupper(trim($this->mParams["mode"])) ? TRUE : FALSE );						
			$tpl->out('travellog/views/traveler_dashboard/tpl.ViewTravelerDashboardNetworkFeedsList.php');
		}
		
		function _loadSurveyList(){
			$this->_checkPrivileges(TRUE);
			
			$this->file_factory->registerClass('GroupFactory');
			
			if( SiteContext::getInstance()->isInCobrand() ){
				$isInCobrand = TRUE;
				$groupsTemp = $this->file_factory->invokeStaticClass('GroupFactory', 'instance')->create(array(SiteContext::getInstance()->getGroupID()));
				$cobrandGroup = $groupsTemp[0];
				$groupsJoined = array_merge(array($cobrandGroup),$this->mTraveler->getTravelerCBSubGroups($cobrandGroup));
			}else{
				$groupsJoined = $this->mTraveler->getGroups();
			}
			
			$surveys = array();
			$groups = array();
			foreach($groupsJoined AS $group){
				$tempSurveys = SurveyForm::getUnparticipatedSurveyFormsOfGroupByTraveler($group->getGroupID(),$this->mTraveler->getTravelerID());
				foreach($tempSurveys AS $temp){
					$surveys[] = $temp;
					$groups[] = $group;
				}
			}
			
			$tpl = new Template();
			$tpl->useMainTemplate(FALSE);
			$tpl->set("surveys",$surveys);
			$tpl->set("groups",$groups);						
			$tpl->out('travellog/views/traveler_dashboard/tpl.ViewTravelerDashboardSurveyList.php');
		}
		
		private function _updateFeatureNoticeVisibility(){
			if( isset($_POST["ADVISOR_ACCOUNT_NOTICE"]) ){
				$this->file_factory->registerClass('FeatureChangeNoticeHelperFactory',array("path"=>"travellog/factory/"));
				$noticeHelper = $this->file_factory->invokeStaticClass ('FeatureChangeNoticeHelperFactory','instance')->create("ADVISOR_ACCOUNT");
				$noticeHelper->setFeatureChangeNoticeHidden($this->mTraveler->getTravelerID());
			}else if( isset($_POST["DASHBOARD_FEATURE_NOTICE"]) ){
				$this->file_factory->registerClass('FeatureChangeNoticeHelperFactory',array("path"=>"travellog/factory/"));
				$noticeHelper = $this->file_factory->invokeStaticClass ('FeatureChangeNoticeHelperFactory','instance')->create("DASHBOARD_FEATURE");
				$noticeHelper->setFeatureChangeNoticeHidden($this->mTraveler->getTravelerID());
			}
		}
	}