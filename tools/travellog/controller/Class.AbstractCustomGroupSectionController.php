<?php

	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/controller/Class.IController.php");
	require_once("travellog/factory/Class.FileFactory.php");
	require_once("Class.dbHandler.php");
	require_once("Class.Paging.php");
	
	abstract class AbstractCustomGroupSectionController implements iController{
		
		/**
		 * constants of sort type
		 */
		const CUSTOM         = 1;
		const ALPHABETICAL   = 2;
		const RECENT_CREATED = 3;
		
		protected 

		$obj_session  = NULL,

		$file_factory = NULL,
		
		$loggedUser  = NULL,

		$isLogged	  = false,
		
		$mNumRowsRetrieve = 6,

		$isMemberLogged = false,

		$isAdminLogged = false,

		$sGroupsCnt_Active = 0,   // count for active subgroups 
		
		$sGroupsCnt_InActive = 0, // count for inactive subgroups
		
		$sGroupsCnt = 0,          // count for all subgroups that are not is custom groups section

		$group 			= NULL,
		
		$subNav		=	NULL,
		
		$backLink	=	NULL,
				
		$tpl		=	NULL;
	
		function __construct(){
			$this->obj_session = SessionManager::getInstance();
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template',array('path'=>''));
		
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('Condition');
			$this->file_factory->registerClass('FilterCriteria2');
			$this->file_factory->registerClass('FeaturedSubGroup');
			$this->file_factory->registerClass('FeaturedCustomSubGroup');
			
			$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerClass('BackLink' , array('path' => 'travellog/model/navigator'));			
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('ViewSubGroupsPanel', array('path' => 'travellog/views'));
			$this->file_factory->registerClass('ViewFeaturedSubGroups', array('path' => 'travellog/views'));
			$this->file_factory->registerClass('ViewCustomGroupSection', array('path' => 'travellog/views'));
			
			$this->file_factory->registerTemplate('LayoutMain');
			
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
			
		}
		
		/**
		 * do the action or serves as the entry point of this controller
		 */
		function performAction(){
	
			$params = array();
			$params['mode'] = ( isset($_GET['mode']))? $_GET['mode'] : 'view';
			$params['mode'] = ( isset($_POST['mode']))? $_POST['mode'] : $params['mode'];
			$params['gID']  = ( isset($_REQUEST['gID']))? $_REQUEST['gID'] : 0 ;
	
			$params['txtGrpName']  = ( isset($_REQUEST['txtGrpName']))? $_REQUEST['txtGrpName'] : '';
			
			$params['action']  = ( isset($_GET['action']))? $_GET['action'] : 'ViewList';
			$params['action']  = ( isset($_POST['action']))? $_POST['action'] : $params['action'];
			
			$params['tabAction'] = (isset($_GET['tabAction'])) ? $_GET['tabAction'] : 'showActive';
			$params['tabAction'] = (isset($_POST['tabAction'])) ? $_POST['tabAction'] : $params['tabAction'];
			
			$params['chkSubGroupID']  = ( isset($_POST['chkSubGroupID']))? $_POST['chkSubGroupID'] : 0 ;
			$params['chkSubGroupAccess']  = ( isset($_POST['chkSubGroupAccess']))? $_POST['chkSubGroupAccess'] : 0 ;
			
			$params['sgID']  = ( isset($_GET['sgID']))? $_GET['sgID'] : 0 ;
		
			$params['txtSectionLabel']  = ( isset($_POST['txtSectionLabel']))? $_POST['txtSectionLabel'] : '' ;
			
			$params['radSort']  = ( isset($_POST['radSort']))? $_POST['radSort'] : 0 ;
		
			$params['urlStr'] = "gID=".$params['gID']."&mode=".$params['mode']."&tabAction=".$params['tabAction'];
			
			$params['message'] = ($this->obj_session->get('custompopup_message')) ? $this->obj_session->get('custompopup_message') : NULL;
			$this->obj_session->unsetVar('custompopup_message');
			
			$params['curPage'] = (isset($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
			
			$this->initializeActionCommonAttributes($params);
			
			switch ($params['action']) {
				case 'addtosection' :
					$this->_addToCustomSection($params);
					break;
				case 'removefromsection':
					$this->_removeFromCustomSection($params['sgID'],$params);
					break;
				case 'setFeatured':
					$this->_setFeaturedGroup($params);
					break;
				case 'unsetFeatured':
					$this->_unsetFeaturedGroup($params);
					break;
				case 'sortgroups':
					$this->_sortGroups($params);
					break;
				case 'save':
					$this->_saveCustomGroup($params['txtSectionLabel'],true);
					break;
			}
		  
			switch ($params['mode']) {	
				case 'add' : 	
				case 'edit':
					$this->_viewCustomGroupForm($params);
					break;
				case 'arrange':
					$this->_viewArrangeSubGroups($params);
					break;
				case 'list':
				  $this->_viewUnDetailedSubGroups($params);
				  break;
				default:
				  // check if the current group have a custom group and the current logged in is the admin.
			    // if not automatically add a new one with the default name as Projects and Programs
			   if ($this->isAdminLogged AND 0 == $this->group->getCustomGroupSection()->getCustomGroupSectionID()) {
			     $this->_saveCustomGroup("Projects and Programs");
			   }
				 $this->_viewSubGroups($params);
			}
		}
		
		/**
		 * Initializes the common attributes of this class based on 
		 * the request data(data from $_REQUEST, $_GET, $_POST and $_SESSION) that are used
		 * in the action functions.
		 * 
		 * @param array $_params the request data
		 * @return void
		 */
		function initializeActionCommonAttributes(&$_params){
			$this->loggedUser = $this->file_factory->getClass('Traveler', array($this->obj_session->get('travelerID')));
			
			$mGroup = $this->file_factory->invokeStaticClass('GroupFactory','instance')->create(array($_params['gID']));
			$this->group = $mGroup[0];
			
			$this->customGroupSection = $this->group->getCustomGroupSection();
				
			$this->isLogged	=	$this->obj_session->get('travelerID') > 0 ? true : false ;
			$this->isMemberLogged	=	$this->group->isMember($this->loggedUser);
			$this->isAdminLogged =	$this->group->getAdministratorID() == $this->obj_session->get('travelerID') || $this->group->isStaff($this->obj_session->get('travelerID')) ? true : false;
			
		}
		
		/**
		 * Initializes the common attributes of this class based on
		 * the request data(data from $_REQUEST, $_GET, $_POST and $_SESSION) that
		 * are used in the view mode functions.
		 * 
		 * @param array $_params
		 * @return void
		 */
		function initializeViewCommonAttributes(&$params){
			// retrieve the subgroups to be shown with their ranking
			$this->curPage = $params['curPage'];
			
			switch ($params['mode']) {
				case 'list':
          // limit
          $limit = null;
          // end of limit
          $params['tabAction'] = null;
          
          $this->sGroupsCnt = $this->customGroupSection->getSubGroupsBySectionCount(null, $params['txtGrpName']);
				  break;
				case 'arrange':
				default:
				  // limit
          $offset = (($this->curPage-1)*$this->mNumRowsRetrieve);
          $limit = new RowsLimit($this->mNumRowsRetrieve, $offset);
          // end of limit
          
          // count
          $this->sGroupsCnt_Active   = $this->customGroupSection->getSubGroupsBySectionCount('showActive', $params['txtGrpName']);
			    $this->sGroupsCnt_InActive = $this->customGroupSection->getSubGroupsBySectionCount('showInActive', $params['txtGrpName']);
          $this->sGroupsCnt = ($this->sGroupsCnt_Active + $this->sGroupsCnt_InActive);
          // end of count
          
          $params['tabAction'] = ("showActive" === $params['tabAction'] AND 0 == $this->sGroupsCnt_Active AND 0 < $this->sGroupsCnt_InActive) ? "showInActive" : $params['tabAction'];
			    $params['tabAction'] = ("showInActive" === $params['tabAction'] AND 0 == $this->sGroupsCnt_InActive) ? "showActive" : $params['tabAction'];
			    $params['tabAction'] = ("showActive" === $params['tabAction'] AND "arrange" == $params['mode'] AND 1 >= $this->sGroupsCnt_Active AND 2 <= $this->sGroupsCnt_InActive) ? "showInActive" : $params['tabAction'];	
			}
					
			$this->sGroupList = $this->customGroupSection->getSubGroupsBySection($params['tabAction'], $params['txtGrpName'], $limit);
			// end of subgroups retrieval
			
			$this->tpl = $this->file_factory->getClass('Template');
		  $this->tpl->set_path("travellog/views/");
			
			$this->subNav = $this->file_factory->getClass('SubNavigation');
			$this->subNav->setContext('GROUP');
			$this->subNav->setContextID($params['gID']);
			$this->subNav->setLinkToHighlight('CUSTOM_GROUP');
				
			(GROUP::ADMIN == $this->group->getDiscriminator() ) ? $this->subNav->setGroupType('ADMIN_GROUP') : $this->subNav->setGroupType('FUN_GROUP');	
			
			$handler = new dbHandler();
			$params['txtGrpName'] = stripslashes(preg_replace("/^'|'$/","",$handler->makeSqlSafeString($params['txtGrpName'])));
			$params['txtGrpName'] = htmlentities($params['txtGrpName'],ENT_QUOTES);
		}
		
		/**
		 * Checks whether to allow the current user for an admin privilege function. If the user
		 * is not allowed it is redirected to view mode of Projects and Programs.
		 * 
		 * @param boolean $_privilege if false redirect to customgroup ordinary view mode and no redirection otherwise (default:false)
		 * @return void
		 */
		function _allowAdminPrivileges(){
			if (!$this->isAdminLogged){
				header("location:customgroupsection.php?gID=".$this->group->getGroupID());
				exit;
			}
		}
		
		/**
		 * Add the list of subgroups to the custom group section. This functionality
		 * is limited to admin account only.
		 * 
		 * @param array $_params the request data
		 * @return void
		 */
		function _addToCustomSection($_params){
			
			$this->_allowAdminPrivileges();
		
			// if  default rank settings or custom settings, append group at bottom
			if (1 >= $this->customGroupSection->getRankSettings()) { 
				foreach($_params['chkSubGroupID'] as $each)
					$this->customGroupSection->addSubGroupToSectionWithRank($each,$_params['chkSubGroupAccess'][$each]);
			}
			// else, add subgroup then auto sort as defined in rank settings
			else {
				foreach($_params['chkSubGroupID'] as $each)
					$this->customGroupSection->addSubGroupToSectionWithNoRank($each);
				//perform auto sort
				CustomizedGroupSection::autoSortGroups($_params['gID'], $this->customGroupSection->getRankSettings());
			}
		
			// for groups moved to custom section, remove any featured groups in the groups box, if any
			foreach($_params['chkSubGroupID'] as $each) {
				$featuredSubGroup = $this->file_factory->getClass('FeaturedSubGroup' , array($each));
				$featuredSubGroup->unsetSubGroupFeatured();
			}
			
			$this->obj_session->set('custompopup_message', "You have successfully added the subgroups to <strong>".$this->customGroupSection->getSectionLabel()."</strong>.");
			
			header("Location: /customgroupsection.php?gID=".$_params['gID'].'&mode='.$_params['mode']);
		}
		
		/**
		 * Removed the given subgroup to the customgroup list.
		 * 
		 * @param integer $sGID the ID of the subgroup to be removed
		 * @param array $_params the request data
		 * @return void 
		 */
		function _removeFromCustomSection($sGID, $_params){
			$this->_allowAdminPrivileges();
			$this->customGroupSection->removeSubGroupFromSection($sGID);
			
			$mGroup = $this->file_factory->invokeStaticClass('GroupFactory','instance')->create(array($sGID));
			$this->subgroup = $mGroup[0];
			
			$this->obj_session->set('custompopup_message', "You have successfully removed the subgroup <strong>".$this->subgroup->getName()."</strong> from <strong>".$this->customGroupSection->getSectionLabel()."</strong>.");
			
			header("Location: /customgroupsection.php?mode=".$_params['mode']."&gID=".$_params['gID']."&tabAction=".$_params['tabAction']."&txtGrpName=".$_params['txtGrpName']);
		}
		
		/**
		 * Changes the label of the custom group section with the given label.
		 * This functionality is limited only to admin privilege account.
		 * 
		 * @param string $new_label
		 * return void
		 */
		function _saveCustomGroup($new_label, $show_message=false){
			$this->_allowAdminPrivileges($this->isAdminLogged || isset($this->customGroupSection) ); // if not logged in, redirect
		  
		  $old_label = $this->group->getCustomGroupSection()->getSectionLabel();
		  
			$customGroupSection = $this->group->getCustomGroupSection();
			$customGroupSection->setSectionLabel($new_label);
			$customGroupSection->setOwnerGroupID($this->group->getGroupID());
			$customGroupSection->save();
			
			if ($show_message) {
			  $this->obj_session->set('custompopup_message', "You have successfully modified the name of this custom group from <strong>$old_label</strong> to <strong>$new_label</strong>.");
			}
			
			header("Location: /customgroupsection.php?gID=".$this->group->getGroupID());
		}
		
		/**
		 * Features the given subgroup(sgID). Functionality is exclusive for admin
		 * account only.
		 * 
		 * @param array $_params the request data or the data in $_SESSION, $_POST and $_GET
		 * @return void
		 */
		function _setFeaturedGroup($_params){
			$this->_allowAdminPrivileges($this->isAdminLogged);
			$featuredCustomSubGroup = $this->file_factory->getClass('FeaturedCustomSubGroup' , array($_params['sgID']));
			$featuredCustomSubGroup->setSubGroupFeatured();
		  
		  $this->obj_session->set('custompopup_message', "You have successfully added the subgroup <strong>".$featuredCustomSubGroup->getName()."</strong> to the list of featured custom subgroups.");
		  
		  header("Location: /customgroupsection.php?mode=".$_params['mode']."&gID=".$_params['gID']."&tabAction=".$_params['tabAction']."&txtGrpName=".$_params['txtGrpName']."&page=".$_params['curPage']);
		}
		
		/**
		 * Unfeatures the given subgroup(sgID). Functionality is exclusive for admin
		 * account only.
		 * 
		 * @param array $_params the request data or the data in $_SESSION, $_POST and $_GET 
		 */
		function _unsetFeaturedGroup($_params){
			$this->_allowAdminPrivileges($this->isAdminLogged);
				
			$featuredCustomSubGroup = $this->file_factory->getClass('FeaturedCustomSubGroup' , array($_params['sgID']));
			$featuredCustomSubGroup->unsetSubGroupFeatured();
			
			$this->obj_session->set('custompopup_message', "You have successfully removed the subgroup <strong>".$featuredCustomSubGroup->getName()."</strong> from the list of featured custom subgroups.");
			
			header("Location: /customgroupsection.php?mode=".$_params['mode']."&gID=".$_params['gID']."&tabAction=".$_params['tabAction']."&txtGrpName=".$_params['txtGrpName']."&page=".$_params['curPage']);
		}
		
		/**
		 * Sorts the group alphabetically, most recent created or changes the view mode to
		 * arrange mode.
		 * 
		 * @param array $_params the request data
		 * 
		 */
		function _sortGroups($_params){
			
			$this->_allowAdminPrivileges($this->isAdminLogged);
			
			switch($_params['radSort']) {
				case AbstractCustomGroupSectionController::CUSTOM :
				  $_params['mode'] = 'arrange';  //for custom sort view arrange mode
				  break;
				case AbstractCustomGroupSectionController::ALPHABETICAL :
				  CustomizedGroupSection::autoSortGroups($_params['gID'], $_params['radSort']);
				  $this->obj_session->set('custompopup_message', "You have successfully sorted subgroups <strong>alphabetically</strong>.");
				  $this->customGroupSection->setRankSettings($_params['radSort']);
		      $this->customGroupSection->saveRankSettings();
				  break;
				case AbstractCustomGroupSectionController::RECENT_CREATED :
				  CustomizedGroupSection::autoSortGroups($_params['gID'], $_params['radSort']);
				  $this->obj_session->set('custompopup_message', "You have successfully sorted subgroups by <strong>most recent created</strong>.");
          $this->customGroupSection->setRankSettings($_params['radSort']);
		      $this->customGroupSection->saveRankSettings();				  
				  break;
				default: // if the sort type is not in the three it maybe a security problem
				  header('location: /index.php');
			}

		  header("Location: /customgroupsection.php?mode=".$_params['mode']."&gID=".$_params['gID']."&tabAction=".$_params['tabAction']);
		}
				
		/**
		 * Fetches the ViewSubGroupsPanel object
		 * 
		 * @param string $action the action in ViewSubGroupsPanel
		 * @param string $mode the mode
		 * @return ViewSubGroupsPanel the ViewSubGroupsPanel object
		 */
		public function fetchViewSubGroupsPanel($mode, $searchKey = "", $action = "showActive"){			
			$sGroupCnt = ("showActive" === $action) ? $this->sGroupsCnt_Active : $this->sGroupsCnt_InActive;
			
			$subGroupsPanel = $this->file_factory->getClass('ViewSubGroupsPanel');
			$subGroupsPanel->setPanel('customGroupSection');
			$subGroupsPanel->setGroup($this->group);
			$subGroupsPanel->setAction($action);
			$subGroupsPanel->setMode($mode);
			$subGroupsPanel->setPanelName($this->customGroupSection->getSectionLabel());
			$subGroupsPanel->setIsAdminLogged($this->isAdminLogged);
			$subGroupsPanel->setSearchKey($searchKey);
			$subGroupsPanel->setNumberOfRowsToRetrieve($this->mNumRowsRetrieve);
		  $subGroupsPanel->setCurrentPage($this->curPage);
			$subGroupsPanel->setSubGroupList($this->sGroupList);
			$subGroupsPanel->setSubGroupCnt($sGroupCnt);
			
			return $subGroupsPanel;	
		}
		
		/**
		 * Fetches the featured subgroups data.
		 * 
		 * @param string $action the action
		 * @param string $mode the mode
		 * @param integer $page
		 * @return string
		 */
		public function fetchFeaturedSubGroups($action, $mode, $page=1){
      $view = $this->file_factory->getClass('ViewFeaturedSubGroups');
      $view->setSubGroupList($this->group->getCustomGroupSection()->getFeaturedSubGroups());
      $view->setMode($mode);
      $view->setParentGroup($this->group);
      $view->setTabAction($action);
      $view->setPage($page);
      return $view->fetchTemplate();
		}

		/**
		 * Renders the arrange mode of custom group section. Limited to admin account only.
		 * 
		 * @param array $_params the request data or the data in $_GET, $_POST and $_SESSION
		 * @return void
		 */
		function _viewArrangeSubGroups($_params){
	    $this->_allowAdminPrivileges();
      
      $this->customGroupSection->setRankSettings(AbstractCustomGroupSectionController::CUSTOM);
		  $this->customGroupSection->saveRankSettings();
	    $this->initializeViewCommonAttributes($_params);
			
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain'))); 
			
			//get subgroups featured in subgroups box with ranking/order
			$this->tplFeatured = $this->fetchFeaturedSubGroups($_params['tabAction'], $_params['mode']);
			$subGroupsPanel = $this->fetchViewSubGroupsPanel('arrange', $_params['txtGrpName'], $_params['tabAction']);
			
			$customGroupSectionPanel = $this->file_factory->getClass('ViewCustomGroupSection');
			$customGroupSectionPanel->setPanel('customGroupSectionContent');
			$customGroupSectionPanel->setGroup($this->group);
			$customGroupSectionPanel->setIsAdminLogged($this->isAdminLogged);
			$customGroupSectionPanel->setSearchKey($_params['txtGrpName']);
			$customGroupSectionPanel->setMode($_params['mode']);
			$customGroupSectionPanel->setActiveCount($this->sGroupsCnt_Active);
			$customGroupSectionPanel->setInActiveCount($this->sGroupsCnt_InActive);
			$customGroupSectionPanel->setTabAction($_params['tabAction']);
			$customGroupSectionPanel->setFeaturedSubGroupsPanel($this->tplFeatured);
			$customGroupSectionPanel->setSubGroupsPanel($subGroupsPanel);
		
			$customGroupSectionContainer = $this->file_factory->getClass('ViewCustomGroupSection');
			$customGroupSectionContainer->setPanel('customGroupSectionContainer');
			$customGroupSectionContainer->setGroup($this->group);
			$customGroupSectionContainer->setIsAdminLogged($this->isAdminLogged);
			$customGroupSectionContainer->setConfirmationMessage($_params['message']);
			$customGroupSectionContainer->setSubNavigation($this->subNav);
			$customGroupSectionContainer->setMode($_params['mode']);
			$customGroupSectionContainer->setCustomGroupSectionPanel($customGroupSectionPanel);
			
			
			$customGroupSectionContainer->render();
	
		}
		
		/**
		 * Renders the normal mode of custom group section or Projects and Programs
		 * 
		 * @param array $_params the request data or the data in $_GET, $_POST and $_SESSION
		 * @return void
		 */
		function _viewSubGroups($_params){
			$this->initializeViewCommonAttributes($_params);
			
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain'))); 
			
			// should proceed only if admin group and not subgroup
			if (!(GROUP::ADMIN == $this->group->getDiscriminator() && !$this->group->isSubGroup() ) ){				
				header("location:group.php?gID=".$_params['gID']);
			}
			
			//get subgroups featured in subgroups box with ranking/order
			$this->tplFeatured = $this->fetchFeaturedSubGroups($_params['tabAction'], $_params['mode'], $_params['curPage']);
						
			$subGroupsPanel = $this->fetchViewSubGroupsPanel('view', $_params['txtGrpName'], $_params['tabAction']);
			
			$customGroupSectionPanel = $this->file_factory->getClass('ViewCustomGroupSection');
			$customGroupSectionPanel->setPanel('customGroupSectionContent');
			$customGroupSectionPanel->setGroup($this->group);
			$customGroupSectionPanel->setSearchKey($_params['txtGrpName']);
			$customGroupSectionPanel->setIsAdminLogged($this->isAdminLogged);
			$customGroupSectionPanel->setTabAction($_params['tabAction']);
			$customGroupSectionPanel->setActiveCount($this->sGroupsCnt_Active);
			$customGroupSectionPanel->setInActiveCount($this->sGroupsCnt_InActive);
			$customGroupSectionPanel->setFeaturedSubGroupsPanel($this->tplFeatured);
			$customGroupSectionPanel->setSubGroupsPanel($subGroupsPanel);
		
			$customGroupSectionContainer = $this->file_factory->getClass('ViewCustomGroupSection');
			$customGroupSectionContainer->setPanel('customGroupSectionContainer');
			$customGroupSectionContainer->setGroup($this->group);
			$customGroupSectionContainer->setIsAdminLogged($this->isAdminLogged);
			$customGroupSectionContainer->setSubNavigation($this->subNav);
			$customGroupSectionContainer->setConfirmationMessage($_params['message']);
			$customGroupSectionContainer->setMode($_params['mode']);
			$customGroupSectionContainer->setCustomGroupSectionPanel($customGroupSectionPanel);
			
			$customGroupSectionContainer->render();
		}
		
		/**
		 * Renders the custom group add/edit form. Functionality is for admin
		 * users only.
		 * 
		 * @param array $_params the request data or the data in $_GET, $_POST and $_SESSION
		 * @return void
		 */
		function _viewCustomGroupForm($_params){
			$this->_allowAdminPrivileges();
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			
			$this->subNav = $this->file_factory->getClass('SubNavigation');
			$this->subNav->setContext('GROUP');
			$this->subNav->setContextID($_params['gID']);
			$this->subNav->setLinkToHighlight('CUSTOM_GROUP');			
			
			$customGroupSectionPanel = $this->file_factory->getClass('ViewCustomGroupSection');
			$customGroupSectionPanel->setPanel('customGroupSectionForm');
			$customGroupSectionPanel->setGroup($this->group);
			$customGroupSectionPanel->setIsAdminLogged($this->isAdminLogged);
		  
			$customGroupSectionContainer = $this->file_factory->getClass('ViewCustomGroupSection');
			$customGroupSectionContainer->setPanel('customGroupSectionContainer');
			$customGroupSectionContainer->setGroup($this->group);
			$customGroupSectionContainer->setIsAdminLogged($this->isAdminLogged);
			$customGroupSectionContainer->setSubNavigation($this->subNav);
			$customGroupSectionContainer->setMode($_params['mode']);
			$customGroupSectionContainer->setCustomGroupSectionPanel($customGroupSectionPanel);
			
			
			$customGroupSectionContainer->render();
			
		}
		
		/**
		 * Renders the list mode view or undetailed mode view for showing the subgroups
		 * in custom group section. This is limited for admin accounts and proworld only.
		 * 
		 * @param array $params the request data
		 * @return void
		 */
    function _viewUnDetailedSubGroups($params) {
			$this->_allowAdminPrivileges();
			$this->initializeViewCommonAttributes($params);
			
			$paging = new Paging($this->sGroupsCnt, $params['curPage'], "txtGrpName=".$params['txtGrpName'], $this->mNumRowsRetrieve);
			
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			
			$is_searching = ("" == trim($params['txtGrpName'])) ? false : true;
			
			$profile_comp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
			$profile_comp->init($this->group->getGroupID());
			
			$allSubGroups = $this->group->getSubGroupsBySection();
			
			$this->tpl->set_path("travellog/views/subgroups/");
			$this->tpl->set("allSubGroups", $allSubGroups);
			$this->tpl->set("subgroups",$this->sGroupList);
			$this->tpl->set('ranksettings',$this->customGroupSection->getRankSettings());
			$this->tpl->set('searchKey', $params['txtGrpName']);
			$this->tpl->set('customGroupSection', $this->customGroupSection);
			$this->tpl->set('all', $this->customGroupSection);
			$this->tpl->set('grpID', $this->group->getGroupID());
			$this->tpl->set('cnt_groups', $this->sGroupsCnt);
			$this->tpl->set('profile', $profile_comp->get_view());
			$this->tpl->set('subNavigation',$this->subNav);
			$this->tpl->set('isAdminLogged',$this->isAdminLogged);
			$this->tpl->set('rows_per_page', $this->mNumRowsRetrieve);
			$this->tpl->set('is_searching', $is_searching);
			$this->tpl->set('success', $params['message']);
			$this->tpl->set('paging', $paging);
			
	    $this->tpl->out('tpl.ViewUnDetailedCustomSubGroupList.php');	    
    }

	}
?>