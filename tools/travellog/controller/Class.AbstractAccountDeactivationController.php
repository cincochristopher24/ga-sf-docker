<?php

	require_once("travellog/factory/Class.FileFactory.php");
 
	class AbstractAccountDeactivationController{
		
		protected $session	= NULL;
		protected $file_factory = NULL;
		protected $config = NULL;
		protected $traveler = NULL;
		
		public function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template', array('path' => ''));
			$this->file_factory->registerClass('SessionManager');
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('Traveler');
			
			$this->file_factory->registerClass("Captcha", array("path" => "captcha/"));
			$this->file_factory->registerClass('ProfileCompFactory', array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
			$this->file_factory->registerTemplate('LayoutMain');

			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			$this->session = $this->file_factory->invokeStaticClass('SessionManager', 'getInstance');
			
			if( $this->isAjaxRequest() ){
				$this->redirect(404);
			}
			
			if( !$this->session->getLogin() ){
				$this->redirect("/login.php?redirect=/account-settings.php");
			}
			
			$this->traveler = $this->file_factory->getClass("Traveler",array($this->session->get("travelerID")));
		}
		
		public function performAction(){
			$action = isset($_POST["action"]) ? $_POST["action"] : "";
			switch($action){
				case "deactivate":
					$this->deactivate();
					break;
				default:
					$this->_default();
					break;
			}
		}
		
		protected function deactivate(){
			$reason = isset($_POST["radReason"]) ? intval($_POST["radReason"]) : NULL;
			$explanation = isset($_POST["txtExplanation"]) ? strip_tags($_POST["txtExplanation"]) : NULL;
			$securityCode = isset($_POST["securityCode"]) ? $_POST["securityCode"] : NULL;
			$encSecurityCode = isset($_POST["encSecCode"]) ? $_POST["encSecCode"] : NULL;
			
			if(!in_array($reason,array(1,2,3,4)) || is_null($explanation)){
				$this->redirect("/account-settings.php");
			}
			if(is_null($securityCode) || is_null($encSecurityCode) || !$this->file_factory->invokeStaticClass("Captcha","validate",array($encSecurityCode,$securityCode))){
				$this->redirect("/account-deactivation.php");
			}
			
			$travelerID = $this->traveler->getTravelerID();

			$serviceProviders = GanetServiceProviderPeer::retrieveAll();
			foreach($serviceProviders As $provider){
				$travelerAccount = GanetTravelerAccountPeer::retrieveTravelerAccountByTravelerIDAndServiceID($travelerID,$provider->getID());
				if($travelerAccount){
					$widgetSetting = NULL;
					if("twitter"==strtolower($provider->getName())){
						$widgetSetting = GanetTwitterSettingPeer::retrieveSettingsByTravelerAccountID($travelerAccount);
					}elseif("facebook"==strtolower($provider->getName())){
						$widgetSetting = GanetFacebookSettingPeer::retrieveSettingsByTravelerAccountID($travelerAccount);
					}
					if($widgetSetting){
						$widgetSetting->delete();
					}
					
					$oauthToken = GanetOAuthTokenPeer::retrieveByTravelerAccount($travelerAccount);
					if($oauthToken){
						$oauthToken->delete();
					}
					
					$travelerAccount->delete();
				}
			}
			
			$deactivatedTraveler = new GanetDeactivatedTraveler();
			$deactivatedTraveler->setTravelerID($travelerID);
			$deactivatedTraveler->setReason($reason);
			$deactivatedTraveler->setExplanation($explanation);
			$deactivatedTraveler->save();
			
			$this->traveler->DeactivateAccount();
			
			$this->sendAccountDeactivationNotification($this->traveler,$deactivatedTraveler);
			
			session_destroy();
			$this->redirect("/");
		}
		
		private function sendAccountDeactivationNotification(Traveler $deactivatedTraveler, GanetDeactivatedTraveler $deactivatedAccount){
			require_once("class.phpmailer.php");
			require_once('travellog/model/Class.SiteContext.php');
			
			$domainConfig 	= SiteContext::getInstance();
			$siteName 		= $domainConfig->getSiteName();
			$siteName		= "GANET" == $siteName ? "GoAbroad Network" : $siteName;
			$siteUrl 		= "http://".$domainConfig->getServerName();
			$travelerName = $deactivatedTraveler->getFullName();
			$signatory		= $siteName == "GoAbroad Network" ? "The GoAbroad Network Team" : $siteName;
			
			
			//MAIL TO TRAVELER
			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->IsHTML(FALSE);
			$mail->Host     = "mail.goabroad.com";

			$mail->From     = "admin@goabroad.net";
			$mail->FromName = "$siteName Admin";

			$mail->AddAddress($deactivatedTraveler->getEmail(),$travelerName);
			$mail->AddBCC("virna.alvero@goabroad.com","Virna Liza Alvero");
			$mail->AddBCC("reynaldo.castellano@goabroad.com","Reynaldo Castellano III");
			//$mail->AddBCC("jul.garcia@goabroad.com","Jul Warren Garcia");

			$mail->Subject = "$siteName Account Deactivation";
			$mail->Body = "Hi $travelerName,".
"

You have deactivated your $siteName account.
If you want to reactivate your account,
please feel free to contact us by clicking on the link below.

$siteUrl/feedback.php

Best Regards,

$signatory
$siteUrl";

			$mail->Send();
			
			//MAIL TO ADMIN
			$mail2 = new PHPMailer();
			$mail2->IsSMTP();
			$mail2->IsHTML(FALSE);
			$mail2->Host     = "mail.goabroad.com";

			$mail2->From     = "admin@goabroad.net";
			$mail2->FromName = "$siteName Admin";
			
			$mail2->AddAddress("admin@goabroad.net","$siteName Admin");
			//$mail2->AddBCC("virna.alvero@goabroad.com","Virna Liza Alvero");
			$mail2->AddBCC("reynaldo.castellano@goabroad.com","Reynaldo Castellano III");
			//$mail2->AddBCC("jul.garcia@goabroad.com","Jul Warren Garcia");
			
			$mail2->Subject = "Deactivated account of $travelerName";
			$mail2->Body = "Hi GoAbroad.net Administrator,".
"

Please be informed that $travelerName has deactivated his/her $siteName account".

			$reason = "";
			switch($deactivatedAccount->getReason()){
				case 1 :	$mail2->Body .= "
because he/she didn't find the site useful.";
							break;
				case 2 :	$mail2->Body .= "
because he/she doesn't know how to use the site.";
							break;
				case 3 :	$mail2->Body .= "
because he/she is switching to another travel networking site.";
							break;
				default:	$mail2->Body .= "
for other reasons not included in the deactivation form options.";
			}
			
			if("" != trim($deactivatedAccount->getExplanation())){
				$mail2->Body .= "
				
$travelerName further explained:
----------
".stripslashes(trim($deactivatedAccount->getExplanation()))."
----------";
			}

			$mail2->Body .= "

Many Thanks,

$signatory
$siteUrl";
	
			$mail2->Send();
			
			//echo "<textarea rows='20' cols='150'>".$mail->Body."</textarea>";
			//echo "<textarea rows='20' cols='150'>".$mail2->Body."</textarea>";
			//exit;
		}
		
		protected function _default(){
			$travelerID = $this->session->get("travelerID");
			
			$subNavigation = $this->file_factory->getClass('SubNavigation');
		 	$subNavigation->setContext('TRAVELER');	
		 	$subNavigation->setContextID($travelerID);
		 	$subNavigation->setLinkToHighlight('MY_PROFILE');

			$profile_comp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$TRAVELER_CONTEXT );
			$profile_comp->init($travelerID, AbstractProfileCompController::$EDIT_ACCOUNT_SETTINGS);

			$template = $this->file_factory->getClass("Template");
		
			Template::setMainTemplate($this->file_factory->getTemplate('LayoutMain'));
			Template::setMainTemplateVar('page_location', 'My Passport');
			Template::includeDependentJs('/min/f=js/prototype.js', array('top'=>true));
			Template::includeDependentJs('js/tools/common.js');
			Template::includeDependentJs('js/utils/json.js');
			Template::includeDependentJs('captcha/captcha.js');
			Template::includeDependentJs('/js/interactive.form.js');
		
			$security = $this->file_factory->invokeStaticClass("Captcha","createImage");
		
			$template->set('profile', $profile_comp->get_view());
			$template->set("subNavigation",$subNavigation);
			$template->set("config",$this->config);
			$template->set("traveler",$this->traveler);
			$template->set("secSrc",$security["src"]);
			$template->set("secCode",$security["code"]);
			$template->out("travellog/views/tpl.AccountDeactivation.php");
		}
		
		protected function isAjaxRequest(){
			if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) 
				&& strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ){
				return TRUE;
			}
			if(	!empty($_SERVER['HTTP_X_REQUESTED_WITH']) 
				&& strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
				return TRUE;
			}
			return FALSE;
		}
		
		public function redirect($location){
			if( 404 == $location ){
				header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
			}else{
				header("location: ".$location);
			}
			exit;
		}
	}