<?php
	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/factory/Class.FileFactory.php");
	require_once("class.phpmailer.php");
	
	abstract class AbstractInviteController
	{
		protected $file_factory 	= NULL;
		protected $obj_session  	= NULL;
		protected $mLoggedTraveler 	= NULL;
		
		public function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/views'));
			$this->obj_session  = SessionManager::getInstance();
			$this->file_factory->registerClass('ToolMan', array('path'=>''));
			$this->file_factory->registerClass('Template',array('path'=>''));
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('Traveler');
						
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
		}
		
		
		public function performAction(){
			$travelerID = $this->obj_session->get('travelerID');
			if(!$travelerID){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/register.php'));
			}
			try{
				$this->mLoggedTraveler = $this->file_factory->getClass('Traveler',array($travelerID));
			}
			catch(exception $e){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			//initiliaze th method to be used
			$localMethod = setLocalVar("method,hidMethod","compose");
			switch($localMethod){
				case "compose":	
					$this->compose();
					break;
				case "send":
					$this->send();
					break;
				default:
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			
		}
		
		public function compose(){
			$subject = "Here's my newest travel discovery!";
			$subjectComm = "";
			$message = 	"Hi!\n\n" .
						"I just came across this great site and I like it so much I insist you try it too! ". 
						"It's the GoAbroad Network, the newest social networking site for travelers. " .
						"You don't have to be a qualified globe-trotter though, this is for every traveler, " .
						"travel enthusiast and aspiring traveler out there. Sign up for your own account and " .
						"you can check out my travels. We can also share travel experiences, tips and photos, " .
						"and who knows, maybe we can even plan a trip together! \n\n" .
						"You can view my GoAbroad.net profile at: http://".$_SERVER['HTTP_HOST']."/".$this->mLoggedTraveler->getUserName()."\n\n" .
						"See you in the GoAbroad Network! \n\n" .
						"http://".$_SERVER['HTTP_HOST']."\n\n";
			$profile = $this->mLoggedTraveler->getTravelerProfile();
			$message .= "-".(strlen($profile->getfirstname()))?$profile->getfirstname().' '.$profile->getlastname():$profile->getusername();
			$address = "";
			$this->showTemplate(array(
				'method'	=> 'compose',
				'subject' 	=> $subject,
				'message' 	=> $message,
				'address' 	=> $address
			));
		}
		
		public function send(){
			$subject = setLocalVar("txtSubject","");
			$message = setLocalVar("txaMessage","");
			$address = setLocalVar("txaAddress","");
			//validate
			
			$vars = array();
			
			$errCodes = array();
			if(!strlen($subject)) $errCodes[] = 0;
			if(!strlen($message)) $errCodes[] = 1;
			if(!strlen($address)) $errCodes[] = 2;				
			if(count($errCodes)){		
				$vars['subject'] = $subject;
				$vars['message'] = $message;
				$vars['address'] = $address;
				$vars['errCodes'] = $errCodes;
				$vars['method'] = 'compose';
			}
			else{				
				$profile = $this->mLoggedTraveler->getTravelerProfile();
				$succ = true;
				try{
					$mail = new PHPMailer();					
				}
				catch(exception $e){
					$succ=false;
				}	
				$mail->IsSMTP();			
				//$mail->IsQmail();
				$mail->IsHTML(false);
				$mail->From = $profile->getEmail();
				$fullname = $profile->getFirstName().' '.$profile->getLastName();
				$mail->FromName = $fullname;					
				$mail->Subject  = $subject;			
				$mail->Body = $message;
				
				//loop through the addresses
				$arAddr = explode(",",$address);
				$trimmedAddresses = array();
				foreach($arAddr as $addr){					
					$trimmedAddr = trim($addr);
					if(strlen($trimmedAddr)){
						$trimmedAddresses[] = $trimmedAddr;
						$mail->AddAddress($trimmedAddr);
						try{
							if(!$mail->Send()){												
								$succ = false;
							}
						}
						catch(exception $e){						
							$succ = false;
						}
						$mail->ClearAddresses();
					}
				}
				if($succ){
					if(1==count($arAddr)){
						$msgSummary = "An invite has been sent to the following email address: ".implode(',',$trimmedAddresses);
					}
					else{
						$msgSummary = "Invites have been sent to the following email addresses: ".implode(',',$trimmedAddresses);
					}
				}
				else{
					$msgSummary = "Sorry your invites was not sent this time. Please try again. ";
				}
				$vars['msgSummary'] = $msgSummary;
				$vars['method'] = 'showSummary';
			}
			$this->showTemplate($vars);
		}
		
		public function showTemplate($vars){
			//initialize the template
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			$tplInvite = $this->file_factory->getClass('Template');
			$tplInvite->set_path("travellog/views/");
			$subNavigation = $this->file_factory->getClass('SubNavigation');		
			$subNavigation->setContext('TRAVELER');
			$subNavigation->setContextID($this->mLoggedTraveler->getTravelerID());
			
			$tplInvite->set("subNavigation",$subNavigation);
			$tplInvite->set("trvID",$this->mLoggedTraveler->getTravelerID());
			foreach($vars as $key => $val){
				$tplInvite->set($key,$val);
			}
			$tplInvite->out("Tpl.Invite.php");
		}
		
		private  function setLocalVar($listVarNames,$defVal,$varType="STRING"){
			//explode the varname		
			$arVarNames = explode(",",$listVarNames);
			foreach($arVarNames as $varName){
				if(isset($_GET[$varName]) || isset($_POST[$varName])){			
					$localVar = (isset($_GET[$varName]))?$_GET[$varName]:$_POST[$varName];			
					switch($varType){
						case "NUMERIC":										
							if(is_numeric($localVar)) return $localVar;
							break;
						default://STRING
							return $localVar;
					}
				}
			}
			return $defVal;
		}
	
	}	
?>