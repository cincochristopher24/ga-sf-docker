<?php
require_once('travellog/controller/Class.AbstractTravelerController.php');
require_once('travellog/model/Class.LocationFactory.php');
require_once("travellog/model/Class.FilterCriteria2.php");
require_once("travellog/model/Class.Condition.php");
class HomeTravelerController extends AbstractTravelerController{
	private $locationID = 0;
	
	function viewContents(){
		$this->viewMain();
		$inc_tpl       = new Template;
		$factory       = LocationFactory::instance();
		$obj_country   = $factory->create($this->locationID);
		$obj_col       = $obj_country->getCities();
		$filter        = NULL;
		$list          = array();
		$obj_travelers = array();
		$query_string  = 'action=home&locationID='.$this->locationID;
		
		if (count($obj_col)){
			foreach($obj_col as $obj){
				$list[] = $obj->getLocationID();		
			}
			$list[] = $this->locationID;
			$cond   = new Condition;

			$cond->setAttributeName("htlocationID");
			$cond->setOperation(FilterOp::$IN);
			$cond->setValue(implode(',',$list));
			
			$filter = new FilterCriteria2();
			$filter->addBooleanOp('AND');
			$filter->addCondition($cond);
		}	
		
		$obj_travelers = Traveler::getAllTravelers($filter);
		$obj_paging    = new Paging        ( count($obj_travelers), $this->page,$query_string, $this->rows );
		$obj_iterator  = new ObjectIterator( $obj_travelers, $obj_paging                                   );
		
		$this->main_tpl->set( 'obj_paging'   , $obj_paging       );
		$this->main_tpl->set( 'obj_iterator' , $obj_iterator     );
		$this->main_tpl->set( 'selected'     , 1                 );
		$this->main_tpl->set( 'travelerID'   , $this->travelerID );
		$this->main_tpl->set( 'isLogin'      , $this->IsLogin    );
		$this->main_tpl->set( 'isAdvisor'    , $this->isAdvisor  );
		
		$this->main_tpl->set( 'mainContent', $this->main_tpl->fetch("travellog/views/tpl.ViewTravelersList.php") );
		$this->main_tpl->out("travellog/views/tpl.Travelers.php");		
	}
	
	function setLocationID($locationID){ $this->locationID = $locationID; }
}
?>
