<?php
	/*
	 * Class.AbstractDestinationController.php
	 * Created on Nov 28, 2007
	 * created by marc
	 */
	
	require_once("travellog/controller/Class.IController.php");
	require_once("travellog/factory/Class.FileFactory.php");
	require_once("Class.Constants.php");
	
	class AbstractDestinationController implements IController{
		
		static $file_factory = NULL;
		static $destination_helper = NULL;
		
		function __construct(){
			
			$this->file_factory = FileFactory::getInstance();
			
			$this->file_factory->registerClass("Country", array("path" => "travellog/model"));
			$this->file_factory->registerClass("TravelLog", array("path" => "travellog/model"));
			$this->file_factory->registerClass("FeaturedInLocation", array("path" => "travellog/model"));
			$this->file_factory->registerClass("HelpText", array("path" => "travellog/model/"));
			$this->file_factory->registerClass("LocationMarker", array("path" => "travellog/model"));
			$this->file_factory->registerClass("SessionManager", array("path" => "travellog/model"));
			$this->file_factory->registerClass("DestinationHelper", array("path" => "travellog/helper"));
			$this->file_factory->registerClass("DestinationFcd", array("path" => "travellog/facade"));
			$this->file_factory->registerClass("MainLayoutView", array("path" => "travellog/views/"));
			$this->file_factory->registerClass("Template", array("path" => ""));
			$this->file_factory->registerClass("Connection", array("path" => ""));
			$this->file_factory->registerClass("Recordset", array("path" => ""));
			$this->file_factory->registerClass("Map");
			$this->file_factory->registerClass("MapFrame", array("file" => "Map", "class" => "MapFrame"));
			
			
			$this->destination_helper = $this->file_factory->invokeStaticClass("DestinationHelper","getInstance");
			
			// views
			$this->file_factory->registerTemplate("LayoutMain");
			$this->file_factory->setPath("CSS", "/css/");
			$this->file_factory->setPath("HEADER_FOOTER", "travellog/views/");
			
		}
		
		function performAction(){
			
			switch($this->destination_helper->getAction()){
				case constants::VIEW_COUNTRY:
					return $this->viewCountry();
				default:
					return $this->viewAllCountries();	
			}
		}
		
		function retrieveAllCountries(){
			
			/**
			 * save all countries with travel logs in an array
			 */			
			
			if( !$arrCountry = $this->file_factory->invokeStaticClass("TravelLog","getCountriesWithLogs") )
			 	$arrCountry = array();
			 	
			$arrCountryCol = array();
			$arrCountryAllCol = array();
			$arrLocationMarker = array();
		
			/**
			* divide all countries into 4 rows, each row contained in an array
			* all rows must be stored in an array
			*/ 
			$maxRowElem = ceil(count($arrCountry) / 4);
			$rowCtr = 0;
			
			foreach ( $arrCountry as $indCountry){
				if ($rowCtr < $maxRowElem)
					$rowCtr++;
				else{
					$rowCtr = 1;
					$arrCountryAllCol[] = $arrCountryCol;
					$arrCountryCol = array();
				}
				$nTempLocationMarker = $this->file_factory->getClass("LocationMarker");
				$nTempLocationMarker->setLocation($indCountry);
				$nTempLocationMarker->setLink('destination.php?view&countryID=' . $indCountry->getCountryID());
				$arrLocationMarker[] = $nTempLocationMarker;
				$arrCountryCol[] = $indCountry;
			}
			$arrCountryAllCol[] = $arrCountryCol;
			
			/**
			* create map frame and map
			*/
			$mFrame = $this->file_factory->getClass("MapFrame");
			$mFrame->setWidth('auto'); 
			$mFrame->setHeight('300');
			$mFrame->setConnectMarkers(false);
			$mFrame->setMapType('world');
			
			$map1 = $this->file_factory->getClass("Map");
			
			return array(
				'countryArr'		=>	$arrCountryAllCol,
				'map'				=>	$map1,
				'mFrame'			=>	$mFrame,
				'arrLocationMarker'	=>	$arrLocationMarker,
				"helpText"			=>	$this->file_factory->getClass("HelpText")
			);
			
		}
		
		function viewAllCountries(){
			
			$allCountryInfo = $this->retrieveAllCountries();
			
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			
			Template::setMainTemplateVar('layoutID', 'main_pages');
			Template::setMainTemplateVar('page_location', 'Destinations');
			Template::includeDependent($allCountryInfo["map"]->getJsToInclude());
			Template::setMainTemplate   ( $this->file_factory->getTemplate('LayoutMain') );
			
			$fcd = $this->file_factory->getClass('DestinationFcd', $allCountryInfo, false);
			$obj_view->setContents( array('contents' => $fcd->retrieveAllCountriesPage()) );
			$obj_view->render();
			
		}
		
		function retrieveCountryInfo(){
			
			/**
			* if countryID is missing redirect to destination.php
			*/
			if ( !$countryID = $this->destination_helper->getCountryID() )
				header('location: destination.php');
			
			$nConnection = $this->file_factory->getClass("Connection");
			$nRecordset = $this->file_factory->getClass("Recordset", array($nConnection));
			
			/** 
			* create an array of locationIDs related to that country
			* locationID of: country,city and region
			*/	
			$arrLocID = array();
				
			$myquery = $nRecordset->Execute("SELECT GoAbroad_Main.tbcountry.locID FROM GoAbroad_Main.tbcountry WHERE GoAbroad_Main.tbcountry.locID <> 0 AND countryID=" . $_GET["countryID"]);
					
			$nLocationID = mysql_fetch_array($myquery);
			if ( count($nLocationID) ){
				$nLocationID = $nLocationID['locID'];
				$arrLocID[] = $nLocationID;
			}
					
			$myquery = $nRecordset->Execute("SELECT GoAbroad_Main.tbcity.locID FROM GoAbroad_Main.tbcity WHERE GoAbroad_Main.tbcity.locID <> 0 AND countryID=" . $_GET["countryID"] . 
				" UNION SELECT locID FROM tblRegion WHERE locID <> 0 AND countryID=" . $_GET["countryID"]);
			
			while ( $row = mysql_fetch_array($myquery)){
				$arrLocID[] = $row['locID'];
			}
			
			/**
			* create object country
			*/
			$my_country = $this->file_factory->getClass("Country", array($countryID));
			
			/**
			* create an array of location for plotting
			*/
			$arrLocationMarker = array();
			$nTempLocationMarker = $this->file_factory->getClass("LocationMarker");
			$nTempLocationMarker->setLocation($my_country);
			$nTempLocationMarker->setLink('destination.php?view&countryID=' . $my_country->getCountryID());
			$arrLocationMarker[] = $nTempLocationMarker;
			
			/**
			* create map frame and map
			*/
			$mFrame = $this->file_factory->getClass("MapFrame");
			$mFrame->setWidth('380');
			$mFrame->setHeight('300');
			$mFrame->setConnectMarkers(false);
			
			$sessionManager = $this->file_factory->invokeStaticClass("SessionManager","getInstance");
			
			return array(
				'country'				=>	$my_country,
				
				'Photos'				=>	$this->file_factory->invokeStaticClass("FeaturedInLocation","getFeaturedPhoto",array($arrLocID, 2)),
				'PhotoCount'			=>	$this->file_factory->invokeStaticClass("FeaturedInLocation","getFeaturedPhotoCount"),
				'PhotoLogs'				=>	$this->file_factory->invokeStaticClass("FeaturedInLocation","getFeatPhotoLog"),
				
				'currentTravelers'		=>	$this->file_factory->invokeStaticClass("FeaturedInLocation","getFeaturedTraveler",array($arrLocID, 3, 1)),
				'TravelerCount'			=>	$this->file_factory->invokeStaticClass("FeaturedInLocation","getFeaturedTravelerCount", array("1")),
				'livingTravelers'		=>	$this->file_factory->invokeStaticClass("FeaturedInLocation","getFeaturedTraveler",array($arrLocID, 3)),
				'TravelersLivingCount'	=>	$this->file_factory->invokeStaticClass("FeaturedInLocation","getFeaturedTravelerCount"),
				
				'map'					=>	$this->file_factory->getClass("Map"),
				'arrLocationMarker'		=>	$arrLocationMarker,
				'mFrame'				=>	$mFrame,
				'title'					=>	$my_country->getName(),
				
				'Travels'				=>	$this->file_factory->invokeStaticClass("FeaturedInLocation","getFeaturedTravel",array($arrLocID, 3)),
				'displayAll'			=>	$this->file_factory->invokeStaticClass("FeaturedInLocation","getFeaturedTravelCount"),
				'displayHeader'			=>	1,
				'displayAllCities'		=>	0,
				
				"travelerID"			=>	$sessionManager->get("travelerID")
				
			);	
		}
		
		function viewCountry(){
			
			$countryInfo = $this->retrieveCountryInfo();
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			
			Template::setMainTemplateVar('page_location', 'Destinations');
			Template::setMainTemplateVar('layoutID', 'page_countinfo');
			Template::includeDependent($countryInfo["map"]->getJsToInclude());
			Template::setMainTemplate   ( $this->file_factory->getTemplate('LayoutMain') );
			
			$fcd = $this->file_factory->getClass('DestinationFcd', $countryInfo, false);
			$obj_view->setContents( array('contents' => $fcd->retrieveCountryPage()) );
			$obj_view->render();
			
		}
		
		static function viewCountryList($countryCol = array() ){
			
			$nContent = array( 
				"countryCol"		=>	$countryCol
			);
			
			require_once("travellog/facade/Class.DestinationFcd.php");
			$fcd = new DestinationFcd($nContent);
			echo $fcd->viewCountryList()->render();
		}
		
	}
	 
?>