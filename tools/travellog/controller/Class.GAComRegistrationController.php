<?php
	/*
	 * Class.GAComRegistrationController.php
	 * Created on Nov 21, 2007
	 * created by marc
	 */
	 
	require_once("travellog/controller/Class.AdvisorRegistrationController.php");
	
	class GAComRegistrationController extends AdvisorRegistrationController{
		
		function viewRegistrationForm($content = array()){
			
			if( !isset($content["ref"]) || !isset($content["gcID"])){
				$clientKeys = $this->register_helper->readClientKeys();
				$content["gcID"] = $clientKeys["gcID"];
				$content["ref"] = $clientKeys["ref"];
			}	
			$clientID = $this->file_factory->invokeStaticClass("AdminGroup","isValidAdmin",array($content["gcID"], $content["ref"]));
			if( $clientID ){
			 	$conn = $this->file_factory->getClass("Connection");
			 	$rs = $this->file_factory->getClass("Recordset",array($conn));
			 	$tmpClient = $this->file_factory->getClass("Client",array($clientID));
		 		$nName = $tmpClient->getInstName();
			 	$myquery = $rs->Execute("SELECT clientID FROM tblGrouptoAdvisor WHERE clientID = $clientID");
			 	if( $row = mysql_fetch_array($myquery) ){
			 		$content["strErrorMessages"] = 'An account has already been created for <b><em><?= $instName; ?></em></b>. You may access your account by <a href="login.php?advisor">logging in</a> to the network.';
			 		$content["redirectPage"] = $this->register_helper->getRedirectPage(constants::HOME_PAGE);
			 	}
			 	else
			 		$content["instname"] = $tmpClient->getInstName();
			 		
			}
			else{
				$content["strErrorMessages"] = 'Permission denied in your request. You will now be redirected to the homepage.';
				$content["redirectPage"] = $this->register_helper->getRedirectPage(constants::REGISTER_PAGE);
			}
			parent::viewRegistrationForm($content);	
		}
		
		function showRegistrationDetails( $advisorData = array() ){
			
			$advisorData = array_merge($advisorData,array("user" => constants::GACOM_CLIENT));
			parent::showRegistrationDetails($advisorData);
				
		}
		
		function saveDetailsToTempTable( &$advisorData = array() ){
			
			$conn = $this->file_factory->getClass("Connection");
			$rs = $this->file_factory->getClass("Recordset",array($conn));
			
			// get clientID if client of GA.com and create group advisor account
			$clientID = $this->file_factory->invokeStaticClass("AdminGroup","isValidAdmin",array($advisorData["gcID"], $advisorData["ref"]));
			 
			if( $advisorData["regID"] )
				$sqlquery = "UPDATE tblTmpRegister SET ";
			else	 
				$sqlquery = "INSERT INTO tblTmpRegister SET ";
			$sqlquery .= "genName = '" . addslashes($advisorData["instname"]) . "',  
					username = '" . addslashes($advisorData["username"]) . "',		 
					password = '" . addslashes($advisorData["password"]) . "',	
					clientID = $clientID , 
					email = '" . addslashes($advisorData["emailAdd"]) . "',
					subscribeNL = ". $advisorData["subscribeNL"];
			if( 0 == $advisorData["regID"] )
				$sqlquery .= " ,regKey = '" . mktime() . "'"; 
			if( $advisorData["regID"])
				$sqlquery .= " WHERE registerID = " . addslashes($advisorData["regID"]) . 
					" AND regKey = '" . addslashes($advisorData["regKey"]) . "'";
			$myquery = $rs->Execute($sqlquery);
			if( 0 == $advisorData["regID"] ){
				$sqlquery = "SELECT registerID, regKey FROM tblTmpRegister WHERE registerID = LAST_INSERT_ID()";
				$myquery = $rs->Execute($sqlquery);
				$row = mysql_fetch_array($myquery);
				$advisorData["regKey"] = $row["regKey"];
				$advisorData["regID"] = $row["registerID"];		
			}
		
			$advisorData["user"] = constants::GACOM_CLIENT;
		}
		
	} 
?>
