<?php
require_once('travellog/controller/Class.AbstractGroupEventController.php');
class GANETGroupEventController extends AbstractGroupEventController{
	
	function performAction(){
		$this->data               = array_merge($_GET, $_POST);
		$this->data['action']     = ( isset($this->data['action']))? $this->data['action'] : 'View';
		$this->data['context']    = ( isset($this->data['context']))? $this->data['context'] : 1;
		$this->data['from_where'] = 'group';    
		switch( $this->data['context'] ){
			case "0":
				$event_controller = $this->file_factory->getClass('ActivityController'); 
				$event_controller->performAction();	 
			break;
			default:
				parent::__applyRules(); 		 		
				parent::performAction();            
		}
		   
	}
}
?>