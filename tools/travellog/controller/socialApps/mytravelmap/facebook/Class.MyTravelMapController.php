<?php
//TODO: refactor
require_once("travellog/model/Class.Travel.php");
require_once("travellog/model/Class.TravelLog.php");
require_once('travellog/helper/Class.EntryHelper.php');

require_once 'travellog/model/socialApps/mytravelmap/facebook/Mvc.php';
require_once 'travellog/model/socialApps/mytravelmap/facebook/Map.php';
require_once 'travellog/model/socialApps/mytravelmap/facebook/Class.FBCountry.php';
require_once 'travellog/model/socialApps/mytravelmap/facebook/Class.FacebookDAO.php';
require_once 'travellog/model/socialApps/mytravelmap/facebook/Class.TravelMapEntry.php';
require_once 'travellog/model/socialApps/mytravelmap/facebook/Class.MyTravelMapDAO.php';
//require_once 'travellog/model/socialApps/mytravelmap/facebook/Class.SingletonConnection.php';
require_once 'travellog/model/socialApps/mytravelmap/facebook/Class.MyTravelMapRepository.php';
require_once 'travellog/model/socialApps/mytravelmap/facebook/Interface.FacebookApplication.php';

class MyTravelMapController extends AbstractController implements FacebookApplication {

  	private $controlVars;
  	private $countryList;
  	private $mapEntries;
  	private $dao;                                          	
  	private $isSynchronized = true;
  	private $userCredentials = array();
  	//variable to avoid the expensive call to update the profile page
  	//in the future update would be automatic, we won't have to call it
  	//explicitly in index.php or the controller;
  	private $suppressProfileUpdate = false; //should be moved to interface?

	const GOABROAD_DOTNET_URL = 'http://www.goabroad.net';
  	const PINS_PATH = "http://www.goabroad.net/facebook/mytravelmap/images/corners/";


  	function __construct($controlVars) {
		// Add user to tbUser if ever user not exists.
		if(!FacebookDAO::isAlreadyAddedInTableUser($controlVars['user']))
			FacebookDAO::addAppUser($controlVars['user']);
    	$this->controlVars = $controlVars;
    	$this->userCredentials = FacebookDAO::getUserCredentials($controlVars['user']);
	    if (empty($this->userCredentials['travelerID'])) $this->isSynchronized = false;
	    $this->dao = new MyTravelMapRepository($this->userCredentials);
  	}

  	function applyInputToModel() {
    	$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'default';
		//if($action=='genmap') {		
		//	var_dump($_REQUEST);
		//	exit;
		//}
    	if (method_exists($this, $action.'Action')) {
      		return call_user_func(array('MyTravelMapController', $action.'Action'));
    	} else {
      		//TODO: redirect to error page
      		throw new Exception("Method {$action}Action() does not exist in MyTravelMapController.");
    	}
  	}

  	private function saveCountryListAction() {
		$newArrCountryID = !empty($_POST['wishList']) ? $_POST['wishList'] : array();
		$oldArrCountryID = self::getMapEntriesCountryID($this->dao->getTravelMapEntries());
		$this->addNewsFeed($newArrCountryID, $oldArrCountryID);
    	$this->dao->updateEntries($newArrCountryID);

    	$this->countryList = $this->dao->getCountryList();
    	$this->mapEntries = $this->dao->getTravelMapEntries();

    	$mav = new ModelAndView('travellog/views/mytravelmap/facebook/mytravelmap.php');
    	$mav->setModelValue('user', $this->controlVars['user']);    
    	$mav->setModelValue('countryList', $this->countryList);
    	$mav->setModelValue('mapEntries', $this->mapEntries);
    	$mav->setModelValue('isSynced', $this->isSynchronized);    
    	$mav->setModelValue('isOwnerUser', $this->controlVars['isOwnerUser']);
    	if ($this->isSynchronized)
    		$mav->setModelValue('uneditableMapEntries', $this->dao->getUneditableMapEntries());

    	return $mav;
  }

  	private function inviteAction() {
    	$this->suppressProfileUpdate = true;

    	//  Get list of friends who have this app installed...
    	$rs = $this->controlVars['facebook']->api_client->fql_query("SELECT uid FROM user WHERE is_app_user=1 and uid IN (SELECT uid2 FROM friend WHERE uid1 = {$this->controlVars['user']})");

    	$friends = "";
    	//  Build an delimited list of users...
    	if ($rs) {
      		for ( $i = 0; $i < count($rs); $i++ ) {
        		if ( $friends != "" ) $friends .= ",";
        		$friends .= $rs[$i]["uid"];
      		}
    	}
    	$formAction = isset($_REQUEST['postadd']) ? 'index.php?action=sync': 'index.php';

    	$mav = new ModelAndView('travellog/views/mytravelmap/facebook/mtm_invite.php');
    	$mav->setModelValue('user', $this->controlVars['user']);
    	$mav->setModelValue('friendsWithApp', $friends);
    	$mav->setModelValue('action', $formAction);

    	return $mav;
  	}

  	private function syncAction() {
    	$this->suppressProfileUpdate = true;
    	$ganetUser = $this->dao->getGanetUser($this->controlVars['user']);
    
    	$mav = new ModelAndView('travellog/views/mytravelmap/facebook/mtm_synchronize.php');
    	$mav->setModelValue('user', $this->controlVars['user']);
    	$mav->setModelValue('ganetEmail', $ganetUser['email']);
    	$mav->setModelValue('ganetUsername', $ganetUser['username']);
    	$mav->setModelValue('isSynchronized', $this->isSynchronized);

    	return $mav;
  	}

  	private function doSyncAction() {

    	$this->countryList = $this->dao->getCountryList();
    	$this->mapEntries = $this->dao->getTravelMapEntries();

		$controllerArgs = array(
			'container'   => 'facebook.com', 
			'containerID' => $this->userCredentials['facebookID'],
			'email' 	  => $_POST['email'],
			'password'    => $_POST['password']
		);
    
		require_once 'travellog/controller/socialApps/Class.SynchronizeAppsController.php';  
		$synchronizeAppsController = new SynchronizeAppsController($controllerArgs);
		$result = $synchronizeAppsController->performAction('sync');

    	$ganetUser = $this->dao->getGanetUser($this->controlVars['user']);

   		$mav = new ModelAndView('travellog/views/mytravelmap/facebook/mtm_synchronize.php');
   		$mav->setModelValue('errors', $result);
    	$mav->setModelValue('ganetEmail', $ganetUser['email']);
    	$mav->setModelValue('ganetUsername', $ganetUser['username']);
    	$mav->setModelValue('isSynchronized', $this->isSynchronized);
    	$mav->setModelValue('user', $this->controlVars['user']);
    	return $mav;
  }

  	private function genmapAction() {
   		//this action is indirectly invoked when calling the default action where
   		//profile update also takes place. so no need to update our profile page
	   	$this->suppressProfileUpdate = true;
	   	$this->countryList = $this->dao->getCountryList();
	   	$this->mapEntries = $this->dao->getTravelMapEntries();
	   	//TODO: mechanism to include files in a less fragile manner
	   	$mav = new ModelAndView('travellog/views/mytravelmap/facebook/mtm_genmap.php');

	   	$mav->setModelValue('user', $this->controlVars['user']);
	   	$mav->setModelValue('isSync', $this->isSynchronized);
	   	$mav->setModelValue('countryList', $this->countryList);
	   	$mav->setModelValue('mapEntries', $this->mapEntries);
	   	$mav->setModelValue('isOwnerUser', $this->controlVars['isOwnerUser']);
	   	$mav->setModelValue('fbFirstName', $this->controlVars['fbFirstName']);

    		return $mav;
  	}

  	private function refreshProfileAction() {
		//call our profile updater directly
		$this->sendProfileMarkup();
    	$facebook = $this->controlVars['facebook'];
		$boxTab = isset($_GET['box']) ? 'v=box_3' : '';
    	$facebook->redirect($facebook->get_facebook_url() . '/profile.php?'.$boxTab);
    	exit;
	}

  	private function defaultAction() {
    	if (isset($_GET['id'])) $this->isOwner = $this->controlVars['user'] == $_GET['id'];
    	$this->countryList = $this->dao->getCountryList();
    	$this->mapEntries = $this->dao->getTravelMapEntries();


    	//TODO: mechanism to include files in a less fragile manner
    	$mav = new ModelAndView('travellog/views/mytravelmap/facebook/mytravelmap.php');
    	$mav->setModelValue('user', $this->controlVars['user']);
    	$mav->setModelValue('countryList', $this->countryList);
  		$mav->setModelValue('mapEntries', $this->mapEntries);
    	$mav->setModelValue('isSynced', $this->isSynchronized);
    	$mav->setModelValue('isOwnerUser', $this->controlVars['isOwnerUser']);
    	$mav->setModelValue('fbFirstName', $this->controlVars['fbFirstName']);
    
		if ($this->isSynchronized)
    		$mav->setModelValue('uneditableMapEntries', $this->dao->getUneditableMapEntries());

    	return $mav;
  	}  

  	//TODO: restrict use of this function if MyTravelMapController has not been instantiated
  	// as this is dependent on certain variables being set by constructer of controller
  	function sendProfileMarkup() {

    	if ($this->suppressProfileUpdate) return;

    	$countryList = $this->dao->getCountryList();
    	$mapEntries = $this->dao->getTravelMapEntries();
		$userID = $this->controlVars['user'];
		$isSynchronized = $this->isSynchronized;

		list($pinBlue, $pinGreen, $pinYellow, $pinShadow, $pinBlueSmall, $pinShadowSmall) = self::getPins();

    	require_once 'travellog/views/mytravelmap/facebook/mtm_profileMarkup.php';
    	require_once 'travellog/views/mytravelmap/facebook/mtm_wallTabProfile.php';
    	$fbml .= $fbnarrow; 
 		$this->controlVars['facebook']->api_client->profile_setFBML(NULL, $userID, $fbml, NULL, NULL, $fbml);
  	}

  	// added by adelbert

  	function updateAllProfile($users = array()) {
    	$countryList = MyTravelMapRepository::getCountryList();	
		list($pinBlue, $pinGreen, $pinYellow, $pinShadow, $pinBlueSmall, $pinShadowSmall) = self::getPins();
		$iteration = 1;
		foreach ($users as $user) {
			$userID = $user['facebookID'];
			if (self::getFacebookApi()->api_client->users_isAppUser($userID)) {

	    		$dao = new MyTravelMapRepository($user);
				if ($mapEntries = $dao->getTravelMapEntries()) {
			    	require 'travellog/views/mytravelmap/facebook/mtm_profileMarkup.php';
			    	require 'travellog/views/mytravelmap/facebook/mtm_wallTabProfile.php';
			    	$fbml .= $fbnarrow; 
					try{
						echo $iteration++." --> facebookID=".$userID."  --> mapEntries=".count($mapEntries)."<br>";
			 			self::getFacebookApi()->api_client->profile_setFBML(NULL, $userID, $fbml, NULL, NULL, $fbml);					
					} catch (Exception $e) {}
	  			} else echo $iteration++." --> facebookID=".$userID."<br>"; 				
			}			
  		}
		echo "<hr>";
 	} 

	static function displayProfileTab($facebookID = null) {
		if(empty($facebookID)) {
			echo 'Unable to fetch My Travel Map. Please try again later.';
		}

		$userCredentials = FacebookDAO::getUserCredentials($facebookID);
		$mtmRepository = new MyTravelMapRepository($userCredentials);
		$countryList = $mtmRepository->getCountryList();
		$mapEntries = $mtmRepository->getTravelMapEntries();
		$userID = $facebookID;
		$isSynchronized = !empty($userCredentials['travelerID']) ? true : false;
		list($pinBlue, $pinGreen, $pinYellow, $pinShadow, $pinBlueSmall, $pinShadowSmall) = MyTravelMapController::getPins();
		
		// Get Profile view for "tab view" and replace "<fb:wide>,</fb:wide>" tags for it to be viewable.
		require_once 'travellog/views/mytravelmap/facebook/mtm_profileMarkup.php';
		$tab_content = str_replace(array('<fb:wide>','</fb:wide>'),'', $fbml);
		
		// Display $tab_content for tab view
		require_once 'travellog/views/mytravelmap/facebook/mtm_tab.php';
	}

  	static function getPins(){
		$pinBlue 		= self::PINS_PATH.'markerBlue12x20.png';
		$pinGreen 		= self::PINS_PATH.'markerGreen12x20.png';
		$pinYellow 		= self::PINS_PATH.'markerYellow12x20.png';
		$pinShadow 		= self::PINS_PATH.'markerShadow22x20.png';		
		$pinBlueSmall 	= self::PINS_PATH.'markerBlue10x17.png';
		$pinShadowSmall = self::PINS_PATH.'markerShadow20x16.png';
		return array($pinBlue, $pinGreen, $pinYellow, $pinShadow, $pinBlueSmall, $pinShadowSmall);
  	}
	
	static function getMapEntriesCountryID($travelMapEntries = array()){
		$arrCountryID = array();
		foreach ($travelMapEntries as $entry) {
			$arrCountryID[] = $entry->getCountry()->getCountryID();
		}
		return $arrCountryID;
	}
			
  	private function addNewsFeed($newArrCountryID = array(),$oldArrCountryID = array()) {
		$facebook = $this->controlVars['facebook'];
		$friends = $facebook->api_client->friends_get();
		$arrCountryID = array_diff($newArrCountryID, $oldArrCountryID);
		
		if (!empty($arrCountryID) && !empty($friends)) {
			$arrCountryName = array();
			for($c=0;$c<count($arrCountryID);$c++){
				$arrCountryName[$c] = MyTravelMapRepository::getCountryNameByID($arrCountryID[$c]);
			}		
			$body_general = ''; 
			$templateBundleID = '61906738942';
			$friends = implode(',', $friends);
			$tokens = array('country' => implode(', ',$arrCountryName), 'id' => $facebook->get_loggedin_user());
			$result = $facebook->api_client->feed_publishUserAction($templateBundleID, $tokens ,$friends, $body_general);
			self::errorCatcher($result, $facebook->get_loggedin_user());
		}
	}		

	static private function errorCatcher($result,$id) {
		if (!$result) {
			$message = 'My Travel Map failed on setting feed_publishUserAction. On facebookID: '.$id;
			mail('adelbert.silla@goabroad.com', 'Facebook My Travel Map Error',$message);
			throw new Exception($message);
		}
	}

  	static function getFacebookApi(){
		require_once('travellog/model/FacebookPlatform/facebook.php');
		$appApikey = '147aa5f035170dd13d8670b15cb3f906';
		$apiSecret = '080705235dd747ed85d4c8ec9909c520';
		return new Facebook($appApikey,$apiSecret);
  	}

	static function getCities() {
		require_once("travellog/model/Class.City.php");
		$city = new City;
		return $city->getCities();
	}

	static function getNewCities() {
		require_once("travellog/model/Class.NewCity.php");
		return NewCity::getAllCities();

	}

}
?>
