<?php
require_once('travellog/helper/ganet_web_service/facebook/Class.fbMyTravelJournalsControllerHelper.php');
require_once('travellog/model/socialApps/mytraveljournals/facebook/Class.fbMyTravelJournalApp.php');
require_once('travellog/controller/socialApps/mytraveljournals/facebook/Class.fbMainDashboard.php');
require_once('travellog/controller/socialApps/mytraveljournals/Class.AbstractMyTravelJournalsController.php');

class fbMytravelJournalsController extends AbstractMyTravelJournalsController {
				
	private $tpl,
			$fbMyTravelJournalApp;	
	const	
			INVITE_FRIENDS = 'invitefriends',
			MTJ_TAB_VIEW = 'mtjtabjview',
			REFRESH_MTJ = 'refresh';
			
	function __construct($travelerID = null) {
			
		self::$entryHelper = new EntryHelper;
		$this->helper = new fbMyTravelJournalsControllerHelper;
		$this->fbMyTravelJournalApp = new fbMyTravelJournalApp;
		
		if($travelerID){
			$facebookID = WebServiceUser::getFacebookIDByTravelerID($travelerID);
			$this->user = new WebServiceUser($facebookID, self::FACEBOOK_DOMAIN);
			$this->updateFbProfile();
		} else {

			if($this->helper->getIsTabView()){
				$facebookID = $_REQUEST['fb_sig_profile_user'];
				$this->user = new WebServiceUser($facebookID, self::FACEBOOK_DOMAIN);
				$this->domain = $this->user->getDomain();
				$this->fbPerformAction();
				exit;				
			}

			if($loggedInUser = $this->fbMyTravelJournalApp->getFacebook()->require_login()) {
				$facebookID = isset($_GET['id']) ? $_GET['id'] : $loggedInUser;
				$this->helper->viewerID	= $loggedInUser;
				$this->user = new WebServiceUser($facebookID, self::FACEBOOK_DOMAIN);
				if( !$this->user->getID() ) $this->user->save();
				$this->domain = $this->user->getDomain();
				$this->fbPerformAction();
				$this->updateFbProfile();
				exit;
			}			
		}
	}

	function fbPerformAction(){

		switch($this->helper->getAction()) {
			case self::INVITE_FRIENDS:
				$this->showInviteFriendsForm();
				break;
			case self::MTJ_TAB_VIEW:
				$this->mtjTabView();
				break;				
			case self::REFRESH_MTJ:
				$this->backToFacebookBoxes();
				break;				
			default:
				$this->performAction();
				break;
		}		
	}

	function mtjTabView(){
		$this->showJournals();		
	}

	
	function showInviteFriendsForm() {
		
		$uids = $this->fbMyTravelJournalApp->getFacebook()->api_client->friends_get();
		
		$friendsCount = array();
		$excludedUids = array(); 
		foreach($uids as $iUid) { 
			if($iUid['is_app_user']==1){
				$excludedUids[] = $iUid['uid'];
			}
			$friendsCount[] = $iUid['uid'];
		}

		$args = array(
			'isSync'		=> AbstractMyTravelJournalApp::isSynchronized($this->user->getID()),
			'fbUserID'		=> $this->user->getWsID(),
			'friendsDiff' 	=> count($friendsCount) - count($excludedUids),
			'viewerIsOwner'	=> $this->user->getWsID() == $this->helper->getAppViewerID(),
			'excludedUIDList'=> implode(',',$excludedUids)
		);

		$MainDashboard = new fbMainDashboard(2);
		require_once('travellog/views/mytraveljournals/facebook_/tpl.mainView.php');			   
		require_once('travellog/views/mytraveljournals/facebook_/tpl.inviteFriends.php');
	}
	
	function updateFbProfile(){

		$dateTimeTool = new GaDateTime;
		$facebookID = $this->user->getWsID();
		$myTravelJournalApp = $this->fbMyTravelJournalApp;
		$facebook = $this->fbMyTravelJournalApp->getFacebook();
				
		$showJournalType = fbMyTravelJournal::getShowJournalType($this->user->getID(), $facebookID);

		$journals = fbMyTravelJournal::getRecentUpdateJournal($this->user->getID(),$this->user->getDomain());
		if($showJournalType == 1)
			$journals = fbMyTravelJournal::getUserSelectedJournals($this->user->getID(),$facebookID,$this->user->getDomain());

		$isSync = AbstractMyTravelJournalApp::isSynchronized($this->user->getID());

		$numDisplayed = count($journals);

		if($isSync == false) 
			$subMessage = 'Account is not synchronized to GoAbroad Network.';
		if($isSync == true && $numDisplayed < 1) 
			$subMessage = 'Account is synchronized to GoAbroad Network but there are no journals yet.';

		require('travellog/views/mytraveljournals/facebook_/tpl.fbProfile.php');
	}		

	static function updateAllFbProfile($users = array()) {
		$journals = array();
		$dateTimeTool = new GaDateTime;
		self::$entryHelper = new EntryHelper;
		$myTravelJournalApp = new fbMyTravelJournalApp;
		$facebook = $myTravelJournalApp->getFacebook();
		$iteration = 1;
		foreach ($users as $user) {
			$facebookID = $user['facebookID'];
			if ($facebook->api_client->users_isAppUser($facebookID)) {
				
				$showJournalType = fbMyTravelJournal::getShowJournalType($user['userID'], $facebookID);
				$journals = fbMyTravelJournal::getRecentUpdateJournal($user['userID'],self::FACEBOOK_DOMAIN);

				if($showJournalType == 1)
					$journals = fbMyTravelJournal::getUserSelectedJournals($user['userID'],$facebookID,self::FACEBOOK_DOMAIN);

				$isSync = AbstractMyTravelJournalApp::isSynchronized($user['userID']);
				$numDisplayed = count($journals);

				if($isSync == false) 
					$subMessage = 'Account is not synchronized to GoAbroad Network.';
				if($isSync == true && $numDisplayed < 1) 
					$subMessage = 'Account is synchronized to GoAbroad Network but there are no journals yet.';
				
				try{
					echo $iteration++." --> facebookID=".$facebookID."  --> Journals=".count($journals)."<br>";
					require('travellog/views/mytraveljournals/facebook_/tpl.fbProfile.php');				
				}
				catch (Exception $e) {}
		    }
		}    
		echo "<hr>";
	}		


	function backToFacebookBoxes(){
		$boxTab = isset($_GET['box']) ? '&v=box_3' : '&v=wall';
		$this->fbMyTravelJournalApp->getFacebook()->redirect('http://www.facebook.com/profile.php?id='.$this->user->getWsID().$boxTab);
	}
}

?>