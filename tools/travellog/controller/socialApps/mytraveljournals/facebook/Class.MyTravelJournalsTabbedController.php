<?php
require_once('travellog/helper/ganet_web_service/facebook/Class.fbMyTravelJournalsControllerHelper.php');
require_once('travellog/model/socialApps/mytraveljournals/facebook/Class.fbMyTravelJournalApp.php');
require_once('travellog/model/socialApps/mytraveljournals/Class.WebServiceUser.php');
require_once('Class.Template.php');
require_once('travellog/controller/socialApps/mytraveljournals/Class.AbstractMyTravelJournalsController.php');
//addded require for redirection
require_once("Class.ToolMan.php");

class MyTravelJournalTabbedController{
	protected	$user,
				$domain,
				$helper;
	
	function __construct() {
		require_once('travellog/model/FacebookPlatform/facebook.php');


		$appapikey = '5f354e06ee9eee0ca1256e364b77f8b3';
		$appsecret = '87b8be26ccf8ea33f776e116ac6cfd3f';
		$facebook = new Facebook($appapikey, $appsecret);
		$user_id = $this->facebook->get_loggedin_user();
		$this->helper = new fbMyTravelJournalsControllerHelper;
		$fbdomain = 'facebook.com';
		$this->user = new WebServiceUser($userID, $fbdomain);
		$this->domain = $this->user->getDomain();
	//	echo "action=".$this->helper->getAction(); exit;
	
	}
	
	function showTabJournal(){
		
		var_dump($this->user);
	}
	
	function showTabMTJinFB(){
		$myTravelJournalApp = new fbMyTravelJournalApp();
		
		$journals = AbstractMyTravelJournal::getUserSelectedJournals($this->user->getID(),$this->user->getWsID(),$this->user->getDomain());
		$args = array(
				'journals'				=>	$journals,
				'name'					=>	$this->helper->getOwnerName(),
				'viewerIsOwner'			=>	$this->user->getWsID() == $this->helper->getAppViewerID(),
				'ownerDotNetUsername'	=>	$this->user->getDotNetUsername()
			);
		
		$totalTravelID = count($journals);
		$this->tpl = new Template();
		
		$this->tpl->setPath('travellog/views/mytraveljournals/facebook_/');
		$this->tpl->set('myTravelJournalApp',$myTravelJournalApp);
		$this->tpl->set('args',$args);
		$this->tpl->set('totalTravelID',$totalTravelID);
		$this->tpl->out('tpl.profile.php');
		
	}
}
?>