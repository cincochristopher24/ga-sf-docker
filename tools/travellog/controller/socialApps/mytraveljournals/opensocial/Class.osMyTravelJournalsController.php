<? 
	require_once 'travellog/helper/ganet_web_service/opensocial/Class.osMyTravelJournalsControllerHelper.php';
	require_once('travellog/model/socialApps/mytraveljournals/opensocial/Class.osMyTravelJournalApp.php');
	require_once('travellog/model/socialApps/mytraveljournals/opensocial/Class.osMyTravelJournal.php');
	class osMyTravelJournalsController extends AbstractMyTravelJournalsController{
		
		function __construct(){
			$this->helper = new osMyTravelJournalsControllerHelper;
			$this->user   = new WebServiceUser($this->helper->ownerID, $this->helper->domain);
			$this->domain = $this->helper->domain;

			self::$entryHelper = new EntryHelper;
		}
	   
	}
	
?>