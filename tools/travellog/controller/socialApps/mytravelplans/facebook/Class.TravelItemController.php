<?php
/**
 * TODO: refactor! this is a nightmare to maintain
 */

require_once('Class.Template.php');
require_once 'travellog/model/FacebookPlatform/facebook.php';
require_once('travellog/model/socialApps/mytravelplans/facebook/Class.TravelScheduleFB.php');

class TravelItemController {

  private $userCredentials = array();
  private $travelSchedule;
  const TEMPLATE_PATH = 'travellog/views/mytravelplans/facebook/';

  function __construct($facebookID) {
    $this->template = new Template();
	$params = array( 'container' => 'facebook.com', 'containerID' => $facebookID);
	$this->travelSchedule = new TravelScheduleFB($params);
	$this->userCredentials = $this->travelSchedule->getUserCredentials();
  }

  public function listTravelItems($facebookID, $isOwner=1, $friendsIDs=null, $publishStream = null) {

    $wishList = !empty($this->userCredentials['travelerID']) ? TravelScheduleFB::getCountriesToTravel($this->userCredentials['travelerID']) : array();
    $friendsCountryIDs = $this->travelSchedule->getFriendsCountryIDs($friendsIDs);
	$onGoingAndNoDatesTravels = TravelScheduleFB::getOrderedTravels($this->userCredentials['userID']);
	$pastTravels = TravelScheduleFB::getPastTravels($this->userCredentials['userID']);
	$includePastTravelsSetting = (bool)$this->userCredentials['travelPlansIncludePast'];
	$travelItems = $includePastTravelsSetting ? array_merge($onGoingAndNoDatesTravels, $pastTravels) : $onGoingAndNoDatesTravels;
    $this->template->set('user', $facebookID);
    $this->template->set('isOwner', $isOwner);
    $this->template->set('wishList', $wishList);
    $this->template->set('travelItems', $travelItems);
    $this->template->set('friendsCountryIDs', $friendsCountryIDs);
    $this->template->set('countryList', TravelScheduleFB::getCountryList());
    $this->template->set('ganetUsername', $this->userCredentials['username']);
    $this->template->set('numPresentTravels', count($onGoingAndNoDatesTravels));
    $this->template->set('itemsToDisplaySetting', $this->userCredentials['travelPlansDisplayed']);
    $this->template->set('includePastTravelsSetting', $this->userCredentials['travelPlansIncludePast']);
    $this->template->set('publishStream', $publishStream);
    $this->template->out(self::TEMPLATE_PATH.'tpl.ViewTravelSchedule.php');

  }

  function getUserCredentials(){
	return $this->userCredentials;
  }

  public function initAddForm($formParams = null) {
    // set parameters to empty/default
    if (!$formParams) {
      $formParams = array();
      $formParams['action'] = 'add';

      $formParams['title'] = '';
      $formParams['details'] = '';
      $formParams['fromMonth'] = '';
      $formParams['fromDay'] = '';
      $formParams['fromYear'] = '';
      $formParams['toMonth'] = '';
      $formParams['toDay'] = '';
      $formParams['toYear'] = '';
      $formParams['countries'] = array();
    }

    $this->setupForm($formParams);
  }

  public function initEditForm() {
    // display form with existing travel item data
    $travelItem = TravelScheduleFB::retrieveByPk($_GET['travelItemID']);

    $formParams = array();

    $formParams['action'] = 'edit';
    $formParams['travelItemID'] = $_GET['travelItemID'];
    $formParams['title'] = $travelItem->getTitle();
    $formParams['details'] = $travelItem->getDetails();

    list($fromYear, $fromMonth, $fromDay) = explode('-', $travelItem->getDateFrom());
    //FIX-ME: day and month are optional
    if (checkdate($fromMonth, $fromDay, $fromYear)) {
      $formParams['fromMonth'] = $fromMonth;
      $formParams['fromDay'] = $fromDay;
      $formParams['fromYear'] = $fromYear;
    } else {
      $formParams['fromMonth'] = '';
      $formParams['fromDay'] = '';
      $formParams['fromYear'] = '';
    }
    list($toYear, $toMonth, $toDay) = explode('-', $travelItem->getDateTo());
    $formParams['toMonth'] = $toMonth;
    $formParams['toDay'] = $toDay;
    $formParams['toYear'] = $toYear;

    $formParams['destinations'] = $travelItem->getDestinations();
    $this->setupForm($formParams);
  }

  public function delTravelItem($user, $friendsIDs=NULL) {
    // Check that logged-in user owns the travelItem then delete

    if (TravelScheduleFB::isValid(trim($_GET['travelItemID']), $this->userCredentials['userID'])) {
      TravelScheduleFB::doDelete(TravelScheduleFB::retrieveByPk($_GET['travelItemID']));
    }
    if (!isset($_GET['view'])) {
      $this->listTravelItems($user, 1, $friendsIDs);
    }//else redirect to profile page
  }

  public function addTravelItem($user, $friendsIDs=NULL) {
  	$feedMsg = null;

  	$isValid =
  		isset($_POST['title']) && !empty($_POST['title']) &&
  		isset($_POST['details']) && !empty($_POST['details']) &&
  		isset($_POST['countries']) && !empty($_POST['countries']);

  	if ($isValid) {
	  	//TODO: remove feed action in composeTravelItem
	    TravelScheduleFB::doInsert($this->composeTravelItem($user,$feedAction = 'added a new'));
	    $feedMsg = 'I added the "'.trim($_POST['title']).'" itinerary to My Travel Plans.';
  	}
    $this->listTravelItems($user, 1, $friendsIDs, $feedMsg);
  }

  public function editTravelItem($user, $friendsIDs=NULL) {
  	$feedMsg = null;
    if (isset($_POST['travelItemID']) && !trim($_POST['travelItemID']) == '') {
    	//TODO: remove feed action in composeTravelItem
	    TravelScheduleFB::doUpdate($this->composeTravelItem($user,$feedAction = 'edited the'));
		$feedMsg = 'I edited the "'.trim($_POST['title']).'" itinerary in My Travel Plans.';
    }
    $this->listTravelItems($user, 1, $friendsIDs, $feedMsg);
  }

  public function synchronize($user) {
    $username = $this->userCredentials['travelerID'] ? $this->userCredentials['username'] : false;

    $this->template->set('user', $user);
    $this->template->set('ganetUserName', $username);
    $this->template->out(self::TEMPLATE_PATH.'tpl.ViewSynchronize.php');
  }

  public function showSettings() {
	$this->template->set('pastTravelsCount', count(TravelScheduleFB::getPastTravels($this->userCredentials['userID'])));
	$this->template->set('totalNumItems', count(TravelScheduleFB::getOrderedTravels($this->userCredentials['userID'])));
	$this->template->set('itemsToDisplaySetting', $this->userCredentials['travelPlansDisplayed']);
	$this->template->set('includePastTravelsSetting', $this->userCredentials['travelPlansIncludePast']);

    $this->template->out(self::TEMPLATE_PATH.'tpl.settings.php');
  }

  public function saveSettings() {
	TravelScheduleFB::editUserSettings( array(
		'travelPlansDisplayed'	 => $_POST['inputSettings'],
		'travelPlansIncludePast' => $_POST['inputPast'] ? '1' : '0',
		'travelerID'			 => $this->userCredentials['travelerID'])
	);
  }

  private function setupForm($formParams) {
    $this->template->set('ctryList', TravelScheduleFB::getCountryList());
    $this->template->set('title', $formParams['title']);
    $this->template->set('details', $formParams['details']);
    $this->template->set('fromMonth', $formParams['fromMonth']);
    $this->template->set('fromDay', $formParams['fromDay']);
    $this->template->set('fromYear', $formParams['fromYear']);
    $this->template->set('toMonth', $formParams['toMonth']);
    $this->template->set('toDay', $formParams['toDay']);
    $this->template->set('toYear', $formParams['toYear']);
    $this->template->set('monthArray', array('', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'));
    $this->template->set('yearArray', array(2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020,2021,2022,2023,2024,2025,2026,2027,2028,2029,2030));
    $this->template->set('action', $formParams['action']);

    if (!empty($formParams['travelItemID']))
      $this->template->set('travelItemID', $formParams['travelItemID']);
    if (!empty($formParams['destinations'])) {
      $this->template->set('destinations', $formParams['destinations']);
    }

    $this->template->out(self::TEMPLATE_PATH.'tpl.FrmTravelSchedule.php');
  }

  private function composeTravelItem($user, $feedAction = '') {
    //$userID = TravelScheduleFB::getUserCredentials($user);

	$travelItem = new TravelPlan();

	$travelItem->setUserID($this->userCredentials['userID']);
	$travelItem->setTitle(trim($_POST['title']));
	$travelItem->setDetails(trim($_POST['details']));
    // Not set when adding
    if (isset($_POST['travelItemID']) && !trim($_POST['travelItemID']) == '') {
      $travelItem ->setTravelWishID($_POST['travelItemID']);
    }

    $countries = $_POST['countries'];
    $details = $_POST['description'];
    $fromMonth = $_POST['destFromMonth'];
    $fromDay = $_POST['destFromDay'];
    $fromYear = $_POST['destFromYear'];
    $toMonth = $_POST['destToMonth'];
    $toDay = $_POST['destToDay'];
    $toYear = $_POST['destToYear'];

    $earliestDate = TravelItemController::getEarliestDate($fromYear, $fromMonth, $fromDay);
    $latestDate = TravelItemController::getLatestDate($toYear, $toMonth, $toDay);

    if($this->countNonZeroDates($fromYear) > 1) {
      $latestFromDate = TravelItemController::getLatestDate($fromYear, $fromMonth, $fromDay);
      if (strtotime($latestFromDate) > strtotime($latestDate)) $latestDate = $latestFromDate;
    }

    $travelItem->setDateFrom($earliestDate);
    $travelItem->setDateTo($latestDate);

    $destinations = array();
    for ($i = 0; $i < count($countries); $i++) {
	  $country = new Country($countries[$i]);
	  $country->setCountryID($countries[$i]);

	  $travelDest = new AppTravelDestinations();
	  $travelDest->setTravelWishID($travelItem->getTravelWishID());
	  $travelDest->setCountry($country);
	  $travelDest->setDescription($description[$i]);

	  $fY = !empty($fromYear[$i])  ? $fromYear[$i]  : '0000';
	  $fM = !empty($fromMonth[$i]) ? $fromMonth[$i] : '00';
	  $fD = !empty($fromDay[$i])   ? $fromDay[$i]   : '00';
	  $tY = !empty($toYear[$i])    ? $toYear[$i]    : '0000';
	  $tM = !empty($toMonth[$i])   ? $toMonth[$i]   : '00';
	  $tD = !empty($toDay[$i])     ? $toDay[$i]     : '00';

	  $travelDest->setStartDate("$fY-$fM-$fD");
	  $travelDest->setEndDate("$tY-$tM-$tD");

	  $destinations[] = $travelDest;

    }

    $travelItem->setDestinations($destinations);

	// added by adelbert
	// purpose: to post news feed..
	  //TODO: this is no longer needed??? HAM 15Nov2010
	//self::addNewsFeed($feedAction,$_POST['title']);

    return $travelItem;
  }

  //TODO: find a way to use php's date functions
  public static function getEarliestDate($year, $month, $day) {
    $earliestYear = '9999';
    $earliestMonth = '13';
    $earliestDay = '32';
    for ($i=0; $i<count($year); $i++) {
      if ($year[$i] == '0000' || empty($year[$i])) continue;
      if ($year[$i] < $earliestYear) {
        $earliestYear = $year[$i];
        $earliestMonth = $month[$i];
        $earliestDay = $day[$i];
      } elseif ($year[$i] == $earliestYear) {
        if ($month[$i] < $earliestMonth) {
          $earliestMonth = $month[$i];
          $earliestDay = $day[$i];
        } elseif ($month[$i] == $earliestMonth) {
          if ($day[$i] < $earliestDay) $earliestDay = $day[$i];
        }
      }
    }
    if ($earliestYear == '9999') $earliestYear = '0000';
    if ($earliestMonth == '13') $earliestMonth = '00';
    if ($earliestDay == '32') $earliestDay = '00';

    return $earliestYear.'-'.$earliestMonth.'-'.$earliestDay;
  }

  public static function getLatestDate($year, $month, $day) {
    $latestYear = '0000';
    $latestMonth = '00';
    $latestDay = '00';
    for ($i=0; $i<count($year); $i++) {
      if ($year[$i] > $latestYear) {
        $latestYear = $year[$i];
        $latestMonth = $month[$i];
        $latestDay = $day[$i];
      } elseif ($year[$i] == $latestYear) {
        if ($month[$i] > $latestMonth) {
          $latestMonth = $month[$i];
          $latestDay = $day[$i];
        } elseif($month[$i] == $latestMonth) {
          if ($day[$i] > $latestDay) $latestDay = $day[$i];
        }
      }
    }

    return $latestYear.'-'.$latestMonth.'-'.$latestDay;
  }

  private function countNonZeroDates($dateArray) {
    $cntNonZeroDates = 0;
    foreach ($dateArray as $d) {
      if ($d != '0000' && $d != '') $cntNonZeroDates++;
    }
    return $cntNonZeroDates;
  }

  public static function insertStatus($travelItems) {
    $currentDate = strtotime(date("Y-m-d"));
    foreach ($travelItems as $t) {
      $past = true;

      $dateFrom = $t->getDateFrom();
      $dateTo = $t->getDateTo();

      list ($fromYear, $fromMonth, $fromDay) = explode('-', $dateFrom);
      list ($toYear, $toMonth, $toDay) = explode('-', $dateTo);
      list ($currentYear, $currentMonth, $currentDay) = explode('-', date("Y-m-d"));

      if ($dateFrom == '0000-00-00' && $dateTo == '0000-00-00') {//filter out empty dates
        $past = false;
      } else if (strtotime($dateTo) >= $currentDate || strtotime($dateFrom) >= $currentDate) {
        $past = false;
      //incomplete dates
      } else if ($fromYear==$currentYear && $fromMonth=='00' && $fromDay=='00') {
        $past = false;
      } else if ($fromYear==$currentYear && $fromMonth==$currentMonth && $fromDay=='00') {
        $past = false;
      } else if ($toYear==$currentYear && $toMonth=='00' && $toDay=='00') {
        $past = false;
      } else if ($toYear==$currentYear && $toMonth==$currentMonth && $toDay=='00') {
        $past = false;
      }

      $t->setPast($past);
    }

    return $travelItems;
  }

  public function listFriendsWithSameDestination($userIDs, $ctryID) {
	$destinations = $this->travelSchedule->getDestinationsWithCountryID($userIDs, $ctryID);

    $this->template->set('containerID', $this->userCredentials['containerID']);
    $this->template->set('destinations', $destinations);

    $this->template->out(self::TEMPLATE_PATH.'tpl.ViewDestinations.php');
  }

  	static function addNewsFeed($action = '',$itinerary = ''){
		$facebook = self::getFacebook();
		$templateBundleID = '50429672774';
		$friends = $facebook->api_client->friends_get();
		if(!empty($friends)) {
			$body_general = '';
			$friends = implode(',', $friends);
			$tokens = array('action'=> $action, 'itinerary' => '<a href ="http://apps.facebook.com/travel-todo/index.php?vid='.$facebook->get_loggedin_user().'">'.$itinerary.'</a>', 'id' => $facebook->get_loggedin_user());
			$result = $facebook->api_client->feed_publishUserAction($templateBundleID, $tokens ,$friends, $body_general);
			self::errorCatcher($result);
		}
	}

	static private function errorCatcher($result) {
		//self::errorCatcher($result);
		if (!$result) {
			throw new Exception('My Travel Plans failed on setting feed_publishUserAction');
		}
	}

	static function updateStaticProfile($travelerID){
		/**
		 * 8Feb2011: profile.setfbml has been deprecated by facebook.
		 *
		 * TODO: remove code invoking this function
		 */
		return;

//		$facebook = self::getFacebook();
//		$userCredentials = TravelScheduleFB::getUserCredentialsByTravelerID($travelerID);
//		$user = $userCredentials['facebookID'];
//		$friendsIDsWithApp = array();
//		require_once(dirname(__FILE__).'/../../../../../../goabroad.net/facebook/mytravelplans/profilebox.php');
	}

	static function updateAllStaticProfile($users = array()){

		/**
		 * 8Feb2011: profile.setfbml has been deprecated by facebook.
		 *
		 * TODO: remove code invoking this function
		 */
		return;

//		$facebook = self::getFacebook();
//		foreach ($users AS $userCredentials) {
//			$user = $userCredentials['facebookID'];
//			if ($facebook->api_client->users_isAppUser($user)) {
//				$friendsIDsWithApp = array();
//				try{
//					echo 'facebookID-->'.$user.'<br>';
//					require(dirname(__FILE__).'/../../../../../../goabroad.net/facebook/mytravelplans/profilebox.php');
//				} catch (Exception $e) {}
//			}
//		}
//		echo '<hr>';
	}
	static function getFacebook(){
		$appapikey = '09cee88315ee8a1072be69ffae844190';
		$appsecret = 'c1e258b26c2ab3c293943d2a44d35336';
		return new Facebook($appapikey, $appsecret);
	}
}
?>
