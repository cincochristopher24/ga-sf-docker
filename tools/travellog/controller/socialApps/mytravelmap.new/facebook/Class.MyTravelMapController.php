<?php
require_once 'Cache/ganetCacheProvider.php';
require_once("travellog/model/Class.Travel.php");
require_once("travellog/model/Class.TravelLog.php");
require_once("travellog/model/Class.EntryTips.php");
require_once('travellog/helper/Class.EntryHelper.php');


require_once 'travellog/model/socialApps/mytravelmap/facebook/Mvc.php';
require_once 'travellog/model/socialApps/mytravelmap/facebook/Map.php';
require_once 'travellog/model/socialApps/mytravelmap/facebook/Class.FBCountry.php';
require_once 'travellog/model/socialApps/mytravelmap/facebook/Class.FacebookDAO.php';
require_once 'travellog/model/socialApps/mytravelmap/facebook/Class.TravelMapEntry.php';
require_once 'travellog/model/socialApps/mytravelmap/facebook/Class.MyTravelMapDAO.php';
require_once 'travellog/model/socialApps/mytravelmap/facebook/Class.MyTravelMapRepository.php';
require_once 'travellog/model/socialApps/mytravelmap/facebook/Interface.FacebookApplication.php';
require_once 'travellog/model/socialApps/mytravelmap/Class.TravelMapUserPeer.php';

require_once 'travellog/model/socialApps/mytravelmap/facebook/location/Class.FBCityLocation.php';
require_once 'travellog/model/socialApps/mytravelmap/facebook/location/Class.FBCountryLocation.php';
require_once 'travellog/model/socialApps/mytravelmap/facebook/location/Class.FBLocation.php';
require_once 'travellog/model/socialApps/mytravelmap/facebook/location/Class.FBLocationPeer.php';
require_once 'travellog/model/socialApps/mytravelmap/facebook/location/Class.FBNewCity.php';


class MyTravelMapController extends AbstractController implements FacebookApplication {

  	private $controlVars;
  	private $countryList;
  	private $mapEntries;
  	private $dao;                                          	
  	private $isSynchronized = true;
  	private $userCredentials = array();
  	private $pinSetting;
  	//variable to avoid the expensive call to update the profile page
  	//in the future update would be automatic, we won't have to call it
  	//explicitly in index.php or the controller;
  	private $suppressProfileUpdate = false; //should be moved to interface?

	const TEMPLATE_PATH = 'travellog/views/mytravelmap/facebook/new/';
	const GOABROAD_DOTNET_URL = 'http://www.goabroad.net';
  	const PINS_PATH = "http://www.goabroad.net/facebook/mytravelmap/images/corners/";


  	function __construct($controlVars) {
    	$this->controlVars = $controlVars;
    	//$this->userCredentials = FacebookDAO::getUserCredentials($controlVars['user']);
    	$this->userCredentials = TravelMapUserPeer::retrieveUserCredentialsFromFbID($controlVars['user']);
	    if (empty($this->userCredentials['travelerID'])) $this->isSynchronized = false;
	    $this->dao = new MyTravelMapRepository($this->userCredentials);
	    $this->pinSetting = $this->userCredentials['pinSetting'];
  	}

  	function applyInputToModel() {
    	$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'default';
		//if($action=='genmap') {		
		//	var_dump($_REQUEST);
		//	exit;
		//}
    	if (method_exists($this, $action.'Action')) {
      		return call_user_func(array('MyTravelMapController', $action.'Action'));
    	} else {
      		//TODO: redirect to error page
      		throw new Exception("Method {$action}Action() does not exist in MyTravelMapController.");
    	}
  	}

  	private function saveCountryListAction() {
		$newArrCountryID = !empty($_POST['wishList']) ? $_POST['wishList'] : array();
		$oldArrCountryID = self::getMapEntriesCountryID($this->dao->getTravelMapEntries());
		$this->addNewsFeed($newArrCountryID, $oldArrCountryID);
    	$this->dao->updateEntries($newArrCountryID);

    	$this->countryList = $this->dao->_getCountryList();
    	$this->mapEntries = $this->dao->getTravelMapEntries();

    	$mav = new ModelAndView(self::TEMPLATE_PATH.'mytravelmap.php');
    	$mav->setModelValue('user', $this->controlVars['user']);    
    	$mav->setModelValue('countryList', $this->countryList);
    	$mav->setModelValue('mapEntries', $this->mapEntries);
    	$mav->setModelValue('isSynced', $this->isSynchronized);    
    	$mav->setModelValue('isOwnerUser', $this->controlVars['isOwnerUser']);
    	if ($this->isSynchronized)
    		$mav->setModelValue('uneditableMapEntries', $this->dao->getUneditableMapEntries());

    	return $mav;
  }

  	private function inviteAction() {
    	$this->suppressProfileUpdate = true;

    	//  Get list of friends who have this app installed...
    	$rs = $this->controlVars['facebook']->api_client->fql_query("SELECT uid FROM user WHERE is_app_user=1 and uid IN (SELECT uid2 FROM friend WHERE uid1 = {$this->controlVars['user']})");

    	$friends = "";
    	//  Build an delimited list of users...
    	if ($rs) {
      		for ( $i = 0; $i < count($rs); $i++ ) {
        		if ( $friends != "" ) $friends .= ",";
        		$friends .= $rs[$i]["uid"];
      		}
    	}
    	$formAction = isset($_REQUEST['postadd']) ? 'index.php?action=sync': 'index.php';

    	$mav = new ModelAndView(self::TEMPLATE_PATH.'mtm_invite.php');
    	$mav->setModelValue('user', $this->controlVars['user']);
    	$mav->setModelValue('friendsWithApp', $friends);
    	$mav->setModelValue('action', $formAction);
    	$mav->setModelValue('isOwnerUser', $this->controlVars['isOwnerUser']);
    	return $mav;
  	}

  	private function syncAction() {
    	$this->suppressProfileUpdate = true;
    	$ganetUser = $this->dao->getGanetUser($this->controlVars['user']);
    
    	$mav = new ModelAndView(self::TEMPLATE_PATH.'mtm_synchronize.php');
    	$mav->setModelValue('user', $this->controlVars['user']);
    	$mav->setModelValue('ganetEmail', $ganetUser['email']);
    	$mav->setModelValue('ganetUsername', $ganetUser['username']);
    	$mav->setModelValue('isSynchronized', $this->isSynchronized);
    	$mav->setModelValue('isOwnerUser', $this->controlVars['isOwnerUser']);

    	return $mav;
  	}

  	private function doSyncAction() {

    	$this->countryList = $this->dao->_getCountryList();
    	$this->mapEntries = $this->dao->_getTravelMapEntries();

		$controllerArgs = array(
			'container'   => 'facebook.com', 
			'containerID' => $this->userCredentials['facebookID'],
			'email' 	  => $_POST['email'],
			'password'    => $_POST['password']
		);
    
		require_once 'travellog/controller/socialApps/Class.SynchronizeAppsController.php';  
		$synchronizeAppsController = new SynchronizeAppsController($controllerArgs);
		$result = $synchronizeAppsController->performAction('sync');

    	$ganetUser = $this->dao->getGanetUser($this->controlVars['user']);

   		$mav = new ModelAndView(self::TEMPLATE_PATH.'mtm_synchronize.php');
   		$mav->setModelValue('message', $result);
    	$mav->setModelValue('ganetEmail', $ganetUser['email']);
    	$mav->setModelValue('ganetUsername', $ganetUser['username']);
    	$mav->setModelValue('isSynchronized', $this->isSynchronized);
	   	$mav->setModelValue('isOwnerUser', $this->controlVars['isOwnerUser']);
    	$mav->setModelValue('user', $this->controlVars['user']);
    	return $mav;
  }

  	private function refreshProfileAction() {
		//call our profile updater directly
		$this->sendProfileMarkup();
    	$facebook = $this->controlVars['facebook'];
		$boxTab = isset($_GET['box']) ? 'v=box_3' : '';
    	$facebook->redirect($facebook->get_facebook_url() . '/profile.php?'.$boxTab);
    	exit;
	}
	
	private function changeSettingAction() {
		$this->suppressProfileUpdate = true;
    
    	$mav = new ModelAndView(self::TEMPLATE_PATH.'mtm_setting.php');
    	$mav->setModelValue('user', $this->controlVars['user']);
    	$mav->setModelValue('pinSetting', $this->pinSetting);
	   	$mav->setModelValue('isOwnerUser', $this->controlVars['isOwnerUser']);
	   	$mav->setModelValue('fbFirstName', $this->controlVars['fbFirstName']);

    	return $mav;
	}
	
	private function doChangeSettingAction() {
   		$result = TravelMapUserPeer::updatePinSetting($_POST['pinSetting'], $this->userCredentials['userID']);
		$this->controlVars['facebook']->redirect('http://apps.facebook.com/my-travel-map/index.php?updatedMyTravelMap');
   		//$mav = new ModelAndView(self::TEMPLATE_PATH.'mtm_setting.php');
 		//$mav->setModelValue('errors', $result);
    	//$mav->setModelValue('user', $this->controlVars['user']);
    	//$mav->setModelValue('pinSetting', $_POST['pinSetting']);
	   	//$mav->setModelValue('isOwnerUser', $this->controlVars['isOwnerUser']);
	   	//$mav->setModelValue('fbFirstName', $this->controlVars['fbFirstName']);
    	//return $mav;
	}
	
	private function addLocationAction() {
		$this->suppressProfileUpdate = true;
		
		$name  = $_REQUEST['name'];
		$locID = $_REQUEST['locID'];
		$lat   = $_REQUEST['latitude'];
		$long  = $_REQUEST['longitude'];
		$countryID = $_REQUEST['countryID'];
		$isNewCountry = $_REQUEST['isNewCountry'];
		$newLocationID = $this->dao->_addNewCity($long,$lat,$locID,$name,$countryID);
    	$newLocationIDs = array();
    	if($isNewCountry)
    		$newLocationIDs = array($newLocationID, $locID);
    	else
    		$newLocationIDs = array($newLocationID);
	
		if($this->isSynchronized) {
    		$id = $this->userCredentials['travelerID'];
			$this->dao->_addSyncedTravelMapEntry($id, $newLocationIDs, $countryID);
			ganetCacheProvider::instance()->getCache()->delete('Traveler_Countries_Traveled_'.$id);

		} else {
			$id = $this->userCredentials['userID'];
			$this->dao->_addUnSyncedTravelMapEntry($id, $newLocationIDs, $countryID);
		}		
		return new ModelAndView('');
	}

  	private function defaultAction() {
    	if (isset($_GET['id'])) $this->isOwner = $this->controlVars['user'] == $_GET['id'];

    	$this->countryList = $this->dao->_getCountryList();
    	$this->mapEntries = $this->dao->_getTravelMapEntries();

		if (isset($_REQUEST['updatedMyTravelMap'])) $this->sendProfileMarkup($this->countryList, $this->mapEntries);
				
		//TODO: mechanism to include files in a less fragile manner
    	$mav = new ModelAndView(self::TEMPLATE_PATH.'mytravelmap.php');
    	$mav->setModelValue('user', $this->controlVars['user']);
    	$mav->setModelValue('countryList', $this->countryList);
  		$mav->setModelValue('mapEntries', $this->mapEntries);
    	$mav->setModelValue('isSynced', $this->isSynchronized);
    	$mav->setModelValue('isOwnerUser', $this->controlVars['isOwnerUser']);
    	$mav->setModelValue('fbFirstName', $this->controlVars['fbFirstName']);
    	$mav->setModelValue('displayMapTip', $this->userCredentials['displayMapTip']);
		if ($this->isSynchronized)
    		$mav->setModelValue('uneditableMapEntries', $this->dao->getUneditableMapEntries());
    	if (isset($_REQUEST['edit']) && $this->controlVars['isOwnerUser'])
    		$mav->setModelValue('edit', true);

    	return $mav;
  	}

  	private function genmapAction() {
   		//this action is indirectly invoked when calling the default action where
   		//profile update also takes place. so no need to update our profile page
	   	$this->suppressProfileUpdate = true;
	   	$this->countryList = $this->dao->_getCountryList();
	   	$this->mapEntries = $this->dao->_getTravelMapEntries();
		$allLocations = array_keys($this->mapEntries);
		foreach($this->mapEntries as $mapEntry){
			foreach($mapEntry->getCountry()->getCityLocationList() as $ct){
				$allLocations[] = $ct->getID();
			}
		}
		
		$friendsIDs = MyTravelMapDAO::_filterFriendsInLocations($this->controlVars['friendsWithApp'], array_unique($allLocations), $this->isSynchronized);
		
	   	//TODO: mechanism to include files in a less fragile manner
	   	$mav = new ModelAndView(self::TEMPLATE_PATH.'mtm_genmap.php');
		
		//MyTravelMapDAO::_filterFriendsInLocations($this->controlVars['friendsWithApp'], array_keys($this->mapEntries));

	   	$mav->setModelValue('user', $this->controlVars['user']);
	   	$mav->setModelValue('isSync', $this->isSynchronized);
	   	$mav->setModelValue('countryList', $this->countryList);
	   	$mav->setModelValue('mapEntries', $this->mapEntries);
		$mav->setModelValue('friendsBeenThere', $friendsIDs);
	   	$mav->setModelValue('isOwnerUser', $this->controlVars['isOwnerUser']);
	   	$mav->setModelValue('fbFirstName', $this->controlVars['fbFirstName']);
    	$mav->setModelValue('pinSetting', $this->pinSetting);
   		return $mav;
  	}
  


	private function getCitiesAction() {
		require_once("travellog/model/Class.LocationFactory.php");
		$location = LocationFactory::instance();
		$location = $location->create($_REQUEST['locationID']);
		$cities = $location->getCountry()->getCities(0, false);
	   	$mav = new ModelAndView(self::TEMPLATE_PATH.'mtm_cities.php');
		$countryID = $location->getCountry()->getCountryID();
		if(is_object($this->dao->_retrieveTravelMapEntry($countryID))) {
			$selectedCities = array_keys($this->dao->_retrieveTravelMapEntry($countryID)->getCountry()->getCityLocationList());			
		} else {
			$selectedCities = array();
		}
		$mav->setModelValue('countryID', $countryID);
		$mav->setModelValue('countryLocationID', $_REQUEST['locationID']);
	   	$mav->setModelValue('selectedCities', $selectedCities);
	   	//$mav->setModelValue('selectedCities', explode('.', $_REQUEST['selectedCities']));
		$mav->setModelValue('latitude', $location->getCoordinates()->getY());
	   	$mav->setModelValue('longitude', $location->getCoordinates()->getX());
	   	$mav->setModelValue('cities', $cities);
		return $mav;
	}

	private function addCitiesAction() {
		$countryLocationID = $_REQUEST['countryLocationID'];
		$countryID = $_REQUEST['countryID'];
		$locationIDs = explode('.', substr($_REQUEST['stringLocationIDs'], 1));
		if($this->isSynchronized) {
    		$id = $this->userCredentials['travelerID'];
			$this->dao->_addSyncedTravelMapEntry($id, $locationIDs, $countryID);
			ganetCacheProvider::instance()->getCache()->delete('Traveler_Countries_Traveled_'.$id);
		} else {
			$id = $this->userCredentials['userID'];
			$this->dao->_addUnSyncedTravelMapEntry($id, $locationIDs, $countryID);
		}		
		$mapEntry = $this->dao->_retrieveTravelMapEntry($countryID);
		
	   	$mav = new ModelAndView(self::TEMPLATE_PATH.'mtm_addCities.php');
		$mav->setModelValue('locationID', $countryLocationID);
	   	$mav->setModelValue('mapEntry', $mapEntry);
		return $mav;
	}

	private function removeLocationAction() {
		$locationIDs = explode('.',$_REQUEST['stringLocationIDs']);
		if($this->isSynchronized) {
    		$id = $this->userCredentials['travelerID'];
			$this->dao->_removeSyncedTravelMapEntry($id, $locationIDs);
			ganetCacheProvider::instance()->getCache()->delete('Traveler_Countries_Traveled_'.$id);
		} else {
			$id = $this->userCredentials['userID'];
			$this->dao->_removeUnSyncedTravelMapEntry($id, $locationIDs);
		}
		return new ModelAndView('');
	}

	private function updateDisplayMapTipAction() {
		TravelMapUserPeer::updateDisplayMapTip($this->userCredentials['userID'], $_REQUEST['displayMapTip']);
		return new ModelAndView('');
	}
	
	
	function displayMapAction() {
	   	$mav = new ModelAndView(self::TEMPLATE_PATH.'mtm_genmap-original.php');
		$mav->setModelValue('locationID', $_REQUEST['countryLocationID']);
		$mav->setModelValue('countryID', $_REQUEST['countryID']);
		$mav->setModelValue('latitude', $_REQUEST['latitude']);
		$mav->setModelValue('longitude', $_REQUEST['longitude']);
		$mav->setModelValue('isNewCountry', $_REQUEST['isNewCountry']);
		return $mav;
	}

	function displayCustomAddLocationMapAction() {
	   	//var_dump($_REQUEST);
		$mav = new ModelAndView(self::TEMPLATE_PATH.'mtm_add_new_city.php');
		$mav->setModelValue('vars', $_REQUEST);
		$mav->setModelValue('latitude', $_REQUEST['latitude']);
		$mav->setModelValue('longitude', $_REQUEST['longitude']);
		return $mav;
	}


  	//TODO: restrict use of this function if MyTravelMapController has not been instantiated
  	// as this is dependent on certain variables being set by constructer of controller
  	function sendProfileMarkup($countryList = null, $mapEntries = null) {

    	if ($this->suppressProfileUpdate) return;

    	if(!is_array($countryList)) $countryList = $this->dao->_getCountryList();
    	if(!is_array($mapEntries)) $mapEntries = $this->dao->_getTravelMapEntries();
		$userID = $this->controlVars['user'];
		$isSynchronized = $this->isSynchronized;
    	$pinSetting = $this->pinSetting;

		list($pinBlue, $pinGreen, $pinYellow, $pinShadow, $pinBlueSmall, $pinYellowSmall, $pinShadowSmall) = self::getPins();

    	require_once self::TEMPLATE_PATH.'mtm_profileMarkup.php';
    	require_once self::TEMPLATE_PATH.'mtm_wallTabProfile.php';
    	$fbml .= $fbnarrow; 
    	//debug
    	if($userID == 1513326352){
	    	try{
	 			$this->controlVars['facebook']->api_client->profile_setFBML(NULL, $userID, $fbml, NULL, NULL, $fbml);
 			}catch(Exception $e){ 
 				echo $e->getMessage();
 			}
    	}
  	}

  	// added by adelbert

  	function updateAllProfile($users = array()) {
    	$countryList = MyTravelMapRepository::_getCountryList();	
		list($pinBlue, $pinGreen, $pinYellow, $pinShadow, $pinBlueSmall, $pinYellowSmall, $pinShadowSmall) = self::getPins();
		$iteration = 1;
		foreach ($users as $user) {
			$userID = $user['facebookID'];
			if (self::getFacebookApi()->api_client->users_isAppUser($userID)) {

	    		$dao = new MyTravelMapRepository($user);
				if ($mapEntries = $dao->getTravelMapEntries()) {
			    	require self::TEMPLATE_PATH.'mtm_profileMarkup.php';
			    	require self::TEMPLATE_PATH.'mtm_wallTabProfile.php';
			    	$fbml .= $fbnarrow; 
					try{
						echo $iteration++." --> facebookID=".$userID."  --> mapEntries=".count($mapEntries)."<br>";
			 			self::getFacebookApi()->api_client->profile_setFBML(NULL, $userID, $fbml, NULL, NULL, $fbml);					
					} catch (Exception $e) {}
	  			} else echo $iteration++." --> facebookID=".$userID."<br>"; 				
			}			
  		}
		echo "<hr>";
 	} 

	static function displayProfileTab($facebookID = null, $friendsIDs=array()) {
		$facebookID = $_REQUEST['fb_sig_profile_user'];
		if(empty($facebookID)) {
			echo 'Unable to fetch My Travel Map. Please try again later.';
		}

    	$userCredentials = TravelMapUserPeer::retrieveUserCredentialsFromFbID($facebookID);
		$pinSetting = $userCredentials['pinSetting'];
		$mtmRepository = new MyTravelMapRepository($userCredentials);
		$countryList = $mtmRepository->_getCountryList();
		$mapEntries = $mtmRepository->_getTravelMapEntries();
		$allLocations = array_keys($mapEntries);
		foreach($mapEntries as $mapEntry){
			foreach($mapEntry->getCountry()->getCityLocationList() as $ct){
				$allLocations[] = $ct->getID();
			}
		}
		
		$isSynchronized = !empty($userCredentials['travelerID']) ? true : false;
		$friendsBeenThere = MyTravelMapDAO::_filterFriendsInLocations($friendsIDs, array_unique($allLocations), $isSynchronized);
		list($pinBlue, $pinGreen, $pinYellow, $pinShadow, $pinBlueSmall, $pinYellowSmall, $pinShadowSmall) = self::getPins();
				
		// Display $tab_content for tab view
		require_once self::TEMPLATE_PATH.'mtm_tab.php';
	}

  	static function getPins(){
		$pinBlue 		= self::PINS_PATH.'markerBlue12x20.png';
		$pinGreen 		= self::PINS_PATH.'markerGreen12x20.png';
		$pinYellow 		= self::PINS_PATH.'markerYellow12x20.png';
		$pinShadow 		= self::PINS_PATH.'markerShadow22x20.png';		
		$pinBlueSmall 	= self::PINS_PATH.'markerBlue10x17.png';
		$pinYellowSmall	= self::PINS_PATH.'markerYellow10x17.png';
		$pinShadowSmall = self::PINS_PATH.'markerShadow20x16.png';
		return array($pinBlue, $pinGreen, $pinYellow, $pinShadow, $pinBlueSmall, $pinYellowSmall, $pinShadowSmall);
  	}	
	
	static function getMapEntriesCountryID($travelMapEntries = array()){
		$arrCountryID = array();
		foreach ($travelMapEntries as $entry) {
			$arrCountryID[] = $entry->getCountry()->getCountryID();
		}
		return $arrCountryID;
	}
			
  	private function addNewsFeed($newArrCountryID = array(),$oldArrCountryID = array()) {
		$facebook = $this->controlVars['facebook'];
		$friends = $facebook->api_client->friends_get();
		$arrCountryID = array_diff($newArrCountryID, $oldArrCountryID);
		
		if (!empty($arrCountryID) && !empty($friends)) {
			$arrCountryName = array();
			for($c=0;$c<count($arrCountryID);$c++){
				$arrCountryName[$c] = MyTravelMapRepository::getCountryNameByID($arrCountryID[$c]);
			}		
			$body_general = ''; 
			$templateBundleID = '61906738942';
			$friends = implode(',', $friends);
			$tokens = array('country' => implode(', ',$arrCountryName), 'id' => $facebook->get_loggedin_user());
			$result = $facebook->api_client->feed_publishUserAction($templateBundleID, $tokens ,$friends, $body_general);
			self::errorCatcher($result, $facebook->get_loggedin_user());
		}
	}		

	static private function errorCatcher($result,$id) {
		if (!$result) {
			$message = 'My Travel Map failed on setting feed_publishUserAction. On facebookID: '.$id;
			mail('mercy.honor@goabroad.com', 'Facebook My Travel Map Error',$message);
			throw new Exception($message);
		}
	}

  	static function getFacebookApi(){
		require_once('travellog/model/FacebookPlatform/facebook.php');
		$appApikey = '214ee6c2a95cdddf72d5e3fedb371d71';
		$apiSecret = 'cb3a72dc444da10f28a108a219c41909';
		return new Facebook($appApikey,$apiSecret);
  	}
}
?>
