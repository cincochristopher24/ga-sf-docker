<?php
require_once 'travellog/model/socialApps/Class.SynchronizeApps.php';      
class SynchronizeAppsController {
	
	public $args;
	 
	const 
		SYNCHRONIZE 		   = 'sync',
		// trapper messages
		FAILED_SYNC			   = 'Failed synchronization proccess.',
		NON_GA_MEMBER		   = 'Not yet a GoAbroad Network Member.',
		INACTIVE_USER		   = 'Your GoAbroad.net Account is inactive.',
		SUSPENDED_USER		   = 'Your GoAbroad.net account is suspended.',
		NONE_TRAVELER_ACCOUNT  = 'Sorry, only traveler account can be synchronized.',
		ACCOUNT_ALREADY_SYNCED = 'GoAbroad.net Account was already synchronized with another user.',
		USING_OWN_ACCOUNT 	   = 'You are currently synchronized with that GoAbroad.net Account.';

	function __construct($args = array()){
		$this->args = $args;
	}
	
	function performAction($action){

		switch($action){

			case self::SYNCHRONIZE:
				return $this->synchronize();
			break;

		}
	}
	
	function synchronize(){

		$sync = new SynchronizeApps($this->args);

		if($sync->isGanetUser()){

			if($sync->isActive()){

				if(!$sync->isSuspended()){

			//		if(!$sync->isAdvisor()){

						if(!$sync->isAlreadySynced()){							

								if($sync->synchronize()){

									$sync->isResynchronize() ? $sync->unsetContainerID() : $sync->deleteDummyUser();
									$sync->setUserID($sync->getUserIdByTravelerID());
									$sync->addUserInTblMyTravelJournals();

									return $sync;

	   							} else return self::FAILED_SYNC;								

						} else return $sync->isUsingOwnAccount() ? self::USING_OWN_ACCOUNT : self::ACCOUNT_ALREADY_SYNCED;

		  //  		} else return self::NONE_TRAVELER_ACCOUNT; 

				} else return self::SUSPENDED_USER;								

			} else return self::INACTIVE_USER;			

		} else return self::NON_GA_MEMBER;
		
	}	
}
?>