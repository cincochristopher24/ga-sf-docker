<?php

require_once('travellog/model/socialApps/Class.fbNeedToUpdateAccountPeer.php');
require_once('travellog/model/socialApps/Class.fbNeedToUpdateAccount.php');

class fbNeedToUpdateAccountController{
	
	const MTB = 1, 
		  MTM = 2,
		  MTP = 3,
		  MTJ = 4;
	static function saveNewlyEditedAccount($travelerID, $application=0){

		if(fbNeedToUpdateAccountPeer::hasFacebookApps($travelerID, $application)){
			$validApplications = array(self::MTB, self::MTM, self::MTP, self::MTJ);
			if(in_array($application, $validApplications)){
				if($fbNeedToUpdateAccount = fbNeedToUpdateAccountPeer::isAccountExists($travelerID,$application)) {
					if($fbNeedToUpdateAccount->getStatus() == 1){
						$fbNeedToUpdateAccount->setStatus(0);
						$fbNeedToUpdateAccount->setDateUpdated(date('Y-m-d h-i-s'));
						$fbNeedToUpdateAccount->update();								
					}
				} else {
					$fbNeedToUpdateAccount = new fbNeedToUpdateAccount;
					$fbNeedToUpdateAccount->setTravelerID($travelerID);
					$fbNeedToUpdateAccount->setApplication($application);
					$fbNeedToUpdateAccount->setStatus(0);
					$fbNeedToUpdateAccount->setDateUpdated(date('Y-m-d h-i-s'));
					$fbNeedToUpdateAccount->create();
				}					
			}					
		}
	}

	static function updateNewlyEditedAccounts() {
		require_once('travellog/controller/socialApps/Class.FacebookProfileAppsUpdaterController.php');
		$fbNeedToUpdateAccounts = fbNeedToUpdateAccountPeer::getAllNewlyEditedAccounts();
		
		foreach ($fbNeedToUpdateAccounts as $fbNeedToUpdateAccount) {
			$travelerID = $fbNeedToUpdateAccount->getTravelerID();

			switch($fbNeedToUpdateAccount->getApplication()) {
				case self::MTB :
					FacebookProfileAppsUpdaterController::updateMyTravelBioStaticViewsInFacebook($travelerID);
				    break;
				case self::MTM :
					FacebookProfileAppsUpdaterController::updateMyTravelMapStaticViewsInFacebook($travelerID);
				    break;
				case self::MTP :
					FacebookProfileAppsUpdaterController::updateMyTravelPlansStaticViewsInFacebook($travelerID);
					FacebookProfileAppsUpdaterController::updateMyTravelMapStaticViewsInFacebook($travelerID);
				    break;
				case self::MTJ :
					FacebookProfileAppsUpdaterController::updateMyTravelJournalsStaticViewsInFacebook($travelerID);
					FacebookProfileAppsUpdaterController::updateMyTravelMapStaticViewsInFacebook($travelerID);
				    break;
			}			

			$fbNeedToUpdateAccount->setStatus(1);
			$fbNeedToUpdateAccount->setDateUpdated(date('Y-m-d h-i-s'));
			$fbNeedToUpdateAccount->update();						
		}
	}	
}
?>