<?php
	/**
	 * @(#) Class.GANETJournalDraftController.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - April 27, 2009
	 */
	
	require_once('travellog/controller/Class.AbstractJournalDraftController.php');
	
	/**
	 * Ganet journal draft controller.
	 */
	class GANETJournalDraftController extends AbstractJournalDraftController {
		
	}
	
