<?php
require_once('travellog/controller/Class.AbstractEntryController.php');
class GANETEntryController extends AbstractEntryController{
	
	function performAction(){
		$this->data           = array_merge($_GET, $_POST);
		$this->data['action'] = ( isset($this->data['action']))? $this->data['action'] : 'view'; 
		parent::__applyRules();
		parent::performAction(); 
	}
}
?>
