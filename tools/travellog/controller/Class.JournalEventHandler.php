<?php
/*
 * Created on 05 30, 08
 * 
 * Purpose: handles journal/journal entry events notification 
 * 
 */
 
 require_once("travellog/controller/Class.gaIObserver.php");
 
 class JournalEventHandler implements gaIObserver {
 	function notify(gaAbstractEvent $evt){
		$props = $evt->getData();
		switch($props['action']){
			case gaActionType::$SAVE:
				$this->processSaveNewJournalEntry($props);
			break; 
		}
	}
	
	// actions:
	// 		- if parent ofjournal entry is owned by an AdminGroup then it should automatically be featured
	function processSaveNewJournalEntry($props){
		require_once('travellog/model/Class.Travel.php');
		require_once('travellog/model/Class.Featured.php');
		$travel = new Travel($props['travelID']);
		$owner = $travel->getOwner();
		$travelID = $props['travelID'];		
		if ($owner instanceof AdminGroup){
			$groupID =  $owner->getGroupID();	
			if (!Featured::isFeaturedTravelInGroup($groupID,$travelID)){		
		 		Featured::setFeaturedTravelInGroup($groupID,$travelID);
		 	}
			
		}	
	}
 	 	
 	
 }
 
 
?>
