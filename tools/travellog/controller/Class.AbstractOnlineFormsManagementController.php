<?php
	require_once("travellog/model/Class.SessionManager.php");
	require_once('travellog/factory/Class.FileFactory.php');
	
	abstract class AbstractOnlineFormsManagementController
	{
		protected $obj_session 		= NULL;
		protected $file_factory 	= NULL;
		
		protected $loggedTraveler 	= NULL;
		protected $pageTitle 		= 'Online Forms';
		
		public function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/views'));
			$this->obj_session  = SessionManager::getInstance();
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			$this->file_factory->registerClass('ToolMan', array('path'=>''));
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('AdminGroup');
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('GaOnlineForm',array('path'=>'travellog/model/gaOnlineForms/'));
			$this->file_factory->registerClass('Template', array('path'=>''));
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('Paging', array('path'=>''));
			$this->file_factory->registerClass('ObjectIterator', array('path'=>''));
		}
		
		
		public function performAction(){
			//check if the user is logged
			$travelerID = $this->obj_session->get('travelerID');
			if(!$travelerID){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/login.php'));
			}
			//check if the user is an administrator
			$this->loggedTraveler = $this->file_factory->getClass('Traveler',array($travelerID));
			$oGroupFactory = $this->file_factory->invokeStaticClass('GroupFactory','instance');
			$oGroup = $oGroupFactory->create($this->file_factory->invokeStaticClass('AdminGroup','getAdvisorGroupID',array($this->loggedTraveler->getTravelerID())));
			
			if(!$this->loggedTraveler->isAdvisor()){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			
			//make sure that the logged traveler is the owner of the form if a formID has been provided
			$frmID = (array_key_exists('frmID',$_GET) && is_numeric($_GET['frmID']))?$_GET['frmID']:0;
			if($frmID != 0){
				//check if the formID exists
				$oForm = $this->file_factory->invokeStaticClass('GaOnlineForm','getFormByOnlineFormID',array($frmID));
				if(is_null($oForm) || $oForm->getGroupID() != $oGroup->getGroupID()) $this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}	
			$method = array_key_exists('method',$_GET)?$_GET['method']:'view';
			$oForm = isset($oForm) ? $oForm : NULL;
			$this->$method($oForm);
		}
		
		protected function view(){
			//get the administered group of the logged traveler
			$oGroupFactory = $this->file_factory->invokeStaticClass('GroupFactory','instance');
			$oGroup = $oGroupFactory->create($this->file_factory->invokeStaticClass('AdminGroup','getAdvisorGroupID',array($this->loggedTraveler->getTravelerID())));
			//$arSurveyForms = $this->file_factory->invokeStaticClass('SurveyForm','getGroupSurveyForms',array($oGroup->getGroupID()));
			$arOnlineForms = array_values($this->file_factory->invokeStaticClass('GaOnlineForm','getFormsByGroupID',array($oGroup->getGroupID())));
			$page = (isset($_GET['page']) && is_numeric($_GET['page']))?$page = $_GET['page']:$page=1;
			$paging =  $this->file_factory->getClass('Paging',array(count($arOnlineForms),$page,'',10));
			$iterator = $this->file_factory->getClass('ObjectIterator',array($arOnlineForms,$paging));
			$this->showTemplate(array(
				'groupFactory'	=> $oGroupFactory,
				'group'			=> $oGroup,
				'onlineForms'	=> $arOnlineForms,
				'paging'		=> $paging,
				'iterator'		=> $iterator
			));
		}
		
		protected function delete($form){
			$form->delete();
			$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/onlineforms/manage.php'));
		}
		
		protected function close($form){
			$form->close();
			$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/onlineforms/manage.php'));
		}
		
		protected function open($form){
			$form->open();
			$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/onlineforms/manage.php'));
		}
		
		protected function showTemplate($vars){
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			$tpl = $this->file_factory->getClass('Template');
			$tpl->set_path('travellog/views/onlineForms/');
			
			$subNavigation = $this->file_factory->getClass('SubNavigation');
			$subNavigation->setContext('GROUP');	
			$subNavigation->setContextID($vars['group']->getGroupID());
			$subNavigation->setGroupType('ADMIN_GROUP');
			$subNavigation->setLinkToHighlight('ONLINE_FORMS');
			
			$vars['subNavigation'] 	= $subNavigation;
			$vars['pageTitle']		= $this->pageTitle;	
			
			$tpl->setVars($vars);
			$tpl->out('tpl.Manage.php');
		}
	}
?>