<?php
/**
 * <b>Event</b> controller.
 * @author Aldwin S. Sabornido
 * @version 2.0 PHP5
 * @see http://www.GoAbroad.com/
 * @copyright 2006-2007
 */
require_once("travellog/model/Class.GroupEvent.php");
require_once("Class.Template.php");
require_once("Class.Date.php");

class EventController
{
	private 
	
	$travelerID        = NULL,
	
	$eventID           = NULL,
	
	$groupID           = NULL,
	
	$errors            = NULL,
	
	$obj_main_template = NULL,
	
	$obj_event         = NULL,
	
	$obj_date          = NULL;
		
	function __construct()
	{
		$obj_main_template = new Template();
		$obj_event	       = new GroupEvent();
		$obj_date          = ClassDate::instance();
	}
	
	public static function Add( $formvars = array() )
	{
		$obj_main_template = new Template();
		$obj_form_template = clone($obj_main_template);
		
		$obj_form_template->set( 'post'          , $formvars               );
		$obj_form_template->set( 'url_action'    , 'event.php?action=save' );
		$obj_form_template->set( 'button'        , 'Add'                   );
		
		$obj_main_template->set( 'form_template' , $obj_form_template->fetch('travellog/views/tpl.FrmEvent1.php') );
		$obj_main_template->set( 'errors'        , $this->errors           );
		$obj_main_template->set( 'main_title'    , 'Add Event'             );
		
		$obj_main_template->out( 'travellog/views/tpl.Event1.php' );
	}
	
	/*public static function Save()
	{
		$event = new GroupEvent();
		$date  = ClassDate::instance();
		
		$event->setGroupID     ( self::$groupID                         );
		$event->setTitle       ( self::$title                           );
		$event->setTheDate     ( $date->CreateODBCDate(self::$date,'/') );
		$event->setDescription ( self::$description                     );
		$event->Create();
	}
	
	public static function Delete()
	{
		$event = new GroupEvent( self::$eventID );
		$event->Delete();
	}
	
	public static function Edit()
	{
		$main_template = new Template();
		$form_template = new Template();
		$event         = new GroupEvent( self::$eventID );
		$date          = ClassDate::instance();
		
		$form_template->set( 'hidGroupID'     , self::$groupID               );
		$form_template->set( 'hidEventID'     , self::$eventID               );
		$form_template->set( 'url_action'     , 'event.php?action=update'    );
		$form_template->set( 'button'         , 'Update'                     );
		
		if ( count( self::$errors ) )
		{
			$form_template->set( 'txtTitle'       , self::$title             );
			$form_template->set( 'txtDate'        , self::$date              );
			$form_template->set( 'txaDescription' , self::$description       );
		}
		else
		{
			$form_template->set( 'txtTitle'       , $event->getTitle()       );
			$form_template->set( 'txtDate'        , $date->FormatSqlDate($event->getTheDate(),'-')     );
			$form_template->set( 'txaDescription' , $event->getDescription() );
			
		}
		
		$main_template->set( 'form_template'  , $form_template->fetch('travellog/views/tpl.FrmEvent.php') );
		$main_template->set( 'errors'         , self::$errors      );
		$main_template->set( 'main_title'     , 'Edit Event'       );
		
		$main_template->out('travellog/views/tpl.Event.php');
	}
	
	public static function Update()
	{
		$event = new GroupEvent( self::$eventID );
		$date  = ClassDate::instance();
		$event->setGroupID     ( self::$groupID                         );
		$event->setTitle       ( self::$title                           );
		$event->setTheDate     ( $date->CreateODBCDate(self::$date,'/') );
		$event->setDescription ( self::$description                     );
		$event->Update();
	}
	
	/**
	 * Setters and Getters
	 */
	public static function setTravelerID( $travelerID )
	{
		self::$travelerID = $travelerID;
	}
	public static function getTravelerID()
	{
		return self::$travelerID;
	}
	
	public static function setEventID( $eventID )
	{
		self::$eventID = $eventID;
	}
	public static function getEventID()
	{
		return self::$eventID;
	}
	
	public static function setGroupID( $groupID )
	{
		self::$groupID = $groupID;
	}
	public static function getGroupID()
	{
		return self::$groupID;
	}
	
	public static function setTitle( $title )
	{
		self::$title = $title;
	}
	public static function getTitle()
	{
		return self::$title;
	}
	
	public static function setDate( $date )
	{
		self::$date = $date;
	}
	public static function getDate()
	{
		return self::$date;
	}
	
	public static function setDescription( $description )
	{
		self::$description = $description;
	}
	public static function getDescription()
	{
		return self::$description;
	}
	
	public static function setErrors( $errors )
	{
		self::$errors = $errors;
	}
	public static function getErrors()
	{
		return self::$errors;
	}
}
?>
