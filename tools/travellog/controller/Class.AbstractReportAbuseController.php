<?php

	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/controller/Class.IController.php");
	require_once("travellog/factory/Class.FileFactory.php");
	require_once("class.phpmailer.php");
	
	abstract class AbstractReportAbuseController implements iController{
		
		protected 

		$obj_session  = NULL,

		$file_factory = NULL,
		
		$loggedUser  = NULL,
		
		$isLogged	  = false,
		
		$isMemberLogged = false,
		
		$isAdminLogged = false,
		
		$arrReportOptions = array(),
		
		$arrErrCode	=	array(),
		
		$messageBody	=	array(),
		
		$tpl			= NULL;
	
		
		function __construct(){
			
			$this->obj_session = SessionManager::getInstance();
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template' , array ( 'path' => '' ));
			$this->file_factory->registerClass('Captcha',array('path'=>'captcha/'));
			
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('TravelerProfile');
			
			$this->file_factory->registerClass('Article');
			$this->file_factory->registerClass('TravelLog');
			$this->file_factory->registerClass('PhotoAlbum');
			$this->file_factory->registerClass('Photo');
			$this->file_factory->registerClass('GroupFactory');
			
			$this->file_factory->registerClass('Travel');
			$this->file_factory->registerClass('PersonalMessage');
			
			$this->file_factory->registerTemplate('LayoutMain');
			
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
			
			$this->file_factory->getClass("Template");
			Template::includeDependentCss("/css/vsgStandardized.css");
			Template::includeDependentCss("/css/modalBox.css");
			Template::includeDependentCss('/css/Thickbox.css');
			Template::includeDependentJs('/js/thickbox.js');
		}
		
		function performAction(){
		
			$params = array();
			$params['action'] = ( isset($_POST['btnSubmit']))? 'Submit' : 'ViewForm';
			
			$params['tID']  = ( isset($_POST['tID']))? $_POST['tID'] : 0 ;
			$params['tID']  = ( isset($_GET['tID']))? $_GET['tID'] : $params['tID'];

			$params['msgID']  = ( isset($_POST['msgID']))? $_POST['msgID'] : 0 ;
			$params['msgID']  = ( isset($_GET['msgID']))? $_GET['msgID'] : $params['msgID'];
			
			$params['pID']  = ( isset($_POST['pID']))? $_POST['pID'] : 0 ;
			$params['pID']  = ( isset($_GET['pID']))? $_GET['pID'] : $params['pID'];
			
			$params['jID']  = ( isset($_POST['jID']))? $_POST['jID'] : 0 ;
			$params['jID']  = ( isset($_GET['jID']))? $_GET['jID'] : $params['jID'];
			
			$params['jeID']  = ( isset($_POST['jeID']))? $_POST['jeID'] : 0 ;
			$params['jeID']  = ( isset($_GET['jeID']))? $_GET['jeID'] : $params['jeID'];
			
			$params['aID']  = ( isset($_POST['aID']))? $_POST['aID'] : 0 ;
			$params['aID']  = ( isset($_GET['aID']))? $_GET['aID'] : $params['aID'];
			
			$params['cat']  = ( isset($_POST['cat']))? $_POST['cat'] : 0 ;
			$params['cat']  = ( isset($_GET['cat']))? $_GET['cat'] : $params['cat'];
			
			$params['genID']  = ( isset($_POST['genID']))? $_POST['genID'] : 0 ;
			$params['genID']  = ( isset($_GET['genID']))? $_GET['genID'] : $params['genID'];
			
			//$this->_allowAdminPrivileges(isset($this->group)); // if group is not set, redirect
		
			switch($params['action']){

				case "Submit":
					
					if ($this->_isValidReportToken($params)) {
						if (0 < count($this->_validateAbuseForm($params))) {
							$params['action'] = 'ViewForm';
							$this->_viewAbuseForm($params);		
						}							
						else {
							$this->_createAbuseReport($params);
							$this->_sendAbuseReport($params);
						}
					}
					elseif (0 < $params['msgID']) {
						if (0 < count($this->_validateAbuseForm_Message($params))) {
							$params['action'] = 'ViewForm';
							$this->_viewAbuseForm($params);	
						}
						else {
							$this->_createAbuseReport_Message($params);			
							$this->_sendAbuseReport($params);
						}
					}
					break;

				default:
				
					$this->_viewAbuseForm($params);
								
			}		
		
		}
		
		
		function _defineCommonAttributes($_params = array()){
			
			//check if user is currently logged in
			if ($this->obj_session->get('travelerID') > 0) {
				$this->isLogged		=	true ;
				$this->loggedUser = $this->file_factory->getClass ('Traveler',array($this->obj_session->get('travelerID')));
			
			}
		
			// create array for report abuse options/checklist	
				$this->arrReportOptions[0] = 'Profile details' ;
				$this->arrReportOptions[1] = 'Profile photo' ;
				$this->arrReportOptions[2] = 'Profile photo caption' ;
				$this->arrReportOptions[3] = 'Journals' ;
				$this->arrReportOptions[4] = 'Journal photo' ;
				$this->arrReportOptions[5] = 'Journal photo caption' ;
				$this->arrReportOptions[6] = 'Comment/s that user left on your profile' ;
				$this->arrReportOptions[7] = "Comment/s that user left on someone else's profile";
				$this->arrReportOptions[8] = 'Private message that user sent you' ;
				$this->arrReportOptions[9] = 'Article' ;
			
			// create array for message body elements
				$this->messageBody['txtAbuse'] = '';
				$this->messageBody['txtAbuseQ&A'] = '';
				$this->messageBody['details'] = '';
				
			//  create generic template
				$this->tpl = $this->file_factory->getClass('Template');
				$this->tpl->set_path("travellog/views/");
				
				$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain'))); 
				
		}
		
		function _allowAdminPrivileges($_privilege = false){
			if (!$_privilege){
				header("location:index.php");
				exit;
			}
		}
	
		function _isValidReportToken($_params = array() ) {
			
			if (0 < $_params['tID'] || 0 < $_params['pID'] || 0 < $_params['jID'] || 0 < $_params['jeID'] || 0 < $_params['aID'] || ( 0 < strlen($_params['cat']) && 0 < $_params['genID'] ))
				return true;
			return false;
		}
		
		
		function _validateAbuseForm($_params = array() ) {
			
			if (!isset($_POST['chkreport']) && 0 == strlen($_POST['txtOther'])):
				$this->arrErrCode['UserOffense'] = "<i style=\"color:red\">Please choose answer.</i><br />";							
			endif;
	
			if(isset($_POST['email']) && strlen($_POST['email']) && !phpmailer::check_email_address($_POST['email'])){
				$this->arrErrCode['ValidEmail'] = " <i style=\"color:red\">Please type in a valid email address or you can leave this field blank.</i>";
			}	

			if (0 == strlen($_POST['txtContent'])):						
				$this->arrErrCode['ContentOffense'] = "<i style=\"color:red\">Please indicate reason.</i><br />";
			endif;
	
			if(!($this->file_factory->invokeStaticClass('Captcha', 'validate', array($_POST['encSecCode'],$_POST['securityCode'])))):
				$this->arrErrCode['MsgCaptcha'] = " <i style=\"color:red\">Invalid security code.</i>";
			endif;
		
			return $this->arrErrCode;
		
		}
		
		
		function _validateAbuseForm_Message($_params = array() ) {
		
			if (0 == strlen($_POST['txtMessage'])):						
				$this->arrErrCode['MsgOffense'] = "<i style=\"color:red\">Please indicate how message was offensive.</i><br />";
			endif;
			
			if(!($this->file_factory->invokeStaticClass('Captcha', 'validate', array($_POST['encSecCode'],$_POST['securityCode'])))):
				$this->arrErrCode['MsgCaptcha'] = " <i style=\"color:red\">Invalid security code.</i>";
			endif;
		
			return $this->arrErrCode;
		
		}
		
		
		function _createAbuseReport($_params = array () ) {
			
			$this->_defineCommonAttributes($_params);
			
			if (0 < $_params['pID'])
				$this->_createAbuseReport_Photo($_params);
			elseif (0 < $_params['jID'])
				$this->_createAbuseReport_Journal($_params);	
			elseif (0 < $_params['jeID'])
				$this->_createAbuseReport_JournalEntry($_params);
			elseif (0 < $_params['tID'])
				$this->_createAbuseReport_Traveler($_params);
			elseif (0 < $_params['aID'])
				$this->_createAbuseReport_Article($_params);	
			
			if (isset($_POST['chkreport']) || strlen($_POST['txtOther'])) {
				
				$this->messageBody['txtAbuseQ&A'] = '<br />What do you find offensive about this user? <br />' ;
				if (isset($_POST['chkreport'])):
					foreach($_POST['chkreport'] as $each):
						$this->messageBody['txtAbuseQ&A'] .= " - " . $this->arrReportOptions[$each] . "<br />";
					endforeach;
				endif;
				
				if (strlen($_POST['txtOther']) && isset($_POST['chkOther'])):
					$this->messageBody['txtAbuseQ&A'] .= " - " . $_POST['txtOther']. "<br />"; 
				endif;
					 
			}
			
			if (0 < strlen($_POST['txtContent']))				
				$this->messageBody['txtAbuseQ&A'] .= '<br />Why do you find the content offensive? <br />' . $_POST['txtContent'];
			
		}
		
		
		function _createAbuseReport_Message($_params = array() ) {
			
			$this->_defineCommonAttributes($_params);
			
			$nMessage = $this->file_factory->getClass('PersonalMessage', array($_params['msgID']));

			$this->messageBody['txtAbuse']  = "Report inappropriate message on " . "<a href='http://" . $_SERVER['SERVER_NAME'] . "/" . $nMessage->getSource()->getUserName() . "'>" . $nMessage->getSource()->getUserName() . "</a>";	

			$this->messageBody['details'] = "<br /><br /><b>Message Details: </b> <br />" ;
			$this->messageBody['details'] .= "From: " . $nMessage->getSource()->getUserName() . "<br />" . "Subject: " . $nMessage->getTitle() . "<br />" ;
			$this->messageBody['details'] .= "Message: " . $nMessage->getText() ;
			
			if (strlen($_POST['txtMessage']))					
				$this->messageBody['txtAbuseQ&A']  .= '<br />Why do you find this message offensive? <br />' . $_POST['txtMessage'];
					
			
		}
		
		
		function _createAbuseReport_Photo($_params = array() ) {
			
			/*********************************
			 * edits made by neri: 11-05-08
			 * changed $params to $_params
			 * changed $pID to $_params['pID'] 
			 *********************************/
			
			switch(strtolower($_params['cat'])) {
			//switch(strtolower($params['cat'])) {
				
				case 'travellog' :
					
					$_context = $this->file_factory->getClass('TravelLog', array($_params['genID'])) ;
					$this->messageBody['txtAbuse'] = "Report user on " . "<a href='http://" . $_SERVER['SERVER_NAME'] . "/profile.php?action=view&travelerID=" . $_context->getTravelerID() . "'>" . $_context->getTraveler()->getUserName() . "</a>";
					
					break;
				
				case 'profile':
	
					$_context = $this->file_factory->getClass('TravelerProfile', array($_params['genID'])) ;
					$this->messageBody['txtAbuse'] = "Report user on " . "<a href='http://" . $_SERVER['SERVER_NAME'] . "/profile.php?action=view&travelerID=" . $_context->getTravelerID() . "'>" . $_context->getUserName() . "</a>";
					break;
					
				case 'photoalbum':
					
					$_context = $this->file_factory->getClass('PhotoAlbum', array($_params['genID'])) ;	
					$this->messageBody['txtAbuse'] = "Report group on " . "<a href='http://" . $_SERVER['SERVER_NAME'] . "/group.php?gID=" . $_context->getGroup()->getGroupID() . "'>" . $_context->getGroup()->getName() . "</a>";	
					
					break;
					
				case 'fungroup':
				
					$factory 	=	$this->file_factory->invokeStaticClass('GroupFactory','instance');
					$_context	=	$factory->create( array($_params['genID']) );
					$this->messageBody['txtAbuse'] = "Report club on " . "<a href='http://" . $_SERVER['SERVER_NAME'] . "/group.php?gID=" . $_context[0]->getGroupID() . "'>" . $_context[0]->getName() . "</a>";
					
					break;
				
				default:
				
					
			}
			
			
			//$photo = $this->file_factory->getClass('Photo' , array($_context[0], $pID) );
			$photo = $this->file_factory->getClass('Photo' , array($_context[0], $_params['pID']) ); 
			
			$this->messageBody['details'] = "<br /><br /><b>Photo Details: </b> <br />" ;								
			$this->messageBody['details'] .= "Caption: " . $photo->getCaption() .  "<br>";
			$this->messageBody['details'] .= "Image: " . "<a href='http://" . $_SERVER['SERVER_NAME'] . "/photomanagement.php?cat=" . strtolower($params['cat']) . "&amp;action=vfullsize&genID=" . $params['genID'] . "&amp;photoID=" . $params['pID'] . "'> view image</a>";
			
			
		}
		
		
		function _createAbuseReport_Journal($_params = array() ) {
			
			/*****************************************************
			 * changed parameter $jID to $_params['jID']
			 * changed $params to $_params
			 * edits by neri: 11-05-08
			 *****************************************************/
			
			$journal = $this->file_factory->getClass('Travel', array($_params['jID']));
			//$journal = $this->file_factory->getClass('Travel', array($jID));
			
			$journalOwner = $journal->getOwner();
			if ($journalOwner instanceof Traveler)
				
				$this->messageBody['txtAbuse'] = "Report user on " . "<a href='http://" . $_SERVER['SERVER_NAME'] . "/" . $journalOwner->getUserName() . "'>" . $journalOwner->getUserName() . "</a>";
			
			elseif ($journalOwner instanceof Group)
				
				$this->messageBody['txtAbuse'] = "Report user on " . "<a href='http://" . $_SERVER['SERVER_NAME'] . "/group.php?gID=" . $journalOwner->getGroupID() . "'>" . $journalOwner->getName() . "</a>";
		
			//$this->messageBody['details'] = "<br /><br />Journal: " . "<a href='http://" . $_SERVER['SERVER_NAME'] . "/journal-entry.php?action=view&travelID=" . $params['jID'] . "'>" . $journal->getTitle() . "</a>";
			$this->messageBody['details'] = "<br /><br />Journal: " . "<a href='http://" . $_SERVER['SERVER_NAME'] . "/journal-entry.php?action=view&travelID=" . $_params['jID'] . "'>" . $journal->getTitle() . "</a>";			
		}
		
		function _createAbuseReport_JournalEntry($_params = array() ) {
			
			$journalentry = $this->file_factory->getClass('TravelLog', array($_params['jeID']));
			
			$journalOwner = $journalentry->getTravel()->getOwner();
			if ($journalOwner instanceof Traveler)
				
				$this->messageBody['txtAbuse'] = "Report user on " . "<a href='http://" . $_SERVER['SERVER_NAME'] . "/" . $journalOwner->getUserName() . "'>" . $journalOwner->getUserName() . "</a>";
			
			elseif ($journalOwner instanceof Group)
				
				$this->messageBody['txtAbuse'] = "Report user on " . "<a href='http://" . $_SERVER['SERVER_NAME'] . "/group.php?gID=" . $journalOwner->getGroupID() . "'>" . $journalOwner->getName() . "</a>";
				
			$this->messageBody['details'] = "<br /><br />Journal: " . "<a href='http://" . $_SERVER['SERVER_NAME'] . "/journal-entry.php?action=view&travellogID=" . $_params['jeID'] . "'>" . $journalentry->getTitle() . "</a>";
			
			
		}
		

		function _createAbuseReport_Traveler($_params = array() ) {
			
			$reportedTraveler = $this->file_factory->getClass('Traveler', array($_params['tID']));	
			
			$this->messageBody['txtAbuse']  = "Report user on " . "<a href='http://" . $_SERVER['SERVER_NAME'] . "/" . $reportedTraveler->getUserName() . "'>" . $reportedTraveler->getUserName() . "</a>";
		}
		
		function _createAbuseReport_Article($_params = array() ) {
			
			$article = $this->file_factory->getClass('Article', array($_params['aID']));
			
			$articleOwner = $article->getOwner();
			if ($articleOwner instanceof Traveler)
				
				$this->messageBody['txtAbuse'] = "Report user on " . "<a href='http://" . $_SERVER['SERVER_NAME'] . "/" . $articleOwner->getUserName() . "'>" . $articleOwner->getUserName() . "</a>";
			
			elseif ($journalOwner instanceof Group)
				
				$this->messageBody['txtAbuse'] = "Report user on " . "<a href='http://" . $_SERVER['SERVER_NAME'] . "/group.php?gID=" . $articleOwner->getGroupID() . "'>" . $articleOwner->getName() . "</a>";
				
			$this->messageBody['details'] = "<br /><br />Article: " . "<a href='http://" . $_SERVER['SERVER_NAME'] . "/article.php?action=view&articleID=" . $_params['aID'] . "'>" . $article->getTitle() . "</a>";
			
			
		}
		
		function _sendAbuseReport($_params = array() ) {
			
			/*********************************************************************
			 * edits by neri: 11-04-08
			 * checks if server name is not local to avoid the error with smtp
			 * also added a referer once the message has been successfully sent
			 *********************************************************************/
			
			if (0 < $_params['tID']) {
				$traveler = $this->file_factory->getClass ('Traveler',array($_params['tID']));
				$referer = $traveler->getUserName()."'s Profile";
				
				// modified by neri to implement the friendly url: 11-07-08 
				$refererLink = '/'.$traveler->getUserName();
			}
			
			else if (0 < $_params['jeID']) {
				$journalEntry = $this->file_factory->getClass ('TravelLog',array($_params['jeID']));
				$referer = $journalEntry->getOwner()->getUserName()."'s Journal Entry";
				$refererLink = '/journal-entry.php?action=view&travellogID='.$journalEntry->getTravelLogID();
			}
			
			else if (0 < $_params['msgID']) {
				$message = $this->file_factory->getClass ('PersonalMessage',array($_params['msgID']));
				$referer = 'your messages.';
				$refererLink = '/messages.php?view&active=inbox&messageID='.$_params['msgID'];
			}
			
			else if (0 < $_params['aID']) {
				$article = $this->file_factory->getClass ('Article',array($_params['aID']));
				$owner = $article->getOwner();
				if($owner instanceof Group)
					$referer = $owner->getName()."'s Article";
				else
					$referer = $owner->getUserName()."'s Article";
				$refererLink = '/article.php?action=view&articleID='.$article->getArticleID();
			}
			
			if ('goabroad.net.local' != $_SERVER['SERVER_NAME']) {
				$mail = new PHPMailer(); 
				$mail->IsSMTP(); 
				$mail->IsHTML(true);
				
				if (isset($this->loggedUser)):
					$mail->FromName = $this->loggedUser->getTravelerProfile()->getUserName(); 
					$mail->From 	= $this->loggedUser->getTravelerProfile()->getEmail();
				elseif(strlen($_POST['email'])):
					$mail->FromName = $_POST['email'];
					$mail->From 	= $_POST['email'];
				else:
					$mail->FromName = "Report Abuse";
					$mail->From 	= "abuse@goabroad.net";
				endif;	
				
				if (isset($_SERVER['REMOTE_ADDR']))
					$this->messageBody['details'] .= "<br /><br /> IP Address: " . $_SERVER['REMOTE_ADDR'] ;
				
				if (isset($_SERVER['HTTP_USER_AGENT']))
					$this->messageBody['details'] .= "<br /><br /> User Agent: " . $_SERVER['HTTP_USER_AGENT'] ;
					
					
				$mail->Subject = "Report Abuse";
				
				$mail->Body = $this->messageBody['txtAbuse'] . "<br /><br />" .  $this->messageBody['txtAbuseQ&A'] . $this->messageBody['details'] ;
				
				$mail->AddAddress("abuse@goabroad.net"); 
				$mail->AddBCC("gapdaphne.notifs@gmail.com"); 
				
				if ($mail->Send())
					$msgSent = 'Your report has successfully been submitted. We will review your report and take the ' .
							   'necessary actions should we find a violation of the GoAbroad Network Terms of Use. <br /> ' .
							   'Thank you for helping us maintain the quality of the site.';
				else
					$msgSent = 'Sorry, the server has encountered an error in sending your report. Please try again.';
			}
			
			else {
				/*************************************************************************
				 * this is a temporary message only when the server in used is the local
				 * added by neri: 11-05-08
				 *************************************************************************/
				 
				$msgSent = 'Your report has successfully been submitted. We will review your report and take the ' .
						   'necessary actions should we find a violation of the GoAbroad Network Terms of Use. <br /> ' .
						   'Thank you for helping us maintain the quality of the site.'; 
			}
				
			$this->tpl->set('action', $_params['action']);
			$this->tpl->set('Msg', $msgSent);
			$this->tpl->set('referer', $referer);
			$this->tpl->set('refererLink', $refererLink);
			$this->tpl->out('tpl.ReportAbuse.php');
		}
		
		
		function _viewAbuseForm($_params = array() ) {
		
			$this->_defineCommonAttributes($_params);
			
			$this->_allowAdminPrivileges($this->_isValidReportToken($_params) || 0 < $_params['msgID']);
			
			$security = $this->file_factory->invokeStaticClass('Captcha','createImage' , array(array('width'=>200)) );
			
			$this->tpl->set('secCode',$security['code']);
			$this->tpl->set('secSource',$security['src']);

			$this->tpl->set('action', $_params['action']);
			$this->tpl->set('loggedUserID', $this->obj_session->get('travelerID'));
		
			(0 < $_params['tID']) 	? $this->tpl->set('tID', $_params['tID']) : '';
			(0 < $_params['msgID']) ? $this->tpl->set('msgID', $_params['msgID']) : '';
			(0 < $_params['pID']) 	? $this->tpl->set('pID', $_params['pID']) : '';
			(0 < $_params['jID']) 	? $this->tpl->set('tID', $_params['jID']) : '';
			(0 < $_params['jeID']) 	? $this->tpl->set('jeID', $_params['jeID']) : '';
			(0 < $_params['aID']) 	? $this->tpl->set('aID', $_params['aID']) : '';
			(0 < $_params['cat']) 	? $this->tpl->set('cat', $_params['cat']) : '';
			(0 < $_params['genID']) ? $this->tpl->set('genID', $_params['genID']) : '';
			
			(0 < isset($_POST)) ? $this->tpl->set('_POST', $_POST) : '';
			
			/*********************************************************************
			 * edits by neri: 11-07-08
			 * added a referer and its corresponding link for cancel
			 *********************************************************************/
			
			if (0 < $_params['tID']) {
				$traveler = $this->file_factory->getClass ('Traveler',array($_params['tID']));
				$refererLink = '/'.$traveler->getUserName();
			}
			
			else if (0 < $_params['jeID']) {
				$journalEntry = $this->file_factory->getClass ('TravelLog',array($_params['jeID']));
				$refererLink = '/journal-entry.php?action=view&travellogID='.$journalEntry->getTravelLogID();
			}
			
			else if (0 < $_params['msgID']) {
				$message = $this->file_factory->getClass ('PersonalMessage',array($_params['msgID']));
				$refererLink = '/messages.php?view&active=inbox&messageID='.$_params['msgID'];
			}
			
			else if (0 < $_params['aID']) {
				$article = $this->file_factory->getClass ('Article',array($_params['aID']));
				$refererLink = '/article.php?action=view&articleID='.$article->getArticleID();
			}
							
			$this->tpl->set('arrReportOptions', $this->arrReportOptions);
			$this->tpl->set('arrErrCode', $this->arrErrCode);
			$this->tpl->set('refererLink', $refererLink);
			
			$this->tpl->out('tpl.ReportAbuse.php');
		}
		
	}
?>