<?php
/*
 * Created on 11 14, 07
 * 
 * Author: K. Gordo
 * Purpose: Comments 'module' abstract controller 
 * 
 */
 
 require_once("travellog/controller/Class.IController.php");
 require_once("travellog/factory/Class.FileFactory.php");
 require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
 require_once('Class.GaString.php');
 require_once('captcha/Class.GaCaptcha.php');
 
 class AbstractCommentsController {
 	protected 
 	   $traveler,
 	   $travelerID,
 	   $referer,
 	   $photoCategory,
 	   $genID,
 	   $template,
 	   $contextID,
 	   $context,
 	   $templatePath,
 	   $comment = "",
 	   $action,
	   $withError = false;
 	
 	
 	function __construct(){
 		$this->file_factory = FileFactory::getInstance();
 		$this->file_factory->registerClass('Template',array('path'=>''));
 		$this->file_factory->registerClass('CommentsView',array('path'=>'travellog/views/'));
 		$this->file_factory->registerClass('TravelLog');
		$this->file_factory->registerClass('Article');
 		$this->file_factory->registerClass('Comment');
 		$this->file_factory->registerClass('Photo');
 		$this->file_factory->registerClass('TravelerProfile');
 		$this->file_factory->registerClass('SubNavigation');
 		$this->file_factory->registerClass('Traveler');
		$this->file_factory->registerClass('GroupFactory');
 		$this->file_factory->registerClass('SessionManager');
 		$this->file_factory->registerClass('Captcha', array('path' => 'captcha/'));
 		
 		$this->file_factory->registerTemplate('LayoutMain');		
		$this->file_factory->setPath('CSS', '/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');

		$this->networkName = "GoAbroad";
		//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
		
 	}
 	
 	/**
 	 * Retrieves the request data or the data from $_SESSION, $_REQUEST, $_GET and $_POST.
 	 * This also handles the setting of default values if a specific data is not passed or set.
 	 * 
 	 * @return void
 	 */
 	function fetchGPCValues(){
		$this->context = (isset($_REQUEST['context'])) ? $_REQUEST['context'] : "";
		$this->contextID = (isset($_REQUEST['contextID'])) ? $_REQUEST['contextID'] : 0;
		$this->photoCategory = (isset($_REQUEST['photocat'])) ? $_REQUEST['photocat'] : "";
		$this->genID = (isset($_REQUEST['genID'])) ? $_REQUEST['genID'] : "";
		$this->message = "";
		$this->action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : "";
    $this->name   = (isset($_REQUEST['name'])) ? trim($_REQUEST['name']) : "";
    $this->email  = (isset($_REQUEST['email'])) ? trim($_REQUEST['email']) : "";
 		$sessMan = $this->file_factory->invokeStaticClass('SessionManager','getInstance',array());
		$this->travelerID = $sessMan->get('travelerID');
		$this->isLogged = (0 == $this->travelerID) ? false : true;
    $this->comment = isset($_REQUEST['comment']) ? urldecode($_REQUEST['comment']) : "";
 		$this->pokeType = isset($_REQUEST['pokeType']) ? $_REQUEST['pokeType'] : "";
 		$this->photoCat = isset($_REQUEST['photoCat']) ? $_REQUEST['photoCat'] : "";
    
		if (isset($_SERVER['HTTP_REFERER']) && !strripos($_SERVER['HTTP_REFERER'],'comment.php') ) 
		  $this->referer = $_SERVER['HTTP_REFERER'];		
 	}
 	
 	/**
 	 * Handles the validation of data. If there are data inconsistincies, this method
 	 * also handles the page redirection.
 	 */
  function validateRequest(){
		
  	
  		// check if action is set, if not redirect
		if (!isset($_REQUEST['action'])) {
		  echo "1"; exit; // note do not delete this one. 
		}
		
		// check if the input captcha is valid
		$client_input = (isset($_REQUEST['securityCode'])) ? $_REQUEST['securityCode'] : "";
	  if(0 >= $this->travelerID AND !(GaCaptcha::validate($client_input))) {
	  	echo "3<<<>>>";
	  	$this->message = "Sorry, the security code you have entered is incorrect. Please try again!";
	  	$this->action = "view";
	    $this->withError = true;
	  }
	  GaCaptcha::clear();
	  // end of validating captcha
	  
		// if user is not logged in email and 
		if (("" == $this->email || "" == $this->name || !GaString::isValidEmailAddress($this->email)) AND !$this->isLogged) {
	  	echo "3<<<>>>";  // note do not delete this one. 
	  	$this->message = (GaString::isValidEmailAddress($this->email)) ? "" : "Email set is not valid. ";
	  	$this->message = ("" == $this->email AND "" == $this->message) ? "The email must be set! ": $this->message ;
	  	$this->message .= ("" == $this->name) ? "The name must be set! ": "";
	  	$this->action = "view";
	    $this->withError = true;
		}
		
		try {
		  $this->traveler = $this->file_factory->getClass('Traveler',array($this->travelerID));
		} 
		catch (Exception $e){}
		
		// there must be context and contextID in REQUEST if the action set is not summary
		if ((!isset($_REQUEST['context']) OR !isset($_REQUEST['contextID'])) AND 'summary' != $this->action) {
		 	echo "1"; exit; // note do not delete this one. 
		}
		
  }
 	
 	function performAction(){			 
 	
 		
 		$this->fetchGPCValues();
 		
 		$this->validateRequest();
 		
		$this->templatePath = 'travellog/views/';
		$this->template = $this->file_factory->getClass('Template');
		$this->template->set_path($this->templatePath);	
		 	
		$snav = $this->file_factory->getClass('SubNavigation');
		$snav->setContext('COMMENTS');	
		$snav->setContextID($this->travelerID);	
		$snav->setLinkToHighlight('COMMENTS');
		$this->template->set('snav',$snav);		
		 	
		Template::setMainTemplate($this->file_factory->getTemplate('LayoutMain'));
		Template::setMainTemplateVar('layoutID', 'main_pages');
		Template::setMainTemplateVar('page_location', 'My Passport');	
		 	
		// insert code for logger system
		require_once('gaLogs/Class.GaLogger.php');
		$travID = ($this->isLogged) ? $this->travelerID : -1;
	  GaLogger::create()->start($travID,'TRAVELER');
		
		switch ($this->action){
		  case 'add':
		 	  $this->doAdd();
		 		break;
		 	case 'edit':
		 		$this->doEdit();
		 		break;
		 	case 'view':
			 	$cv = $this->file_factory->getClass('CommentsView');
				$cv->setContext($this->context,$this->contextID);
				$cv->setTravelerID($this->travelerID);
				$cv->readComments();
				$cv->setConfirmationMessage($this->message);
				if($this->withError){
					$cv->showConfirmationMessageOnly(true);
					// !!IMPORTANT invalidate cache first to show confirmation message instead of comments
					$cv->invalidateCacheEntry();
				}
				echo $cv->renderAjax();
		 		break;
		 	case 'summary':
		 		$this->doSummary();
		 		break;	
		 	case 'delete':
		 		$this->doDelete();
		 		break;
		 	case 'savenew':
		 		$this->doSaveNew();
		 		break;	
		 	case 'saveedit':
		 		$this->doSaveEdit();
		 		break;	
		 	default:
		 		echo "1"; exit; // note do not delete this one. 
		 	}	
		 		
 	}
 	 	
 	/**************************************************************
 	 * function doEdit
 	 */
 	 
 	 function doEdit() {
 	 	 	 	
 	 	// check if contextID is set
 	 	if (!isset($this->contextID))
 	 		header("Location: /index.php");
 	 	
 	 	try { 	 	
 	 		//$comment = new Comment($this->contextID);
 	 		$comment = $this->file_factory->getClass('Comment',array($this->contextID));
 	 		//-- rule: only the author  can edit a comment
 	 		if ($this->travelerID != $comment->getAuthor()) 
 	 			header("Location: /index.php");
 	 													
 	 		$this->template->set("context",$this->context);	 	 		
	 		$this->template->set("contextID",$comment->getCommentID());						// for edits the contextID will be the commentID 
	 		$this->template->set("comment",$comment->getTheText());
	 		if (isset($referer))
	 			$this->template->set("referer",$this->referer);
	 		
	 		
	 		$snav = $this->file_factory->getClass('SubNavigation');
 			$snav->setContext('TRAVELER');	
 			$snav->setContextID($this->travelerID);
	 		$this->template->set('snav',$snav);
	 		$this->template->out('tpl.EditComment.php');
 	 		
 	 	}catch (Exception $e){}
 	 	 	 	 	
 	 }
 	 
 	 
  /**
   * handles the saving of a new comment.
   * 
   * @return void
   */
 	function doSaveNew(){
 		// invalidate cached views(html) of comments
 		require_once('Cache/ganetCacheProvider.php');
 		$cache	=	ganetCacheProvider::instance()->getCache();
 		$key 	=	'CommentsView_hashes_' . $this->context . '_' . $this->contextID;
 		if ($cache != null){
 			$hashKeys	=	$cache->get($key);
 			
 			if ($hashKeys){
 				// found array of keys to invalidate
 				foreach ($hashKeys as $hashKey)
 					$cache->delete($hashKey);
 			}
 			// delete also the hashKeys
 			$cache->delete($key);
 		}
 		
 		
 		$approved = Comment::UNAPPROVED;
 		$owner = null;
		switch ($this->context){
		  case CommentType::$TRAVELLOG:
			  $entry = new TravelLog($this->contextID);
			  $owner = $entry->getOwner();
		 	  break;
		  case CommentType::$PROFILE:
			  $owner = new Traveler($this->contextID);
			  break;
    	  case CommentType::$ARTICLE:
			  $article = $this->file_factory->getClass('Article',array($this->contextID));
			  $owner = $article->getOwner();	
		 	  break;
		  case CommentType::$VIDEO:
			  $video = new VideoAlbumToVideo($this->contextID);

			  if( 0 == (int) $video->getParentAlbum()->getAlbumID() ){
					$video = new TravelLogToVideo($this->contextID);
					$journalEntry = $video->getParentTravelLog();
					$owner = $journalEntry->getOwner();
			  }
			  else{
					$videoAlbum = $video->getParentAlbum();
					$owner = $videoAlbum->getOwner();
			  }
			  break;			
		}
		
		if ($owner instanceof Traveler) {
			$ownerID = $owner->getTravelerID();
		}
		else {
			$ownerID = $owner->getAdministratorID();
		}
	  		
 		// if the user is not logged in automatically set it as UNAPPROVED;
		if ($this->isLogged){
		  // For traveler owner of the photo/journal entry/traveler profile 
		  // where the comment was made we check if the logged in author of comment 
		  // is a friend of the owner. If not unapproved it.
		  // For admin group who is the owner of the photo/journal entry 
		  // where the comment was made we check if the logged in author of comment
		  // is a member of the admin group.

		  $poster = new Traveler($this->travelerID);
		
		  $posterIsAdvisor = false;
		  if($poster->isAdvisor()){//if the poster is an advisor of one of the owner's groups.
				if ($owner instanceof Traveler) {
					$ownerGroups = $owner->getGroups();
					foreach($ownerGroups as $iOwnerGroup){
						if($iOwnerGroup->getAdministratorID() == $poster->getTravelerID()){
							$posterIsAdvisor = true;
							break;
						}	
					}
				}
				else if ($owner instanceof AdminGroup) {
					if ($owner->getAdministratorID() == $poster->getTravelerID()) {
						$posterIsAdvisor = true;
					}
				}
		  }
	  	  if ((($owner instanceof Traveler) AND $owner->isFriend($this->travelerID)) // check if friend of the owner
	  	    	|| (($owner instanceof AdminGroup) AND $owner->isMember($poster)) // check if a member of the group
	  	    	|| ($ownerID === $this->travelerID) // check if the owner is posting to himself
	            || $posterIsAdvisor
			){
	  	    	$approved = Comment::APPROVED;
			    echo "2<<<>>>"; // note do not delete this one.
	  	  	}
	   }
		
		if (Comment::UNAPPROVED == $approved) {
			echo "3<<<>>>"; // note do not delete this one. 
			$this->message = "You have successfully added a shout-out! Your shout-out is subject for approval so we temporarily hide your shout-out.";
		}
 		
 		$commentObj = $this->file_factory->getClass('Comment');
		$commentObj->setAuthor($this->travelerID);
		$commentObj->setTheText($this->comment);
		$commentObj->setPokeType($this->pokeType);
		$commentObj->setCommentType($this->context);
		$commentObj->setCreated(date('Y-m-d G:i'));
		$commentObj->setEmail($this->email);
		$commentObj->setName($this->name);
		$commentObj->setIsApproved($approved);
		$commentObj->setFromSite($_SERVER['HTTP_HOST']);
		
		$page = "page";
		
		switch ($this->context){
 			case CommentType::$TRAVELLOG:
 				$tlog = $this->file_factory->getClass('TravelLog',array($this->contextID));
 				$tlog->addComment($commentObj);
				$page = "journal";
			 	break;
 			case CommentType::$PROFILE:
 				$profile = $this->file_factory->getClass('TravelerProfile',array($this->contextID));
 				$profile->addComment($commentObj);
 				break; 				
			case CommentType::$ARTICLE:
 				$article = $this->file_factory->getClass('Article',array($this->contextID));
 				$article->addComment($commentObj);
				$page = "article";
 				break;
			case CommentType::$VIDEO:
				$video = new VideoAlbumToVideo($this->contextID);

				if( 0 == (int) $video->getParentAlbum()->getAlbumID() ){
					$video = new TravelLogToVideo($this->contextID);
				}

				$video->addComment($commentObj);
				$page = "video";
				break;
 		}
 		
 		// create the appropriate message to the author of comment.
		if (Comment::UNAPPROVED == $approved) {
			$networkName = (isset($GLOBALS['CONFIG'])) ? $GLOBALS['CONFIG']->getSiteName() : "GoAbroad Network";
			echo "3<<<>>>"; // note do not delete this one.
			
			$this->message = sprintf("Posts are moderated by %s to conform with our " .
				"<a onclick=\"window.open('/shout-out-policy.php?network=$networkName', 'shout-out-policy', 'resizable=no,width=490,height=300,top=' + ((screen.width/2)- 1000) + ',left=20'); return false;\" href=\"javascript:void(0)\" class=\"read_first\">Shout-out Policy</a>. " .
				"", $networkName);
			if ($this->isLogged) {
				$username = ($owner instanceof Traveler) ? $owner->getUserName() : $owner->getName();
				if($owner instanceof Traveler){
					$this->message .= "Because you are not friends with ".$owner->getUserName().", your shout-out may not appear on this page until it has been reviewed and deemed appropriate for posting. <br /> <br />";
					$this->message .= sprintf("If you are following %s's %ss and want your future shout-outs to appear immediately, you may want to " .
						"<a onclick=\"CustomPopup.initialize('Request %s as a Friend ?','Are you sure you want to add %s as a friend?','/friend.php?action=request&fID=%d&profile','Send Request','1');CustomPopup.createPopup();\" href=\"javascript:void(0);\">Add %s as a friend</a>.",$username,$page,$username,$username,$ownerID,$username);
				}
			}
			else {
				$this->message .= "If you do not have a GoAbroad Network account, we reserve the right to review your shout-out. It may not appear on this page until it has been reviewed and deemed appropriate for posting. <br /> <br />";
				$this->message .= "If you wish for your shout-outs to appear immediately, you may <a href=\"/login.php\">Login</a> ";
				if (isset($GLOBALS['CONFIG']))
					$this->message .= "here.";
				else {
					$this->message .= "or <a href=\"/register.php\">create a FREE GoAbroad Network account</a>.";
				} 
			}
				
		} 		
 		
 		// check if the author of comment is not the owner of the object where the comment is made.
 		// check also if the comment is approved
 	  //if ($this->travelerID != $ownerID AND Comment::APPROVED == $approved){
	    if (Comment::APPROVED == $approved){
		  RealTimeNotifier::getInstantNotification(RealTimeNotifier::RECEIVED_GREETING, 
			  array(
				  'COMMENT' => $commentObj,
				  'POSTED_BY_OWNER' => $this->travelerID == $ownerID
				)
			)->send();
		}
		else if(Comment::UNAPPROVED == $approved){
		  RealTimeNotifier::getInstantNotification(RealTimeNotifier::RECEIVED_GREETING, 
			  array(
				 'COMMENT' => $commentObj,
				 'FOR_RECEIVER' => false
			    )
			)->send();
		}
		
		echo '4<<<>>>';
		// START: don't edit -- tricky!

		$cv = $this->file_factory->getClass('CommentsView');
		$cv->setContext($this->context,$this->contextID);
		$cv->setTravelerID($this->travelerID);
		$cv->setPhotoCategory($this->photoCat);
		$cv->setConfirmationMessage($this->message);
 	
	 	if($this->context == 2)
	 		$cv->setGenID(1);
	
		$cv->readComments();
 		echo $cv->renderAjax();

		
		// END: don't edit -- tricky!

	 }
 	
 	
 	
 	/****************************************************
	 * function doAdd
	 */ 		
 
 	function doAdd(){
 		 			
 		$snav = $this->file_factory->getClass('SubNavigation');
 		$snav->setContext('TRAVELER');	
 		$snav->setContextID($this->travelerID);
 		$this->template->set("context",$this->context);
 		$this->template->set("contextID",$this->contextID);		
 		if (isset($this->referer))
 			$template->set("referer",$this->referer);			
 		$this->template->set('snav',$snav);
 		$this->template->out('tpl.AddComment.php');
 		 		
 	}
 	
 	
 	/**********************************************************
 	 * function doDelete
 	 */
 	 
 	 function doDelete() {
 	 	// NOTE  : this function is not used (the one used by the app is in the class travellog/service/comments/Class.DeleteCommentRequest.php 
 	 	
 	 	//$comment = new Comment($this->contextID);
 	 	$comment = $this->file_factory->getClass('Comment',array($this->contextID));
 		$context = $comment->getContext();
 		$context->removeComment($comment); 	 		
 		$comment->Delete();

 		
 		
 		// invalidate cached views(html) of comments
 		require_once('Cache/ganetCacheProvider.php');
 		$cache	=	ganetCacheProvider::instance()->getCache();
 		$key 	=	'CommentsView_hashes_' . $this->context . '_' . $this->contextID;
 		if ($cache != null){
 			$hashKeys	=	$cache->get($key);
 			if ($hashKeys){
 				// found array of keys to invalidate
 				foreach ($hashKeys as $hashKey){
 					$cache->delete($hashKey);
 				}
 				
 				$cache->delete($key);
 			} 
 		} 
 		//exit;
 		//--
 		$cv = $this->file_factory->getClass('CommentsView');
		$cv->setContext($context,$this->contextID);
		$cv->setTravelerID($this->travelerID);
	 	$cv->readComments();
	 	echo $cv->renderAjax();
 	 }

 	/***********************************************************
 	 * function doSaveEdit
 	 */	
 	
 	function doSaveEdit() {
 		
 		$comment = $_POST['comment']; 		
 		//$comment = ereg_replace('[\<\>]','&nbsp;',$comment);    // prevent scripts to be entered
 		$comment = htmlspecialchars($comment);
 		//$contextID = $_POST['contextID'];
 		
 		$commentObj = $this->file_factory->getClass('CommentsView',array($this->contextID));
 		$commentObj->setTheText($comment);
 		$commentObj->setCreated(date('Y-m-d G:i'));
 		$commentObj->Update();
 		 		 		
 		if (isset($this->referer))
 				header ("Location: " . $this->referer);
 		
 	}
 	
 	/*********************************************************************
 	 * function doSummary   OK!!!
 	 */
 	 
 	 function doSummary(){
 	 	
 	 	
 	 	// get profile comments
 	 	$profileComments = array();
 	 	$comments = $this->traveler->getTravelerProfile()->getComments();
 	 
 	 	$this->template->set("commentCount",count($comments));
 	 	 	 	
 	 	// get travellog comments
 	 	//$tlogs = Travellog::getTravellogs($this->traveler);
 	 	$tlogs = $this->file_factory->invokeStaticClass('TravelLog','getTravellogs',array($this->traveler));
 	 
 	 	$jEntries = array();
 	 	if (null != $tlogs) {
	 	 	foreach ($tlogs as $tlog){
	 	 		$comments = $tlog->getComments();
	 	 		if (NULL != $comments)
	 	 			$jEntries[] = array('travellog' => $tlog,'count' => count($comments)); 	 	 			 		  	 		
	 	 	}
 	 	} 	
 	 	$this->template->set("jEntries",$jEntries );
 	  	 	
 	 	// get photo comments
 	 	$photos = $this->traveler->getTravelerProfile()->getPhotos();
 	 	$photoComments = array();
 	 	if (null != $photos) {
	 	 	foreach ($photos as $photo){
	 	 		$comments = $photo->getComments();
	 	 		if (NULL != $comments) {
	 	 			$ctx = $photo->getContext();
	 	 			$genID = 0;
	 	 			switch (get_class($ctx)) {
						case 'TravelerProfile':
							$genID = $ctx->getTravelerID();
							break;
						case 'TravelLog':
							$genID = $ctx->getTravelLogID();
							break;
						case 'FunGroup':
							$genID = $ctx->getGroupID();
							break;
						case 'AdminGroup':
							$genID = $ctx->getGroupID();
							break;		
						case 'PhotoAlbum':
							$genID = $ctx->getPhotoAlbumID();
							break;	
						case 'Program':
							$genID = $ctx->getProgramID();
							break;										 	 				
	 	 			}
	 	 			
	 	 			$photoComments[] = array('photo' => $photo,'count' => count($comments), 'genID' => $genID );
	 	 		} 
	 	 	}
 	 	}	
 	 	
 	 	$this->template->set("photoComments",$photoComments);
 	 	$this->template->set("travelerID",$this->traveler->getTravelerID()); 	 	
 	 	$this->template->out('tpl.CommentSummary.php');
 	 	
 	 }
 	 
 	 /*******************************************************
 	 * function canDeleteComment - function to determine if 
 	 *    the currently logged user can delete the comment
 	 */
 	 
 	 function canDeleteComment($comment) { 	 	 	 	
 	 	 	 
 	 	if ($this->travelerID == $comment->getAuthor())
 	 		return true;
 	 		
 	 	$context = $comment->getContext();	
 	 	switch (get_class($context)){ 	 		
 	 		case 'Photo': 	 			
 	 			if (isPhotoOwner($this->travelerID,$context))
 	 				return true;
 	 			break;
 	 		case 'TravelLog': 	 			
 	
  				if ($this->travelerID == $context->getTravelerID())
 					 return true; 
 	 			 	 		
 	 			break;
 	 		case 'Traveler':

 				if ($this->travelerID == $context->getTravelerID())
					return true;  					
  				break; 	 		
			case 'Article':	
				if ($this->travelerID == $context->getAuthorID())
					return true;  					
  				break;
 	 	} 	 	
 	 	
 	 	return false;
 	 }
 	 
 	 /***************************************************
 	  * checks whether a given traveler w/ this travelerID is the owner of the photo
 	  */ 	 
 	 function isPhotoOwner($travelerID,$photo){
 	 	switch ($photo->getPhotoTypeID()) {
			case PhotoType::$PROFILE:					
				$tp = $photo->getContext();
				if ($travelerID == $tp->getTravelerID())
					return true;		                                                 					
				break;					
			case PhotoType::$TRAVELLOG:					
				$tl = $photo->getContext();
				if ($travelerID == $tl->getTravelerID())
					return true;		           		
				break;					
			case PhotoType::$FUNGROUP:									
				$group = $photo->getContext();
				if ($travelerID == $group->getAdministrator()->getTravelerID())
					return true;
				break;				
			case PhotoType::$ADMINGROUP:									
				$group = $photo->getContext();
				if ($travelerID == $group->getAdministrator()->getTravelerID())
					return true;
				break;					
			case PhotoType::$PHOTOALBUM:									
				$pAlbum = $photo->getContext();
				$gf = GroupFactory::instance();
				$group = $gf->create(array($pAlbum->getGroupID()));
				if ($travelerID == $group[0]->getAdministrator()->getTravelerID() ) 
					return true;							
				break;				
			case PhotoType::$PROGRAM:									
				$program = $photo->getContext();
				$gf = GroupFactory::instance();
				$group = $gf->create(array($program->getGroupID()));
				if ($travelerID == $group[0]->getAdministrator()->getTravelerID())
				 	return true;
				break;																								
									
		}
		return false;
 	 }
 	
 	
 
 }
?>
