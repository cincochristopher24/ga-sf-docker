<?php
/*
	Filename:		Class.GANETTravelMapsController.php
	Author:			Jonas Tandinco
	Date Created:	Jan/03/2007
	Putpose:		controller implementation for pure GoAbroad.net functionality

	EDIT HISTORY:
*/

require_once('travellog/controller/Class.AbstractTravelMapsController.php');

class GANETTravelMapsController extends AbstractTravelMapsController{
	public function performAction(){
		parent::performAction();
	}
}
?>