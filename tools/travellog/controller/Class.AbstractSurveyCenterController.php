<?php
	require_once("travellog/model/Class.SessionManager.php");
	require_once('travellog/factory/Class.FileFactory.php');
	
	abstract class AbstractSurveyCenterController
	{
		protected $obj_session 		= NULL;
		protected $file_factory 	= NULL;
		
		protected $loggedTraveler 	= NULL;
		protected $pageTitle 		= 'Survey Center';
		
		public function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/views'));
			$this->obj_session  = SessionManager::getInstance();
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			$this->file_factory->registerClass('ToolMan', array('path'=>''));
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('AdminGroup');
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('SurveyForm', array('path'=>'travellog/model/formeditor/'));
			$this->file_factory->registerClass('Template', array('path'=>''));
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('Paging', array('path'=>''));
			$this->file_factory->registerClass('ObjectIterator', array('path'=>''));
			
			/*****************************************************
			 * added by neri: 11-06-08
			 * registers the class used by the profile component 
			 *****************************************************/
			 
			$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
		}
		
		
		public function performAction(){
			//check if the user is logged
			$travelerID = $this->obj_session->get('travelerID');
			if(!$travelerID){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/login.php'));
			}
			//check if the user is an administrator
			$this->loggedTraveler = $this->file_factory->getClass('Traveler',array($travelerID));
			if(!array_key_exists('gID',$_GET)){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}			
			$oGroupFactory = $this->file_factory->invokeStaticClass('GroupFactory','instance');
			try{
				$groups = $oGroupFactory->create(array($_GET['gID']));
				$oGroup = count($groups) ? $groups[0] : null;
			}
			catch(exception $e){
				$oGroup = null;
			}
			
			if(is_null($oGroup) || $oGroup->getDiscriminator() != GROUP::ADMIN){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			if($oGroup->getAdministratorID() != $this->loggedTraveler->getTravelerID() && !$oGroup->isSuperStaff($this->loggedTraveler->getTravelerID())){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			
			//make sure that the logged traveler is the owner of the form if a formID has been provided
			$frmID = (array_key_exists('frmID',$_GET) && is_numeric($_GET['frmID']))?$_GET['frmID']:0;
			if($frmID != 0){
				if(!$this->file_factory->invokeStaticClass('SurveyForm','isExists',array($frmID))){
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
				}
				$oForm = $this->file_factory->getClass('SurveyForm',array($frmID));
				if($oForm->getGroupID()!=$oGroup->getGroupID()){
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
				}
			}
			$method = array_key_exists('method',$_GET)?$_GET['method']:'view';
			$oForm = isset($oForm) ? $oForm : NULL;
			$this->$method($oForm,$oGroup);
		}
		
		protected function view($oForm,$oGroup){
			//get the administered group of the logged traveler
			$oGroupFactory = $this->file_factory->invokeStaticClass('GroupFactory','instance');
			$arSurveyForms = $this->file_factory->invokeStaticClass('SurveyForm','getGroupSurveyForms',array($oGroup->getGroupID()));
			$this->showTemplate(array(
				'groupFactory'	=> $oGroupFactory,
				'group'			=> $oGroup,
				'surveyForms'	=> $arSurveyForms
			));
		}
		
		protected function delete($form,$oGroup){
			$form->delete();
			$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/surveycenter.php?gID='.$oGroup->getGroupID()));
		}
		
		protected function lock($form,$oGroup){
			$form->setAsLocked();
			$form->save();
			$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/surveycenter.php?gID='.$oGroup->getGroupID()));
		}
		
		protected function unlock($form,$oGroup){
			$form->setAsUnlocked();
			$form->save();
			$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/surveycenter.php?gID='.$oGroup->getGroupID()));
		}
		
		protected function showTemplate($vars){
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			$tpl = $this->file_factory->getClass('Template');
			$tpl->set_path('travellog/views/');
			$tpl->set('groupFactory',$vars['groupFactory']);
			$tpl->set('oGroup',$vars['group']);
			
			$tpl->set('arSurveyForms',$vars['surveyForms']);
			
			/****************************************************
			 * edits by neri: 11-06-08
			 * added code for displaying th profile component
			 ****************************************************/
			
			$profile_comp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
			$profile_comp->init($vars['group']->getGroupID());
			$tpl->set('profile', $profile_comp->get_view());
			
			$subNavigation = $this->file_factory->getClass('SubNavigation');
			$subNavigation->setContext('GROUP');	
			$subNavigation->setContextID($vars['group']->getGroupID());
			$subNavigation->setGroupType('ADMIN_GROUP');
			$subNavigation->setLinkToHighlight('SURVEY_CENTER');
			
			
			$page = (isset($_GET['page']) && is_numeric($_GET['page']))?$page = $_GET['page']:$page=1;
			
			$paging =  $this->file_factory->getClass('Paging',array(count($vars['surveyForms']),$page,'gID='.$vars['group']->getGroupID(),5));
			$iterator = $this->file_factory->getClass('ObjectIterator',array($vars['surveyForms'],$paging));
			$tpl->set('paging',$paging);
			$tpl->set('iterator',$iterator);
			$tpl->set('subNavigation',$subNavigation);
			$tpl->set('pageTitle',$this->pageTitle);
			$tpl->set('isAdmin', $vars['group']->getAdministratorID() == $this->loggedTraveler->getTravelerID() ? TRUE : FALSE);
			$tpl->out('tpl.SurveyCenter.php');
		}
					
	}
?>