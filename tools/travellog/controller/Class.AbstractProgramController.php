<?php
require_once("travellog/model/Class.SessionManager.php");
require_once("Class.ToolMan.php");
require_once("travellog/controller/Class.IController.php");
require_once("travellog/factory/Class.FileFactory.php");
//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
abstract class AbstractProgramController implements IController{
	
	protected static $REQUIRES_ACTION_RULES = array('ADD', 'SAVE', 'EDIT', 'DELETE', 'CREATE' ); 
	
	protected 
	
	$data         = array(),
	
	$obj_session  = NULL,
	
	$file_factory = NULL; 
	
	function __construct(){
		
		$this->obj_session  = SessionManager::getInstance();
		$this->file_factory = FileFactory::getInstance(); 
		
		$this->file_factory->registerClass('Program');
		$this->file_factory->registerClass('ActivityController', array('path' => 'travellog/controller/', 'file' => 'GANETActivityController'));
		$this->file_factory->registerClass('GroupFactory');
		$this->file_factory->registerClass('AdminGroup');
		$this->file_factory->registerClass('Condition');
		$this->file_factory->registerClass('FilterCriteria2');
		$this->file_factory->registerClass('Traveler');
		$this->file_factory->registerClass('SubNavigation');
		$this->file_factory->registerClass('Template'               , array('path' => ''));
		$this->file_factory->registerClass('Paging'                 , array('path' => ''));
		$this->file_factory->registerClass('ObjectIterator'         , array('path' => ''));
		$this->file_factory->registerClass('HtmlHelpers'            , array('path' => ''));
		$this->file_factory->registerClass('GaDateTime'               , array('path' => ''));
		
		$this->file_factory->registerTemplate('LayoutMain');
		$this->file_factory->registerTemplate('ViewProgram');
		$this->file_factory->registerTemplate('ViewProgramLists');
		$this->file_factory->registerTemplate('ViewProgramDetail');
		$this->file_factory->registerTemplate('FrmProgramManagement');
		
		
		$this->file_factory->setPath('CSS', '/css/');
		$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');  
	}
	
	function performAction(){
		switch( strtolower($this->data['action']) ){
			case "edit":
			case "create":
				$this->__getData();
				$this->_viewAddForm();
			break;
			
			case "delete":
				$this->_delete();
			break;
			
			case "save":
				$this->__validateForm();
				$this->_save();
			break;
			
			case "programlists":
				$this->_viewProgramLists();    
			break;
			
			case "programdescription":
				$this->_viewEntryDescription();
			break;
			
			default:
				$this->_viewProgram();
		}
	}
	
	protected function _viewAddForm(){
		$obj_template = $this->file_factory->getClass('Template');
		$obj_template->set('props', $this->data);
		
		echo $obj_template->fetch( $this->file_factory->getTemplate('FrmProgramManagement') ); 
	}
	
	protected function _save(){
		if( $this->data['pID'] ){
			$obj_program = $this->file_factory->getClass('Program', array($this->data['pID']));
			$obj_program->setTitle         ( $this->data['title']       );
			$obj_program->setStart         ( $this->data['start_date']  );			
			$obj_program->setFinish        ( $this->data['finish_date'] );
			$obj_program->setDescription   ( $this->data['description'] );
			$obj_program->setDisplayPublic ( $this->data['display']     );
			$obj_program->Update();
			
			if ( count($obj_program->getSubgroups()) ){
				foreach( $obj_program->getSubgroups() as $obj_subgroup ){
					$obj_group = $this->file_factory->getClass('AdminGroup', array($obj_subgroup->getGroupID()));
					$obj_group->removeProgram( $this->data['pID'] );
				}
			}
		} 
		else{
			$obj_program = $this->file_factory->getClass('Program');
			$obj_program->setGroupID       ( $this->data['gID']         );
			$obj_program->setTitle         ( $this->data['title']       );
			$obj_program->setStart         ( $this->data['start_date']  );			
			$obj_program->setFinish        ( $this->data['finish_date'] );
			$obj_program->setDescription   ( $this->data['description'] );
			$obj_program->setDisplayPublic ( $this->data['display']     );
			$obj_program->Create();
		}
		$obj_group = $this->file_factory->getClass('AdminGroup', array($this->data['gID']));
		if( $obj_group->isParent() && count($this->data['subgroups']) ){
			foreach( $this->data['subgroups'] as $groupID ){
				$obj_group = $this->file_factory->getClass('AdminGroup', array($groupID));
				$obj_group->addProgram($obj_program);
			}
		}
		 
	}
	
	protected function _viewProgram(){
		$obj_template           = $this->file_factory->getClass('Template');
		$obj_navigation         = $this->file_factory->getClass('SubNavigation');
		$this->data['gID']      = ( isset($this->data['gID']) )? $this->data['gID'] : 0; 
		$this->data['pID']      = ( isset($this->data['pID']) )? $this->data['pID'] : 0;
		$this->data['page']     = ( isset($this->data['page']) )? $this->data['page'] : 1;
		$this->data['is_login'] = $this->obj_session->getLogin();
		
		if( !$this->data['gID'] && !$this->data['pID'])
			ToolMan::redirect('/group.php');
		
		if( $this->data['pID'] ){
			$obj_program       = $this->file_factory->getClass('Program', array($this->data['pID']));   
			$this->data['gID'] = $obj_program->getGroupID();
		}
		
		$obj_navigation->setContext        ( 'GROUP'             );
		$obj_navigation->setContextID      (  $this->data['gID'] );
		$obj_navigation->setLinkToHighlight( 'ITINERARIES'       );
		$obj_navigation->setGroupType      ( 'ADMIN_GROUP'       );
		
		$obj_template->set( 'props'         , $this->data               );
		$obj_template->set( 'contents'      , $this->_getProgramLists() );
		$obj_template->set( 'obj_navigation', $obj_navigation           );
		
		$gID    = $this->data['gID'];
		$pID    = $this->data['pID'];   
		$action = $this->data['action'];
		
		$code = <<<BOF
<script type="text/javascript">   
//<![CDATA[
	jQuery.noConflict();   
	jQuery(document).ready(function(){     
		gID       = $gID;
		currentID = $pID;
		action    = "$action";  
		jQuery('.itineraries li').observeProgramEvents();   
		jQuery("#loading").ajaxStart(function(){
		   jQuery(this).css('display', 'inline');
		   jQuery('.content #content_details').empty();  
		   jQuery('.content #activity_lists').empty();  
		});
		jQuery("#loading").ajaxStop(function(){
		   jQuery(this).css('display', 'none'); 
		});  
	});  
//]]>  
</script>
BOF;
		
		Template::includeDependentJs( '/js/jquery-1.1.4.pack.js' );
		Template::includeDependentJs( '/js/date.js'              );
		Template::includeDependentJs( '/js/jquery.datePicker.js' );
		Template::includeDependentJs( '/js/jquery.program.js'    );
		Template::includeDependent  ( $code                      );  
		Template::includeDependentCss( '/min/f=css/styles.css'   		 ); 
		Template::setMainTemplateVar('page_location', 'Groups');
		Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );
		if( isset($GLOBALS['CONFIG']) )
			Template::setMainTemplateVar('title', $GLOBALS['CONFIG']->getSiteName() . ' Itineraries');
		else
			Template::setMainTemplateVar('title', 'Itineraries');
		$obj_template->out( $this->file_factory->getTemplate('ViewProgram') );
	}
	
	protected function _viewProgramLists(){
		$this->data['gID']  = ( isset($this->data['gID']) )? $this->data['gID']   : 0; 
		$this->data['pID']  = ( isset($this->data['pID']) )? $this->data['pID']   : 0;
		$this->data['page'] = ( isset($this->data['page']) )? $this->data['page'] : 1;
		echo $this->_getProgramLists();
	}
	
	protected function _delete(){
		$obj_program = $this->file_factory->getClass('Program', array($this->data['pID']));
		$obj_program->Delete(); 
	}
	
	protected function _getProgramLists(){
		$obj_template            = $this->file_factory->getClass('Template');
		$obj_condition           = $this->file_factory->getClass('Condition');
		$rows_per_page           = 10;
		$obj_condition1          = clone($obj_condition);
		$obj_group               = $this->file_factory->getClass('AdminGroup', array($this->data['gID']));
		$this->data['is_member'] = false;
		$this->data['travelerID']= ( isset($this->data['travelerID']) )? $this->data['travelerID']: 0;
		$this->data['is_login']  = $this->obj_session->getLogin();
		$obj_filter              = NULL;
		$obj_order               = NULL;
		
		if( $this->data['is_login'] ){
			$this->data['travelerID'] = $this->obj_session->get('travelerID');
			$obj_traveler             = $this->file_factory->getClass('Traveler');
			$this->data['is_member']  = $obj_group->isMember($obj_traveler);
		}
		
		$this->data['is_owner'] = ( $this->data['travelerID'] == $obj_group->getAdministratorID() )? true: false;
		
		if( !$this->data['is_member'] && $this->obj_session->get('travelerID') != $obj_group->getAdministratorID() ){
			$obj_filter = $this->file_factory->getClass('FilterCriteria2');
			$obj_order  = clone($obj_filter);
			
			$obj_condition->setAttributeName("displaypublic");
			$obj_condition->setOperation(FilterOp::$EQUAL);
			$obj_condition->setValue(1);
			$obj_filter->addBooleanOp('AND');
			$obj_filter->addCondition($obj_condition);
			
			$obj_condition1->setAttributeName("tblProgram.programID");
			$obj_condition1->setOperation(FilterOp::$ORDER_BY_DESC);
			$obj_order->addCondition($obj_condition1); 
		}
		
		$col_programs = $obj_group->getPrograms('', $obj_filter, $obj_order);
		
		if( count($col_programs) ){
			if( $this->data['pID'] ) $this->data['page'] = $this->__checkPosition( $col_programs, $rows_per_page );
			$obj_paging   = $this->file_factory->getClass( 'Paging', array(count($col_programs),$this->data['page'], 'action=ProgramLists&gID='.$this->data['gID'], $rows_per_page) );
			$obj_iterator = $this->file_factory->getClass( 'ObjectIterator', array($col_programs, $obj_paging) );
			$obj_paging->setOnclick('jQuery().nextNprevProgram');
			$obj_template->set( 'obj_paging'  , $obj_paging   );
			$obj_template->set( 'obj_iterator', $obj_iterator ); 
		} 
		$obj_template->set( 'props', $this->data   );
		
		return $obj_template->fetch( $this->file_factory->getTemplate('ViewProgramLists') );
	}
		
	protected function _viewEntryDescription(){
		$obj_template           = $this->file_factory->getClass('Template');
		$this->data['is_login'] = $this->obj_session->getLogin();
		$this->data['is_owner'] = false;
		$subgroup_names         = NULL;
		
		if( $this->data['pID'] ){
			$this->data['obj_program'] = $this->file_factory->getClass('Program', array($this->data['pID']));
			$this->data['gID']         = $this->data['obj_program']->getGroupID();
			$obj_group                 = $this->file_factory->getClass('AdminGroup', array($this->data['gID']));
			$col_subgroups             = $this->data['obj_program']->getSubgroups();
			$this->data['is_owner']    = ( $this->obj_session->get('travelerID') == $obj_group->getAdministratorID() )? true: false;
			
			if ( count( $col_subgroups ) ){
				$ctr = 1;
				foreach( $col_subgroups as $obj_subgroup ){
					if( $ctr == 1 )
						$subgroup_names = $obj_subgroup->getName();
					else{ 
						if($ctr != count($col_subgroups)) 
							$subgroup_names .= ', '.$obj_subgroup->getName();
						else
							$subgroup_names .= ' and '.$obj_subgroup->getName();
					}
					$ctr++;
				}
			}
		}
		$obj_template->set( 'props'         , $this->data      );
		$obj_template->set( 'subgroup_names', $subgroup_names  );
		
		echo $obj_template->fetch($this->file_factory->getTemplate('ViewProgramDetail'));
	}
		
	protected function __checkPosition( $col_programs, $rowsperpage ){
		$page = 1;
		for($ctr=0; $ctr < count($col_programs); $ctr++ ){
			if ( $col_programs[$ctr]->getProgramID() == $this->data['pID'] ){
				$page = ceil(($ctr+1)/$rowsperpage);
				break;
			}
		}  
		return $page;
	}
	
	protected function __getData(){
		if( array_key_exists('pID', $this->data) && $this->data['pID'] && strtolower($this->data['action']) != 'save' ){
			$obj_program                      = $this->file_factory->getClass('Program'   , array($this->data['pID']));
			$obj_group                        = $this->file_factory->getClass('AdminGroup', array($obj_program->getGroupID()));
			$selected_subgroups               = array();
			foreach( $obj_program->getSubgroups() as $obj_subgroup ){
				$selected_subgroups[] = $obj_subgroup->getGroupID();
			}
			$this->data['title']           = $obj_program->getTitle();
			$this->data['description']     = $obj_program->getDescription();
			$this->data['start_date']      = $obj_program->getStart();
			$this->data['finish_date']     = $obj_program->getFinish();
			$this->data['gID']             = $obj_program->getGroupID();
			$this->data['display']         = $obj_program->getDisplayPublic();
			$this->data['subgroups']       = $selected_subgroups;
			$this->data['subgroups_lists'] = $obj_group->getSubGroups();
			$this->data['errors']          = array();
			$this->data['mode']            = 'edit';
		}
		else{ 
			$obj_group                     = $this->file_factory->getClass('AdminGroup', array($this->data['gID']));
			$this->data['pID']             = ( isset($this->data['pID'])         )? $this->data['pID']        : 0;
			$this->data['title']           = ( isset($this->data['title'])       )? $this->data['title']      : '';
			$this->data['description']     = ( isset($this->data['description']) )? $this->data['description']: '';
			$this->data['start_date']      = ( isset($this->data['start_date'])  )? $this->data['start_date'] : '';
			$this->data['finish_date']     = ( isset($this->data['finish_date']) )? $this->data['finish_date']: '';
			$this->data['display']         = ( isset($this->data['display'])     )? $this->data['display']    : 1;
			$this->data['subgroups']       = ( isset($this->data['subgroups'])   )? $this->data['subgroups']  : array();
			$this->data['subgroups_lists'] = $obj_group->getSubGroups(); 
			$this->data['errors']          = array();
			$this->data['mode']            = 'add';
		}	
	}
	
	protected function __applyRules(){ 
		if( in_array( strtoupper($this->data['action']), AbstractProgramController::$REQUIRES_ACTION_RULES) ){
			// TODO: If some actions requires a more complex rules use a rule object or method instead
			switch( strtolower($this->data['action']) ){ 
				case 'add':	
					if( !$this->obj_session->getLogin() ){
						ToolMan::redirect('/group.php?gID='.$this->data['gID']);
						exit;
					}
				break; 
				
				case 'save':
					if( !isset($_POST['action']) ){	
						echo '<h2>Requires a post method.</h2>';
						exit;
					}	
				break;
				case 'delete':	
					if( !isset($_POST['action']) ){	
						echo '<h2>Requires a post method.</h2>';
						exit;
					}			  
				case 'edit':
					$obj_program = $this->file_factory->getClass('Program', array($this->data['pID']));
					$obj_group   = $this->file_factory->getClass('AdminGroup', array($obj_program->getGroupID()));
					if(  $obj_group->getAdministratorID() != $this->obj_session->get('travelerID') ){
						echo '<h2>You are not allowed to '. $this->data['action'] .' this itinerary.</h2>';  
						exit; 
					}	
				case 'create':
					if( !$this->obj_session->getLogin() ){
						echo '<h2>You are not allowed to add.</h2>';
						exit;
					}
				break; 
			}
		}
	}
	
	protected function __validateForm(){
		$this->__getData(); 
		$errors = array();
		if( !preg_match('/.+/', $this->data['title']) ){
			$errors[] = 'Title is a required field!';
		}
		if( !preg_match('/.+/', $this->data['description']) ){
			$errors[] = 'Description is a required field!';
		}
		if( !preg_match('/.+/', $this->data['start_date']) ){ 
			$errors[] = 'Start date is a required field!';
		}
		if( !preg_match('/.+/', $this->data['finish_date']) ){ 
			$errors[] = 'End date is a required field!';  
		}
		
		$this->data['errors'] = $errors;
		
		if( count($this->data['errors']) ){ 
			$this->_viewAddForm();
			exit;	
		}
	}
	
}
?>
