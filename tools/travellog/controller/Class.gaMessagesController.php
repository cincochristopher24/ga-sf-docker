<?php

require_once ('Class.Template.php');
require_once ('travellog/model/messageCenter/manager/Class.gaMessagesManagerRepository.php');

class gaMessagesController {
	
	private $viewTemplate;
	private static $publicActions = array('view');

	public function __construct() {
		$this->viewTemplate = new Template;
		Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
	}
	
	public function execute() {
		if (isset($_GET['act'])) {
			$action = $_GET['act'];
		} else {
			$action = 'view';
		}
		
		// require login for non public actions
		if (!in_array($action, self::$publicActions)) {
			$this->requireAuthentication();
		}
		
		switch ($action) {
			case 'messageGroupStaff':
				$this->handleMessageGroupStaff();
				
				break;
				
			case 'messageTraveler':
				$this->handleMessageTraveler();
				
				break;
				
			case 'messageStaffOfManyGroups':
				$this->handleMessageStaffOfManyGroups();
			
				break;
				
			case 'messageManyTravelers':
				$this->handleMessageManyTravelers();

				break;
			
			default:
				$this->handleViewMessageCenter();
				
				break;
		}
	}
	
	
	private function handleViewMessageCenter() {
		
	}
	
	/**
	 * REQUIREMENTS: for a traveler to send messages to staff of many different groups in behalf of the group,
	 * he must be logged on and have admin privileges (advisor, super staff, staff)
	 */
	private function handleMessageStaffOfManyGroups() {
		if (!isset($_GET['gID']) || !($group = gaGroupMapper::getGroup(trim($_GET['gID']))) || !in_array($_SESSION['travelerID'], $group->getOverseerIDs())) {
			header("location: /messages.php");
			exit;
		}
		
		if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
			// handle form submission
			$errors = $this->validateComposeMessageForm();
			if (!isset($errors["recipientList"]) && !$this->validateGroupNameOfStaffMessageRecipients()) {
				$errors["recipientList"] = "Please check to make sure you have spelled the recipient's name correctly.";
			}
			
			if (count($errors) > 0) {
				$this->showMessageStaffOfManyGroupsForm($group, $errors);
			} else {
				$this->sendMessageToGroupStaffOfManyGroups($group);
				
				$_SESSION['flashMessage'] = 'Message sent';
				header("location: /messages.php?gID=" . $group->getID());
				exit;
			}
		} else {
			$this->showMessageStaffOfManyGroupsForm($group);
		}
	}
	
	private function validateGroupNameOfStaffMessageRecipients() {
		$groupNames = explode(',', trim($_POST['txtDestination']));
		foreach ($groupNames as $key => $groupName) {
			$groupNames[$key] = trim($groupName);
		}
		
		$groups = gaGroupMapper::retrieveGroupsByGroupNames($groupNames);
		
		if (count($groups) > 0) {
			return true;
		}
		
		return false;
	}
	
	private function showMessageStaffOfManyGroupsForm(gaGroup $group, array $errors = array()) {
		$profileComponent = $this->getGroupProfileComponent($group);
		$subnavigation    = $this->getGroupSubNavigation($group);
		
		$inboxLink  = "/messages.php?gID=" . $group->getID();
		$outboxLink = "/messages.php?gID=" . $group->getID() . "&show_outbox";
		
		require_once('travellog/model/Class.SiteContext.php');
		$siteContext = SiteContext::getInstance();
		
		if ($siteContext->isInCobrand()) {
			$siteDisplayName = $group->getName();
		} else {
			$siteDisplayName = 'GoAbroad Network';
		}
		
		$notifyByEmail = true;
		if (isset($_POST['btnSend']) && !isset($_POST['chkSendNotification'])) {
			$notifyByEmail = false;
		}
		
		$suggestions = gaGroupMapper::retrieveCoveredGroupNames($group);
		foreach ($suggestions as $key => $suggestion) {
			$suggestions[$key] = "'$suggestion'";
		}
		
		// auto suggest
		Template::includeDependentJs("/js/actb.js");
		Template::includeDependentJs("/js/commons.js");

		$this->viewTemplate->set_vars(array(
				'recipientsListLocked' => false,
				'recipientDisplayName' => isset($_POST['txtDestination']) ? $_POST['txtDestination'] : $group->getName(),
				'subject'              => isset($_POST['txtSubject']) ? $_POST['txtSubject'] : '',
				'body'                 => isset($_POST['txaMsgText']) ? $_POST['txaMsgText'] : '',
				'notifyByEmail'        => $notifyByEmail ? 'checked' : '',
				'profileComponent'     => $profileComponent->get_view(),
				'subnavigation'        => $subnavigation,
				'inboxLink'            => $inboxLink,
				'outboxLink'           => $outboxLink,
				'siteDisplayName'      => $siteDisplayName,
				'errors'               => $errors,
				'autoSuggestSetting'   => 'on',
				'suggestions'          => $suggestions,
				'helpText'             => 'Send messages to your <strong>groups</strong> - just type in the name of the group. To send the same message to many groups, ' .
										  'just separate their names with commas.<br /><br />' .
										  'To send a message to a <strong>member</strong>, please go to the member\'s profile and click Send a Message.',
			));
		
		$this->viewTemplate->out('travellog/views/messageCenter/tpl.FrmComposeMessage.php');
	}
	
	private function sendMessageToGroupStaffOfManyGroups(gaGroup $sendingGroup) {
		$groupNames = explode(',', trim($_POST['txtDestination']));
		foreach ($groupNames as $key => $groupName) {
			$groupNames[$key] = trim($groupName);
		}
		
		$groups = gaGroupMapper::retrieveGroupsByGroupNames($groupNames);
		
		$collRecipientIDs = array();
		$collGroupIDs = array();
		foreach ($groups as $group) {
			$collGroupIDs[] = $group->getID();
			
			if ($group->isParent()) {
				$collRecipientIDs = array_merge($collRecipientIDs, array($group->getAdministratorID()), $group->getSuperStaffIDs());
			} else {
				$collRecipientIDs = array_merge($collRecipientIDs, $group->getStaffIDs());
			}
		}
		
		// make sure a recipient doesnt' receive the message more than once since it's possible
		// that a certain traveler is a staff of many groups and those groups could possibly be
		// included as recipients of this message
		$collRecipientIDs = array_unique($collRecipientIDs);
		$collGroupIDs = array_unique($collGroupIDs);
		
		$newMessage = new gaToGroupStaffPersonalMessage();
			
		$newMessage->setSenderID     ( $_SESSION['travelerID'] );
		$newMessage->setSubject      ( "[[{$sendingGroup->getName()}]" . $_POST['txtSubject']    );
		$newMessage->setBody         ( $_POST['txaMsgText']    );
		$newMessage->setRecipientIDs ( $collRecipientIDs       );
		
		$newMessage->setGroupIDs     ( $collGroupIDs           );
		$newMessage->setGroups       ( $groups                 );
		
		$newMessage->send();
	}
	
	private function handleMessageManyTravelers() {
		if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
			// handle form submission
			$errors = $this->validateComposeMessageForm();
			if (!isset($errors["recipientList"]) && !$this->validateTravelerMessageRecipients()) {
				$errors["recipientList"] = "Please check to make sure you have spelled the recipient's name correctly.";
			}
			
			if (count($errors) > 0) {
				$this->showMessageManyTravelersForm($errors);
			} else {
				$this->sendMessageToManyTravelers();
				
				$_SESSION['flashMessage'] = 'Message sent';
				header("location: /messages.php");
				exit;
			}
		} else {
			$this->showMessageManyTravelersForm();
		}
	}
	
	private function validateTravelerMessageRecipients() {
		$userNames = explode(',', trim($_POST['txtDestination']));
		foreach ($userNames as $key => $userName) {
			$userNames[$key] = trim($userName);
		}
		
		$recipientIDs = gaTravelerMapper::getFriendIDsFromUserNames($_SESSION['travelerID'], $userNames);
		
		if (count($recipientIDs) > 0) {
			return true;
		}

		return false;
	}
	
	private function sendMessageToManyTravelers() {
		$userNames = explode(',', trim($_POST['txtDestination']));
		foreach ($userNames as $key => $userName) {
			$userNames[$key] = trim($userName);
		}
		
		$recipientIDs = gaTravelerMapper::getFriendIDsFromUserNames($_SESSION['travelerID'], $userNames);
		
		$newMessage = new gaToTravelerPersonalMessage();
			
		$newMessage->setSenderID     ( $_SESSION['travelerID']   );
		$newMessage->setSubject      ( $_POST['txtSubject']      );
		$newMessage->setBody         ( $_POST['txaMsgText']      );
		$newMessage->setRecipientIDs ( $recipientIDs );
		
		$newMessage->send();
		
		self::sendEmailNotification($newMessage);
	}
	
	private function showMessageManyTravelersForm(array $errors = array()) {
		$profileComponent = $this->getProfileComponent();
		$subnavigation    = $this->getSubNavigation();
		
		$inboxLink  = "/messages.php";
		$outboxLink = "/messages.php?show_outbox";
		
		require_once('travellog/model/Class.SiteContext.php');
		$siteContext = SiteContext::getInstance();
		
		if ($siteContext->isInCobrand()) {
			$siteDisplayName = $siteContext->getSiteName();
		} else {
			$siteDisplayName = 'GoAbroad Network';
		}
		
		if (isset($_POST['btnSend']) && !isset($_POST['chkSendNotification'])) {
			$notifyByEmail = false;
		} else {
			$notifyByEmail = true;
		}
		
		$suggestions = gaTravelerMapper::getFriendUserNames($_SESSION['travelerID']);
		foreach ($suggestions as $key => $suggestion) {
			$suggestions[$key] = "'$suggestion'";
		}
		
		// auto suggest
		Template::includeDependentJs("/js/actb.js");
		Template::includeDependentJs("/js/commons.js");

		$this->viewTemplate->set_vars(array(
				'recipientsListLocked' => false,
				'recipientDisplayName' => isset($_POST['txtDestination']) ? $_POST['txtDestination'] : '',
				'subject'              => isset($_POST['txtSubject']) ? $_POST['txtSubject'] : '',
				'body'                 => isset($_POST['txaMsgText']) ? $_POST['txaMsgText'] : '', 
				'notifyByEmail'        => $notifyByEmail ? 'checked' : '',
				'profileComponent'     => $profileComponent->get_view(),
				'subnavigation'        => $subnavigation,
				'inboxLink'            => $inboxLink,
				'outboxLink'           => $outboxLink,
				'siteDisplayName'      => $siteDisplayName,
				'errors'               => $errors,
				'autoSuggestSetting'   => 'on',
				'suggestions'          => $suggestions,
				'helpText'             => 'Send a message to your friends and other travelers on GoAbroad Network. To send the same message to many recipients, just separate their names with commas.',
			));
		
		$this->viewTemplate->out('travellog/views/messageCenter/tpl.FrmComposeMessage.php');
	}
	
	private function handleMessageTraveler() {
		if (!isset($_GET['id']) || !($traveler = gaTravelerMapper::getTraveler(trim($_GET['id'])))) {
			header("location: /messages.php");
			exit;
		}
		
		if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
			// handle form submission
			$errors = $this->validateComposeMessageForm();
			
			if (count($errors) > 0) {
				$this->showMessageTravelerForm($traveler, $errors);
			} else {
				$this->sendMessageToTraveler($traveler);
				
				$_SESSION['flashMessage'] = 'Message sent to ' . $traveler->getUserName();
				header("location: /messages.php");
				exit;
			}
		} else {
			$this->showMessageTravelerForm($traveler);
		}
	}
	
	private function sendMessageToTraveler(gaTraveler $traveler) {
		$newMessage = new gaToTravelerPersonalMessage();
			
		$newMessage->setSenderID     ( $_SESSION['travelerID']   );
		$newMessage->setSubject      ( $_POST['txtSubject']      );
		$newMessage->setBody         ( $_POST['txaMsgText']      );
		$newMessage->setRecipientIDs ( array($traveler->getID()) );
		
		// if the sender and recipients are not friends, set message as spam
		if (in_array($_SESSION['travelerID'], $traveler->getFriendIDs())) {
			$newMessage->setSpamStatus(true);
		} else {
			$newMessage->setSpamStatus(false);
		}
		
		$newMessage->send();
	}
	
	private function showMessageTravelerForm(gaTraveler $traveler, array $errors = array()) {
		$profileComponent = $this->getProfileComponent();
		$subnavigation    = $this->getSubNavigation();
		
		$inboxLink  = "/messages.php";
		$outboxLink = "/messages.php?show_outbox";
		
		require_once('travellog/model/Class.SiteContext.php');
		$siteContext = SiteContext::getInstance();
		
		if ($siteContext->isInCobrand()) {
			$siteDisplayName = $siteContext->getSiteName();
		} else {
			$siteDisplayName = 'GoAbroad Network';
		}
		
		$notifyByEmail = true;
		if (isset($_POST['btnSend']) && !isset($_POST['chkSendNotification'])) {
			$notifyByEmail = false;
		}

		$this->viewTemplate->set_vars(array(
				'recipientsListLocked' => true,
				'recipientDisplayName' => $traveler->getUserName(),
				'subject'              => isset($_POST['txtSubject']) ? $_POST['txtSubject'] : '',
				'body'                 => isset($_POST['txaMsgText']) ? $_POST['txaMsgText'] : '',              
				'notifyByEmail'        => $notifyByEmail ? 'checked' : '',
				'profileComponent'     => $profileComponent->get_view(),
				'subnavigation'        => $subnavigation,
				'inboxLink'            => $inboxLink,
				'outboxLink'           => $outboxLink,
				'siteDisplayName'      => $siteDisplayName,
				'errors'               => $errors,
				'autoSuggestSetting'   => 'off',
				'suggestions'          => '',
				'helpText'             => '',
			));
		
		$this->viewTemplate->out('travellog/views/messageCenter/tpl.FrmComposeMessage.php');
	}
	
	private function handleMessageGroupStaff() {
		if (!isset($_GET['gID']) || !($group = gaGroupMapper::getGroup(trim($_GET['gID'])))) {
			header("location: /messages.php");
			exit;
		}
		
		
		if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
			// handle form submission
			$errors = $this->validateComposeMessageForm();
			if (count($errors) > 0) {
				$this->showMessageGroupStaffForm($group, $errors);
			} else {
				// RULE: if a subgroup doesn't have staff, message is sent to super staff and admin
				if ($group->isParent() || count($group->getStaffIDs()) <= 0) {
					$this->sendMessageToGroupStaff($group, array_merge(array($group->getAdministratorID()), $group->getStaffIDs()));
				} else {
					$this->sendMessageToGroupStaff($group, $group->getStaffIDs());
				}
				
				$_SESSION['flashMessage'] = 'Message sent to ' . $group->getName() . ' staff';
				header("location: /messages.php");
				exit;
			}
		} else {
			$this->showMessageGroupStaffForm($group);
		}
	}
	
	private function sendMessageToGroupStaff(gaGroup $group, array $recipientIDs) {
		$newMessage = new gaToGroupStaffPersonalMessage();
			
		$newMessage->setSenderID     ( $_SESSION['travelerID']     );
		$newMessage->setSubject      ( "[{$group->getName()}]" . $_POST['txtSubject'] );
		$newMessage->setBody         ( $_POST['txaMsgText']        );
		$newMessage->setRecipientIDs ( array_unique($recipientIDs) );
		
		$newMessage->setGroupIDs     ( array($group->getID())      );
		$newMessage->setGroups       ( array($group)               );
		
		$newMessage->send();
		self::sendEmailNotification($newMessage);
	}
		
	private function validateComposeMessageForm() {
		$errors = array();
		
		// make sure there is a recipient if necessary
		if (isset($_POST['txtDestination']) && trim($_POST['txtDestination']) == '') {
			$errors["recipientList"] = "Please write the recipient(s) of your message.";
		}
		
		// make sure subject is provided
		if (trim($_POST['txtSubject']) == '') {
			$errors['subject'] = "Please write a subject of your message.";
		}
		
		// make sure message content is provided
		if (trim($_POST['txaMsgText']) == '') {
			$errors['body'] = 'You have not yet written a message.';
		}
		
		// SMART ERROR MESSAGING HERE
		if (trim($_POST["txtSubject"]) == '' && trim($_POST["txaMsgText"]) == '') {
			$errors["subjectAndBody"] = "Please write your message and its subject.";

			unset($errors["subject"], $errors["body"]);
		}

		return $errors;
	}
	
	private function showMessageGroupStaffForm(gaGroup $group, array $errors = array()) {
		$profileComponent = $this->getGroupProfileComponent($group);
		$subnavigation    = $this->getGroupSubNavigation($group);

		$inboxLink  = "/messages.php";
		$outboxLink = "/messages.php?show_outbox";
		
		require_once('travellog/model/Class.SiteContext.php');
		$siteContext = SiteContext::getInstance();
		
		if ($siteContext->isInCobrand()) {
			$siteDisplayName = $group->getName();
		} else {
			$siteDisplayName = 'GoAbroad Network';
		}
		
		$notifyByEmail = true;
		if (isset($_POST['btnSend']) && !isset($_POST['chkSendNotification'])) {
			$notifyByEmail = false;
		}

		$this->viewTemplate->set_vars(array(
				'recipientsListLocked' => true,
				'recipientDisplayName' => $group->getName(),
				'subject'              => isset($_POST['txtSubject']) ? $_POST['txtSubject'] : '',
				'body'                 => isset($_POST['txaMsgText']) ? $_POST['txaMsgText'] : '',
				'notifyByEmail'        => $notifyByEmail ? 'checked' : '',
				'profileComponent'     => $profileComponent->get_view(),
				'subnavigation'        => $subnavigation,
				'inboxLink'            => $inboxLink,
				'outboxLink'           => $outboxLink,
				'siteDisplayName'      => $siteDisplayName,
				'errors'               => $errors,
				'autoSuggestSetting'   => 'off',
				'suggestions'          => '',
				'helpText'             => '',
			));
		
		$this->viewTemplate->out('travellog/views/messageCenter/tpl.FrmComposeMessage.php');
	}
	
	//######################### M I S C E L L A N E O U S ##################################
	
	private function getProfileComponent() {
		if (isset($_GET['gID'])) {
			$group = gaGroupMapper::getGroup($_POST['gID']);
		}
		
		// check if logged on user is an advisor
		$group = gaGroupMapper::retrieveAdvisedGroup($_SESSION['travelerID']);
		
		if (is_null($group)) {
			// user is an ordinary traveler
			return $this->getTravelerProfileComponent(gaTravelerMapper::getTraveler($_SESSION['travelerID']));
		} else {
			// user is an advisor
			return $this->getGroupProfileComponent($group);
		}
	}
	
	private function getSubNavigation () {
		// check if logged on user is an advisor
		$group = gaGroupMapper::retrieveAdvisedGroup($_SESSION['travelerID']);
		
		if (is_null($group)) {
			// user is an ordinary traveler
			return $this->getTravelerSubNavigation(gaTravelerMapper::getTraveler($_SESSION['travelerID']));
		} else {
			// user is an advisor
			return $this->getGroupSubNavigation($group);
		}
	}
	
	private function getGroupSubNavigation(gaGroup $group) {
		require_once 'travellog/model/Class.SubNavigation.php';
		$subNavigation = new SubNavigation;
		
		$subNavigation->setContext("GROUP");
		$subNavigation->setContextID($group->getID());
		
		if ($group->isParent()) {
			$subNavigation->setGroupType("ADMIN_GROUP");
		} else {
			$subNavigation->setGroupType("FUN_GROUP");
		}
		
		$subNavigation->setLinkToHighlight('MESSAGE_CENTER');
			
		return $subNavigation;
	}
	
	private function getGroupProfileComponent(gaGroup $group) {
		require_once 'travellog/components/profile/controller/Class.ProfileCompFactory.php';
		$component = ProfileCompFactory::getInstance()->create(ProfileCompFactory::$GROUP_CONTEXT);
		$component->init($group->getID());
		
		return $component;
	}
	
	private function getTravelerProfileComponent(gaTraveler $traveler) {
		require_once 'travellog/components/profile/controller/Class.ProfileCompFactory.php';
		
		$component = ProfileCompFactory::getInstance()->create(ProfileCompFactory::$TRAVELER_CONTEXT);
		$component->init((int) $traveler->getID());
		
		return $component;
	}
	
	private function getTravelerSubNavigation(gaTraveler $traveler) {
		require_once 'travellog/model/Class.SubNavigation.php';
		$subNavigation = new SubNavigation;
		
		$subNavigation->setContext("TRAVELER");
		$subNavigation->setContextID((int) $traveler->getID());
		$subNavigation->setLinkToHighlight('MESSAGES');
			
		return $subNavigation;
	}
	
	private function requireAuthentication() {
		if (!(isset($_SESSION['travelerID']) && $_SESSION['travelerID'] != 0)) {
			header("location: /login.php?failedLogin&redirect=/messages.php?" . $_SERVER['QUERY_STRING']);
			exit;
		}
	}
	
	
	// ################# EMAIL NOTIFICATIONS ####################
	
	private static function sendEmailNotification($newMessage){
		require_once("travellog/helper/Class.MessageHelper.php");
		require_once('travellog/model/notifications/Class.Notification.php');
		require_once('travellog/model/notifications/Class.NotificationComposer.php');
		require_once('travellog/model/notifications/Class.NotificationSender.php');
	//	var_dump($newMessage);
		$mHelper = MessageHelper::getInstance();
		$mData = $mHelper->readMessageData();
	//	var_dump($mData);
		$domainConfig 	= SiteContext::getInstance();
		$emailName		= $domainConfig->getEmailName();
		$siteName 		= preg_replace(array('/^(the)\s/i','/\s(team)$/i'),'',$emailName);
		$siteUrl 		= $domainConfig->getServerName();
		
		$includeMessage = (isset($mData['sendNotification']) ? TRUE : FALSE);
		$footerVars = array(
			'siteName'	=> $siteName,
			'siteUrl'	=> $siteUrl
		);
		
		var_dump($newMessage);
		/*
		$sender = gaTravelerMapper::getTraveler($newMessage->getSenderID());
	//	var_dump($messageSource);
	//	var_dump($messageSource->getEMailAddress());
	//	require_once('class.phpmailer.php');
	
	//	var_dump($mail->ErrorInfo);
		foreach($newMessage->getRecipientIDs() as $recipientID){
			$recipient = gaTravelerMapper::getTraveler($recipientID);
			$vars = array_merge($footerVars, array(
				'mData'			=> $mData,
				'sender'		=> $sender,
				'recipient'		=> $recipient,
				'emailName'		=> $emailName,
				'isMessageIncluded' => $includeMessage,
				'notificationFooter'=> NotificationComposer::compose($footerVars,'tpl.IncNotificationFooter.php')
			));
		/*	$body = NotificationComposer::compose($vars, 'tpl.NewMessageCenterNotificationTemplate.php');
			$mail = new PHPMailer();  
				$mail->isHTML(true);  
				$mail->IsSMTP();
				$mail->From = "admin@goabroad.com";
				$mail->Host     = "mail.goabroad.com";
				//foreach($sendTo AS $to){
				$mail->AddAddress('nash.lesigon@goabroad.com', ''); 
				//}
				//unset($to);

				$mail->Subject = 'You have a new message on '.$siteName;
				$mail->Body     = $body;
			//	var_dump($mail);
			//	$x = $mail->Send();
				if(!$mail->Send()) {  
					//	var_dump($mail);
					echo "ERROR ".$mail->ErrorInfo;
				}
				*/
			/*
			$notification = new Notification;
			$notification->setMessage(NotificationComposer::compose($vars, 'tpl.NewMessageCenterNotificationTemplate.php'));
			$notification->setSubject('You have a new message on '.$siteName);
			$notification->setSender($domainConfig->getAdminEmail());
			$notification->setSenderName($emailName);
			$notification->addRecipient($recipient->getEMailAddress());
			
		
		//	$mNotifications[] = $notification;
		}
		
		
		NotificationSender::getInstance()->send($mNotifications,false); */
		
	}
	
}