<?php
	/**
	 * Created By: Cheryl Ivy Q. Go
	 * 27 November 2007
	 *Class.AbstractEditGATravelBioController.php
	 */

	require_once 'travellog/factory/Class.FileFactory.php';
	require_once 'travellog/controller/Class.IController.php';

	abstract class AbstractEditGATravelBioController implements IController{

		public 
	
		$obj_session  = NULL,
	
		$file_factory = NULL;

		function __construct(){
			$this->file_factory = FileFactory::getInstance();
			//$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/views'));
			//$this->file_factory->registerClass('Answer' , array('path' => 'travellog/model/travelbio/'));
			//$this->file_factory->registerClass('TravelBioUser' , array('path' => 'travellog/model/travelbio/'));
			//$this->file_factory->registerClass('ProfileQuestion', array('path' => 'travellog/model/travelbio/'));
			//$this->file_factory->registerClass('TravelBioManager', array('path' => 'travellog/model/travelbio/'));

			$this->file_factory->registerClass('SessionManager', array("path" => "travellog/model/"));
				
			//$this->file_factory->registerClass('Template', array('path'=>''));
			//$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('ToolMan');

			//$this->file_factory->setPath('CSS', '/css/');
			//$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');

			//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
		}

		function performAction(){
		
			$sessMan = $this->file_factory->invokeStaticClass('SessionManager','getInstance',array());
			$travelerID = $sessMan->get('travelerID');

			if(0 == $travelerID){
				ToolMan::redirect('/login.php');
			}
			
			//Temporary redirect HAM 14Nov2008 			
			header('location: /widget.php?travelerID='.$travelerID.'&action=editMTB');
			exit;
			
			// insert code for logger system
			require_once('gaLogs/Class.GaLogger.php');
			GaLogger::create()->start($travelerID,'TRAVELER');
			
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			$tpl = $this->file_factory->getClass('Template');
			$tpl->set_path('travellog/views/travelbio/');

			// get the travelbio user
			$travelBioUser = $this->file_factory->invokeStaticClass('TravelBioUser', 'getTravelBioUserByTravelerID', array($travelerID));

			//check if ready to save
			if(array_key_exists('submit', $_POST)){

				//process the answers
				if(is_null($travelBioUser)){
					$travelBioUser = $this->file_factory->getClass('TravelBioUser');
					$travelBioUser->setTravelerID($travelerID);
					$travelBioUser->Save();
				}

				/****
				* Parts of the name of the form variables
				* 0 = prefix (stated above)
				* 1 = questionID
				****/

				require_once 'travellog/model/travelbio/Class.AnswerValue.php';
				$arPrefixes = array(
					'txt'=> AnswerValue::TEXT,
					'txa'=> AnswerValue::TEXTAREA,
					'chk'=> AnswerValue::CHECKBOX,
					'txtchk'=> AnswerValue::TEXT
				);

				//add the answers		
				$travelBioUser->clearAnswers();	
				$answerRepo = array();
				$withAns = false;
				$withOthers = false;
				foreach($_POST as $key => $val){
					$part = explode('_',$key);
					if(array_key_exists($part[0],$arPrefixes)){
						if((is_string($val) && strlen($val)) || is_array($val)){					
							$answer = $this->file_factory->getClass('Answer');
							$answer->setProfileQuestionID($part[1]);
							$answer->setTravelBioUserID($travelBioUser->getTravelBioUserID());
							$val = (is_array($val))?$val:array($val);
							foreach($val as $iVal){
								if('Others'==$iVal){ $withOthers = true; }
								if( strlen($iVal) ){
									if( ( 'txtchk'==$part[0] && $withOthers) || 'txtchk' !=$part[0] ){
										$withAns = true;
										$answer->addValue($arPrefixes[$part[0]],$iVal);
									}
								}
							}						
							if(!array_key_exists($part[1],$answerRepo)){
								$answerRepo[$part[1]] = $answer;
							}
							else{
								if( ( 'txtchk'==$part[0] && $withOthers) || 'txtchk' !=$part[0] ){
									$answerRepo[$part[1]]->addValue($arPrefixes[$part[0]],$iVal);
								}
							}	
						}
					}
				}		
				foreach($answerRepo as $iAns){
					$iAns->save();
				}
				$isSync	= $travelBioUser->isSynchronized();

				$this->file_factory->invokeStaticClass('TravelBioManager', 'refreshFbUserProfileBox', array($travelBioUser));
				$tpl->set('tplAction','SHOW_SUMMARY');
			}
			else{		
				//get questions
				$profileQuestions = $this->file_factory->invokeStaticClass('ProfileQuestion', 'getQuestions');
				if(is_null($travelBioUser)){
					$isSync = false;
					$labelAction = 'Add Travel Bio';
					$answers = array();
				}
				else{
					$labelAction = 'Edit Travel Bio';
					$isSync	= $travelBioUser->isSynchronized();
					$answers = $travelBioUser->getAnswers();
				}		

				$structAns = array();
				foreach($answers as $iAns){
					if(!array_key_exists($iAns->getProfileQuestionID(),$structAns)){
						$structAns[$iAns->getProfileQuestionID()] = $iAns->getValues();
					}
				}
				$tpl->set('profileQuestions',$profileQuestions);		
				$tpl->set('structAns',$structAns);
				$tpl->set('tplAction','SHOW_FORM');
				$tpl->set('labelAction',$labelAction);
			}
			$subNav = $this->file_factory->getClass('SubNavigation');
			$subNav->setContext('TRAVELER');
			$subNav->setContextID($travelerID);
			$subNav->setLinkToHighlight('MY_PROFILE');	
			$tpl->set('travelerID',$travelerID);
			$tpl->set('isSync',$isSync);
			$tpl->set('subNav',$subNav);
			$tpl->out('tpl.EditGaTravelBio.php');
		}
	}
?>