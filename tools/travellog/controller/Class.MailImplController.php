<?php
require_once("travellog/controller/Class.gaIObserver.php");
class MailImplController implements gaIObserver{
	function notify(gaAbstractEvent $evt){
		$props = $evt->getData();
		switch($props['action']){
			case gaActionType::$SAVE:
				$this->sendMail($props);
			break; 
		}
	}
	
	private function sendMail($props){
		require_once("travellog/model/Class.PendingNotification.php");
		if ( $this->proceedToSend($props) ){
			require_once("travellog/model/Class.AdminGroup.php");
			if( !array_key_exists('obj_traveler', $props) ){
				require_once("travellog/model/Class.Traveler.php");
				$props['obj_traveler'] = new Traveler($props['travelerID']);	
			}
			
			if( $props['obj_traveler']->isAdvisor() ){
				$context   = PendingNotification::GROUP_CONTEXT;
				$contextID = AdminGroup::getAdvisorGroupID($props['travelerID']);
			}
			else{
				$context   = PendingNotification::TRAVELER_CONTEXT;
				$contextID = $props['travelerID'];
			}
			
			$notification = new PendingNotification();
			$notification->setContext($context);//context (GROUP_CONTEXT or TRAVELER_CONTEXT)
			$notification->setContextID($contextID);//contextID travelerID or GroupID
			$notification->setSection('JOURNAL_ENTRY');
			$notification->setAction('ADD');
			$notification->setGenID($props['travellogID']);
			$notification->setLink("http://".$_SERVER['HTTP_HOST']."/journal-entry.php?action=view&travellogID=".$props['travellogID']);//link to the journal entry
			
			
			
			if( $props['groupmembersaddressbook'] ) $notification->addRecipient(PendingNotificationRecipient::GROUP_MEMBERS_ADDRESS_BOOK_ENTRIES);
			if( $props['groupmembers']            ) $notification->addRecipient(PendingNotificationRecipient::GROUP_MEMBERS);
			if( $props['addressbook']             ) $notification->addRecipient(PendingNotificationRecipient::ADDRESS_BOOK_ENTRIES);
			$notification->save();
		}
	} 
	
	private function proceedToSend($props){
		return ( (isset($props['groupmembersaddressbook']) && $props['groupmembersaddressbook']) || (isset($props['groupmembers']) && $props['groupmembers']) || (isset($props['addressbook']) && $props['addressbook']) )? true: false;   
	}
}
?>
