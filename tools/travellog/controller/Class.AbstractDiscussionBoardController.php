<?php
	require_once('travellog/factory/Class.FileFactory.php');
	
	abstract class AbstractDiscussionBoardController {
		
		/**
		 * Registers the template files and paths of the classes.
		 */
		function __construct(){
			$file_factory = FileFactory::getInstance();
			$file_factory->registerClass('ProfileCompFactory', array('path' => 'travellog/components/profile/controller/'));
			$file_factory->registerClass('SubNavigation');
			$file_factory->registerClass('Template', array('path' => ''));
			$file_factory->registerTemplate('LayoutMain');
			$file_factory->setPath('CSS', '/css/');
			$file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			$file_factory->getClass("Template");
			Template::includeDependentCss("/css/vsgStandardized.css");
			Template::includeDependentCss("/css/modalBox.css");
			Template::includeDependentCss('/css/Thickbox.css');
			Template::includeDependentJs('/js/thickbox.js');
		}
		
		/**
		 * Shows the home discussion board of a group.
		 * 
		 * @param int $id The ID of the group
		 * 
		 * @return void
		 */
		function home($id = 0){			
			
			require_once('travellog/action/discussion_board/Class.Home.php');
			$params = array();
			$params['group_id'] = $id;			
			$action = new Home($params);
			
			$action->execute();
		}
		
		/**
		 * Activates the __call magic function. 
		 */
		function __call($method, $arguments){
			require_once('travellog/action/Class.ControllerAction.php');
			
			ControllerAction::showFileNotFound();
		}
	}