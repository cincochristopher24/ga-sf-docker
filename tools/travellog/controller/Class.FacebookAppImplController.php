<?php
require_once("travellog/controller/Class.gaIObserver.php");

class FacebookAppImplController implements gaIObserver{
	
	function notify(gaAbstractEvent $evt){
		$props = $evt->getData();
		switch($props['action']){
			case gaActionType::$SAVE:
				$this->save($props);
			break; 
			case gaActionType::$UPDATE:
				$this->update($props);
			break;
			case gaActionType::$DELETE:
				$this->delete($props);
			break;  
		}
	}
	
	private function save($props){
		require_once('travellog/rules/Class.FacebookAppActionRulesFactory.php');
		require_once('travellog/model/Class.TravelSchedule.php');
		$props['travelWishID'] = TravelSchedule::getTravelWishIDByTravelID( $props['travelID'] );
		$factory               = FacebookAppActionRulesFactory::getInstance();
		$rules                 = $factory->createRules( $props );
		
		//$rules['FacebookAppSaveRules']->applyRules($props); 
	}
	
	private function update($props){
		require_once('travellog/rules/Class.FacebookAppActionRulesFactory.php');
		require_once('travellog/model/Class.TravelSchedule.php');
		$props['travelWishID'] = TravelSchedule::getTravelWishIDByTravelID( $props['travelID'] );
		$factory               = FacebookAppActionRulesFactory::getInstance();
		$rules                 = $factory->createRules( $props );
		
		//$rules['FacebookAppUpdateRules']->applyRules($props); 
	}
	
	private function delete($props){
		require_once('travellog/rules/Class.FacebookAppActionRulesFactory.php');
		require_once('travellog/model/Class.TravelSchedule.php');
		$props['travelWishID'] = TravelSchedule::getTravelWishIDByTravelID( $props['travelID'] );
		$factory               = FacebookAppActionRulesFactory::getInstance();
		$rules                 = $factory->createRules( $props );
		
		//$rules['FacebookAppDeleteRules']->applyRules($props); 
	}

}
?>
