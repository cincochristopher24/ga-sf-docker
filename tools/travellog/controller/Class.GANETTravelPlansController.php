<?php
/*
	Filename:		Class.GANETTravelPlansController.php
	Author:			Jonas Tandinco
	Date Created:	Nov/12/2007
	Putpose:		controller implementation for pure GoAbroad.net functionality

	EDIT HISTORY:
*/

require_once('travellog/controller/Class.AbstractTravelPlansController.php');

class GANETTravelPlansController extends AbstractTravelPlansController{
	public function performAction(){
		parent::performAction();
	}
}
?>