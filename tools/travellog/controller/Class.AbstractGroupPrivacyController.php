<?php

	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/controller/Class.IController.php");
	require_once("travellog/factory/Class.FileFactory.php");
	
	abstract class AbstractGroupPrivacyController implements iController{
		
		/***********************************************************************
		 * edits of neri:
		 * 		redirected user to login.php if not logged in and 
		 * 			tries to access the preference page in
		 * 			performAction()									01-04-09	
		 * 		set value of receiveUpdates as 5 or 6, where 5 
		 * 			is the default value in _updatePrivacy():		01-09-09
		 * 		set the current page parameter to edit privacy 
		 * 			preference in _showGroupPrivacy():				11-10-08
		 * 		registered the class used by the profile component
		 * 			in the constructor:								11-06-08
		 * 		highlighted the group name in the sub-navigation
		 * 			in _defineCommonAttributes():					11-06-08
		 * 		added code for displaying the profile component in
		 * 			_showGroupPrivacy():							11-04-08
		 ***********************************************************************/
		
		protected 

		$obj_session  = NULL,

		$file_factory = NULL,
		
		$loggedUser  = NULL,
		
		$isLogged	  = false,
		
		$isMemberLogged = false,
		
		$isAdminLogged = false,
		
		$group 			= NULL,
		
		$backLink		= NULL,
		
		$tpl			= NULL;
		
		function __construct(){
			
			$this->obj_session = SessionManager::getInstance();
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template' , array( 'path' => '' ));
			
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('PrivacyPreferenceType');
			$this->file_factory->registerClass('GroupPrivacyPreference');
			 
			$this->file_factory->registerClass('ProfileCompFactory' , array('path' => 'travellog/components/profile/controller/'));
			$this->file_factory->registerClass('RandomTravelBioView', array('path' => 'travellog/views/'));
			
			$this->file_factory->registerClass('SubNavigation');
			
			$this->file_factory->registerTemplate('LayoutMain');
			
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
				
		}
		
		function performAction(){
			
			$params = array();
			$params['gID'] = ( isset ($_POST['groupID'])) ? $_POST['groupID'] : 0;
			$params['gID'] = ( isset ($_REQUEST['groupID'])) ? $_REQUEST['groupID'] : $params['gID'];
			$params['action'] = ( isset ($_REQUEST['action'])) ? $_REQUEST['action'] : 'showprivacy';
			
			if(!$this->obj_session->getLogIn()){
				header("location: login.php?failedLogin&redirect=group-privacy.php?".$_SERVER['QUERY_STRING']);
				exit;			 
			}
			
			switch($params['action']) {
				
				case "updateprivacy":
					$this->_updatePrivacy($params);
					break;
				default:
					$this->_showGroupPrivacy($params);
			}
			
		}
		
		
		function _defineCommonAttributes($_params = array()){
			
			//check if user is currently logged in
			if ($this->obj_session->get('travelerID') > 0) {
				$this->isLogged		=	true ;
				$this->loggedUser = $this->file_factory->getClass ('Traveler',array($this->obj_session->get('travelerID')));
			
			}
	
			// if groupID is defined, instantiate group and check if logged user is member or adminstrator
			if (0 < $_params['gID']){
				$mGroup = $this->file_factory->invokeStaticClass ('GroupFactory','instance')->create(array($_params['gID']));
				$this->group = $mGroup[0];

				//$this->isAdminLogged	=	$this->group->getAdministratorID() == $this->obj_session->get('travelerID') ? true : false;
				$this->isAdminLogged	=	( $this->group->getAdministratorID() == $this->obj_session->get('travelerID') || $this->group->isStaff($this->obj_session->get('travelerID'))) ? true : false;
				
				$this->subNav = $this->file_factory->getClass('SubNavigation');
				$this->subNav->setLinkToHighlight('GROUP_NAME');		
				$this->subNav->setContext('GROUP');
				$this->subNav->setContextID($_params['gID']);
			
			}
			
			//  create generic template
				$this->tpl = $this->file_factory->getClass('Template');
				$this->tpl->set_path("travellog/views/");
				
				$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain'))); 
				
		}
		
		function _allowAdminPrivileges($_privilege = false){
			if (!$_privilege){
				header("location:index.php");
				exit;
			}
		}
		
		
		function _updatePrivacy($_params = array() ) {
			
			$this->_defineCommonAttributes($_params);
			
			$this->_allowAdminPrivileges(isset($this->group) && $this->isAdminLogged);
			
			$showAlerts = false;
			$showBulletin = false;
			$showFiles = false;
			$showGroups = false;
			$showCalendar = false;
			$receiveUpdates = 5;
			$autoGroupJournals = true;
				
			$showAlerts = (isset($_REQUEST['alerts']) &&  'public' ==  $_REQUEST['alerts'] ) ? true : false;			

			$showFiles = (isset($_REQUEST['files']) && 'public' ==  $_REQUEST['files']) ? true : false;

			$showBulletin = (isset($_REQUEST['bulletin']) && 'public' == $_REQUEST['bulletin']) ? true : false;

			$showGroups = (isset($_REQUEST['groups']) && 'public' == $_REQUEST['groups']) ? true : false;	

			$showCalendar = (isset($_REQUEST['calendar']) && 'public' == $_REQUEST['calendar']) ? true :  false;				

			$receiveUpdates = (isset($_REQUEST['updates']) && 'receive' == $_REQUEST['updates']) ? 5 : 6;
			
			$autoGroupJournals = (isset($_REQUEST['journals']) && 'manual' == $_REQUEST['journals']) ? false : true;

			$gpp = $this->file_factory->getClass('GroupPrivacyPreference');
			$gpp->setGroupID($this->group->getGroupID());
			$gpp->setViewAlerts($showAlerts);
			$gpp->setDownloadFiles($showFiles);
			$gpp->setViewGroups($showGroups);
			$gpp->setViewBulletin($showBulletin);
			$gpp->setViewCalendar($showCalendar);
			$gpp->setModeOfReceivingUpdates($receiveUpdates);
			$gpp->setAutoModeGroupJournals($autoGroupJournals);
			$gpp->update();
			
			
			$_params['message'] = 'Privacy Preferences updated.';
			
			$this->_showGroupPrivacy($_params);
			
		}
		
		function _showGroupPrivacy($_params = array()) {

			$this->_defineCommonAttributes($_params);
			
			$this->_allowAdminPrivileges($this->isAdminLogged);
			
			// set group privacy preference
		  	$this->tpl->set("gpp",$this->group->getPrivacyPreference());
		  	$this->tpl->set("group",$this->group);
		  	$this->tpl->set('snav',$this->subNav);
		  	
		  	$profile_comp = $this->file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
			$profile_comp->init($this->group->getGroupID(), AbstractProfileCompController::$EDIT_PRIVACY_PREFERENCES);
			$this->tpl->set('profile', $profile_comp->get_view());
		  	if (isset($_params['message'])){
				$this->tpl->set('message',$_params['message'] );
			}
		
		  	$this->tpl->out('tpl.GroupPrivacyPreference.php');
		  
		}
		
	}
?>