<?php
	
	/*
	 * Class.AbstractRegisterController.php
	 * Created on Nov 19, 2007
	 * created by marc
	 */
	
	// require_once("travellog_/domains/" . $_SERVER['HTTP_HOST'] . ".php");
	require_once("travellog/factory/Class.FileFactory.php");
	require_once("travellog/controller/Class.IController.php");
	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/helper/Class.RegisterHelper.php");
	require_once("Class.Constants.php"); 
	require_once("travellog/rules/Class.TravelerAccountRules.php");
	require_once("travellog/model/Class.TravelerPeer.php");   
	// added by bert
	require_once("travellog/model/socialApps/mytraveljournals/Class.WebServiceUser.php");
	 
	class AbstractRegisterController implements IController{
		
		protected $file_factory = NULL;
		protected $session_manager = NULL;
		
		function __construct(){
			
			$this->file_factory = Filefactory::getInstance();
			$this->session_manager = SessionManager::getInstance();
			$this->register_helper = RegisterHelper::getInstance();
			
			if( $this->session_manager->getLogIn() ){
				if( $this->session_manager->get("LAST_OAUTH_ACTION") && "LINK" == $this->session_manager->get("LAST_OAUTH_ACTION") ){
					$this->linkTravelerProfile();
				}else{
					$session = session::getInstance($this->session_manager->get('travelerID'));
					$session->killSession();

					session_unset();
					session_destroy();
					$_SESSION = array();	

					if( isset($_COOKIE['login']) )
						unset( $_COOKIE['login']);
				}
			}
			
			$this->file_factory->registerClass('Traveler', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('MailGenerator', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('HelpText', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('Crypt', array('path' => ''));
			$this->file_factory->registerClass('GroupFactory', array('path' => 'travellog/model/'));
			$this->file_factory->registerClass('RegisterFcd'  , array('path' => 'travellog/facade/'));
			$this->file_factory->registerClass('MainLayoutView', array('path' => 'travellog/views/'));
			$this->file_factory->registerClass('RegisterLoginController', array('path' => 'travellog/controller/'));	
			$this->file_factory->registerClass('Captcha', array('path' => 'captcha/'));
			$this->file_factory->registerClass('Template', array('path' => ''));
			$this->file_factory->registerClass('Connection', array('path' => ''));
			$this->file_factory->registerClass('Recordset', array('path' => ''));
			$this->file_factory->registerClass('phpmailer', array('path' => ''));
			$this->file_factory->registerClass('Crypt', array('path' => ''));
			$this->file_factory->registerClass('Validator', array('path' => ''));
			
			// views
			$this->file_factory->registerTemplate('LayoutMain');
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			
		}
		
		function performAction(){
			
			switch( $this->register_helper->getAction() ){
				
				case constants::FILL_UP:
					return $this->viewRegistrationForm();
					break;
				
				case constants::SUBMIT_DETAILS:
					return $this->submitDetails();
					break;
					
				case constants::CONFIRM_DETAILS:
					return $this->registerUser();
					break;		
					
				case constants::BACK:
					return $this->modifyDetails();
					break;
						
				default:
					$this->viewChooseRegister();
			}
			
		}
		
		function viewChooseRegister(){
			
			$ht = $this->file_factory->getClass("HelpText");
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			
			$nContent = array(
				"advisorQuestion" => $ht->getHelpText('advisor-register-help-head'),
				"advisorAnswer" => $ht->getHelpText('advisor-register-help-content')
			);
			
			Template::setMainTemplateVar('layoutID', 'main_pages');
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );  
			
			$fcd = $this->file_factory->getClass('RegisterFcd', $nContent, false);
			$obj_view->setContents( array('contents' => $fcd->retrieveChooseRegister()) );
			$obj_view->render();
		}
		
		function validateUserDetails($content = array()){
			
			$arrError= array();
			$arrErrorMessage = array();
			
			$arrErrorMessage = TravelerAccountRules::getUsernameErrorMessage($content["username"]);
			
			if( !("" == $content["password"] && "" == $content["pwdConfirm"] && !is_null($content["service_provider"])) ){
				$arrErrorMessage = $arrErrorMessage + TravelerAccountRules::getPasswordErrorMessage($content["password"], $content["pwdConfirm"]);
			}		
			$arrErrorMessage = $arrErrorMessage + TravelerAccountRules::getEmailErrorMessage($content["emailAdd"]);
			
			// agree with terms of use
			if ( !$content["agreeTerm"] )
				$arrErrorMessage[constants::DISAGREE_TERMS] = "You need to agree with the terms of use before you can start sharing your travels.";
			
			// captcha
			if( is_null($content["service_provider"]) && !($this->file_factory->invokeStaticClass("Captcha","validate",array($content["code"],$content["securityCode"])) ))
				$arrErrorMessage[constants::SECURITY_CODE_MISMATCH] = "Sorry, the code you have entered is incorrect. Please try again.";
			
			return $arrErrorMessage;	
		}

		function showRegistrationDetails($userData = array()){
			
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			
			Template::setMainTemplateVar('layoutID', 'main_pages');
			Template::setMainTemplate( $this->file_factory->getTemplate('LayoutMain') );  
			$fcd = $this->file_factory->getClass('RegisterFcd', $userData, false);
			$obj_view->setContents( array('contents' => $fcd->retrieveRegistrationDetails()) );
			$obj_view->render();
			
		}
		
		function registerUser(){
			
			$conn = $this->file_factory->getClass("Connection");
			$rs = $this->file_factory->getClass("Recordset",array($conn));
			
			$travelerData = $this->register_helper->readTravelerData();
			
			$sqlquery = "SELECT * FROM tblTmpRegister " .
				"WHERE registerID = '" . mysql_real_escape_string($travelerData["regID"]) . "' " .  
				" AND regKey = '" . mysql_real_escape_string($travelerData["regKey"]) . "'";
			$myquery = $rs->Execute($sqlquery);
			
			if($tmpRow = mysql_fetch_array($myquery)){
				$arrTrav = $this->saveUserDetails($tmpRow);		
				$crypt = $this->file_factory->getClass("Crypt");
				$cryptTravID = $crypt->encrypt($arrTrav['travelerID']);
				$cryptTravID = substr($cryptTravID,0,strlen($cryptTravID)-1);
				$myCrypt = array();
				$myCrypt["ref"] = $cryptTravID;  
				$registerLoginController = $this->file_factory->getClass('RegisterLoginController', array($myCrypt) );
				return $registerLoginController->performAction(); 
			}
			else{
				header("location:" . $this->register_helper->getRedirectPage(constants::REGISTER_PAGE));
			}
		
		}
		
		function saveUserDetails($tmpRow){
			
			$conn = $this->file_factory->getClass("Connection");
			$rs = $this->file_factory->getClass("Recordset",array($conn));
						
			$rs->execute("DELETE FROM tblTmpRegister " .
				"WHERE registerID = '" . mysql_real_escape_string($tmpRow["registerID"]) . "' " .  
				"AND regKey = '" . mysql_real_escape_string($tmpRow["regKey"]) . "'");
			$sqlquery = "INSERT INTO tblSendable(sendabletype) VALUES(1)";
			$myquery = $rs->Execute($sqlquery); 
			$sendableID = mysql_insert_id($conn->GetConnection()); 
			$sqlquery = "INSERT INTO tblTraveler SET " .
					"firstname = '" . mysql_real_escape_string($tmpRow["genName"]) . "', " .
					"lastname = '" . mysql_real_escape_string($tmpRow["lastName"]) . "', " . 
					"username = '" . mysql_real_escape_string($tmpRow["username"]) . "', " .
					"password = '" . mysql_real_escape_string($tmpRow["password"]) . "', " .
					"email = '" . mysql_real_escape_string($tmpRow["email"]) . "', " . 
					"university = '" . mysql_real_escape_string($tmpRow["university"]) . "', " . 
					"sendableID = $sendableID, " .
					"dateregistered = now()"; //dateregistered added by ianne - 11/25/2008
			$myquery = $rs->Execute($sqlquery);
			$travelerID = mysql_insert_id();
			/**
			 * get the travelerID of the user
			 * insert needed information in database for forums
			 */
						
			// insert traveler ranking
			$sqlquery = "INSERT INTO tblTravelerRanking SET travelerID = $travelerID";
			$rs->Execute($sqlquery);	
			
			// insert privacy preference
			$rs->Execute("INSERT INTO tblPrivacyPreference SET " .
					"preferencetype = 18, " .
					"preference = 5," .
					"travelerID = {$travelerID} ");		

			if ( FALSE === strpos($_SERVER["SERVER_NAME"],"dev") 
				&& FALSE === strpos($_SERVER["SERVER_NAME"],".local") 
				&& FALSE === strpos($_SERVER["SERVER_NAME"],"test.goabroad.net")
				&& FALSE === strpos($_SERVER["SERVER_NAME"],"staging.goabroad.net") )
			{
				// added by bert
				WebServiceUser::addTravelerToWebServiceUser($travelerID);
				// added by nash
				WebServiceUser::addTravelerToTravelBioUser($travelerID);
			}
			
			$this->saveCommonUserDetails($travelerID,$tmpRow["email"]);
			
			if($tmpRow["subscribeNL"])
				$this->subscribeNL($tmpRow["email"]);
				
			return array("travelerID" => $travelerID);
		
		}
		
		function saveCommonUserDetails($travelerID,$email,$cobrandData=NULL){
			
			$this->saveServiceProviderTravelerAccount($travelerID);
			
			$conn = $this->file_factory->getClass("Connection");
			$rs = $this->file_factory->getClass("Recordset",array($conn));
			
			/**
			 * remove user from invite list
			 */
			$sqlquery = "SELECT groupID FROM tblEmailInviteList WHERE email = '" . mysql_real_escape_string($email) . "'";
			$myquery = $rs->Execute($sqlquery);
			if( mysql_num_rows($myquery) ){
				$tmpTraveler = $this->file_factory->getClass("Traveler",array($travelerID));
				$factory = $this->file_factory->invokeStaticClass("GroupFactory","instance");
				while( $tmpGroupID = mysql_fetch_array($myquery) ){
					$mGroup = $factory->create( array($tmpGroupID["groupID"]) );
					$group = $mGroup[0];
					$group->addInvites( array($tmpTraveler) , $sendEmailNotif=FALSE);
					$group->removeFromEmailInviteList($email);
				}
			}
			 
			try{
				$mailGenerator = $this->file_factory->getClass("MailGenerator", array($travelerID,3,$cobrandData));
				$mailGenerator->send();	
			}
			catch(exception $e){}
		}
		
		function viewWelcomePage($user = constants::TRAVELER){
			
			$obj_view = $this->file_factory->getClass('MainLayoutView');
			
			Template::setMainTemplate("travellog/views/tpl.LayoutMain.php");
			Template::setMainTemplateVar('layoutID', 'main_pages');
			Template::setMainTemplateVar('page_location', 'Welcome To The GoAbroad Network');
			
			$fcd = $this->file_factory->getClass('RegisterFcd', array("user"=>$user), false);
			$obj_view->setContents( array('contents' => $fcd->retrieveWelcomePage(constants::VIEW_WELCOME_PAGE)) );
			$obj_view->render();
			
		}
		
		/**
		* if user wants to subscribe in GoAbroad Newsletter
		*/
			
		function subscribeNL($email,$entryPoint = 7){
			
			$myCurl = curl_init();
			// set URL and other appropriate options
			curl_setopt_array($myCurl, array(
					CURLOPT_URL 			=> 	"http://www.goabroad.com/submitemail.cfm",
					CURLOPT_HEADER			=>	false,
					CURLOPT_RETURNTRANSFER	=>	true,
					CURLOPT_POST			=>	true,
					CURLOPT_POSTFIELDS		=>	"email={$email}&frmEntryPoint={$entryPoint}"		
				));
			curl_exec($myCurl);	
			curl_close($myCurl);
		}
		
		function checkLogin(){
			if( $this->session_manager->getLogIn() ){
				
				
			}
		}
		
		protected function getServiceProviderAuthorization(){
			$context = isset($_GET["context"]) ? $_GET["context"] : NULL;
			$oauth_token = isset($_GET["oauth_token"]) ? $_GET["oauth_token"] : NULL;            
			
			if(!in_array($context,array("twitter","google","facebook")) || is_null($oauth_token) ){
				return FALSE;
			}
			
			$oauth_api = OAuthAPIFactory::getInstance()->create($context);
			$credentials = $oauth_api->getCredentials();
			if( !$credentials ){
				$credentials = $oauth_api->validateUser($oauth_token);
			}
			if( $credentials && $credentials["registered_traveler_id"] ){
				if( !$this->loginAuthorizedUser($credentials["registered_traveler_id"],$oauth_api) ){
					$oauth_api->clearCredentials();
					header("location: /");
					exit;
				}
			}
			
			return $credentials;
		}
		
		private function saveServiceProviderTravelerAccount($travelerID){
			if( isset($_POST["hdnAuthorizedSP"]) && !empty($_POST["hdnAuthorizedSP"])){
				$oauth_api = OAuthAPIFactory::getInstance()->create($_POST["hdnAuthorizedSP"]);
				if($oauth_api){
					$credentials = $oauth_api->getCredentials();
					$oauth_api->registerTravelerAccount($travelerID,$credentials["service_provider_userid"]);
					$oauth_api->clearCredentials();
				}	
			}
		}
		
		protected function linkTravelerProfile(){
			$this->session_manager->unsetVar("LAST_OAUTH_ACTION");
			
			$context = isset($_GET["context"]) ? $_GET["context"] : NULL;
			$oauth_token = isset($_GET["oauth_token"]) ? $_GET["oauth_token"] : NULL;
			if( !in_array($context,array("twitter","google","facebook")) || is_null($oauth_token) ){
				header("location: /login.php?redirect=/account-settings.php");
				exit;
			}
			
			$oauth_api = OAuthAPIFactory::getInstance()->create($context);
			$credentials = $oauth_api->getCredentials();
			if( !$credentials ){
				$credentials = $oauth_api->validateUser($oauth_token);
			}
			
			if( !$credentials["registered_traveler_id"] ){
				$oauth_api = OAuthAPIFactory::getInstance()->create($context);
				$oauth_api->linkTravelerProfile($this->session_manager->get("travelerID"),$credentials["service_provider_userid"]);
			}
			
			$oauth_api->clearCredentials();
			
			header("location: /account-settings.php");
			exit;
		}
	} 
?>