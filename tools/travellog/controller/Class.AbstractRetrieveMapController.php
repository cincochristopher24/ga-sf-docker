<?php
	
	require_once("travellog/controller/Class.IController.php");
	require_once("travellog/factory/Class.FileFactory.php");
	require_once("Class.Template.php");

	/**
	* Facilitates retrieving of google maps through ajax
	*/
	abstract class AbstractRetrieveMapController implements IController{
		
		protected $data = array();
		protected $obj_session = NULL;
		protected $file_factory = NULL;
		protected $mapHelper = NULL;
		
		public function __construct(){
			$this->obj_session  = SessionManager::getInstance();
			$this->file_factory = FileFactory::getInstance(); 

			$this->file_factory->registerClass('RetrieveMapHelper', array('path' => 'travellog/helper/'));
			$this->file_factory->registerClass('TravelLog');
		}
		
		function performAction(){
			$this->mapHelper = $this->file_factory->getClass('RetrieveMapHelper');
			switch($this->data['context']){
				case RetrieveMapHelper::MY_TRAVEL_MAP:
					$loggedTraveler = $this->obj_session->get('travelerID');
					$travelerID = $this->data['travelerID'];
					$isOwnerLogged = $loggedTraveler == $travelerID;
					$this->data['ID'] = $isOwnerLogged ? $loggedTraveler : $travelerID;
					echo $this->mapHelper->getMyTravelMap($this->data);
				break;
				
				case RetrieveMapHelper::JOURNAL_ENTRY_MAP:
					echo $this->mapHelper->getJournalEntryMap($this->data);
				break;
				
				case RetrieveMapHelper::JOURNALS_MAP:
					if(isset($GLOBALS['CONFIG']))
						$col_countries = $this->file_factory->invokeStaticClass( 'TravelLog', 'getCountriesWithLogs', array($GLOBALS['CONFIG']->getGroupID()) );
					else
						$col_countries = $this->file_factory->invokeStaticClass( 'TravelLog', 'getCountriesWithLogs' );
					echo $this->mapHelper->getJournalsMap($col_countries);
				break;
				
				case RetrieveMapHelper::ARTICLE_MAP:
					echo $this->mapHelper->getArticleMap($this->data);
				break;
				
				default: return;
			}
		}
		
	}