<?

require_once("travellog/helper/Class.HelperTraveler.php");
require_once("travellog/model/Class.FilterCriteria2.php");
require_once("travellog/model/Class.Traveler.php");
require_once("travellog/model/Class.Condition.php");
require_once('travellog/model/Class.HelpText.php');
require_once('Class.FormHelpers.php');
require_once ("travellog/model/Class.PrivacyPreference.php");
require_once("Class.Paging.php");
require_once("Class.Template.php");
require_once("Class.ObjectIterator.php");

class TravelerController
{
	
	private 
	
	$travelerID, 
	
	$locationID = 0,
	
	$page       = 1,
	
	$main_tpl   = false,
	
	$IsLogin    = false,
	
	$keywords   = NULL,
	
	$type       = NULL,
	
	$searchby   = NULL;
	
	function __construct()
	{
		$this->main_tpl = new Template();
	}
	
	function getControls($viewType)
	{
		
		$inc_tpl                  = clone $this->main_tpl;
		$travelers_count          = 0;
		$hometown_travelers_count = 0;
		$newest_traveler_count    = 0;
		$search_template          = NULL;
		$obj_help                 = new HelpText();
		if ($this->IsLogin):
			$obj_traveler = new Traveler($this->travelerID);
			if ( $obj_traveler->getPresentLocation() != NULL ):
				$hometown_travelers_count = 1;
			endif;
		endif; 
		$newest_travelers = Traveler::getCountNewestMembers();
		if ( $newest_travelers ):
			$newest_traveler_count = 1;
		endif;
		$obj_travelers = Traveler::getCountAllTravelers();
		if ( $obj_travelers ):
			$travelers_count  = 1;
		endif;

		$code = <<<BOF
<script type = "text/javascript">
//<![CDATA[
	var redirect = function(){
		if( document.getElementById('action').value != 'location' ){
			window.location.href = '/travelers.php?action='+document.getElementById('action').value;
			document.getElementById('COUNTRYSEARCH').style.display = 'none';
		}else document.getElementById('COUNTRYSEARCH').style.display = 'block';
	}
	
	var redirect1 = function(){
		if( document.getElementById('locationID').value > 0 )
			window.location.href = '/travelers.php?locationID='+document.getElementById('locationID').value;
	}
//]]>
</script>
BOF;
		Template::setMainTemplate('travellog/views/tpl.LayoutMain.php');
		Template::includeDependent($code);
	
		$this->main_tpl->set( 'travelers_count'          , $travelers_count                        );
		$this->main_tpl->set( 'obj_help'                 , $obj_help                               );
		$this->main_tpl->set( 'hometown_travelers_count' , $hometown_travelers_count               );
		$this->main_tpl->set( 'newest_traveler_count'    , $newest_traveler_count                  );
		$this->main_tpl->set( 'obj_present_locations'    , $this->getTravelersInPresentLocation() );
		// Appended title is not needed - icasimpan Apr 24, 2007
		if ( count($obj_travelers = Traveler::getAllTravelers()) ):
			$inc_tpl->set( 'keywords' , $this->keywords );
			$inc_tpl->set( 'searchby' , $this->searchby );
			$search_template = $inc_tpl->fetch('travellog/views/tpl.FrmTravelerSearch.php');
		endif;
		
		$this->type = $viewType;
		
		switch($viewType){
			case "1":
				$mainContent = $this->viewAll();
				break;
			case "2":
				$mainContent = $this->viewHometown();
				break;
			case "3":
				$mainContent = $this->viewNewest();
				break;
			case "4":
				$mainContent = $this->viewPopular();
				break;
			case "5":
				$mainContent = $this->viewLatest();
				break;	
			case "6":
				$mainContent = $this->viewAll();
				break;
			case "7":
				$mainContent = $this->viewAll();
				break;
		}
		
		
		$this->main_tpl->set('isLogin'        , $this->IsLogin);
		$this->main_tpl->set('incLocation'    , $this->getLocation());
		$this->main_tpl->set('viewType'       , $viewType );
		$this->main_tpl->set('mainContent'    , $mainContent);
		$this->main_tpl->set('search_template',$search_template);
		$this->main_tpl->out("travellog/views/tpl.Travelers.php");
	} 
	
	private function getTravelersInPresentLocation()
	{
		if ( $this->IsLogin ):
			$obj_traveler = new Traveler($this->travelerID);
			if ( $obj_traveler->getPresentLocation() != NULL ):
				return $obj_traveler->getOtherTravelersInPresentLocation();
			endif;
		else:
			return NULL;
		endif;
	}
	
	function viewAll()
	{
		require_once('travellog/model/Class.LocationFactory.php');
		require_once('travellog/model/Class.SearchFirstandLastnameService.php');
		require_once('travellog/model/Class.TravelerSearchCriteria2.php');
		require_once("Class.Criteria2.php");
		$obj_travelers = array();
		$filter        = NULL;
		$query_string  = '';
		$isAdvisor     = false;
		$offset        = ($this->page*12)-12; 
		$rows          = 12;
		$query_string  = 'action=all';
		if( $this->searchby != NULL ){
			$cond = new Condition;
			if ( $this->searchby == 1 )
				$cond->setAttributeName("username");
			elseif ( $this->searchby == 2 )
				$cond->setAttributeName("email");
			elseif ( $this->searchby == 3 )
				$cond->setAttributeName("interests");	
			
			
			$cond->setOperation(FilterOp::$LIKE);
			$cond->setValue($this->keywords);
			$filter = new FilterCriteria2();
			$filter->addBooleanOp('AND');
			$filter->addCondition($cond);
			if( strlen(trim($this->searchby)) ){ 
				$query_string = 'keywords='.$this->keywords.'&searchby='.$this->searchby;
			}
		}elseif ($this->locationID){
			$factory     = LocationFactory::instance();
			$obj_country = $factory->create($this->locationID);
			$cond        = new Condition;
			$obj_col     = $obj_country->getCities();
			$list        = array();
			
			if (count($obj_col)){
				foreach($obj_col as $obj){
					$list[] = $obj->getLocationID();		
				}
				$list[] = $this->locationID;
				if( $this->type == 7 )
					$cond->setAttributeName("htlocationID");
				else
					$cond->setAttributeName("currlocationID");
				$cond->setOperation(FilterOp::$IN);
				$cond->setValue(implode(',',$list));
				$filter = new FilterCriteria2();
				$filter->addBooleanOp('AND');
				$filter->addCondition($cond);
				
			}	
			$query_string = 'locationID='.$this->locationID;
		}
		
		if ($this->IsLogin){
			$obj_traveler  = new Traveler($this->travelerID);
			$this->main_tpl->set( 'obj_friend' , $obj_traveler );
			
			if ($obj_traveler->isAdvisor()) $isAdvisor = true;
			
			if ( $this->searchby == 4 ){
				$obj_search          = new SearchFirstandLastnameService(); 	
				$dummy_obj_travelers = $obj_search->performSearch($this->keywords);
			}else{
				if ($filter==NULL){
					$myobj               = new TravelerSearchCriteria2;
					$c                   = new Criteria2();
					$c->mustNotBeEqual('travelerID', $this->travelerID);
					$c->setOrderBy    ('ranking DESC');
					$c->setLimit($offset.','.$rows);                
					$obj_travelers = $myobj->getAllTravelers($c);
				}else{ 
					$obj_travelers = Traveler::getAllTravelers($filter);
				}
			}
		}else{
			if ( $this->searchby == 4 ){
				$obj_search    = new SearchFirstandLastnameService(); 	
				$obj_travelers = $obj_search->performSearch($this->keywords);
			}else{
				if ($filter==NULL){
					$myobj         = new TravelerSearchCriteria2;
					$c             = new Criteria2();
					$c->setOrderBy('ranking DESC');
					$c->setLimit  ($offset.','.$rows);          
					$obj_travelers = $myobj->getAllTravelers($c);
				}else{ 
					$obj_travelers = Traveler::getAllTravelers($filter);
				}
			}
		}
				
		if( isset($myobj) ){ 
			$obj_paging    = new Paging( $myobj->getTotalRecords(),$this->page,$query_string,12 );
			$obj_iterator  = new ObjectIterator($obj_travelers, $obj_paging, true);
		}else{
			$obj_paging    = new Paging( count($obj_travelers),$this->page,$query_string,12 );
			$obj_iterator  = new ObjectIterator($obj_travelers,$obj_paging);
		}
		
		$this->main_tpl->set( 'obj_paging'   , $obj_paging   );
		$this->main_tpl->set( 'obj_iterator' , $obj_iterator );
		$this->main_tpl->set( 'selected'     , 1             );
		$this->main_tpl->set( 'travelerID', $this->travelerID );
		$this->main_tpl->set( 'isLogin'   , $this->IsLogin );
		$this->main_tpl->set( 'isAdvisor' , $isAdvisor );
		return $this->main_tpl->fetch("travellog/views/tpl.ViewTravelersList.php");
	}
	
	function viewHometown()
	{
		$obj_travelers   = array();
		$isAdvisor       = false;
		if ($this->IsLogin):
			$obj_traveler        = new Traveler($this->travelerID);
			if ($obj_traveler->isAdvisor()) $isAdvisor = true;
			$dummy_obj_travelers = $obj_traveler->getOtherTravelersInMyHomeTown();
			if(count($dummy_obj_travelers)):
				foreach($dummy_obj_travelers as $dummy_obj_traveler):
					if(!$obj_traveler->isBlocked($dummy_obj_traveler->getTravelerID()) && $dummy_obj_traveler->getTravelerID() != $this->travelerID):
						$obj_travelers[] = $dummy_obj_traveler;	
					endif;
				endforeach;
			endif;
		else:
			$obj_traveler  = new Traveler($this->travelerID);
			$obj_travelers = $obj_traveler->getOtherTravelersInMyHomeTown();
		endif;
		$obj_paging     = new Paging( count($obj_travelers),$this->page,'action=myhometown',12 );
		$obj_iterator   = new ObjectIterator($obj_travelers,$obj_paging);
		$this->main_tpl->set( 'obj_paging'     , $obj_paging       );
		$this->main_tpl->set( 'obj_iterator'   , $obj_iterator     );
		$this->main_tpl->set( 'obj_traveler'   , $obj_traveler     );
		if ( $this->IsLogin ):
			$this->main_tpl->set( 'travelerID' , $this->travelerID );
			$this->main_tpl->set( 'isLogin'    , $this->IsLogin    );
			$this->main_tpl->set( 'obj_friend' , $obj_traveler     );
		endif;
		$this->main_tpl->set( 'selected'       , 2                 );
		$this->main_tpl->set( 'isAdvisor'      , $isAdvisor        );
		return $this->main_tpl->fetch("travellog/views/tpl.ViewTravelersList.php");
	}
	
	function viewNewest(){
		$obj_travelers   = array();
		$isAdvisor       = false;
		if ($this->IsLogin):
			$obj_traveler        = new Traveler($this->travelerID);
			if ($obj_traveler->isAdvisor()) $isAdvisor = true;
			$dummy_obj_travelers = Traveler::getNewestMembers();
			if(count($dummy_obj_travelers)):
				foreach($dummy_obj_travelers as $dummy_obj_traveler):
					if(!$obj_traveler->isBlocked($dummy_obj_traveler->getTravelerID()) && $dummy_obj_traveler->getTravelerID() != $this->travelerID):
						$obj_travelers[] = $dummy_obj_traveler;	
					endif;
				endforeach;
			endif;
		else:
			$obj_travelers  = Traveler::getNewestMembers();
		endif;
		if ($this->IsLogin):
			$obj_friend     = new Traveler($this->travelerID);
			$this->main_tpl->set( 'obj_friend' , $obj_friend );
		endif;
		$obj_paging     = new Paging( count($obj_travelers),$this->page,'action=newest',12 );
		$obj_iterator   = new ObjectIterator($obj_travelers,$obj_paging);
		$this->main_tpl->set( 'obj_paging'   , $obj_paging   );
		$this->main_tpl->set( 'obj_iterator' , $obj_iterator );
		$this->main_tpl->set( 'selected'     , 3             );
		$this->main_tpl->set( 'travelerID', $this->travelerID );
		$this->main_tpl->set( 'isLogin'   , $this->IsLogin );
		$this->main_tpl->set( 'isAdvisor' , $isAdvisor );
		return $this->main_tpl->fetch("travellog/views/tpl.ViewTravelersList.php");
	}
	
	function viewPopular(){
		require_once("travellog/model/Class.RowsLimit.php");
		$rows          = $this->page*12;
		$obj_travelers = array();
		$isAdvisor     = false;
		
		$obj_limit = new RowsLimit($rows,0);
		if ($this->IsLogin):
			$obj_traveler        = new Traveler($this->travelerID);
			if ($obj_traveler->isAdvisor()) $isAdvisor = true;
			$dummy_obj_travelers = Traveler::getMostPopular($obj_limit);
			if(count($dummy_obj_travelers)):
				foreach($dummy_obj_travelers as $dummy_obj_traveler):
					if(!$obj_traveler->isBlocked($dummy_obj_traveler->getTravelerID()) && $dummy_obj_traveler->getTravelerID() != $this->travelerID):
						$obj_travelers[] = $dummy_obj_traveler;	
					endif;
				endforeach;
			endif;
		else:
			$obj_travelers  = Traveler::getMostPopular($obj_limit);
		endif;
		if ($this->IsLogin):
			$obj_friend     = new Traveler($this->travelerID);
			$this->main_tpl->set( 'obj_friend' , $obj_friend );
		endif;
		$obj_paging     = new Paging( Traveler::$statvar,$this->page,'action=popular',12 );
		$obj_iterator   = new ObjectIterator($obj_travelers,$obj_paging);
		$this->main_tpl->set( 'obj_paging'   , $obj_paging   );
		$this->main_tpl->set( 'obj_iterator' , $obj_iterator );
		$this->main_tpl->set( 'selected'     , 3             );
		$this->main_tpl->set( 'travelerID', $this->travelerID );
		$this->main_tpl->set( 'isLogin'   , $this->IsLogin );
		$this->main_tpl->set( 'isAdvisor' , $isAdvisor );
		return $this->main_tpl->fetch("travellog/views/tpl.ViewTravelersList.php");
	}
	
	function viewLatest(){
		require_once('travellog/model/Class.TravelerSearchCriteria2.php');
		require_once("Class.Criteria2.php");
		$offset    = ($this->page*12)-12; 
		$rows      = 12;
		$isAdvisor = false;
		$myobj     = new TravelerSearchCriteria2;
		$c         = new Criteria2();
		$c->setOrderBy('latestlogin DESC');
		$c->setLimit  ($offset.','.$rows);    
		if ($this->IsLogin):
			$obj_traveler = new Traveler($this->travelerID);
			if ($obj_traveler->isAdvisor()) $isAdvisor = true;
			$this->main_tpl->set( 'obj_friend' , $obj_traveler );
			$c->mustNotBeEqual('travelerID', $this->travelerID);
		endif;
		$obj_travelers  = $myobj->getAllTravelers($c);
		$obj_paging     = new Paging( $myobj->getTotalRecords(),$this->page,'action=lastlogin',12 );
		$obj_iterator   = new ObjectIterator($obj_travelers,$obj_paging,true);
		
		$this->main_tpl->set( 'obj_paging'   , $obj_paging   );
		$this->main_tpl->set( 'obj_iterator' , $obj_iterator );
		$this->main_tpl->set( 'selected'     , 3             );
		$this->main_tpl->set( 'travelerID', $this->travelerID );
		$this->main_tpl->set( 'isLogin'   , $this->IsLogin );
		$this->main_tpl->set( 'isAdvisor' , $isAdvisor );
		return $this->main_tpl->fetch("travellog/views/tpl.ViewTravelersList.php");
	}
	
	function getLocation(){
		require_once("travellog/model/Class.SearchTravelerInLocationService.php");
		$objNewMember = new SearchTravelerInLocationService();
		$col_country  = $objNewMember->performSearch();
		$selected     = NULL;
		if (count($col_country)){
			$Return = '<select name="locationID" id="locationID" onchange="redirect1()">';
			foreach($col_country as $obj_country){
				if( $this->locationID == $obj_country->getLocationID() ) $selected = "selected";
				$Return .= '<option value="'.$obj_country->getLocationID().'" '.$selected.'>'.$obj_country->getName().'</option>';
				$selected     = NULL;
			}
			if( $this->locationID == 0 ) $selected = "selected";
			$Return .= '<option value="0" '.$selected.'>- choose a country -</option>';
			$Return .= '</select>';
		}
		return $Return;
	}
	
	function setTravelerID($travelerID)
	{
		$this->travelerID = $travelerID;
	}
	function setIsLogin($IsLogin)
	{
		$this->IsLogin = $IsLogin;
	}
	function setPage($page)
	{
		$this->page = $page;
	}
	function setSearchBy($searchby)
	{
		$this->searchby = $searchby;
	}
	function setKeywords($keywords)
	{
		$this->keywords = $keywords;
	}
	function setLocationID($locationID)
	{
		$this->locationID = $locationID;
	}
	
}
				
?>

