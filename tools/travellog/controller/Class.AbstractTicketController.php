<?php
/*
 * Created: December 17, 2008
 *
 * 
 */

	require_once('travellog/model/Class.SessionManager.php');
	require_once('travellog/controller/Class.IController.php');
	require_once('travellog/factory/Class.FileFactory.php');
	
	abstract class AbstractTicketController implements IController {
		
		protected 
		
		$info			= NULL,
		
		$errors			= NULL,
			
		$obj_session  	= NULL,

		$file_factory 	= NULL,
		
		$tpl 			= NULL;
			
			
		function __construct () {
			
			$this->obj_session = SessionManager::getInstance();
			$this->file_factory = FileFactory::getInstance();
			
			$this->file_factory->registerClass('Template' , array ( 'path' => '' ));
			$this->file_factory->registerTemplate('LayoutMain');
			
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			
			//require_once('travellog/domains/'.$_SERVER['SERVER_NAME'].'.php');
		}
		
		function performAction () {
			$params = array();
			
			$params['action'] 	= (isset($_GET['action'])) ? $_GET['action'] : "send";
			
			switch($params['action']){
				case "show_form":
					$this->_viewAddTicketForm();
					break;
				case "send"	:
					$this->_sendTicket();
					break;			
			}
			
		}
		
		function _init(){
			$this->file_factory->invokeStaticClass('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			
			$this->tpl = $this->file_factory->getClass('Template');
			$this->tpl->set_path('travellog/views/');
		}
		
		function _viewAddTicketForm(){
			$this->_init();
			$params = $this->getParams();
			
			$this->tpl->set('firstname',$params['firstname']);
			$this->tpl->set('lastname',$params['lastname']);
			$this->tpl->set('email',$params['email']);
			$this->tpl->set('subject',$params['subject']);
			$this->tpl->set('message',$params['message']);
			
			$ticket_text = (isset($GLOBALS['CONFIG']))	?	$GLOBALS['CONFIG']->getSiteName()	:	"GoAbroad Network";
			$this->tpl->set('site',$ticket_text);
			
			$this->tpl->set('errors',NULL);
			if(count($this->errors))
				$this->tpl->set('errors',$this->errors);
				
			$this->tpl->set('success',NULL);	
			if(isset($_GET['success']))	
				$this->tpl->set('success',"The ticket has been sent.");
			
			
			Template::setMainTemplateVar('title',$ticket_text.' - Error Ticket');
			$this->tpl->out('tpl.SendTicket.php');
		}
		
		function _sendTicket(){
			$params = $this->getParams();
			$continue = $this->_validate($params);
						
			if($continue){
				require_once("class.phpmailer.php");
				$this->_fetchClientInfo();
				//echo "<pre>".var_dump($this->info)."</pre>";exit;
				$msg = "Goabroad.net Error Ticket\n\n" .
					   $params['message'] ."\n\n".
					   "Browser:".$this->info['user_agent']  ."\n".
					   "Remote Address: ".$this->info['ip']  ."\n".
					   "Browser Stack Trace: \n";

				$arr = $this->info['stack_trace'];
				$index = 0;
				for($i=(sizeof($arr)-1);$i>=0;$i--){
					if(!is_null($arr[$i])){
						$msg .= "\t\t\t#".$index.": ".$arr[$i]."\n";
						$index++;
					}	
				}
				
				$mail = new PHPMailer();
				$mail->IsSMTP();
				$mail->IsHTML(false);
				$mail->From     = $params['email'];
				$mail->FromName = $params['firstname'].' '.$params['lastname'];
				$mail->Subject  = $params['subject'];
				$mail->Body     = $msg;
				$mail->AddAddress('bugs-goabroad.net@goabroad.com');
				$mail->Send();
				
				header('location: /ticket.php?action=show_form&success');
			}	
			
		}
		
		function _validate($params=array()){
			$errors = array();
			
			if(!strlen(trim($params['firstname']))){
				$errors[0] = 'Please fill in your name.';
			}
			
			if(!strlen(trim($params['email']))){
				$errors[1] = 'Please fill in your email address.';				
			}else{
				//if(!er_egi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $params['email']))
				if(!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i', $params['email']))
			 		$errors[1]="Invalid email address.";
			}
			
			if(!strlen(trim($params['subject']))){
				$errors[2] = 'Please fill in the subject field.';
			}
			
			if(!strlen(trim($params['message']))){
				$errors[3] = 'Please fill in the message field.';
			}
			
			$this->errors = $errors;
			if(count($this->errors)){
				$this->_viewAddTicketForm();
				exit;
			}
			return true;	
		}
		
		function _fetchClientInfo(){
			$_browser_history = $this->obj_session->get('browsing_history');
			$this->info = array();
			
			$this->info['stack_trace']			= $_browser_history;
			$this->info['ip']					= $_SERVER['REMOTE_ADDR'];
			$this->info['user_agent']			= $_SERVER['HTTP_USER_AGENT'];
		}
		
		function getParams(){
			$params = array();
			$params['firstname']	=	isset($_POST['txt_firstname'])	?	$_POST['txt_firstname']	: "";
			$params['lastname']		=	isset($_POST['txt_lastname'])	?	$_POST['txt_lastname']	: "";
			$params['email']		=	isset($_POST['txt_email'])		?	$_POST['txt_email']		: "";
			$params['subject']		=	isset($_POST['txt_subject'])	?	$_POST['txt_subject']	: "";
			$params['message']		=	isset($_POST['txa_message'])	?	$_POST['txa_message']	: "";
			return $params;
		}
		
		function getBrowserDetails(){
			$browser_details 				= array();
			$user_agent 					= explode(' ',$_SERVER['HTTP_USER_AGENT']);
			$browser_details['platform']	= $user_agent[4].' '.$user_agent[5].' '.$user_agent[6]; 
			$browser_details['parent'] 		= $user_agent[11]; 
			return $browser_details;
		}
		
	}	