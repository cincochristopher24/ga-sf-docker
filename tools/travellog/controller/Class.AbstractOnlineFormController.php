<?php
	require_once("travellog/model/Class.SessionManager.php");
	require_once("travellog/factory/Class.FileFactory.php");
	/**
	  * - The user must be logged to participate
	  * - The user must be a member of the of one the group participants of the online form
	  * - The valid traveler participant cannot participate in a locked survey form.
	  */
	
	abstract class AbstractOnlineFormController
	{
		protected $obj_session 		= NULL;
		protected $file_factory = NULL;
		protected $mOnlineForm = NULL;
		
		public function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerTemplate('LayoutMain', array( 'path' => 'travellog/views/'));
			$this->obj_session  = SessionManager::getInstance();
			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');
			$this->file_factory->registerClass('ToolMan', array('path'=>''));
			$this->file_factory->registerClass('Traveler');
			$this->file_factory->registerClass('GaOnlineForm', array('path'=>'travellog/model/gaOnlineForms/'));
			$this->file_factory->registerClass('GaOnlineFormParticipation', array('path'=>'travellog/model/gaOnlineForms/'));
			$this->file_factory->registerClass('GaFormHelper',array('path'=>'travellog/model/gaForms/'));
			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass('GroupFactory');
			$this->file_factory->registerClass('Template',array('path'=>''));
		}
		
		public function performAction(){
			//check if the user has logged in
			$travelerID = $this->obj_session->get('travelerID');
			if(!$travelerID){
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/login.php'));
			}
			//check if the frmID is available
			if( (!isset($_GET['frmID']) || !is_numeric($_GET['frmID'])) && (!isset($_POST['frmID']) || !is_numeric($_POST['frmID'])) ){		
				$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			}
			
			$oTraveler = $this->file_factory->getClass('Traveler',array($travelerID));
			$isAdvisor = $oTraveler->isAdvisor();
			
			if(!$isAdvisor){
				if( (!isset($_GET['gID']) || !is_numeric($_GET['gID'])) && (!isset($_POST['gID']) || !is_numeric($_POST['gID'])) ){		
					$this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));	
				}
			}
			
			$frmID = (isset($_GET['frmID']) ? $_GET['frmID'] : 0);
			$gID =  (isset($_GET['gID']) ? $_GET['gID'] : 0);
			
			//check if the online formID is valid			
			
			$this->mOnlineForm = $this->file_factory->invokeStaticClass('GaOnlineForm','getFormByOnlineFormID',array($frmID));
			if(is_null($this->mOnlineForm)) $this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			
			if(!$this->mOnlineForm->isOpen()){
				$this->showTemplateMessage(array('message'=>'Sorry but you cannot use this online form because it is currently marked as closed. '));
				exit;
			}
			
			//check if the gID is a valid group
			$gFactory =  $this->file_factory->invokeStaticClass('GroupFactory','instance');
			$oGroup  =  $gFactory->create($gID);
			if(is_null($oGroup)) $this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			
			//check if the given group is a participant of the online form
			$oParticipant = null;
			$participants = $this->mOnlineForm->getParticipants();
			foreach($participants as $iParticipant){
				if($iParticipant->getGroupID() == $oGroup->getGroupID()){
					$oParticipant = $iParticipant;
					break;
				}
			}
			
			if(is_null($oParticipant)) $this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			//check if the logged traveler is a member of the given group
			if(!$oGroup->isMember($oTraveler)) $this->file_factory->invokeStaticClass('ToolMan','redirect',array('/index.php'));
			
			$gaFormHelper = $this->file_factory->invokeStaticClass('GaFormHelper','create',array('formPrefix'=>'gffield'));
			
			$formParticipation = null;
			$errors = array();
			
			$formParticipation = $this->file_factory->invokeStaticClass('GaOnlineFormParticipation','getParticipationByParticipantIDAndTravelerID',array($oParticipant->getGaOnlineFormParticipantID(),$oTraveler->getTravelerID()));			
			//var_dump($formParticipation);
			$prevPostValues = array();
			if(count($_POST)) $prevPostValues = $gaFormHelper->sanitizeFormFields($_POST);
			elseif(!is_null($formParticipation)) $prevPostValues = $formParticipation->getFieldValues();
			//submit
			if(count($_POST) > 0){
				if(is_null($formParticipation)){
					$formParticipation = new GaOnlineFormParticipation();
					$formParticipation->setGaOnlineFormParticipantID($oParticipant->getGaOnlineFormParticipantID());
					$formParticipation->setTravelerID($oTraveler->getTravelerID());
				}
				$errors = $formParticipation->saveFormValue($prevPostValues,$isAdvisor);
				if(true === $errors){
					//display the success message
					$msg = $isAdvisor  
						? 'Your test participation was successfully completed. Your answers were not saved since you are not a qualified participant.'
						: 'Thank you form filling out this online form';
					$this->showTemplateMessage(array('message'=>$msg));
					exit;
				}
			}
			$this->showFormTemplate(
				array(
					'participation'	=> $formParticipation,
					'errFields'		=> $errors,
					'gID'			=> $gID,
					'prevPostValues'=> $prevPostValues,
					'formData'		=> $this->mOnlineForm->getFormData(),
					'gaFormHelper'	=> $gaFormHelper,
					'isAdvisor'		=> $isAdvisor
				)
			);
		}
		
		private function createTemplate($vars=array()){
			$this->file_factory->invokeStaticClass ('Template','setMainTemplate', array($this->file_factory->getTemplate('LayoutMain')));
			$tpl = $this->file_factory->getClass('Template');
			$tpl->set_path('travellog/views/onlineForms/');
			$subNavigation = $this->file_factory->getClass('SubNavigation');
			$subNavigation->setContext('GROUP');
			$subNavigation->setContextID($this->mOnlineForm->getGroupID());
			$subNavigation->setGroupType('ADMIN_GROUP');
			$subNavigation->setLinkToHighlight('ONLINE_FORMS');	
			$tpl->set("subNavigation",$subNavigation);
			return $tpl;
		}
		
		public function showTemplateMessage($vars=array()){
			$tpl = $this->createTemplate();
			$tpl->set('action','SHOW_MESSAGE');
			$tpl->set('message',$vars['message']);
			$tpl->out('tpl.OnlineForm.php');
		}
		
		public function showFormTemplate($vars=array()){
			//all is well, show the survey form
			$tpl = $this->createTemplate();
			$vars['onlineForm'] = $this->mOnlineForm;
			$vars['action'] = 'SHOW_FORM';
			$tpl->setVars($vars);
			$tpl->out('tpl.OnlineForm.php');
		}
		
	}
?>