<?php
	/*********************************************************/
	/**  	@author Cheryl Ivy Q. Go						**/
	/**  	Created on 19 March 2008						**/
	/**  	NewMemberController implements iController		**/
	/*********************************************************/

	require_once 'travellog/factory/Class.FileFactory.php';
	require_once 'travellog/controller/Class.IController.php';
	
	abstract class AbstractNewMemberController implements iController{
		function __construct(){
			$this->file_factory = FileFactory::getInstance();
			$this->file_factory->registerClass('Template' , array ( 'path' => '' ));

			$this->file_factory->registerClass('SubNavigation');
			$this->file_factory->registerClass("Crypt", array("path" => ""));
	 		$this->file_factory->registerClass('Session', array('path' => 'travellog/model'));
			$this->file_factory->registerClass('Traveler', array('path'=> 'travellog/model/'));
			$this->file_factory->registerClass('GroupFactory', array('path'=> 'travellog/model/'));
			$this->file_factory->registerClass('SearchStaffPanel', array('path'=> 'travellog/views/'));
			$this->file_factory->registerClass('ViewGroupMembers', array('path'=> 'travellog/views/'));
			$this->file_factory->registerClass('SessionManager', array ("path" => "travellog/model/"));
			$this->file_factory->registerClass('AssignStaffToSubGroup', array('path'=> 'travellog/model/'));
			$this->file_factory->registerClass("AdminGroup", array("path" => "travellog/model/"));

			$this->file_factory->setPath('CSS', '/css/');
			$this->file_factory->setPath('HEADER_FOOTER', 'travellog/views/');

			//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');			
		}
		
		function performAction(){
			$this->tpl = $this->file_factory->getClass('Template');
			$this->tpl->set_path("travellog/views/");

			// decrypt
			$crypt = $this->file_factory->getClass("Crypt");
			$travelerId = $crypt->decrypt($_GET['tID']);
			
			// instantiate traveler
			$traveler = null;
			try {
				$traveler = $this->file_factory->getClass('Traveler', array ($travelerId));
			}
			catch (Exception $e) {} 
			
			if (0 < $travelerId){
				$changedInfo = $this->file_factory->invokeStaticClass("AssignStaffToSubGroup", "isUserInfoChanged", array($travelerId));

				if ($traveler->isActive() && $changedInfo ){
					header("location:passport.php");
					exit;
				}
				else{
					$groupId = $this->file_factory->invokeStaticClass("AdminGroup", "getAdvisorGroupID", array($travelerId));
					// create session for traveler
					$sessionManager = $this->file_factory->invokeStaticClass("SessionManager", "getInstance");
					$sessionManager->set("isLogin",		1);
					$sessionManager->set("travelerID",	$travelerId);
					$sessionManager->set("login",		crypt($travelerId, CRYPT_BLOWFISH));
					$sessionManager->set("gID",			$groupId);
					$sessionManager->set("domain",		$_SERVER["HTTP_HOST"]);

					$session = $this->file_factory->invokeStaticClass("Session", "getInstance", array($travelerId));
					$session->startSession();

					if (0 < $sessionManager->get('travelerID')){
						// include log system
						require_once 'gaLogs/Class.GaLogger.php';
						GaLogger::create()->start($travelerId, 'TRAVELER');
					}

					// get parent group of subgroups assigned
					$parentId = $this->file_factory->invokeStaticClass("AssignStaffToSubGroup", "getParentGroup", array($travelerId));

					// get staffed groups of traveler
					$staffedGroups = Traveler::getStaffedGroups($travelerId, array($parentId));
					
					$viewchange = $this->file_factory->getClass('SearchStaffPanel', array());
					$viewchange->setLoggedUserId($travelerId);
					$viewchange->retrieveChange();

					$this->tpl->set("viewchange",		$viewchange);
					$this->tpl->set("travelerId",		$travelerId);
					$this->tpl->set("totalInvites",		count($staffedGroups));
					$this->tpl->set("staffedGroups",	$staffedGroups);

					$this->tpl->out("tpl.ViewNewMember.php");
				}
			}
			else{
				header("location:index.php");
				exit;
			}
		}
	}
?>