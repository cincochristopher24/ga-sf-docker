<?php
/**
* <b>TravelTipsVO</b> value object.
* @author Aldwin S. Sabornido
* @version 1.0 PHP5
* @see http://www.GoAbroad.com/
* @copyright 2007-2008
*/
require_once("travellog/vo/Class.SetGet.php");
class TravelTipsVO extends SetGet{
	protected
	
	$mTravelTipID = 0,
	
	$mTravelLogID = 0,
	
	$mDescription = NULL;
}
?>
