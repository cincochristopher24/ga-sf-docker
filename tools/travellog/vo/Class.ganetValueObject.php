<?php

 
 class ganetValueObject{
    protected $values = array();
    
    public function set($name,$value){
           $this->values[$name] = $value;
    }

    public function get($name) {
           if (array_key_exists($name,$this->values)) {
               return $this->values[$name];
           } else {
               throw new Exception('Name not found in value object');
           }
    }
    
    public function isKeyExists($name) {
		return isset($this->values[$name]) || array_key_exists($name, $this->values); 
    }
}
?>
