<?php
class SetGet{
	function __call($method, $arguments) {
        $prefix   = substr($method, 0, 3);
        $property = 'm'.substr($method, 3);
		
		if (empty($prefix) || empty($property)) {
            return;
        }
		if (property_exists($this,$property)){
	        if ($prefix == "get" && isset($this->$property)) {
	            return $this->$property;
	        }
	        if ($prefix == "set") {
	            $this->$property = $arguments[0];
	        }
		}else throw new exception ('Undefined property name '.$property.'!');
	}
}
?>
