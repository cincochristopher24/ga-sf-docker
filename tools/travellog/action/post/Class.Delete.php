<?php
	/**
	 * @(#) Class.Delete.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - June 22, 2009
	 */
	 
	require_once('travellog/action/Class.ControllerAction.php');
	require_once('travellog/model/Class.PostPeer.php');
	
	/**
	 * Action class for deleting a post.
	 */
	class Delete extends ControllerAction {
	
		function execute(){
			require_once('gaLogs/Class.GaLogger.php'); 
			GaLogger::create()->start($this->mData['logger_id'],'TRAVELER');   
			
			if ($this->validate()) {
				if ($this->mData['post']->delete()) {
					if (0 < $this->mData['discussion']->getPostsCount()) {
						$date = $this->mData['discussion']->getLatestPost()->getDateCreated();
						$this->mData['discussion']->setDateUpdated($date);
						$this->mData['discussion']->save();
						if ($this->mData['topic']->getLatestDiscussion()->getId() == $this->mData['discussion']->getId()) {
							$this->mData['topic']->setDateUpdated($date);
							$this->mData['topic']->save();
						}
						
						$this->redirect('/discussion/posts/'.$this->mData['discussion']->getId());
					}
					else {
						$this->mData['discussion']->delete();
						$discussions_count = $this->mData['topic']->getDiscussionsCount();
						if (0 == $discussions_count) {
							$this->mData['topic']->setDateUpdated($this->mData['topic']->setDateCreated());
						}
						else {
							$this->mData['topic']->setDateUpdated($this->mData['topic']->getLatestDiscussion()->getDateUpdated());
						}
						$this->mData['topic']->save();
						if ($this->mData['is_subgroup']) {
							$this->redirect('/discussion_board/home/'.$this->mData['group']->getGroupID());
						}
						else {
							$this->redirect('/topic/discussions/'.$this->mData['topic']->getId());
						}
					}
				}
			}
			
			$this->showFileNotFound();
		}
		
		/**
		 * General rules:
		 * 	1. The current user must be logged.
		 * 	2. The url must be /post/delete/<post_id>.
		 * 	3. <post_id> must be a valid ID of a post.
		 * 	4. The current user must be a staff or the administrator of the group in which the 
		 * 		post is related or the current user is the poster of the post.
		 * 
		 */
		function validate(){
			if (isset($this->mData['post_id']) && 0 < $this->mData['post_id'] && 0 < $this->mData['logger_id']) {
				$this->mData['post'] = PostPeer::retrieveByPk($this->mData['post_id']);
				
				if (!is_null($this->mData['post'])) {
					$this->mData['discussion'] = $this->mData['post']->getDiscussion();
					$this->mData['topic'] = $this->mData['discussion']->getTopic();
					$this->mData['group'] = $this->mData['topic']->getGroup();
					$this->mData['is_subgroup'] = (0 < $this->mData['group']->getParentID());
					$this->mData['is_admin_logged'] = ($this->mData['group']->getAdministratorID() == $this->mData['logger_id'] || $this->mData['group']->isStaff($this->mData['logger_id']));							
					
					if ($this->mData['logger_id'] == $this->mData['post']->getPosterId() OR $this->mData['is_admin_logged']) {
						return true;
					}
				}
			}
			
			return false;				
		}
	
	}