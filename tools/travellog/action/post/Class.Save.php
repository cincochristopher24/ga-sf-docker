<?php
	/**
	 * @(#) Class.Save.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - June 19, 2009
	 */
	
	require_once('travellog/action/Class.ControllerAction.php');
	require_once('travellog/model/Class.DiscussionPeer.php');
	require_once('travellog/model/Class.TravelerPeer.php');
	require_once('Class.Template.php');		
	
	/**
	 * Action class for saving a post.
	 */
	class Save extends ControllerAction {
	
		function execute(){
			require_once('gaLogs/Class.GaLogger.php'); 
			GaLogger::create()->start($this->mData['logger_id'],'TRAVELER');   
			
			if ($this->validate()) {
				if (isset($this->mData['errors'])) {
					$file_factory = FileFactory::getInstance();
					
					$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('title', $this->mData['group']->getName() . ' Discussion Board - GoAbroad.net'));
					$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('layoutID','discussion_board-1.2'));
					
					$this->mData['sub_navigation'] = $file_factory->getClass('SubNavigation');
					$this->mData['sub_navigation']->setContext('GROUP');
					$this->mData['sub_navigation']->setContextID($this->mData['group']->getGroupID());
					$this->mData['sub_navigation']->setLinkToHighlight('DISCUSSION_BOARD');
				
					$profile_comp = $file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
					$profile_comp->init($this->mData['group']->getGroupID());
					$this->mData['profile'] = $profile_comp->get_view();
					
					$file_factory->invokeStaticClass('Template','setMainTemplate', array($file_factory->getTemplate('LayoutMain')));
					
					$tpl = new Template();
					$tpl->setVars($this->mData);
					$tpl->out('travellog/views/discussionboard/tpl.FrmPost.php');
					exit;
				}
				else {
					$this->mData['post']->setMessage($this->mData['message']);
					// Create new post
					if (0 == $this->mData['post_id']) {
						$date = date('Y-m-d H:i:s');
						$this->mData['post']->setDiscussionId($this->mData['discussion_id']);
						$this->mData['post']->setPosterId($this->mData['logger_id']);
						$this->mData['post']->setDateCreated($date);	
						
						if ($this->mData['post']->save()) {
							$this->mData['topic']->setDateUpdated($date);
							$this->mData['topic']->save();
							$this->mData['discussion']->setDateUpdated($date);
							$this->mData['discussion']->save();
							
							if(in_array($this->mData['group']->getGroupID(), DiscussionPeer::$_DIGEST_ENABLED_GROUPS) && in_array($this->mData['group']->getParentID(), DiscussionPeer::$_DIGEST_ENABLED_GROUPS)){
								require_once('travellog/model/notifications/Class.RealTimeNotifier.php');
								RealTimeNotifier::getInstantNotification(RealTimeNotifier::ADD_DISCUSSION_POST, 
									array(
										'POST'			=> $this->mData['post'],
										'DISCUSSION'	=> $this->mData['discussion'] 
								))->send();
							}
							
							$this->redirect("/discussion/posts/".$this->mData['discussion']->getId()."#".$this->mData['post']->getId());
						}					
					}
					else {
						$this->mData['post']->save();
						$this->redirect("/discussion/posts/".$this->mData['discussion']->getId()."#".$this->mData['post']->getId());
					}
					
					$this->redirect("/group.php?gID=".$this->mData['group']->getGroupID());
				}
			}
			else {
				$this->showFileNotFound();
			}
		}
		
		/**
		 * General rules:
		 * 	1. The current user must be logged.
		 * 	2. $_POST['txaMessage'] exists.
		 * 
		 * Validation rules for saving an edited post:
		 * 	1. $_POST['hidPostId'] exists.
		 * 	2. $_POST['hidPostId'] must be a valid ID of a post.
		 * 	3. The current user must be a staff or the administrator of the group in which the 
		 * 		post is related or the current user is the poster of the post.
		 * 
		 * Validation rules for saving a reply to a discussion.
		 * 	1. $_POST['hidDiscussionId'] exists.
		 * 	2. $_POST['hidDiscussionId'] must be a valid ID of a discussion.
		 * 	3. The current user is :
		 * 		3.1 A staff or administrator of the group in which the discussion is related.
		 * 		3.2 A member of the group(Applicable for subgroup and root group).
		 * 		3.3 A member of the root group if the discussions topic is set to open for all
		 * 			members of the root group which also includes the members of the subgroup.
		 */
		function validate(){
			if (isset($_POST['txaMessage']) AND 0 < $this->mData['logger_id']) {
				// Validate message
				$this->mData['message'] = trim($_POST['txaMessage']);
				if (empty($this->mData['message'])) {
					$this->mData['errors']['message'] = "Please enter a valid message.";
				}
				
				// Save after editing
				if (isset($_POST['hidPostId']) AND 0 < $_POST['hidPostId']) {
					require_once('travellog/model/Class.PostPeer.php');
					
					$this->mData['post'] = PostPeer::retrieveByPk($_POST['hidPostId']);
					if (!is_null($this->mData['post'])) {
						$this->mData['post_id'] = $this->mData['post']->getId();
						$this->mData['discussion'] = $this->mData['post']->getDiscussion();
						$this->mData['discussion_id'] = $this->mData['discussion']->getId();
						$this->mData['topic'] = $this->mData['discussion']->getTopic();
						$this->mData['group'] = $this->mData['topic']->getGroup();
						$this->mData['is_subgroup'] = (0 < $this->mData['group']->getParentID());
						$this->mData['is_admin_logged'] = ($this->mData['group']->getAdministratorID() == $this->mData['logger_id'] || $this->mData['group']->isStaff($this->mData['logger_id']));							
						
						if ($this->mData['logger_id'] == $this->mData['post']->getPosterId() OR $this->mData['is_admin_logged']) {
							return true;
						}
					}
				}
				// Save after creating a reply to discussion
				else if (isset($_POST['hidDiscussionId']) AND 0 < $_POST['hidDiscussionId']) {
					require_once('travellog/model/Class.DiscussionPeer.php');
					require_once('travellog/model/Class.Post.php');
					$this->mData['post'] = new Post();
					$this->mData['post_id'] = 0;
					$this->mData['discussion'] = DiscussionPeer::retrieveByPk($_POST['hidDiscussionId']);
					
					if (!is_null($this->mData['discussion'])) {
						$this->mData['discussion_id'] = $this->mData['discussion']->getId();
						$this->mData['traveler'] = TravelerPeer::retrieveByPk($this->mData['logger_id']);
						$this->mData['topic'] = $this->mData['discussion']->getTopic();
						$this->mData['group'] = $this->mData['topic']->getGroup();
						$this->mData['is_subgroup'] = (0 < $this->mData['group']->getParentID());
						$this->mData['parent_group'] = ($this->mData['is_subgroup']) ? $this->mData['group']->getParent() : $this->mData['group'];
						$this->mData['is_admin_logged'] = ($this->mData['group']->getAdministratorID() == $this->mData['logger_id'] || $this->mData['group']->isStaff($this->mData['logger_id']));
						$this->mData['is_member_logged'] = ($this->mData['is_admin_logged'] || $this->mData['group']->isMember($this->mData['traveler']));
						$this->mData['is_root_member_logged'] = ($this->mData['is_subgroup']) ? ($this->mData['is_member_logged'] || $this->mData['parent_group']->isMember($this->mData['traveler'])) : $this->mData['is_member_logged'];

						switch ($this->mData['topic']->getPrivacySetting()) {
							case TopicPeer::PUBLIC_SETTING:
							case TopicPeer::MEMBERS_ONLY_SETTING:
								if ($this->mData['is_root_member_logged']) {
									return true;
								}
							break;						
							case TopicPeer::SUBGROUP_PUBLIC_SETTING:
							case TopicPeer::SUBGROUP_MEMBERS_ONLY_SETTING:
								if ($this->mData['is_member_logged']) {
									return true;
								}						
							break;
						}
					}
				} // end of saving after creating a reply
			}
			
			return false;				
		}		
	
	}
	
