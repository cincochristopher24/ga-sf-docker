<?php	

require_once("travellog/action/Class.ControllerAction.php");
require_once("travellog/model/Class.SiteContext.php");
require_once("Class.Template.php");
require_once("travellog/model/Class.SubNavigation.php");
require_once("travellog/model/traveler/Class.TravelerUser.php");
require_once("travellog/model/Class.TravelerPeer.php");
require_once("travellog/model/Class.GroupPeer.php");
require_once("travellog/components/profile/controller/Class.ProfileCompFactory.php");
require_once("Class.Paging.php");
require_once("Class.StringFormattingHelper.php");
require_once("travellog/model/Class.SqlCriteria2.php");
require_once("travellog/components/traveler/Class.AdminTravelerGroupSearchFormComponent.php");
require_once("travellog/components/traveler/Class.TravelerGroupsAndCategoriesComponent.php");
require_once("travellog/components/traveler/Class.TravelerGroupsComponent.php");

/**
 * Action class for showing the groups of the traveler.
 */
class TravelerGroupsAction extends ControllerAction
{
	/***  Constants  ***/
	const CATEGORY_SEARCH = 1;
	const GROUP_SEARCH = 2;
	
  /**
   * @var  Traveler  The owner of the groups viewed.
   */
  private $mTraveler = null;
  
  /**
   * @var  Group  The cobrand group if we are in cobrand context.
   */
  private $mGroup = null;
  
  /**
   * @var  TravelerUser  The current user of ganet
   */
  private $mUser = null;
  
  public function __construct(array $data = null)
  {
    parent::__construct($data);
    
    $this->mData['keyword'] = isset($_GET['keyword']) ? $_GET['keyword'] : '';
    $this->mData['keyword'] = isset($_POST['keyword']) ? $_POST['keyword'] : $this->mData['keyword'];
    $this->mData['search_by'] = isset($_GET['search_by']) ? $_GET['search_by'] : self::CATEGORY_SEARCH;
    $this->mData['search_by'] = isset($_POST['search_by']) ? $_POST['search_by'] : $this->mData['search_by'];
    $this->mData['page'] = isset($_GET['page']) ? $_GET['page'] : 1;
    $this->mData['is_searching'] = (isset($_GET['keyword']) OR isset($_POST['keyword']));
  }
  
  public function validate()
  {
    $file_factory = FileFactory::getInstance();
  	$this->mTraveler = $file_factory->getClass('Traveler', array($this->mData['travelerID']));
	if($this->mTraveler->isSuspended() || $this->mTraveler->isDeactivated()){
		header("location: /group.php");
		exit;
	}

  	$this->mUser = TravelerUser::getInstance();
  	
  	if (!is_null($this->mTraveler) && $this->mTraveler->getTravelerID() > 0) 
  	{
      $site_context = SiteContext::getInstance();
      if ($site_context->isInCobrand())
      {
    	  $this->mGroup = GroupPeer::retrieveByPK($site_context->getGroupID()); 
        if (!is_null($this->mGroup))
        {
      	  return true;
        } 
      }
      else
      {
        return true;
      }
  	}
    
    return false;
  }
  
  public function execute()
  {
    $site_context = SiteContext::getInstance();
    if ($site_context->isInCobrand())
    {
    	$this->mGroup = GroupPeer::retrieveByPK($site_context->getGroupID());
    	if ($this->mUser->getUserID() == $this->mTraveler->getTravelerID() AND GroupPeer::isTravelerAdminOfGroup($this->mTraveler, $this->mGroup))
    	{
        $this->showGroupsOfAdminOnCobrand();	
      }
      else
      {
        $this->showGroupsOfTraveler();
      } 
    }
    else
    {
      $this->showGroupsOfTraveler();
    } 
  }
  
  public function showGroupsOfAdminOnCobrand()
  {
    $site_context = SiteContext::getInstance();
  	    
    $page = isset($this->mData['page']) ? $this->mData['page'] : 1;
    $is_owner = ($this->mUser->getUserID() == $this->mTraveler->getTravelerID());
    $modified_name = ("s" == substr($this->mTraveler->getUserName(), -1)) ? $this->mTraveler->getUserName().'\'' : $this->mTraveler->getUserName().'\'s';
    
    $file_factory = FileFactory::getInstance();
    Template::setMainTemplate($file_factory->getTemplate('LayoutMain')); 
		
    $subNavigation = new SubNavigation();
    $subNavigation->setContext('TRAVELER');
    $subNavigation->setContextID($this->mTraveler->getTravelerID());
    $subNavigation->setLinkToHighlight('MY_GROUPS');
    
    $profile_comp = ProfileCompFactory::getInstance()->create(ProfileCompFactory::$TRAVELER_CONTEXT);
    $profile_comp->init($this->mTraveler->getTravelerID());		
			
    $obj_session = SessionManager::getInstance();	
    $success_message = ($obj_session->get('custompopup_message')) ? $obj_session->get('custompopup_message') : NULL;
    $obj_session->unsetVar('custompopup_message');
    
    $search_form_component = new AdminTravelerGroupSearchFormComponent($this->mTraveler);
    $search_form_component->set('keyword', $this->mData['keyword']);
    $search_form_component->set('selected_search_by', $this->mData['search_by']);
    
    $data_component = (self::CATEGORY_SEARCH == $this->mData['search_by']) ? new TravelerGroupsAndCategoriesComponent($this->mTraveler, $this->mGroup) : new TravelerGroupsComponent($this->mTraveler);
    $data_component->set('keyword', $this->mData['keyword']);
    $data_component->set('is_searching', $this->mData['is_searching']);
    $data_component->set('page', $this->mData['page']);

    $tpl = new Template();
    $tpl->set('profile_component', $profile_comp->get_view());
    $tpl->set('sub_navigation_component', $subNavigation);
    $tpl->set('search_form_component', $search_form_component);
    $tpl->set('data_component', $data_component);
    $tpl->set('title', (($site_context->isInCobrand()) ? $site_context->getSiteName() : "GoAbroad Network").' - '.$modified_name.' Groups');
    $tpl->set('main_title', ($is_owner) ?  'My Groups' : $modified_name.' Groups');
    $tpl->set('success', $success_message);
    $tpl->set('page_location', 'My Passport');
    $tpl->set('travelerID', $this->mTraveler->getTravelerID());
    $tpl->out('travellog/views/traveler/tpl.ViewTravelerGroupsAndCategories.php');    
  }
  
  public function showGroupsOfTraveler()
  {
    $modified_name = ("s" == substr($this->mTraveler->getUserName(), -1)) ? $this->mTraveler->getUserName().'\'' : $this->mTraveler->getUserName().'\'s';
    $page = isset($this->mData['page']) ? $this->mData['page'] : 1;
    $is_owner = ($this->mTraveler->getTravelerID() == $this->mUser->getUserID());
    
    $site_context = SiteContext::getInstance();
    
    $file_factory = FileFactory::getInstance();
    Template::setMainTemplate($file_factory->getTemplate('LayoutMain')); 
		
    $subNavigation = new SubNavigation();
    $subNavigation->setContext('TRAVELER');
    $subNavigation->setContextID($this->mTraveler->getTravelerID());
    $subNavigation->setLinkToHighlight('MY_GROUPS');
    
    $profile_comp = ProfileCompFactory::getInstance()->create(ProfileCompFactory::$TRAVELER_CONTEXT);
    $profile_comp->init($this->mTraveler->getTravelerID());		
			
    $obj_session = SessionManager::getInstance();	
    $success_message = ($obj_session->get('custompopup_message')) ? $obj_session->get('custompopup_message') : NULL;
    $obj_session->unsetVar('custompopup_message');
					
    $groups = $this->mTraveler->getGroups(null, new RowsLimit(6, (($page-1)*6)), $this->mData['keyword']);
    $total_rec = $this->mTraveler->getGroupsCount($this->mData['keyword']);
    $query_string = "travelerID=".$this->mTraveler->getTravelerID();
    $query_string = ($this->mData['is_searching']) ? $query_string."&amp;keyword=".$this->mData['keyword'] : $query_string;

	if(!$obj_session->getLogin()){
		Template::includeDependentCss("/css/vsgStandardized.css");
		Template::includeDependentCss("/css/modalBox.css");
		Template::includeDependentCss("/css/Thickbox.css");
		Template::includeDependentJs("/js/thickbox.js");
	}
			
    $paging = new Paging($total_rec, $page, $query_string, 6);

    $tpl = new Template();
    $tpl->set('profile_component', $profile_comp->get_view());
    $tpl->set('sub_navigation_component', $subNavigation);
    $tpl->set('main_title', ($is_owner) ?  'My Groups' : $modified_name.' Groups');
    $tpl->set('is_owner', $is_owner);
    $tpl->set('is_searching', $this->mData['is_searching']);
    $tpl->set('is_cobrand', $site_context->isInCobrand());
    $tpl->set('title', (($site_context->isInCobrand()) ? $site_context->getSiteName() : "GoAbroad Network").' - '.$modified_name.' Groups');
    $tpl->set('keyword', $this->mData['keyword']);
    $tpl->set('current_page', $page);
    $tpl->set('paging', $paging);
    $tpl->set('group_list', $groups);
    $tpl->set('groups_count', $total_rec);
    $tpl->set('loggedUser', $this->mTraveler);
    $tpl->set('success', $success_message);
    $tpl->set('page_location', 'My Passport');
    $tpl->set('travelerID', $this->mTraveler->getTravelerID());
    $tpl->out('travellog/views/traveler/tpl.ViewTravelerGroups.php');
  }
  
  public function onValidationFailure()
  {
    $this->redirect("/login.php?redirect=".$_SERVER['REQUEST_URI']);
  }
}
	
