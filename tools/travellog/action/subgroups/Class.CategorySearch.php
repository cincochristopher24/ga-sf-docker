<?php
	/**
	 * @(#) Class.CategorySearch.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - June 25, 2009
	 */
	
	require_once('travellog/action/Class.ControllerAction.php');
	require_once('travellog/model/Class.GroupPeer.php');
	require_once('Class.StringFormattingHelper.php');
	
	/**
	 * Action class for searching for a category.
	 */
	class CategorySearch extends ControllerAction {
	
		function execute(){
			if ($this->validate()) {
	      if (0 < $this->mData['parent_group']->getSubGroupCategoriesCount()) {
		      $session = SessionManager::getInstance();
					$this->mData['success'] = ($session->get('custompopup_message')) ? $session->get('custompopup_message') : NULL;	      
		      $session->unsetVar('custompopup_message');
					
					$this->mData['categories'] = $this->mData['parent_group']->getSubGroupCategories("%".$this->mData['keyword']."%", true);
					$this->mData['is_admin_logged'] = ($this->mData['parent_group']->getAdministratorID() == $this->mData['logger_id'] || $this->mData['parent_group']->isStaff($this->mData['logger_id']));
					
					$file_factory = FileFactory::getInstance();
					$file_factory->invokeStaticClass('Template','setMainTemplate', array($file_factory->getTemplate('LayoutMain')));				
					
					$tpl = new Template();
					$tpl->setVars($this->mData);
					$tpl->out('travellog/views/subgroups/tpl.ViewCategorySearch.php');
	      }
	      else {
	      	$this->redirect('/subgroups/group_search/');
	      }
			}
			else {
				$this->redirect('/index.php');
			}
		}
		
		function validate(){
			if (isset($this->mData['parent_group_id'])) {
				$this->mData['parent_group'] = GroupPeer::retrieveByPk($this->mData['parent_group_id']);
				if (!is_null($this->mData['parent_group'])) {
					$privacy_preference = new GroupPrivacyPreference($this->mData['parent_group_id']);
					if ($privacy_preference->getViewGroups() || 0 < $this->mData['logger_id']) {
						return true;
					}
				}
			}
			
			return false;
		}		
	
	}
	
