<?php
	/**
	 * @(#) Class.GroupSearch.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - June 25, 2009
	 */
	
	require_once('travellog/action/Class.ControllerAction.php');
	require_once('travellog/model/Class.GroupPeer.php');
	
	/**
	 * Action class for searching a group.
	 */
	class GroupSearch extends ControllerAction {
	
		function execute(){
			if ($this->validate()) {
				$file_factory = FileFactory::getInstance();
				$file_factory->invokeStaticClass('Template','setMainTemplate', array($file_factory->getTemplate('LayoutMain')));	      
	      
	      $numPages = 10;
	      
	      $session = SessionManager::getInstance();
				$this->mData['success'] = ($session->get('custompopup_message')) ? $session->get('custompopup_message') : NULL;	      
	      $session->unsetVar('custompopup_message');
	      $this->mData['groups'] = $this->mData['parent_group']->getMySubGroups("%".$this->mData['keyword']."%", null, " subgrouprank", new RowsLimit($numPages, (($this->mData['page']-1)*$numPages)));
			  $this->mData['groups_count'] = $this->mData['parent_group']->getMySubGroupsCount("%".$this->mData['keyword']."%");
				$this->mData['paging'] = new Paging($this->mData['groups_count'], $this->mData['page'], "/subgroups/group_search/".$this->mData['keyword'], $numPages, true, true);
				$this->mData['categories_count'] = $this->mData['parent_group']->getSubGroupCategoriesCount();
				$this->mData['is_admin_logged'] = ($this->mData['parent_group']->getAdministratorID() == $this->mData['logger_id'] || $this->mData['parent_group']->isStaff($this->mData['logger_id']));
				
				$tpl = new Template();
				$tpl->setVars($this->mData);
				$tpl->out('travellog/views/subgroups/tpl.ViewGroupSearch.php');
			}
			else {
				$this->redirect('/index.php');
			}
		}
		
		function validate(){
			if (isset($this->mData['parent_group_id'])) {
				$this->mData['parent_group'] = GroupPeer::retrieveByPk($this->mData['parent_group_id']);
				if (!is_null($this->mData['parent_group'])) {
					$privacy_preference = new GroupPrivacyPreference($this->mData['parent_group_id']);
					if ($privacy_preference->getViewGroups() || 0 < $this->mData['logger_id']) {
						return true;
					}
				}
			}
			
			return false;
		}	
	
	}
	
