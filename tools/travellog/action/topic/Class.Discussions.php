<?php
	/**
	 * @(#) Class.Discussions.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - June 20, 2009
	 */
	
	require_once('travellog/action/Class.ControllerAction.php');
	require_once('travellog/model/Class.TopicPeer.php');
	require_once('travellog/model/Class.TravelerPeer.php');
	require_once('travellog/model/Class.DiscussionPeer.php');
	require_once('travellog/helper/Class.DiscussionHelper.php');
	
	/**
	 * Action class for showing the discussions of the topic.
	 */
	class Discussions extends ControllerAction {
		
		function execute(){
			if ($this->validate()) {
				$file_factory = FileFactory::getInstance();
				
				$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('title', $this->mData['group']->getName() . ' Discussion Board - GoAbroad.net'));
				$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('layoutID','discussion_board-1.2'));
				
				$this->mData['sub_navigation'] = $file_factory->getClass('SubNavigation');
				$this->mData['sub_navigation']->setContext('GROUP');
				$this->mData['sub_navigation']->setContextID($this->mData['group']->getGroupID());
				$this->mData['sub_navigation']->setLinkToHighlight('DISCUSSION_BOARD');
			
				$profile_comp = $file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
				$profile_comp->init($this->mData['group']->getGroupID());
				$this->mData['profile'] = $profile_comp->get_view();
				
				$this->mData['privacy'] = $this->mData['topic']->getPrivacySetting();
				$this->mData['group_id'] = $this->mData['group']->getGroupID();
				
				$criteria = new SqlCriteria2();
				$criteria->addOrderByColumn(DiscussionPeer::DATE_UPDATED, SqlCriteria2::DESC);
				
				$this->mData['discussions'] = $this->mData['topic']->getDiscussions($criteria);
				$this->mData['discussions_count'] = count($this->mData['discussions']);
				$this->mData['last_post'] = (0 < $this->mData['discussions_count']) ? $this->mData['discussions'][$this->mData['discussions_count']-1]->getLatestPost() : null;
 				$this->mData['posts_count'] = 0;
				$this->mData['discussion_helper'] = new DiscussionHelper($this->mData['is_admin_logged']);
				$this->mData['discussion_helper']->setTemplate('tpl.IncDiscussion4.php');
				foreach ($this->mData['discussions'] as $discussion) {
					$this->mData['discussion_helper']->addDiscussion($discussion);
					$this->mData['posts_count'] = $this->mData['posts_count'] + $discussion->getPostsCount();
				}
				
				$file_factory->invokeStaticClass('Template','setMainTemplate', array($file_factory->getTemplate('LayoutMain')));
				
				$tpl = new Template();
				$tpl->setVars($this->mData);
				$tpl->out('travellog/views/discussionboard/tpl.ViewDiscussions.php');
				exit;
			}
			else {
				$this->showFileNotFound();
			}
		}
		
		/**
		 * General Rules:
		 * 	1. The url must be /discussion/show_posts/<discussion_id>
		 * 	2. <discussion_id> must be a valid ID of a discussion.
		 * 	3. The current user is :
		 * 		3.1 A staff or administrator of the group in which the discussion is related.
		 * 		3.2 A member of the group(Applicable for subgroup and root group) if the discussions 
		 * 			related topic is set to show to members only.
		 * 		3.3 A member of the root group if the discussions topic is set to show to 
		 * 			members of the root group which also includes the members of the subgroup.
		 * 		3.4 Show to public if the discussions topic is set to show to public.
		 */		
		function validate(){
			if (isset($this->mData['topic_id']) && 0 < $this->mData['topic_id']) {
				$this->mData['topic'] = TopicPeer::retrieveByPk($this->mData['topic_id']);
				
				if (!is_null($this->mData['topic'])) {
					$this->mData['traveler'] = TravelerPeer::retrieveByPk($this->mData['logger_id']);
					$this->mData['group'] = $this->mData['topic']->getGroup();
					
					if (0 == $this->mData['group']->getParentId()) {
						$this->mData['is_admin_logged'] = (0 < $this->mData['logger_id'] && ($this->mData['group']->getAdministratorID() == $this->mData['logger_id'] || $this->mData['group']->isStaff($this->mData['logger_id'])));
						$this->mData['is_member_logged'] = (0 < $this->mData['logger_id'] && ($this->mData['is_admin_logged'] || $this->mData['group']->isMember($this->mData['traveler'])));
						
						if (TopicPeer::PUBLIC_SETTING == $this->mData['topic']->getPrivacySetting() || $this->mData['is_member_logged']) {
							return true;
						}
					}
				}
			}
			
			return false;				
		}					
		
	}