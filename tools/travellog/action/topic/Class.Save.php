<?php
	/**
	 * @(#) Class.Save.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - June 23, 2009
	 */
	
	require_once('travellog/action/Class.ControllerAction.php');
	require_once('travellog/model/Class.TopicPeer.php');
	
	/**
	 * Action class for saving a newly created topic or saving an edited topic.
	 */
	class Save extends ControllerAction {
		
		function execute(){
			if ($this->validate()) {
				if (0 < count($this->mData['errors'])) {
					$file_factory = FileFactory::getInstance();
					
					$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('title', $this->mData['group']->getName() . ' Discussion Board - GoAbroad.net'));
					$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('layoutID','discussion_board-1.2'));
					
					$this->mData['sub_navigation'] = $file_factory->getClass('SubNavigation');
					$this->mData['sub_navigation']->setContext('GROUP');
					$this->mData['sub_navigation']->setContextID($this->mData['group']->getGroupID());
					$this->mData['sub_navigation']->setLinkToHighlight('DISCUSSION_BOARD');
				
					$profile_comp = $file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
					$profile_comp->init($this->mData['group']->getGroupID());
					$this->mData['profile'] = $profile_comp->get_view();
					
					$file_factory->invokeStaticClass('Template','setMainTemplate', array($file_factory->getTemplate('LayoutMain')));
					
					$tpl = new Template();
					$tpl->setVars($this->mData);
					$tpl->out('travellog/views/discussionboard/tpl.FrmTopic.php');
					exit;
				}
				else {
					$date = date('Y-m-d H:i:s');
					
					if (!$this->mData['topic']->exists()) {
						$this->mData['topic']->setGroupId($this->mData['group']->getGroupID()); 
						$this->mData['topic']->setCreatorId($this->mData['logger_id']);
						$this->mData['topic']->setDateCreated($date);
						$this->mData['topic']->setDateUpdated($date);
					}
					$this->mData['topic']->setTitle($this->mData['title']);
					$this->mData['topic']->setDescription($this->mData['description']);
					$this->mData['topic']->setPrivacySetting($this->mData['privacy_setting']);
					if ($this->mData['topic']->save()) {
						$this->redirect('/topic/discussions/'.$this->mData['topic']->getId());
					}
				}
			}
			
			$this->showFileNotFound();
		}
		
		/**
		 * General Rules:
		 * 	1. $_POST['txtTitle'] exists.
		 * 	2. $_POST['txaDescription'] exists.
		 * 	3. $_POST['rdoPrivacy'] exists.
		 * 	4. Saving a new topic is for advisor group only.
		 * 
		 * Editing a topic:
		 * 	1. $_POST['hidGroupId'] exists.
		 * 	2. $_POST['hidGroupId'] is a valid ID of a group.
		 * 	3. The current logged user is a staff or administrator of the given group.
		 * 
		 * Saving a topic:
		 * 	1. $_POST['hidTopicId'] exists and is greater than 0.
		 * 	2. $_POST['hidTopicId'] is a valid ID of a topic.
		 * 	3. The current logged user is a staff or administrator of the related group of the topic. 
		 */
		function validate(){
			if (isset($_POST['txtTitle']) AND isset($_POST['txaDescription']) AND isset($_POST['rdoPrivacy']) AND 0 < $this->mData['logger_id']) {
				$this->mData['title'] = trim($_POST['txtTitle']);
				$this->mData['description'] = trim($_POST['txaDescription']);
				$this->mData['privacy_setting'] = trim($_POST['rdoPrivacy']);
				$this->mData['errors'] = array();
				
				// Validate title
				if (empty($this->mData['title'])) {
					$this->mData['errors']['title'] = "Please enter a valid title.";
				} // End of title validation
				
				// Validate description
				if (empty($this->mData['description'])) {
					$this->mData['errors']['description'] = "Please enter a valid description.";
				} // End of description validation.
				
				// Validate privacy setting
				if (empty($this->mData['privacy_setting']) OR (TopicPeer::PUBLIC_SETTING != $this->mData['privacy_setting'] AND TopicPeer::MEMBERS_ONLY_SETTING != $this->mData['privacy_setting'])) {
					$this->mData['errors']['privacy_setting'] = "";
				} // End of privacy setting validation
				
				if (isset($_POST['hidTopicId']) AND is_numeric($_POST['hidTopicId']) AND 0 < $_POST['hidTopicId']) {
					
					$this->mData['topic'] = TopicPeer::retrieveByPk($_POST['hidTopicId']);
					if (!is_null($this->mData['topic'])) {
						$this->mData['group'] = $this->mData['topic']->getGroup();
						$this->mData['topic_id'] = $this->mData['topic']->getId();
						$this->mData['group_id'] = $this->mData['group']->getGroupID(); 
						if (0 == $this->mData['group']->getParentID() AND Group::ADMIN == $this->mData['group']->getDiscriminator()) {
							if ($this->mData['logger_id'] == $this->mData['group']->getAdministratorID() || $this->mData['group']->isStaff($this->mData['logger_id'])) {
								return true;
							}
						}
					}
				}
				else if (isset($_POST['hidGroupId']) AND is_numeric($_POST['hidGroupId']) AND 0 < $_POST['hidGroupId']) {
					require_once('travellog/model/Class.GroupPeer.php');
					require_once('travellog/model/Class.Topic.php');
					
					$this->mData['group'] = GroupPeer::retrieveByPk($_POST['hidGroupId']);
					
					if (!is_null($this->mData['group'])) {
						$this->mData['topic_id'] = 0;
						$this->mData['topic'] = new Topic();
						$this->mData['group_id'] = $this->mData['group']->getGroupID(); 
						if (0 == $this->mData['group']->getParentID() AND Group::ADMIN == $this->mData['group']->getDiscriminator()) {
							if ($this->mData['logger_id'] == $this->mData['group']->getAdministratorID() || $this->mData['group']->isStaff($this->mData['logger_id'])) {
								return true;
							}						
						}
					}
				}
			}
			
			return false;
		}	
		
	}