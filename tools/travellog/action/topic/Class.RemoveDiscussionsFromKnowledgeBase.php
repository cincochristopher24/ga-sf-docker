<?php
	/**
	 * @(#) Class.RemoveDiscussionsFromKnowledgeBase.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - June 26, 2009
	 */
	
	require_once('travellog/action/Class.ControllerAction.php');
	require_once('travellog/model/Class.TopicPeer.php');
	require_once('travellog/model/Class.DiscussionPeer.php');
	
	/**
	 * Action class for removing the topic's discussions from the parent group's 
	 * knowledge base.
	 */
	class RemoveDiscussionsFromKnowledgeBase extends ControllerAction {
		
		function execute(){
			if ($this->validate()) {
				$this->mData['topic']->setPrivacySetting($this->mData['privacy_setting']);
				if ($this->mData['topic']->save()) {
					$criteria = new SqlCriteria2();
					$criteria->add(DiscussionPeer::ADDED_TO_KNOWLEDGE_BASE, DiscussionPeer::KNOWLEDGE_BASE_ADDED);					
					$discussions = $this->mData['topic']->getDiscussions($criteria);
					foreach($discussions as $discussion){
						$discussion->setAddedToKnowledgeBase(DiscussionPeer::KNOWLEDGE_BASE_NOT_ADDED);
					}
					if (DiscussionPeer::doUpdate($discussions)) {
						echo "({ isSuccessful: true, message: 'You have successfully removed the discussions of the topic from the knowledge base.' })";
						exit;
					}
				}				
			}
			
			if ($this->isAjaxRequest()) {
				echo "({ isSuccessful : false, message : 'An error occured while removing the discussions of the topic!'})";
				exit;
			}
			else {
				$this->showFileNotFound();
			}
		}
		
		/**
		 * General Rules:
		 * 	1. The given topic ID must be a valid ID of the topic.
		 * 	2. The current user must be an administrator or staff of the related group of the topic.
		 * 	3. The related group of the topic is a subgroup.
		 */
		function validate(){
			if ($this->isAjaxRequest()) {
				if (isset($this->mData['topic_id']) && 0 < $this->mData['logger_id']) {
					$this->mData['topic'] = TopicPeer::retrieveByPk($this->mData['topic_id']);
					
					if (!is_null($this->mData['topic'])) {
						$this->mData['group'] = $this->mData['topic']->getGroup();
						$this->mData['is_admin_logged'] = ($this->mData['group']->getAdministratorID() == $this->mData['logger_id'] || $this->mData['group']->isStaff($this->mData['logger_id']));
						if ($this->mData['is_admin_logged'] AND Group::ADMIN == $this->mData['group']->getDiscriminator() AND 0 < $this->mData['group']->getParentID()) {
							return true;
						}
					}
				}
			}
			
			return false;
		}		
		
	}