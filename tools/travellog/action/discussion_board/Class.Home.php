<?php
	/**
	 * @(#) Class.Home.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - June 23, 2009
	 */
	
	require_once('travellog/action/Class.ControllerAction.php');
	require_once('travellog/model/Class.GroupPeer.php');
	require_once('travellog/model/Class.TopicPeer.php');
	require_once('travellog/model/Class.DiscussionPeer.php');	
	require_once('travellog/model/Class.TravelerPeer.php');
	require_once('travellog/helper/Class.DiscussionHelper.php');
	require_once('travellog/model/Class.PosterPeer.php');
	
	/**
	 * Action class for showing the discussion board home of a group.
	 */
	class Home extends ControllerAction {
	
		function execute(){
			if ($this->validate()) {
				if ($this->mData['is_subgroup']) {
					$this->showSubGroupDiscussionBoardHome();
				}
				else {
					$this->showRootGroupDiscussionBoardHome();
				}
			}
			else {
				$this->showFileNotFound();
			}
		}
		
		/**
		 * Shows the subgroups discussion board home page.
		 * 
		 * @return void
		 */
		private function showSubGroupDiscussionBoardHome(){
			if ($this->validateSubGroup()) {
				$file_factory = FileFactory::getInstance();
				
				$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('title', $this->mData['group']->getName() . ' Discussion Board - GoAbroad.net'));
				$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('layoutID','discussion_board-1.2'));
				
				$this->mData['recent_discussions'] = array();
				$this->mData['posts_counts'] = DiscussionPeer::getPostsCount($this->mData['discussions']);					
				$this->mData['posts_count'] = array_sum($this->mData['posts_counts']);
				$this->mData['discussion_helper'] = new DiscussionHelper($this->mData['is_admin_logged']);
				$this->mData['discussion_helper']->setTemplate('tpl.IncDiscussion4.php');
				$this->mData['discussion_helper']->addDiscussions($this->mData['discussions']);
				$this->mData['last_post'] = (0 < $this->mData['discussions_count']) ? $this->mData['discussions'][0]->getLatestPost() : null;
				
				$this->mData['sub_navigation'] = $file_factory->getClass('SubNavigation');
				$this->mData['sub_navigation']->setContext('GROUP');
				$this->mData['sub_navigation']->setContextID($this->mData['group']->getGroupID());
				$this->mData['sub_navigation']->setLinkToHighlight('DISCUSSION_BOARD');
			
				$profile_comp = $file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
				$profile_comp->init($this->mData['group']->getGroupID());
				$this->mData['profile'] = $profile_comp->get_view();
				
				foreach ($this->mData['discussions'] as $discussion) {
					if (1 < $discussion->getPostsCount()) {
						$this->mData['recent_discussions'][] = $discussion;
					}
				}
				
				$file_factory->invokeStaticClass('Template','setMainTemplate', array($file_factory->getTemplate('LayoutMain')));
				
				$tpl = new Template();
				$tpl->setVars($this->mData);
				$tpl->out('travellog/views/discussionboard/tpl.ViewSubGroupDiscussionBoard.php');
			}
			else {
				$this->redirect('/index.php');
			}
		}
		
		/**
		 * Apply the subgroup rules.
		 * 
		 * Sub Group Discussion Board Rules:
		 * 	1. If the current user is an admin(meaning administrator of the group or staff) or member 
		 * 		of the group this will always show.
		 * 	2. If the current user is not a member of the group but a member of the root group, this will
		 * 		only show if the discussion board is set to show to members of the root group or show to public.
		 * 	3. If the current user is public(meaning not a member) this will only show if the discussion board
		 * 		is set so that the public can view and if there are discussions that have preferences to be viewed 
		 * 		by the public.
		 * 
		 * @return boolean true if the current data is valid.
		 */
		private function validateSubGroup(){
			$this->mData['traveler'] = TravelerPeer::retrieveByPk($this->mData['logger_id']);
			$this->mData['is_admin_logged'] = (0 < $this->mData['logger_id'] AND ($this->mData['group']->getAdministratorID() == $this->mData['logger_id'] || $this->mData['group']->isStaff($this->mData['logger_id'])));
			$this->mData['is_member_logged'] = ($this->mData['is_admin_logged'] || (0 < $this->mData['logger_id'] AND $this->mData['group']->isMember($this->mData['traveler'])));			
			$this->mData['can_reply'] = $this->mData['is_member_logged'];			
			$this->mData['parent_group'] = $this->mData['group']->getParent();
			$this->mData['is_root_member_logged'] = ($this->mData['is_member_logged'] || (0 < $this->mData['logger_id'] AND $this->mData['parent_group']->isMember($this->mData['traveler'])));				
			
			$criteria = new SqlCriteria2();
			$criteria->add(TopicPeer::GROUP_ID, $this->mData['group_id']);
			
			$criteria2 = new SqlCriteria2();
			$criteria2->addOrderByColumn(DiscussionPeer::DATE_UPDATED, SqlCriteria2::DESC);
			if (!$this->mData['is_admin_logged']) {
				$criteria2->add(DiscussionPeer::STATUS, DiscussionPeer::ACTIVE);
			}
			
			$this->mData['topic'] = TopicPeer::doSelectOne($criteria);
			$this->mData['topic'] = (is_null($this->mData['topic'])) ? TopicPeer::createTopicForGroup($this->mData['group']) : $this->mData['topic'];		
			$this->mData['discussions'] = $this->mData['topic']->getDiscussions($criteria2);					
			$this->mData['discussions_count'] = count($this->mData['discussions']);
			
			switch ($this->mData['topic']->getPrivacySetting()) {						
				case TopicPeer::SUBGROUP_PUBLIC_SETTING:								
					if ($this->mData['is_member_logged'] || 0 < $this->mData['discussions_count']) {						
						return true;
					}							
				break;
				case TopicPeer::SUBGROUP_MEMBERS_ONLY_SETTING:
					if ($this->mData['is_member_logged']) {
						return true;
					}															
				break;
				case TopicPeer::PUBLIC_SETTING:
					if ($this->mData['is_root_member_logged']) {
						$this->mData['can_reply'] = true;
						return true;
					}
					else if (0 < $this->mData['discussions_count']) {						
						return true;
					}
				break;
				case TopicPeer::MEMBERS_ONLY_SETTING:
					if ($this->mData['is_root_member_logged']) {
						$this->mData['can_reply'] = true;
						return true;
					}
				break;
			}
			
			return false;	
		}
		
		/**
		 * Shows the root group's discussion board home page.
		 * 
		 * @return void
		 */
		private function showRootGroupDiscussionBoardHome(){
			if ($this->validateRootGroup()) {
				$criteria = new SqlCriteria2();
				$criteria->add(TopicPeer::GROUP_ID, $this->mData['group_id']);
				$criteria->addOrderByColumn(TopicPeer::DATE_UPDATED, SqlCriteria2::DESC);
				if (!$this->mData['is_member_logged']) {
					$criteria->add(TopicPeer::PRIVACY_SETTING, TopicPeer::PUBLIC_SETTING);
				}							
				$this->mData['topics'] = TopicPeer::doSelect($criteria);
				
				$criteria = new SqlCriteria2();
				if (!$this->mData['is_admin_logged']) {
					$criteria->add(DiscussionPeer::STATUS, DiscussionPeer::ACTIVE);
				}
				$this->mData['topic_discussions_count'] = TopicPeer::getDiscussionsCount($this->mData['topics'], $criteria);
				
				$criteria->addOrderByColumn(DiscussionPeer::DATE_UPDATED, SqlCriteria2::DESC);
				$criteria->addLimit(3);			
				$this->mData['recent_discussions'] = array();
				foreach ($this->mData['topics'] as $topic) {
					if (isset($this->mData['topic_discussions_count'][$topic->getId()]) AND 0 < $this->mData['topic_discussions_count'][$topic->getId()]) {
						$discussions = $topic->getDiscussions($criteria);
						$this->mData['discussion_helpers'][$topic->getId()] = new DiscussionHelper($this->mData['is_admin_logged']);
						$this->mData['discussion_helpers'][$topic->getId()]->setTemplate('tpl.IncDiscussion2.php');
						$this->mData['discussion_helpers'][$topic->getId()]->addDiscussions($discussions);
						
						foreach ($discussions as $discussion) {
							if (1 < $discussion->getPostsCount()) {
								$this->mData['recent_discussions'][] = $discussion;	
							}
						}
					}
				}
				
				$this->mData['added_discussions'] = DiscussionPeer::getDiscussionsAddedToKnowledgeBase($this->mData['group']);
				$this->mData['recent_discussions'] = array_merge($this->mData['added_discussions'], $this->mData['recent_discussions']);			
				usort($this->mData['recent_discussions'], array("DiscussionPeer", "compareDiscussion"));
				$this->mData['posts_count'] = array_sum(DiscussionPeer::getPostsCount($this->mData['recent_discussions']));
				$this->mData['latest_post'] = (0 < count($this->mData['recent_discussions'])) ? $this->mData['recent_discussions'][0]->getLatestPost() : null;					
				
				$file_factory = FileFactory::getInstance();
				$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('title', $this->mData['group']->getName() . ' Discussion Board - GoAbroad.net'));
				$file_factory->invokeStaticClass('Template', 'setMainTemplateVar', array('layoutID','discussion_board-1.2'));
				
				$this->mData['sub_navigation'] = $file_factory->getClass('SubNavigation');
				$this->mData['sub_navigation']->setContext('GROUP');
				$this->mData['sub_navigation']->setContextID($this->mData['group']->getGroupID());
				$this->mData['sub_navigation']->setLinkToHighlight('DISCUSSION_BOARD');			
			
				$profile_comp = $file_factory->invokeStaticClass('ProfileCompFactory','getInstance',array())->create( ProfileCompFactory::$GROUP_CONTEXT );
				$profile_comp->init($this->mData['group']->getGroupID());
				$this->mData['profile'] = $profile_comp->get_view();
				
				$file_factory->invokeStaticClass('Template','setMainTemplate', array($file_factory->getTemplate('LayoutMain')));
				
				// set kb help text
				if( $this->mData['is_admin_logged'] ){
					$this->mData['kbHelpText'] = isset($GLOBALS['CONFIG'])
						? "The Knowledge Base is your site's main discussion board. Topics posted here are viewable by all members. If there are any discussion threads in your groups that you want to share with all the members of your site, simply click on 'Add to Knowledge Base'."
						: "The Knowledge Base is your group's main discussion board. Topics posted here are viewable by all members of your Advisor Group. If there are any discussion threads in your subgroups that you want to share with all the members of your group, simply click on 'Add to Knowledge Base'.";
				}
				elseif( $this->mData['is_member_logged'] ){
					$this->mData['kbHelpText'] = "Welcome to the Knowledge Base. This is where you will find relevant discussions about ".$this->mData['group']->getName().". Click on the Topic name to view the discussion.";
				}
				else{
					$this->mData['kbHelpText'] = "Welcome to the Knowledge Base. This is where you will find relevant discussions about ".$this->mData['group']->getName().".";
				}
				
				
				$tpl = new Template();
				$tpl->setVars($this->mData);
				$tpl->out('travellog/views/discussionboard/tpl.ViewRootGroupDiscussionBoard.php');		
			}
			else {
				$this->redirect('/index.php');
			}
		}
		
		/**
		 * Apply the root group rules.
		 * 
		 * Root Group Discussion Board Rules:
		 * 	1. If the current user is an admin(meaning administrator of the group or staff) this
		 * 		will always show.
		 * 	2. If the current user is a member but not an admin, this will show if the group
		 * 		has topics already.
		 * 	3. If the current user is public(meaning not a member) this will only show if there
		 * 		are discussions that have preferences to be viewed by the public.
		 * 
		 * @return boolean true if the current data is valid.
		 */
		private function validateRootGroup(){
			$this->mData['traveler'] = TravelerPeer::retrieveByPk($this->mData['logger_id']);
			$this->mData['is_admin_logged'] = (0 < $this->mData['logger_id'] AND ($this->mData['group']->getAdministratorID() == $this->mData['logger_id'] || $this->mData['group']->isStaff($this->mData['logger_id'])));
			$this->mData['is_member_logged'] = ($this->mData['is_admin_logged'] || (0 < $this->mData['logger_id'] AND $this->mData['group']->isMember($this->mData['traveler'])));			
			$this->mData['can_reply'] = $this->mData['is_member_logged'];
			$this->mData['discussions_count'] = DiscussionPeer::getGroupDiscussionsCount($this->mData['group'], $this->mData['logger_id']);
			$this->mData['topics_count'] = TopicPeer::getTopicsCount($this->mData['group'], $this->mData['logger_id']);
			
			if ($this->mData['is_admin_logged'] || ($this->mData['is_member_logged'] && $this->mData['topics_count']) || $this->mData['discussions_count']) {
				return true;
			}
			
			return false;
		}
		
		/**
		 * General rules:
		 * 	1. The url must be /discussion_board/home/<group_id>.
		 * 	2. <group_id> must be a valid ID of a root group or a subgroup.
		 * 
		 * @return boolean true if the current data is valid.
		 */
		function validate(){
			if (isset($this->mData['group_id']) && 0 < $this->mData['group_id']) {
				$this->mData['group'] = GroupPeer::retrieveByPk($this->mData['group_id']);
				
				if (!is_null($this->mData['group']) AND Group::ADMIN == $this->mData['group']->getDiscriminator()) {
					$this->mData['is_subgroup'] = (0 < $this->mData['group']->getParentID());
					return true;
				}
			}
			
			return false;				
		}	
	
	}