<?php
	/**
	 * @(#) Class.FetchDiscussionJSON.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - 05 28, 09
	 */
	
	/**
	 * Action class for fetching discussion as JSON.
	 */
	class FetchDiscussionJSON {

		/**
		 * Validates the given data.
		 * 
		 * @return boolean true if the data are valid. false otherwise.
		 */
		static function validate(array &$data){
			require_once('travellog/model/Class.DiscussionPeer.php');
			$data['discussion'] = DiscussionPeer::retrieveByPk($data['discussionID']);
			
			return (is_null($data['discussion'])) ? false : true;
		}
		
		/**
		 * Executes the action.
		 * 
		 * @param mixed[] $data The data from the controller.
		 * 
		 * @return void
		 */
		static function execute(array $data){
			if (self::validate($data)) {
				$tpl = " " .
					"	{	".
					"		'isSuccessful': true, ".
					"		'object': {".
					"			'id': ".$data['discussion']->getID().", ".
					"			'featured': ".$data['discussion']->getIsFeatured().", ".
					"			'privacySetting': ".$data['discussion']->getPrivacySetting().", ".
					"			'title': '".preg_replace('/\'/', '\\\'', $data['discussion']->getTitle())."'".
					"		}".
					"	}";
				
				echo preg_replace('/\s+/', ' ', $tpl);
			}
			else {
				echo "{'isSuccessful': false, 'object': null }";
			}
		}
		
	}
	
