<?php
class EntriesWritenCount{
	function GetFilterList(&$props){
		require_once('Class.Connection.php');
		require_once('Class.Recordset.php');
		$conn = new Connection;
		$rs1  = new Recordset( $conn );
		$rs2  = clone($rs1);
		
		$sql1 = sprintf
					(
						'SELECT SQL_CALC_FOUND_ROWS travellogID FROM tblTraveler, tblTravel, tblTrips, tblTravelLog ' .
						'WHERE ' .
						'tblTraveler.travelerID 					= tblTravel.travelerID ' .
						'AND tblTraveler.isSuspended 	= 0 ' .
						'AND tblTraveler.active 				> 0 ' .
						'AND tblTravel.travelID 				= tblTrips.travelID ' .
						'AND tblTrips.locID    					IN (%s) ' .
						'AND tblTrips.tripID    					= tblTravelLog.tripID ' .
						'AND tblTravelLog.deleted 			= 0 ' . 
						'GROUP BY travellogID ' .
						'LIMIT 1'
						,$props['locationIDList']
					);
					
		$rs1->Execute($sql1);
		
		$sql2 = 'SELECT FOUND_ROWS() as totalrecords'; 
		$rs2->Execute($sql2);
		
		$props['entries_writen_count'] = $rs2->Result(0, 'totalrecords'); 
	}
}
?>
