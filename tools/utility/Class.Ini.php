<?php	
	// edited by: marc
	// inserted entry for dev.teachabroad.com :: forums
	class Ini{	
		/*
		/public static $host = "192.168.6.4";
		public static $user = "root";
		public static $pass = "";		
		public static $gMapKey = "ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RScORTG_sOsrv1IBKpZjqjBOwk9LhSSRQf3fsJxmnB-oWoJEJWls7bRhQ"; // test.goabroad.com
		*/
		
     
		public static function getHost(){
			switch($_SERVER['SERVER_NAME']){
				case "dev.goabroad.com":
					return "192.168.3.9";
					break;
				case "dev.goabroad.net":
				case "dev.teachabroad.com":
					return "192.168.3.9";
					break;
				case "www.goabroad.net":
					return "eth0.ga_database01";
					break;
				case "aifs.goabroad.net.local":
				case "goabroad.net.local":
					return "192.168.1.120";
					break;
				case "goabroad.com.local":
					return "192.168.1.120";
					break;
				case "goabroad.info.local":
					return "192.168.1.120";
					break;
                //separated dev & www database connections - icasimpan June 12,2007
                case "ga-dev01.goabroad.info":
                case "dev.goabroad.info":
                case "aifs-dev.goabroad.net":
                        return "192.168.3.9";
                        break;
				case "ciee-dev.goabroad.net":
		                return "192.168.3.9";
		                break;
				case "proworld-dev.goabroad.net":
				        return "192.168.3.9";
				        break;	
				case "proworld.goabroad.net":
				        return "192.168.3.9";
				        break;	
				case "www.goabroad.info":
					return "eth0.ga_database01";
					break;
				case "gap.goabroad.com.local":
					return "192.168.1.120";
					break;
				case "test.goabroad.net":
					return "192.168.3.9";
					break;
				
				case "local.proj":
				case "localhost":
				case "aifs.goabroad.net.local":
				case "globalsemesters.goabroad.net.local":
					return "192.168.1.120";
					break;
				case "aifs-web03.goabroad.net":
					return "192.168.3.9";
					break;
				case "aifs.goabroad.net":
					return "192.168.3.9";
					break;
				case "proworld.goabroad.net.local":
					return "192.168.1.120";
					break;
				// added case for test.goabroad.net.local
				case "test.goabroad.net.local":
					return "192.168.6.155";
					break;
				default:// its either ga-web01.goabroad.com or ga-web02.goabroad.com or ga-web03.goabroad.com or ga-web04.goabroad.com
					return "eth0.ga_database01";
			}
		}
		public static function getUser(){
			switch($_SERVER['SERVER_NAME']){
				case "dev.goabroad.com":
				    return "dbroot";
					break;
				case "dev.goabroad.net":
					return "dbroot";
					break;
				case "dev.teachabroad.com":
					return "dbroot";
					break;
				case "www.goabroad.net":
					return "dbroot";
					break;
				case "local.proj":
				case "aifs.goabroad.net.local":
				case "my.aifs.org.local":
				case "goabroad.net.local":
					return "root";
					break;
				case "goabroad.com.local":
					return "root";
					break;
				case "goabroad.info.local":
					return "root";
					break;
				case "goabroad.info":
					return "root";
					break;
				case "gap.goabroad.com.local":
					return "root";
					break;
				
				// Added for testing
				case "aifs.goabroad.net.local":
					return "root";
					break;
				case "test.goabroad.net":
					return "goabroad";
					break;
				case "localhost":
					return "root";
					break;
				case "proworld.goabroad.net.local":
					return "root";
					break;
				case "test.goabroad.net.local":
					return "root";
					break;
				default:// its either ga-web01.goabroad.com or ga-web02.goabroad.com or ga-web03.goabroad.com or ga-web04.goabroad.com
					//return "root";
                                        return "dbroot";
			}
		}
		public static function getPassword(){
			switch($_SERVER['SERVER_NAME']){
				case "dev.goabroad.com":
                                        return "w1l*0b1_#hunt3r$";
					break;
			
				case "dev.goabroad.net":
				case "dev.teachabroad.com":
                    return "w1l*0b1_#hunt3r$";
					break;
				case "www.goabroad.net":
                    return "w1l*0b1_#hunt3r$";
					break;
				case "aifs.goabroad.net.local":
				case "my.aifs.org.local":
				case "goabroad.net.local":
					return "";
					break;
				case "goabroad.com.local":
					return "";
					break;
				case "goabroad.info.local":
					return "";
					break;	
				case "goabroad.info":
					return "";
					break;
				case "gap.goabroad.com.local":
					return "";
					break;
				
				// Added for testing
				case "aifs.goabroad.net.local":
					return "";
					break;
				case "test.goabroad.net":
                                        return "w1l*0b1_#hunt3r$";
					break;
				case "local.proj":
				case "localhost":
					return "";
					break;	
				case "proworld.goabroad.net.local":
					return "";
					break;	
				case "test.goabroad.net.local":
					return "";
					break;
				default:// its either ga-web01.goabroad.com or ga-web02.goabroad.com or ga-web03.goabroad.com or ga-web04.goabroad.com
                                        return "w1l*0b1_#hunt3r$";
			}
		}
		public static function getGMapKey(){
			switch($_SERVER['SERVER_NAME']){
				case "dev.goabroad.net":
					return "ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RSxAnUob8CCeJxJdtYJBdK6zFkHgxQ5xa2hmJL9BHBK116ssslR4r05yA"; 
					break;
				case "www.goabroad.net":
					return "ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RQwbP-JBhpjGqlbK3A4xF5aLrtY6RSWOoaOMJXoU0oq2GqMOlflVMN-hg";
					break;
				case "aifs.goabroad.net.local":
					return "ABQIAAAA2kMdBmIUP_yRWOMgM_8pPRR7Mtn6aYuvBXvfml3eYH7YKBZuDhQRfH8k1NUAUnqZzm3th50k06OOnQ";
					break;
				case "aifs-dev.goabroad.net":
					return "ABQIAAAA2kMdBmIUP_yRWOMgM_8pPRSuhWSY3v2csAEBjf9N5IzGMINBMBR-j0EguIP45CKb1y9na3WTrQvD2Q";
					break;
				case "goabroad.net.local":
					return "ABQIAAAAxssX44ErD-uqyd-GW74yDBS_IWFy692u9pZ7fVeFegkeYwDfHRQ0DmyB5o0hSF4zV4IdIwGBmzCXEw";
					break;
				case "gap.goabroad.com.local":
					return "ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RTV-_3LGXMxVigSUOPcZFnxax-H6BSM1zymuVhaY_MSoa-RlZLqtZgJ0g";
					break;
				case "test.goabroad.net":
					return "ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RThc8huphRfTZhCAFwcw9vOKp4exhQdDEMcpAZRlfpP1_-IS8JTPlUDqA";
					break;
				case "aifs.goabroad.net":
					return "ABQIAAAA2kMdBmIUP_yRWOMgM_8pPRQrvjx87JdXXCOXbdVxwlvWKrY8WxQ_WbjonOfN_EJS8NRSfe9yAbgR9Q";
					break;	
				case "proworld-dev.goabroad.net":
					return "ABQIAAAA2kMdBmIUP_yRWOMgM_8pPRQJPjEz3m8Ep2cXZt6oHWplRmafRhTQp25ZSB4TusPW_bKomQnBGkA4IA";
					break;	
				case "proworld.goabroad.net":
					return "ABQIAAAA2kMdBmIUP_yRWOMgM_8pPRSMkUCtVtwEkgi-LACZFk1vfOFK1RSuKN6AYHgsfrMUXtWGP4BloC_ZIQ";
					break;			
			}
		}

		public static function getMailServer(){
			//temporary
			if($_SERVER['SERVER_NAME'] == 'dev.goabroad.net'){
				//return '216.241.160.141';
				//return '216.241.160.153';
			}
			return "192.168.2.16";
			/***
			switch($_SERVER['SERVER_NAME']){
				case "aifs-web03.goabroad.net":
					return "192.168.2.15";
					break;
				default:
					return "192.168.3.6";
			}
			***/
		}		
		//public static $gMapKey = "ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RR_mv8qxSfI61UPf1AUwo3FOOuLUhRKkqFi4o2p6bfPfZfqmGtkz3Jlaw"; // ip ni aldwin
		//public static $gMapKey = "ABQIAAAAxssX44ErD-uqyd-GW74yDBS_IWFy692u9pZ7fVeFegkeYwDfHRQ0DmyB5o0hSF4zV4IdIwGBmzCXEw"; // goabroad.net.local
		//public static $gMapKey = "ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RT2yXp_ZAY8_ufC3CFXhHIE1NvwkxQK172wzBL98irx9meqK9uiOlMTOw"; // localHost
		//public static $gMapKey = "ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RSxAnUob8CCeJxJdtYJBdK6zFkHgxQ5xa2hmJL9BHBK116ssslR4r05yA"; // dev.goabroad.net
		//public static $gMapKey = "ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RQwbP-JBhpjGqlbK3A4xF5aLrtY6RSWOoaOMJXoU0oq2GqMOlflVMN-hg"; // www.goabroad.net
		//public static $gMapKey = "ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RTaRo377kmnnPR-m6WZJ1eKrLA3uhRWOb5bAJou0jRcQpZZJ4tu_Ndvog"; // www.goabroad.com					
		//public static $gMapKey = "ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RThc8huphRfTZhCAFwcw9vOKp4exhQdDEMcpAZRlfpP1_-IS8JTPlUDqA"; // test.goabroad.net
		
		public static function isTemplateLogDisplyed() {
			return ($_SERVER['REMOTE_ADDR'] == '125.212.64.118' || 0 < strlen(strstr($_SERVER['SERVER_NAME'], 'goabroad.net.local')) || $_SERVER['SERVER_NAME'] == 'dev.goabroad.net');	
		}
		
	}
	/*** For the Templating System (June 01, 2007)***/
	/*
	if ($_SERVER['REMOTE_ADDR'] == '125.212.64.118' || $_SERVER['SERVER_NAME'] == 'goabroad.net.local' || $_SERVER['SERVER_NAME'] == 'dev.goabroad.net') {
			require_once('Class.Template.php');
			Template::displayLog();
	}
	*/	
?>
