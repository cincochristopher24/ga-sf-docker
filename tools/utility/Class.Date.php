<?php
class ClassDate
{
	private static $instance;
	
	private function ClassDate() 
	{
	}
	
 	public static function instance()
 	{
 		if (self::$instance == null ) {
 			self::$instance = new ClassDate();
 		}
 		return self::$instance;	
 	}

	function CreateODBCDate($datevalue,$delimeter)
	{
		$newdate = explode($delimeter,$datevalue);
		$newdate = $newdate[2].'-'.$newdate[0].'-'.$newdate[1]; 
		return $newdate;
	}
	
	function FormatSqlDate($datevalue,$delimeter)
	{
		$newdate = explode($delimeter,$datevalue);
		$newdate = $newdate[1].'/'.$newdate[2].'/'.$newdate[0]; 
		return $newdate;
	}	 		 	
}
?>
