<?php
/**
 * Created on Mar 1, 2007
 * Author: Daf 
 */
 
//require_once('Class.Ini_Config.php');

	 class ErrorLog{
	 	
	 	/**
	     * Define variables for Class Member Attributes
	     */
	        private $mConn     		 = NULL;
	        private $mRs       		 = NULL;
	        
	        private $mLogSiteID  	 = NULL;
			private $mLogSiteName 	 = '';
	          
	        private $mErrorLogID  	 = NULL;
	        private $mLogDate	  	 = NULL;
	        private $mErrorType  	 = NULL;
	        private $mMessage  	 	 = NULL;
	        private $mScriptName  	 = NULL;
	        private $mLineNum  	 	 = NULL;
	        private $mRemoteAddress  = NULL;
	        private $mReferer  	 	 = NULL;
	        private $mBrowser  	 	 = NULL;
	        private $mQueryString  	 = NULL;
	        private $mMethod  	 	 = NULL;
	        private $mFileName 	 	 = NULL;
			private $mLoggedTravID 	 = NULL;
			private $mArrPostValues  = array();
			private $mStrPostValues  = '';
			private $mStackTrace 	 = '';
									
	        // static fields
			public static $LogSiteID = 0;	 		
	 		public static $totalRecords = 0;
			    
	 	function ErrorLog($errorLogID = 0){
	 		
			$this->mErrorLogID = $errorLogID;
	 		
			//require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
	    	//$ini = IniConfig::instance();
			//$this->mConn = mysql_connect($ini->getHost(),$ini->getUser(),$ini->getPassword()) or die(mysql_error());
			
			require_once('serverConfig/Class.ServerConfig.php');
			$serverConf = ServerConfig::getInstance();
			
	        $this->mConn = mysql_connect($serverConf->getDbHost(),$serverConf->getDbUser(),$serverConf->getDbPassword()) or die(mysql_error());
            
            if (0 < $this->mErrorLogID){
                
                $sql = "SELECT Logs.tblErrorLog.* , Logs.tblLogSite.logSiteName  FROM Logs.tblErrorLog, Logs.tblLogSite WHERE Logs.tblErrorLog.logSiteID = tblLogSite.logSiteID AND errorlogID = '$this->mErrorLogID' ";
                $this->mRs = mysql_query($sql,$this->mConn);
                
                if (0 == mysql_num_rows($this->mRs)){
                    throw new Exception("Invalid ErrorLogID");        // ID not valid so throw exception
                }
                
                $row = mysql_fetch_array($this->mRs);
                
                // Sets values to its class member attributes.
                $this->mLogDate     	= $row['logdate'];  
                $this->mErrorType   	= stripslashes($row['errortype']);       
                $this->mMessage      	= stripslashes($row['message']);       
                $this->mScriptName  	= stripslashes($row['scriptname']);     
                $this->mLineNum     	= $row['linenum'];           
                $this->mRemoteAddress  	= stripslashes($row['remoteaddress']);       
                $this->mReferer      	= stripslashes($row['referer']);       
                $this->mBrowser   		= stripslashes($row['browser']);       
                $this->mQueryString    	= stripslashes($row['querystring']);       
                $this->mMethod   		= stripslashes($row['method']);   
				$this->mFileName   		= stripslashes($row['filename']);   
                $this->mLoggedTravID	= stripslashes($row['travelerID']);   
				$this->mStrPostValues	= stripslashes($row['postvalues']);  
				$this->mStackTrace		= stripslashes($row['stacktrace']);   
				
                // Sets a value to its parent key.
                $this->mLogSiteID   = $row['logsiteID'];
				$this->mLogSiteName   = $row['logSiteName'];
                
            }
	 	}
	 	
	 	function setLogSiteID($logsiteID){
	 		$this->mLogSiteID  = $logsiteID;
	 	}
	 	
	 	function setErrorType($errortype){
	 		$this->mErrorType  = $errortype;
	 	}
	 	
	 	function setMessage($message){
	 		$this->mMessage  = $message;
	 	}
	 	
	 	function setScriptName($scriptname){
	 		$this->mScriptName  = $scriptname;
	 	}
	 	
	 	function setLineNum($linenum){
	 		$this->mLineNum  = $linenum;
	 	}
	 	
	 	function setRemoteAddress($remoteaddr){
	 		$this->mRemoteAddress  = $remoteaddr;
	 	}
	 	
	 	function setReferer($referer){
	 		$this->mReferer  = $referer;
	 	}
	 	
	 	function setBrowser($browser){
	 		$this->mBrowser  = $browser;
	 	}
	 	
	 	function setQueryString($querystring){
	 		$this->mQueryString  = $querystring;
	 	}
	 	
	 	function setMethod($method){
	 		$this->mMethod  = $method;
	 	}
	 	
		function setFileName($filename){
	 		$this->mFileName  = $filename;
	 	}
	
		function setLoggedTravID($travelerID){
	 		$this->mLoggedTravID  = $travelerID;
	 	}
		
		function setPostValues($arrpostvalues){
	 		$this->mArrPostValues  = $arrpostvalues;
	 	}
	
		function setStackTrace($stacktrace){
	 		$this->mStackTrace  = $stacktrace;
	 	}
	 	
		// getters
	 	function getErrorDate(){
	 		return $this-> mLogDate;
	 	}
	 	
	 	function getErrorType(){
	 		return $this-> mErrorType;
	 	}
	 	
	 	function getMessage(){
	 		return $this-> mMessage;
	 	}
	 	
	 	function getScriptName(){
	 		return $this-> mScriptName;
	 	}
	 	
	 	function getLineNum(){
	 		return $this-> mLineNum;
	 	}
	 	
	 	function getRemoteAddress(){
	 		return $this-> mRemoteAddress;
	 	}
	 	
	 	function getReferer(){
	 		return $this-> mReferer;
	 	}
	 	
	 	function getBrowser(){
	 		return $this-> mBrowser;
	 	}
	 	
	 	function getQueryString(){
	 		return $this-> mQueryString;
	 	}
	 	
	 	function getMethod(){
	 		return $this-> mMethod;
	 	}
	
		function getFileName(){
	 		return $this-> mFileName;
	 	}
	 	
		function getLoggedTravID(){
	 		return $this-> mLoggedTravID;
	 	}
		
		function getPostValues(){
	 		return $this-> mStrPostValues;
	 	}
	
		function getStackTrace(){
	 		return $this-> mStackTrace;
	 	}
	 	
		function getLogSiteName(){
			if (0==strlen($this->mLogSiteName)) {
				$sql = "SELECT Logs.tblLogSite.logSiteName  FROM Logs.tblLogSite WHERE Logs.tblLogSite.logSiteID = '$this->mLogSiteID' ";
			    $rs = mysql_query($sql,$this->mConn);
				if (mysql_num_rows($rs)) {
                   $row = mysql_fetch_array($rs);	
				   $this->mLogSiteName = $row['logSiteName'];;
				}
			}

			return $this->mLogSiteName;
		}
	
        function Create(){
            
            $AErrorType 	= addslashes($this->mErrorType);
            $AMessage 		= addslashes($this->mMessage);
            $AScriptName 	= addslashes($this->mScriptName);
            $ARemoteAddress = addslashes($this->mRemoteAddress);
            $AReferer 		= addslashes($this->mReferer);
            $ABrowser 		= addslashes($this->mBrowser);
            $AQueryString 	= addslashes($this->mQueryString);
            $AMethod 		= addslashes($this->mMethod);
			$AFileName 		= addslashes($this->mFileName);
            
            if (0==strlen($this->mStrPostValues)) {
				foreach ($this->mArrPostValues as $key => $value) 
					$this->mStrPostValues .= $this->mStrPostValues . $key . " ^ " . $value . " # " ;
			}
			
			$AStackTrace 		= addslashes($this->mStackTrace);
			
            $now  = date("Y-m-d H:i:s");
            
            $sql = "INSERT into Logs.tblErrorLog (logsiteID, logdate, errortype, message, scriptname, linenum, remoteaddress, referer, browser, querystring, method, filename, travelerID, postvalues, stacktrace) " .
                    "VALUES ('$this->mLogSiteID', '$now', '$AErrorType', '$AMessage', '$AScriptName', '$this->mLineNum', '$ARemoteAddress', '$AReferer', '$ABrowser', '$AQueryString', '$AMethod' , '$AFileName' , '$this->mLoggedTravID' , '$this->mStrPostValues', '$AStackTrace')";
                               
            $this->mRs = mysql_query($sql,$this->mConn); 
            
            $this->mErrorLogID = intval(mysql_insert_id($this->mConn));
            
        }
	 	
	 	function getDetails(){
	 		
	 		$txtBody = '';
			    	
			$txtBody .= $this-> mLogDate . "<br />" ;
			$txtBody .= $this-> mErrorType . "<br />" ;
			$txtBody .= $this-> mMessage . "<br />" ;
			$txtBody .= "Error occurred at line " . $this-> mLineNum . " in <b>" .  $this-> mScriptName . "</b><br />" ;
			$txtBody .= "Referrer: " . $this-> mReferer . "<br />" ;
			$txtBody .= "Remote Address: " . $this->mRemoteAddress . "<br />" ;
			$txtBody .= "Browser: " . $this-> mBrowser . "<br />" ;
			$txtBody .= "Method: " . $this-> mMethod . "<br />" ;
			$txtBody .= "Query String: " . $this-> mQueryString . "<br />" ;
			$txtBody .= "Server Name: " . $this->getLogSiteName() . "<br />" ;
			$txtBody .= "File Name: " . $this-> mFileName . "<br />" ;
			$txtBody .= "Logged TravelerID: " . $this-> mLoggedTravID . "<br />" ;
			
			$postValues = '';
			
			//if array of POST was set, loop thru to construct structure
			foreach ($this->mArrPostValues as $key => $value)
				$postValues .= "[ " .  $key . " ]". " => " . $value . " <br /> " ;
			
			// if no structure was created, loop thru string of post vars, if any and conctruct structure
			if (0==strlen($postValues) && strlen($this->mStrPostValues)){
				$arrPostValues = explode("#",$this->mStrPostValues);
				foreach($arrPostValues as $eachValue){
					$arrKeyValue = explode("^",$eachValue);
					$postValues .= "<br />[ " .  $arrKeyValue[0] . " ]". " => " . $arrKeyValue[1] ;
				}
			}
			
			$txtBody .= "POST Values:" . $postValues . "<br />" ;
		
			$txtBody .= "Stack Trace:<br /> " . $this->mStackTrace . "<br />" ;
			
			return $txtBody;
	 	}
	 	
	 	function sendEmail (){
			/***
			require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
	    	$ini = IniConfig::instance();
			
			if ($ini->IsAllowSendEmail()) {
				require_once("class.phpmailer.php");
			  
				$mail = new PHPMailer(); 
				$mail->IsSMTP(); 
				$mail->IsHTML(true);
			
				$mail->FromName = "GoAbroad Network Error";
				$mail->From = "error@goabroad.net";
			 
				$mail->Subject = "GoAbroad Network Error";
				$mail->Body = $this->getDetails() ;
				$mail->AddAddress("error@goabroad.net");
				$mail->Send();					
			}
			else
				echo($this->getDetails());
			***/
			require_once('serverConfig/Class.ServerConfig.php');
			//if(!ServerConfig::getInstance()->isMailDisabled()){
				require_once("class.phpmailer.php");
				$mail = new PHPMailer(); 
				$mail->IsSMTP(); 
				$mail->IsHTML(true);
				$mail->FromName = "GoAbroad Network Error";
				$mail->From = "error@goabroad.net";
				$mail->Subject = "GoAbroad Network Error";
				$mail->Body = $this->getDetails() ;
				$mail->AddAddress("error@goabroad.net");
				$mail->Send();
			//}
			//else{
				//echo($this->getDetails());
			//}
			
		}

	 	public static function isExistLogSite($logsite){
	 		
			require_once("Class.Connection.php");
		   	require_once("Class.Recordset.php");
			
			/***
			require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
	    	$ini = IniConfig::instance();
			***/
			
			try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			/*
	 		$conn = mysql_connect($ini->getHost(),$ini->getUser(),$ini->getPassword()) or die(mysql_error());
            
			$sql = "SELECT * from Logs.tblLogSite WHERE logSiteName = '$logsite' ";
			$rs = mysql_query($sql,$conn);
			
			if (0 == mysql_num_rows($rs))
               return false;
            else {
            	$row = mysql_fetch_array($rs);
            	self::$LogSiteID = $row['logSiteID'];
            	return true;  		 	
            }
			*/
			
			$sql = "SELECT * from Logs.tblLogSite WHERE logSiteName = '$logsite' ";
			$rs-> Execute($sql);
			
			if (0 == $rs->Recordcount())
               return false;
            else {
            	self::$LogSiteID = $rs->Result(0,"logSiteID");
            	return true;  		 	
            }
			
	 	}
	 	
	 	public static function addLogSite($logsite){
	 		
			/***
			require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
	    	$ini = IniConfig::instance();
	
	 		$conn = mysql_connect($ini->getHost(),$ini->getUser(),$ini->getPassword()) or die(mysql_error());
			***/
			if (!self::isExistLogSite($logsite)){
				require_once('Class.dbHandler.php');
	            $db = new dbHandler();
				$sql = "INSERT INTO Logs.tblLogSite (logSiteName) VALUES('$logsite')";
				//$rs = mysql_query($sql,$conn);
				//self::$LogSiteID = intval(mysql_insert_id($conn));
				$db->execute($sql);
				self::$LogSiteID = intval($db->getLastInsertedID());
			}	
	 	}
	 	
	 	public static function getLogSiteID(){
	 		return self::$LogSiteID;
	 	}
	
	 	public static function getLogSites(){
			
			require_once("Class.Connection.php");
		   require_once("Class.Recordset.php");
		
	 		try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}

	 		$sqlQuery = "select distinct logSiteID from Logs.tblLogSite group by logSiteName order by logSiteID asc";
	 		$rs-> Execute($sqlQuery);

	 		$arrLogSites = array();
	 		
	 		if (0 < $rs->Recordcount()){
	 			while ($logSite = mysql_fetch_array($rs->Resultset())){
	 				array_push($arrLogSites, new LogSite($logSite['logSiteID']));
	 			}
	 		}

	 		return $arrLogSites;
	 	}
	 	
	 	public static function getErrorLogs($_logSiteID = 0, $_rowsLimit = NULL, $_filterCriteria2 = NULL){
			
			require_once("Class.Connection.php");
		   require_once("Class.Recordset.php");
		
	 		try {
	 			$conn = new Connection();
				$rs   = new Recordset($conn);
				$rs2  = new Recordset($conn);
 			}
			catch (Exception $e) {				   
			   throw $e;
			}
			
			if ($_rowsLimit == NULL)
				$queryLimit = " ";
			
			else{
				$row = $_rowsLimit->getRows();
				$offset = $_rowsLimit->getOffset() * $row;
				$queryLimit = " LIMIT ".$offset. ", ".$row;
			}

			if ($_filterCriteria2){
				$ctr = 0;
				$condition = "";
				foreach($_filterCriteria2 as $criteria){
					$myconditions = $criteria-> getConditions();
					$attname = $myconditions[0]-> getAttributeName();
					$operation = $myconditions[0]-> getOperation();
					$value = $myconditions[0]-> getValue();
					
					if (0 < strlen($value)){
						$condition = $condition . " AND ";

						switch ($operation){
						 	case FilterOp::$GREATER_THAN_OR_EQUAL:
						 		if (NULL != $value)
						 			$condition = $condition . $attname . " >= " . " '$value' ";
						 		break;
						 	case FilterOp::$LESS_THAN_OR_EQUAL:
						 		if (NULL != $value){						 			
									$timeStamp = strtotime($value);
									$timeStamp += 24 * 60 * 60 * 1; // (add 7 days)
									$newDate = date("Y-m-d", $timeStamp);
						 			$condition = $condition . $attname . " <= " . " '$newDate' ";
						 		}
						 		break;
						 	case FilterOp::$LIKE:
						 		if (NULL != $value)
						 			$condition = $condition . $attname . " LIKE " . " '%$value%' ";
						 		break;
						}
					}
				}
			}
			else
				$condition = "";

			$sqlQuery = " select SQL_CALC_FOUND_ROWS errorlogID from Logs.tblErrorLog where 1 = 1 " . $condition .
						" and logsiteID = " . $_logSiteID .	" order by errorlogID desc " . $queryLimit;
			$rs-> Execute($sqlQuery);
			
			$sqlQuery2 = "select FOUND_ROWS() as totalRecords";
			$rs2-> Execute($sqlQuery2);
			
			ErrorLog::$totalRecords = $rs2-> Result(0, "totalRecords");
			$arrErrorLogs = array();
			
			if (0 < $rs->Recordcount()){
				while ($errorLogs = mysql_fetch_array($rs->Resultset())){
					array_push($arrErrorLogs, new ErrorLog($errorLogs['errorlogID']));
				}
			}
			
			return $arrErrorLogs;
	 	}
	 	
	 	public static function setException ($e){
	 		
	 		$log=array();
	    	$type = 'EXCEPTION';
	    
		    if (!ErrorLog::isExistLogSite($_SERVER['SERVER_NAME']))
		    	ErrorLog::addLogSite($_SERVER['SERVER_NAME']);
		    
		    $logSiteID = ErrorLog::getLogSiteID();
			
			// added by chris start session
			require_once('travellog/model/Class.SessionManager.php');
		    
			$errorLog = new ErrorLog();
			$errorLog->setLogSiteID($logSiteID);
			$errorLog->setErrorType($type);
			$errorLog->setMessage(htmlentities($e->getMessage()));
			$errorLog->setScriptName($e->getFile());
			$errorLog->setLineNum($e->getLine());
			if (isset($_SERVER['REMOTE_ADDR']) && strlen($_SERVER['REMOTE_ADDR']))
				$errorLog->setRemoteAddress(htmlentities($_SERVER['REMOTE_ADDR']));
			if (isset($_SERVER['HTTP_REFERER']) && strlen($_SERVER['HTTP_REFERER']))
				$errorLog->setReferer(htmlentities($_SERVER['HTTP_REFERER']));
			if (isset($_SERVER['HTTP_USER_AGENT']) && strlen($_SERVER['HTTP_USER_AGENT']))
				$errorLog->setBrowser(htmlentities($_SERVER['HTTP_USER_AGENT']));
			if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']))
				$errorLog->setQueryString(htmlentities($_SERVER['QUERY_STRING']));
			if (isset($_SERVER['REQUEST_METHOD']) && strlen($_SERVER['REQUEST_METHOD']))
				$errorLog->setMethod(htmlentities($_SERVER['REQUEST_METHOD']));
			if (isset($_SERVER['SCRIPT_NAME']) && strlen($_SERVER['SCRIPT_NAME']))
				$errorLog->setFileName(htmlentities($_SERVER['SCRIPT_NAME']));
			if (isset($_SESSION['travelerID']))
				$errorLog->setLoggedTravID($_SESSION['travelerID']);
			if (isset($_POST))
				$errorLog->setPostValues($_POST);
		
			$errorLog->setStackTrace($e->getTraceAsString());
			
			$errorLog->Create();
			
			$errorLog->sendEmail();
	 	}
	 	
		public static function showTicketForm($e=NULL){
			self::setException($e);
			header("location: /ticket.php?action=show_form");
		}
	 }
?>
