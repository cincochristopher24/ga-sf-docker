<?php
	/**
	* just holds all date times that will be converted to local time in js
	**/
	
	require_once('JSON.php');
	
	class DateTimeHolder {
		
		// keys that will be used
		const KEY_DATETIME			= 'dateTime';
		const KEY_CONTAINER_CLASS	= 'containerClass';
		const KEY_TIMEZONE			= 'timezone';
		const KEY_EVENT_ID			= 'eventID';
		
		// event details
		const KEY_EVENT_TITLE		= 'eventTitle';
		const KEY_EVENT_CONTEXT		= 'eventContext';
		
		// profile details
		const KEY_EVENT_LINK		= 'eventLink';
		const KEY_EVENT_CONTROLS	= 'eventControls';
		
		const EVENT_PREFIX			= "eventDateTime";
		
		private static $instance 	= null;
		private $dateTimes			= array();
		private $eventDetails       = array();
		private $json				= null;
		
		private function __construct(){
			$this->json 			= new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
		}
		
		public function addDateTime($dateTime, $containerClass, $timezone){
			$this->dateTimes[] 	= array_merge( $this->eventDetails, array(
					self::KEY_DATETIME			=> $dateTime,
					self::KEY_CONTAINER_CLASS	=> $containerClass,
					self::KEY_TIMEZONE			=> $timezone
				)
			);
		}
		
		public function getAllDateTimes(){
			return $this->dateTimes;
		}
		
		public function toJsonData(){
			return $this->json->encode($this->dateTimes);
		}
		
		static public function getInstance(){
			if( is_null(self::$instance) ) self::$instance = new self;
			return self::$instance;
		}
		
		// add event details
		public function addEventsDetails($eventID,$eventTitle,$eventContext){
			$this->eventDetails = array(
				self::KEY_EVENT_ID			=> $eventID,
				self::KEY_EVENT_TITLE		=> $eventTitle,
				self::KEY_EVENT_CONTEXT		=> $eventContext
			);
		}
		
		public function addProfileEventDetails($eventID,$eventTitle,$eventContext,$eventLink,$eventControls){
			$this->eventDetails = array(
				self::KEY_EVENT_ID			=> $eventID,
				self::KEY_EVENT_TITLE		=> $eventTitle,
				self::KEY_EVENT_CONTEXT		=> $eventContext,
				self::KEY_EVENT_LINK		=> $eventLink,
				self::KEY_EVENT_CONTROLS	=> $eventControls
			);
		}
	}

