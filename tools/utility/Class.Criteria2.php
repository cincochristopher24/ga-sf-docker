<?php
	/**
	 * <b>Criteria2</b> class.
	 * @author Aldwin S. Sabornido
	 * @version 1.0 PHP5
	 * @see http://www.GoAbroad.com/
	 * @copyright 2006-2007
	 */
	
	class Criteria2 {
		/**
		 * Criteria2 constants
		 */
		const EQUAL = "=";
		const NOT_EQUAL = "<>";
		const GREATER_THAN = ">";
		const LESS_THAN = "<";
		const GREATER_EQUAL = ">=";
		const LESS_EQUAL = "<=";
		const ISNULL = "IS NULL";
		const ISNOTNULL = "IS NOT NULL";
		const LIKE = "LIKE";
		const IN = "IN";
		const NOT_IN = "NOT IN";
		const LEFT_JOIN = "LEFT JOIN";
		const RIGHT_JOIN = "RIGHT JOIN";
		
		/**
		 * The array of sql ORDER BY's.
		 * 
		 * @var array
		 */
		private $mOrderBy = array();
		
		/**
		 * The sql limit representation.
		 * 
		 * @var string
		 */
		private $mLimit = "";
		
		/**
		 * The criterias object.
		 * 
		 * @var array
		 */
		private $mCriteria2 = array();
		
		/**
		 * The array of sql GROUP BY's.
		 * 
		 * @var array
		 */
		private $mGroupBy = array();
		
		private 
			
		$_CRITERIA = NULL,
		
		$_FIELDS   = NULL,
		
		$_TABLES   = NULL,
		
		$_ORDERBY  = NULL,
		
		$_GROUPBY  = NULL,
		
		$_LIMIT    = NULL;
		
		/**
		 * Adds a new criteria.
		 * 
		 * @param string $column The column name to be added.
		 * @param mixed $value The value for the column.
		 * 
		 * @return void
		 */
		function add($column, $value, $op = Criteria2::EQUAL){
			switch ($op) {
				case Criteria2::EQUAL:
				case Criteria2::NOT_EQUAL:
				case Criteria2::GREATER_THAN:
				case Criteria2::LESS_THAN:
				case Criteria2::GREATER_EQUAL:
				case Criteria2::LESS_EQUAL:
				case Criteria2::LIKE:
					$criterion  = $column." ".$op." ".$value."";
					break;
				case Criteria2::ISNULL:
				case Criteria2::ISNOTNULL:
					$criterion = "".$value." ".$op;
					break;
				case Criteria2::IN:
				case Criteria2::NOT_IN:
					$criterion = $column." ".$op." ($value)"; 
					break;
					default:
						return;
			}
			
			$this->mCriteria2[] = $criterion;
		}
		
		/**
		 * Adds a GROUP BY column.
		 * 
		 * @param string $column The name of the column to be added in the GROUP BY clause.
		 * 
		 * @return void
		 */
		function addGroupByColumn($column){
			$this->mGroupBy[] = $column;
		}
		
		/**
		 * Adds a sql ORDER BY $column.
		 * 
		 * @param string $column The name of the column to be added an ORDER BY clause.
		 * @param boolean $isAscending The flag whether to add an ascending or descending ORDER BY.
		 * 
		 * @return void
		 */
		function addOrderByColumn($column, $isAscending = true) {
			$column .= ($isAscending) ? " ASC" : " DESC";
			$this->mOrderBy[] = $column;
		}
		
		/**
		 * Adds a sql LIMIT clause
		 * 
		 * @param integer $rows The number of rows in the LIMIT clause.
		 * @param integer $offset The offset of the data to be retrieved.
		 * 
		 * @return void
		 */
		function addLimit($rows, $offset = 0){
			$this->mLimit = "LIMIT ".$offset.",".$rows;
		}
		
		/**
		 * Returns the created LIMIT clause.
		 * 
		 * @return string
		 */
		function createLimitClause(){
			return $this->mLimit;
		}
		
		/**
		 * Returns the GROUP BY clause created from the given columns added to GROUP BY.
		 * 
		 * @return string the GROUP BY clause created from the given columns added to GROUP BY.
		 */
		function createGroupByClause(){
			return (0 < count($this->mGroupBy)) ? ' GROUP BY '.implode($this->mGroupBy, ', ') : '';
		}
		
		/**
		 * Returns the ORDER BY clause created from the given columns added to ORDER BY.
		 * 
		 * @return string the ORDER BY clause created from the given columns added to ORDER BY.
		 */
		function createOrderByClause(){
			return (0 < count($this->mOrderBy)) ? ' ORDER BY '.implode($this->mOrderBy, ', ') : '';
		}
		
		/**
		 * Returns the sql clause generated from the given limit, order by and criterias.
		 * 
		 * @return string the sql clause generated from the given limit, order by and criterias.
		 */
		function createSqlClause(){
			$sql = "";
			$sql .= ' '.$this->createWhereClause();
			$sql .= ' '.$this->createGroupByClause();
			$sql .= ' '.$this->createOrderByClause();
			$sql .= ' '.$this->createLimitClause();
			return $sql;
		}
		
		/**
		 * Returns the WHERE clause created.
		 * 
		 * @return string the WHERE clause created.
		 */
		function createWhereClause(){
			return (0 < count($this->mCriteria2)) ? " WHERE ".implode($this->mCriteria2, " AND ") : "";
		}
		
		/**
		 * Merges two criterias
		 */
		function mergeCriteria2(){
			
		}
		
		function mustBeEqual($field,$value){
			$this->_CRITERIA[] = $field .' = '. mysql_escape_string($value);	
		}
		
		function mustBeEqualInString($field,$value){
			$this->_CRITERIA[] = $field .' = "'. mysql_escape_string($value).'"';	
		}
		
		function mustNotBeEqual($field,$value){
			$this->_CRITERIA[] = $field .'<> '. mysql_escape_string($value);	
		}
		
		function mustBeGreaterThan($field,$value){
			$this->_CRITERIA[] = $field .'> '. mysql_escape_string($value);	
		}
		
		function mustBeLessThan($field,$value){
			$this->_CRITERIA[] = $field .'< '. mysql_escape_string($value);	
		}
		
		function mustBeLike($field,$value){
			$this->_CRITERIA[] = $field .' LIKE \'%'. $value .'%\'';	
		}
		
		function setGroupBy($groupby){
			$this->_GROUPBY = ' GROUP BY '.$groupby;
		}
		
		function setOrderBy($orderby){
			$this->_ORDERBY = ' ORDER BY '.$orderby;
		}
		
		function setLimit($offset,$rows=NULL){
			$this->_LIMIT =  ' LIMIT '.(strlen($rows)? $offset.','.$rows : $offset);
		}
		
		function addTable($param){
			$this->_TABLES[] = $param;
		}
		
		function addField($param){
			$this->_FIELDS[] = $param;
		}
		
		function getTables(){
			if (count($this->_TABLES))
				return ','.implode(' , ',$this->_TABLES);
			else
				return NULL;
		}
		
		function getFields(){
			if (count($this->_FIELDS))
				return ','.implode(' , ',$this->_FIELDS);
			else
				return NULL;
		}
		
		function getCriteria2Only($ext=" AND "){
			return (0 < count($this->_CRITERIA)) ? $ext.' '.implode(' AND ',$this->_CRITERIA) : "";
		}
		
		function getCriteria2($ext=NULL){
			if (count($this->_CRITERIA))
				return $ext.' '.implode(' AND ',$this->_CRITERIA).' '.$this->_GROUPBY.' '.$this->_ORDERBY.' '.$this->_LIMIT;
			else
				return ' '.$this->_GROUPBY.' '.$this->_ORDERBY.' '.$this->_LIMIT;
		}
		
		/**
		 * added by K. Gordo (05/07/09) for use as part of the key in caching
		 * @return unknown_type
		 */
		public function getHash(){
			return hash('md5','Criteria2' . $this->getCriteria2() );		
		 }
		
	}