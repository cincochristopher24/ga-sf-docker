<?php
	/**
	 * @author: Reynaldo Castellano III
	 * @version: 2.2
	 * Note: 
	 * 		This class is an attemp to mimic the conventional recordset class offered in high level languages.
	 * Such recordsets offer methods like moveFirst, moveLast, moveNext, movePrev. Also, this utility class 
	 * prevent applications from being tightly coupled to database implementations.	
	 */
	class iRecordset implements ArrayAccess, Iterator{
		private $data = null;
		private $ptr = 0;
		private $recordCount = 0;
		private $arFields = array();
		private $currentRow = null;
		private $isBOF = true;
		private $isEOF = true;
		private $cols = array();
		
		/**
		 * Constructor function.
		 * @access public
		 * @param resource $queryData : returned from a call to database query function.
		 */
		public function __construct($queryData=null){
			if(null !== $queryData){
				if(is_resource($queryData) && 'mysql result' == get_resource_type($queryData)){
					$this->data = $queryData;
					$this->recordCount = mysql_num_rows($this->data);
					$numFields = mysql_num_fields($this->data);
					for($i=0;$i<$numFields;$i++){
						$this->arFields[] = mysql_field_name($this->data, $i);
					}
					if($this->recordCount > 0){
						$this->_seekData(0);
					}
				}
				else{
					//throw new exception('Invalid query data passed to object of type iRecordset.');
					$this->throwException('Invalid query data passed to object of type iRecordset.');
				}
			}
		}
		
		private function throwException($msg,$trace=null){
			if(is_null($trace)){
				$backtrace = debug_backtrace();
				$trace = $backtrace[1];
			}
			throw new exception($msg.' The error occurred in file: <<< '.$trace['file'].'('.$trace['line'].') >>>');
		}
		
		private function _seekData($pos){
			$this->ptr = $pos;
			if($this->ptr < 0 || $this->ptr >= $this->recordCount){				
				$this->isBOF = $this->ptr < 0;
				$this->isEOF = $this->ptr >= $this->recordCount;
				$this->currentRow = null;
				return false;	
			}
			$this->isBOF = false;
			$this->isEOF = false;
			mysql_data_seek($this->data,$pos);
			$this->currentRow = mysql_fetch_assoc($this->data);
			return mysql_data_seek($this->data,$pos);
		}
		
		private function _initCols(){
			if(count($this->cols) > 0) return false;			
			$tempPtr = $this->ptr;
			$this->_seekData(0);
			//initialize the fields as arrays
			$arFlds = $this->arFields;
			$cols = array();
			foreach($arFlds as $fld){
				$cols[$fld] = array();
			}
			while ($row = mysql_fetch_assoc($this->data)) {
			   	//loop through the fields
			 	foreach($arFlds as $fld){
					$cols[$fld][] = $row[$fld];
				}
			}
			$this->cols = $cols;
			$this->_seekData($tempPtr);
			return true;
		}
		/*** 
		 * 	ArrayAccess Implementation 
		 ***/
		public function offsetExists($offset){
           return $offset <= 0 && $offset >= $this->recordCount-1;
        }
 
        public function offsetGet($offset){        	
        	if($this->_seekData($offset)){        	
	        	return $this->currentRow;
        	}
        	else{
				//throw new exception("Offset [$offset] out of bound in object of type iRecordset.");
				$this->throwException("Offset [$offset] out of bound in object of type iRecordset.");
        	}
        }
 
        public function offsetSet($offset,$value){
            //throw new Exception("Cannot perform set operation on object of type iRecordset. This object is read only.");
			$this->throwException('Cannot perform set operation on object of type iRecordset. This object is read only.');
        }
 
        public function offsetUnset($offset){
			//throw new Exception("Cannot perform unset operation on object of type iRecordset. This object is read only.");
			$this->throwException('Cannot perform unset operation on object of type iRecordset. This object is read only.');
        }
		
		/*** End of ArrayAccess Implementation ***/
		
		/*** 
		 * Iterator Implementation 
		 ***/
		function current(){
        	return $this->currentRow;	        	
        }
 
        function key(){
            return $this->ptr;
        }
 
        function next(){
            return $this->moveNext();
        }
 
        public function rewind(){
			if($this->BOF() && $this->EOF()){
				return false;
			}
			return $this->moveFirst();
			/**
			if($this->valid()){
            	return $this->moveFirst();	
            }
			return false;
			**/
        }
 
        function valid(){                        
            return ( !$this->BOF() && !$this->EOF() );
        }
		
		/* End of Iterator Implementation */
		
		public function moveFirst(){			
			if($this->BOF() && $this->EOF()){
				//throw new exception('Cursor position is out of bound.');
				$this->throwException('Cursor position is out of bound.');
			}
			$this->_seekData(0);
		}
		
		public function moveLast(){			
			if($this->BOF() && $this->EOF()){
				//throw new exception('Cursor position is out of bound.');	
				$this->throwException('Cursor position is out of bound.');
			}
			$this->_seekData($this->recordCount-1);			
		}
		
		public function moveNext(){						
			if($this->EOF()){
				//throw new exception('Cursor position is out of bound.');
				$this->throwException('Cursor position is out of bound.');
			} 			
			return $this->_seekData($this->ptr+1);								
		}
		
		public function movePrev(){			
			if($this->BOF()){
				//throw new exception('Cursor position is out of bound.');
				$this->throwException('Cursor position is out of bound.');
			}
			return $this->_seekData($this->ptr-1);
		}
		
		public function moveTo($pos){
			if(!$this->_seekData($pos)){
				//throw new exception('Cursor position is out of bound.');
				$this->throwException('Cursor position is out of bound.');
			}
			return true;			
		}
		
		public function EOF(){
			return $this->isEOF;
		}
		
		public function BOF(){
			return $this->isBOF;
		}

		public function retrieveRecordCount(){
			return $this->recordCount;
		}
		
		public function retrieveFields(){
			return $this->arFields;
		}
		
		public function retrieveNumberOfFields(){
			return count($this->arFields);
		}
		
		public function retrieveColumn($fld=null){
			if(!in_array($fld,$this->arFields)){
				//throw new exception("Invalid fieldName[$fld] in method retrieveColumn of object of type iRecordset.");
				$this->throwException("Invalid fieldName[$fld] in method retrieveColumn of object of type iRecordset.");
			}
			$this->_initCols();
			$cols = $this->cols;
			return $cols[$fld];
		}
		
		public function retrieveRow($idx=null){			
			if( !is_null($idx) ){
				if(!is_numeric($idx)){
					//throw new exception('Row index must be of type numeric.');
					$this->throwException('Row index must be of type numeric.');
				}
				else{
					if(($idx >= $this->recordCount || $idx < 0)){
						//throw new exception('Error! Trying to retrieve a row that is out of bound.');
						$this->throwException('Error! Trying to retrieve a row that is out of bound.');
					}
					else{
						$tempPtr = $this->ptr;
						$this->_seekData($idx);
						$retData = mysql_fetch_assoc($this->data);
						$this->_seekData($tempPtr);
						return $retData;
					}
				}
			}
			else{
				return $this->currentRow;
			}
		}
		
		public function retrieveData(){
			return $this->data;
		}
		
		public function freeResult(){
			if(is_resource($this->data) && 'mysql result' == get_resource_type($this->data)){
				mysql_free_result($this->data);
			}
		}
		
		function __call($method,$arguments) {
	        $prefix = substr($method, 0, 3);
	        $fldName = substr($method, 3);
	        $iFieldName = $this->_getFieldName($fldName);
			$exception = null;
			if($prefix == 'get' && null != $iFieldName){
	        	if(count($arguments)){
		        	if(is_numeric($arguments[0])){
		        		if( $arguments[0] >= 0 && $arguments[0] < $this->recordCount ){
		        			return mysql_result($this->data,$arguments[0],$iFieldName);
		        		}
		        		else{
							$exception = 'Row index '.$arguments[0].' is out of bound.';
		        		}
		        	}
		        	else{
						$exception = 'Row index '.$arguments[0].' must be of type numeric';
		        	}
	        	}
	        	else{
	 				if(!$this->BOF() && !$this->EOF()){
						return $this->currentRow[$iFieldName];
	 				}
	 				$exception = 'Cursor position is out of bound.';
	        	}
	        }
	        else{		        
				$exception = 'Undefined method call: '.$method;
	        }
			if(!is_null($exception)){
				$backtrace = debug_backtrace();
				$this->throwException($exception,$backtrace[1]);
			}
	    }
	    
	    private function _getFieldName($fldName){	    	
	    	foreach($this->arFields as $fld){	    		
	    		if(strtolower($fldName) == strtolower($fld)) return $fld;
	    	}	    	
	    	return null;
	    }
	    
	    function __toString(){
	    	return $this->constructStringToDump();
	    }
	    
		private function constructStringToDump(){
			try{				
				$dumpStr = "<table border=\"1\"cellpadding=\"5\">\n";
				//set the header of the table
				$tempNumFields = count($this->arFields) + 1;
				$dumpStr.= "<tr><td colspan=\"$tempNumFields\" bgcolor=\"DDDDD\">Recordcount: $this->recordCount</td></tr>";				
				$dumpStr.= "<tr>\n";			
				$dumpStr.= "<th  bgcolor=\"DDDDD\">#</th>";
				$arFlds = $this->arFields;				
				foreach($arFlds as $fld){
		        	$dumpStr.= "<th bgcolor=\"CCCCCC\">";
		           	$dumpStr.= $fld;
		           	$dumpStr.= "</th>\n";
		      	}
		      	$dumpStr.= "</tr>\n";
		      	if($this->recordCount > 0){					
					$tempPtr = $this->ptr;
					$this->_seekData(0);
					$loopCtr = 0;
					$data = $this->data;
					while ($row = mysql_fetch_array($data)){
					   $loopCtr++;
					   $bgColor = ($loopCtr % 2)?'#E0E0E0':'#ECECEC';
					   $dumpStr.= "<tr>";
					   //loop through the fieldlist
					   $dumpStr.= "<td bgcolor=\"ddddd\">$loopCtr</td>";
					   foreach($arFlds as $fld){
				        	$dumpStr.= "<td bgcolor=\"$bgColor\">";
				           	$dumpStr.= $row[$fld];
				           	$dumpStr.= "</td>\n";
				      	}
					   $dumpStr.= "</tr>";
					}
					$this->_seekData($tempPtr);
				}
				$dumpStr.= "</table>";
				return $dumpStr;
			}
			catch(exception $e){
				throw new exception($e);
			}
		}	    
	    
	    public function dump(){ 
			echo $this->constructStringToDump();
		}

		public function dumpXml(){
			try{
				//construct the data string for each field				
				require_once("Class.XmlEscaper.php");
				header("Content-type:text/xml");
				echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
				$fieldData = array();
				$fieldNum = count($this->arFields);
				for ($i=0; $i<$fieldNum; $i++) {
			       	$fieldData[$i] = "";
			    }
				if($this->recordCount > 0){
					$tempPtr = $this->ptr;
					$this->_seekData(0);
					$data = $this->data;
					while ($row = mysql_fetch_array($data)){
					   for ($i=0; $i<$fieldNum; $i++) {											
				        	$fieldData[$i] .= "<data>".xmlEscaper::escapeXml($row[mysql_field_name($data, $i)])."</data>";
				      	}
					}
					$this->_seekData($tempPtr);
				}
				echo "<IRecordset fields=\"".xmlEscaper::escapeXml(implode($this->arFields,","))."\"  recordcount = \"". $this->recordCount ."\">";
					//loop through the fields
					for ($i=0; $i < $fieldNum; $i++) {
						echo "<field name=\"".xmlEscaper::escapeXml(mysql_field_name($data, $i))."\" type= \"".mysql_field_type($data, $i)."\">";
						echo $fieldData[$i];
						echo "</field>"; 
					}
				echo "</IRecordset>";
			}
			catch(exception $e){
				throw new exception($e); 
			}
		}	    
	}
?>