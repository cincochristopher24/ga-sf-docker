<?php

interface ganetICache {
	function set($key,$value,$flags = array());
	function add($key,$value,$flags = array());
	function get($key);
	function delete($key,$flags = array());
	function flush();
	function getStatus(); // online/offline status
	
}


?>