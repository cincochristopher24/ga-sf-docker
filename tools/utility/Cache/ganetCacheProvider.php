<?php

class ganetCacheProvider {
	protected static $instance = null;
	protected $cache = null;
	
	public static function instance(){
		if (self::$instance == null){
			self::$instance = new ganetCacheProvider();
		}
		return self::$instance;
	}
	
	protected function ganetCacheProvider(){
		if (class_exists('Memcache')) {
			require_once('Cache/ganetMemcache.php');
			try {
				$this->cache = new ganetMemcache();
			} catch (Exception $e) {
				$this->cache = null;
			}	
		}
	}
	
	
	function getCache() {
		return $this->cache;
	}
	
}

?>