<?php
require_once('Class.DataModel.php');

class GroupJournals {
	
	private $group_id = 0;
	private static $instance = null;
	
	protected $conn = null;
	protected $csv_list = array();
	
	public static function getInstance($group_id){
		if( is_null(self::$instance) ){
			self::$instance = new self($group_id);
		}
		return self::$instance;
	}
	
	private function __construct($group_id){
		$this->group_id = $group_id;
		$this->conn = new dbHandler('db=Travel_Logs');
	}
	
	public function executeBackup(){
		$sql_person = "SELECT 
							person.travelerID, 
							person.firstname, 
							person.lastname
						FROM tblTraveler AS person
						INNER JOIN tblGrouptoTraveler AS group_traveler
							ON group_traveler.travelerID = person.travelerID
						WHERE 1
							AND group_traveler.groupID = $this->group_id
						";
		
		$rs_person = new iRecordset($this->conn->execQuery($sql_person));
		
	//	var_dump($rs_person);
		$result = array();
		
		$csv_field_keys = array("Firstname", "Lastname", "Journal Title", "Journal Description", "Journal Entry Title", "Journal Entry Description", "Featured");
		
		$this->csv_list = array(
					array_values($csv_field_keys)
				);
		
		$csv_list_value = array();
		while(!$rs_person->EOF()){
		//	echo "<br/>".$rs_person->getFirstname();
			$journals = array();
			$entries = array();
			
			$csv_list_value = array($rs_person->getFirstname(),
									$rs_person->getLastname()
									);
			
			
			$travelerID = $rs_person->getTravelerID();
			$sql_journals = "SELECT 
								journal.travelID,
								journal.title,
								journal.description,
								journal.lastupdated
							FROM tblTravel AS journal
							INNER JOIN tblGroupApprovedJournals AS approvedJournals
								ON approvedJournals.travelID = journal.travelID
							WHERE 1
								AND journal.travelerID = $travelerID
								AND approvedJournals.groupID = $this->group_id
								AND journal.publish = 1
								AND journal.deleted = 0
								AND journal.entryCount > 0
							";
		//	echo $sql_journals;
			$rs_journals = new iRecordset($this->conn->execQuery($sql_journals));
			$journal_cnt = 1;
			while(!$rs_journals->EOF()){
			//	echo "<br/> - ".$rs_journals->getTitle();
				$travelID = $rs_journals->getTravelID();
				$sql_entries = "SELECT
									entry.travellogID,
									entry.title,
									entry.description,
									entry.callout,
									entry.logdate,
									entry.lastedited,
									entry.isfeatured
								FROM tblTravelLog as entry
								WHERE 1
									AND entry.travelID = $travelID
									AND entry.publish = 1
									AND entry.deleted = 0
								";
				$rs_entries = new iRecordset($this->conn->execQuery($sql_entries));
				while(!$rs_entries->EOF()){
				//	echo "<br/> --- ".$rs_entries->getTitle();
				//	echo "<br/> ----- ".substr($rs_entries->getDescription(), 0, 50);
					$entries[] = array("ID" => $rs_entries->getTravellogID(),
										"title" => $rs_entries->getTitle(),
										"description" => $rs_entries->getDescription(),
										"callout" => $rs_entries->getCallout(),
										"log_date" => $rs_entries->getLogdate(),
										"last_edited" => $rs_entries->getLastedited(),
										"is_featured" => $rs_entries->getIsfeatured()
										);
				
					$rs_entries->moveNext();
				}
				
				if($journal_cnt == 1){
					array_push($csv_list_value, $rs_journals->getTitle(), $rs_journals->getDescription());
					
					$this->setJournalEntriesList($entries, $csv_list_value);
				}
				else {
					$new_csv_list_value = array("--", "--", $rs_journals->getTitle(), $rs_journals->getDescription());
					$this->setJournalEntriesList($entries, $new_csv_list_value);
					
				}
				$journal_cnt++;
				
				// $journals[] = array("ID" => $rs_journals->getTravelID(),
				// 					"title" => $rs_journals->getTitle(),
				// 					"description" => $rs_journals->getDescription(),
				// 					"last_updated" => $rs_journals->getLastupdated(),
				// 					"entries" => $entries
				// 					);
				
				
				
				$rs_journals->moveNext();
			}
			
			// $person = array("ID" => $rs_person->getTravelerID(),
			// 					"firstname" => $rs_person->getFirstname(),
			// 					"lastname" => $rs_person->getLastname(),
			// 					"journals" => $journals
			// 					);
			// 
			// $result[] = $person;
			
			
			$rs_person->moveNext();
		}
		
		
		// echo "<pre>";
		//var_dump($result);
		
		// $csv_list_value = array();
		// 
		// foreach($result as $person){
		// 	$csv_list_value = array($person['firstname'],
		// 							$person['lastname']
		// 							);
		// 				
		// 	$journals = $person['journals'];
		// 	$journal_cnt = 1;
		// 	foreach($journals as $journal){
		// 		if($journal_cnt == 1){
		// 			array_push($csv_list_value, $journal['title'], $journal['description']);
		// 			
		// 			$this->setJournalEntriesList($journal['entries'], $csv_list_value);
		// 		}
		// 		else {
		// 			$new_csv_list_value = array("--", "--", $journal['title'], $journal['description']);
		// 			$this->setJournalEntriesList($journal['entries'], $new_csv_list_value);
		// 			
		// 		}
		// 		$journal_cnt++;
		// 	}
		// 	
		// 	
		// 							
		// 	
		// }
		
		
		
	//	var_dump($csv_list);
		
		
		
		// $field_keys = array("firstname", "lastname");
		// $field_val_1 = array("nash", "lesigon");
		// $field_val_2 = array("test", "xxxx");
		// $list = array(
		// 	array_values($field_keys),
		// 	array_values($field_val_1),
		// 	array_values($field_val_2)
		// );
		
		$date = date('Y-m-d');
		$csvFile = 'backup_'.$this->group_id.'_'.$date.'.csv';
		//$csvName  = dirname(__FILE__).'/../../../goabroad.net/scripts/group_journal_backup/files/'.$csvFile;
		$csvName  = dirname(__FILE__).'/../../../goabroad.net/custom/LIVINGROUTES/backup/'.$csvFile;

		$csvFileHandle = fopen($csvName, 'w') or die("cannot open file : $csvName");
		foreach ($this->csv_list as $line) {
		    fputcsv($csvFileHandle, $line);
		}
		
		fclose($csvFileHandle);
		
		
		
		
		
	}
	
	private function setJournalEntriesList($entries, $csv_list_value){
		$entry_count = 1;
		foreach($entries as $entry){
			if($entry_count == 1){
				array_push($csv_list_value, $entry['title'], $entry['description'], $entry['is_featured']);
				array_push($this->csv_list, array_values($csv_list_value));
			}
			else {
				$new_csv_list_entry_value = array("--", "--", "--", "--", $entry['title'], $entry['description'], $entry['is_featured']);
				array_push($this->csv_list, array_values($new_csv_list_entry_value));
			}
			$entry_count++;
		}
	}
}