<?php
/*
 * Created on 04 8, 09
 * 
 * Author:
 * Purpose: a singleton class to provide connection for database access using Class.Connection.php
 * 			Another purpose is not to ember a connection object in the models cause when its cached and 
 * 			retrieved from cache the connection resource is no longer valid 
 * 
 */
 
 require_once('Class.Connection.php');
 require_once('Class.Recordset.php');
 
 class ConnectionProvider {
 	protected static $instance = null;
 	protected $conn = null;
 	protected $rs	= null;
 	protected $rs2	= null;
 	protected $rs3	= null;
 	protected $rs4	= null;
 	
 	
 	public static function instance(){
 		if (self::$instance == null){
 			self::$instance	=	new ConnectionProvider(); 
 		}
 		return self::$instance;
 	}
 	
 	protected function ConnectionProvider(){
 		$this->conn	=	new Connection();
 		$this->rs	=	new Recordset($this->conn);
 		$this->rs2	=	new Recordset($this->conn);
 		$this->rs3	=	new Recordset($this->conn);
 		$this->rs4	=	new Recordset($this->conn);
 	}
 	
 	function getConnection(){
 		return $this->conn;
 	}
 	
 	function getRecordset(){
 		return $this->rs;
 	}
 	
 	function getRecordset2(){
 		return $this->rs2;
 	}
 	
 	function getRecordset3(){
 		return $this->rs3;
 	}
 	
 	function getRecordset4(){
 		return $this->rs4;
 	}
 	
 	
 
 }
 
 
 
?>
