<?php
	class ErrorLoggerConfig
	{
		static private $instance = null;
		static public function getInstance(){
			if(is_null(self::$instance)){
				self::$instance = new ErrorLoggerConfig();
			}
			return self::$instance;
		}
		
		private $mConfigVars = array(
			'logging_disabled'			=> 1,
			'error_mail_disabled' 		=> 1,
			'excluded_domains'			=> array(),
			'mail_recipients'			=> array(),
			'is_real_time'				=> 1,
			'display_to_browser'		=> 1
		);
		
		private function __construct(){
			//load the config file
			$configFile = '/var/www/websites/configFiles/ErrorLogger.ini';
			if(file_exists($configFile)){				
				$iniVals = parse_ini_file($configFile,true);
				foreach($iniVals as $key => $val){
					if(array_key_exists($key, $this->mConfigVars)){
						$this->mConfigVars[$key] = $iniVals[$key];
					}
				}
			}			
		}
		
		public function isLoggingDisabled(){
			return $this->mConfigVars['logging_disabled'] == 1;
		}
		
		public function isErrorMailDisbaled(){
			return $this->mConfigVars['error_mail_disabled'] == 1;
		}
		
		public function getExcludedDomains(){
			return $this->mConfigVars['excluded_domains'];
		}
		
		public function getMailRecipients(){
			return $this->mConfigVars['mail_recipients'];
		}
		
		public function isRealTime(){
			return $this->mConfigVars['is_real_time'] == 1;
		}
		
		public function isDisplayableToBrowser(){
			return $this->mConfigVars['display_to_browser'] == 1;
		}

	}
?>