<?php
	require_once('Class.DataModel.php');
	require_once('Class.ErrorLoggerConfig.php');
	require_once('class.phpmailer.php');
	require_once('Class.GaDateTime.php');
	require_once('errorSender/Class.ErrorSender.php');
//	require_once('errorSender/Class.GaNetErrorSender.php');
//	require_once('errorSender/Class.DotComErrorSender.php');
//	require_once('errorSender/Class.AffiliatesErrorSender.php');
	
	class GaErrorLog extends DataModel
	{
		private $mErrLoggerConfig = null;
		
		public function __construct($logID=0){
			parent::initialize('Logs','tblErrors2');
			parent::load($logID);
			if(is_null($this->getID())){
				$this->setDateAdded(GaDateTime::dbDateTimeFormat());
			}
			$this->mErrorLoggerConfig = ErrorLoggerConfig::getInstance();
		}
		
		private function transformToString($var){
			ob_start();
			print_r($var);
			$str = ob_get_contents();
			ob_end_clean();
			return $str;			
			//return var_export($var, true);
		}
		
		public function save(){
			GaLogger::create()->beginNoTrackingSection();
			try{
				parent::save();
			}
			catch(exception $e){
				GaLogger::create()->endNoTrackingSection();
				throw $e;
			}
			GaLogger::create()->endNoTrackingSection();
		}
		
		public function setVars($vars){
			/***
			$newVars = array_slice($vars,0);
			if(array_key_exists('PHP_AUTH_PW',$newVars['_SERVER'])){
				$newVars['_SERVER']['PHP_AUTH_PW'] = '***';
			}
			***/
			if(isset($vars['SERVER']) && isset($vars['SERVER']['PHP_AUTH_PW'])){
				$vars['SERVER']['PHP_AUTH_PW'] = '***';
			}
			parent::executeMethod('setVars',array($this->transformToString($vars)));
		}
		
		public function setStackTrace($trace){
			//disable storing of backtrace. -- Naldz (Sept. 13, 2010)
			//parent::executeMethod('setStackTrace',array($this->transformToString($trace)));			
		}
		
		public function send($addresses=array()){
			//TO DO: find a way to store errors when the database is down. Or how to log errors that are caused when database is down.
			if($this->getID() && count($addresses)){
				$mail = new PHPMailer(); 
				$mail->IsSMTP();
				$mail->IsHTML(true);
			
				$mail->FromName = "GoAbroad Network Error";
				$mail->From = "error@goabroad.net";
		 
				$mail->Subject = "GoAbroad Network Error";
				$mail->Body = $this->getAsMessage();
				
				foreach($addresses as $iAddr){
					$mail->AddAddress($iAddr);
				}
				$mail->Send();
				$this->setSent(1);
				$this->save();
			}
			else{
				throw new exception('Cannot send a non saved error or the recipient array must not be empty.');
			}
		}
		
		public function getAsMessage($bulk=true){
		//	echo $this->trimVarsToGetSession($this->getVars()); exit;
			$str = "
				ID : {$this->getID()}<br />
				TYPE: {$this->getType()}<br />
				MESSAGE : {$this->getMessage()}<br />
				FILE : {$this->getFile()}<br />
				LINE : {$this->getLine()}<br />
				DATE : {$this->getDateAdded()}<br />
				SERVER_NAME: {$this->trimVarsToGetHost($this->getVars())}<br />
				SESSION: <pre>{$this->trimVarsToGetSession($this->getVars())}</pre><br />
				 
				";
				
			
			if($bulk){	
				$str .="VARIABLES: <br />
						<pre>{$this->getVars()}</pre><br />
						STACKTRACE : <br />
						<pre>{$this->getStackTrace()}</pre><br />";
			}
			/***
			else{
				if($this->getID() > 45250){//this is to ensure we dont operate of previous error logs since we would be getting a parse error.
					eval('$priVars='.$this->getVars().';');
					if(isset($priVars['SERVER'])){
						if(isset($priVars['SERVER']['HTTP_HOST']) ){
							$str .= "HTTP_HOST: {$priVars['SERVER']['HTTP_HOST']}<br />";
						}
						if(isset($priVars['SERVER']['QUERY_STRING']) ){
							$str .= "QUERY_STRING: {$priVars['SERVER']['QUERY_STRING']}<br />";
						}
					}
					//ckeck the travelerID
					if(isset($priVars['SESSION'])){
						$sessionVars = $this->transformToString($priVars['SESSION']);
						$str .= "SESSION: <br /><pre>$sessionVars</pre>";
					}
				}
			}
			***/
			return $str;
		}
		
		static public function sendAllUnsentErrors(){
			$db = new dbHandler('db=Logs');
			$rs = new iRecordset($db->execute("
				SELECT * 
				FROM tblErrors2
				WHERE `sent` = 0
				LIMIT 900
			")); // original query - DO NOT ERASE -nash
		
		/*	// - test query
			$rs = new iRecordset($db->execute("
				SELECT * 
				FROM tblErrors2
				WHERE `sent` = 0
				ORDER BY ID DESC
				LIMIT 10
			"));
			*/
		/*	// - test query
			$rs = new iRecordset($db->execute("
				SELECT * 
				FROM tblErrors2
				ORDER BY ID DESC
				LIMIT 100
			"));
		*/ 
			$message = '';
			$errorIDs = array();
			
			$errorSender = new ErrorSender();
			
			foreach($rs as $row){
				$errorIDs[] = $row['ID'];
			//	echo "<br/>".$row['ID'];
				$errorLog = new self($row['ID']);
				
				$errorSender->filter($errorLog);
				
			}
			
			$errorSender->send();
			
			if(count($errorIDs)){
				$errorIDs_chunk = array_chunk($errorIDs, 300);
				foreach($errorIDs_chunk as $chunk_IDs){
					$ids = implode(',',$chunk_IDs);
				/*	$sql = "UPDATE tblErrors2
							SET sent = 1
							WHERE ID IN ($ids)";
					echo "<br/> update table sql : ".$sql;*/
					$db->execute("
						UPDATE tblErrors2
						SET sent = 1
						WHERE ID IN ($ids)
					");
				}
				
			}
		}
		
		// helper functions added by nash
		public function trimVarsToGetSession($vars){
			$subject = $vars;
			
			$subject = $this->getStringByStringPosition($subject, '[SESSION]', '[GET]');
			return $subject;
		}
		
		public function trimVarsToGetHost($vars){
			$subject = $vars;
			
			$subject = $this->getStringByStringPosition($subject, '[SERVER]', '[SESSION]');
			
		//	$pattern = '/\bHTTP_HOST\b/i';
			$start = 15;
			$pattern = '/\bSERVER_NAME\b/i';
			$pos = strpos($subject, $pattern);
			
			preg_match($pattern, $subject, $matches, PREG_OFFSET_CAPTURE);
			if(!count($matches)){
				$pattern = '/\bHOSTNAME\b/i';
				preg_match($pattern, $subject, $matches, PREG_OFFSET_CAPTURE);
				$start = 12;
			}
			
			if(!count($matches)){
				return "";
			}
			
			$num1 = $matches[0][1];
			$mystring = substr($subject, $num1);
			$findme = '[';
			$pos = strpos($mystring, $findme);
			return substr($mystring, $start, ($pos-$start));
		}
		
		public function getStringByStringPosition($string, $pattern1, $pattern2){
			
			$pos1 = strpos($string, $pattern1);
			$pos2 = strpos($string, $pattern2);
			
			$a = ($pos2 - $pos1);
			
			return substr($string, $pos1, $a);
		}
	}
?>