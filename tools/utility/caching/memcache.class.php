<?php

/**
 * class name: memcache.class.php 
 * @author Joel C. Llano
 * @access public
 * Date Created: July 16 2007
 * singleton implementation of memcache
 */
 
class Cache {

	protected $mc_servers = array();
	protected $mc_servers_count;
	static $instance;
	
	static function singleton(){
	    
	    switch($_SERVER['HTTP_HOST']){
				case "dev.goabroad.net":
					$servers = 	array(
									array('192.168.3.10'=>'11211'),
	        			 			array('192.168.3.11'=>'11211'),
	    					 		array('192.168.3.3'=>'11211'),
	    				 			array('192.168.3.2'=>'11211')
	    				 		);
	   			break; 
				
				case "aifs-dev.goabroad.net":
					$servers = 	array(
									array('192.168.3.10'=>'11211'),
	        			 			array('192.168.3.11'=>'11211'),
	    					 		array('192.168.3.3'=>'11211'),
	    				 			array('192.168.3.2'=>'11211')
	    				 		);
	   			break;
	   			
				case "www.goabroad.net":
					$servers = 	array(
									array('192.168.3.10'=>'11211'),
	        			 			array('192.168.3.11'=>'11211'),
	    				 			array('192.168.3.3'=>'11211'),
	    				 			array('192.168.3.2'=>'11211')
	    				 		);
				break;
				case "goabroad.net.local":
					$servers = 	array(
									array('192.168.6.162'=>'11211')
								);
				break;
				case "my.aifs.org.local":
					$servers = 	array(
									array('192.168.6.162'=>'11211')
								);
				break;
				default:
					$servers = 	array(
									array('192.168.3.10'=>'11211'),
	        			 			array('192.168.3.11'=>'11211'),
	    					 		array('192.168.3.3'=>'11211'),
	    				 			array('192.168.3.2'=>'11211')
	    				 		);
				break;
				
				
				}
	    			
	    
	    
	    self::$instance || self::$instance = new Cache($servers);
	    return self::$instance;
	}
	
	/**
	 * Accepts the 2-d array with details of memcached servers
	 *
	 * @param array $servers
	 */
	protected function __construct(array $servers){
	    if (!$servers){
	        trigger_error('No memcache servers to connect',E_USER_WARNING);
	    }
	    
	    /**
	     * check temporary if memcache library exist.
	     */
	    if(function_exists('memcache_pconnect')){
	    	for ($i = 0, $n = count($servers); $i < $n; ++$i){
	        	( $con = memcache_pconnect(key($servers[$i]), current($servers[$i])) )&& $this->mc_servers[] = $con;
	    	}
	    }
	    
	    $this->mc_servers_count = count($this->mc_servers);
	    if (!$this->mc_servers_count){
	        $this->mc_servers[0]=null;
	    }
	}
	/**
	 * Returns the resource for the memcache connection
	 *
	 * @param string $key
	 * @return object memcache
	 */
	protected function getMemcacheLink($key){
	    if ( $this->mc_servers_count <2 ){
	        //no servers choice
	        return $this->mc_servers[0];
	    }
	    return $this->mc_servers[(crc32($key) & 0x7fffffff)%$this->mc_servers_count];
	}
	
	/**
	 * Clear the cache
	 *
	 * @return void
	 */
	static function flush() {
	    $x = self::singleton()->mc_servers_count;
	    
	    /**
	     * check temporary if memcache library exist.
	     */
	    if(function_exists('memcache_pconnect')){
		    for ($i = 0; $i < $x; ++$i){
		        $a = self::singleton()->mc_servers[$i];
		        self::singleton()->mc_servers[$i]->flush();
		    }
	    }
	}
	
	/**
	 * Returns the value stored in the memory by it's key
	 *
	 * @param string $key
	 * @return mix
	 */
	static function get($key) {
		/**
	     * check temporary if memcache library exist.
	     */
	    if(function_exists('memcache_pconnect')){
			return self::singleton()->getMemcacheLink($key)->get($key);
	    }else{
	    	return false;
	    }
	}
	
	/**
	 * Store the value in the memcache memory (overwrite if key exists)
	 *
	 * @param string $key
	 * @param mix $var
	 * @param bool $compress
	 * @param int $expire (seconds before item expires)
	 * @return bool
	 */
	static function set($key, $var, $compress=0, $expire=0) {
	    /**
	     * check temporary if memcache library exist.
	     */
	    if(function_exists('memcache_pconnect')){
	    	return self::singleton()->getMemcacheLink($key)->set($key, $var, $compress?MEMCACHE_COMPRESSED:null, $expire);
	    }else{
	    	return false;
	    }
	}
	/**
	 * Set the value in memcache if the value does not exist; returns FALSE if value exists
	 *
	 * @param sting $key
	 * @param mix $var
	 * @param bool $compress
	 * @param int $expire
	 * @return bool
	 */
	static function add($key, $var, $compress=0, $expire=0) {
	    /**
	     * check temporary if memcache library exist.
	     */
	    if(function_exists('memcache_pconnect')){
	    	return self::singleton()->getMemcacheLink($key)->add($key, $var, $compress?MEMCACHE_COMPRESSED:null, $expire);
	    }else{
	    	return false;
	    }
	}
	
	/**
	 * Replace an existing value
	 *
	 * @param string $key
	 * @param mix $var
	 * @param bool $compress
	 * @param int $expire
	 * @return bool
	 */
	static function replace($key, $var, $compress=0, $expire=0) {
	    /**
	     * check temporary if memcache library exist.
	     */
	    if(function_exists('memcache_pconnect')){
	   		return self::singleton()->getMemcacheLink($key)->replace($key, $var, $compress?MEMCACHE_COMPRESSED:null, $expire);
	    }else{
	    	return false;
	    }
	}
	/**
	 * Delete a record or set a timeout
	 *
	 * @param string $key
	 * @param int $timeout
	 * @return bool
	 */
	static function delete($key, $timeout=0) {
	    /**
	     * check temporary if memcache library exist.
	     */
	    if(function_exists('memcache_pconnect')){
	    	return self::singleton()->getMemcacheLink($key)->delete($key, $timeout);
	    }else{
	    	return false;
	    }
	}
	/**
	 * Increment an existing integer value
	 *
	 * @param string $key
	 * @param mix $value
	 * @return bool
	 */
	static function increment($key, $value=1) {
	    /**
	     * check temporary if memcache library exist.
	     */
	    if(function_exists('memcache_pconnect')){
	    	return self::singleton()->getMemcacheLink($key)->increment($key, $value);
	    }else{
	    	return false;
	    }
	}
	
	/**
	 * Decrement an existing value
	 *
	 * @param string $key
	 * @param mix $value
	 * @return bool
	 */
	static function decrement($key, $value=1) {
	    /**
	     * check temporary if memcache library exist.
	     */
	    if(function_exists('memcache_pconnect')){
	    	return self::singleton()->getMemcacheLink($key)->decrement($key, $value);
	    }else{
	    	return false;
	    }
	}
	/*
	static function getStatus(){
		return self::singleton()->getMemcacheLink($key)->getExtendedStats();
	}
	*/
//class end
}

?>