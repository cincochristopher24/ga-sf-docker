<?php
if (!defined('PHPUnit_MAIN_METHOD')) {
    define('PHPUnit_MAIN_METHOD', 'AllUtilityTests::main');
}
 
require_once 'PHPUnit/Framework.php';
require_once 'PHPUnit/TextUI/TestRunner.php';
 

require_once 'RandomStringGeneratorTest.php';
require_once 'GAFileTest.php';
require_once 'GAFileResourceTest.php';
require_once 'GAFileResourceManagerTest.php';
require_once 'GATemplateTest.php';
require_once 'GAHtmlTemplateTest.php';
require_once 'GATemplateOutputTest.php';



class AllUtilityTests {
    public static function main()
    {
        PHPUnit_TextUI_TestRunner::run(self::suite());
    }
 
    public static function suite()
    {
        $suite = new PHPUnit_Framework_TestSuite('GA Utility Tests');
 
		$suite->addTestSuite('RandomStringGeneratorTest');
		$suite->addTestSuite('GAFileTest');
		$suite->addTestSuite('GAFileResourceTest');
		$suite->addTestSuite('GAFileResourceManagerTest');
		$suite->addTestSuite('GATemplateTest');
		$suite->addTestSuite('GAHtmlTemplateTest');
		$suite->addTestSuite('GATemplateOutputTest');
		        
 
        return $suite;
    }
}
 
if (PHPUnit_MAIN_METHOD == 'AllUtilityTests::main') {
    AllUtilityTests::main();
}

?>