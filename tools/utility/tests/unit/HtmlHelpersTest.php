<?php
/**
 * Created on Jun 21, 2007
 * 
 * @author     Wayne Duran
 */

require_once 'Class.HtmlHelpers.php';
require_once 'Class.RandomStringGenerator.php';

class HtmlHelpersTest extends PHPUnit_Framework_TestCase {
	
	protected function setUp() {
		$this->generator = RandomStringGenerator::instance();
		//$this->helper    = HtmlHelpers::instance();
	}
	
	function testTruncatingWordThatIsLessThanLengthOfLimit() {
		$teststr = $this->generator->getAlphaNumeric(10);
		$this->assertEquals($teststr, HtmlHelpers::truncateWord($teststr, 11), 'Unable to truncate text that is less than the limit in length.');
	}
	
	function testTruncatingWordThatIsGreaterThanLengthOfLimit() {
		$teststr = $this->generator->getAlphaNumeric(28);
		$result  = HtmlHelpers::truncateWord($teststr, 11);
		// Expected result
		$shouldbe = substr($teststr, 0, 10).'&#8230;';
		
		
		$this->assertEquals($shouldbe , $result, 'Text did not match expected result.');
		$this->assertTrue(strlen($result) < strlen($teststr), 'Result text should be less than the length of the original text');
	}
	
	function testTruncatingWordUsingCustomEllipsis() {
		$teststr = $this->generator->getAlphaNumeric(28);
		$result  = HtmlHelpers::truncateWord($teststr, 11, '...');
		// Expected result
		$shouldbe = substr($teststr, 0, 10).'...';
		
		$this->assertEquals($shouldbe , $result, 'Text did not match expected result.');
		$this->assertTrue(strlen($result) < strlen($teststr), 'Result text should be less than the length of the original text');
	}
	
	function testAddingPrefixAndSuffixToTextile() {
		$teststr = "I want to know you\nbetter.";
		$this->assertEquals("AAA<p>I want to know you<br />\nbetter.</p>BBB", HtmlHelpers::Textile($teststr, array('prefix'=>'AAA', 'suffix'=>'BBB')));
	}
	
	function testCreatingLists() {
		$result = HtmlHelpers::createList(array('1', 'B', '3', 'd'));
		$expected = '<ul><li class="first">1</li><li>B</li><li>3</li><li class="last">d</li></ul>';
		$this->assertEquals($expected, $result, 'Did not create list properly');
	}
	
	function testCreatingListWithIdArgumentReturnsAListWithTheId() {
		$result = HtmlHelpers::createList(array('A', '2', 'C', '4'), array('id'=>'navigation'));
		$expected = '<ul id="navigation"><li class="first">A</li><li>2</li><li>C</li><li class="last">4</li></ul>';
		$this->assertEquals($expected, $result, 'Did not create list properly');
	}
	
}

?>
