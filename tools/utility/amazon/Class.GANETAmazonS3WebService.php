<?php
	require_once("amazon/Class.S3.php");

	class GANETAmazonS3WebService{
		const SVN_STATUS_CODE_OFFSET	= '2';
		const SVN_STATUS_NEW_FILE 		= 'A ';
		const SVN_STATUS_MODIFIED_FILE 	= 'U ';
		const SVN_STATUS_DELETED_FILE 	= 'D ';
		
		const S3_REMOVE_FILE	= 	0;
		const S3_PUT_FILE		= 	1;
		
		const S3_BUCKET_NAME = 'ganet-assets';
		const S3_BUCKET_NAME_TEST = 'ganet-assets-test';
		
		const S3_DOMAIN_NAME = 'dfe1zrvl08k3v.cloudfront.net';
		const S3_DOMAIN_NAME_TEST = 'd2ytdade2c58qr.cloudfront.net';
		
		
		private $s3 = null;
		private $queueFiles = array();
		private $assetList = array(
			'css',
			'js' ,
			'images' ,
			'users',
		);
		private $s3Action = null;

		static private $instance = null;
		
		static public function getInstance(){
			if(is_null(self::$instance)){
				self::$instance = new self;
			}
			return self::$instance;
		}
		
		private function __construct() {
			// temporary setup - where to store these private data?
			$awsAccessKey = "AKIAIZK2SEW3P4SCWOZA";
			$awsSecretKey = "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";
			
			$this->s3 = new S3($awsAccessKey, $awsSecretKey);
		}
		
		public function setAction($action = null){
			$this->s3Action = $action;
		}
		
		public function getDomainName(){
			//CLI_ENVIRONMENT is defined in export-assets-to-cloudfront.php accessible via cli
			//or it should be defined in frontend controllers that are run via cli
			if( defined('CLI_ENVIRONMENT') ){
				if( "prod" == CLI_ENVIRONMENT ){
					return self::S3_DOMAIN_NAME;
				}
				return self::S3_DOMAIN_NAME_TEST;
			}
			if( FALSE === strpos($_SERVER["SERVER_NAME"],".local") ){
				return self::S3_DOMAIN_NAME;
			}
			return self::S3_DOMAIN_NAME_TEST;
		}
		
		public function getBucketName(){
			//CLI_ENVIRONMENT is defined in export-assets-to-cloudfront.php accessible via cli
			//or it should be defined in frontend controllers that are run via cli
			if( defined('CLI_ENVIRONMENT') ){
				if( "prod" == CLI_ENVIRONMENT ){
					return self::S3_BUCKET_NAME;
				}
				return self::S3_BUCKET_NAME_TEST;
			}
			if( FALSE === strpos($_SERVER["SERVER_NAME"],".local") ){
				return self::S3_BUCKET_NAME;
			}
			return self::S3_BUCKET_NAME_TEST;
		}
		
		public function pushRawFilestoS3($rawFiles, $subPath = null){
			
			$this->setAction(self::S3_PUT_FILE);
			$processedFiles = $this->processRawFiles($rawFiles, $subPath);
			$this->storeFiles($processedFiles);
			
		}
		
		public function removeRawFilesFromS3($rawFiles, $subPath = null){
			
			$this->setAction(self::S3_REMOVE_FILE);
			$processedFiles = $this->processRawFiles($rawFiles, $subPath);
			$this->storeFiles($processedFiles);
			
		}
		
		public function pushSvnUpdatedFilestoS3($svnLogFile){
			
			$processedFiles = $this->processSvnLogFile($svnLogFile);
			$this->storeFiles($processedFiles);
			
		}
		
		
		//public function that processes raw files - get the attributes based on the actual raw filename submitted
		public function processRawFiles($rawFiles, $subPath = null){
			
			$validFileTypes = "(css|js|jpg|jpeg|gif|bmp|png|pdf)";
			$pattern = "/[^\s]+.".$validFileTypes."$/";
			
			$fileImageTypes = array(
					'jpg',
					'jpeg',
					'gif',
					'bmp',
					'png',
			);
			
			$rawFiles = !is_array($rawFiles) ? array($rawFiles) : $rawFiles;
			
			$processedFiles = array();
			
			$tempSubPath = !is_null($subPath) ? preg_replace("/^(users\/)/","",$subPath,1) : "";
			foreach($rawFiles as $file){
				if (preg_match($pattern, strtolower($file))){
					$fileExt = substr($file, strrpos($file,".")+1);
					if( !is_null($subPath) ){
						$fileType =  preg_match("/^(users)/",$subPath) ? 'users' : ( in_array(strtolower($fileExt),$fileImageTypes) ? 'images' : $fileExt );
					}else{
						$fileType = in_array(strtolower($fileExt),$fileImageTypes) ? 'images' : $fileExt;
					}
					$processedFiles[$fileType][$this->s3Action][] = $tempSubPath.$file;
				}	
			}
			return $processedFiles;
		}
		
		// public function that processes svn log files and returns array of js, css and image files
		public function processSvnLogFile($logFileFullPath){
			
			$validFileTypes = "(css|js|jpg|jpeg|gif|bmp|png)";
			$pattern = "/[^\s]+.".$validFileTypes."$/";
			
			$svnStatusValidStoreAction = array(
				self::SVN_STATUS_NEW_FILE,
				self::SVN_STATUS_MODIFIED_FILE
			);

			$validPaths = array(
				//'css'		=> "/trunk/web/css/",
				//'js'		=> "/trunk/web/js/",
				'images' 	=> "/web/images/"
			);

			$svnFiles = array(
				'css' 		=> array(),
				'js' 		=> array(),
				'images' 	=> array(),
			);

		
			$handle = fopen($logFileFullPath, 'r');
			if ($handle){
				$buffer = fgets($handle);
				while(!feof($handle)){
					$buffer = trim(fgets($handle,16384));
					if (preg_match($pattern, strtolower($buffer))){
						foreach($validPaths as $key => $path){
							// check if file in current line is in a valid asset path
							if (0 < stripos($buffer,$path)){
								// log file must indicate either added or modified; or delete action
								// for now, implement add file only; disable delete method first
								if (in_array(substr($buffer, 0, self::SVN_STATUS_CODE_OFFSET), $svnStatusValidStoreAction)){
									$S3Action = self::S3_PUT_FILE;
									$svnFiles[$key][$S3Action][] =str_replace($path,'',substr($buffer, stripos($buffer,$path)));
								}
								//elseif (self::SVN_STATUS_DELETED_FILE == substr($buffer, 0,  self::SVN_STATUS_CODE_OFFSET)){
								//	$S3Action = self::S3_REMOVE_FILE;
								//}
								//$svnFiles[$key][$S3Action][] =str_replace($path,'',substr($buffer, stripos($buffer,$path)));
							}	
						}
					}
					if (preg_match("/Updated to revision/i", $buffer)) break;
				}
				fclose($handle);
				
			}
		
		return $svnFiles;

		}
		
		private function storeFiles($files){
			$this->queueFiles = $files;
		
			foreach($this->assetList as $asset ){
				if (array_key_exists($asset, $this->queueFiles)){
					//constant DOCUMENT_ROOT is defined in export-assets-to-cloudfront.php which is only accessible via cli
					//or it should be defined in frontend controllers that are run via cli
					$rootDir = "" == $_SERVER['DOCUMENT_ROOT'] ? DOCUMENT_ROOT."/".$asset."/" : $_SERVER['DOCUMENT_ROOT']."/".$asset."/";
					foreach($this->queueFiles[$asset] as $actionKey => $destinationFiles ){
						if (self::S3_PUT_FILE == $actionKey){
							foreach ($destinationFiles as  $destinationFile){
								$status = $this->doStoreFile($rootDir.$destinationFile, $this->getBucketName(), $asset."/".$destinationFile, S3::ACL_PUBLIC_READ);
							}
						}
						elseif (self::S3_REMOVE_FILE == $actionKey){
							foreach ($destinationFiles as  $destinationFile){
								$status = $this->doRemoveFile($this->getBucketName(), $asset."/".$destinationFile);
							}
						}
					}
				}
			}
		}
		
		private function doStoreFile($input, $bucket, $uri, $acl = S3::ACL_PRIVATE, $isResend=FALSE){
			$response = $this->s3->putObjectFile($input, $bucket, $uri, $acl);
			if( !$response ){
				if( !$isResend ){
					$failedAmazonS3Request = new GanetFailedAmazonS3Request();
					$failedAmazonS3Request->setAction(self::S3_PUT_FILE);
					$failedAmazonS3Request->setBucket($bucket);
					$failedAmazonS3Request->setUri($uri);
					$failedAmazonS3Request->setInput($input);
					$failedAmazonS3Request->setAcl($acl);
					$failedAmazonS3Request->save();
				}
				if( 1 < $_SERVER["argc"] ){//echo status when page accessed via cli
					echo "failed sending ".$input."\n";
				}
			}else if( 1 < $_SERVER["argc"] ){//echo status when page accessed via cli
				echo $input." successfully sent\n";
			}
			return $response;
		}
	
		private function doRemoveFile($bucket, $uri){
			$response = $this->s3->deleteObject($bucket, $uri);
			if( !$response ){
				$failedAmazonS3Request = new GanetFailedAmazonS3Request();
				$failedAmazonS3Request->setAction(self::S3_REMOVE_FILE);
				$failedAmazonS3Request->setBucket($bucket);
				$failedAmazonS3Request->setUri($uri);
				$failedAmazonS3Request->save();
			}
			return $response;
		}
		
		public function pushFile($input, $bucket, $uri, $acl = S3::ACL_PRIVATE, $isResend=FALSE){
			return $this->doStoreFile($input, $bucket, $uri, $acl, $isResend);
		}
	}