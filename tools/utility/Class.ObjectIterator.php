<?
/**
* <b>Result Iterator</b> class.
* @author Aldwin S. Sabornido
* @version 1.0 PHP5
* @see http://www.GoAbroad.com/
* @copyright 2006-2007
*/
class ObjectIterator
{
    private $it;
    public function __construct($object , Paging $paging, $optimize = false)
    {
    	   
	    // get ArrayObject
    	$arrobj = new ArrayObject();
    	// get Iterator object
    	
    	$this->it=$arrobj->getIterator();
    	if( !$optimize ){
	    	if ($paging->getStartRow() >= 0 ){
		    	for( $i = $paging->getStartRow(); $i < $paging->getEndRow(); $i++):
		    		if ( count($object) > $i ){
		    			if (is_object($object[$i]) )
		    				$arrobj[] = $object[$i];
		    		}
		    	endfor;
	    	}
    	}else{
    		if( count($object) ){
	    		foreach( $object as $obj ){
	   				$arrobj[] = $obj;
	    		}
    		}  
    	}
    }
    
    // reset pointer of result set
    public function rewind()
    {
        	return $this->it->rewind();
    }
    
    // get current row
    public function current()
    {
        	return $this->it->current();
    }
    
    // check if still a valid position
    public function valid()
    {
  			return $this->it->valid();
    } 	
    
    // get next row
    public function next()
    {
        	return $this->it->next();
    }
    
    // seek row
    public function seek($pos)
	{
	        if(!is_int($pos)||$pos<0)
	        {
	         		throw new Exception('Invalid position');
	        }
	        return $this->it->seek($pos);
    }
    
    // count rows
    public function count()
    {
     		return $this->it->count();
    }
}
?>
