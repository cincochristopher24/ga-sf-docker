<?php
	/***
	 * Author: Reynaldo Castellano III
	 * Definition: Delegator base class for Decorator Pattern
	 * Date: March 23, 2007
	 */
	
	abstract class Decorator
	{
		protected $mCoreObject = null;
		protected $mPassiveMethods = array(); 
			
		function __construct($coreObject=null/*,$passiveMethods=array()*/){
			if(!is_object($coreObject)){
				throw new exception("An object is required in constructor method of object of type Decorator.");
			}
			else{
				$this->mCoreObject = $coreObject;
			}
			/*
			if(is_array($passiveMethods)){
				foreach($passiveMethods as $iMethod){
					$this->addPassiveMethod($iMethod);
				}
			}
			*/
		}
		
		final protected function addPassiveMethod($method=''){
			if(is_string($method) && !in_array($method,$this->mPassiveMethods) && strlen($method)){
				$this->mPassiveMethods[] = $method;
			}
		}
		
		final protected function removePassiveMethod($method=''){
			if(in_array($method,$this->mPassiveMethods)){
				//get the index of passive method
				$flipped = array_flip($this->mPassiveMethods);
				array_splice($this->mPassiveMethods,$flipped[$method], 1);
			}
		}
		
		//Returns the coreObject of this decorator
		final public function getCoreObject(){
			return $this->mCoreObject;
		}
		
		//determines if one of the objects in the object chain is an instance of a given classname		
		final public function isInstanceOf($class=null){
			//check if the class is a string				
			if(is_string($class)){
				$className = $class;
			}
			else{
				if(is_object($class)){
					$className = get_class($class);
				}
				else{
					throw new exception("Invalid argument [$class] in method isIntanceof of object of type ".get_class($this).". It must be of type string or an intance of a class.");
				}
			}
			if($this instanceof $class){
				return true;
			}
			else{
				if($this->mCoreObject instanceof Decorator){
					return $this->mCoreObject->isInstanceOf($class);
				}
				else{
					return get_class($this->mCoreObject) == $class;
				}
			}	
		}
		
		/***
		 * Function that would determine if one of the objects within the object chain has a method with the name passed as parameter. 
		 */
		
		final public function methodExists($methodName=null){
			if(!is_string($methodName)){
				throw new exception("Invalid argument [$methodName] in method methodExists of object of type ".get_class($this));
			}
			if(in_array($methodName, get_class_methods($this))){
				return true;
			}
			else{
				if($this->mCoreObject instanceof Decorator){
					return $this->mCoreObject->methodExists($methodName); 
				}
				else{
					return in_array($methodName, get_class_methods($this->mCoreObject));
				}
			}
		}
		
		/**
		 * method delegator
		 */
		final function __call($method,$args){			
			/***
			 * - There are certain problems when dealing with the object hierarchy. If you are trying to call a public method in the third level
			 * object and in the second level object there is a private method with the same methodname that you are calling, then an error will be returned since
			 * the interpreter will assume that you are trying to call a private method in the second level object which is not allowed.
			 * - To avoid calling private methods that would result to an error, we must check if the method is declared as public
			 * in the coreObject. Otherwise lets call directly its delegator method (__call) if one is available to delegate the requested 
			 * method (if coreobject is a delegator).
			 **/
			$trace = debug_backtrace();
			if(in_array($method, get_class_methods($this->mCoreObject)) && !in_array($method,$this->mPassiveMethods) ){
				return call_user_func_array(array($this->mCoreObject, $method), $args);	
			}
			else{ //if the method that is being accessed is not accessible, try to call the handler method (__call). If all fails, throw an error.
				if(in_array('__call', get_class_methods($this->mCoreObject))){
					return call_user_func_array(array($this->mCoreObject, '__call'),array($method,$args));
				}
				else{
					throw new exception("Undefined method [$method].".'<<< Error occured in FILE: '.$trace[0]['file'].' on LINE: '.$trace[0]['line'].' >>>');
				}
			}
		}
	}
?>