<?php

/**
 * @author		Jonas Tandinco
 * created		Nov/14/2008
 */

class gaParameterHolder {
	
	protected $attributes = array();
	protected $nameSpaces = array();
	
	public function get($varName, $nameSpace = NULL) {
		if (!is_null($nameSpace)) {
			if (array_key_exists($nameSpace, $this->nameSpaces)) {
				if (array_key_exists($varName, $this->nameSpaces[$nameSpace])) {
					return $this->nameSpaces[$nameSpace][$varName];
				} else {
					throw new Exception ("variable not set for namespace: $nameSpace");
				}
			} else {
				throw new Exception('namespace not set.');
			}
		} elseif (array_key_exists($varName, $this->attributes)) {
			return $this->attributes[$varName];
		} else {
			throw new Exception("Variable $varName not set");
		}
	}
	
	public function set($varName, $value, $nameSpace = NULL) {
		if (!is_null($nameSpace)) {
			$this->nameSpaces[$nameSpace][$varName] = $value;
		} else {
			$this->attributes[$varName] = $value;
		}
	}
	
}