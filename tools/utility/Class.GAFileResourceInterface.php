<?php
/**
 * Created on Jun 25, 2007
 * 
 * @author     Wayne Duran
 */

interface GAFileResourceInterface {
	public function getIdentifier();
	public function getLookUpPaths(); // must return an array
	
	/**
	 * getFilename
	 * Handle string manipulations and return the proper 
	 * filename based on your naming conventions.
	 */
	public function getFileName($file);
}
?>
