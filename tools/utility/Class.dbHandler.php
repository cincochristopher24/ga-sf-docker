<?php
	/***
		Class that represents a database layer with GaLogging implemtation
	***/
	require_once('gaLogs/Class.DataTracker.php');
	require_once('Class.DBConnection.php');
	
	class dbHandler extends DBConnection{
	
		public function execute($strQry){
			//initialize tracker
			$tracker = DataTracker::create();
			$tracker->analyzeQuery($strQry,$this->cnn);
			
			$qry = parent::execute($strQry);
			
			//finalize tracker
			$tracker->trackChanges($qry);
			return $qry;
		}
	}
?>