<?php

require_once('utility/errorSender/interface/Class.ErrorSenderObserver.php');

class FacebookAppsObserver implements ErrorSenderObserver {
	
	private $errorMessage = null;
	private $storeErrorMessage = false;
	private $recipients = array('gapdaphne.notifs@gmail.com');
	
	public function __construct(){}

	public function notifyObserver(GaErrorLog $errorLog){
		$this->storeErrorMessage = false;
		$this->_filter($errorLog);
		$this->_setMessage($errorLog);		
	}

	public function storeErrorMessage(){
		return $this->storeErrorMessage;
	}

	public function send(){
		if(!is_null($this->errorMessage)){
			echo "<br/><strong>send error message from facebook apps sender : </strong><br/>";
			$mail = new PHPMailer(); 
			$mail->IsSMTP();
			$mail->IsHTML(true);

			$mail->FromName = "Facebook Apps Error";
			$mail->From = "error@goabroad.net";

			$mail->Subject = "Facebook Apps Error (Bulk)";
		//	$message = "<p><strong><em>This is a test mail from error logger, please ignore. -nash</strong></em></p>";
		//	$mail->Body = $message.$this->errorMessage;
			
			$mail->Body = $this->errorMessage;
			
			$addresses = $this->recipients;
			foreach($addresses as $iAddr){
				$mail->AddAddress($iAddr);
			}
		//	$mail->AddAddress('reynaldo.castellano@goabroad.com');
		//	$mail->AddAddress('nash.lesigon@goabroad.com');
		//	$mail->AddBCC('nash.lesigon@goabroad.com');
			$mail->AddBCC('ganet.notifs@gmail.com');
			$mail->Send();

		//	echo $message;
			echo $this->errorMessage;
			echo "<br/><strong><em>SENT</em></strong><br/>";
		}
	}

	/* 
	PRIVATE FUNCTIONS
	*/

	private function _setMessage(GaErrorLog $errorLog){
		if($this->storeErrorMessage){
			$message = $errorLog->getAsMessage(false);
			$message .= '<br />======================== *** ==========================<br />';
			$this->errorMessage = $this->errorMessage.$message;
		}

	}

	private function _filter(GaErrorLog $errorLog){
		$script_filename = $this->_getScriptFileNameFromErrorLog($errorLog);
		$pattern = "/facebook/";
		if(strpos($script_filename, $pattern) != false){
			$this->storeErrorMessage = true;
		}
		else {
			$this->storeErrorMessage = false;
		}
	}

	private function _getScriptFileNameFromErrorLog(GaErrorLog $errorLog){
		$subject = $errorLog->getVars();
		$script_filename = $this->_getStringByPattern($subject, '[SCRIPT_FILENAME]', '[REMOTE_PORT]');
		return $script_filename;
	}

	private function _getStringByPattern($string, $pattern1, $pattern2){
		$pos1 = strpos($string, $pattern1);
		$pos2 = strpos($string, $pattern2);

		$a = ($pos2 - $pos1);

		return substr($string, $pos1, $a);
	}

}