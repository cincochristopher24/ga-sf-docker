<?php

class ErrorSender {
	
	private $observers = array();
	
	public function __construct(){		
		$observerErrorSenderDir = dirname(__FILE__).'/ErrorSenderObservers/';
		$errorSenderClasses = scandir($observerErrorSenderDir);
		foreach($errorSenderClasses as $errorSenderClass){
			$fileNameLn = strlen($errorSenderClass);
			if('.php' == substr($errorSenderClass,$fileNameLn-4,$fileNameLn)){
				require_once($observerErrorSenderDir.'/'.$errorSenderClass);
				
				$key = strtoupper(str_replace(array('Class.','Observer.php'),'',$errorSenderClass));
				$clsName = str_replace(array('Class.','.php'),'',$errorSenderClass);
				
				$this->observers[$key] = new $clsName;
			}
			
		}
		
	}
	
	public function filter(GaErrorLog $errorLog){
		foreach($this->observers as $key => $observer){
			$observer->notifyObserver($errorLog);			
		}	
	}
	
	
	public function send(){
		foreach($this->observers as $observer){
			$observer->send();
		}
	}
}

