<?php
/**
* <b>TableHelpers</b> class.
* @author Wayne Duran
* @see http://www.GoAbroad.com/
*/

require_once('Class.HtmlHelpers.php');
class TableHelpers
{
	public static function instance()
 	{
 		if (self::$instance == null ) {
 			self::$instance = new TableHelpers();
 		}
 		
 		return self::$instance;	
 	}	
	
	// Creates a table row
	// Accepts an array where the integer index represent the sequence
	// by which each cell is populated
	public static function CreateRowFromArray($array, $row_attribs = '') {
		// row_attribs accepts an associative array
		// key value pairs are mapped to the corresponding attribute set
		// the key would be the attribute name, and the value will be the value of the attribute
		// for example the array ('class' => 'foo', 'value' => 'bar') would output the following attribute sets
		// class="foo" value="bar"
		$row = '<tr'. self::CreateAttributes($row_attribs) .'>';
		
		foreach ($array as $cell) {
			$row .= '<td>' . $cell . '</td>';
		}
		
		return($row . '</tr>');
	}
	
	
	// Creates a table row
	// Accepts an associative array and a cell sequence
	// The cell sequence is an array containing the keys
	// by which each cell would be arranged in order
	public static function CreateRowFromAssoc($array, $cell_sequence, $row_attribs = '') {
		// row_attribs accepts an associative array
		// key value pairs are mapped to the corresponding attribute set
		// the key would be the attribute name, and the value will be the value of the attribute
		// for example the array ('class' => 'foo', 'value' => 'bar') would output the following attribute sets
		// class="foo" value="bar"
		$row = '<tr'. self::CreateAttributes($row_attribs) .'>';
		
		foreach ($cell_sequence as $key) {
			$row .= '<td>' . $array[$key] . '</td>';
		}
		
		return($row . '</tr>');
	}
	
	// Creates a table row
	// Accepts an object and an array of method names
	public static function CreateRowFromObject($object, $method_calls, $row_attribs = '') {
		// row_attribs accepts an associative array
		// key value pairs are mapped to the corresponding attribute set
		// the key would be the attribute name, and the value will be the value of the attribute
		// for example the array ('class' => 'foo', 'value' => 'bar') would output the following attribute sets
		// class="foo" value="bar"
		$row = '<tr'. self::CreateAttributes($row_attribs) .'>';
		
		foreach ($method_calls as $method) {
			$row .= '<td>' . $object->$method() . '</td>';
		}
		
		return($row . '</tr>');
	}
	
	
	// This function accepts an array of associative arrays (2-dimensioinal
	// array) and an array on how to sequence each field --- sequence to display each field
	// on a table. The elements of the main array must be of the same type.
 	public static function CreateRowsFromArrays($rowsarray, $sequence) {
 		$rows = "";
		
		$i = 0;
		foreach ($rowsarray as $row) {
			$i++;
			$rows .= self::CreateRowFromAssoc($row, $sequence,  array( 'class' => (($i % 2 == 0) ? 'odd' : 'even') )   );
 		}
		
		return $rows;
 	}
	
 	// This function accepts an array of objects with methods to acces data
	// from each object and an array on what the methods are --- sequence to display each field
	// on a table. The elements of the main array must be of the same type.
 	public static function CreateRowsFromCollection ($collection, $methods)
 	{
 		$rows = "";
		
		$i = 0;
		foreach ($collection as $object) {
			$i++;
			$rows .= self::CreateRowFromObject($object, $methods,  array( 'class' => (($i % 2 == 0) ? 'odd' : 'even') )   );
 		}
 		return $rows;
 	}		 	 	
 	
 	
 }	
?>
