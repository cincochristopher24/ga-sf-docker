<?
/**
* <b>Database Connection</b> class.
* @author Aldwin S. Sabornido
* @version 1.0 PHP5
* @see http://www.GoAbroad.com/
* @copyright 2006-2007
*/

//require_once('Class.Ini.php');
//require_once('Class.IniConfig.php');
require_once('serverConfig/Class.ServerConfig.php');

class Connection
{
	private   $host;
	private   $user;
	private   $pass;
	private   $dbname;
	protected $connection;
	
	private static $isInitialized = false;
	private static $sHost;
	private static $sUser;
	private static $sPass;
	private static $sConnection;
	
	function __construct($dbname='Travel_Logs')
	{
		/**
		require_once('travellog_/domains/'.$_SERVER['SERVER_NAME'].'.php');
		$ini = IniConfig::instance();
		$this->host   = $ini->getHost();
	    $this->user   = $ini->getUser();
	    $this->pass   = $ini->getPassword();
		*/
		if (!self::$isInitialized) { // if connection is initialized the first time
			/***
			$ini = IniConfig::instance();
			self::$sHost  = $ini->getHost();
		    self::$sUser  = $ini->getUser();
		    self::$sPass  = $ini->getPassword();			
			***/
			$serverConfig = ServerConfig::getInstance();
			self::$sHost  = $serverConfig->getDbHost();
		    self::$sUser  = $serverConfig->getDbUser();
		    self::$sPass  = $serverConfig->getDbPassword();			
			self::$isInitialized = true;
		}
		
		$this->dbname = $dbname;
		
		//$this->connection = self::$sConnection;
		//$this->connection = mysql_connect($this->host,$this->user,$this->pass) or die(mysql_error());
		
		$this->connection = mysql_connect(self::$sHost,self::$sUser,self::$sPass) or die(mysql_error());
		
	}
	
	function Close()
	{		
		MYSQL_CLOSE($this->connection) OR DIE(MYSQL_ERROR());				
	}
	
	function GetConnection()
	{
		return $this->connection;
	}
	
	function GetDBName(){
		return $this->dbname;  
	}	
	
	public function __wakeup()
    {
        $this->connection	=	mysql_connect(self::$sHost,self::$sUser,self::$sPass) or die(mysql_error());
    }
	
}

?>
