<?php
include_once('Class.TableHelpers.php');
include_once('Class.FormHelpers.php');
require_once('Class.Ini.php');
require_once('Class.GAHtmlTemplate.php');

class Template extends GAHtmlTemplate {
	
	public function set_vars($array) {
		$this->setVars($array);
	}
	
	public function set_path($path) {
		$this->setPath($path);
	}
	
	static function isBrowserSupported(){

		$myBrowser = get_browser(null, true);	
		$allow = true;
		
		switch($myBrowser['browser']){
			case 'Firefox':
				if( 1.5 > $myBrowser['version'] )
					$allow = false;
				break;
			case 'IE':
				if( 5.5 > $myBrowser['version'] )
					$allow = false;
				break;
			case 'Opera':
				if( 8 > $myBrowser['version'] )
					$allow = false;
				break;
			case 'Safari':
				if( 2 > $myBrowser['version'] )
					$allow = false;
				break;
			case 'Lynx':
				$allow = false;
				break;
			case 'Netscape':
				if( 7 > $myBrowser['version'] )
					$allow = false;
				break;					 
		}
		
		return $allow;
	}
	
	protected static $fb_required_tags = array();
	protected static $profile_fb_like_visible = TRUE;
	public static function isProfileFBLikeVisible(){
		return self::$profile_fb_like_visible;
	}
	public static function hideProfileFBLike(){
		self::$profile_fb_like_visible = FALSE;
	}
	public static function clearFBRequiredMetaTags(){
		self::$fb_required_tags = array();
	}
	public static function setFBRequiredMetaTags($tags=array()){
		self::$fb_required_tags = $tags;
	}
	public static function renderFBRequiredMetaTags(){
		foreach(self::$fb_required_tags AS $tag){
			echo $tag;
		}
	}
	
}

require_once('serverConfig/Class.ServerConfig.php');
if(ServerConfig::getInstance()->isTemplateInfoShown()){
	Template::displayLog();
}
if(ServerConfig::getInstance()->isJsMinified()){
	Template::setMinify(true);
}

Template::includeDependentJs("/min/g=ClassTemplateJs");
Template::includeDependent  ("<script type=\"text/javascript\">jQuery.noConflict();</script>");