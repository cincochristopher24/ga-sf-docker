<?php
require_once('Class.DataModel.php');

class LoginLogger {
	
	private static $instance = null;
	protected $conn = null;
	public static function getInstance(){
		if( is_null(self::$instance) ){
			self::$instance = new self;
		}
		return self::$instance;
	}
	
	private function __construct(){
		$this->conn = new dbHandler('db=Travel_Logs');
	}
	
	public function log(array $params){
		$sql = "INSERT INTO tblLoginLogs SET email = ".GaString::makeSqlSafe($params['email']).", ".
				"password = ".GaString::makeSqlSafe($params['password']).", ".
				"attempDate=now(), ".
				"ipAddress=".GaString::makeSqlSafe($params['remote_address']).", ".
				"status=".$params['status'];
		$this->conn->execQuery($sql);
		
	}
}