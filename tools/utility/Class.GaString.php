<?php
	/**
	 * @author	: Reynaldo E. Castellano III
	 * @date 	: Sept. 24, 2007
	 */
	class GaString
	{
		public static function makeSqlSafe($str){
			// Stripslashes
			if (get_magic_quotes_gpc()){
				$str = stripslashes($str);
		   	}
		   	// Quote if not a number or a numeric string
		   	//if (!is_numeric($str)){
		       	$str = "'" . mysql_real_escape_string($str) . "'";
		   	//}
		   	return $str;
		}
		
		public static function isValidEmailAddress($email){
	    	// First, we check that there's one @ symbol, and that the lengths are right
			//if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)){
			if (!preg_match("/^[^@]{1,64}@[^@]{1,255}$/", $email)){
			  // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
			  return false;
			}
			// Split it into sections to make life easier
			$email_array = explode("@", $email);
			$local_array = explode(".", $email_array[0]);
			for ($i = 0; $i < sizeof($local_array); $i++){
				//if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
				if (!preg_match("/^(([A-Za-z0-9!#$%&'*+\/=?^_`{|}~-][A-Za-z0-9!#$%&'*+\/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$/", $local_array[$i])) {
			    	return false;
			  	}
			}  
			//if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])){ // Check if domain is IP. If not, it should be valid domain name
			if (!preg_match("/^\[?[0-9\.]+\]?$/", $email_array[1])){ // Check if domain is IP. If not, it should be valid domain name
				$domain_array = explode(".", $email_array[1]);
			  	if (sizeof($domain_array) < 2) {
			    	return false; // Not enough parts to domain
			  	}
			  	for ($i = 0; $i < sizeof($domain_array); $i++){
			    	//if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])){
					if (!preg_match("/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$/", $domain_array[$i])){
			      		return false;
			    	}
			 	}
			}
			return true;  
	    }
		
		public static function makePossessive($str){
			$ar = str_split($str);
			if(strlen($str)){
				return ('s' == strtolower($ar[count($ar)-1]) ? $str."'" : $str."'s" );
			}
			return $str;
		}
		
		public static function isValidUrl($url){
			$pattern = '/^[a-zA-Z]+[:\/\/]+[A-Za-z0-9\-_]+\\.+[A-Za-z0-9\.\/%&=\?\-_]+$/i';
	      	return preg_match($pattern, $url) > 0;
		}
		
		public static function activateUrlLinks($str){
			//activate all the links in the post, if it is not enclosed in an link(a) tag.
			//find all words that starts with a url protocol (e.g http, https)
			$pattern1 = '@(?:https?|ftp|file)://[\S]+(?:[\S])@';
			$subject = $str;
			$dummySubject = preg_replace_callback('/<a.*<\/a>/iU',create_function('$matches','return str_repeat("~",strlen($matches[0]));'),$subject);
			preg_match_all($pattern1,$dummySubject,$matches,PREG_OFFSET_CAPTURE);
			if(count($matches)){
				$newMatches = array_reverse($matches[0]);
				foreach($newMatches as $iMatch){
					//check if it is a valid url
					if(self::isValidUrl($iMatch[0])){
						$url = '<a href="'.$iMatch[0].'">'.$iMatch[0].'</a>';
						$subject = substr_replace($subject,$url,$iMatch[1],strlen($iMatch[0]));
					}
				}
			}
			return $subject;
		}
		/**
		* @return formatted number $###,###.##
		**/
		public static function dollarFormat($number){
			if(!is_numeric($number))
				return $number;
			return '$' . trim(number_format($number, 2, '.',','));
		}
		
		static public function truncateByWord($str,$numChars,$dotTrail=false){
			if(strlen($str) > $numChars){
				$chunks = explode(' ',$str);
				$result = '';
				foreach($chunks as $iChunk){
					//var_dump(strlen($result.' '.$iChunk));
					if(strlen($result.' '.$iChunk) > $numChars){
						return ($dotTrail ? $result.'...' : $result);
					}
					$result.= ' '.$iChunk;
				}
				return $result;
			}
			return $str;
		}
		
	}
?>