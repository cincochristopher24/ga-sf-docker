<?php
/** 
 * GAFileResourceManager, A File and Resource Path Management Class
 * 
 * Created on Jun 20, 2007
 *
 * @author     Wayne Duran
 * 
 * TODO: Add Path caching
 */


require_once('Class.GAFileResource.php');

class GAFileResourceManager {
	
	private static $instance = NULL;
	private $resources = array();
	
	public static function instance()
 	{
 		if (self::$instance == NULL ) {
 			self::$instance = new GAFileResourceManager();
 		}
 		
 		return self::$instance;	
 	}
 	
 	private function __contstruct() {}
 	
 	private function __clone() {}
 	
 	
 	public function register(GAFileResourceInterface $resource) {
 		$this->resources[$resource->getIdentifier()] = $resource;
 		return true;
 	}
 	
 	
 	public function __call($method, $arguments) {
 		// simple string functions
 		$action = '';
 		
 		if (0 === strpos($method, 'get')) {
 			// It's a get method. Extract the Resource Type
 			$name = substr($method, 3);
 			$action = 'get';
 		} else {
 			throw new GAFileResourceManagerMethodUnknownException("The method specified is uknown.");
 		}
 		
 		// :TODO Maybe this can be refactored to use the expression if($resource = $this->resources[$name])?
 		if($this->validateResource($name)) {
 			return $this->lookUpResource($name, $arguments[0]);
 		} else {
 			throw new GAFileResourceUnknownException("Unknown or unregistered resource '$name'");
 		}
 	}
 	
 	private function validateResource($resource_name) {
 		return array_key_exists($resource_name, $this->resources);
 	}
 	
 	
 	private function lookUpResource($resource_id, $file) {
 		// Resource Type Found
		$resource = $this->resources[$resource_id];
		
		// File Name Extraction
		$fname = $resource->getFileName($file);
		$paths = array_reverse(array_merge($resource->getLookUpPaths(), array('')));
		
		foreach($paths as $key => $val) {
			
			if (is_string($key)) {
				$path = $val;
				$out  = $key;
			} else {
				$path = $val;
				$out  = $val;
			}
			if ($this->file_exists_incpath($path . $fname)) {
				return $out . $fname;
				break;
			}
		}
		throw new GAFileResourceManagerException ("The file '$fname' was not found");
 	}
 	
 	
	/**
	 * Check if a file exists in the include path
	 *
	 * @version     1.2.1
	 * @author      Aidan Lister <aidan@php.net>
	 * @link        http://aidanlister.com/repos/v/function.file_exists_incpath.php
	 * @param       string     $file       Name of the file to look for
	 * @return      mixed      The full path if file exists, FALSE if it does not
	 */
	private function file_exists_incpath ($file)
	{
	    $paths = explode(PATH_SEPARATOR, get_include_path());
	 
	    foreach ($paths as $path) {
	        // Formulate the absolute path
	        $fullpath = $path . DIRECTORY_SEPARATOR . $file;
	 
	        // Check it
	        if (file_exists($fullpath)) {
	            return true;
	        }
	    }
	 
	    return false;
	}
}



class GAFileResourceManagerException extends GAFileResourceException {} 
class GAFileResourceUnknownException extends GAFileResourceException {}
class GAFileResourceManagerMethodUnknownException extends GAFileResourceManagerException {}
?>
