<?
/**
* <b>Result Iterator</b> class.
* @author Aldwin S. Sabornido
* @version 1.0 PHP5
* @see http://www.GoAbroad.com/
* @copyright 2006-2007
*/
class ResultIterator
{
    private $it;
    public function __construct(Recordset $obj)
    {
        if(get_resource_type($obj->Resultset())!='mysql result')
        {
            throw new Exception('result must be a MySQL result set');
        }
    	   
	    // get ArrayObject
    	$arrobj = new ArrayObject();
    	// get Iterator object
    	$this->it=$arrobj->getIterator();
    	while($row=mysql_fetch_assoc($obj->Resultset()))
    	{
        	//$arrobj[]=implode(' ',$row);
        	$arrobj[]=$row;
    	}
    }
    
    // reset pointer of result set
    public function rewind()
    {
        return $this->it->rewind();
    }
    
    // get current row
    public function current()
    {
        return $this->it->current();
    }
    
    // check if still a valid position
    public function valid()
    {
  		return $this->it->valid();
    } 	
    
    // get next row
    public function next()
    {
        return $this->it->next();
    }
    
    // seek row
    public function seek($pos)
	{
        if(!is_int($pos)||$pos<0)
        {
            throw new Exception('Invalid position');
        }
        return $this->it->seek($pos);
    }
    
    // count rows
    public function count()
    {
        return $this->it->count();
    }
}
?>
