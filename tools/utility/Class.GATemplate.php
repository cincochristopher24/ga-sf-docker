<?php
/**
 * Created on Jul 3, 2007
 * 
 * @author     Wayne Duran
 */

class GATemplate implements ArrayAccess {
	protected $vars = array(); // Holds all the template variables
	protected $path; // Path to the templates
	protected $template_file; // Template file to use
	private static $helpers = array();
	private static $helper_methods = array();
	
	
	public function __construct() {
		
	}
	

	/**
	 * Method that adds functionality to the template object.
	 * Accepts a valid defined class name. Public static
	 * methods defined in the class are added to the object's
	 * method list and will be available to the calling object
	 * as if it was defined as public function
	 */
	public static function registerHelper($class) {
		if (!class_exists($class))
			return false;
		
		$reflector = new ReflectionClass($class);
		
		foreach ($reflector->getMethods() as $method) {
			if ($method->isStatic() && $method->isPublic()) {
				self::$helper_methods[$method->getName()] = $method;
			}
		}
		self::$helpers[] = $class;
		return true;
	}
	
	/**
	 * Flush the list of helper methods. Use with caution.
	 * The effect of executing this method is undoable.
	 * You need to re-register the Helper methods again
	 * to use those methods
	 */
	public static function clearHelperRegistry() {
		self::$helpers = array();
		self::$helper_methods = array();
	}
	
	public function __call($name, $arguments) {
		if (array_key_exists($name, self::$helper_methods)) {
			return self::$helper_methods[$name]->invokeArgs(NULL, $arguments);
		} else {
			throw new GATemplateException('Undefined method "'.$name.'" or the helper method was not registered');
			return NULL;
		}
	}
	
	
	public function getPath() {
		return $this->path;
	}
	
	
	public function setPath($path) {
		$this->path = $path;
	}
	
	
	/**
	 * Set the template to use.
	 */
	public function setTemplate($file) {
		$this->template_file = $file;
	}

	/**
	 * Set or add a template variable.
	 *
	 * @param		string			$name		The name of the variable.
	 * @param		mixed				$value	The value for the variable.
	 * @return	GATemplate					This object
	 */
	public function set($name, $value) {
		$this->vars[$name] = $value;
		
		return $this;
	}
	
	public function get($name) {
		return $this->vars[$name];
	}
	
	/**
	 * An alias of set(). 
	 * 
	 * @param		string			$name		The name of the variable.
	 * @param		mixed				$value	The value for the variable.
	 * @return	GATemplate					This object
	 */
	public function setVar($name, $value) {
		return $this->set($name, $value);
	}

	/**
	 * Retrieves the value of the variable.
	 * 
	 * @param		string		$name			The name of the variable.
	 * @param		mixed			$default	The default value if the this template has no variable with name equal to $name.
	 * @return	mixed								The type of value is defined by the value set using setVar() in case the variable exists
	 * 															and the type of $default in case the variable does not exists.
	 */
	public function getVar($name, $default = null) {
		return ($this->hasVar($name)) ? $this->vars[$name] : $default;
	}
	
	/**
	 * Checks if a variable exists in the template.
	 * 
	 * @param		string		$name		The name of the variable to be check.
	 * @return	boolean						true, if $name exists as a variable of this template and false otherwise.
	 */
	public function hasVar($name) {
		return array_key_exists($name, $this->vars);
	}

	/**
	 * Set a bunch of variables at once using an associative array.
	 *
	 * @param array $vars array of vars to set
	 * @return void
	 */
	public function setVars($vars, $clear = false) {
		foreach ($vars as $var => $val) {
			$this->set($var, $val);
		}
	}
	
	
	public function getVars() {
		return $this->vars;
	}
	
	
	
	/**
	 * Array Access interface implementation.
	 * These methods allow for accessing the template
	 * variables used for displaying by the array
	 * syntax: $tpl['variable_name']; The following
	 * code are equivalent: 
	 * $tpl->set('var', 'value');
	 * $tpl['var'] = 'value';
	 */
	public function offsetExists($offset) {
		return array_key_exists($offset, $this->vars);
	}
	
	/**
	 * Array Access interface implementation
	 */
	public function offsetGet($offset) {
		if (array_key_exists($offset, $this->vars)) {
			return $this->vars[$offset];
		} else {
			return NULL;
		}
	}
	
	/**
	 * Array Access interface implementation
	 */
	public function offsetSet($offset, $value) {
		$this->set($offset, $value);
	}
	
	/**
	 * Array Access interface implementation
	 */
	public function offsetUnset($offset) {
		unset($this->vars[$offset]);
	}


	/**
	 * Open, parse, and return the template file.
	 *
	 * @param string string the template file name
	 *
	 * @return string
	 */
	public function fetch($_file = NULL) {
		if (NULL == $_file) {
			$_file = $this->template_file;
		}
		
		extract($this->vars);           // Extract the variables set
		ob_start();						// Start output buffering
		include($this->path . $_file);	// Include the file
		$_contents = ob_get_contents();	// Get the contents of the buffer
		ob_end_clean();					// End buffering and discard
		return $_contents;				// Return the contents
		
	}
	
	
	public function __toString() {
		return $this->fetch();
	}
}

class GATemplateException extends Exception {}
?>
