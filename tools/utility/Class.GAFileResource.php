<?php
/**
 * Created on Jun 25, 2007
 * 
 * @author     Wayne Duran
 */



require_once('Class.GAFileResourceInterface.php');

class GAFileResource implements GAFileResourceInterface {
	
	private $lookUpPaths = array();

	private static $instance = NULL;
 	
 	private function __contstruct() {}
 	
 	private function __clone() {}
	
	public static function instance()
 	{
 		if (self::$instance == NULL ) {
 			self::$instance = new self();
 		}
 		
 		return self::$instance;	
 	}
 	
	public function flushLookUpPathList() {
		array_splice($this->lookUpPaths, 0);
	}
	
	public function getIdentifier() {
		return 'File';
	}

	public function addLookUpPath() {
		$nargs = func_num_args();
		$args = func_get_args();
		if ( $nargs === 1 && is_string($args[0])) {
			// A single string for a path
			$this->lookUpPaths[] = $args[0];
		} elseif ( $nargs === 1 && is_array($args[0])) {
			// An Associative array
			foreach($args[0] as $key => $val) {
				$this->lookUpPaths[$val] = $key;
			}
		} elseif ( $nargs === 2) {
			$this->lookUpPaths[$args[1]] = $args[0];
		} else {
			throw new GAFileResourceException('Invalid argument.');
		}
	}
	
	public function getLookUpPaths() {
		return $this->lookUpPaths;
	}
	
	public function getFileName($file) {
		return $file;
	}
	
	public function withPaths() {
		if (func_num_args() > 0) {
			$paths = array_reverse(func_get_args());
			
			foreach($paths as $path) {
				$this->addLookUpPath($path);
			}
		}
		
		return $this;
	}
}

class GAFileResourceException extends Exception {}
?>
