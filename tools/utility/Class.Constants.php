<?php
	/*
	 * Class.Constants.php
	 * Created on Nov 12, 2007
	 * created by marc
	 */
	 
	 class constants{
	 	
	 	// allowed cobrands
	 	static $ALLOWED_COBRANDS  = array('COBRAND' , 'ADVISOR', 'AIFS', 'PROWORLD', 'GLOBALSCHOLAR', 'CIS', 'CEA', 'IEP', 'ALLIANCEABROAD' , 'CULTURALEMBRACE' , 'WORLDENDEAVORS');
	 	
	 	// error actions
		const NON_REDIRECT = 0;
		const AUTO_REDIRECT = 1;
		 
		// message sending status 
		const FAILED = 0;
		const BLOCKED = 1;
		const SENT = 2;
		
		// users
		const TRAVELER = 0;
		const GROUP = 1;
		const TRAVELERCB = 2;
		const GACOM_CLIENT = 3;
		const MODERATOR = 4;
		
		// register
		const CHOOSE = 2;
		
		// folders for messages
		const INBOX = 'inbox';
		const DRAFTS = 'drafts';
		const SENT_ITEMS = 'sent';
		const TRASH = 'trash';
		const INQUIRY = 'inquiry'; 
		const PENDING_MESSAGES = 'pendingMessages';
		
		// message types
		const MESSAGE_TYPE_INBOX = 1;
		const MESSAGE_TYPE_DRAFTS = 3;
		const MESSAGE_TYPE_INQUIRY = 4;
		
		/**
		 * views
		 */
		const PAGING = "viewPaging";
		
		// message
		const MESSAGE_FORM = "MessageForm";
		const MESSAGE_CONFIRMATION = "MessageConfirmation";
		const VIEW_MESSAGE = "ViewMessage";
		const MESSAGE_LIST = "ViewMessageList";
		const MESSAGE_NAVIGATION = "MessageNAvigation";
		const MESSAGE_PAGING = "MessagePaging";
		const MESSAGE_ERRORS = "MessageErrors";
		const MESSAGE_ACTIONS = "MessageActions";
		const SPAM_LIST = "ViewSpamList";
		
		// register
		const VIEW_CHOOSE_REGISTER = "viewChooseRegister";
		const VIEW_GROUP_REGISTRATION_FORM = "viewGroupRegistration";
		const VIEW_TRAVELER_REGISTRATION_FORM = "viewTravelerRegistration";
		const VIEW_REGISTER_CONFIRMATION = "viewRegisterConfirmation";
		const VIEW_REGISTRATION_DETAILS = "viewRegistrationDetails";
		const VIEW_WELCOME_PAGE = "viewWelcomePage";
		
		// login
		const VIEW_LOGIN_PAGE = "viewLoginPage";
		const VIEW_LOGIN_FORM = "viewLoginForm";
		
		// activation support
		const VIEW_ACTIVATION_SUPPORT_FORM_PAGE = "viewActivationSupportFormPage";
		const VIEW_ACTIVATION_SUPPORT_CONFIRMATION_PAGE = "viewActivationSupportConfirmationPage";
		
		// destination
		const VIEW_ALL_COUNTRIES_PAGE = "viewAllCountriesPage";
		const VIEW_COUNTRY_PAGE = "viewCountryPage";
		const VIEW_TRAVELERS_LIVING_IN_COUNTRY = "viewTravelersLivingInCountry";
		const VIEW_TRAVELERS_CURRENTLY_IN_COUNTRY = "viewTravelersCurrentlyInCountry";
		const VIEW_JOURNALS_IN_COUNTRY = "viewJournalsInCountry"; 
		const VIEW_COUNTRY_LIST = "viewCountryList";
		
		// forgot password
		const VIEW_FORGOT_PASSWORD_FORM_PAGE = "viewForgotPasswordFormPage";
		const VIEW_FORGOT_PASSWORD_CONFIRMATION_PAGE = "viewForgotPasswordCnfirmationPage";
		
		// student profile
		const VIEW_STUDENT_PROFILE_PAGE = "viewStudentProfilePage";
		const VIEW_REFERED_LISTINGS = "viewReferedListings";
		const VIEW_COUNTRY_PREFERENCE = "viewCountryPreference";
		const VIEW_STUDENT_PREFERENCE = "viewStudentPreference";
		const VIEW_STUDENT_PREFERENCE_FORM = "viewStudentPreferenceForm";
		const VIEW_STUDENT_PROFILE_FORM = "viewStudentProfileForm";
		const VIEW_STUDENT_COMMUNICATIONS = "viewStudentCommunications";
		
		// advisor
		const VIEW_STUDENTS_LIST = 'viewStudentsList';
		const VIEW_LISTINGS_LIST = 'viewListingsList';
		const VIEW_MESSAGES_PER_STUDENT_AND_LISTING = 'viewMessagesPerStudentAndListing';
		/**
		 * end of views
		 */
		
		// login options
		const DISALLOW_ERROR = 0;
		const DISALLOW_INACTIVE = 2;
		const DISALLOW_SUSPENDED = 3;
		const DISALLOW_DEACTIVATED = 4;
		const ALLOW = 1;
		
		
		// mail generator
		const ACCOUNT_ACTIVATION = "accountActivation";
		const SUBSCRIBE_NEWSLETTER = "subscribeNewsLetter";
		const MAIL_ADMIN = "mailAdmin";
		const WELCOME = "welcome";
		const RESEND_ACTIVATION_EMAIL = "resendActivation";
		const FORGOT_PASSWORD = "forgotPassword";
		
		// online advisor
		const VIEW_BY_LISTING = "viewByListing";
		const VIEW_BY_STUDENT = "viewByStudent";
		const MODE_AJAX = 1; 
		const VIEW_LISTING = 'viewListing';
		const VIEW_CLIENT = 'viewClient';
		const CHANGE_VIEW = 'changeView';
		
		/**
		 * error codes
		 */
		
		
		const EMPTY_EMAIL_ADDRESS = 10;
		const INVALID_EMAIL_FORMAT = 11;
		const SECURITY_CODE_MISMATCH = 14;
		
		// messages
		const ERR_SEND_TO_SELF = 0;
		const ERR_BLOCKED = 1;
		const ERR_SUSPENDED = 2;
		const ERR_FAILED = 3;
		const ERR_UNDEFINED_DEST = 4;
		const ERR_SUBJECT = 5;
		const ERR_MESSAGE = 6;
		const ERR_SUBJECT_MESSAGE = 7;
		
		// register
		const EMPTY_FIRSTNAME = 1;
		const EMPTY_LASTNAME = 2;
		const EMPTY_USERNAME = 3; 
		const USERNAME_ALREADY_USED = 4;
		const INVALID_USERNAME_FORMAT = 5;
		const EMPTY_GROUPNAME = 6;
		const GROUPNAME_ALREADY_USED = 7;
		const INVALID_PASSWORD_FORMAT = 8;
		const PASSWORD_CONFIRMATION_FAILED = 9;
		const EMAIL_ALREADY_USED = 12;
		const DISAGREE_TERMS = 13;
		const INVALID_GROUPNAME_FORMAT = 15;
		const USERNAME_EXCEEDS_LIMIT = 16;
		const EMAIL_BLACKLISTED = 17;
		
		// activation support
		const TRAVELER_DOES_NOT_EXIST = 1;
		const EMPTY_MESSAGE = 2;
		
		/**
		 * end of error codes
		 */ 
		 
		/**
		 * user actions
		 */
		
		// generic
		const BACK = "back";
		const FILL_UP = "fillUp";
		const SUBMIT_DETAILS = "submitDetails";
		const DELETE = "delete";
		const EDIT = "edit";
		const VIEW = "view";
		
		// messages
		const SEND = "send";
		const SAVE_AS_DRAFT = "saveAsDraft";
		const MOVE_TO_TRASH = "moveToTrash";
		const COMPOSE = "compose";
		const REPLY = "reply";
		const INQUIRE = "inquire";
		const VIEW_MESSAGE_LIST = "viewMessageList";
				
		// register
		/* const CHOOSE_REGISTER_TYPE = "chooseRegisterType";
		const GROUP_SUBMIT_DETAILS = "advisorRegister";
		const TRAVELER_SUBMIT_DETAILS = "travelerRegister";
		const GROUP_CONFIRM_DETAILS = "groupConfirmDetails";
		const TRAVELER_CONFIRM_DETAILS = "travelerConfirmDetails";
		const GROUP_FILL_UP = "groupFillUp";
		const TRAVELER_FILL_UP = "travelerFillUp";
		const CLIENT_FILL_UP = "clientFillUp";
		const REGISTER_CONFIRMATION = "registerConfirmation"; */
		const CHOOSE_REGISTER_TYPE = "chooseRegisterType";
		const CONFIRM_DETAILS = "confirmDetails";
		
		
		// login
		const LOGIN = "login";
		const NON_LOGIN_PAGE_FILL_UP = "nonLoginPageFillUp";
		const LOGOUT = "logout";
		const FAILED_LOGIN = "failedLogin";
		const AUTO_LOGIN = "autoLogin";
		
		// account activation support
		const ACTIVATE_ACCOUNT = "activateAccount";
		const HELP_ACTIVATE = "helpActivate";
		
		// destinations
		const VIEW_COUNTRY = "viewCountry";
		const VIEW_ALL_COUNTRIES = "viewAllCountries";
				
		/**
		 * end of user actions
		 */ 
		
		// admin
		const ADMIN_TRAVELER_ID = 213;
		
		// redirects
		const REGISTER_PAGE = "registerPage";
		const LOGGED_IN_PAGE = "loggedInPage";
		const WELCOME_PAGE = "welcomePage";
		const HOME_PAGE = "homePage";

		//recaptcha
		const RECAPTCHA_SITE_KEY = '6LeS6g0TAAAAANGET0vpigumLW_FcAiOs_hMep6z';
		const RECAPTCHA_SECRET_KEY = '6LeS6g0TAAAAAIpuOqs3nR4pTaxOKWSzNyKVNNIz';
		
	 }
?>
