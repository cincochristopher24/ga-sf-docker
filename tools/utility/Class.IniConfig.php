<?php

class IniConfig {
	
	private static $instance = NULL;
	private static $registry = array();
	private static $configVals = array();
	
	private $defaultValues = array (
			'GMapKey' => 'ABQIAAAAdRzrMy_PPtAJC3X9Cv3l5RQwbP-JBhpjGqlbK3A4xF5aLrtY6RSWOoaOMJXoU0oq2GqMOlflVMN-hg',
			'MailServer' => '192.168.2.16',
			'IsTemplateLogDisplay' => false ,
			'IsAllowSendEmail' => true ,
			'Site'	=>	'GANET'
		);
		
	public static function instance(){
		
		if (self::$instance == NULL){
			self::$instance = new IniConfig();
		}
		return self::$instance;
	}
	
	public static function register($ini ) {
		self::$registry[$ini->getIdentifier()] = $ini;
		return true;
	}
	
	public static function getRegistry(){
		return self::$registry;
	}
	
	public function __call($iniVal , $arg) {

		$id = $_SERVER['SERVER_NAME'];
		$key = str_replace('get', '', $iniVal);
	
		if (self::$configVals)
			return self::$configVals[$key];
		elseif (array_key_exists($id, self::$registry)) {
			$iniobj = self::$registry[$id];
			$arrInitVals = $iniobj->getAvailableInitializationValues();
			self::$configVals = array_merge($this->defaultValues, $arrInitVals);
			return self::$configVals[$key];	
		}
		else
			return $this->defaultValues[$key];
		
	}
	
}

class InitException extends Exception {}

?>