<?
	/**
	* <b>Hybrid Paging</b> class.
	* note: this class was derived from Class.Paging.php by Sabornido, Aldwin
	*	  : to accomodate paging using AJAX 
	*	  : this class is to be used along with moo.fx.js, moo.fx.pack.js, moo.ajax.js and prototype1.lite.js
	* @author : marc
	* Sep 22, 2006
	* @version 1.0 PHP5
	* @see http://www.GoAbroad.com/
	* @copyright 2006-2007
	*/
	
	// for usual browser loading
	define('BROWSER_LOAD',0);	
	// ajax
	define('AJAX',1);

	class HybridPaging
	{
		private $recordcount;
		
		private $page;
		
		private $rowsperpage;
		
		private $next;
		
		private $previous;
		
		private $last;
		
		private $first;
		
		private $links='';
		
		private $querystring;
		
		private $elementID = '';
		
		private $page_load = BROWSER_LOAD;
		
		private $source_page = '';
		
		protected $startrow;
		
		protected $endrow;
		
		function __construct( $recordcount, $page, $querystring='', $rowsperpage=1, $page_load=BROWSER_LOAD, $elementID = '', $source_page = '')
		{
			$displaynum = 10;
			$this->recordcount = $recordcount;
			$this->rowsperpage = $rowsperpage;
			$totalpages = $this->getTotalPages();
			
			if( 1 >= $totalpages )
				return NULL;
			
			$this->page = $page;
			( $querystring != "" )? $this->querystring = "&".$querystring : $this->querystring = "";
			$this->elementID = $elementID;
			$this->page_load = $page_load;
			$this->source_page = $source_page;
			
			$this->first = 1;
			$this->last = $totalpages;
			
			( $recordcount == $page )? $this->next = $recordcount : $this->next = $page+1;
			( $page == 1 )? $this->previous = 1 : $this->previous = $page-1;
			
			if ( $totalpages < 11 && $page < 11 )
			{
				$startloop = 1;
				$endloop = $totalpages;
					
			}	
			else
			{
				if ( $page < 6)
				{ 
					$startloop = 1;
					$endloop = 10;
					
				}	
				else 
				{
					if ( ($page+5) >= $totalpages )
					{
						if ( ($totalpages-10) < 0)
							$startloop = 1;
						else
							$startloop = $totalpages-10;
							
						$endloop = $totalpages;
					}	
					else
					{
						if ( ($page-5) < 0)
							$startloop = 1;
						else
							$startloop = $page-5;
						$endloop = $page+5;
					}	
				}	
			}
			
			$this->links .= "&nbsp;";
			
			$this->startrow = $page*$rowsperpage-$rowsperpage;
			
			 ( $this->startrow+$rowsperpage  < $recordcount )? $this->endrow = $this->startrow+$rowsperpage : $this->endrow = $recordcount;
			
			if( $this->page_load == BROWSER_LOAD )
				while ( $startloop <= $endloop )
				{
					if ( $startloop != $page )
						$this->links .= "<a href=\"http://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']."?page=".$startloop.$this->querystring."\">".$startloop."</a>&nbsp;";
					else
						$this->links .= "<strong>".$startloop."</strong>&nbsp;";
						
					$startloop++;
				}
			else		
				while ( $startloop <= $endloop )
				{
					if ( $startloop != $page )
						$this->links .= "<a onclick=\"javascript: load_page('$this->elementID','$this->source_page','page=$startloop$this->querystring')\">" . $startloop . "</a>&nbsp;";
					else
						$this->links .= "<strong>".$startloop."</strong>&nbsp;";
						
					$startloop++;
				}
				
		}
		
		
		function getNext()
		{
			if( $this->page_load == BROWSER_LOAD )
				if ( $this->last != $this->page )
					return print("<a href=\"http://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']."?page=".$this->next.$this->querystring."\">"."Next</a>&nbsp;");
				else
					return print("<strong>Next</strong>&nbsp;");
			else		
				if ( $this->last != $this->page )
					return print("<a onclick=\"javascript: load_page('$this->elementID','$this->source_page','page=$this->next$this->querystring')\">Next</a>&nbsp;");
				else
					return print("Next&nbsp;");
		}	
		
		function getPrevious()
		{
			if( $this->page_load == BROWSER_LOAD )
				if ( $this->page != 1 )
					return print("<a href=\"http://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']."?page=".$this->previous.$this->querystring."\">"."Previous</a>&nbsp;");
				else
					return print("<strong>Previous</strong>&nbsp;");
			else		
				if ( $this->page != 1 )
					return print("<a onclick=\"javascript: load_page('$this->elementID','$this->source_page','page=$this->previous$this->querystring')\">Previous</a>&nbsp;");
				else
					return print("Previous&nbsp;");
			
		}
		
		function getLast()
		{	
			if( $this->page_load == BROWSER_LOAD )
				if ( $this->last != $this->page )
					return  print("<a href=\"http://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']."?page=".$this->last.$this->querystring."\">Last</a>&nbsp;");
				else
					return print("<strong>Last</strong>&nbsp;");
			else		
				if ( $this->last != $this->page )
					return print("<a onclick=\"javascript: load_page('$this->elementID','$this->source_page','page=$this->last$this->querystring')\">Last</a>&nbsp;");
				else
					return print("Last&nbsp;");
		}
		
		function getFirst()
		{
			if( $this->page_load == BROWSER_LOAD )
				if ( $this->page != 1 )
					return print("<a href=\"http://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']."?page=".$this->first.$this->querystring."\">First</a>&nbsp;");
				else
					return print("<strong>First</strong>&nbsp;");
			else		
				if ( $this->page != 1 )
					return print("<a onclick=\"javascript: load_page('$this->elementID','$this->source_page','page=$this->first$this->querystring')\">First</a>&nbsp;");
				else
					return print("First&nbsp;");
		}
		
		function getLinks()
		{
			return print($this->links);		
		}	
		
		function getStartRow()
		{
				return $this->startrow;
		}	
		
		function getEndRow()
		{
				return $this->endrow;
		}	
		
		function getTotalPages()
		{
				return ceil( $this->recordcount/$this->rowsperpage );
		}
	}	
?>
