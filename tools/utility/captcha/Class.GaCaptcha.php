<?php
	require_once('travellog/model/Class.SessionManager.php');
	class GaCaptcha
	{
		const CAPTCHA_LENGTH 	= 7;
		const CAPTCHA_WIDTH		= 200;
		const CAPTCHA_HEIGHT	= 60;
		
		static private $CHARSET	= array('a','b','c','d','e','f','g','h','i','j','k','m','n','p','q','r','s','t','u','v','w','x','y','z','2','3','4','5','6','7','8');
		
		static public function validate($paramKey){
			//check if the securit code is available in session
			if(array_key_exists('GaCaptchaKey',$_SESSION) && strlen($_SESSION['GaCaptchaKey'])){
				require_once('Class.Crypt.php');
				$crypt = new Crypt();
				return $crypt->decrypt($_SESSION['GaCaptchaKey']) === $paramKey;	
			}
			return false;
		}
		
		static public function clear(){
			$_SESSION['GaCaptchaKey'] = null;
		}
		
		static public function getImageSource(){
			return '/gaCaptcha/gaCaptcha.php';
		}
		
		static private function createSecurityCode(){
			$tempCharSet = self::$CHARSET;
	    	$arCode = array();
			for($i=0;$i<self::CAPTCHA_LENGTH;$i++){
				$idx = array_rand($tempCharSet);
				$arCode[] = $tempCharSet[$idx];
				//remove the index from the final charset
				unset($tempCharSet[$idx]);
				$tempCharSet = array_values($tempCharSet);
			}
			return implode('',$arCode);
		}
		
		static public function createImage(){			
			require_once('Class.Crypt.php');
			$crypt = new Crypt();
			$_SESSION['GaCaptchaKey'] = $crypt->encrypt(self::createSecurityCode());
			
			require_once('Class.Crypt.php');
			$crypt = new Crypt();
			$code = $crypt->decrypt($_SESSION['GaCaptchaKey']);
			$dirName = dirname(__FILE__);
			$fontPath 	= $dirName.'/fonts/';
			//get the avilable fonts
			$dirFonts = scandir($fontPath);
			$fonts = array();
			foreach($dirFonts as $iFont){
				if(preg_match('/.ttf$/i',$iFont)){
					$fonts[] = $fontPath.$iFont;
				}
			}
			$cFontFiles = $fonts;
			$curFont = $cFontFiles[2];
			//--start the actual creation of the image--

			$cWidth 	= self::CAPTCHA_WIDTH;
			$cHeight	= self::CAPTCHA_HEIGHT;

			//create the image
			$img = imagecreate($cWidth,$cHeight);
			//allocate white background color
			$transColor = imagecolorallocate($img, 255, 255, 255);
			
			$arCode = str_split($code);
			//draw the characters
			$codeLen = count($arCode);
			
			//create first the characters to draw and later draw them. This is better to have a more accurate font spacing			
			$arCharInfo = array();
			$totalCharWidth = 0;
			$maxH = $cHeight; 
			
			$curFontColor = imagecolorallocate($img,0,0,0);
		    
			for($i=0;$i< $codeLen;$i++) {
		        // select random font	            
		        // select random font size
		        $curFontSize = 30;
		        // select random angle
		        $fontAngle = rand(-15, 15);	            	                     
		        // get dimensions of character in selected font and text size
		        $charDetails = imageftbbox($curFontSize, $fontAngle, $curFont, $arCode[$i]);

		       	$curWidth = abs($charDetails[0]) + abs($charDetails[2]);
		       	$curHeight = abs($charDetails[1]) + abs($charDetails[5]);

		       	$maxH = ($curHeight > $maxH)?$curHeight:$maxH;
		        $arCharInfo[] = array(
		        	'charDetails' => $charDetails,
		        	'fontDetails' => array(
		        		'font' 	=> $curFont,
		        		'color'	=> $curFontColor,
		        		'size' 	=> $curFontSize,
		        		'angle' => $fontAngle,
		        		'width' => $curWidth,
		        		'height'=> $curHeight
		        	),
		        	'char' => $arCode[$i]
		        );
		        $totalCharWidth += $curWidth;
			}
			//compute the spacing of the letters base on the total character width - total image width / character length
			if($totalCharWidth >= $cWidth || $maxH > $cHeight){
				//create the image
				$cWidth = ($totalCharWidth >= $cWidth)?($totalCharWidth - $cWidth) + 20 : $cWidth;
				$cHeight = ($maxH > $cHeight)?$maxH:$cHeight;
				$img = imagecreate($cWidth, $cHeight);
				//allocate white background and make it tranparent
				$transColor = imagecolorallocate($img, 255, 255, 255);
			}

			$widthOrigin = 0;
			$spacing = (int)(($cWidth - $totalCharWidth) / $codeLen);
			$halfSpacing = (int)($spacing / 2);
			foreach($arCharInfo as $iChar){
				// write text to image
				$widthOrigin += $halfSpacing;
				//echo $widthOrigin.'<br />';
				$iX = $widthOrigin;
				$widthOrigin += (int)($iChar['fontDetails']['width']);
				$iY = ($cHeight / 2) + ($iChar['fontDetails']['height'] / 4);
		        imagefttext(
		        	$img, 
		        	$iChar['fontDetails']['size'],
		        	$iChar['fontDetails']['angle'],
		        	$iX,
		        	$iY,
		        	$iChar['fontDetails']['color'],
		        	$iChar['fontDetails']['font'],
		        	$iChar['char']
		        );
		        $widthOrigin += $halfSpacing;
			}

			//draw random dots

			for($i=0;$i<1800;$i++){
				$randomX = rand(0, $cWidth);
				$randomY = rand(0, $cHeight);
				imageline($img,$randomX,$randomY,$randomX+1,$randomY+rand(-1,1),$curFontColor);	
			}

			/*** draw the lines for distortion ***/
			//find the center of the canvas
			$centerX = $cWidth / 2;
			$centerY = $cHeight / 2;

			//get the x 90% from center x
			$lowerRangeX = $centerX * .20;
			$upperRangeX = $centerX * .80;

			$quadrant1 = array(
				'startX' 	=> $centerX - $upperRangeX,
				'endX'		=> $centerX + $lowerRangeX
			); 
			$quadrant2 = array(
				'startX'	=> $centerX - $lowerRangeX,
				'endX'		=> $centerX + $upperRangeX
			);

			//draw a line in quadrant 1
			$lowerHeight = $cHeight - ($cHeight*.80);
			$upperHeight = $cHeight - ($cHeight*.20);
			
			//draw a line in quadrant 1
			self::drawDistortionLines($img,$quadrant1['startX'],rand($lowerHeight,$upperHeight),$quadrant1['endX'],rand($lowerHeight,$upperHeight),$curFontColor);
			//draw a line in quadrant 2
			self::drawDistortionLines($img,$quadrant2['startX'],rand($lowerHeight,$upperHeight),$quadrant2['endX'],rand($lowerHeight,$upperHeight),$curFontColor);
			return $img;
		}
		
		static private function drawDistortionLines($canvas,$startX,$startY,$endX,$endY,$color){
			imageline($canvas,$startX,$startY-1,$endX,$endY-1,$color);
			imageline($canvas,$startX,$startY,$endX,$endY,$color);
			imageline($canvas,$startX,$startY+1,$endX,$endY+1,$color);
		}
	}
?>