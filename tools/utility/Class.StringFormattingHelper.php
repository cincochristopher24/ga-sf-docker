<?php
class StringFormattingHelper {
	/**
	 * Trim a string to a given number of words
	 *
	 * @param $string
	 *   the original string
	 * @param $count
	 *   the word count
	 * @param $ellipsis
	 *   The ellipsis to use. Default is '' (empty) string
	 *   
	 * @return
	 *   trimmed string with ellipsis added if it was truncated
	 */
	public static function wordTrim($string, $count, $ellipsis = ''){
	  $words = explode(' ', strip_tags($string));
	  if (count($words) > $count){
	    array_splice($words, $count);
		$words[] = $ellipsis;
	  }
	  $string = implode(' ', $words);
	  return $string;
	}
	
	public static function possessive($name) {
	    return (strlen($name)-1 == strripos($name,'s')) ? $name."'":$name."'s";
	}
}
?>