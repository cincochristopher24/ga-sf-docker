<?

class HelperGlobal
{
	
		public static function displayLoginLink($prepend = '', $append = '')
		{
				if(self::isLogin())
				{
					return $prepend.'<a href="/logout.php">Log out &raquo;</a>'.$append;
				}
				else
				{
					return $prepend.'<a href="/login.php">Log in</a>'.$append;
				}
		}
		
		public static function displayLink($prepend = '', $append = '')
		{
			return self::displayLoginLink();
		}
		
		
		
		public static function displayRegisterLink($prepend = '', $append = '')
		{
				if(!self::isLogin())
				{
					return $prepend.'<a href="/register.php">Register</a>'.$append;
				}
				else
				{
					return '';
				}
		}
		
		
		public static function isLogin()
		{
			$isLogged = false;
			if( isset($_SESSION["login"]) && isset($_SESSION["travelerID"]) && isset($_SESSION["domain"]) ){
				if( 0 == strcmp(crypt($_SESSION["travelerID"],CRYPT_BLOWFISH),$_SESSION["login"]) && 0 == strcmp($_SERVER["HTTP_HOST"],$_SESSION["domain"]) )
					$isLogged = true;
			}
			return $isLogged;
			
		}
		
		// added by marc
		
		public static function getUserName()
		{
			if( self::isLogin() ){
				require_once('travellog/model/Class.Traveler.php');
				$nTraveler = new Traveler($_SESSION['travelerID']);
				$tmpUsername = $nTraveler->getUserName();
				unset($nTraveler);
				return $tmpUsername;				
			}
			return NULL;
		}
		
		/***
		 * Added by naldz (March 22, 2007)
		 */
		public static function getGroupName($gID){
			if ( self::isLogin() ){
				require_once('travellog/model/Class.GroupFactory.php');
				$factory =  GroupFactory::instance();
				$group   =  $factory->create(array($gID));
				return $group[0]->getName();
			}
			return null;
		}
		
		/**
		 * added by K. Gordo 3/22/07
		 */
		public static function getGroup($gID){
			if ( self::isLogin() ){
				require_once('travellog/model/Class.GroupFactory.php');
				$factory =  GroupFactory::instance();
				$group   =  $factory->create(array($gID));
				return $group[0];	
			}
			return null;
		}
		
		/***
		 * Added by naldz (March 22, 2007)
		 */
		public static function isGroupAdvisor($groupID,$travelerID){
			if ( self::isLogin() ){
				require_once('travellog/model/Class.AdminGroup.php');
				return AdminGroup::isGroupAdvisor($travelerID,$groupID);
			}
			return false;
		}
		/***
		 * Added by naldz (March 22, 2007)
		 */
		public static function isAdvisor(){
			if ( self::isLogin() ){
				require_once('travellog/model/Class.Traveler.php');
				$iTraveler = new Traveler($_SESSION['travelerID']);
				return $iTraveler->isAdvisor();				
			}
			return false;
		}
		/***
		 * Added by naldz (March 22, 2007)
		 */
		public static function getAdvisorGroupID($travelerID){
			if ( self::isLogin() ){
				require_once('travellog/model/Class.AdminGroup.php');
				return AdminGroup::getAdvisorGroupID($travelerID);
			}
			return null;
		}
		
		/*
		 * Added by marc, Magy 28 2007
		 * dirLoc :: directory
		 * 	-> forums
		 */
		public static function getLoginPage( $dirLoc = "" ){
			//removed checking for forums (Sept 16, 2010)
			/***
			if( ereg("forums",$dirLoc) ){
				$dirLoc = ereg_replace("redirect=","forums/" . $_GET["redirect"].  "?",$_SERVER["QUERY_STRING"] );
				$dirLoc = ereg_replace("&sid=[0-9a-z]*","",$dirLoc);
				header("location: http://" . $_SERVER["SERVER_NAME"] . "/login.php?failedLogin&redirect=$dirLoc" );
				exit;
			}
			else
			***/
			header("location: http://" . $_SERVER["SERVER_NAME"] . "/login.php?failedLogin&redirect=" . $_SERVER["REQUEST_URI"] );
			exit;
		}
		
		public static function redirectLoginPage($url){
			return str_replace("^","&",$url);
		}
		
		public static function getEmailAddress() {
			if( self::isLogin() ){
				require_once('travellog/model/Class.Traveler.php');
				$nTraveler = new Traveler($_SESSION['travelerID']);
				$tmpUsername = $nTraveler->getEmail();
				unset($nTraveler);
				return $tmpUsername;				
			}
			return NULL;
		}

}
?>
