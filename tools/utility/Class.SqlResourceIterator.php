<?php
	/**
	 * @(#) Class.SqlResourceIterator.php
	 * 
	 * This iterator is applicable only to models that accepts array parameters to their constructors.
	 * Example:
	 * 		$result = mysql_query("SELECT * FROM tblUSUContactType");
	 * 		$iterator = new SqlResourceIterator($result, "ContactType");
	 * 
	 * 		while($iterator->hasNext()){
	 * 			var_dump($iterator->next());
	 * 		}
	 * You can also use this iterator to get the value of an existent field in the iterator.
	 * Example:
	 * 		$result = mysql_query("SELECT count(*) as count FROM tblUSUContactType");
	 * 		$iterator = new SqlResourceIterator($result);
	 * 		$iterator->count();
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - Jan 30, 2009
	 */
	 
	class SqlResourceIterator {
		private $resource = null;
		private $numRows  = 0;
		private $ptr = -1;
		private $class = null;
		private $path = "";
		private $data = array();
		
		/**
		 * The constructor of this class. Accepts only one parameter.
		 * 
		 * @throws exception
		 * @param resource $resource The result resource that is being evaluated. This result comes from a call to mysql_query().
		 * @param string $class The class name of the expected object that is returned by the next method. If this is not set the next method will return an array.
		 * @param string $path The absolute path of the class. The default path is goabroad/model/usu/.
		 */
		function __construct($resource, $class = null, $path = "travellog/model") {
			if (is_resource($resource) AND 'mysql result' == get_resource_type($resource)) {
				$this->resource = $resource;
				$this->numRows = mysql_num_rows($this->resource);
				$this->class = $class;
				$this->path = preg_replace("/\/$/", "", $path);
				$this->path .= "/"; 
			}
			else {
				throw new exception("Invalid resource passed to ObjectIterator");
			}
		}
		
		/**
		 * Returns true if the iteration has more elements.
		 * 
		 * @return boolean Returns true if the iteration has more elements.
		 */
		function hasNext(){
			return (($this->ptr + 1) == $this->numRows) ? false : true;
		}
		
		/**
		 * Returns the next element in the iteration. The element returned is an instance of the class passed
		 * in the constructor of this iterator. If the class is not set this returns an array. 
		 * 
		 * @throws exception
		 * @return array|Object Returns the next element in the iteration.
		 */
		function next(){
			if ($this->hasNext()) {
				$this->ptr++;
				if (!isset($this->data[$this->ptr])) {
					mysql_data_seek($this->resource, $this->ptr);
					$this->data[$this->ptr] = mysql_fetch_assoc($this->resource);
					if (!is_null($this->class)) {
						require_once($this->path."Class.".$this->class.".php");
						$obj = new ReflectionClass($this->class);
						$this->data[$this->ptr] = $obj->newInstanceArgs(array($this->data[$this->ptr]));				
					}
				}
				return $this->data[$this->ptr];
			}
			else {
				throw new exception("There are no next element in the iteration");
			}
		}
		
		/**
		 * Rewinds the iterator. This means that the call of next method after calling this method, will result 
		 * to the first element in the iterator.
		 * 
		 * @return void
		 */
		function rewind(){
			$this->ptr = -1;
		}
		
		/**
		 * Sets the path of the object created.
		 * 
		 * @param string $path
		 * @return void
		 */
		function setPath($path){
			$this->path = $path;
		}
		
		/**
		 * Returns the number of elements of the iterator.
		 * 
		 * @return integer
		 */
		function size(){
			return $this->numRows;
		}
		
		/**
		 * Returns the value of a field in the current row.
		 * Example:
		 *  	$query = mysql_query("SELECT a.name FROM tblGroup as a");
		 * 		$it = new SqlResourceIterator($query);
		 * 		$it->name();  // Returns the name of the first row
		 * 		$it->next();  
		 * 		$it->name();  // Returns the name of the first row
		 * 		$it->next();
		 * 		$it->name();  // Returns the name of the second row
		 * 
		 * @param string $field The field where to retrieve the value.
		 * @return mixed
		 */
		function __call($field, $arguments) {
			if (0 < $this->size()) {
				$index = (-1 == $this->ptr) ? 0 : $this->ptr;
				mysql_data_seek($this->resource, $index);
				$array = mysql_fetch_assoc($this->resource);
				
				if (isset($array[$field])) {
					return $array[$field];
				}
				else {
					throw new exception("Field name does not exist.");
				}
			}
			else {
				throw new exception("There are no element found in the iteration.");
			}			
		}
	}