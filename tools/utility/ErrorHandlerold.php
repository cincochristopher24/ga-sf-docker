<?php
 
function error_handler($error,$message,$file,$line, $vars)
{ 
    if($error & error_reporting())
    {
        $log=array();
        switch($error)
        {
            case E_ERROR:
                $type='FATAL';
                break;
            case E_WARNING:
                $type='ERROR';
                break;
            case E_NOTICE:
                $type='WARNING';
                break;
            default:
                $type='Unknown error type ['.$error.']';
                break;
        }
        
        $dt = date('M d, Y g:i:s A');  
       
        $err = "<errorentry>\n";  
        $err .= "\t<datetime>" .$dt. "</datetime>\n";
        $err .= "\t<errortype>" .$type. "</errortype>\n";  
        $err .= "\t<errormsg>" .htmlentities($message). "</errormsg>\n"; 
        $err .= "\t<scriptname>" .$file. "</scriptname>\n";  
        $err .= "\t<scriptlinenum>" .$line. "</scriptlinenum>\n";  
        
        
        if (isset($_SERVER['REMOTE_ADDR']) && strlen($_SERVER['REMOTE_ADDR']))	
        	$err .= "\t<remoteaddr>" .htmlentities($_SERVER['REMOTE_ADDR']). "</remoteaddr>\n";
        
        if (isset($_SERVER['HTTP_REFERER']) && strlen($_SERVER['HTTP_REFERER']))
        	$err .= "\t<referer>" .htmlentities($_SERVER['HTTP_REFERER']). "</referer>\n"; 
        
        if (isset($_SERVER['HTTP_USER_AGENT']) && strlen($_SERVER['HTTP_USER_AGENT']))
        	$err .= "\t<browser>" .htmlentities($_SERVER['HTTP_USER_AGENT']). "</browser>\n"; 
        	
        if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']))
        	$err .= "\t<querystring>" .htmlentities($_SERVER['QUERY_STRING']). "</querystring>\n"; 
        	
        if (isset($_SERVER['REQUEST_METHOD']) && strlen($_SERVER['REQUEST_METHOD']))
        	$err .= "\t<method>" .htmlentities($_SERVER['REQUEST_METHOD']). "</method>\n"; 
        	
        $err .= "</errorentry>\n\n";  
        
        $path = '/var/www/websites/sites/errorlogs/';
        $fullpath = $path .$_SERVER['SERVER_NAME'].'.log';
        
        if (!file_exists($path))
        	mkdir($path);
        
		error_log($err, 3, $fullpath);
    
    }
}


function exception_handler($e)
{ 
 
    $log=array();
    $type = 'EXCEPTION';
    
    $dt = date('M d, Y g:i:s A');  
   
    $err = "<errorentry>\n";  
    $err .= "\t<datetime>" .$dt. "</datetime>\n";
    $err .= "\t<errortype>" .$type. "</errortype>\n";  
    $err .= "\t<errormsg>" .htmlentities($e->getMessage()). "</errormsg>\n"; 
    $err .= "\t<scriptname>" .$e->getFile(). "</scriptname>\n";  
    $err .= "\t<scriptlinenum>" .$e->getLine(). "</scriptlinenum>\n";  
    
    
    if (isset($_SERVER['REMOTE_ADDR']) && strlen($_SERVER['REMOTE_ADDR']))	
    	$err .= "\t<remoteaddr>" .htmlentities($_SERVER['REMOTE_ADDR']). "</remoteaddr>\n";
    
    if (isset($_SERVER['HTTP_REFERER']) && strlen($_SERVER['HTTP_REFERER']))
    	$err .= "\t<referer>" .htmlentities($_SERVER['HTTP_REFERER']). "</referer>\n"; 
    
    if (isset($_SERVER['HTTP_USER_AGENT']) && strlen($_SERVER['HTTP_USER_AGENT']))
    	$err .= "\t<browser>" .htmlentities($_SERVER['HTTP_USER_AGENT']). "</browser>\n"; 
    	
    if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']))
    	$err .= "\t<querystring>" .htmlentities($_SERVER['QUERY_STRING']). "</querystring>\n"; 
    	
    if (isset($_SERVER['REQUEST_METHOD']) && strlen($_SERVER['REQUEST_METHOD']))
    	$err .= "\t<method>" .htmlentities($_SERVER['REQUEST_METHOD']). "</method>\n"; 
    	
    if (strlen($e->getTraceAsString()))
    	$err .= "\t<stacktrace>" .htmlentities($e->getTraceAsString()). "</stacktrace>\n"; 
    	
    $err .= "</errorentry>\n\n";  
    
    $path = '/var/www/websites/sites/errorlogs/';
    $fullpath = $path .$_SERVER['SERVER_NAME'].'.log';
    
    if (!file_exists($path))
    	mkdir($path);
    
	error_log($err, 3, $fullpath);
 
}

set_error_handler('error_handler');

set_exception_handler('exception_handler');


?>