<?php

class CookieManager{
	
	private static $instance = NULL;
	
	private function __construct(){
	}
	
	public function getInstance(){
		if( is_null(self::$instance) ){
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	public function set($name=NULL,$value=NULL,$expire=0,$path="/",$domain=".goabroad.net",$secure=FALSE,$httponly=FALSE){
		if( is_null($name) ){
			throw new Exception("Cookie name expected!");
		}
		setcookie($name,$value,$expire,$path,$domain,$secure,$httponly);
	}
	
	public function get($name=NULL){
		if( is_null($name) ){
			throw new Exception("Cookie name expected!");
		}
		return isset($_COOKIE[$name]) ? $_COOKIE[$name] : FALSE;
	}
	
	public function clear($name=NULL,$path="/",$domain=".goabroad.net"){
		if( is_null($name) ){
			return;
		}
		if( isset($_COOKIE[$name]) ){
			unset($_COOKIE[$name]);
			setcookie($name,"",time()-(3600*25),$path,$domain);
		}
	}
}