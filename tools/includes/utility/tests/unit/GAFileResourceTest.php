<?php
/**
 * Created on Jun 24, 2007
 * 
 * @author     Wayne Duran
 */

require_once('Class.GAFileResource.php');


class GAFileResourceA extends GAFileResource {
	private static $instance;
	
	public static function instance()
 	{
 		if (self::$instance == NULL ) {
 			self::$instance = new self();
 		}
 		
 		return self::$instance;	
 	}
}

class GAFileResourceB extends GAFileResource {
	private static $instance;
	
	public static function instance()
 	{
 		if (self::$instance == NULL ) {
 			self::$instance = new self();
 		}
 		
 		return self::$instance;	
 	}
}

class GAFileResourceTest extends PHPUnit_Framework_TestCase {
	
	protected $GFR;
	protected $class_name = 'GAFileResource';
	protected $resource_id = 'File';
	
	protected function setUp() {
		$class = new ReflectionClass($this->class_name);
		$instance_method = $class->getMethod('instance');
		$this->GFR = $instance_method->invoke(NULL); 
	}
	
	protected function tearDown() {
		$this->GFR->flushLookUpPathList();
	}
	
	public function testSingleton() {
		$b = GAFileResource::instance();
		$a = GAFileResource::instance();
		
		$this->assertSame($b, $a, 'Did not pass singleton test');
		$this->assertSame($b, $this->GFR, 'Did not pass singleton test');
	}
		
	
	public function testGetId() {
		$this->assertEquals($this->resource_id, $this->GFR->getIdentifier(), 'Wrong ID passed');
	}
	
	
	public function testGetLookUpPathReturnsAnArray() {
		$this->assertTrue(is_array($this->GFR->getLookUpPaths()), 'getLookUpPath did not return an array');
		$this->assertEquals(0, count($this->GFR->getLookUpPaths()), 'Initial value returns non-zero length');
	}
	
	
	public function testAddLookUpPath() {
		$this->assertEquals(0, count($this->GFR->getLookUpPaths()));
		$this->GFR->addLookUpPath('/');
		$this->GFR->addLookUpPath('test/');
		$this->GFR->addLookUpPath('anothertest/');
		
		$arrtest = $this->GFR->getLookUpPaths();
		$this->assertEquals(3, count($arrtest), 'Wrong number of array length');
		
		$this->assertEquals('/', $arrtest[0], 'Unable to add path');
		
	}
	
	
	public function testGetFileName() {
		$this->assertEquals('file', $this->GFR->getFileName('file'), 'Unable to return expected file');
	}
	
	public function testFluentInterfaceInAddingPaths() {
		$this->assertEquals('GAFileResource', 
							get_class(GAFileResource::instance()
										->withPaths(
													'path2/path3/',
													'path2/',
													'path1/')),
							'withPaths did not return an instance of GAResourceFile');
		
		$arrtest = $this->GFR->getLookUpPaths();
		$this->assertEquals('path1/', $arrtest[0], 'Unable to properly add path. Unexpected value for first element in lookup paths');
		$this->assertEquals('path2/path3/', $arrtest[2], 'Unable to properly add path. Unexpected value for first element in lookup paths');
		$this->assertEquals(3, count($arrtest), 'Wrong Number of lookup paths');
	}
		
	// $this->markTestIncomplete('Test not implemented yet');
	
	public function testSingletonInDescendants() {
		
		$A = GAFileResourceA::instance();
		$B = GAFileResourceB::instance();
		
		$this->assertTrue($A instanceof GAFileResourceA, 'Test object $A is not a GAFileResourceA');
		$this->assertTrue($B instanceof GAFileResourceB, 'Test object $B is not a GAFileResourceB');
		$this->assertNotEquals($A, $B, 'Test objects $A and $B are equal!');
		$this->assertNotSame($A, $B,  'Test objects $A and $B are the same!');
	}
} 
?>
