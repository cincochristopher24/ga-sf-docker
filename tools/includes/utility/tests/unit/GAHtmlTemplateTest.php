<?php
/**
 * Created on Jul 3, 2007
 * 
 * @author     Wayne Duran
 */

require_once 'Class.GAHtmlTemplate.php';
require_once 'GATemplateTest.php';
require_once 'Class.GAFile.php';



class TT extends GAHtmlTemplate {
	public static function clearDependents() {
		parent::clearDependents();
	}
	
	public static function resetSettings() {
		parent::clearDependents();
		self::$forceNotToDisplayLog = NULL;
	}
} 


class GAHtmlTemplateTest extends PHPUnit_Framework_TestCase {
	
    protected $classname;
    protected $cleanUpList = array();
	protected $defaults = array();
	
    public function __construct() {
    	parent::__construct();
    	$this->classname = 'GAHtmlTemplate';
    }
    
    protected function setUp() {
    	if (array_key_exists('minifiy', $this->defaults)) {
			GAHtmlTemplate::setMinify($this->defaults['minify']);
		}
		T::$main_template_name = 'temp/main.php';
		T::$main_template_contents = '<html>
			<head>
			<title><?=$title?></title>
			</head>
			<body>
			<h1>Main Template Here</h1>
			<?=$contents ?>
			</body>
			</html>' ;
		T::$included_tempalate_name = 'temp/inc.php';
		T::$included_template_contents = '
			<h2>This is in included template</h2>
			<p><?= $var ?></p>
			<p><strong><?= $this[\'var2\'] ?></strong></p>
			';
    	$tplrf = new ReflectionClass($this->classname);
    	$this->T = $tplrf->newInstance();	

    	if (!file_exists('temp')) {
    		mkdir('temp');	
    	} else {
    		// Empty temp
    		$dir = opendir('temp');
    		while($file = readdir($dir)) {
    			if ($file !== '.' && $file !== '..') @ unlink('temp/'.$file);
    		} 
    	}

    	// Create Test Template Files
    	$this->tpl_main = $this->createTestFile(T::$main_template_name,T::$main_template_contents);

		$this->tpl_inc = $this->createTestFile(T::$included_tempalate_name,T::$included_template_contents);
		
    	GAHtmlTemplate::setDefaultJsPath('');
    	GAHtmlTemplate::setDefaultCssPath('');
		$this->defaults['minify'] = GAHtmlTemplate::isMinify();
    }
    
    protected function tearDown() {
    	$this->T = NULL;
    	
    	foreach($this->cleanUpList as $f) {
    		$f->delete();
    	}
    	
    	@ rmdir('temp');
    	GATemplate::clearHelperRegistry();
    	
    	TT::resetSettings();
    	GAHtmlTemplate::displayLog(FALSE);

		GAHtmlTemplate::setMinify($this->defaults['minify']);
    }

	protected function hasString($haystack, $needle) {
    	if(strpos($haystack, $needle) > -1) {
    		return true;
    	} else {
    		return false;
    	}
    }
    
    protected function stringInBetween($needle, $haystack, $prefix = '', $suffix = '') {
    	//return preg_match
    }
    
    protected function createTestFile($name, $content) {
		$f = GAFile::create($name)
					->write($content)
					->save();
		$this->cleanUpList[] = $f;
		return $f;
    }
	
    
    protected function standardItems($var = 'Lorem Ipsum', $var2 = 'Shake it baby now!', $title = 'Main Title') {
    	GAHtmlTemplate::setMainTemplate(T::$main_template_name );
    	GAHtmlTemplate::setMainTemplateVar('title', $title);
    	
    	$this->T['var']  = $var;
    	$this->T['var2'] = $var2;
    }
    
    protected function out() {    	
    	ob_start();
	    	$this->T->out(T::$included_tempalate_name);
	    	$haystack = ob_get_contents();
    	ob_end_clean();
    	return $haystack;
    }
    
    
    public function testIncludingMainTemplate() {
    	$this->standardItems();
    	$haystack = $this->out();
    	
    	$this->assertTrue(strpos($haystack, '<html>') === 0, 'Unable to include main template in output');
    	$this->assertTrue(strrpos($haystack, '</html>') > 40, 'Unable to include main template properly in output');
    	$this->assertTrue($this->hasString($haystack, '<title>Main Title</title>'), 'Unable to set variable in main template');
    	$this->assertTrue($this->hasString($haystack, '<p>Lorem Ipsum</p>'), 'Unable to set variable in included template');
    	$this->assertTrue($this->hasString($haystack, '<p><strong>Shake it baby now!</strong></p>'), 'Unable to set variable in included template');
    }
    
    
    public function testNotIncludingMainTemplate() {
    	$this->standardItems();
    	$this->T->useMainTemplate(FALSE);
    	$haystack = $this->out();
		
    	$this->assertFalse(strpos($haystack, '<html>') === 0, 'Including main template in output');
    	$this->assertFalse(strrpos($haystack, '</html>') > 40, 'Including main template properly in output');
    	$this->assertFalse($this->hasString($haystack, '<title>Main Title</title>'), 'Title variable in Main Template was set');
    	$this->assertTrue($this->hasString($haystack, '<p>Lorem Ipsum</p>'), 'Variable was set properly');
    	$this->assertTrue($this->hasString($haystack, '<p><strong>Shake it baby now!</strong></p>'), 'Variable was set properly i n');
    }
    
    public function testIncludingInHead() {
    	$this->standardItems();
    	GAHtmlTemplate::includeDependent('<script type="text/javascript"></script>');
    	
    	$haystack = $this->out();
    	
    	$this->assertTrue(strpos($haystack, '<html>') === 0, 'Unable to include main template in output');
    	$this->assertTrue($this->hasString($haystack, '<script type="text/javascript"></script>'."\n</head>"), 'Unable to include snippet in head');
    }
	

	public function testIncludingAtTheEndOfTheBody() {
		$this->standardItems();
    	GAHtmlTemplate::includeDependent('<script type="text/javascript"></script>', array('bottom'=>TRUE));
    	
    	$haystack = $this->out();
    	
    	$this->assertTrue($this->hasString($haystack, '<script type="text/javascript"></script>'."\n</body>"), 'Unable to include snippet before ending body tag');
	}
	
    public function testLogging() {
    	$this->standardItems();
    	GAHtmlTemplate::displayLog();
    	
    	$haystack = $this->out();
    	$this->assertTrue($this->hasString($haystack, '<div id="Template_Class_Log"><h1>Template Log</h1>'), 'Unable to display log. Test to look for opening code fails');
		$this->assertTrue($this->hasString($haystack, 'html body #Template_Class_Log {'), 'Unable to write styling snippet in head area');
		$this->assertTrue($this->hasString($haystack, "</style>\n</head>"), 'Unable to write styling snippet in head area');
    	$this->assertTrue($this->hasString($haystack, 'var2'), 'Test string label for a variable not found');
    	$this->assertTrue($this->hasString($haystack, '(string)'),'Test string label for the type of variable');
    	$this->assertTrue($this->hasString($haystack, T::$included_tempalate_name),'Test displaying of templates in log failed');
    	// $this->assertTrue(preg_match('/Template_Class_Log.+Lorem Ipsum.+/', $haystack) > 0, 'Test for displaying values in template log');
    }
    
    public function testDisablingLoggingInInstance() {
    	$this->standardItems();
    	GAHtmlTemplate::displayLog();
    	$this->T->disableLogging();
    	$haystack = $this->out();
    	$this->assertFalse($this->hasString($haystack, '<div id="Template_Class_Log"><h1>Template Log</h1>'), 'Not expecting template log here');
    	$this->assertFalse($this->hasString($haystack, 'html body #Template_Class_Log {'), 'Not expecting log styling here');
    }
	
	public function testDoNotUseMainTemplate() {
		$this->standardItems();
		$this->T->doNotUseMainTemplate();
		$haystack = $this->out();
		$this->assertFalse($this->hasString($haystack, '<html>'), 'Text should not appear here.');
	}
    
    public function testIncludingJs() {
    	$this->standardItems();
    	
    	GAHtmlTemplate::includeDependentJs('/script/javascript.js');
    	$haystack = $this->out();
    	$this->assertTrue($this->hasString($haystack, '<script src="/script/javascript.js" type="text/javascript"></script>'."\n</head>"), 'Javascript snippet not found in head');
	}
	
 	public function testIncludingJsWithOptionsDeferSetToTrueSetsAttributeDefer() {
    	$this->standardItems();
    	GAHtmlTemplate::includeDependentJs('/script/javascript.js', array('defer'=> TRUE));
    	$haystack = $this->out();
    	$this->assertTrue($this->hasString($haystack, '<script src="/script/javascript.js" type="text/javascript" defer="defer"></script>'."\n</head>"), 'Javascript snippet not found in head');
    }

	public function testIncludingJsWithDefaultJsPathSet() {
    	$this->standardItems();
    	GAHtmlTemplate::setDefaultJsPath('/default/');
    	GAHtmlTemplate::includeDependentJs('script/javascript.js', array('defer'=> TRUE));
    	$haystack = $this->out();
    	$this->assertTrue($this->hasString($haystack, '<script src="/default/script/javascript.js" type="text/javascript" defer="defer"></script>'."\n</head>"), 'Javascript snippet not found in head');
    }
	
	public function testIncludingJsWithOptionBottomSetToTrueIncludesThatJsBeforeTheEndingBodyTagInsteadOfBeforeEndingHeadTag()
	{
		$this->standardItems();
		GAHtmlTemplate::includeDependentJs('/script/javascript.js', array('bottom'=> TRUE));
    	$haystack = $this->out();
    	$this->assertFalse(
			$this->hasString(
				$haystack,
				'<script src="/script/javascript.js" type="text/javascript"></script>'."\n</head>"
			),
			'Javascript snippet MUST NOT be found in head'
		);
		$this->assertTrue(
			$this->hasString(
				$haystack,
				'<script src="/script/javascript.js" type="text/javascript"></script>'."\n</body>"
			),
			'Javascript snippet must be found before the ending body tag'
		);
	}
	
	public function testIncludingJsMultipleTimesButInHeadWillOnlyIncludeOnce()
	{
		$this->standardItems();
		GAHtmlTemplate::includeDependentJs('/script/javascript.js');
		GAHtmlTemplate::includeDependentJs('/script/javascript.js');
    	$haystack = $this->out();
		$this->assertEquals(1,
			substr_count($haystack, '<script src="/script/javascript.js" type="text/javascript"></script>'),
			'Javascript snippet must not be included more than once'
		);
	}
	
	public function testIncludingJsMultipleTimesButInBodyWillOnlyIncludeOnce()
	{
		$this->standardItems();
		GAHtmlTemplate::includeDependentJs('/script/javascript.js', array('bottom'=> TRUE));
		GAHtmlTemplate::includeDependentJs('/script/javascript.js', array('bottom'=> TRUE));
    	$haystack = $this->out();
		$this->assertEquals(1,
			substr_count($haystack, '<script src="/script/javascript.js" type="text/javascript"></script>'),
			'Javascript snippet must not be included more than once'
		);
	}
	
	
	public function testIncludingJsMultipleTimesButInBodyAndHeadWillYieldToOnlyIncludeOnce()
	{
		$this->standardItems();
		GAHtmlTemplate::includeDependentJs('/script/javascript.js');
		GAHtmlTemplate::includeDependentJs('/script/javascript.js', array('bottom'=> TRUE));
    	$haystack = $this->out();
		$this->assertEquals(1,
			substr_count($haystack, '<script src="/script/javascript.js" type="text/javascript"></script>'),
			'Javascript snippet must not be included more than once'
		);
    	$this->assertTrue(
			$this->hasString(
				$haystack,
				'<script src="/script/javascript.js" type="text/javascript"></script>'."\n</head>"
			),
			'Javascript snippet MUST be found in head'
		);
		$this->assertFalse(
			$this->hasString(
				$haystack,
				'<script src="/script/javascript.js" type="text/javascript"></script>'."\n</body>"
			),
			'Javascript snippet MUST NOT be found before the ending body tag'
		);
	}
	
	public function testIncludingJsMultipleTimesButInBodyAndHeadWillYieldToIncludingInHead()
	{
		$this->standardItems();
		GAHtmlTemplate::includeDependentJs('/script/javascript.js', array('bottom'=> TRUE));
		GAHtmlTemplate::includeDependentJs('/script/javascript.js');
    	$haystack = $this->out();
		$this->assertEquals(1,
			substr_count($haystack, '<script src="/script/javascript.js" type="text/javascript"></script>'),
			'Javascript snippet must not be included more than once'
		);
    	$this->assertTrue(
			$this->hasString(
				$haystack,
				'<script src="/script/javascript.js" type="text/javascript"></script>'."\n</head>"
			),
			'Javascript snippet MUST be found in head'
		);
		$this->assertFalse(
			$this->hasString(
				$haystack,
				'<script src="/script/javascript.js" type="text/javascript"></script>'."\n</body>"
			),
			'Javascript snippet MUST NOT be found before the ending body tag'
		);
	}
    
    public function testIncludingCss() {
    	$this->standardItems();
    	
    	GAHtmlTemplate::includeDependentCss('/css/stylesheet.css');
    	$haystack = $this->out();
    	$this->assertTrue($this->hasString($haystack, '<link href="/css/stylesheet.css" media="screen" rel="stylesheet" type="text/css" />'."\n</head>"), 'CSS snippet not found in head');
    }
	
	public function testIncludingCssWithMediaAural()
	{
		$this->standardItems();
    	GAHtmlTemplate::includeDependentCss('/css/stylesheet.css', array('media'=> 'aural'));
    	$haystack = $this->out();
    	$this->assertTrue($this->hasString($haystack, '<link href="/css/stylesheet.css" media="aural" rel="stylesheet" type="text/css" />'."\n</head>"), 'CSS snippet with media="aural" was not found.');
	}
    
    public function testIncludingCssUsingAtImportStyle()
    {
    	$this->standardItems();
    	GAHtmlTemplate::includeDependentCss('/css/stylesheet.css', array('@import'=> true));
    	$haystack = $this->out();
    	$this->assertTrue($this->hasString($haystack, '<style type="text/css" media="screen">@import url("/css/stylesheet.css");</style>'."\n</head>"), 'CSS snippet using @import style was not found in head');
    }
    
	public function testIncludingCssUsingAtImportStyleWithMediaAural()
	{
		$this->standardItems();
		GAHtmlTemplate::includeDependentCss('/css/stylesheet.css', array('@import'=> true, 'media' => 'aural'));
    	$haystack = $this->out();
    	$this->assertTrue($this->hasString($haystack, '<style type="text/css" media="aural">@import url("/css/stylesheet.css");</style>'."\n</head>"), 'CSS snippet using @import style with media="aural" was not found in head');
	}
    
    public function testMultipleIncludes() {
    	$this->standardItems();
    	
    	GAHtmlTemplate::includeDependentCss('/css/stylesheet.css');
    	GAHtmlTemplate::includeDependentJs('/script/javascript.js');
    	GAHtmlTemplate::includeDependentJs('/script/anotherjavascript.js', array('defer'=> TRUE));
    	
    	$haystack = $this->out();
    	$this->assertTrue($this->hasString($haystack, '<script src="/script/javascript.js" type="text/javascript"></script>'."\n"), 'Javascript snippet not found in head');
    	$this->assertTrue($this->hasString($haystack, '<link href="/css/stylesheet.css" media="screen" rel="stylesheet" type="text/css" />'."\n"), 'CSS snippet not found in head');
    	$this->assertTrue($this->hasString($haystack, '<script src="/script/anotherjavascript.js" type="text/javascript" defer="defer"></script>'."\n"), 'Javascript snippet not found in head');
    	
    }
    
    public function testMultipleOuts() {
    	$this->tpl_inc->write('
			<h2>This is in included templates</h2>
			<p><?= $var->out() ?></p>
			<p><strong><?= $this[\'var2\'] ?></strong></p>
			')
			->save();
		
		
    	$this->createTestFile('temp/inc2.php', '<span><?= $v ?></span>');
    	$this->createTestFile('temp/inc3.php', '<code><?= $x ?></code><? $y->out()?>');
    	$this->createTestFile('temp/inc4.php', 'Nawala!');
    	
    	
    	$tpl1 = new GAHtmlTemplate();
    	$tpl1->setTemplate('temp/inc2.php');
    	$tpl1['v'] = 'FAT';
    	//$this->assertEquals('', $this->out());
    	
    	$tpl3 = new GAHtmlTemplate();
    	$tpl3->setTemplate('temp/inc4.php');
    	
    	$tpl2 = new GAHtmlTemplate();
    	$tpl2['x'] = 'BAT';
    	$tpl2['y'] = $tpl3;
    	$tpl2->setTemplate('temp/inc3.php');
    	
    	$this->standardItems($tpl1,$tpl2);
    	
    	$haystack = $this->out();
    	$this->assertTrue($this->hasString($haystack, '<p><span>FAT</span></p>'), 'Preliminary test for properly including template inside template failed');
    	$this->assertTrue($this->hasString($haystack, '<p><strong><code>BAT</code>Nawala!'), 'Secondary test for properly including template inside template failed');
    	$this->assertFalse(strpos($haystack, '<html>', 5) > 5, 'Multiple occurrence of main template code in output found (<html>)');
    	//$this->assertFalse(strpos($haystack, '</html>', -5), 'Multiple occurrence of main template code in output found (</html>)');
    	
    }
	
	public function testGettingDefaultMinifySetting()
	{
		$this->assertFalse(GAHtmlTemplate::isMinify(), 'Default setting for minify must be false');
	}
	
	public function testSettingMinifySetting()
	{
		GAHtmlTemplate::setMinify(true);
		$this->assertTrue(GAHtmlTemplate::isMinify(), 'Setting failed for isMinify');
	}
	
	public function testIncludingJsFileWillReturnTheMinifiedPathWhenMinifySettingisTrue()
	{
		$this->standardItems();
		GAHtmlTemplate::setMinify(true);
    	
    	GAHtmlTemplate::includeDependentJs('/script/javascript.js');
    	$haystack = $this->out();
    	$this->assertTrue($this->hasString($haystack, '<script src="/minify_f.php?files=/script/javascript.js" type="text/javascript"></script>'."\n</head>"), 'Minified Javascript snippet not found in head');
	}
	
	public function testIncludingJsFileWillReturnThePathNormallyWhenMinifySettingisFalse()
	{
		$this->standardItems();
		GAHtmlTemplate::setMinify(false);
    	
    	GAHtmlTemplate::includeDependentJs('/script/javascript.js');
    	$haystack = $this->out();
    	$this->assertTrue($this->hasString($haystack, '<script src="/script/javascript.js" type="text/javascript"></script>'."\n</head>"), 'Javascript snippet not found in head');
	}
	
	public function testIncludingMultipleJsFilesWhenMinifyIsFalseWillNotMinifyAllFiles()
	{
		$this->standardItems();
		GAHtmlTemplate::setMinify(false);
    	
    	GAHtmlTemplate::includeDependentJs(array('/script/javascript.js', '/script/prototype.js', 'js/test.js'));
    	$haystack = $this->out();
    	$this->assertTrue($this->hasString($haystack, '<script src="/script/javascript.js" type="text/javascript"></script>'."\n".
													  '<script src="/script/prototype.js" type="text/javascript"></script>'."\n".
													  '<script src="js/test.js" type="text/javascript"></script>'."\n</head>"), 'Javascript snippet not found in head');
	}
	
	public function testIncludingMultipleJsFilesWhenMinifyIsTrueWillMinifyAllFiles()
	{
		$this->standardItems();
		GAHtmlTemplate::setMinify(true);
    	
    	GAHtmlTemplate::includeDependentJs(array('/script/javascript.js', '/script/prototype.js', 'js/test.js'));
    	$haystack = $this->out();
    	$this->assertTrue($this->hasString($haystack, '<script src="/minify_f.php?files=/script/javascript.js,/script/prototype.js,js/test.js" type="text/javascript"></script>'."\n</head>"), 'Javascript snippet not found in head');
	}
	
	public function testIncludingJsWithMinifyOnAndSrcHasUrlParams()
	{
		$this->standardItems();
		GAHtmlTemplate::setMinify(true);
        
		GAHtmlTemplate::includeDependentJs('/script/javascript.js?param1=val1&param2=val2');
        $haystack = $this->out();
		$this->assertContains('<script src="/minify_f.php?files=/script/javascript.js&amp;param1=val1&amp;param2=val2" type="text/javascript"></script>'."\n</head>", $haystack, 'Javascript snippet not found in head');
	}
	/*
	public function testIncludingMultipleJsFilesWhenMinifyIsTrueWillMinifyAllFilesWithUrlParams()
	{
		$this->standardItems();
		GAHtmlTemplate::setMinify(true);

    	GAHtmlTemplate::includeDependentJs(array('/script/javascript.js?param1=val1&param2=val2', '/script/prototype.js?param3=val3', 'js/test.js'));
    	$haystack = $this->out();
    	$this->assertTrue($this->hasString($haystack, '<script src="/minify_f.php?files=/script/javascript.js,/script/prototype.js,js/test.js&amp;param1=val1&amp;param2=val2&amp;param3=val3" type="text/javascript"></script>'."\n</head>"), 'Javascript snippet not found in head');
	}
	public function testIncludingMultipleJsFilesWhenMinifyIsTrueWillMinifyAllFilesWithUrlParamsAsString()
	{
		GAHtmlTemplate::setMinify(true);

    	$test = GAHtmlTemplate::includeDependentJs(array('/script/javascript.js?param1=val1&param2=val2', '/script/prototype.js?param3=val3', 'js/test.js'), array('return_string' => true));
    	$this->assertEquals('<script src="/minify_f.php?files=/script/javascript.js,/script/prototype.js,js/test.js&amp;param1=val1&amp;param2=val2&amp;param3=val3" type="text/javascript"></script>', $test, 'Javascript string not returned');
	}
	*/
	
	public function testTurningOffMinifyByOptions()
	{
		$this->standardItems();
		GAHtmlTemplate::setMinify(true);

    	GAHtmlTemplate::includeDependentJs('/script/javascript.js', array('minify' => false));
    	$haystack = $this->out();
    	$this->assertTrue($this->hasString($haystack, '<script src="/script/javascript.js" type="text/javascript"></script>'."\n</head>"), 'Javascript snippet not found in head');
	}
	
	public function testIncludingDirectlyWithoutCompiling()
	{
		$this->tpl_main->setContent(
			'<html><head><title><?=$title?></title>
			<?php
				GAHtmlTemplate::includeDependentJs("/js/hownice.js", array("include_here" => true));
			?><!-- TEST -->
			</head><body><?=$contents ?></body></html>'
		);
		$this->tpl_main->save();
		
		$this->standardItems();
		$haystack = $this->out();
		$this->assertTrue($this->hasString($haystack, '<script src="/js/hownice.js" type="text/javascript"></script>'."<!--"), 'Javascript snippet not found in head');
	}
	
	public function testReturningIncludedJsAsString()
	{
		$string = GAHtmlTemplate::includeDependentJs('/js/this.js', array('return_string' => true));
		$this->assertEquals('<script src="/js/this.js" type="text/javascript"></script>', $string, 'Javascript string not returned');
	}
	
	public function testGettingIfJsIsIncludedAlready()
	{
		GAHtmlTemplate::includeDependentJs('/test/javascript.js');
		$this->assertTrue(GAHtmlTemplate::isIncluded('/test/javascript.js'), 'GAHtmlTemplate did not say that the file was included already');
	}
	
	public function testGettingIfJsIsIncludedAlreadyWhenJsIsNotIncluded()
	{
		$this->assertFalse(GAHtmlTemplate::isIncluded('/test/some-not-javascript.js'), 'GAHtmlTemplate did not say that the file was included already');
	}
	
	public function testGettingIfJsIsIncludedWhenJsIsIncludedInBottom()
	{
		GAHtmlTemplate::includeDependentJs('/test/javascript.js', array('bottom' => true));
		$this->assertTrue(GAHtmlTemplate::isIncluded('/test/javascript.js'), 'GAHtmlTemplate did not say that the file was included already');
	}
	
	public function testGettingIfJsIsIncludedAlreadyForMultipleJs()
	{
		$files = array('/1.js', '2.js', '/test/3.js', '4.js');
		GAHtmlTemplate::includeDependentJs($files);
		foreach ($files as $file) {
			$this->assertTrue(GAHtmlTemplate::isIncluded($file), 'GAHtmlTemplate did not say that the file "'. $file .'" was included already');
		}
	}
	
	public function testGettingIfJsIsIncludedAlreadyForMultipleJsWithMinify()
	{
		GAHtmlTemplate::setMinify(true);
		$files = array('/1.js', '2.js', '/test/3.js', '4.js');
		GAHtmlTemplate::includeDependentJs($files);
		foreach ($files as $file) {
			$this->assertTrue(GAHtmlTemplate::isIncluded($file), 'GAHtmlTemplate did not say that the file "'. $file .'" was included already');
		}
	}
	
	public function testReturningIncludedJsAsStringForMultipleJs()
	{
		$string = GAHtmlTemplate::includeDependentJs(array('/js/this.js', '/js/another.js'), array('return_string' => true));
		$this->assertEquals('<script src="/js/this.js" type="text/javascript"></script>'."\n".'<script src="/js/another.js" type="text/javascript"></script>', $string, 'Javascript string not returned');
	}
	
	public function testReturningIncludedJsAsStringWithMinify()
	{
		GAHtmlTemplate::setMinify(true);
		$string = GAHtmlTemplate::includeDependentJs('/js/this.js', array('return_string' => true));
		$this->assertEquals('<script src="/minify_f.php?files=/js/this.js" type="text/javascript"></script>', $string, 'Javascript string not returned');
	}
	
	public function testReturningIncludedJsAsStringForMultipleJsWithMinify()
	{
		GAHtmlTemplate::setMinify(true);
		$string = GAHtmlTemplate::includeDependentJs(array('/js/this.js', '/js/another.js'), array('return_string' => true));
		$this->assertEquals('<script src="/minify_f.php?files=/js/this.js,/js/another.js" type="text/javascript"></script>', $string, 'Javascript string not returned');
	}
	
	public function testSettingWhereToStoreMinifiedFiles()
	{
		$this->standardItems();
		GAHtmlTemplate::setMinify(true);
		$testpath = '/this/is/just/a/normal/path';
		GAHtmlTemplate::setMinifyCacheDir($testpath);
		$this->assertEquals($testpath, GAHtmlTemplate::getMinifyCacheDir(), 'Unable to get the minifiy cache directory');
	}
	
	public function testGettingMainTemplateVar()
	{
		$test = 'Yadayada';
		GAHtmlTemplate::setMainTemplateVar('title', $test);
		$this->assertEquals($test, GAHtmlTemplate::getMainTemplateVar('title'), 'Unable to obtain template var');
	}
	
	public function testGettingMainTemplateVarThatIsNotSet()
	{
		$this->assertEquals(null, GAHtmlTemplate::getMainTemplateVar('unkownvar'), 'Unable to obtain template var');
	}
	/*
	public function testIncludingDependentSnippetTwiceWillNotReincludeItAgain()
	{
		$this->standardItems();
		$snippet = '<script type="text/javascript/">alert("Hello World");</script>';
		$dummy = '<script type="text/javascript/">alert("This is a test");</script>';
		GAHtmlTemplate::includeDependent($snippet);
		GAHtmlTemplate::includeDependent($dummy);
		GAHtmlTemplate::includeDependent($snippet);
		$haystack = $this->out();
		//echo $haystack;
		$this->assertFalse($this->hasString($haystack, $snippet."\n".$dummy."\n".$snippet."\n".'</head>'), 'Unexpected text. Snippet was reincluded');
		$this->assertTrue($this->hasString($haystack, $snippet."\n".$dummy."\n".'</head>'), 'Unexpected text. Snippet Was probably reincluded');
	}*/
	
	public function testNotGettingDuplicatesForJsWithDifferentCallingOptions()
	{
		$this->standardItems();
		GAHtmlTemplate::setMinify(true);
		GAHtmlTemplate::includeDependentJs('/js/this.js');
		GAHtmlTemplate::includeDependentJs(array('/js/this.js', '/js/another.js'));
		$haystack = $this->out();
		$this->assertEquals(1, substr_count($haystack, '/js/this.js'), 'Instance of javascript should only occur once');
	}
	
	public function testNotGettingDuplicatesForJsWithDifferentCallingOptionsMultiFirst()
	{
		$this->standardItems();
		GAHtmlTemplate::setMinify(true);
		GAHtmlTemplate::includeDependentJs(array('/js/this.js', '/js/another.js'));
		GAHtmlTemplate::includeDependentJs('/js/this.js');
		$haystack = $this->out();
		$this->assertEquals(1, substr_count($haystack, '/js/this.js'), 'Instance of javascript should only occur once');
	}
}
?>
