<?php
/**
 * Created on Jul 29, 2007
 * 
 * @author     Wayne Duran
 */

require_once 'Class.GATemplateVariableIterator.php';

class GATemplateVariableIteratorTest extends PHPUnit_Framework_TestCase {
	
	protected function setUp() {
		
	}
	
	
	protected function tearDown() {
		
	}
	
	
	function testNumericallyIndexedArray() {
		$arr = array('zero', 'one', 'two', 'three', 'four');
		$iterator = new GATemplateVariableIterator($arr);
		
		$i = 0;
		foreach($iterator as $item) {
			$this->assertEquals($arr[$i], $item, 'Element seeked by numeric index did not match');
			$i++;
		}
		
		$this->assertEquals(count($arr), count($iterator), 'Element numbers did not match');
	}
	
	
	function testAssociativeArray() {
		$arr = array('zero'  => 'v_zero',
					 'one'   => 'v_one',
					 'two'   => 'v_two',
					 'three' => 'v_three',
					 'four'  => 'v_four');
		
		$iterator = new GATemplateVariableIterator($arr);$i = 0;
		$keys = array_keys($arr);
		
		foreach ($iterator as $item) {
			$this->assertEquals($arr[$i], $item, 'Element seeked by numeric index did not match');
			$i++;
		}
		
		foreach ($keys as $key) {
			$this->assertEquals($arr[$key], $iterator[$key], 'Element seeked by key did not match');
		}
		
		$this->assertEquals(count($arr), count($iterator), 'Element numbers did not match');
	}
}

?>
