<?php
/**
 * Created on Jul 18, 2007
 * 
 * @author     Wayne Duran
 */

require_once 'Class.GATemplateOutput.php';

class GATemplateOutputTest extends PHPUnit_Framework_TestCase {
	
	protected function setUp() {
		
	}
	
	protected function tearDown() {
		
	}
	
	public function testGettingParent() {
		$teststr  = 'Template Output Should solve our problems, right?';
		$teststr2 = 'The quick brown fox jumps over the lazy dog';
		$teststr3 = 'Lorem ipsum dolor sikmet amet';
		
		$tplout  = new GATemplateOutput($teststr);
		$tplout2 = new GATemplateOutput($teststr2);
		$tplout2->setParent($tplout);
		$tplout3 = new GATEmplateOutput($teststr3, $tplout);
		
		$this->assertSame($tplout, $tplout2->getParent(), 'Parent for $tplout2 was not detected');
		$this->assertNotSame($tplout2, $tplout3->getParent(), 'Parent object is wrong');
		$this->assertSame($tplout, $tplout3->getParent(), 'Parent object is wrong');
	}
	
	public function testSetContent() {
		$tplout = new GATemplateOutput();
		
		$teststr  = 'Template Output Should solve our problems, right?';
		$teststr2 = 'The quick brown fox jumps over the lazy dog';
		
		$tplout->setContent($teststr);
		
		$this->assertEquals($teststr, $tplout->getContent(), 'Unexpected content obtained' );
		
		$tplout2 = new GATemplateOutput($teststr2); 
		$this->assertEquals($teststr2, $tplout2->getContent(), 'Unexpected content obtained' );
	}
	
	public function testToString() {
		$teststr = 'Whenever we\'re weary, we are sad.';
		$tplout = new GATemplateOutput($teststr);
		
		$this->assertEquals($teststr, $tplout->__toString(), 'ToString Method returned an unexpected result:'.$tplout->__toString());
	}
	
}

?>
