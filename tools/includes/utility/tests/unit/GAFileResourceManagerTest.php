<?php
/*
 * Created on Jun 20, 2007
 * 
 * @author     Wayne Duran
 */


require_once('Class.GAFileResourceManager.php');
require_once('Class.GAFileResource.php');
require_once('Class.RandomStringGenerator.php');

class F {
	
	static $RS;
	
	static $xval;
	static $xext;
	static $xpath1;
	static $xpath2;
	static $xpath3;
	
	static $yval;
	static $yext;
	static $ypath1;
	static $ypath2;
	static $ypath3;
	
	
	static function init() {
		self::$RS     = RandomStringGenerator::instance();
		
		self::$xval   = self::$RS->getPhpLabel(mt_rand(1,50));
		self::$xext   = self::$RS->getPhpLabel(mt_rand(3,4));
		self::$xpath1 = self::$RS->getLowercaseAlpha(mt_rand(5,8)).'/';
		self::$xpath2 = self::$xpath1 .'/'.self::$RS->getLowercaseAlpha(mt_rand(5,8)).'/';
		self::$xpath3 = self::$RS->getLowercaseAlpha(mt_rand(5,8)).'/';
		
		
		self::$yval   = self::$RS->getPhpLabel(mt_rand(1,50));
		self::$yext   = self::$RS->getPhpLabel(mt_rand(3,4));
		self::$ypath1 = self::$RS->getLowercaseAlpha(mt_rand(5,8)).'/';
		self::$ypath2 = self::$RS->getLowercaseAlpha(mt_rand(5,8)) .'/'.self::$RS->getLowercaseAlpha(mt_rand(5,8)).'/';
		self::$ypath3 = self::$RS->getLowercaseAlpha(mt_rand(5,8)).'/';
	}
}


class TemplateResource implements GAFileResourceInterface {
	
	private   $paths = array();
	private   $pathsCreated = array();
	private   $filesCreated = array();
	
	public function getIdentifier() {
		return 'TemplateResource';
	}
	
	public function addLookUpPath($path) {
		// For this testing purposes only, we prepend the 'temp/' to path 
		$this->paths[] = 'temp/'.$path;
	}
	
	public function getLookUpPaths() {
		return $this->paths;
	}
	public function getFileName($file) {
		return $file.'.trs';
	}

	/* The Following Functions Are For Testing Purposes ONLY */
	public function createPath($path) {
		// Make sure we're not putting them anywhere else
		$path = 'temp/'.$path;
		$dirs = explode('/', $path);
	    $dir='';
	    foreach ($dirs as $part) {
	        $dir.=$part.'/';
	        if (!is_dir($dir) && strlen($dir)>0)
	            mkdir($dir, 0777);
	    }
		$this->pathsCreated[] = $path;
		
		return true;
	}
	
	private function deletePath($path) {
		rmdir($path);
	}
	
	public function createFile($path, $filename) {
		$path = 'temp/'.$path;
		
		if (!$handle = fopen($path.$filename, 'a')) {
        	throw new Exception("Cannot open file ($filename)");
        	exit;
	    } else {
	    	$this->filesCreated[] = $path.$filename;
	    }
	
		
	    // Write $somecontent to our opened file.
	    if (fwrite($handle, $path.$filename.' created') === FALSE) {
	        throw new Exception("Cannot write to file ($filename)");
	        exit;
	    }
	    
	    return true;
	}
	
	private function deleteFile($filename) {
		unlink($filename);
	}
	
	
	public function unlink($path)
    {
        /*    make sure the path exists    */
        if(!file_exists($path)) return false;
       
        /*    If it is a file or link, just delete it    */
        if(is_file($path) || is_link($path)) return @unlink($path);
       
        /*    Scan the dir and recursively unlink    */
        $files = scandir($path);
        foreach($files as $filename)
        {
            if($filename == '.' || $filename == '..') continue;
            $file = str_replace('//','/',$path.'/'.$filename);
            self::unlink($file);
        }//end foreach
       
        /*    Remove the parent dir    */
        if(!@rmdir($path)) return false;
        return true;
    }//end function unlink

	
	function __destruct() {
		// Cleanup!
		
		foreach ($this->filesCreated as $file) {
			$this->deleteFile($file);
		}
		
		foreach ($this->pathsCreated as $path) {
			$this->unlink($path);
		}
		
		$this->unlink('temp/');
		
		
	} 
}

class XResource extends TemplateResource {
	
	public function getIdentifier() {
		return F::$xval;
	}
	
	public function getFileName($file) {
		return $file.'.'.(F::$xext);
	}
}

class YResource extends TemplateResource {
	
	public function getIdentifier() {
		return F::$yval;
	}
	public function getFileName($file) {
		return $file.'.'.(F::$yext);
	}
}


class GAFileResourceManagerTest extends PHPUnit_Framework_TestCase implements GAFileResourceInterface {
	
	protected $PM;
	private   $paths = array();
	
	protected function setUP() {
		$this->PM = GAFileResourceManager::instance();
		F::init();
	}
	
	public function getIdentifier() {
		return 'GAFileResourceManagerTest';
	}
	
	public function addLookUpPath($path) {
		$this->paths[] = $path;
	}
	
	public function getLookUpPaths() {
		return $this->paths;
	}
	
	public function getFileName($file) {
		return 'GAFileResourceManagerTest'.$file;
	}
	
	/**
	 * Start Tests
	 */
	public function testInstantiation() {
		$this->assertEquals(get_class($this->PM), 'GAFileResourceManager', 'Not a GAResourcehManager instance');
	}
	
	
	public function testSingleton() {
		$b = GAFileResourceManager::instance();
		$this->assertSame($b, $this->PM, 'Not Singleton!');
	}
    
    
    public function testRegister() {
    	$this->assertTrue($this->PM->register($this), 'Cannot Register');
    	$this->assertTrue($this->PM->register(new XResource()), 'Cannot Register X Resource');
    	$this->assertTrue($this->PM->register(new YResource()), 'Cannot Register Y Resource');
    	
	}
	
    
    
    
    public function testRegisterAndGetFile() {
    	
    	try {
    		$this->PM->getTest('test');
    		throw new Exception();
    	} catch (Exception $e) {
    		$this->assertEquals('GAFileResourceUnknownException', get_class($e), 'Unable to throw proper exception when passing uknown Resource');
    		$this->assertEquals('Unknown or unregistered resource \'Test\'', $e->getMessage(), 'Wrong Message');
    	}
    	
    	$this->assertTrue($this->PM->register($this), 'Cannot Register');
    	try {
    		$this->PM->moda();
    		throw new Exception();
    	} catch (Exception $e) {
    		$this->assertEquals('GAFileResourceManagerMethodUnknownException', get_class($e), 'Unable to throw proper exception when invoking uknown method');
    	}
    	
    	$test = $this->PM->getGAFileResourceManagerTest('.php');
    	$this->assertEquals('GAFileResourceManagerTest.php', $test, 'Wrong resource return');
    	
    	/* 
    	$test = $this->PM->getGAFileResourceManagerTest('Karasa');
    	$this->assertEquals(false, $test, 'Must return false when no file is found');
    	*/
    }
    
    
    
    
    public function testLookUps() {
    	$xr = new XResource();
    	$this->PM->register($xr);
    	
    	// Create The Paths First
    	$xr->createPath(F::$xpath1);
    	$xr->createPath(F::$xpath2);
    	$xr->createPath(F::$xpath3);
    	
    	// Create Some Test Files
    	$fname1 = 'foo.'.F::$xext;
    	$fname2 = 'bar.'.F::$xext;
    	$fname3 = 'foobar.'.F::$xext;
    	$fname4 = 'foobar2.'.F::$xext;
    	
    	$xr->createFile(F::$xpath1, $fname1);
    	
    	$xr->createFile(F::$xpath1, $fname2);
    	$xr->createFile(F::$xpath2, $fname2);
    	$xr->createFile(F::$xpath3, $fname2);  ///////
    	
    	$xr->createFile(F::$xpath1, $fname3); 
    	$xr->createFile(F::$xpath2, $fname3);  ///////
    	
    	$xr->createFile(F::$xpath3, $fname4); ////
    	
    	// Register these paths
    	$xr->addLookUpPath(F::$xpath1);
    	$xr->addLookUpPath(F::$xpath2);
    	$xr->addLookUpPath(F::$xpath3);
    	
    	////////// Start Testing!!! /////////
    	$xid = F::$xval;
    	
    	// Test that we get $fname1
    	eval('$file1 = $this->PM->get'.F::$xval.'("foo");');
    	$this->assertEquals('temp/'.F::$xpath1.$fname1, $file1, "Looking for $file1: Wrong file returned!");
    	
    	eval('$file2 = $this->PM->get'.F::$xval.'("bar");');
    	$this->assertEquals('temp/'.F::$xpath3.$fname2, $file2, "Looking for $file2: Wrong file returned!");
    	
    	eval('$file3 = $this->PM->get'.F::$xval.'("foobar");');
    	$this->assertEquals('temp/'.F::$xpath2.$fname3, $file3, "Looking for $file3: Wrong file returned!");
    	
    	eval('$file4 = $this->PM->get'.F::$xval.'("foobar2");');
    	$this->assertEquals('temp/'.F::$xpath3.$fname4, $file4, "Looking for $file4: Wrong file returned!");
    	
    }
    
    public function testFailedLookUps() {
    	$tr = new TemplateResource();
    	$this->PM->register($tr);
    	try {
	    	// Test that we get $fname1
	    	$f = $this->PM->getTemplateResource('NonExistingFile');
    		$this->assertTrue(false, 'Must Throw Exception!');
    	} catch (Exception $e) {
    		
    		$this->assertTrue($e instanceof GAFileResourceManagerException, 'Exception thrown is not a GAFileResourceManagerException:'.get_class($e));
    		$this->assertTrue(strpos($e->getMessage(), 'NonExistingFile.trs') > 0, 'GAFileResourceManagerExceptionn does not properly indicate which template it tried to include');
    	}
    }
    
    
    public function testRequires() {
    	$this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }
    
    
    
    public function testIncludes() {
    	$this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }
}
?>
