<?php
	/*******
	 * AUTHOR : Reynaldo Castellano III
	 * DATE : June 8, 2007
	 */
	class Captcha
	{
		const NUMERIC 	= 0;
		const STRING	= 1;
		const HASH		= 2;
		const BOOLEAN	= 3;
		//const CHARSET	= 'A,B,C,D,E,G,H,K,L,M,N,O,P,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,p,q,r,s,t,u,v,w,x,y,z,2,3,4,5,6,7,8';
		const CHARSET	= 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,p,q,r,s,t,u,v,w,x,y,z,2,3,4,5,6,7,8';
		const DEF_WIDTH		= 150;
		const DEF_HEIGHT	= 50;
		const DEF_CHARLEN	= 5;
		const DEF_MIN_FONT_SIZE	= 25;
		const DEF_MAX_FONT_SIZE	= 35;
		
		const MIN_WIDTH		= 50;
		const MAX_WIDTH 	= 200;
		const MIN_HEIGHT	= 20;
		const MAX_HEIGHT	= 80;		
		const MIN_FONT_SIZE	= 20;
		const MAX_FONT_SIZE = 40;		
		const MIN_CHARLEN 	= 3;
		const MAX_CHARLEN 	= 10;
		
		const DEF_CHAR_SPACE = 6;
		const MIN_CHAR_SPACE = 5;
		
		private $imgData = null;
		
		public static function createImage($opts=array()){			
			
			/****
			
			$dirName = dirname(__FILE__);			
			$fontPath 	= $dirName.'/fonts/';
			$tempPath	= $dirName.'/temp/';
			
			//perform chmod if necessary
			//chmod($tempPath,0644);
			
			echo 'File Exists: '.var_dump(file_exists($tempPath)).'<br />';
			echo 'Is Writable: '.var_dump(is_writable($tempPath)).'<br />';
			echo 'Can Chmod: '.var_dump(chmod($tempPath,0777)).'<br />';
			
			$cWidth 	= self::initVar($opts,'width',self::NUMERIC,self::DEF_WIDTH);
			$cWidth		= ($cWidth > self::MAX_WIDTH)?self::MAX_WIDTH:$cWidth;
			$cWidth		= ($cWidth < self::MIN_WIDTH)?self::MIN_WIDTH:$cWidth;
			
			$cHeight 	= self::initVar($opts,'height',self::NUMERIC,self::DEF_HEIGHT);
			$cHeight	= ($cHeight > self::MAX_HEIGHT)?self::MAX_HEIGHT:$cHeight;
			$cHeight	= ($cHeight < self::MIN_HEIGHT)?self::MIN_HEIGHT:$cHeight;
			
			$cCharLen	= self::initVar($opts,'charLen',self::NUMERIC,self::DEF_CHARLEN);
			$cCharLen	= ($cCharLen > self::MAX_CHARLEN)?self::MAX_CHARLEN:$cCharLen;
			$cCharLen	= ($cCharLen < self::MIN_CHARLEN)?self::MIN_CHARLEN:$cCharLen;
								
			//get the avilable fonts
			$dirFonts = scandir($fontPath);
			$fonts = array();
			
			foreach($dirFonts as $iFont){
				if(preg_match('/.ttf$/i',$iFont)){
					$fonts[] = $fontPath.$iFont;
				}
			}
			$cFontFiles = self::initVar($opts,'fontFiles',self::HASH,$fonts);
			//$cFontFiles = self::initVar($opts,'fontFiles',self::HASH,array($fontPath.'monofont.ttf', '/fonts/VeraBI.ttf', '/fonts/VeraMoBd.ttf', '/fonts/VeraSe.ttf'));
			//$cFontFiles = self::initVar($opts,'fontFiles',self::HASH,array($fontPath.'monofont.ttf'));
			
			$cFontColor = self::initVar($opts,'fontColor',self::HASH,'RANDOM');
			$fsRange 	= self::initVar($opts,'fontSizeRange',self::HASH,array());
			
			$cFsMin		= self::initVar($fsRange,'min',self::NUMERIC,self::DEF_MIN_FONT_SIZE);
			$cFsMin 	= ($cFsMin < self::MIN_FONT_SIZE || $cFsMin > self::MAX_FONT_SIZE)?self::MIN_FONT_SIZE:$cFsMin;			
			$cFsMax		= self::initVar($fsRange,'max',self::NUMERIC,self::DEF_MAX_FONT_SIZE);
			$cFsMax 	= ($cFsMax > self::MAX_FONT_SIZE)?self::MAX_FONT_SIZE:$cFsMax;
			$cCharSet 	= self::initVar($opts,'characterSet',self::STRING,self::CHARSET);
			
			//constant charset
			$constCharSet = explode(',',self::CHARSET);
			
			//check if the charset is valid
			$arCharSet = explode(',',$cCharSet);
			$finalCharSet = array();
			//make sure that all array elements are single aplha numeric characters
			foreach($arCharSet as $char){
				if(in_array($char,$constCharSet)){
					$finalCharSet[] = $char;
				}
			}
			$finalCharSet = array_unique($finalCharSet); 
			//check if each character is a one length string and a alpha numeric character.
			if(count($finalCharSet) < $cCharLen){
				$finalCharSet = $constCharSet;
			}
						
			//generate the security code 
			$arCode = array();
			for($i=0;$i<$cCharLen;$i++){
				$idx = array_rand($finalCharSet);
				$arCode[] = $finalCharSet[$idx];
				//remove the index from the final charset
				unset ($finalCharSet[$idx]);
				$finalCharSet = array_values($finalCharSet);
			}
		    $strCode = implode('',$arCode);
				
			//--start the actual creation of the image--

			//create the image
			$img = imagecreate($cWidth, $cHeight);
			//allocate white background and make it tranparent
			//$transColor = imagecolorallocate($img, 255, 255, 255);
			$transColor = imagecolorallocate($img, 0, 0, 0);
			imagecolortransparent($img,$transColor);			
					
			//draw the characters
			$codeLen = count($arCode);
			//create first the characters to draw and later draw them. This is better to have a more accurate font spacing
			
			$arCharInfo = array();
			
			$codeWidth = 0;
			
			for($i=0;$i< $codeLen;$i++) {
	            // select random font	            
	            $curFont = $cFontFiles[array_rand($cFontFiles)];	            
	            $curFontColor =  (is_array($cFontColor)
	            	?imagecolorallocate($img,$cFontColor[0],$cFontColor[1],$cFontColor[2])
	            	:imagecolorallocate($img,rand(0, 100),rand(0, 100),rand(0, 100))
	            );
	            $randColor = rand(0, 120);	            	          
	            // select random font size
	            $curFontSize = rand($cFsMin, $cFsMax);
	            // select random angle
	            $fontAngle = rand(-30, 30);	            	                     
	            //$aCharDetails = imageftbbox($curFontSize, $fontAngle, $curFont, $arCode[$i]);
	            // get dimensions of character in selected font and text size
	            $charDetails = imageftbbox($curFontSize, $fontAngle, $curFont, $arCode[$i]);
	            $codeWidth =+  $charDetails[0] - $charDetails[2]; 
	            $arCharInfo[] = array(
	            	'charDetails' => $charDetails,
	            	'fontDetails' => array(
	            		'font' => $curFont,
	            		'color'=> $curFontColor,
	            		'size' => $curFontSize,
	            		'angle' => $fontAngle
	            	),
	            	'char' => $arCode[$i]
	            ); 
	            $spacing = (int)($cWidth / $cCharLen);
	            $iX = $spacing / 4 + $i * $spacing;
	            $iCharHeight = $charDetails[2] - $charDetails[5];
	            $iY = $cHeight / 2 + $iCharHeight / 4; 	            
	            // write text to image
	            imagefttext($img, $curFontSize, $fontAngle, $iX, $iY, $curFontColor, $curFont, $arCode[$i]);
	    	}	    		    	
	    	
	    	//save the image temporarily
	    	$imgFileName =$tempPath.time().$strCode.'.gif';
			imagegif($img, $imgFileName);
			$imgData = 'data:image/gif;base64,'.base64_encode(file_get_contents($imgFileName));				
			unlink($imgFileName); 
	    	require_once('Class.Crypt.php');
	    	$crypt = new Crypt();
	    	return array('src'=>$imgData,'code'=>$crypt->encrypt($strCode));	    	
	    	****/
	    	require_once('Class.Crypt.php');
	    	$crypt = new Crypt();
	    	$cCharSet 	= self::initVar($opts,'characterSet',self::STRING,self::CHARSET);
			//constant charset
			$constCharSet = explode(',',self::CHARSET);			
			//check if the charset is valid
			$arCharSet = explode(',',$cCharSet);		
	    	$finalCharSet = array();
			//make sure that all array elements are single aplha numeric characters
			foreach($arCharSet as $char){
				if(in_array($char,$constCharSet)){
					$finalCharSet[] = $char;
				}
			}
			$finalCharSet = array_unique($finalCharSet);	    	
	    	
	    	$cWidth 	= self::initVar($opts,'width',self::NUMERIC,self::DEF_WIDTH);
			$cWidth		= ($cWidth > self::MAX_WIDTH)?self::MAX_WIDTH:$cWidth;
			$cWidth		= ($cWidth < self::MIN_WIDTH)?self::MIN_WIDTH:$cWidth;
			
			$cHeight 	= self::initVar($opts,'height',self::NUMERIC,self::DEF_HEIGHT);
			$cHeight	= ($cHeight > self::MAX_HEIGHT)?self::MAX_HEIGHT:$cHeight;
			$cHeight	= ($cHeight < self::MIN_HEIGHT)?self::MIN_HEIGHT:$cHeight;
	    	
	    	$cCharLen	= self::initVar($opts,'charLen',self::NUMERIC,self::DEF_CHARLEN);
			$cCharLen	= ($cCharLen > self::MAX_CHARLEN)?self::MAX_CHARLEN:$cCharLen;
			$cCharLen	= ($cCharLen < self::MIN_CHARLEN)?self::MIN_CHARLEN:$cCharLen;

	    	$arCode = array();
			for($i=0;$i<$cCharLen;$i++){
				$idx = array_rand($finalCharSet);
				$arCode[] = $finalCharSet[$idx];
				//remove the index from the final charset
				unset ($finalCharSet[$idx]);
				$finalCharSet = array_values($finalCharSet);
			}
		    $strCode = implode('',$arCode);		    
		    $cryptCode = $crypt->encrypt($strCode);
	    	//var_dump(rawurlencode($cryptCode));exit;s
	    	return array('src'=>'/captcha.php?code='.rawurlencode($cryptCode).'&width='.$cWidth.'&height='.$cHeight.'&dt='.time(),'code'=>$cryptCode);
	    	//return array('src'=>'/captcha.php?code='.$cryptCode.'&width='.$cWidth.'&height='.$cHeight,'code'=>$cryptCode);
		}		
		
		private static function initVar($hash,$key,$type="STRING",$defVal){
			$hash = (is_array($hash)) ? $hash : array();
			$result =array_key_exists($key,$hash)?$hash[$key]:$defVal;
			switch($type){
				case self::STRING:
					$result = (is_string($result)?$result:$defVal);
				break;
				case self::NUMERIC:
					$result = (is_numeric($result)?$result:$defVal);
				break;
				case self::HASH:
					$result = (is_array($result)?$result:$defVal);
				break;
				case self::BOOLEAN:
					$result = (is_bool($result)?$result:$defVal);
				break;
			}			 
			return $result;
		}
		
		public static function validate($securityCode,$inputStr,$isCaseSensitive=false){
			require_once('Class.Crypt.php');
	    	$crypt = new Crypt();
	    	if($isCaseSensitive){
	    		return $crypt->decrypt($securityCode) === $inputStr;	
	    	}
			else{
				return strtolower($crypt->decrypt($securityCode)) == strtolower($inputStr);
			}
		}
	}
	/**
	$options = array(
		'width' 		=> 	200,//default : 150
		'height'		=>	50,//default : 50
		'charLen'		=> 	5,//default 6 max 10 min 3	
		//'fontFiles' 		=> 	array('/fonts/tahoma.ttf'),	
		'fontSizeRange' => 	array(
			'min'=>25,//default : 15
			'max'=>40//default : 18
		)	
		//'fontColor'		=> array(255,255,255)//default : random		
	);
	$security = Captcha::createImage($options);
	**/
	
	
?>