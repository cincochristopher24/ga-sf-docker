<?
/**
* isNull,isEmail,isDigit,isCurrency and isURL
* @param args         any struct (required)
* @result returns a boolean
* @author Aldwin S. Sabornido, www.goabroad.com
* @version 1.0 2006-05-15
*/	
class Validator
{
	
	static $instance = NULL;
	
	static function getInstance(){
		if( self::$instance == NULL ) self::$instance = new Validator;
		return self::$instance;
	}
	
	function isNull($list)
	{
		$error = false;
		foreach( $list as $value )
		{
			if (trim($value) == "")
			{
				$error = true;
				break;
			}	
		}	
		return $error;
	}	
	
	function isEmail($email)
	{
		//$pattern = '^(([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([;.](([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+)*$';
		$pattern = '/^([a-zA-Z0-9_\'+*$%\^&!\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9:]{1,})+$/i';
		return preg_match( $pattern , $email ); 
		//return eregi( $pattern , $email ); 
	}
	
	// only for mm[-/]dd[-/]yyyy format
	function isValidDate($date){
		
		if(preg_match("/^[0-9]{2}[-\/][0-9]{2}[-\/][0-9]{4}$/",trim($date)) ){
			$date = split('[-\/]',trim($date));
			if( checkdate($date[0],$date[1],$date[2]) )
				return true;
			else
				return false;	
		}
		else
			return false;
	}
	
}
?>
