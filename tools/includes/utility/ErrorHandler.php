<?php
	//if (isset($_SERVER['SERVER_NAME'])) {
		/***
		$MAINDOTNET_DOMAIN  = array(
			'goabroad.net.local',
			'dev.goabroad.net',
			'ga-web03.goabroad.net',
			'www.goabroad.net'
		);		
		$COBRAND_DEV_DOMAIN  = array(
			'advisor.dev.goabroad.net',
			'aifs.dev.goabroad.net',
			'myproworld.dev.goabroad.net',
			'cis.dev.goabroad.net',
			'cea.dev.goabroad.net',
			'iep.dev.goabroad.net',
			'globalscholar.dev.goabroad.net',
			'allianceabroad.dev.goabroad.net',
			'culturalembrace.dev.goabroad.net',
			'worldendeavors.dev.goabroad.net',
			'educasian.dev.goabroad.net',
			'eliabroad.dev.goabroad.net',
			'globalsemesters.dev.goabroad.net',
			'livingroutes.dev.goabroad.net',
			'academictreks.dev.goabroad.net',
			'ambafrance-us.dev.goabroad.net',
			'australearn.dev.goabroad.net',
			'central.dev.goabroad.net',			
			'ciee.dev.goabroad.net',
			'myisic.dev.goabroad.net',	
			'neumann.dev.goabroad.net',
			'nvcc.dev.goabroad.net',
			'quinnipiac.dev.goabroad.net',
			'samford.dev.goabroad.net',		
			'solabroad.dev.goabroad.net',
			'englishfirst.dev.goabroad.net',
			'cisdenver.dev.goabroad.net',
			'ucdenver.dev.goabroad.net',
			'gvi.dev.goabroad.net',
			'internationalstudent.dev.goabroad.net',
			'ucboulder.dev.goabroad.net',
			'dse.dev.goabroad.net',
			'yha.dev.goabroad.net'
		);
		
		$COBRAND_PROD_DOMAIN  = array(
			'advisor.goabroad.net',
			'aifs.goabroad.net',
			'myproworld.goabroad.net',
			'cis.goabroad.net',
			'cea.goabroad.net',
			'iep.goabroad.net',
			'globalscholar.goabroad.net',
			'allianceabroad.goabroad.net',
			'culturalembrace.goabroad.net',
			'worldendeavors.goabroad.net',
			'educasian.goabroad.net',
			'eliabroad.goabroad.net',
			'globalsemesters.goabroad.net',
			'livingroutes.goabroad.net',
			'academictreks.goabroad.net',
			'ambafrance-us.goabroad.net',
			'australearn.goabroad.net',
			'central.goabroad.net',			
			'ciee.goabroad.net',
			'myisic.goabroad.net',	
			'neumann.goabroad.net',
			'nvcc.goabroad.net',
			'quinnipiac.goabroad.net',
			'samford.goabroad.net',		
			'solabroad.goabroad.net',
			'englishfirst.goabroad.net',
			'cisdenver.goabroad.net',
			'ucdenver.goabroad.net',
			'gvi.goabroad.net',
			'internationalstudent.goabroad.net',
			'ucboulder.goabroad.net',
			'ga-web01.goabroad.net',
			'dse.goabroad.net',
			'yha.goabroad.net'
		);
	    $alldomains =  array_merge($MAINDOTNET_DOMAIN , $COBRAND_DEV_DOMAIN , $COBRAND_PROD_DOMAIN ) ;
		***/
		/***
		$excludedDomains = array(
			'www.chromedia.com',
			'ga-web01.cacti.goabroad.com',
			'cacti.goabroad.com',
			'www.chromedia.com',
			'ga_web01.gallery.chromedia.com',
			'gallery.chromedia.com',
			'phpmyadmin-db01.goabroad.com',
			'phpmyadmin-db02.goabroad.com',
			'ga_web01.phpmyadmin.goabroad.com',
			'phpmyadmin.goabroad.com',
			'calendar.goabroad.com'
		);
		
		// proceed to log errors if its a main .net domain or a cobranded site
		if( !in_array( $_SERVER['SERVER_NAME'], $excludedDomains) )
			require_once('ErrorLogger.php');
		
	}
	***/
	require_once('errorLogger/Class.ErrorLogger.php');
	ErrorLogger::getInstance()->start();
?>