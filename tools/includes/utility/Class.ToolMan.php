<?php
	class ToolMan
	{
		
		public static function makeSqlSafeString($str){			
			// Stripslashes
			if (get_magic_quotes_gpc()){
				$str = stripslashes($str);
		   	}
		   	// Quote if not a number or a numeric string
		   	//if (!is_numeric($str)){
		       	$str = "'" . mysql_real_escape_string($str) . "'";
		   	//}
		   	return $str;
		}
		
		public static function redirect($dest=''){
			header("Location: $dest");
			exit;
		}
		
		public static function isValidDate($date_string) {
			try{									
				$ts = strtotime($date_string);
				if(!$ts){ 
					return false;	
				}
				$d = intval(date('d', $ts));
				$m = intval(date('m', $ts));
				$y = intval(date('y', $ts));						
				return checkdate($m, $d, $y);
			}
			catch(exception $e){
				return false;
			}   				
		}
		
		/***
		* Script from phpbuilder
		* yyyy	year
		*	q	Quarter
		*	m	Month
		*	y	Day of year
		*	d	Day
		*	w	Weekday
		*	ww	Week of year
		*	h	Hour
		*	n	Minute
		*	s	Second
		**/
		public static function dateAdd($interval, $number, $date){		    		    		    	
		    $date_time_array = getdate($date);
		    $hours = $date_time_array['hours'];
		    $minutes = $date_time_array['minutes'];
		    $seconds = $date_time_array['seconds'];
		    $month = $date_time_array['mon'];
		    $day = $date_time_array['mday'];
		    $year = $date_time_array['year'];		
		    switch ($interval) {		    
		        case 'yyyy':
		            $year+=$number;
		            break;
		        case 'q':
		            $year+=($number*3);
		            break;
		        case 'm':
		            $month+=$number;
		            break;
		        case 'y':
		        case 'd':
		        case 'w':
		            $day+=$number;
		            break;
		        case 'ww':
		            $day+=($number*7);
		            break;
		        case 'h':
		            $hours+=$number;
		            break;
		        case 'n':
		            $minutes+=$number;
		            break;
		        case 's':
		            $seconds+=$number;
		            break;
		    }
		    return mktime($hours,$minutes,$seconds,$month,$day,$year);		     
		}
		
		public static function isValidEmailAddress($email){
	    	// First, we check that there's one @ symbol, and that the lengths are right
			if (!preg_match("/^[^@]{1,64}@[^@]{1,255}$/", $email)){
			  // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
			  return false;
			}
			// Split it into sections to make life easier
			$email_array = explode("@", $email);
			$local_array = explode(".", $email_array[0]);
			for ($i = 0; $i < sizeof($local_array); $i++){
				if (!preg_match("/^(([A-Za-z0-9!#$%&'*+\/=?^_`{|}~-][A-Za-z0-9!#$%&'*+\/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$/", $local_array[$i])) {
			    	return false;
			  	}
			}  
			if (!preg_match("/^\[?[0-9\.]+\]?$/", $email_array[1])){ // Check if domain is IP. If not, it should be valid domain name
				$domain_array = explode(".", $email_array[1]);
			  	if (sizeof($domain_array) < 2) {
			    	return false; // Not enough parts to domain
			  	}
			  	for ($i = 0; $i < sizeof($domain_array); $i++){
			    	//if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])){
					if (!preg_match("/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$/", $domain_array[$i])){
			      		return false;
			    	}
			 	}
			}
			return true;  
	    }
	    
	    /***
	     * This function is from rune at zedeler dot dk
	     */
	    public static function deepCompare($a,$b) {
			if(is_object($a) && is_object($b)) {
			    if(get_class($a)!=get_class($b)){
			    	return false;
			    }
			    foreach($a as $key => $val) {
			    	if(!self::deepCompare($val,$b->$key)){
			        	return false;
			    	}
			    }
			    return true;
		  	}
			else if(is_array($a) && is_array($b)) {
				while(!is_null(key($a)) && !is_null(key($b))) {
			    	if (key($a)!==key($b) || !self::deepCompare(current($a),current($b))){
			        	return false;
			    	}
			    	next($a); 
			    	next($b);
			    }
			    return (is_null(key($a)) && is_null(key($b)));
			}
			else{
				return ($a===$b);
			}
		}	  
		
		static function convertTitleToFriendlyURLFormat($title){
			for($counter = 0; $counter < strlen($title); $counter++){
				if (substr($title,$counter,1) === " ")	  $char = "-"; 
				elseif (substr($title,$counter,1) === "-") $char = "_"; 
				else $char = substr($title,$counter,1);
				$newTitle = $newTitle.$char;				
			}		     		
			return $newTitle; 
		}  	
		
		static function truncateText($text,$length = 0) {
			if(0 != strlen($text)){
				if($length < strlen($text) ) $return = substr($text,0,$length)."...";	   
				else $return = $text; 	
				return $return;
			}
		}
		
		static function getAbsolutePath($relativePath=''){
			$trace = debug_backtrace();
			if(count($trace) > 0){
				$dir = explode('/',$trace[0]['file']);
				return implode('/',array_slice($dir,0,count($dir)-1)).'/'.$relativePath;
			}
			return '';
		}
		/***
			- OPTIONS
				* path 		=> the path to the strategy files
				* key 		=> key 
		***/
		static public function loadClassInstances($options=array('path'=>'','key'=>array())){
			$objects = array();
			$files = scandir($options['path']);
			foreach($files as $iFile){
				//check if it is a php file
				$key = str_replace($options['key'],'',$iFile);
				if($key !== $iFile){
					$comKey = strtoupper($key);
					$clsName = str_replace(array('Class.','.php'),'',$iFile);
					require_once($options['path'].'/'.$iFile);
					$objects[$comKey] = new $clsName();
				}
			}
			return $objects;
		}
		
		static public function loadClasses($options=array('path'=>'','key'=>array())){
			$objects = array();
			$files = scandir($options['path']);
			foreach($files as $iFile){
				//check if it is a php file
				$key = str_replace($options['key'],'',$iFile);
				if($key !== $iFile){
					$comKey = strtoupper($key);
					$clsName = str_replace(array('Class.','.php'),'',$iFile);
					$objects[$comKey] = $clsName;
				}
			}
			return $objects;
		}
		
		/**
		 * Returns true when the given file name exists in the ini include path.
		 * 
		 * @param string $file_name The name of the file to be tested.
		 * 
		 * @return boolean Returns true when the given file name exists in the ini include path.
		 * 
		 * @author Antonio Pepito Cruda Jr.
		 */
		static function file_exists($file_name){
			$paths = explode(":", ini_get("include_path"));
			foreach($paths as $path){
				if(file_exists($path."/".$file_name)){
					return true;
				}
			}
			
			return false;
		}
		
	}	
?>