<?php
	/***
	 * @author: Reynaldo Castellano III
	 * Date Created: Dec 19, 2006
	 * Comment: The code was originally written by David _blank_. I've just made it shorter and added some utf8 encoding.
	 */
	class XmlEscaper{
		public static function escapeXml ($s){			
			$s = utf8_encode($s);
			$result = '';
			$len = strlen($s);
			for ($i = 0; $i < $len; $i++) {			
			  	if(ord($s{$i}) <= 127){
			  		$result .= htmlentities($s{$i}); 
			  	}
				else if (ord($s{$i}) > 127) {
				    if ((ord($s{$i}) & 0xf0) == 0xf0) {//4 bytes
					    $result .= $s{$i++};
					    $result .= $s{$i++};
					    $result .= $s{$i++};
					    $result .= $s{$i};
				    }
					else if ((ord($s{$i}) & 0xe0) == 0xe0) {//3 bytes
						$result .= $s{$i++};
						$result .= $s{$i++};
						$result .= $s{$i};
				    }
					else if ((ord($s{$i}) & 0xc0) == 0xc0) {//2 bytes
						$result .= $s{$i++};
						$result .= $s{$i};
				    }
			  	}
				else {
			    	$result .= $s{$i};
			  	}
			}
			return $result;
		}
	}
?>
