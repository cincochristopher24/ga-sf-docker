<?php
/**
 * GATemplateVariable
 * An Wrapper Iterator for collections passed as variable in
 * GATemplate to simplify iterations/loops 
 * Created on Jul 29, 2007
 * 
 * @author     Wayne Duran
 */

class GATemplateVariableIterator implements Iterator, ArrayAccess, Countable {
	private $collection = NULL;
	private $valid      = FALSE;
	private $counter    = NULL;
	private $count      = NULL;
	private $keys       = array();
	private $type       = NULL;
	
	const   ANARRAY          = 1;
	const   ANITERATOR       = 2;
	const   ANARRAYACCESSOBJ = 3;
	
	function __construct($collection) {
		if (is_array($collection)) {
			$this->type = self::ANARRAY;
		} elseif ($collection instanceof Iterator) {
			$this->type = self::ANITERATOR;
		} elseif ($collection instanceof ArrayAccess) {
			$this->type = self::ANARRAYACCESSOBJ;
		}
		
		$this->collection = $collection;
		$this->rewind();
	}
	
	function rewind(){
		$this->valid   = (FALSE !== reset($this->collection));
		$this->counter = 0;
	}
	
	function count() {
		if (is_null($this->count)) {
			if ($this->collection instanceof Countable || is_array($this->collection)) {
				$this->count = count($this->collection);
			}
		}
		return $this->count;
	}
	
	function next() {
		$this->counter++;
		return next($this->collection);
	}
	
	function current() {
		return current($this->collection);
	}
	
	function valid() {
		switch ($this->type) {
			case self::ANARRAY:
				
				break;
			case self::ANARRAYACCESSOBJ:
				return $this->collection->offSetExists();
				break;
			case self::ANITERATOR:
				break;
		}
	}
	
	
	
	
} 

?>
