<?php

interface Initialization {
	public function getIdentifier();
	public function getAvailableInitializationValues();
}

?>