<?php

	class ColumnsUtility{
		
		private $array_of_contents  = null,
				$default			= null;
		
		public static function createInstance(){
			return new ColumnsUtility();
		}
				
		public function setDefaults($user_default=null){
			$defaults = array(
				"number_of_columns" => 2,
				"content" 			=> "",
				"word_per_column"	=> 200
			);
			
			$this->default = array();
			$this->default = array_merge($defaults,$user_default);
		}
				
		public function divideContentToColumns(){
			$string 		   	= $this->default['content'];
			$word_per_column   	= $this->default['word_per_column'];
			$number_of_columns	= $this->default['number_of_columns'];
			
			$str_array 			= explode(" ",$string);
			$total_length 		= count($str_array);
			$length_per_column 	= ceil($total_length/$number_of_columns);

			$min = 1;
			if($total_length % 2 != 0)
				$min = 2;
				
			if($total_length <= $word_per_column)
				return array();
			
			$this->array_of_contents = array();
			for($i=0;$i<$number_of_columns;$i++){
				$lim1 = ($i*$length_per_column);
				$lim2 = (($i+1)*$length_per_column);
				
				if($i == ($number_of_columns-1))
					$this->array_of_contents['col'.$i] = $this->implode($str_array,$lim1,($lim2-$min));
				else $this->array_of_contents['col'.$i] = $this->implode($str_array,$lim1,($lim2-1));
			}
			
			for($j=0;$j<($number_of_columns-1);$j++){
				$tags = $this->parseStringForUnclosedTags(explode(" ",$this->array_of_contents['col'.$j]));
				while(count($tags)){
					$tag = array_pop($tags);
					$this->array_of_contents['col'.$j] .= preg_replace('/(<([\s]*([a-z]+))(.*)>)/','</$2>',$tag);
					$this->array_of_contents['col'.($j+1)] = $tag.$this->array_of_contents['col'.($j+1)];
				}
			}
			$this->convertLinksInColumns();
			return $this->array_of_contents;
		}

		protected function parseStringForUnclosedTags($array){
			$otag = array(); 
			foreach($array as $elt){
				if(preg_match_all("/<(.|\n)*?>/",$elt,$match,PREG_PATTERN_ORDER)){
					for($i=0;$i<count($match[0]);$i++){
						if(preg_match_all("/<([^\/]|\n)*?>/",$match[0][$i],$omatch,PREG_PATTERN_ORDER))
							array_push($otag,$omatch[0][0]);
						else
							array_pop($otag);
					}
				}
			}
			return $otag;
		}

		protected function implode($array, $lim1, $lim2){
			$string = "";
			
			for($i = $lim1; $i<=$lim2; $i++){
				$string .= $array[$i]." ";
			}
			return trim($string);
		}
		
		protected function convertLinksInColumns(){			
			for($i=0;$i<$this->default['number_of_columns'];$i++){
				if(!preg_match('/<p[^>]*>/',$this->array_of_contents['col'.$i]))
					//$this->array_of_contents['col'.$i] = nl2br(ereg_replace("[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]","<a href=\"\\0\">\\0</a>", $this->array_of_contents['col'.$i]));
					$this->array_of_contents['col'.$i] = nl2br(preg_replace("/[[:alpha:]]+:\/\/[^<>[:space:]]+[[:alnum:]\/]/","<a href=\"\\0\">\\0</a>", $this->array_of_contents['col'.$i]));
				else
					//$this->array_of_contents['col'.$i] = ereg_replace("[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]","<a href=\"\\0\">\\0</a>", $this->array_of_contents['col'.$i]);
					$this->array_of_contents['col'.$i] = preg_replace("/[[:alpha:]]+:\/\/[^<>[:space:]]+[[:alnum:]\/]/","<a href=\"\\0\">\\0</a>", $this->array_of_contents['col'.$i]);
			}
		}
	}	
?>