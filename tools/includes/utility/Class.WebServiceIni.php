<?php
//echo 'require_once = WebServiceIni<br>';	
	/*
	 * Created on Jul 8, 2008
	 */
	 
	class WebServiceIni{
		
		// domains
		const 
			FACEBOOK 	= 	'facebook.com',
			MYSPACE		=	'myspace.com',
			ORKUT		=	'orkut.com',
			HI5			=	'hi5.com',
			NING		=	'ning.com';
		
		// apps
		const 
			MY_TRAVEL_JOURNALS 	=	'myTravelJournals',
			MY_TRAVEL_MAP		=	'myTravelMap',
			MY_TRAVEL_BIO		=	'myTravelBio',
			MY_TRAVEL_PLANS		=	'myTravelPlans';
			
		// error messages
		const
			UNDEFINED_USER			=	'Undefined user',
			NON_GA_MEMBER			=	'Not GoAbroad Network Member.',
			SUSPENDED_USER			= 	'Your GoAbroad Network account is suspended.',
			USING_USERNAME			= 	'Please use your email to sychronize your account to Goabroad Network.',
			ACCOUNT_ALREADY_SYNCED	= 	'Account already synchronized with another user.',
			ACCOUNT_NOT_SYNCED		= 	'You have to synchronize your accounts first before you can change you settings.';
			
		static function getValidDomains(){
			return array(self::FACEBOOK, self::MYSPACE, self::ORKUT, self::HI5, self::NING);
		}
		
	} 
?>
