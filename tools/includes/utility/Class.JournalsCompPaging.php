<?php
/*
 * Created on 10 9, 08
 * 
 * Author:
 * Purpose: 
 * 
 */
 
 require_once('Class.Paging.php');
 
 class JournalsCompPaging extends Paging {
 	protected $origParams = '';
 	
 	/* 
	* EDITS: ianne - 05/12/2009
	* added $limit_pages for page limit
	*/	 	
 	function JournalsCompPaging( $recordcount, $page, $querystring='', $rowsperpage=1, $limit_pages=true ){
 		parent::__construct($recordcount, $page, $querystring, $rowsperpage, $limit_pages);
 		$this->stripOffPageAndJournalCompParam();	
 	}
 	
 	
 	// remove the page and jcp param from $_SERVER['QUERY_STRING'] to retain the original params which may be used by the context of the whole page
 	// 	 containing the Journals component
 	function stripOffPageAndJournalCompParam(){
 		//echo 'query string:' . $_SERVER['QUERY_STRING'] . '<br/>';
 		$this->origParams = preg_replace('/(&)?page=[0-9]+/','',$_SERVER['QUERY_STRING'],-1);		// remove page param 
 		$this->origParams = preg_replace('/&jcp=(.)+/','',$this->origParams,-1);					// remove jcp param
 		//$this->origParams = preg_replace('/\/page\/[0-9]+(\/)?/','',$_SERVER['QUERY_STRING'],-1);		// remove page param 
 		//$this->origParams = preg_replace('/jcp\/(.)+/','',$this->origParams,-1);					// remove jcp param
		//echo 'stripped:' . $this->origParams; 			 
 	}
 	
 	
 	function getNext()
	{
		if ( $this->totalpages ):
			if ( $this->last != $this->page )
			{
				if ( $this->onclick != NULL )
					return print('<a class="next" href="javascript:void(0)" id="next" onclick="'.$this->onclick.'(\'page='.$this->prevalue.$this->next.$this->appvalue.$this->querystring.'\')"><span>Next</span></a> ');
				else
					return print('<a class="next" href="http://'.$_SERVER['SERVER_NAME'].$this->scriptName."?".$this->origParams."page=".$this->prevalue.$this->next.$this->appvalue.$this->querystring."&#anchor"."\">"."<span>Next</span></a> ");
			}	
			else
				return print('<span class="next">Next</span> ');
		endif;
	}
	
	function getPrevious()
	{
		if ( $this->totalpages ):
			if ( $this->page != 1 )
			{
				if ( $this->onclick != NULL )
					return print('<a class="previous" href="javascript:void(0)" id="prev" onclick="'.$this->onclick.'(\'page='.$this->prevalue.$this->previous.$this->appvalue.$this->querystring.'\')"><span>Prev</span></a> ');
				else
					return print('<a class="previous" href="http://'.$_SERVER['SERVER_NAME'].$this->scriptName."?" . $this->origParams . "&page=".$this->prevalue.$this->previous.$this->appvalue.$this->querystring."&#anchor" ."\">"."<span>Prev</span></a> ");
			}
			else
				return print('<span class="previous">Prev</span> ');
		endif;
	}
	
	function getLast()
	{
		if ( $this->totalpages ):
			if ( $this->last != $this->page )
			{
				if ( $this->onclick != NULL )
					return print('<a class="last" href="javascript:void(0)" onclick="'.$this->onclick.'(\'page='.$this->prevalue.$this->last.$this->appvalue.$this->querystring.'\')"><span>Last</span></a> ');
				else
					return  print('<a class="last" href="http://'.$_SERVER['SERVER_NAME'].$this->scriptName. "?" . $this->origParams . "page=".$this->prevalue.$this->last.$this->appvalue.$this->querystring."&#anchor"."\">"."<span>Last</span></a> ");
			}
			else
				return print('<span class="last">Last</span> ');
		endif;
	}
	
	function getFirst()
	{
		if ( $this->totalpages ):
			if ( $this->page != 1 )
			{
				if ( $this->onclick != NULL )
					return print('<a class="first" href="javascript:void(0)" onclick="'.$this->onclick.'(\'page='.$this->prevalue.$this->first.$this->appvalue.$this->querystring.'\')"><span>First</span></a> ');
				else
					return print('<a class="first" href="http://'.$_SERVER['SERVER_NAME'].$this->scriptName."?". $this->origParams . "&page=".$this->prevalue.$this->first.$this->appvalue.$this->querystring."\">"."<span>First</a></span> ");
				
			}
			else
				return print('<span class="first">First</span> ');
		endif;
	}
	
	function getLinks($displaynum = NULL)
	{
		// Reset the links value to prevent repetition
		$this->links = '';
		
		if ($this->origParams != '')
			$this->origParams .= '&';
 		
			
		
		$all = (is_null($displaynum))? $this->displaynum : (int) $displaynum;
		if ($all < 1) {
			return print ('');
		}
		$half = floor($all/2);
		
		
		if ( $this->totalpages ){

			if ( $this->totalpages < ($all+1) && $this->page < ($all+1) ){
				$startloop = 1;
				$endloop   = $this->totalpages;
			}else{
				if ( $this->page <= $half){
					$startloop = 1;
					$endloop   = $all;
				}else{	
					if ( ($this->page+$half) >= $this->totalpages ){
						if ( ($this->totalpages-$all) < 0)
							$startloop = 1;
						else
							$startloop = $this->totalpages-$all;
							
						$endloop = $this->totalpages;
					}else{	
						if ( ($this->page-$half) < 0)
							$startloop = 1;
						else
							$startloop = $this->page-$half;
						$endloop = $this->page+$half;
					}	
				}	
			}
			while ( $startloop <= $endloop ){
				if ( $startloop != $this->page )
					if ( $this->onclick != NULL )
						$this->links .= '<a href="javascript:void(0)" onclick="'.$this->onclick.'(\'page='.$this->prevalue.$startloop.$this->appvalue.$this->querystring.'\')">'.$startloop.'</a> ';
					else{
						$this->links .= "<a href=\"http://".$_SERVER['SERVER_NAME'].$this->scriptName."?".$this->origParams."page=".$this->prevalue.$startloop.$this->appvalue.$this->querystring. '&#anchor' . "\">".$startloop."</a> ";
						
					}	
				else
					$this->links .= "<strong>".$startloop."</strong> ";
					
				$startloop++;
			}	
			//echo '<pre>';print_r($_SERVER);exit;
		}	
			
		return print($this->links);
	}
 	
 	
 }
 
?>
