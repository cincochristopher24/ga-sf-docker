<?php
	class GaPreparedStatement
	{		
		static public function create($statementID,$sqlStmt){
			return new GAPreparedStatement($statementID,$sqlStmt);
		}
		
		private $mStatementID	= null;
		private $mSqlStatement	= null;
		
		public function __construct($statementID,$sqlStatement){
			$this->mStatementID	= $statementID;
			$this->mSqlStatement= $sqlStatement;
		}
		
		public function getStatementID(){
			return $this->mStatementID;
		}
		
		public function getSqlStatement(){
			return $this->mSqlStatement;
		}
	}
?>