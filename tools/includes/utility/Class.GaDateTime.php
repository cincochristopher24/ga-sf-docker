<?php
/**
 * <b>GaDateTime</b> class
 * Date and Time manipulation and formatting
 * @author Wayne
 * @date November 28, 2006
 */
/**
 * Edits: Previously known as Class DateTime 
 */
class GaDateTime {
	private static $now = null;
	
	private static $sec = 1;
	private static $min = 60;
	private static $hour = 3600;
	private static $day = 86400;
	
	
	private $date = null;
	//private $mutationdate = null;
	
	public static function safeDate($date = '') {
		if ('' == $date) {
			$date = time();
		}
		if (!is_int($date)) {
			
			if (strpos($date, ' to ') > 1) {
				$dates = explode(' to ', $date);
				$dates[0] = strtotime($dates[0]);
				$dates[1] = strtotime($dates[1]);
				return $dates;
			} 
			
			$date = strtotime($date);
		}
		return $date;
	}
	
	public static function create($date) {
		return new GaDateTime($date);
	}
	
	public static function n($date) {
		return self::create($date);
	}
	
	function __construct($date = '') {
		$this->date = self::safeDate($date);
	}
	
	public static function now() {
		if (null == self::$now) {
			self::$now = time();
		}
		return self::$now;
	}
	
	public static function thisDay() {
		return strtotime(date('F j, Y', self::now()));
	}
	
	public static function thisMonth() {
		return strtotime(date('F 1, Y', self::now()));
	}
	
	public static function thisYear() {
		return strtotime(date('Y', self::now()));
	}
	
	public static function theDayTomorrow() {
		return strtotime(date('F j, Y', self::now() + self::$day));
	}
	
	public static function theNextDayTomorrow() {
		return strtotime(date('F j, Y', self::now() + 2*(self::$day)));
	}
	
	public static function diff($date1, $date2) {
		return ($date1->get() - $date2->get());
	}
	
	
	private static function parseFuncName($name) {
		$name = str_replace('_', ' ', $name);
		$name = str_replace('minus', '-', $name);
		$name = str_replace('plus', '+', $name);
		return $name;
	}
	
	public function __call($name, $args) {
		//echo self::parseFuncName($name);
		if (is_array($this->date)) {
			$this->date = strtotime(self::parseFuncName($name), $this->date);
		}
		$this->date = strtotime(self::parseFuncName($name), $this->date);
		return $this;
	}
	
	function get() {
		return $this->date;
	}
	
	function set($date) {
		$this->date =self::safeDate($date);
		return $this;
	}
	
	function friendlyFormat($date = NULL) {
		if ($date === NULL) {
			$date = $this->date;
		} else {
			$date = self::safeDate($date);
		}
		
		if (is_array($date)) {
			$out = $this->friendlyFormat($date[0]) . ' to ' .$this->friendlyFormat($date[1]);
			return $out;
		}		
		
		if ($date > self::thisDay() && $date < self::theDayTomorrow()) {
			return 'Today '. date('g:i A', $date);
		} elseif ($date > self::thisYear()) {
			return date('D M j, Y', $date); 
		} else {
			return date('D M j, Y', $date); 
		}
	}
	
	function htmlDateFormat() {
		$d = ' <span class="month">'.date('M', $this->date).'</span> '.
			'<span class="day">'.date('j', $this->date).'</span> '.
			'<span class="comma">,</span> '.
			'<span class="year">'.date('Y', $this->date).'</span> ';
		return $d;
	}
	
	/***
	 * Returns the date like Friday, October 3, 2008 12:00 AM 
	 **/
	/*for group events tab | Nick Aguilos February 10,2009 */
	function htmlEventDateFormat() {
		$d = '<span class="day">'.date('j', $this->date).'</span> '.
		 ' <span class="month">'.date('F', $this->date).'</span> '.
		'<span class="comma">,</span> '.
			'<span class="year">'.date('Y', $this->date).'</span> ';
		return $d;
	}
	
	function standardFormat() {
		return date('l, F j, Y g:i A', $this->date);
	}
	
	/**
	 * Returns the date like 2/30/08
	 **/
	function minFormat($delimiter = '/', $day_first = false) {
	    return date("n\\{$delimiter}j\\{$delimiter}y", $this->date  );
	}
	
	function __toString() {
		return $this->date . '';
	}

	/*** 
	 * @author: Naldz Castellano III
	 * Date : Aug. 29, 2007
	 * Checks if a given string is a valid date
	 * @param string $date - The string to be evaluated
	 * @return boolean - True if the given string is a valid date. Otherwise false.
	 ***/
	public static function isValidDate($date){
		try{
			$ts = strtotime($date);
			if(!$ts) return false;
			return true;
		}
		catch(exception $e){
			return false;
		}
	}
	
	/***
	 * @author: Naldz Castellano III
	 * Date : Aug. 29, 2007
	 * Compares two given dates.
	 * @param string $date1 - The first date string. Can also be a numeric timestamp
	 * @param string $date2 - The second date string. Can also be a numeric timestamp
	 * @return int 
	 * 	-1 if date1 is lesser than date2
	 * 	 0 if date1 and date2 are equal
	 * 	 1 if date1 is greater than date2
	 ***/

	public static function dateCompare($date1,$date2){
		if(!is_numeric($date1)){
			if(!self::isValidDate($date1)) throw new exception('Invalid first date parameter '.$date1.' passed to static function dateCompare of class GaDateTime.');
			$cvDate1 = strtotime($date1);
		}
		else{
			//if the date is numeric, assume that it is already a timestamp
			$cvDate1 = $date1;
		}
		if(!is_numeric($date2)){
			if(!self::isValidDate($date2)) throw new exception('Invalid second date parameter '.$date2.' passed to static function dateCompare of class GaDateTime.');	
			$cvDate2 = strtotime($date2);
		}
		else{
			//if the date is numeric, assume that it is already a timestamp
			$cvDate2 = $date2;
		}		
		if ($cvDate1 < $cvDate2) {	return -1;	}
		elseif($cvDate1 == $cvDate2){ return 0;	}
		else{ return 1;	}
	}
	
	/***
	 * @author: Naldz Castellano III
	 * Date: Aug. 29 2007
	 * Adds an interval to a given date string
	 * @param string $date - a valid date string. Can also be a numeric timestamp 
	 * @param string $datePart - the datepart in the interval will be based. Valid dateparts are as follows:
	 *	 months 	= m; 
     *   days 		= d; 
     *   years 		= y; 
     *   hours 		= h; 
     *   minutes 	= i; 
     *   seconds 	= s; 
	 * @return int - the date after the addition expressed in timestamp 
	 ***/

	public static function dateAdd($date,$datePart,$interval){
		if(!is_numeric($date)){
			if(!self::isValidDate($date)) throw new exception('Invalid date parameter '.$date.' passed to static function dateAdd of class GaDateTime.');
			if(!is_numeric($interval)) throw new exception('Invalid interval parameter '.$interval.' passed to static function dateAdd of class GaDateTime.');				
			$arTime = getDate(strtotime($date));
		}
		else{
			//if the date is numeric, assume that it is already a timestamp
			$arTime = getDate($date);
		}
		switch($datePart){
			case 'm':
				return mktime($arTime['hours'], $arTime['minutes'], $arTime['seconds'], $arTime['mon']+$interval, $arTime['mday'], $arTime['year']);
			break;
			case 'd':
				return mktime($arTime['hours'], $arTime['minutes'], $arTime['seconds'], $arTime['mon'], $arTime['mday']+$interval, $arTime['year']);
			break;
			case 'y':
				return mktime($arTime['hours'], $arTime['minutes'], $arTime['seconds'], $arTime['mon'], $arTime['mday'], $arTime['year']+$interval);
			break;
			case 'h':
				return mktime($arTime['hours']+$interval, $arTime['minutes'], $arTime['seconds'], $arTime['mon'], $arTime['mday'], $arTime['year']);
			break;
			case 'i':
				return mktime($arTime['hours'], $arTime['minutes']+$interval, $arTime['seconds'], $arTime['mon'], $arTime['mday'], $arTime['year']);
			break;
			case 's':
				return mktime($arTime['hours'], $arTime['minutes'], $arTime['seconds']+$interval, $arTime['mon'], $arTime['mday'], $arTime['year']);
			break;
			case 'w':
				
			break;
			default:
				throw new exception ('Invalid datepart parameter '.$datePart.' passed to static function dateAdd of class GaDateTime.');
		}	
	}
	
	/***
	 * @author: Naldz Castellano III
	 * Date: April 3, 2008
	 * Calculates the difference between two given dates.
	 * @param string $dateFrom - a valid date string. Can also be a numeric timestamp .
	 * @param string $dateTo   - a valid date string. Can also be a numeric timestamp.		
	 * @param string $datePart - the datepart in the interval will be based. Valid dateparts are as follows:
	 *	 years 		= y; 
     *   months 	= m; 
     *   days 		= d; 
     *   hours 		= h; 
     *   minutes 	= i; 
     *   seconds 	= s; 
	 * @return int - the difference between the two given dates based on the datepart
	 ***/
	//SECOND, MINUTE, HOUR, DAY, WEEK, MONTH, QUARTER, or YEAR
	public static function dateDiff($dateFrom, $dateTo, $datePart){
		require_once('Class.dbHandler.php');
		require_once('Class.GaString.php');
		$dateParts = array(
			'y' => 'YEAR',
			'm' => 'MONTH',
			'd' => 'DAY',
			'h'	=> 'HOUR',
			'i'	=> 'MINUTE',
			's'	=> 'SECOND'
		);
		if(!is_numeric($dateFrom)){
			if(!self::isValidDate($dateFrom)) throw new exception('Invalid date parameter '.$dateFrom.' passed to static function dateDiff of class GaDateTime.');
			$dateFromTs = strtotime($dateFrom);
		}
		else{
			$dateFromTs = $dateFrom;
		}
		if(!is_numeric($dateTo)){
			if(!self::isValidDate($dateTo)) throw new exception('Invalid date parameter '.$dateTo.' passed to static function dateDiff of class GaDateTime.');
			$dateToTs = strtotime($dateTo);
		}
		else{
			$dateToTs = $dateTo;
		}
		if(!array_key_exists($datePart,$dateParts)){
			throw new exception('Invalid datePart parameter '.$datePart.' passed to static function dateDiff of class GaDateTime.');
		}
		
		$dbDateFrom = self::dbDateTimeFormat($dateFromTs);
		$dbDateTo	= self::dbDateTimeFormat($dateToTs);
		$db = new dbHandler();
		$sql = 'SELECT TIMESTAMPDIFF('.$dateParts[$datePart].','.GaString::makeSqlSafe($dbDateFrom).','.GaString::makeSqlSafe($dbDateTo).') as dateDiff;';
		$rs = new iRecordset($db->execute($sql));
		return $rs->getDateDiff(0);
	}
	
	/***
	 * @author Naldz Castellano III
	 * Returns a datetime in a db datetime format
	 * Date: Oct. 24, 2007
	 */
	public static function dbDateTimeFormat($ts=null){
		if(!is_null($ts) && !is_numeric($ts)){
			$ts = strtotime($ts);
		}
		return is_null($ts) ? date("Y-m-d G:i:s") : date("Y-m-d G:i:s",$ts);
	}
	
	/***
	 * @author Naldz Castellano III
	 * Returns a date in a db date format
	 * Date: Oct. 24, 2007
	 */
	public static function dbDateFormat($ts=null){
		if(!is_null($ts) && !is_numeric($ts)){
			$ts = strtotime($ts);
		}
		return is_null($ts) ? date("Y-m-d") : date("Y-m-d",$ts);
	}
	
	
	/***
	* @author: Naldz Castellano III
	* Returns an array of intervals between the two dates.
	* Date: Feb. 11, 2009
	* @param string $startDate - a valid date string. Can also be a numeric timestamp .
	* @param string $endDate   - a valid date string. Can also be a numeric timestamp.
	* @param string $intervalType
	* @param string $intervalType - the intervalType. Valid intervalTypes are as follows:
    *   months 		= m; 
	* 	weeks		= w;
    *   days 		= d; 
	***/
	public static function getDateIntervals($startDate,$endDate,$intervalType){		
		$intervals = array();
		if('d' == $intervalType){
			$cont = true;
			$newDate = date("M d, Y",strtotime($startDate));
			while($cont){
				$intervals[] = $newDate;
				$newDate = date("M d, Y",GaDateTime::dateAdd($newDate,'d',1));
				$cont = GaDateTime::dateCompare($newDate,$endDate) <= 0;
			}
		}
		elseif('w' == $intervalType){
			$startSearchDate 	= GaDateTime::getFirstDayOfWeek($startDate);
			$endSearchDate		= GaDateTime::getLastDayOfWeek($endDate);
			$cont = true;
			$newDate = date("M d, Y",$startSearchDate);
			while($cont){
				$intervals[] = $newDate;
				$newDate = date("M d, Y",GaDateTime::dateAdd($newDate,'d',7));
				$cont = GaDateTime::dateCompare($newDate,$endSearchDate) < 0;
			}
		}
		elseif('m' == $intervalType){
			$cont = true;
			$newDate = date("M Y",strtotime($startDate));
			while($cont){
				$intervals[] = $newDate;
				$newDate = date("M Y", GaDateTime::dateAdd($newDate,'m',1));
				$cont = GaDateTime::dateCompare($newDate,$endDate) <= 0;
			}
		}
		return $intervals;
	}
	
	/***
		@author: Naldz Castellano
		@description: Returns the date of the first day of week. (Monday)
	***/
	
	public static function getFirstDayOfWeek($date){
		$dateTs = !is_numeric($date) ? strtotime($date) : $date;
		return GaDateTime::dateAdd($dateTs,'d',1-date('w',$dateTs));
	}
	
	/***
		@author: Naldz Castellano
		@description: Returns the date of the last day of week. (Sunday)
	***/
	public static function getLastDayOfWeek($date){
		$dateTs = !is_numeric($date) ? strtotime($date) : $date;
		return GaDateTime::dateAdd($dateTs,'d',7-date('w',$dateTs));
	}
	
	/***
		@author: Naldz Castellano
		@description: Returns the date of the first day of month
	***/
	public static function getFirstDayOfMonth($date){
		$dateTs = !is_numeric($date) ? strtotime($date) : $date;
		return GaDateTime::dateAdd($dateTs,'d',((date('j',$dateTs)-1) * -1));
	}
	
	/***
		@author: Naldz Castellano
		@description: Returns the date of the last day of month.
	***/
	public static function getLastDayOfMonth($date){
		$dateTs = !is_numeric($date) ? strtotime($date) : $date;
		return GaDateTime::dateAdd($dateTs,'d',date('t',$dateTs)-date('j',$dateTs));
	}

	///		@author Adelbert Silla 
	///		Returns a common date format
	///		Example: March 27, 2008 
	///		Date Added: July 10, 2008
    ///
	function commonDateFormat($date = NULL) {
		if ($date === NULL) {
			$date = $this->date;
		} else {
			$date = self::safeDate($date);
		}	
		if (is_array($date)) {
			$out = $this->friendlyFormat($date[0]) . ' to ' .$this->friendlyFormat($date[1]);
			return $out;
		}		
	
		if ($date > self::thisDay() && $date < self::theDayTomorrow()) {
			return 'Today '. date('g:i A', $date);
		} elseif ($date > self::thisYear()) {
			return date('F j, Y', $date); 
		} else {
			return date('F j, Y', $date); 
		}
	}
	
	
	/**
	* @return string difference of two dates in a form [difference] . part
	* usage: 
	*	GaDateTime::descriptiveDifference('2008-07-01 18:22:35', '2019-07-01 18:22:35')
	*   -- returns 11 Years
	*	GaDateTime::descriptiveDifference('2008-07-01 18:22:35', '2019-07-01 18:22:35', 'd', false)
	*	-- returns 4017 Days
	**/
	public static function descriptiveDifference($d1, $d2, $part = "s", $isRecursive = true){

		$conversion = array(
			'y' => array('description' => 'Year', 'next' => null),
			'm' => array('description' => 'Month', 'next' => 12),
			'd' => array('description' => 'Day', 'next' => date('t', (is_numeric($d1) ? $d1 : strtotime($d1)) )),
			'h'	=> array('description' => 'Hour', 'next' => 24),
			'i'	=> array('description' => 'Minute', 'next' => 60),
			's'	=> array('description' => 'Second', 'next' => 60)
		);

		$keys = array_keys($conversion);

		$diff = GaDateTime::dateDiff($d1, $d2, $part);
		$temp = array_flip($keys);	
		if(!is_null($conversion[$part]['next']) and $diff >= $conversion[$part]['next'] and $isRecursive)
			return self::descriptiveDifference($d1, $d2, $keys[$temp[$part]-1]);
		return $diff . ' ' . $conversion[$part]['description'] . ($diff > 1 ? 's' : '');
	}
	
	/**
	 * This method returns formatted date
	 * 
	 * @param string $format
	 * @param int $date
	 * @return string
	 */
	public static function formatDate($format=NULL, $date=NULL) {
		$newdate = !is_null($date) ? self::safeDate($date) : self::now();
		return ((!empty($format) && is_string($format)) ? date($format,$newdate) : $date);
	}
	
}
?>
