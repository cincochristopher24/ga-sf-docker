<?php
	/**
	 * @(#) Class.ResourceIterator.php
	 * 
	 * @package ganet.util
	 *
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - Jan 30, 2009
	 */
	
	/**
	 * Iterator for sql resource. Sql resource is the return type of mysql_query when you do
	 * SELECT and SHOW queries.
	 * <p>
	 * How to use this iterator:
	 * </p>
	 * 
	 * <p>
	 * 	The tbcontact table:
	 * </p>
	 * <code>
	 * 	------------------------------------
	 * 	| contactID | firstName | lastName |
	 * 	------------------------------------
	 * 	|     1     |  Antonio  |  Cruda   |
	 * 	------------------------------------
	 * 	|     2     |   Junix   |  Cruda   |
	 * 	------------------------------------
	 * 	|     3     |  Floricel | Colibao  |
	 * 	------------------------------------	
	 * </code>
	 * 
	 * <p>
	 * 	The Contact class: (Acceptable class construct of this iterator)	
	 * 	<code> 
	 * 		class Contact {
	 *			private $mContactID = 0;
	 *			private $mName = "";
	 *					
	 *			function __construct($params = 0){
	 *				if(is_numeric($params) AND 0 < $params) {
	 *					// query database
	 *				}
	 *			}
	 *
	 *			function getContactID(){
	 *				return $this->mContactID;
	 *			}
	 *
	 *			function getName(){
	 *				return $this->mName;
	 *			}
	 *
	 *			function initialize($props){
	 *				if (0 < count($props)) {	
	 *					$this->mContactID = isset($props['contactID']) ? $props['contactID'] : $this->mContactID;
	 *					$this->mName = isset($props['name']) ? $props['name'] : $this->mName;
	 *				}
	 *			}
	 *		}
	 *		</code>
	 *	</p>
	 * 
	 * <p>
	 * 	1. Iterate the resource using foreach.
	 * </p>
	 * 	<code>
	 * 		$result = mysql_query("SELECT contactID, firstName, lastName FROM tbcontact");
	 * 		$it = new SqlResourceIterator($result);
	 *  	foreach ($it as $key=>$value) {
	 * 			var_dump($value); // shows an array with keys contactID, firstName and lastName. 
	 *  	}
	 * 	</code>
	 * 
	 * <br />
	 * 
	 * <p>
	 * 	2. Iterate the resource using while loop.
	 * </p>
	 * 
	 * <p>Example 2.1</p>
	 * <code>
	 * 	$result = mysql_query("SELECT contactID, firstName, lastName FROM tbcontact");
	 * 	$it = new SqlResourceIterator($result);
	 *  while ($it->valid()) {
	 *  	echo $it->key(); // shows the key pointer of the current value in the iterator.
	 * 		var_dump($it->current()); // shows an array with keys contactID, firstName and lastName.
	 * 		$it->next(); // iterates the iterator
	 *  }	
	 * </code>
	 * 
	 * <br />
	 * 
	 * <p>
	 * 	3. Access iterator like an array.
	 * </p>
	 * <p>Example 3.0</p>
	 * <code>
	 *	$result = mysql_query("SELECT contactID, firstName, lastName FROM tbcontact");
	 *	$it = new SqlResourceIterator($result);
	 *	for ($i = 0; $i < count($it); $i++) {
	 *  	echo $it[$i]['contactID'];
	 * 		echo $it[$i]['firstName'];
	 * 		echo $it[$i]['lastName'];
	 * 	}
	 * </code>
	 * <p>Example 3.1</p>
	 * <code>
	 *	$result = mysql_query("SELECT contactID, name FROM tbcontact");
	 *	$it = new SqlResourceIterator($result, "Contact");
	 *	for ($i = 0; $i < count($it); $i++) {
	 *  	echo $it[$i]->getContactID();
	 * 		echo $it[$i]->getName();
	 * 	}
	 * </code>
	 * 
	 * <p>
	 * 	4. Gets the value of field in the current row.
	 * </p>
	 * <code>			
	 *	$result = mysql_query("SELECT contactID, firstName, lastName FROM tbcontact");
	 *	$it = new SqlResourceIterator($result);
	 *	$it->getfirstName(); // returns Antonio
	 *	$it->getFirstName(); // returns Antonio
	 *	$it->getLastName(); // returns Cruda
	 *	$it->next();
	 *	$it->getfirstName(); // returns Junix
	 *	$it->getFirstName(); // returns Junix
	 *	$it->next();
	 *	$it->current(); // returns array(
	 *									// 'contactID'=>3, 
	 *									// 'firstName'=>'Floricel'
	 *									// 'lastName'=>'Colibao'
	 *									// )
	 *	$it->next();
	 *	$it->getfirstName(); // throws method name does not exists exception. 
	 * </code>
	 * 
	 * <br />
	 * 
	 * <p>
	 *	5. You can use this iterator to retrieve each row in the resource.
	 * </p>
	 * <code>	
	 * 		$result = mysql_query("SELECT contactID, firstName, lastName FROM tbcontact");
	 * 		$it = new SqlResourceIterator($result);
	 * 		$contacts = array();
	 * 		while($it->valid()){
	 * 			$contacts[] = $it->current();
	 * 			$it->next();
	 * 		}
	 * </code>
	 * 
	 * <br />
	 * 
	 * <p>
	 * 	6. You can use this iterator so that it will return the object you wish. For this 3rd use, 
	 * 		this is only useful for classes that accepts array as parameter in its constructor. I assume 
	 * 		also that this array parameter is used to initialize the properties of the class.
	 * </p>
	 *		
	 *		<br />
	 *
	 *		<p>
	 * 			The caller file:
	 * 		</p>
	 * 		<code> 
	 * 			$result = mysql_query("SELECT contactID, name FROM tbcontact");
	 * 			$contacts = new SqlResourceIterator($result, "Contact");
	 * 			// if Contact class is not in the relative path, travellog/model, you can use
	 * 			// $contacts = new SqlResourceIterator($result, "Contact", the_relative_path);
	 * 			foreach ($contacts as $contact) {
	 * 				echo "Contact ID: ".$contact->getContactID();
	 * 				echo "Name: ".$contact->getName();
	 * 			}
	 * 		</code>
	 */
	class ResourceIterator implements Iterator, ArrayAccess {
		
		/**
		 * The class name of the expected object returned by next method.
		 * 
		 * @var string
		 */
		private $mClass = null;
		
		/**
		 * The storage for instantiated data.
		 * 
		 * @var array
		 */
		private $mData = array();
		
		/**
		 * The path of the expected object class name of object returned bu next method.
		 * 
		 * @var string
		 */
		private $mPath = "";
		
		/**
		 * The row pointer in the resource. This serves as a tracker for the row that
		 * will be retrieved.
		 * 
		 * @var integer
		 */
		private $mPtr = 0;
		
		/**
		 * The handler for the resource connection.
		 * 
		 * @var resource
		 */
		private $mResource = null;
		
		/**
		 * The number of rows in the resource.
		 * 
		 * @var integer
		 */
		private $mSize = 0;
		
		/**
		 * Constructs a new resource iterator for the given resource argument.
		 * 
		 * @throws exception Throws exception when the resource passed is not a valid mysql result.
		 * 
		 * @param resource $resource The result resource that is being evaluated. This result comes from a call to mysql_query().
		 * @param string $class The class name of the expected object that is returned by the next method. If this is not set the next method will return an array.
		 * @param string $path The absolute path of the class. The default path is travellog/model.
		 */
		function __construct($resource, $class = null, $path = "travellog/model") {
			if (is_resource($resource) AND 'mysql result' == get_resource_type($resource)) {
				$this->mResource = $resource;
				$this->mSize = mysql_num_rows($this->mResource);
				$this->mClass = $class;
				$this->mPath = preg_replace("/\/$/", "", $path);
				$this->mPath .= "/";
				
				if (!is_null($this->mClass)) {
					require_once($this->mPath."Class.".$this->mClass.".php");
				}
			}
			else {
				throw new exception("Invalid resource passed to ResourceIterator");
			}
		}
		
		/**
		 * Returns the value of a field in the current row.
		 * <p>
		 * Example:
		 * </p>
		 * 	<code>
		 *  
		 *	$query = mysql_query("SELECT a.name FROM tblGroup as a");
		 *	$it = new SqlResourceIterator($query);
		 *	$it->getName();  // Returns the name of the first row
		 *	$it->getname();  // Returns the name of the first row
		 *	$it->next();  
		 *	$it->getname();  // Returns the name of the first row
		 *	$it->next();
		 *	$it->getname();  // Returns the name of the second row
		 * 	
		 * 	</code>
		 * 
		 * @param string $field The field where to retrieve the value.
		 * @return mixed
		 */
		function __call($field, $arguments){
			if (preg_match("/^get/", $field)) {
				$field = preg_replace("/^get/", "", $field);
				$field = strtolower(substr($field, 0, 1)).substr($field, 1);
				if ($this->valid()) {
					mysql_data_seek($this->mResource, $this->mPtr);
					$array = mysql_fetch_assoc($this->mResource);
					
					if (isset($array[$field])) {
						return $array[$field];
					}
				}
			}
			
			throw new exception("Method name $field does not exist.");			
		}
		
		/**
		 * Returns the current element in the iterator. Returns false when the iterator pointer points to 
		 * an invalid key or key that exceeds the size of the iterator. The element returned is an instance 
		 * of the class passed in the constructor if it is set and array if its not.
		 * 
		 * @return Object|array|boolean the current element in the iterator; false when the iterator pointer.
		 */
		function current(){
			try {
				return $this->fetchData($this->mPtr);
			}
			catch (exception $ex) {
				return false;
			}		
		}
		
		/**
		 * Shows the content of the resource iterator.
		 * 
		 * @return void
		 */	
		function dump(){
			$class = $this->mClass;
			echo (!is_null($this->mClass)) ? "Class: ".$this->mClass."<br/>" : "";
			$this->mClass = null;
			echo "<table border='1'>";
			echo "	<tr>";
			while($obj = mysql_fetch_field($this->mResource))
				echo "	<th>".$obj->name."</th>";
			echo "	</tr>";
			while($this->valid()){
				echo "<tr>";
				$record = $this->current();
				foreach($record as $val){
					echo "<td>$val</td>";
				}
				echo "</tr>";
				$this->next();
			}
			echo "</table>";
			
			$this->mClass = $class;
		}
		
		/**
		 * Fetches the data in the given index from the resource. The element returned is an instance 
		 * of the class passed in the constructor if it is set and array if its not.
		 * 
		 * @param integer $index The index of the data in the resource to be fetched.
		 * 
		 * @return Object|array the element in the given index.
		 */
		private function fetchData($index){
			if ($this->offsetExists($index)) {
				if (!isset($this->mData[$index])) {
					mysql_data_seek($this->mResource, $index);
					$this->mData[$index] = mysql_fetch_assoc($this->mResource);
					if (!is_null($this->mClass)) {				
						$ref = new ReflectionClass($this->mClass);
						$obj = $ref->newInstanceArgs(array());
						$obj->initialize($this->mData[$index]);
						$this->mData[$index] = $obj;
					}
				}
				
				return $this->mData[$index];
			}
			else {
				return null;
			}		
		}
		
		/**
		 * Returns the current iterator pointer.
		 * 
		 * @return integer the current iterator pointer. 
		 */
		function key(){
			return $this->mPtr;
		}
		
		/**
		 * Points the iterator pointer to the next element of the iterator.
		 * 
		 * @return void
		 */
		function next(){
			if ($this->valid()) {
				$this->mPtr++;
			}
		}
		
		/**
		 * Returns true if the given index is a valid index of the resource iterator. 
		 * This is used for array access of the SqlResourceIterator.
		 * 
		 * @param integer $index The index to be tested if it is a valid index of the SqlResourceIterator.
		 * 
		 * @return boolean true if the given index is a valid index of the resource iterator; Otherwise returns false.
		 */
		function offsetExists($index){
			return (is_numeric($index) AND $index < $this->mSize AND $index >= 0); 
		}
		
		/**
		 * Returns the element in the given index. Returns null if the index doesn't exists.
		 * 
		 * @param integer $index The index of the element to be retrieved.
		 * 
		 * @throws exception if the index doesn't exists.
		 * 
		 * @return Object|array The element in the given index. Returns the object created from instantiating the class passed in the constructor if it has been instantiated; otherwise array.
		 */
		function offsetGet($index){
			try {
				return $this->fetchData($index);
			}
			catch (exception $ex) {
				throw $ex;
			}
		}
		
		/**
		 * Implemented from ArrayAccess. This is an invalid method of SqlResourceIterator.
		 * 
		 * @throws exception Do not call this method. This is only for ArrayAccess.
		 * 
		 * @return void
		 */
		function offsetSet($offset , $value){
			throw new exception("Invalid method called.");
		}
		
		/**
		 * Implemented from ArrayAccess. This is an invalid method of SqlResourceIterator.
		 * 
		 * @throws exception Do not call this method. This is only for ArrayAccess.
		 * 
		 * @return void
		 */
		function offsetUnset($offset){
			throw new exception("Invalid method called.");
		}
		
		/**
		 * Points the iterator pointer to the first element of the iterator.
		 * 
		 * @return void
		 */
		function rewind(){
			$this->mPtr = 0;
		}
		
		/**
		 * Returns the number of elements of the iterator.
		 * 
		 * @return integer the number of elements of the iterator.
		 */
		function size(){
			return $this->mSize;
		}
		
		/**
		 * Returns true when the iterator pointer points to a valid key in the iterator.
		 * 
		 * @return boolean true when the iterator pointer points to a valid key in the iterator; false otherwise.
		 */
    function valid(){
			return ($this->mPtr < $this->mSize);
    }
	}
