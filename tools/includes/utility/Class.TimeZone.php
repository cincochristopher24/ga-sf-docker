<?php
	/**
	* list of time zones
	* based on TimeZone.tblTimeZones
	**/
	
	require_once('Class.DataModel.php');
	
	class TimeZone extends DataModel {
		
		private static $gmtToIdentifier = array();
		
		public function __construct($id=null,$data=null){
			parent::initialize('TimeZone', 'tblTimeZones');
			if( $id && !$this->load($id) )
				throw new exception('Invalid timezoneID.');
			if( is_array($data) )
				$this->setFields($data);
		}
		
		/**
		* @param string in format [-+]dddd
		**/
		public static function getGmtToIdentifier($gmtOffset=0){
			if( empty(self::$gmtToIdentifier) ) self::initGmtToIdentifier();
			if( array_key_exists($gmtOffset, self::$gmtToIdentifier) )
				return self::$gmtToIdentifier[$gmtOffset];
			return array();
		}
		
		private static function initGmtToIdentifier(){
			self::$gmtToIdentifier = array();
			if( !function_exists('date_create') )
				return;
				
			foreach( self::fetch() as $row ){
				try{
					if( isset($_GET['dbhDebug']) ){
						echo '<pre>';
						print_r($row['timeZone']);
						print_r(new DateTimeZone($row['timeZone']));
						echo '</pre>';
					}
					$dz = new DateTimeZone($row['timeZone']);
					if( $dz instanceof DateTimeZone ){
						$offset = date_create('now', $dz)->format('O');
						if(!isset(self::$gmtToIdentifier[$offset]))
							self::$gmtToIdentifier[$offset] = array();
						self::$gmtToIdentifier[$offset][] = $row['timeZone'];
					}
				}
				catch(exception $e){
				}
			}
		}
		
		/**
		* @param string the time to be converted
		* @param string time zone identifier
		* @param int the offset of the remote machine
		* @return string converted time in m-d-Y G:i:s format
		*/
		public static function convertTimeToRemoteTime($dateTime, $tzID=null, $remoteOffset, $returnFormat=null){
			if( is_null($returnFormat) ){
				$returnFormat = "%m-%d-%Y %H:%M:%S";
			}
			
			$serverDateTime = new DateTime;
			$serverTZID		= $serverDateTime->getTimezone()->getName();
			$subjectDateStr	= $dateTime;
			$subjectDateTZID = $tzID;
			if( is_null($subjectDateTZID) )
				$subjectDateTZID = $serverTZID;
				
			// pear date package
			include_once( "Date.php");
			$pearDate = new Date($subjectDateStr);

			// set time zone to subject time zone
			$pearDate->setTZByID($subjectDateTZID);
			
			// format remoteOffset in GMT +/-xxxx
			$gmtOffset	= self::getGMToffsetString($remoteOffset);
			
			// get timezone ids in remoteOffset
			$arrTZIDS 	= self::getGmtToIdentifier($gmtOffset);
			$remoteTZID	= !empty($arrTZIDS) ? $arrTZIDS[0] : $serverTZID; // get one tz id from array or use server tz id
			
			// switch time zone to remote time zone
			$pearDate->convertTZByID($remoteTZID);
			
			if( array_key_exists('dbhDebug', $_GET) ){
				echo "FORMAT: $returnFormat<br />";
			}
			
			// converted time in m-d-Y G:i:s format
			$convertedTime = $pearDate->format($returnFormat);
			
			// switch time zone back to server time zone
			$pearDate->convertTZByID($serverTZID);
			return array('dateTime' => $convertedTime, 'timezone' => $remoteTZID);
		}
		
		private static function getGMToffsetString($offset){
			if( $offset ) {
				$offset = -$offset;
				$gmtOffsetStr = ( $offset > 0 ) ? '+' : '-';
				$gmtOffsetStr .= (($t=floor($offset / 60)) < 10 ? '0'.$t :$t);
				$gmtOffsetStr .= (($offset % 60) < 10 ) ? '0' . $offset % 60 : $offset % 60;
				return $gmtOffsetStr;
			}
		}
		
		public static function getGrouped($options=array()){
			
			$arr = array();
			$date = date('Y-m-d');
			
			
			foreach( self::fetch($options) as $k => $row ){
				try{
					$s = substr(date_create($date, new DateTimeZone($row["timeZone"]))->format('O'),0,3);
					//echo "$k --> $s<br />";
					if( !in_array($s, $arr) ){
						$arr[$row["id"]] = (int) $s;
					}
				}
				catch(exception $e){
					// catch bad timezone exception
				}
				
				if( count($arr) > 28 ){ 
					break;
				}
				
			}
			$temp = array_flip($arr);
			$gmts = array_keys($temp);
			sort($gmts);
			$arr = array();
			foreach( $gmts as $each ){
				$arr[$temp[$each]] = 'GMT ' . ($each>= 0 ? '+':'').$each;
			}
			return $arr;
		}
		
		
		public static function getGrouped2($options=array()){
			$continents = array('Africa','America','Antarctica','Arctic','Asia','Atlantic','Australia','Europe','Indian','Pacific');
			$arr = array();
			foreach( $continents as $v )
				$arr[$v] = array();
			$arr['Others'] = array();
			foreach( self::fetch($options) as $row ){
				$timezone 	= $row['timeZone'];
				$temp		= explode('/',$timezone);
				$continent 	= isset($temp[0]) ? $temp[0] : '';
				$sub1 		= isset($temp[1]) ? $temp[1] : '';
				$sub2 		= isset($temp[2]) ? $temp[2] : '';
				$s = '';
				
				if( '' != $sub1 )
					$s = $sub1 . ('' != $sub2 ? '/' . $sub2 : '');
				else
					$s = '' != $sub2 ? $sub2 : '';
				
				
				if( in_array($continent, $continents) ){
					if( '' != $s )
						$arr[$continent][$row['id']] = $s; 
				}
				else{
					$arr['Others'][$row['id']] = $timezone;
				}
			}
			return $arr;
		}
		
		public static function findInstances($options=array()){
			$arr= array();
			foreach( self::fetch($options) as $row ){
				$arr[] = new self(null,$row);
			}
			return $arr;
		}
		
		private static function fetch($options = array()){
			$default = array(
				'TIMEZONE'	=> null,
				'ORDER_BY'	=> null,
				'LIMIT'		=> null
			);
			$options= array_merge($default,$options);
			$db		= new dbHandler('db=TimeZone');
			$sql 	= 'SELECT * FROM TimeZone.tblTimeZones WHERE 1 ';
			if( !is_null($options['TIMEZONE']) )
				$sql .= 'AND tblTimeZones.timeZone = ' . GaString::makeSqlSafe($sql) . ' ';
			if( !is_null($options['ORDER_BY']) )
				$sql .= 'ORDER BY ' . $options['ORDER_BY'] . ' ';
			if( !is_null($options['LIMIT']) )
				$sql .= 'LIMIT ' . $options['LIMIT'];
			$rs = new iRecordset($db->execute($sql));
			return $rs;
		}
		
		// this will truncate current data in TimeZone.tblTimeZones and replace it with the ones in the timezone identifier list
		public static function reloadDatabase(){
			// disable this function
			return;
			if( function_exists('timezone_identifiers_list') ){
				$db 	= new dbHandler('db=TimeZone');
				$sql 	= "TRUNCATE TABLE `TimeZone`.`tblTimeZones`";
				$db->execute($sql);
				foreach( timezone_identifiers_list() as $zone ){
					$tz = new self;
					$tz->setTimeZone($zone);
					$tz->save();
				}
			}
		}

	}

?>