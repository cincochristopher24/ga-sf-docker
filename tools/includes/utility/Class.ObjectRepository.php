<?php
	/**
	 * Enhanced version of ObjectsRepo class. This supports all class names.
	 * 
	 * @package ganet.util
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - March 17, 2009
	 */
	
	/**
	 * Storage class of objects created. This is useful for preventing re-instantiation 
	 * of the same object especially for models.
	 * <p>How to use (Assume a Contact class exists):</p>
	 * <code>
	 * 	$contact = new Contact(12);
	 * 	$repo = ObjectRepository::instance();
	 * 	$repo->storeObject($contact, $contact->getContactID);
	 * </code>
	 * 
	 */
	class ObjectRepository {
		
		/**
		 * The array where the objects created is stored.
		 * 
		 * @var array
		 */
		private $mRepository = array();
		
		/**
		 * The instance of the ObjectRepository. This is used for the singleton
		 * pattern of this class.
		 * 
		 * @var ObjectRepository
		 */
		private static $sInstance = null;
		
		/**
		 * Constructs a new ObjectRepository object. Scope is private to create a singleton
		 * pattern implementation. Create instance of this class by calling ObjectRepository::instance().
		 */
		private function __construct(){}
		
		/**
		 * Returns the instance of this class.
		 * 
		 * @return ObjectRepository the instance of this class.
		 */
		public static function instance(){
			if (is_null(self::$sInstance)) {
				self::$sInstance = new ObjectRepository();
			}
			
			return self::$sInstance;
		}
		
		/**
		 * Returns true if the given object with primary key equal to the given primary key 
		 * exists in the object repository.
		 * 
		 * @param string $class The class name of the object to be tested if it exists.
		 * @param integer|string $primary The unique key or ID of the object model.
		 * 
		 * @return boolean true if the given object with primary key equal to the given primary key exists in the object repository.
		 */
		function objectExists($class, $primary){
			return isset($this->mRepository[$class][$primary]) ? true : false;
		}
		
		/**
		 * Returns the object with class name equal to the given class and with unique key
		 * equal to the given unique key.
		 * 
		 * @param string $class The class name of the object to be retrieved.
		 * @param integer|string $primary The unique key or ID of the object model.
		 * 
		 * @return mixed(Object) object with class name equal to the given class and with unique key
		 * 											 equal to the given unique key; null if the class name with ID equal to
		 * 											 the given ID is does not exists as keys in the repository.
		 */
		function retrieveObject($class, $primary){
			return ($this->objectExists($class, $primary)) ? $this->mRepository[$class][$primary] : null;
		}
		
		/**
		 * Stores the given object.
		 * 
		 * @param mixed(Object) $object The object to be stored.
		 * @param mixed $primary The unique key or ID of the object model. 
		 * 
		 * @return void
		 */
		function storeObject($object, $primary){
			$this->mRepository[get_class($object)][$primary] = $object;
		}
		
	}
	
