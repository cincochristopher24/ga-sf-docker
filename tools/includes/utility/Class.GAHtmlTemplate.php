<?php
/**
 * Created on Jul 3, 2007
 * 
 * @author     Wayne Duran
 */

require_once 'Class.GATemplate.php';
require_once 'Minify.php';

class GAHtmlTemplate extends GATemplate {
	
	private   static $template_files        = array();
	private   static $main_template         = NULL;
	private   static $outStack = 0;
	protected static $main_template_vars    = array();
	protected static $dependents            = array();
	protected static $dependents_body       = array();
	protected static $displayLog            = false;
	protected static $forceNotToDisplayLog  = NULL;
	protected static $time_start            = NULL;
	protected static $default_template_path = '';
	protected static $default_js_location   = '';
	protected static $default_css_location  = '';
	protected static $vars_used             = array();
	protected static $minify                = false;
	protected static $minify_cache_dir      = NULL;
	protected        $use_main_template     = true;
	protected static $default_dependent_ops = array(
		'bottom'        => false,
		'id'            => null,
		'include_here'  => false,
		'return_string' => false 
	);
	
	public function __construct($path = NULL) {
		parent::__construct();
		if (self::$main_template) {
			$this->use_main_template = true;
		}
		if (NULL == self::$time_start) {
			self::$time_start = microtime(true);
		}
		if (NULL !== $path) {
			$this->setPath($path);
		}
	}
	

	public function set($name, $value) {
		parent::set($name, $value);
		$type = gettype($value);
		self::$vars_used[] = array($name, $type);
	}
	
	
	public static function setMainTemplate($tpl) {
		if (is_string($tpl)) 
			self::$main_template = $tpl;
	}
	
	/**
	 * Sets the directory where the
	 * minified and compiled files will be placed
	 *
	 * @return void
	 **/
	public static function setMinifyCacheDir($directory)
	{
		self::$minify_cache_dir = $directory;
	}
	
	/**
	 * Returns the directory where the minified
	 * and compiled files will be placed
	 *
	 * @return void
	 **/
	public static function getMinifyCacheDir()
	{
		return self::$minify_cache_dir;
	}
	

	/**
	 * Use this to set whether you'll use a main template or not
	 * @return boolean 
	 */
	function useMainTemplate($bool = true) {
		$this->use_main_template = $bool;
	}
	
	
	function doNotUseMainTemplate() {
		$this->useMainTemplate(false);
	}
	
	
	public static function setMainTemplateVar($name, $value) {
		self::$main_template_vars[$name] = $value;
		$type = gettype($value);
		self::$vars_used[] = array($name, $type);
	}
	
	public function getMainTemplateVar($name)
	{
		if (array_key_exists($name, self::$main_template_vars)) {
			return self::$main_template_vars[$name];
		}
		return null;
	}
	
	public static function includeDependent($str, $options = array()) {
		$options = array_merge(self::$default_dependent_ops, $options);
		if ($options['bottom']) {
			if ($options['id']) {
				self::$dependents_body[$options['id']] = $str;
			} else {	
				self::$dependents_body[$str] = $str;
			}
		} elseif (array_key_exists('top', $options) && $options['top']){
			if ($options['id']) {
				self::$dependents_body[$options['id']] = $str;
			} else {	
				self::$dependents = array_merge(array($str => $str), self::$dependents); 
			}
		} elseif ($options['include_here']) {
			echo $str;
		} elseif ($options['return_string']) {
			return $str;
		} else {
			if ($options['id']) {
				self::$dependents[$options['id']] = $str;
			} else {	
				self::$dependents[$str] = $str;
			}
		}
	}
	
	public static function unsetDependent($key) {
		unset (self::$dependents[$key]);
	}
	
	
	static function setDefaultJsPath($location) {
		self::$default_js_location = $location;
	}
	
	
	static function setDefaultCssPath($location) {
		self::$default_css_location = $location;
	}
	
	
	public static function includeDependentJs($script_file, array $options = array()) {
		// Default options:
		$def_options = array ('minify'=>true, 'defer'=>false);
		$options = array_merge($def_options, $options);
		
		if (is_array($script_file)) {
			if (self::$minify) {
				$params = array();
				$scripts = array();
				foreach ($script_file as $file) {
					if (strpos($file,'?')>0) {
						$f = explode('?', $file);
						$scripts[] = $f[0];
						$params[]  = $f[1];
					} else {
						$scripts[] = $file;
					}
				}
				if (count($params)) {
					return self::includeDependentJs(implode(',', $scripts).'&'.implode('&', $params), $options);
				} else {
					return self::includeDependentJs(implode(',', $scripts), $options);
				}
			} else {
				$test = array();
				foreach ($script_file as $file) {
					$test[] = self::includeDependentJs($file, $options);
				}
				if ($test[0]) {
					return implode("\n", $test);
				}
			}
		} else {
			if (!preg_match('/[^:\?#]+:\/\//', $script_file) & strpos($script_file, '/') !== 0) {
				$script_file = self::$default_js_location.$script_file;
			}
		
			if (self::$minify && $options['minify']) {
				$script_file = str_replace('?', '&', $script_file); // we already have a '?' when appending it to the minify script below
				$script_file = '/minify_f.php?files='.$script_file;
			}
			return self::includeDependent('<script src="'.htmlentities($script_file).'" type="text/javascript"'.
				($options['defer'] ? ' defer="defer"':'') .
				'></script>', $options
			);
		}
	}

	
	public static function includeDependentCss($css_file, $options = array()) {
		if (!preg_match('/[^:\?#]+:\/\//', $css_file) & strpos($css_file, '/') !== 0) {
			$css_file = self::$default_css_location.$css_file;
		}
		
		if(!isset($options['media'])) {
			$options['media'] = 'screen';
		}
		
		if (isset($options['@import']) && $options['@import']) {
			self::includeDependent('<style type="text/css" media="'.$options['media'].'">@import url("'.$css_file.'");</style>');
		} else {
			self::includeDependent('<link href="'.$css_file.'" media="'.$options['media'].'" rel="stylesheet" type="text/css" />');
		}
	}

	/**
	 * Flush the list of dependents. Use with caution.
	 * clearDependents()' side-effects are undoable.
	 * You must call includeDependents again to include
	 * your dependents
	 */
	protected static function clearDependents() {
		self::$dependents = array();
		self::$dependents_body = array();
	}
	
	/**
	 * Get the minify settings
	 *
	 * @return boolean
	 **/
	public static function isMinify()
	{
		return self::$minify;
	}
	
	/**
	 * Set the minify setting
	 *
	 * @param value boolean
	 * @return void
	 **/
	public static function setMinify($value)
	{
		self::$minify = $value;
	}
	
	public function includeInHead($str) {
		self::includeDependent($str);
	}
	
	private function addIncludes($output) {
		$str = '';
		foreach (self::$dependents as $dependent) {
			$str .= "$dependent\n";
		}
		return str_replace('</head>', $str.'</head>', $output);
	}
	
	private function addBodyIncludes($output) {
		$str ='';
		foreach (self::$dependents_body as $dependent) {
			$str .= "$dependent\n";
		}
		return str_replace('</body>', $str.'</body>', $output);
	}
	
	
	public function addLog($output) {
	
		self::includeDependent(
			'<style type="text/css">' .
			'html body #Template_Class_Log {' .
			'padding: 2em; background-color: white; text-align: center;}' .
			'html body #Template_Class_Log h1 {' .
			'font-size: 16px; margin: 1em 0; }' .
			'html body #Template_Class_Log table {' .
			'background-color: #ccc; text-align: left; margin: 0 auto;}' .
			'html body #Template_Class_Log * {' .
			'font: 10px "Lucida Grande", Verdana, Arial, Helvetica, sans-serif;' .
			'color: #333;background-color: #fff;}' .
			'html body #Template_Class_Log table th {' .
			'font-weight: bold; background-color: #efefef; text-align: right}' .
			'html body #Template_Class_Log table td {' .
			'background-color: #fff;}' .
			'html body #Template_Class_Log table td ul {' .
			'list-style-type: square; margin-top: 0; padding-left:1.3em}' .
			'</style>'
		);
		
		$execution_time = microtime(true) - self::$time_start;
		$out = '<div id="Template_Class_Log"><h1>Template Log</h1>';
		$out .= '<table cellpadding="4" cellspacing="1" summary="A table of information of the template files loaded and the variables used on the templates">';
		$out .= '<tr><th scope="row" align="right" valign="top">Execution Time:</th><td>'. $execution_time .' second(s)</td></tr>';		
		$out .= '<tr><th scope="row" align="right" valign="top">Default Template Path:</th><td>'. self::$default_template_path .'</td></tr>';
		$out .= '<tr><th scope="row" align="right" valign="top">Templates Used:</th><td><ul>';
		foreach (self::$template_files as $template => $tplval) {
			$out .= "<li>$template</li>";
		}
		$out .= '</ul></td></tr>' .
				'<tr><th scope="row" align="right" valign="top">Main Template:</th>' .
				'<td>'. self::$main_template . '</td></tr>';
		$out .= '<tr><th scope="row" align="right" valign="top">Variables Used:</th><td><ul>';
		foreach (self::$vars_used as $var) {
			$out .= "<li>$var[0] ($var[1])</li>";
		}
		$out .= '</ul></td></tr>';
		$out .= '</table></div>';
		$bodytag = strrpos($output, '</body>');
		if($bodytag > 0) {
			$output = str_replace('</body>', $out.'</body>',$output);
		} else {
			$output .= $out;
		}
		return $output;
		
	}
	
	
	public function disableLogging() {
		self::$forceNotToDisplayLog = true;
	}
	
	
	public static function displayLog($bool = true) {
		self::$displayLog = $bool;
	}
	
	public function fetch($file = NULL) {
		if ($file) {
		    self::$template_files[$this->path.$file] = '';
		} else {
		    self::$template_files[$this->path.$this->template_file] = '';
		}
		return parent::fetch($file);
	}
	
	public static function isExistDependent($dependent) {
		if (!preg_match('/[^:\?#]+:\/\//', $dependent) & strpos($dependent, '/') !== 0) {
			$dependent = self::$default_js_location.$dependent;
		}
		$dependent ='<script src="'.htmlentities($dependent).'" type="text/javascript"></script>';
		return in_array($dependent, self::$dependents);
	}
	
	function _e($string) {
	  return htmlspecialchars($string);
	}
	
	/** 
	* Outputs the template and contents. When a main template
	* is detected, that template is used and 'wraps' around the
	* $file template passed. Dependent snippets are automatically
	* included
	* 
	* @param string string the template file name
	* @return void
	* @author Wayne Duran
	*/
	function out($_file = NULL) {
		self::$outStack += 1;
		$_output = $this->fetch($_file);
		self::$outStack -= 1;
		if (self::$main_template && self::$outStack === 0) {
			if ($this->use_main_template) {
				$_mtpl = new self();
				$_mtpl['contents'] = $_output;
				if (array_key_exists('layoutID', self::$main_template_vars)) {
					$_mtpl['bodyHtmlID'] = str_replace('/', '_s_', self::$main_template_vars['layoutID']);
				}
				$_mtpl->setVars(self::$main_template_vars);
				$_output = $_mtpl->fetch(self::$main_template);
				$_mtpl = NULL;
				if (self::$displayLog && self::$forceNotToDisplayLog !== TRUE) {
					$_output = $this->addLog($_output);
				} elseif (!self::$displayLog) {
					self::unsetDependent('Template_Class_Log_Style');
				}
				if (isset($title)) {
					$str_output = preg_replace('/(<title[^>]*>)(.*?)(<\/title>)/i', "$1$2 &raquo; $title$3", $str_output);
				}
				$_output = $this->addBodyIncludes($this->addIncludes($_output));
			}
		}
		echo $_output;
	}
}

?>
