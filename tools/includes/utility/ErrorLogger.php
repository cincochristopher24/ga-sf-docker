<?php

	require_once('Class.ErrorLog.php');
	
		
	function error_handler($error,$message,$file,$line, $vars)
	{
	    if($error & error_reporting())
	    {
	        $log=array();
	        switch($error)
	        {
	            case E_ERROR:
	                $type='FATAL';
	                break;
	            case E_WARNING:
	                $type='ERROR';
	                break;
	            case E_NOTICE:
	                $type='WARNING';
	                break;
	            default:
	                $type='Unknown error type ['.$error.']';
	                break;
	        }
	        
	        if (!ErrorLog::isExistLogSite($_SERVER['SERVER_NAME']))
	        	ErrorLog::addLogSite($_SERVER['SERVER_NAME']);
	        
	        $logSiteID = ErrorLog::getLogSiteID();
	        
	        $errorLog = new ErrorLog();
	    	$errorLog->setLogSiteID($logSiteID);
	    	$errorLog->setErrorType($type);
	    	$errorLog->setMessage(htmlentities($message));
	    	$errorLog->setScriptName($file);
	    	$errorLog->setLineNum($line);
	    	if (isset($_SERVER['REMOTE_ADDR']) && strlen($_SERVER['REMOTE_ADDR']))
	    		$errorLog->setRemoteAddress(htmlentities($_SERVER['REMOTE_ADDR']));
	    	if (isset($_SERVER['HTTP_REFERER']) && strlen($_SERVER['HTTP_REFERER']))
	    		$errorLog->setReferer(htmlentities($_SERVER['HTTP_REFERER']));
	    	if (isset($_SERVER['HTTP_USER_AGENT']) && strlen($_SERVER['HTTP_USER_AGENT']))
	    		$errorLog->setBrowser(htmlentities($_SERVER['HTTP_USER_AGENT']));
	    	if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']))
	    		$errorLog->setQueryString(htmlentities($_SERVER['QUERY_STRING']));
	    	if (isset($_SERVER['REQUEST_METHOD']) && strlen($_SERVER['REQUEST_METHOD']))
	    		$errorLog->setMethod(htmlentities($_SERVER['REQUEST_METHOD']));
	    	if (isset($_SERVER['SCRIPT_NAME']) && strlen($_SERVER['SCRIPT_NAME']))
				$errorLog->setFileName(htmlentities($_SERVER['SCRIPT_NAME']));
			if (isset($_SESSION['travelerID']))
				$errorLog->setLoggedTravID($_SESSION['travelerID']);
		/*	if (isset($_POST))
				$errorLog->setPostValues($_POST);*/
							
	    	$errorLog->Create();
	    	
	    	if ($type!='WARNING'):
		    	$errorLog->sendEmail();
				//header("location: /ticket.php?action=show_form");
		    endif;
	    }
	}
	
	
	function exception_handler($e)
	{ 
		ErrorLog::setException($e);
	 	//ErrorLog::showTicketForm($e);
	}
	
	set_error_handler('error_handler');
	
	set_exception_handler('exception_handler');

?>