<?php
	/*
	** Quick solution for getting the last THREE pages visited by a user.
	** This class is designed for the said purpose only.
	*/
	require_once('travellog/model/Class.SessionManager.php');

	class LastVisitedPages{
		
		const MAX_SIZE 			= 3;
		
		public static function insert($url){
			if(!strpos($url,'ticket.php') != -1) // exclude ticket.php
				return;
				
			$obj_session = SessionManager::getInstance();
			$pageArray = array();
			$size = 0;	$top=0;
			if($obj_session->get('browsing_history')){
				$pageArray = $obj_session->get('browsing_history');
				$size = sizeof($pageArray);
				$top = $size-1;
			}
			
			if(isset($pageArray[$top]))
				if($pageArray[$top] == $url)
					return;
			if($size >= LastVisitedPages::MAX_SIZE){
				for($i=0;$i<(LastVisitedPages::MAX_SIZE-1);$i++){
					$pageArray[$i]	=	$pageArray[($i+1)];
				}
				$pageArray[$top]	=	$url;				
			}else{
				$pageArray[] = $url;
			}
			
			$obj_session->set('browsing_history',$pageArray);
		}
		
		public static function getHistory(){
			return SessionManager::getInstance()->get('browsing_history');
		}
		
		
	}
