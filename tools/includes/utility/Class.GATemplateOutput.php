<?php
/**
 * Created on Jul 18, 2007
 * 
 * @author     Wayne Duran
 */


class GATemplateOutput {
	
	private $parent = NULL;
	private $content         = NULL;
	
	public function __construct($content = NULL, $parent = NULL) {
		$this->setContent($content);
		if (!is_null($parent))
			$this->setParent($parent);
	}
	
	public function setParent(GATemplateOutput $parent) {
		$this->parent = $parent;
	}
	
	public function getParent () {
		return $this->parent;
	}
	
	public function getContent() {
		return $this->content;
	}
	
	public function getContents() {
		return $this->getContent();
	}
	
	public function setContent($content) {
		if (!is_null($content) && is_string($content)) {
			$this->content = $content; 
		}
	}
	
	public function __toString() {
		return $this->content;
	}
}

?>
