<?php
	require_once('Class.dbHandler.php');
	class DbHostResolver
	{
		static private $instance = null;
		static public function create($host,$user,$password){
			if(is_null(self::$instance)){
				self::$instance = new self($host,$user,$password);
			}
			return self::$instance;
		}
		
		private $mDbHost = null;
		private function __construct($host,$user,$password,$db=null){
			require_once('gaLogs/Class.DataTracker.php');
			DataTracker::create()->beginNoTrackingSection();
				try{
					//$db = new dbHandler("host=$host;uid=$user;pwd=$password;db=$db");
					//we have to do a separate mysql_connect rather than using the utility layer due to wrong abstraction.
					$db = new dbHandler(mysql_connect($host,$user,$password));
					$rsOffset = new iRecordset($db->execute('SELECT @@auto_increment_offset as autoIncrementOffset;'));
				}
				catch(exception $e){
					DataTracker::create()->endNoTrackingSection();
					throw $e;
				}
			DataTracker::create()->endNoTrackingSection();
			$this->mDbHost = 'eth0.ga_database0'.$rsOffset->getAutoIncrementOffset(0);
		}
		
		public function getHost(){
			return $this->mDbHost;
		}
	}
?>