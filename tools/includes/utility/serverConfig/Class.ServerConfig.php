<?php
	/***
		dataSource = localDataSource.ini

		[mail]
		disabled = 1
		host = ""

		[template]
		showInfo = 0
		jsMinify = 0
	***/
	
	class ServerConfig
	{
		//implement singleton pattern
		static private $instance = null;
		static public function getInstance(){
			if(is_null(self::$instance)) self::$instance = new self;
			return self::$instance;
		}
		
		private $mConfig = array();
		private function __construct(){
			//set the default config
			$this->mConfig = parse_ini_file('defaultConfig.ini',true);
			$this->mConfig['db'] = parse_ini_file('dataSources/'.$this->mConfig['dataSource']);
			
			//override the default config
			if(file_exists('/serverConfig/serverConfig.ini')){
				$serverConfig = parse_ini_file('/serverConfig/serverConfig.ini',true);
				$this->mConfig = $this->deepArrayMerge($this->mConfig,$serverConfig);
				$this->mConfig['db'] = parse_ini_file('dataSources/'.$serverConfig['dataSource']);
			}
			
			//override the config with the domain config
			//domain configs are not allowed to override the serverType property
			$serverType = $this->mConfig['serverType'];
			
			if(array_key_exists('SERVER_NAME',$_SERVER)){
				$domainName = $_SERVER['SERVER_NAME'];
				if(file_exists(dirname(__FILE__).'/domains/'.strtolower($domainName).'.ini')){
					$domainConf = parse_ini_file('domains/'.strtolower($domainName).'.ini',true);
					$this->mConfig = $this->deepArrayMerge($this->mConfig,$domainConf);
					$this->mConfig['db'] = parse_ini_file('dataSources/'.$domainConf['dataSource']);
				}
			}
			
			$this->mConfig['serverType'] = $serverType;
			
			//if load balancing has been enabled
			if($this->mConfig['db']['loadBalance']){
				require_once('Class.DbHostResolver.php');
				$this->mConfig['db']['host'] = DbHostResolver::create($this->mConfig['db']['host'],$this->mConfig['db']['user'],$this->mConfig['db']['password'])->getHost();
			}
		}
		
		private function deepArrayMerge($ar1,$ar2){
			foreach($ar2 as $key => $val){
				if(is_array($val) && (array_key_exists($key,$ar1) && is_array($ar1[$key]))){
					$ar1[$key] = $this->deepArrayMerge($ar1[$key],$val);
				}
				else{
					$ar1[$key] = $val;
				}
			}
			return $ar1;
		}
		
		public function getServerType(){
			return $this->mConfig['serverType'];
		}
		
		/*** Database ***/
		public function getDbHost(){
			return $this->mConfig['db']['host'];
		}
		
		public function getDbUser(){
			return $this->mConfig['db']['user'];
		}
		
		public function getDbPassword(){
			return $this->mConfig['db']['password'];
		}
		
		public function getDefaultDatabase(){
			return $this->mConfig['db']['defaultDatabase'];
		}
		
		/*** Mail ***/
		public function isMailDisabled(){
			return $this->mConfig['mail']['disabled'];
		}
		
		public function getMailHost(){
			return $this->mConfig['mail']['host'];
		}
		
		/*** Template ***/
		public function isTemplateInfoShown(){
			return $this->mConfig['template']['showInfo'];
		}
		
		public function isJsMinified(){
			return $this->mConfig['template']['jsMinify'];
		}
		
		/** Memcached Servers **/
		public function getMemcachedServers(){
			return $this->mConfig['memcached_servers'];
		}
		
	}
?>