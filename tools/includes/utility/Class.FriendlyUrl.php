<?php
	/**
	 * @(#) Class.FriendlyUrl.php
	 * 
	 * @author Antonio Pepito Cruda Jr.
	 * @version 1.0 - March 9, 2009
	 */
	
	/**
	 * Class that handles encoding and decoding of friendly urls.
	 * 
	 * GANET Friendly URL Consortium:
	 * 	1. A valid string that is GANET Friendly URL convertible is a string that has no
	 * 		'+' characters.
	 * 	2. All space characters in a string will be converted to '-' character.
	 * 	3. All '-' characters will be converted to '_' character.
	 * 	4. All '_' characters will be converted to '+' character. 
	 */
	class FriendlyUrl {
		
		/**
		 * Encodes a string to valid friendly url string.
		 * 
		 * @param string $url The url to be encoded.
		 * 
		 * @return string a valid friendly url string derived from the given url.
		 */
		static function encode($url){
			// $friendlyURL = preg_replace("/_/", "+", $url);
			$friendlyURL = preg_replace("/_/", '—', $url) ; // convert underscore to &mdash; do not touch.
			$friendlyURL = preg_replace("/-/", "_", $friendlyURL);			
			return preg_replace("/ /", "-", $friendlyURL);
		}
		
		/**
		 * Converts a friendly url string to its original string.
		 * 
		 * @param string $url The friendly url to be decoded.
		 * 
		 * @return string original string of the friendly url.
		 */
		static function decode($url){
			$friendlyURL = preg_replace("/-/", " ", $url);
			$friendlyURL = preg_replace("/_/", "-", $friendlyURL) ;			
			$friendlyURL = preg_replace("/—/", "_", $friendlyURL) ;	// convert &mdash; back to underscore. do not touch.		
			// return preg_replace("/\+/", "_", $friendlyURL);			
			return $friendlyURL;
		}
		
		/**
		 * Checks if a string is friendly url convertible or there are no characters that
		 * violates GANET friendly url consortium.
		 * 
		 * @param string $string The string to be tested if it violates GANET friendly url consortium.
		 * 
		 * @return boolean true if the given string does not violate GANET friendly url consortium.
		 */
		static function isFriendlyUrlConvertible($string){
			return preg_match("/\+/", $string);
		}
	}