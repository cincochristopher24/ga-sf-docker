<?php

	class EmailDomain {
		
		static $BLACKLISTED = array(
			'163.com', '162.com', 'yeah.net'
		);                     
		
		static function isBlackListed($email){
			$emailParts = explode('@', $email);
			if(isset($emailParts[1])){
				return in_array(strtolower($emailParts[1]), self::$BLACKLISTED);
			}
			
			return false;
		}
		
	}