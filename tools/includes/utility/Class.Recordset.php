<?
/**
* <b>Database Results</b> class.
* @author Aldwin S. Sabornido
* @version 1.0 PHP5
* @see http://www.GoAbroad.com/
* @copyright 2006-2007
*/

//require_once('caching/memcache.class.php');
/***
 * Added by naldz for db data tracking
 * Date: Dec. 12, 2007
 */
require_once('gaLogs/Class.DataTracker.php');
class Recordset
{
	protected $connection;
	protected $rs; 
	
	function __construct(Connection $obj)
	{
		//if(Cache::get('dbconnection-cache')){
		//	$this->connection = Cache::get('dbconnection-cache');
		//}else{
			$this->connection = $obj;
		//	Cache::set('dbconnection-cache',$obj);
		//}
		
		
	}
	
	function Execute($query)
	{
		mysql_select_db($this->connection->GetDBName(),$this->connection->GetConnection()) or die(mysql_error()); 
				
		/***
		 * Added by naldz for db data tracking
		 * Date: Dec. 12, 2007
		 */
		$tracker = DataTracker::create(); 
		$trace = debug_backtrace();
		$tracker->analyzeQuery($query,$this->connection->GetConnection(),$trace[0]);
		
		$startTime = microtime(true);
		mysql_query('SET NAMES utf8',$this->connection->GetConnection())       or die(mysql_error()); 
		$this->rs = mysql_query($query,$this->connection->GetConnection())     or die( $this->throwError($query) );
		$endTime = microtime(true);
		if(array_key_exists('dbhdebug',$_GET)){
			$debug_trace= $trace[0];
			$rs = new iRecordset(mysql_query('SELECT CONNECTION_ID() as conID;',$this->connection->GetConnection()));
			$rsOffset = new iRecordset(mysql_query('SELECT @@auto_increment_offset as autoIncrementOffset;',$this->connection->GetConnection()));
			echo '<strong>DB SERVER: <i>'.($rsOffset->getAutoIncrementOffset(0) == 1 ? 'db01' : 'db02' ).'</i></strong><br />';
			//echo '<strong>File:'.$debug_trace['file'].' Line:'.$debug_trace['line'].' ObjID:'.$this.' ConnID:'. $rs->getConID(0) .'</strong><br />';
			echo '<strong>File:'.$debug_trace['file'].' Line:'.$debug_trace['line'].' ConnID:'. $rs->getConID(0) .'</strong><br />';
			for($i=1;$i<10;$i++){
				if(!isset($trace[$i]) || !isset($trace[$i]['file']) || !isset($trace[$i]['line'])){
					break;
				}
				echo $trace[$i]['file'],': ',$trace[$i]['line'],'<br />';			
			}
			echo '<i>'.$query.'</i><br />';
			printf('Elapse Time:<b style="color:red">%f</b><br /><br />', ($endTime - $startTime));
		}
		
		/***
		 * Added by naldz for db data tracking
		 * Date: Dec. 12, 2007
		 */
		$tracker->trackChanges($this->rs);
		return $this->rs;
	}		

	function throwError($_msg){	// function added by daf 05.29.2008 - to catch mysql exceptions
		$msg = mysql_error(). ' ( ' . $_msg . ' )';
		$e = new Exception($msg);
		//require_once('Class.ErrorLog.php');
		//ErrorLog::setException($e);
		require_once('errorLogger/Class.ErrorLogger.php');
		ErrorLogger::getInstance()->handleException($e);
	}
	
	function Recordcount()
	{
		if ( $this->rs != false )
		{
			return mysql_num_rows($this->rs);		
		}		
		return null;
	}				
	
	function Result($row,$fieldname)
	{
		if ( $this->Recordcount() )
		{
			return mysql_result($this->rs,$row,$fieldname);
		}	
		return null;
	}	
	
	function Resultset()
	{
		return $this->rs;
	}
	/**
	*Fetch a result row as an object
	*@return Object Result
	*Added by: Joel
	*July-20-2007
	*/
	function FetchAsObject(){
		return mysql_fetch_object($this->rs);
	}
	
	function AffectedRows()
	{
		/***
		 * Modified by: Naldz(March 13, 2008)
		 */
		//return DataTracker::create()->getAffectedRows($this->connection->GetConnection());
		return mysql_affected_rows();
	}
	
	function GetCurrentId()
	{
		/**
		 * Modified by: Naldz
		 * Avoid using PHP's mysql_insert_id. Instead, get it from the database with MySql's last_insert_id function. More robust and more reliable approach.
		 */
		$rs = mysql_query('SELECT LAST_INSERT_ID() as currentID;',$this->connection->GetConnection());
		return mysql_result($rs,0,'currentID');
		//return intval(mysql_insert_id($this->connection->GetConnection()));
	}
			
	function Close()
	{		
		$this->connection->Close();				
	}			
	
	/**
	* This function will always try to encode $text to base64, except when $text is a number. This allows us to Escape all data before they're inserted in the database, regardless of attribute type.
	* @param string $text
	* @return string encoded to base64
	*/
	function Escape($text)
	{
		if (!is_numeric($text))
		{
			return base64_encode($text);
		}
		return $text;
	}

	// -------------------------------------------------------------
	function Unescape($text)
	{
		if (!is_numeric($text))
		{
			return base64_decode($text);
		}
		return $text;
	}
	
	/**************************************************
//	 *  sets a Connection object 
	 * 			4/22/09 kgordo
	 */
	
	function setConnection(Connection $con){
		$this->connection	=	$con;
	}
	
}	
?>
