<?php
	
	//require_once "Class.Recordset";
	//require_once "Class.Connection";

	class LogSite{
		
		private $logSiteID = 0;
		
		function LogSite($_logSiteID = 0){			
			try{
				$this-> conn = new Connection();
				$this-> rs = new Recordset($this-> conn);
			}
			catch(exception $e){
				throw $e;
			}
			
			if (0 < $_logSiteID){
				$sqlQuery = "select logSiteName from Logs.tblLogSite where logSiteID = " . $_logSiteID;
				$this-> rs-> Execute($sqlQuery);
				
				$this-> logSiteName = $this-> rs-> Result(0, "logSiteName");				
			}
				
			$this-> logSiteID = $_logSiteID;
			return;
		}
		
		function getLogSiteID(){
			return $this-> logSiteID;
		}
		
		function getLogSiteName(){
			return $this-> logSiteName;
		}
	}
?>