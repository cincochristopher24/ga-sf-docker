<?php

	require_once('Cache/ganetICache.php');
	require_once('serverConfig/Class.ServerConfig.php');
	
	class ganetMemcache implements ganetICache {
	    protected $mc = null;
		protected $host = '192.168.1.120';
		protected $port = 11211;
		protected $timeout = 1;
		protected static $inst	= null;			// ganetMemcache instance
		
		protected $prodCacheServers = array(
				//array('host'=>'192.168.3.2','port'=>'11211'),
				//array('host'=>'192.168.3.3','port'=>'11211'),
				array('host'=>'10.78.22.82','port'=>'11211'),
				array('host'=>'10.78.22.83','port'=>'11211'),
				//array('host'=>'192.168.3.10','port'=>'11211'),
				//array('host'=>'`','port'=>'11211')
			);
		
		protected $devCacheServers = array(array('host'=>'192.168.3.9','port'=>'11211'));									
		
		//protected $prodCacheServers = array(array('host'=>'192.168.3.10','port'=>'11211'));
											
		
		
		
		/*public static function instance(){
			if (self::$inst == null){
				self::$inst = new ganetMemcache();
			}
			return self::$inst;
		}*/
		
		function ganetMemcache(){
			/*****************************************************
			 * NOTE: REMOVE CODE BELOW IF THERE IS A MEMCACHE AVAILABLE 
			 */
			//throw new Exception("I can't install memcache client in my computer");
			/***************************
			 * NOTE:
			 */
			
			$this->mc = new Memcache();
	
			/*$memcache_obj = memcache_connect("192.168.3.9", 11211);
			if (!$memcache_obj){
				echo 'cannot connect';
			} else {
				echo 'success';
			}
			exit;*/
			
			// set up where memcache will connect
			
			
			/***
			$hostAr	=	explode('.',$_SERVER['SERVER_NAME']);		
			$root	=	$hostAr[count($hostAr)-1];
			$domPref =	$hostAr[0];
			***/
			$domPref = strtolower(ServerConfig::getInstance()->getServerType());
			//if (strtolower($root) == 'local') {
			if('local' == $domPref){
				// dev environment in local machine
				if (!$this->mc->connect($this->host,$this->port,$this->timeout)){
					throw new Exception("Unable to connect to local memcache server");
				} else {
					//echo '> connected to local memcache server <br/>';
				}
			}
			//else if ($domPref == 'dev'){ 
			elseif('development' == $domPref){
				$noOfServers = count($this->devCacheServers);
				$noOfSuccessConnects = 0;
				foreach($this->devCacheServers as $server){
					if ($this->mc->addServer($server['host'],$server['port'])){
						$noOfSuccessConnects++;					
					}
				}
				if ($noOfSuccessConnects == 0){
					throw new Exception("Unable to connect to dev memcache server");
				} 
			} else {
				$noOfServers = count($this->prodCacheServers);
				$noOfSuccessConnects = 0;
				foreach($this->prodCacheServers as $server){
					if ($this->mc->addServer($server['host'],$server['port'])){
						$noOfSuccessConnects++;					
					}
				}
				if ($noOfSuccessConnects == 0){
					throw new Exception("Unable to connect to production memcache server");
				} 
			}
		}
		
		function set($key,$value,$flags = array()){
			$flag = 0;
			$expire = 0;
			if (isset($flags['FLAG']))
				$flag = $flags['FLAG'];
				
			if (isset($flags['EXPIRE']))
				$expire = $flags['EXPIRE'];
				
			$this->mc->set($key,$value,$flag,$expire);
//			if (!$this->mc->set($key,$value,$flag,$expire)){
//				throw new Exception("Unable to set value to memcache server");
//			}
			
		}
		function add($key,$value,$flags = array()){
			$flag = 0;
			$expire = 0;
			if (isset($flags['FLAG']))
				$flag = $flags['FLAG'];
				
			if (isset($flags['EXPIRE']))
				$expire = $flags['EXPIRE'];
				
			if (!$this->mc->add($key,$value,$flag,$expire)){
				throw new Exception("Unable to set value to memcache server");
			}
		}
		function get($key){
			//echo "Getting $key from cache<br/>";
			return $this->mc->get($key);
		}
		function delete($key,$flags = array()){
			return $this->mc->delete($key,0);
		}
		function flush(){
			return $this->mc->flush();
		}
		function getStatus(){
			return $this->mc->getServerStatus();
		}
	
	}

?>