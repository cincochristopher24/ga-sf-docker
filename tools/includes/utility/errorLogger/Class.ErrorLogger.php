<?php
	require_once('Class.GaString.php');
	require_once('class.phpmailer.php');
	require_once('Class.GAErrorLog.php');
	require_once('Class.ErrorLoggerConfig.php');
	require_once('Class.ToolMan.php');
	
	class ErrorLogger
	{
		static private $instance = null;
		static public function getInstance(){
			if(is_null(self::$instance)){
				self::$instance = new ErrorLogger();
			}
			return self::$instance;
		}
		
		private $mErrorTypes = array(
			1		=> 'E_ERROR',	
			2 		=> 'E_WARNING',
			4		=> 'E_PARSE',
			8		=> 'E_NOTICE',
			16		=> 'E_CORE_ERROR',
			32  	=> 'E_CORE_WARNING',
			64		=> 'E_COMPILE_ERROR',
			128		=> 'E_COMPILE_WARNING',
			256		=> 'E_USER_ERROR',
			512		=> 'E_USER_WARNING',
			1024	=> 'E_USER_NOTICE',
			6143	=> 'E_ALL',
			2048	=> 'E_STRICT',
			4096	=> 'E_RECOVERABLE_ERROR',
			8192	=> 'E_DEPRECATED',
			16384	=> 'E_USER_DEPRECATED'
		);
		
		private $mLoggerConfig = null;
		
		private $mIsRealTime = false;
		private $mExcludeMainRecipients = false;
		private $mDisplayToBrowser = false;
		private $mTempErrorRecipients = array();
		private $mError = null;
		
		private function __construct(){
			//check if we are suppose to be logging the current http domain.
			//load the file for the configuration...
			$this->mLoggerConfig = ErrorLoggerConfig::getInstance();
		}
			
		public function start(){
			//check if we are suppose to turn on logging for the current domain
			if(!(isset($_SERVER['SERVER_NAME']) && in_array($_SERVER['SERVER_NAME'],$this->mLoggerConfig->getExcludedDomains()))){
				//set internal handlers
				if(!$this->mLoggerConfig->isLoggingDisabled()){					
					set_error_handler(array($this, 'handleError'));
					set_exception_handler(array($this, 'handleException'));
					register_shutdown_function(array($this, 'handleFatalError'));
				}
			}
		}
		
		public function setToRealTime($realTime=true){
			$this->mIsRealTime = $realTime;
		}
		
		public function isRealTime(){
			return $this->mIsRealTime;
		}
		
		public function addErrorMailRecipient($emailAdd=null){
			if(GaString::isValidEmailAddress($emailAdd)){
				$this->mTempErrorRecipients[] = $emailAdd;
			}
		}
		
		public function excludeMainRecipients($exclude=false){
			$this->mExcludeMainRecipients = $exclude;
		}
		
		public function displayToBrowser($display=false){
			$this->mDisplayToBrowser = $display;
		}
		
		public function handleError($error,$message,$file,$line,$vars){
			if($error & error_reporting()){
				if($error != 8){//notice
					$errorType = (array_key_exists($error,$this->mErrorTypes) ? $this->mErrorTypes[$error] : 'Unknown Error Type!');
					$this->logError($errorType,$message,$file,$line,$vars);
				}
			}
		}
		
		public function handleException($e){
			$this->logError('Exception',$e->getMessage(),$e->getFile(),$e->getLine(),null,$e->getTrace());
		}
		
		public function handleFatalError(){
			$e = error_get_last();
			if(is_array($e) && $e['type'] === E_ERROR) {
				$this->logError('E_ERROR',$e['message'],$e['file'],$e['line'],null);
			}
		}
		
		private function logError($errType,$message,$file,$line,$vars,$stackTrace=null){
			//TO DO: find a way to store errors when the database is down. Or how to log errors that are caused when database is down.
			//$GLOBALS['local'] = $vars;
			$errorLog = new GAErrorLog();
			$errorLog->setType($errType);
			$errorLog->setMessage($message);
			$errorLog->setFile($file);
			$errorLog->setLine($line);
			
			//$errorLog->setVars($GLOBALS);
			//log only some of the variables
			$sessionVar = isset($_SESSION) ? $_SESSION : NULL;
			
			$errorLog->setVars(array(
				'SERVER' 	=> $_SERVER,
				'SESSION'	=> $sessionVar,
				'GET'		=> $_GET,
				'POST'		=> $_POST,
				'FILES'		=> $_FILES,
				'COOKIE'	=> $_COOKIE,
				'REQUEST'	=> $_REQUEST,
				'ENV'		=> $_ENV
			));
			
			$errorLog->setStackTrace($stackTrace);
			$errorLog->save();
			
			//edit: 24 March 2010 - to get GAErrorLog
			$this->mError = $errorLog;
			
			if(!$this->mLoggerConfig->isErrorMailDisbaled() && ($this->mLoggerConfig->isRealTime() || $this->mIsRealTime)){
				$addresses = $this->mTempErrorRecipients;
				if(!$this->mExcludeMainRecipients){
					$addresses = array_merge($addresses,$this->mLoggerConfig->getMailRecipients());
				}
				$errorLog->send($addresses);
			}
			if($this->mDisplayToBrowser || $this->mLoggerConfig->isDisplayableToBrowser()){
				echo $errorLog->getAsMessage();
			}
			else{
				//ToolMan::redirect("/ticket.php?action=show_form");
			}
		}
		
		//edit: 24 March 2010 - to get GAErrorLog (for dotCom)
		public function retrieveGaError(){
			return $this->mError;
		}
	}
?>