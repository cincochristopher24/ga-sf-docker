<?php
	require_once('Class.iRecordset.php');
	require_once('Class.GaPreparedStatement.php');
	
	class DBConnection{
		static protected $prepStmtID = 0;
		static protected $conCache = array();
		
		protected $cnn 		= null;
		protected $host 	= null;
		protected $user 	= null;
		protected $password = null;
		protected $dbName 	= null;
		protected $conID	= null;
		protected $conInfo	= null;
		
		function __construct($con=null){
			if(is_resource($con) && 'mysql link' == get_resource_type($con)){
				$this->cnn = $con;
			}
			else{
				require_once('serverConfig/Class.ServerConfig.php');
				$serverConfig = ServerConfig::getInstance();
				$arConf = array(
					'host'	=> $serverConfig->getDbHost(),
					'uid' 	=> $serverConfig->getDbUser(),
					'pwd'	=> $serverConfig->getDbPassword(),
					'db'	=> $serverConfig->getDefaultDatabase()
				);
				
				if(is_string($con)){
					$arCon = explode(';',$con);
					foreach($arCon as $iCon){
						$conArgs = explode('=',$iCon);
						if(array_key_exists($conArgs[0],$arConf)){
							$arConf[$conArgs[0]] = trim($conArgs[1]); 
						}
					}	
				}
				$this->setHost($arConf['host']);
				$this->setUser($arConf['uid']);
				$this->setPassword($arConf['pwd']);
				$this->mDb = $arConf['db'];
				$this->connect();
				$this->setDB($arConf['db']);
			}
			
			$this->conInfo = $this->getConnectionResourceInfo($this->cnn);
			if(!array_key_exists($this->conInfo,self::$conCache)){
				self::$conCache[$this->conInfo] = array(
					'conID'					=> null,
					'getActiveDatabaseStmt'	=> null,
					'getConnectionIDStmt'	=> null,
					'getLastInsertedIDStmt'	=> null
				);
			}
			$this->conID = self::$conCache[$this->conInfo]['conID'];
		}

		protected function getConnectionResourceInfo($cnn){
			ob_start();
			echo $cnn;
			$info = ob_get_contents();
			ob_end_clean();
			return $info; 
		}
				
		public function setHost($host=""){
			$this->host = $host;
		}
		
		public function setUser($user=""){
			$this->user = $user;		
		}
		
		public function setPassword($password=""){
			$this->password = $password;
		}
		
		public function connect(){
			if($this->cnn == null){
				try{
					$this->cnn = mysql_connect($this->host,$this->user,$this->password);
					mysql_query('SET NAMES utf8',$this->cnn);
				}
				catch(exception $e){
					 throw new exception(mysql_error());
				}
			}
		}
		
		public function setDB($vdb=""){						
			$this->dbName = $vdb;
			if(strlen($this->dbName)){
				$res = mysql_select_db($vdb, $this->cnn);
				if(!$res)throw new Exception(mysql_error());
			}
		}
		
		public function getConnectionID(){
			if(is_null($this->conID)){
				if(is_null(self::$conCache[$this->conInfo]['getConnectionIDStmt'])){
					self::$conCache[$this->conInfo]['getConnectionIDStmt'] = $this->prepareStatement('SELECT CONNECTION_ID() as conID;');
				}
				$rs = new iRecordset($this->executeStatement(self::$conCache[$this->conInfo]['getConnectionIDStmt']));
				$this->conID = $rs->getConID(0);
				self::$conCache[$this->conInfo]['conID'] = $this->conID;
			}
			return $this->conID;
		}
		
		public function isConnectionOpen(){
			return (is_resource($this->cnn) && 'mysql link' == get_resource_type($this->cnn));
		}
		
		public function getActiveDatabase(){
			if(is_null(self::$conCache[$this->conInfo]['getActiveDatabaseStmt'])){
				self::$conCache[$this->conInfo]['getActiveDatabaseStmt'] = $this->prepareStatement('SELECT DATABASE() as defDatabase;');
			}
			$rs = new iRecordset($this->executeStatement(self::$conCache[$this->conInfo]['getActiveDatabaseStmt']));
			return $rs->getDefDatabase(0);
		}
		
		public function getLastInsertedID(){
			if(is_null(self::$conCache[$this->conInfo]['getLastInsertedIDStmt'])){
				self::$conCache[$this->conInfo]['getLastInsertedIDStmt'] = $this->prepareStatement('SELECT LAST_INSERT_ID() as lastInsertedID;');
			}
			$rs = new iRecordset($this->executeStatement(self::$conCache[$this->conInfo]['getLastInsertedIDStmt']));
			return $rs->getLastInsertedID(0);
		}
		
		public function getAffectedRows(){
			return mysql_affected_rows($this->cnn);
		}
		
		public function execQuery($strqry){
			return $this->execute($strqry);
		}
		
		/***
		public function prepareStatement($strQry){
			$statementID = 'dbhandler_prep_stmt_'.self::$prepStmtID++;
			$prepSql = 'PREPARE '.$statementID.' FROM "'.addslashes($strQry).'"';
			try{
				$this->execute($prepSql);
				return GaPreparedStatement::create($statementID,$strQry);
			}
			catch(exception $e){
				throw $e;
			}
			return null;
		}
		***/
		
		public function prepareStatement($strQry){
			$statementID = 'dbhandler_prep_stmt_'.self::$prepStmtID++;
			$prepSql = 'PREPARE '.$statementID.' FROM "'.addslashes($strQry).'"';
			try{
				$this->execute($prepSql);
				return GaPreparedStatement::create($statementID,$strQry);
			}
			catch(exception $e){
				throw $e;
			}
			return null;
		}
		
		public function deallocateStatement($prepStmt){
			$this->execute("DEALLOCATE PREPARE {$prepStmt->getStatementID()}");
		}
		
		public function executeStatement($prepStmt,$params=array()){
			if(count($params)){
				$sqlParams = array();
				foreach($params as $num => $value){
					$paramName = "@prepStmtParam_{$prepStmt->getStatementID()}_{$num}";
					$sqlParams[$paramName] = $paramName.' = '.$this->makeSqlSafeString($value);
				}
				$this->execute('SET '.implode(', ',$sqlParams));
				$execSql = 'EXECUTE '.$prepStmt->getStatementID().' USING '.implode(',',array_keys($sqlParams)); 
			}
			else{
				$execSql = 'EXECUTE '.$prepStmt->getStatementID();
			}
			if(array_key_exists('dbhdebug',$_GET)){
				echo '<i>'.$prepStmt->getSqlStatement().'</i><br />';
			}
			return $this->execute($execSql);
		}
		
		public function execute($strQry){
			//var_dump($strQry);
			$this->setDB($this->dbName);
			$startTime = microtime(true);
			$qry = mysql_query($strQry,$this->cnn);
			if(array_key_exists('dbhdebug',$_GET)){
				$trace = debug_backtrace();
				echo '<i>'.$strQry.'</i><br />';
				for($i=0;$i<6;$i++){					
					if(!isset($trace[$i]) || !isset($trace[$i]['file']) || !isset($trace[$i]['line'])){
						break;
					}
					echo $trace[$i]['file'],': ',$trace[$i]['line'],'<br />';			
				}
				printf('Query Time: <span style="color:red">%f</span><br /><br />', microtime(true) - $startTime);
			}
			
			if (false === $qry){
				$trace = debug_backtrace();
				if( (count($trace) > 1) && 
					(array_key_exists('function',$trace[1])	&& 
					($trace[1]['function'] == 'execQuery' || $trace[1]['function'] == 'prepare' || $trace[1]['function'] == 'executeStatement')) && 
					(array_key_exists('class',$trace[1]) && $trace[1]['class'] == 'dbHandler')
				){
					$debug_trace = $trace[1];
				}
				else{
					$debug_trace = $trace[0];
				}
				throw new exception(mysql_error().' <<< SQL: ' . $strQry .' >>> {{{ The Error occured in FILE: '.$debug_trace['file'].' at LINE : '.$debug_trace['line'].' }}}');
			}
			return $qry;
		}
		
		public function close(){
			mysql_close($this->cnn);
			$this->cnn = null;
		}
		
		public function getConnection(){
			return $this->cnn;
		}
		
		public function makeSqlSafeString($str){
			$str = (get_magic_quotes_gpc()) ? stripslashes($str) : $str;
			return ("CURRENT_TIMESTAMP" === $str) ? $str : "'".mysql_real_escape_string($str, $this->cnn)."'";
		}
		
		/**
		 * Begins a new transaction.
		 * 
		 * @return boolean true on success and false on failure.
		 */
		public function startTransaction(){
			//$this->connect();
			return mysql_query("START TRANSACTION", $this->cnn);
		}
		
		/**
		 * Commits the changes of the transaction in the database making it permanent.
		 * 
		 * @return boolean true on success and false on failure.
		 */
		public function commit(){
			return mysql_query("COMMIT", $this->cnn);
		}
  	
		/**
		 * Rolls back the current transaction, canceling its changes.
		 * 
		 * @return boolean true on success and false on failure.
		 */
		public function rollback(){
			return mysql_query("ROLLBACK", $this->cnn);
		}
		
	}
?>