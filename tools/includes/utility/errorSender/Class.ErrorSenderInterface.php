<?php

interface ErrorSenderInterface {
	
	function setErrorMessage($v);
	function setErrorDomain($v);
	
	function getErrorMessage();
	
	function send();
	
}