<?php
require_once('errorLogger/Class.ErrorLoggerConfig.php');
require_once('Class.ErrorSenderInterface.php');
require_once('Class.DomainNames.php');
require_once('class.phpmailer.php');

class DotComErrorSender implements ErrorSenderInterface {
	
	private $errorMessage = null;
	private $errorDomain = null;
	private $dotcom_prod_domains  = array();
				
	private $dotcom_local_domains = array();
	
	public function __construct(){
		$this->dotcom_prod_domains = DomainNames::getInstance()->getDotComProdDomains();
		$this->dotcom_local_domains = DomainNames::getInstance()->getDotComLocalDomains();
	}
	
	public function setErrorDomain($domain){
		$this->errorDomain = $domain;
	}
	
	public function setErrorMessage($errorMessage){
		if($this->isInDomainList()){
			$this->errorMessage = $this->errorMessage.$errorMessage;
		}
	}
	
	public function getErrorMessage(){
		echo $this->errorMessage;
	}
	
	public function send(){
		$mail = new PHPMailer(); 
		$mail->IsSMTP();
		$mail->IsHTML(true);
	
		$mail->FromName = "DotCom Error";
		$mail->From = "error@goabroad.net";
 
		$mail->Subject = "DotCom Error (Bulk)";
		$mail->Body = $this->errorMessage;
		
	//	$addresses = ErrorLoggerConfig::getInstance()->getMailRecipients();
	//	foreach($addresses as $iAddr){
	//		echo "<br/> recipient : ".$iAddr."<br/>";
	//		$mail->AddAddress($iAddr);
	//	}
	//	$mail->AddAddress('reynaldo.castellano@goabroad.com');
	//	$mail->AddAddress('nash.lesigon@goabroad.com');
		$mail->AddAddress('ganet.notifs@gmail.com');
	//	$mail->AddCC('nash.lesigon@goabroad.com');
		$mail->Send();
	}
	
	private function isInDomainList(){
		if(in_array($this->errorDomain, $this->dotcom_prod_domains) || in_array($this->errorDomain, $this->dotcom_local_domains)){
			return true;
		}
		return false;
	}
}