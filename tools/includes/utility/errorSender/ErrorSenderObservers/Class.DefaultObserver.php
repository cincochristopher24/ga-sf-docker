<?php

require_once('utility/errorSender/interface/Class.ErrorSenderObserver.php');

class DefaultObserver implements ErrorSenderObserver {
	
	private $errorMessage = null;
	private $storeErrorMessage = true;
	private $recipients = array();
	
	public function __construct(){}
	
	public function notifyObserver(GaErrorLog $errorLog){
		$vars = $errorLog->getVars();
		$this->_setMessage($errorLog);
	}
	
	public function storeErrorMessage(){
		return $this->storeErrorMessage;
	}
	
	public function send(){
		if(!is_null($this->errorMessage)){
			echo "<br/><strong>send error message from default sender : </strong><br/>";
			$mail = new PHPMailer(); 
			$mail->IsSMTP();
			$mail->IsHTML(true);

			$mail->FromName = "GoAbroad Site Errors";
			$mail->From = "error@goabroad.net";

			$mail->Subject = "GoAbroad Site Errors (Bulk)";
		//	$message = "<p><strong><em>This is a test mail from error logger, please ignore. -nash</strong></em></p>";
		//	$mail->Body = $message.$this->errorMessage;
			
			$mail->Body = $this->errorMessage;
			
			$addresses = ErrorLoggerConfig::getInstance()->getMailRecipients();
			foreach($addresses as $iAddr){
				$mail->AddAddress($iAddr);
			}
		//	$mail->AddAddress('reynaldo.castellano@goabroad.com');
		//	$mail->AddAddress('nash.lesigon@goabroad.com');
		//	$mail->AddCC('nash.lesigon@goabroad.com');
			$mail->Send();

		//	echo $message;
			echo $this->errorMessage;
			echo "<br/><strong><em>SENT</em></strong><br/>";
		}
	}
	
	/* 
	PRIVATE FUNCTIONS
	*/
	
	private function _setMessage(GaErrorLog $errorLog){		
		$message = $errorLog->getAsMessage(false);
		$message .= '<br />======================== *** ==========================<br />';
		$this->errorMessage = $this->errorMessage.$message;
	}

}