<?php

require_once('utility/errorSender/interface/Class.ErrorSenderObserver.php');

class KayaObserver implements ErrorSenderObserver {
	
	private $errorMessage = null;
	private $storeErrorMessage = false;
	private $domainPattern = 'kayavolunteer.com';
	private $recipients = array('gapdaphne.notifs@gmail.com');
	
	public function __construct(){}
	
	public function notifyObserver(GaErrorLog $errorLog){
		$this->storeErrorMessage = false;
		$this->_filter($errorLog);
		$this->_setMessage($errorLog);
		
	}
	
	public function storeErrorMessage(){
		return $this->storeErrorMessage;
	}
	
	public function send(){
		if(!is_null($this->errorMessage)){
			echo "<br/><strong>send error message from KAYA sender : </strong><br/>";
			$mail = new PHPMailer(); 
			$mail->IsSMTP();
			$mail->IsHTML(true);
	
			$mail->FromName = "KAYA Site Error";
			$mail->From = "error@kayavolunteer.com";
 
			$mail->Subject = "KAYA Site Error (Bulk)";
		//	$message = "<p><strong><em>This is a test mail from error logger, please ignore. -nash</strong></em></p>";
		//	$mail->Body = $message.$this->errorMessage;
			$mail->Body = $this->errorMessage;
		//	$addresses = ErrorLoggerConfig::getInstance()->getMailRecipients();
			$addresses = $this->recipients;
			foreach($addresses as $iAddr){
				$mail->AddAddress($iAddr);
			}
		//	$mail->AddAddress('reynaldo.castellano@goabroad.com');
		//	$mail->AddAddress('nash.lesigon@goabroad.com');
		//	$mail->AddBCC('nash.lesigon@goabroad.com');
			$mail->AddBCC('ganet.notifs@gmail.com');
		
			$mail->Send();
		//	echo $message;
			echo $this->errorMessage;
			echo "<br/><strong><em>SENT</em></strong><br/>";
			
		}
	}
	
	/* 
	PRIVATE FUNCTIONS
	*/
	
	private function _setMessage(GaErrorLog $errorLog){
		if($this->storeErrorMessage){
			$message = $errorLog->getAsMessage(false);
			$message .= '<br />======================== *** ==========================<br />';
			$this->errorMessage = $this->errorMessage.$message;
		}
	}
	
	private function _filter(GaErrorLog $errorLog){
		
		$errorDomain = $this->_getDomainFromErrorLog($errorLog);
		$pattern1 = $this->domainPattern;
		$pattern2 = $errorDomain;
		if(strpos($pattern2, $pattern1) != false){
			$this->storeErrorMessage = true;
		}
		else {
			$this->storeErrorMessage = false;
		}		
		
	}
	
	private function _getDomainFromErrorLog(GaErrorLog $errorLog){
		$subject = $errorLog->getVars();
		$subject = $this->_getStringByPattern($subject, '[SERVER]', '[SESSION]');		
		$start = 15;
		$pattern = '/\bSERVER_NAME\b/i';
		$pos = strpos($subject, $pattern);
		
		preg_match($pattern, $subject, $matches, PREG_OFFSET_CAPTURE);
		if(!count($matches)){
			$pattern = '/\bHOSTNAME\b/i';
			preg_match($pattern, $subject, $matches, PREG_OFFSET_CAPTURE);
			$start = 12;
		}
		
		if(!count($matches)){
			return "";
		}
		
		$num1 = $matches[0][1];
		$mystring = substr($subject, $num1);
		$findme = '[';
		$pos = strpos($mystring, $findme);
		return substr($mystring, $start, ($pos-$start));
	}
	
	private function _getStringByPattern($string, $pattern1, $pattern2){
		$pos1 = strpos($string, $pattern1);
		$pos2 = strpos($string, $pattern2);
		
		$a = ($pos2 - $pos1);
		
		return substr($string, $pos1, $a);
	}
	
	
}