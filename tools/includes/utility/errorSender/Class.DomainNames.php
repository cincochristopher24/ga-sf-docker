<?php

class DomainNames {
	
	static private $instance = null;
	static public function getInstance(){
		if(is_null(self::$instance)){
			self::$instance = new self();
		}
		return self::$instance;
	}
	
/*	private static $GANET_PROD_DOMAINS = array(
		'advisor.goabroad.net',
		'aifs.goabroad.net',
		'myproworld.goabroad.net',
		'cis.goabroad.net',
		'cea.goabroad.net',
		'iep.goabroad.net',
		'globalscholar.goabroad.net',
		'allianceabroad.goabroad.net',
		'culturalembrace.goabroad.net',
		'worldendeavors.goabroad.net',
		'educasian.goabroad.net',
		'eliabroad.goabroad.net',
		'globalsemesters.goabroad.net',
		'livingroutes.goabroad.net',
		'academictreks.goabroad.net',
		'ambafrance-us.goabroad.net',
		'australearn.goabroad.net',
		'central.goabroad.net',			
		'ciee.goabroad.net',
		'myisic.goabroad.net',	
		'neumann.goabroad.net',
		'nvcc.goabroad.net',
		'quinnipiac.goabroad.net',
		'samford.goabroad.net',		
		'solabroad.goabroad.net',
		'englishfirst.goabroad.net',
		'cisdenver.goabroad.net',
		'ucdenver.goabroad.net',
		'gvi.goabroad.net',
		'internationalstudent.goabroad.net',
		'ucboulder.goabroad.net',
		'ga-web01.goabroad.net',
		'dse.goabroad.net',
		'yha.goabroad.net',
		'www.goabroad.net',
		'staging.goabroad.net'   // for the staging dotNet
		);
		
	private static $GANET_LOCAL_DOMAINS = array(
		'advisor.goabroad.net.local',
		'aifs.goabroad.net.local',
		'myproworld.goabroad.net.local',
		'cis.goabroad.net.local',
		'cea.goabroad.net.local',
		'iep.goabroad.net.local',
		'globalscholar.goabroad.net.local',
		'allianceabroad.goabroad.net.local',
		'culturalembrace.goabroad.net.local',
		'worldendeavors.goabroad.net.local',
		'educasian.goabroad.net.local',
		'eliabroad.goabroad.net.local',
		'globalsemesters.goabroad.net.local',
		'livingroutes.goabroad.net.local',
		'academictreks.goabroad.net.local',
		'ambafrance-us.goabroad.net.local',
		'australearn.goabroad.net.local',
		'central.goabroad.net.local',			
		'ciee.goabroad.net.local',
		'myisic.goabroad.net.local',	
		'neumann.goabroad.net.local',
		'nvcc.goabroad.net.local',
		'quinnipiac.goabroad.net.local',
		'samford.goabroad.net.local',		
		'solabroad.goabroad.net.local',
		'englishfirst.goabroad.net.local',
		'cisdenver.goabroad.net.local',
		'ucdenver.goabroad.net.local',
		'gvi.goabroad.net.local',
		'internationalstudent.goabroad.net.local',
		'ucboulder.goabroad.net.local',
		'ga-web01.goabroad.net.local',
		'dse.goabroad.net.local',
		'yha.goabroad.net.local',
		'goabroad.net.local'
		);
		*/
	private static $DOTCOM_PROD_DOMAINS = array(
		"usu.goabroad.com",
		"volunteer.goabroad.com",
		"teach.goabroad.com",
		"intern.goabroad.com",
		"jobs.goabroad.com",
		"study.goabroad.com",
		"languagestudy2.goabroad.com",
		"ecotravel.goabroad.com",
		"highschoolstudy.goabroad.com",
		"www2.goabroad.com",
		"teflcourses.goabroad.com",
		"degree.goabroad.com"
		);
	
	private static $DOTCOM_LOCAL_DOMAINS = array(
		"usu.goabroad.com.local",
		"volunteer.goabroad.com.local",
		"teach.goabroad.com.local",
		"intern.goabroad.com.local",
		"jobs.goabroad.com.local",
		"study.goabroad.com.local",
		"languagestudy.goabroad.com.local",
		"ecotravel.goabroad.com.local",
		"highschoolstudy.goabroad.com.local",
		"www.goabroad.com.local",
		"teflcourses.goabroad.com.local",
		"degree.goabroad.com.local"
		);
	
	private static $AFFILIATES_DOMAINS = array(
		'www.kayavolunteer.com'
		);
	
/*	public function getGaNetProdDomains(){
		return self::$GANET_PROD_DOMAINS;
	}
	
	public function getGaNetLocalDomains(){
		return self::$GANET_LOCAL_DOMAINS;
	}
*/	
	public function getDotComProdDomains(){
		return self::$DOTCOM_PROD_DOMAINS;
	}
	
	public function getDotComLocalDomains(){
		return self::$DOTCOM_LOCAL_DOMAINS;
	}
	
	public function getAffiliatesDomains(){
		return self::$AFFILIATES_DOMAINS;
	}
	
	public function getNames(){
		$domains = array_merge(self::$AFFILIATES_DOMAINS, self::$DOTCOM_PROD_DOMAINS, self::$DOTCOM_LOCAL_DOMAINS);
		return $domains;
	}
}
	
	