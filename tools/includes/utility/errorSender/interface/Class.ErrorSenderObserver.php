<?php

interface ErrorSenderObserver {
	
	function notifyObserver(GaErrorLog $errorLog);
	function storeErrorMessage();
	function send();
}