<?php
require_once('errorLogger/Class.ErrorLoggerConfig.php');
require_once('Class.ErrorSenderInterface.php');
require_once('Class.DomainNames.php');
require_once('class.phpmailer.php');

class AffiliatesErrorSender implements ErrorSenderInterface {
	
	private $errorMessage = null;
	private $errorDomain = null;
	private $fromName = null;
	private $subject = null;
	private $domainNames = array();
		
	public function __construct(){
		$this->domainNames = DomainNames::getInstance()->getAffiliatesDomains();
	}
	
	public function setErrorDomain($domain){
		$this->errorDomain = $domain;
	}
	
	public function setErrorMessage($errorMessage){
		if($this->isInDomainList()){
			$this->errorMessage = $this->errorMessage.$errorMessage;
		}
	}
	
	public function getErrorMessage(){
	//	var_dump($this->domainNames);
		echo $this->errorMessage;
	}
	
	public function send(){
		$mail = new PHPMailer(); 
		$mail->IsSMTP();
		$mail->IsHTML(true);
	
		$mail->FromName = "Affiliates Error";
		$mail->From = "error@goabroad.net";
 
		$mail->Subject = "Affiliates Error (Bulk)";
		$mail->Body = $this->errorMessage;
		
	//	$addresses = ErrorLoggerConfig::getInstance()->getMailRecipients();
	//	foreach($addresses as $iAddr){
	//		echo "<br/> recipient : ".$iAddr."<br/>";
	//		$mail->AddAddress($iAddr);
	//	}
	//	$mail->AddAddress('reynaldo.castellano@goabroad.com');
	//	$mail->AddAddress('nash.lesigon@goabroad.com');
		$mail->AddAddress('ganet.notifs@gmail.com');
	//	$mail->AddCC('nash.lesigon@goabroad.com');
		$mail->Send();
	}
	
	public function setFromName($fromName){
		$this->fromName = $fromName;
	}
	
	public function setSubject($subject){
		$this->subject = $subject;
	}
	
	private function isInDomainList(){
		if(in_array($this->errorDomain, $this->domainNames)){
			return true;
		}
		return false;
	}
}