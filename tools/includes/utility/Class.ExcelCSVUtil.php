<?php
	require_once("travellog/model/Class.PathManager.php");
	
	class ExcelCSVUtil{
		
		//error type
		const	NO_ERROR	=	0;
		const	INIT_ERROR	=	1;
		const	INVALID_FILE_ERROR	=	2;
		const	FILE_READ_ERROR	=	3;
		
		//file type
		const	CSV			=	"CSV";
		const	EXCEL		=	"EXCEL";
		
		private $mType =	self::CSV;
		private $mPathFilename = FALSE;
		
		private $mData = NULL;//this should be an array of arrays. each array element in this variable represents a row in the file
		private $mParams = NULL;
		
		private $mInitialized = FALSE;
		private $mError	= self::INIT_ERROR;
		
		private static $instance = NULL;

		private function __construct(){
		}

		public static function getInstance(){
			if ( is_null(self::$instance) ){
				self::$instance= new ExcelCSVUtil();
			}
			return self::$instance;
		}
		
		private function setError($code=0){
			$this->mError = $code;
		}
		
		public function getError(){
			return (self::NO_ERROR < $this->mError) ? $this->mError : FALSE;
		}
		
		public function initialize($params=array()){
			$temp = array(	"columnNames"	=>	array("email","firstname","lastname"),
							"CSVSeparator"	=>	",",
			 				"pathManager"	=>	NULL,
							"postFile"		=>	NULL,
							"discardFile"	=>	TRUE	);
			$this->mParams = array_merge($temp,$params);
			
			//check columnnames
			if( 1 > count($this->mParams["columnNames"]) ){
				$this->setError(self::INIT_ERROR);
				throw new Exception("Invalid item 'columnNames' in parameter array! Must be an array of column names.");
			}
			
			//check post file
			$postFile = $this->mParams["postFile"];
			if( is_null($postFile) || '' == $postFile['name'] ){
				$this->setError(self::INVALID_FILE_ERROR);
				throw new exception("Invalid item 'postFile' in parameter array!");
			}
			$postFile = $this->mParams["postFile"];
			$ext = substr($postFile['name'], -3, 3);
			if( "csv" == $ext ){
				$this->mType = self::CSV;
			}elseif( "xls" == $ext ){
				$this->mType = self::EXCEL;
			}else{
				$this->setError(self::INVALID_FILE_ERROR);
				throw new exception("Invalid file extension. Expected .csv or .xls!");
			}
			
			//check pathmanager
			$pathManager = $this->mParams["pathManager"];
			if( !($pathManager instanceof PathManager) ){
				$this->setError(self::INIT_ERROR);
				throw new exception("Invalid item 'pathManager' in parameter array! Must be an instance of PathManager.");
			}
			$path = $pathManager->GetPathrelative();
			if( !is_dir($path) ){
				$pathManager->CreatePath();
			}
			$this->mPathFilename = $path.uniqid().time().'.'.$ext;
			$isUploaded = move_uploaded_file($postFile["tmp_name"],$this->mPathFilename);
			if( !$isUploaded ){
				$this->setError(self::FILE_READ_ERROR);
				throw new exception("Unable to move uploaded file.");
			}
			
			try{
				$this->mData = array();
				$this->_prepareData();
				if( $this->mParams["discardFile"] ){
					$this->_cleanup();
				}
				$this->mInitialized = TRUE;
				$this->setError(self::NO_ERROR);
			}catch(exception $e){
				throw $e;
			}
			
			return $this;
		}
		
		private function _prepareData(){
			if( FALSE == ($fileHandler = fopen($this->mPathFilename, 'r')) ){
				$this->setError(self::FILE_READ_ERROR);
				throw new exception ($this->mPathFilename . ' not found!');
			}
			
			$headers = $this->mParams["columnNames"];
			if(self::CSV == $this->mType){ // load information from a csv file (comma-separated csv file)
				$ctr = 0;
				while(FALSE !== ($data = fgetcsv($fileHandler,1000, $this->mParams["CSVSeparator"]))){
					$dataCtr = 0;
					foreach($data as $iData){
						$this->mData[$ctr][(array_key_exists($dataCtr,$headers) ? $headers[$dataCtr]: $dataCtr)] = $iData;
						$dataCtr++;
					}
					$ctr ++;
				}
			}elseif(self::EXCEL == $this->mType){ // load information from an excel file
				require_once('phpExcelReader/reader.php');
				$excelReader = new Spreadsheet_Excel_Reader;
				$excelReader->setOutputEncoding('CP1251');
				try{
					$excelReader->read($this->mPathFilename);  // throws exception if file is not readable
				}catch(exception $e){
					$this->setError(self::FILE_READ_ERROR);
					throw $e;
				}
				/** START: **/
				$entryCtr = 1;
				
				// loop through every worksheet
				for($sheetCtr = 0; $sheetCtr < count($excelReader->sheets); $sheetCtr++){
					for ($rowCtr = 1; $rowCtr <= $excelReader->sheets[$sheetCtr]['numRows']; $rowCtr++, $entryCtr++) {
						if(isset($excelReader->sheets[$sheetCtr]['cells'][$rowCtr])){
							for ($colCtr = 1; $colCtr <= $excelReader->sheets[$sheetCtr]['numCols']+1; $colCtr++) {
								if($colCtr > count($headers)){//just take the first N columns where N=count(headers)
									break;
								}
								$this->mData[$entryCtr-1][(array_key_exists($colCtr-1,$headers) ? $headers[$colCtr-1]: $colCtr)] = isset($excelReader->sheets[$sheetCtr]['cells'][$rowCtr][$colCtr]) ? trim($excelReader->sheets[$sheetCtr]['cells'][$rowCtr][$colCtr]) : '';
							}
						}
					}
				}
				/** END: **/
			}else{
				$this->setError(self::INVALID_FILE_ERROR);
				throw new Exception("Invalid file type. Expected Excel or CSV file!");
			}
		}
		
		private function _cleanup(){
			if( $this->mPathFilename && file_exists($this->mPathFilename) ){
				unlink($this->mPathFilename);
			}
		}
		
		private $mCurIndex = 0;
		private $mRowcount = 0;
		public function reset(){
			if( $this->getError() ){
				throw new exception("ExcelCSVUtil wasn't initialized.");
			}
			$this->mRowcount = count($this->mData);
			$this->mCurIndex = 0;
			return TRUE;
		}
		public function fetchRow(){//gets the current row and moves the index to the next
			if( $this->getError() ){
				throw new exception("ExcelCSVUtil wasn't initialized.");
			}
			if( $this->mCurIndex < $this->mRowcount ){
				return $this->mData[$this->mCurIndex++];
			}
			return FALSE;
		}
	}