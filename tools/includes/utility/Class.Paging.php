<?
/**
* <b>Paging</b> class.
* @author Aldwin S. Sabornido
* @version 1.0 PHP5
* @see http://www.GoAbroad.com/
* @copyright 2006-2007
*/
class Paging
{
	protected $recordcount;
	
	protected $page;
	
	protected $totalpages;
		
	protected $rowsperpage;
	
	protected $prevalue = '';
	
	protected $appvalue = '';
	
	protected $onclick  = NULL;
	
	protected $next;
	
	protected $previous;
	
	protected $last;
	
	protected $first;
	
	protected $links='';
	
	protected $querystring;
	
	protected $startrow;
	
	protected $endrow;
	
	protected $displaynum=10;
	
	protected $scriptName = '';
	
	protected $is_friendly_url = false;
	
	function __construct( $recordcount, $page, $querystring='', $rowsperpage=1, $limit_pages=true, $is_friendly_url = false )
	{
		$this->totalpages  = ceil( $recordcount/$rowsperpage );
		
		/* 
		* EDITS: ianne - 05/12/2009
		* allowed page limit to be set
		*/
		
		if(is_int($limit_pages))
			$this->totalpages  = ( $this->totalpages > $limit_pages )? $limit_pages: $this->totalpages;
		else	
			$this->totalpages  = ( $this->totalpages > 50 && $limit_pages )? 50: $this->totalpages;
							
		$this->recordcount = $recordcount;
		$this->page        = $page;
		$this->rowsperpage = $rowsperpage;
		$this->querystring = $querystring;
		$this->querystring = (false == $is_friendly_url)? "&amp;".$this->querystring : $this->querystring;
		$this->is_friendly_url = $is_friendly_url;
		$this->first       = 1;
		$this->last        = $this->totalpages;
		
		$this->next        = ( $recordcount == $page )? $recordcount : $page+1;
		$this->previous    = ( $page == 1 )? 1 : $page-1;
		
		$this->links      .= '';
		
		$this->startrow    = ($this->page*$this->rowsperpage)-$this->rowsperpage;
		
		$this->endrow      = ( $this->startrow+$this->rowsperpage  < $this->recordcount )?  $this->startrow+$this->rowsperpage : $this->recordcount;
		
		$this->scriptName = $_SERVER['PHP_SELF'];
	}
	
	function showPagination(array $options = array()) {
		// Don't Show Pagination if there is only one page
		if($this->totalpages < 2) {
			return;
		}
		
		$default_options = array(
			'show_details'   => false, // Displays the text "Showing n-n of #..." pagination details
			'details_prefix' => 'Showing ',
			'label'          => '',   // e.g. if 'Entries',  "Showing 1-10 of 40 Entries"
			'edges'          => true, // Show first and last links
			'pages_to_show'  => 10,
			'mini'           => false 
		);
		
		$options = array_merge($default_options, $options);
		
		if ($this->totalpages > 10) {
			$first_and_last = true;
		} else {
			$first_and_last = false;
		}
		
		
		
		echo '<div class="pagination', $options['mini'] ? ' mini': '', '">';
		
		if($options['show_details']) {
			$last_on_page = $this->page * $this->rowsperpage;
			$real_last_on_page = ($last_on_page < $this->recordcount) ? $last_on_page : $this->recordcount;
			$start_on_page = $last_on_page - $this->rowsperpage + 1;
			echo '<p>', $options['details_prefix'], $start_on_page, ' - ', $real_last_on_page, ' of ', $this->recordcount, ' ', $options['label'], '</p>';
		}
		
		echo '<div class="pagination_pages">';
		if ($first_and_last && $options['edges'])
			$this->getFirst();
		$this->getPrevious();
		if (!$options['mini'])
			$this->getLinks($options['pages_to_show']);
		$this->getNext();
		if ($first_and_last && $options['edges'])
			$this->getLast();
		
		echo '</div><div class="clear"></div></div>';
	}
	
	function showMiniPagination(array $options = array()) {
		$this->showPagination(array('edges'=>false, 'pages'=>0, 'mini'=>true, 'details_prefix'=>''));
	}
	
	function getNext()
	{
		if ( $this->totalpages ):
			if ( $this->last != $this->page )
			{
				if ($this->is_friendly_url) {
					if ( $this->onclick != NULL )
						return print('<a class="next" href="javascript:void(0)" id="next" onclick="'.$this->onclick.'(\''.$this->querystring.'/'.$this->next.'\')"><span>Next</span></a> ');
					else
						return print('<a class="next" href="http://'.$_SERVER['SERVER_NAME'].$this->querystring."/".$this->next." \">"."<span>Next</span></a> ");					
				}
				else {
					if ( $this->onclick != NULL )
						return print('<a class="next" href="javascript:void(0)" id="next" onclick="'.$this->onclick.'(\'page='.$this->prevalue.$this->next.$this->appvalue.$this->querystring.'\')"><span>Next</span></a> ');
					else
						return print('<a class="next" href="http://'.$_SERVER['SERVER_NAME'].$this->scriptName."?page=".$this->prevalue.$this->next.$this->appvalue.$this->querystring."\">"."<span>Next</span></a> ");
				}
			}	
			else
				return print('<span class="next">Next</span> ');
		endif;
	}
	
	function getPrevious()
	{
		if ( $this->totalpages ):
			if ( $this->page != 1 )
			{
				if ($this->is_friendly_url) {
					if ( $this->onclick != NULL )
						return print('<a class="previous" href="javascript:void(0)" id="prev" onclick="'.$this->onclick.'(\''.$this->querystring.'/'.$this->previous.'\')"><span>Prev</span></a> ');
					else
						return print('<a class="previous" href="http://'.$_SERVER['SERVER_NAME'].$this->querystring.'/'.$this->previous." \">"."<span>Prev</span></a> ");					
				}
				else {		
					if ( $this->onclick != NULL )
						return print('<a class="previous" href="javascript:void(0)" id="prev" onclick="'.$this->onclick.'(\'page='.$this->prevalue.$this->previous.$this->appvalue.$this->querystring.'\')"><span>Prev</span></a> ');
					else
						return print('<a class="previous" href="http://'.$_SERVER['SERVER_NAME'].$this->scriptName."?page=".$this->prevalue.$this->previous.$this->appvalue.$this->querystring."\">"."<span>Prev</span></a> ");
				}
			}
			else
				return print('<span class="previous">Prev</span> ');
		endif;
	}
	
	function getLast()
	{
		if ( $this->totalpages ):
			if ( $this->last != $this->page )
			{
				if ($this->is_friendly_url) {
					if ( $this->onclick != NULL )
						return print('<a class="last" href="javascript:void(0)" id="next" onclick="'.$this->onclick.'(\''.$this->querystring.'/'.$this->last.'\')"><span>Last</span></a> ');
					else
						return print('<a class="last" href="http://'.$_SERVER['SERVER_NAME'].$this->querystring.'/'.$this->last." \">"."<span>Last</span></a> ");					
				}
				else {				
					if ( $this->onclick != NULL )
						return print('<a class="last" href="javascript:void(0)" onclick="'.$this->onclick.'(\'page='.$this->prevalue.$this->last.$this->appvalue.$this->querystring.'\')"><span>Last</span></a> ');
					else
						return  print('<a class="last" href="http://'.$_SERVER['SERVER_NAME'].$this->scriptName."?page=".$this->prevalue.$this->last.$this->appvalue.$this->querystring."\">"."<span>Last</span></a> ");
				}
			}
			else
				return print('<span class="last">Last</span> ');
		endif;
	}
	
	function getFirst()
	{
		if ( $this->totalpages ):
			if ( $this->page != 1 )
			{
				if ($this->is_friendly_url) {
					if ( $this->onclick != NULL )
						return print('<a class="first" href="javascript:void(0)" id="first" onclick="'.$this->onclick.'(\''.$this->querystring.'/'.$this->first.'\')"><span>First</span></a> ');
					else
						return print('<a class="first" href="http://'.$_SERVER['SERVER_NAME'].$this->querystring.'/'.$this->first." \">"."<span>First</span></a> ");					
				}
				else {				
					if ( $this->onclick != NULL )
						return print('<a class="first" href="javascript:void(0)" onclick="'.$this->onclick.'(\'page='.$this->prevalue.$this->first.$this->appvalue.$this->querystring.'\')"><span>First</span></a> ');
					else
						return print('<a class="first" href="http://'.$_SERVER['SERVER_NAME'].$this->scriptName."?page=".$this->prevalue.$this->first.$this->appvalue.$this->querystring."\">"."<span>First</span></a> ");
				}
			}
			else
				return print('<span class="first">First</span> ');
		endif;
	}
	
	function getLinks($displaynum = NULL)
	{
		// Reset the links value to prevent repetition
		$this->links = '';
		
		$all = (is_null($displaynum))? $this->displaynum : (int) $displaynum;
		if ($all < 1) {
			return print ('');
		}
		$half = floor($all/2);
		
		
		if ( $this->totalpages ){

			if ( $this->totalpages < ($all+1) && $this->page < ($all+1) ){
				$startloop = 1;
				$endloop   = $this->totalpages;
			}else{
				if ( $this->page <= $half){
					$startloop = 1;
					$endloop   = $all;
				}else{	
					if ( ($this->page+$half) >= $this->totalpages ){
						if ( ($this->totalpages-$all) < 0)
							$startloop = 1;
						else
							$startloop = $this->totalpages-$all;
							
						$endloop = $this->totalpages;
					}else{	
						if ( ($this->page-$half) < 0)
							$startloop = 1;
						else
							$startloop = $this->page-$half;
						$endloop = $this->page+$half;
					}	
				}	
			}
			while ( $startloop <= $endloop ){
				if ( $startloop != $this->page ) {
					if ($this->is_friendly_url) {
						if ( $this->onclick != NULL )
							$this->links .= '<a href="javascript:void(0)" onclick="'.$this->onclick.'(\''.$this->querystring.'/'.$startloop.'\')"><span>'.$startloop.'</span></a> ';
						else
							$this->links .= '<a href="http://'.$_SERVER['SERVER_NAME'].$this->querystring.'/'.$startloop." \">"."<span>".$startloop."</span></a> ";					
					}
					else {					
						if ( $this->onclick != NULL )
							$this->links .= '<a href="javascript:void(0)" onclick="'.$this->onclick.'(\'page='.$this->prevalue.$startloop.$this->appvalue.$this->querystring.'\')">'.$startloop.'</a> ';
						else
							$this->links .= "<a href=\"http://".$_SERVER['SERVER_NAME'].$this->scriptName."?page=".$this->prevalue.$startloop.$this->appvalue.$this->querystring."\">".$startloop."</a> ";
					}
				}
				else {
					$this->links .= "<strong>".$startloop."</strong>";
				}
				
				$startloop++;
			}	
			
		}	

		return print($this->links);
	}
	
	function getStartRow()
	{
			return $this->startrow;
	}	
	
	function getEndRow()
	{
			return $this->endrow;
	}	
	
	function getTotalPages()
	{
		return $this->totalpages;
	}
	
	function getRecordCount(){
		return $this->recordcount;
	}
		
	function AppendPageValue( $appvalue )
	{
		$this->appvalue = $appvalue;
	}
	
	function PrependPageValue( $prevalue )
	{
		$this->prevalue = $prevalue;
	}
	
	function setOnclick( $onclick )
	{
		$this->onclick = $onclick;
	}
	
	function getOnclick()
	{
		return $this->onclick;
	}
	
	function setScriptName($name){
		$this->scriptName = $name;
	}
	
	function getScriptName(){
		return $this->scriptName;
	}
	
}	
?>
