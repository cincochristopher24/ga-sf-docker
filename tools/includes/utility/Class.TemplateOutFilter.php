<?php
/*
 * Created on 12 14, 06
 *
 * @author Kerwin Gordo
 * Purpose: a class used by Template class to filter output (turn off the display of variables set on the template).
 * 			Rules are set on this class or subclasses and this is set to the template after instantiation.
 * 			Upon calling 'out' on the template, the template delegates the filtering of the variables to this class   
 */
 
 class TemplateOutFilter {
 	private $context = null;				// filter context by which filtering rules will depend
 	
 	/**
 	 * constructor
 	 * @param object $filterContext the context for filtering (traveler object for the initial case)
 	 */
	public function TemplateOutFilter($filterContext) {
		$this->context = $filterContext;
	} 	
 	
 	
 	/**
 	 * function to filter out some variables based on rules of this class
 	 * @param array $vars the array of variables passed by the template class to be used for displaying as html
 	 * @return array the filtered array of variables
 	 */
 	public function filter($vars){
 		if (null != $this->context && 'Traveler' == get_class($this->context) ) {
 			
 		}	
 		return $vars;
 	}
 	
 }
 
?>
