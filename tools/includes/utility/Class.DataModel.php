<?php
	/***
	 * @author: Reynaldo Castellano III
	 * @version: 1.0
	 * Date: April 10, 2008
	 * Note: This a generic DataModel(data access) class.
	 ***/
	
	require_once('Class.dbHandler.php');
	require_once('Class.GaString.php');
	require_once('Class.iRecordset.php');
	
	abstract class DataModel
	{
		/*** error codes ***/
		const UNDEFINED_SCHEMA_NAME = 1;
		const SCHEMA_ERROR 			= 2;
		const NO_PRIMARY_KEY 		= 3;
		const FORBIDDEN_CALL_TO_PROTECTED_METHOD = 4;
		const UNDEFINED_METHOD_CALL = 5;
		const READ_ONLY_PRIMARY_KEY	= 6;
		
		const DATABASE 	= 'Database';
		const TABLE		= 'Table';
		const PROTECTED_ATTRIBUTES = 'ProtectedAttributes';
		
		private static $SCHEMA_CACHE = array();
		
		protected $mFieldMap		= array();
		protected $mFields 			= array();
		private   $mPublicFields	= null;
		protected $mPrimaryField	= null;
		protected $mProtectedFields = array();
		
		protected $mDatabaseName 	= null;
		protected $mTableName		= null;
		
		protected $mDb				= null;
		
		final protected function initialize($dbName=null,$tbName=null){
			if(is_null($dbName) || is_null($tbName)){
				throw new exception('Database name and table name are both required to initialize a DataModel object.',self::UNDEFINED_SCHEMA_NAME);
			}
			$this->mDb = new dbHandler();
			$this->mDatabaseName = $dbName;
			$this->mTableName = $tbName;
			//get the table info to initialize the fields

			//create the schema key
			$schemaKey = $this->mDatabaseName.'_'.$this->mTableName;			
			//check if the schema exists in the schema cache
			if(!array_key_exists($schemaKey,self::$SCHEMA_CACHE)){
				$sql =	'DESCRIBE '.$this->mDatabaseName.'.'.$this->mTableName;
				$rs = new iRecordset($this->mDb->execQuery($sql));
				if(!$rs->retrieveRecordCount()){
					throw new exception('Unable to locate table ['.$this->mTableName.'] in database ['.$this->mDatabaseName.'].',self::SCHEMA_ERROR);
				}
				self::$SCHEMA_CACHE[$schemaKey] = $rs;
			}
			$rs = self::$SCHEMA_CACHE[$schemaKey];			
			//check if the db.table exists
			foreach($rs as $row){
				if('pri' == strtolower($row['Key']) || 'auto_increment' == strtolower($row['Extra'])){
					$this->mPrimaryField = $row['Field'];
				}
				$this->mFields[$row['Field']] = null;
				$this->mFieldMap[ucfirst($row['Field'])] = $row['Field'];
			}
			if(is_null($this->mPrimaryField)){
				throw new exception('The table '.$this->mDatabaseName.'.'.$this->mTableName.' does not contain a primary key field. Such field is required to initialize a DataModel object.',self::NO_PRIMARY_KEY);
			}
		}
		
		/***
		 * DATABASE 			- The name of the Database
		 * TABLE				- The name of the table in which the model will be based.
		 * PROTECTED_ATTRIBUTES - An array of table field names that should be treated as protected attrbutes, thus limiting the access to the client. 
		 */
		final protected function initDataModel($params=array()){
			$database 	= (array_key_exists(self::DATABASE,$params) && strlen($params[self::DATABASE]) ? $params[self::DATABASE] : null);
			$table	 	= (array_key_exists(self::TABLE,$params) && strlen($params[self::TABLE]) ? $params[self::TABLE] : null);
			$this->initialize($database,$table);
			$this->mProtectedFields = ( array_key_exists(self::PROTECTED_ATTRIBUTES, $params) && is_array($params[self::PROTECTED_ATTRIBUTES]) ? $params[self::PROTECTED_ATTRIBUTES] : array() );
		}
		
		final protected function getPublicFields(){
			if(is_null($this->mPublicFields)){
				$fieldNames = array_keys($this->mFields);
				//remove all the fieldNames that are present in the protected fields
				$publicFields = array();
				foreach($fieldNames as $iFieldName){
					if(!in_array($iFieldName,$this->mProtectedFields)){
						$publicFields[] = $iFieldName;
					}
				}
				$this->mPublicFields = $publicFields;
			}
			return $this->mPublicFields;
		}
		
		final public function methodExists($methodName=null){
			if(in_array($methodName, get_class_methods($this))){
				return true;
			}
			else{//check on the dynamic properties
				//get the properties that are not in protected fields
				$publicFields = $this->getPublicFields();
				$fldName = substr($methodName, 3);
				$fldName = strtolower(substr($fldName, 0, 1)).substr($fldName, 1);
				return in_array($fldName,$publicFields);
			}
		}
		
		
		public function clearFields(){
			foreach($this->mFields as $iFieldName => $iFieldValue){
				$this->mFields[$iFieldName] = null;
			}
		}
		
		protected function load($param=0){
			$sql = 	'SELECT * FROM '.$this->mDatabaseName.'.'.$this->mTableName.' '.
					'WHERE '.$this->mPrimaryField.'='.GaString::makeSqlSafe($param);
			$rs = new iRecordset($this->mDb->execute($sql));
			if($rs->retrieveRecordCount() > 0){
				foreach($rs as $row){
					foreach($this->mFields as $iFieldName => $iFieldValue){
						$this->mFields[$iFieldName] = $row[$iFieldName]; 
					}
				}
				return true;
			}
			return false;
		}
		
		public function save(){
			$sqlFields = array();
			foreach($this->mFields as $fieldName => $fieldValue){
				if($fieldName != $this->mPrimaryField){
					$sqlFields[] = '`'.$fieldName.'`'.'='.GaString::makeSqlSafe($fieldValue);
				}
			}
			if(is_null($this->mFields[$this->mPrimaryField])){
				//perform add
				$sql = 	'INSERT INTO `'.$this->mDatabaseName.'`.`'.$this->mTableName.'` SET '.implode(',',$sqlFields);
				$this->mDb->execute($sql);
				$this->mFields[$this->mPrimaryField] = $this->mDb->getLastInsertedID();
				
			}
			else{
				//perform update
				$sql = 	'UPDATE `'.$this->mDatabaseName.'`.`'.$this->mTableName.'` SET '.implode(',',$sqlFields).' '.
						'WHERE `'.$this->mPrimaryField.'`='.GaString::makeSqlSafe($this->mFields[$this->mPrimaryField]);
				$this->mDb->execute($sql);
			}
		}
		
		public function delete(){
			$qry = 	'DELETE FROM `'.$this->mDatabaseName.'`.`'.$this->mTableName.'` '.
					'WHERE `'.$this->mPrimaryField.'` = '.GaString::makeSqlSafe($this->mFields[$this->mPrimaryField]);					
			$this->mDb->execQuery($qry);
			$this->clearFields();
		}
		
		/** takes an array whose keys are the fieldnames and the value as fieldvalues **/
		protected function setFields($fields){
			foreach($fields as $iFieldName => $iFieldValue){
				if(array_key_exists($iFieldName,$this->mFields)){
					$this->mFields[$iFieldName] = $iFieldValue;
				}
			}
		}
		
		protected function executeMethod($method,$arguments=array()){
			return $this->__call($method,$arguments);
		}
		
		/***
			For dynamic setters and getters
		***/
		public function __call($method,$arguments) {
	        $prefix = substr($method, 0, 3);
	        $fldName = substr($method, 3);
	        $iFieldName = $this->_getFieldName($fldName);
	        if(!is_null($iFieldName) && ('get'==$prefix || 'set'==$prefix)){
				//check if the field is protected				
				if(in_array($iFieldName,$this->mProtectedFields)){
					$trace = debug_backtrace();
					if(!array_key_exists(2,$trace) || !array_key_exists('object',$trace[2]) || $trace[2]['object'] !== $this){
						throw new exception('Call to a protected method '.get_class($this).'::'.$method.' in '.$trace[1]['file'].' on line '.$trace[1]['line'],self::FORBIDDEN_CALL_TO_PROTECTED_METHOD);
					}
				}
				if($prefix == 'get'){
					return $this->mFields[$iFieldName];
		        }
				else{
					if($iFieldName != $this->mPrimaryField){
						$this->mFields[$iFieldName] = (count($arguments) ? $arguments[0] : null);
					}
					else{
						throw new exception('Unable to set primary field: '.$iFieldName,self::UNDEFINED_METHOD_CALL);
					}
				}
			}
			else{
				throw new exception('Undefined method call: '.$method);
			}
	    }
	    
	    private function _getFieldName($fldName){	    	
			//return (array_key_exists($fldName,$this->mFieldMap) ? $this->mFieldMap[$fldName] :null);
			foreach($this->mFields as $iFieldName => $iFieldValue){	    		
	    		if($fldName == ucfirst($iFieldName)) return $iFieldName;
	    	}	    	
	    	return null;
			
	    }
	}
?>