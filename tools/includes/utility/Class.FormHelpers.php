<?
/**
* <b>FormHelpers</b> class.
* @author Aldwin S. Sabornido
* @version 1.0 PHP5
* @see http://www.GoAbroad.com/
* @copyright 2006-2007
*/

class FormHelpers
{
	private static $tab_index = 1;
	
	public static function instance()
 	{
 		if (self::$instance == null ) {
 			self::$instance = new FormHelpers();
 		}
 		
 		return self::$instance;	
 	}
 	
	static function CreateTextBox($name, $value = '', $attribs = '') {
		// attribs accepts an associative array
		// key value pairs are mapped to the corresponding attribute set
		// the key would be the attribute name, and the value will be the value of the attribute
		// for example the array ('class' => 'foo', 'value' => 'bar') would output the following attribute sets
		// class="foo" value="bar"
		
		$attributes = '';
		
		foreach ($attribs as $attname => $attvalue) {
			if ($attname == 'disabled') {
				$attvalue ? $attributes .= "disabled=\"disabled\"" : $attributes .= '';
				break;
			}
			
			$attributes .= "$attname=\"$attvalue\" ";
		}
		
		if (!array_key_exists('id', $attribs)) {
			$id = "id=\"$name\"";
		} else {
			$id = '';			
		}
		
		$input = "<input type=\"text\" $id name=\"$name\" value=\"".htmlspecialchars($value)."\" $attributes />";
		//print_r($attribs);
		return $input;
	}
	
 	static function CreateOptions($assoc,$keyvalue='')
 	{
 		
 		$options = "";
		foreach($assoc as $key => $value)
 		{
 			
 			($keyvalue==$key & $keyvalue !== '')? $selected = 'selected="selected"': $selected = '';
 			$options .= "<option value=\"$key\"  $selected>".htmlspecialchars($value, ENT_NOQUOTES).'</option>';	
 		}
 		return $options;
 	}
 	
 	static function CreateCollectionOption($arrvalues,$method1,$method2,$keyvalue='')
 	{
 		$options = "";
 		foreach($arrvalues as $arrvalue)
 		{
			
 			//$key = call_user_method($method1,$arrvalue);
 			//$value = call_user_method($method2,$arrvalue);
			$key = call_user_func(array($arrvalue, $method1));
 			$value = call_user_func(array($arrvalue, $method2));
			
 			($keyvalue==$key)? $selected = 'selected="selected"': $selected = '';
 			$options .= "<option value=\"$key\" $selected>".htmlspecialchars($value, ENT_NOQUOTES).'</option>';	
 		}
 		return $options;	 
 	}		 	 	
 	
 	static function CreateRadio($assoc,$name,$keyvalue='')
 	{
 		$options = array();
		$i = 0;
		foreach($assoc as $key => $value)
 		{
 			$i++;
			($keyvalue==$key)? $selected = 'checked="true"': $selected = '';
 			$options[]= "<input type=\"radio\" name=\"$name\" id=\"$name\" value=\"$key\" $selected />".htmlspecialchars($value, ENT_NOQUOTES);	
 		}
 		return $options;
 	}
 	
 	static function CreateCheckbox($assoc,$name,$arrvalues='', $prefix = '', $suffix = '')
 	{
 		$options = array();
		$i = 0;
		foreach($assoc as $key => $value) {
			$i++;
 			$selected = '';
 			if (is_array($arrvalues)) {
 				// TODO: optimize this code;
				foreach($arrvalues as $arrvalue) {
 					if ($arrvalue==$key) {
	 					 $selected = 'checked="checked"';
	 					 break;
 					}
 				}
 			}
			$id  = $name.'_'.$i;
 			$options[]= $prefix."<input type=\"checkbox\" name=\"$name\" id=\"$id\" value=\"$key\" $selected /> <label for=\"$id\">". htmlspecialchars($value, ENT_NOQUOTES).'</label>'.$suffix;	
 		}
 		return $options;
 	}		 
 	
 	static function CreateChkbox_ASSOC($assoc,$name,$arrvalues='', $prefix = '', $suffix = '')
 	{
 		$options = array();
		foreach($assoc as $elem)
 		{
 			$selected = '';
 			if (is_array($arrvalues))
 			{
 				foreach($arrvalues as $arrvalue)
 				{
 					if ($arrvalue==$elem["key"])
 					{
	 					 $selected = 'checked="checked"';
	 					 break;
 					}
 				}
 			}
			
 			$options[]= $prefix."<input type='checkbox' name='" . $name . "' id='" . $elem["ID"] . "' value=" . $elem["key"] . " " . $selected . "/>".
 				"<label for='" . $elem["ID"] . "'>" . htmlspecialchars($elem["value"], ENT_NOQUOTES) . "</label>".$suffix;	
 		}
 		return $options;
 	}
 	
 	static function CreateCalendar($date_value=NULL,$day_name='day',$month_name='month',$year_name='year'){
 		
 		$now           = date("d.m.Y");
 		$now           = explode('.',$now);
 		$arr_days      = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31);
 		$arr_months    = array('January','February','March','April','May','June','July','August','September','October','November','December');
 		
 		$day_value     = $now[0];
 		$month_value   = $now[1];
 		$year_value    = $now[2];
 		
 		if($date_value != NULL){
 			$date_value   = explode('-',$date_value);
 			$day_value    = $date_value[2];
	 		$month_value  = $date_value[1];
	 		$year_value   = $date_value[0];
 		}
 		
 		$option_day    = '<span><select name="'.$day_name.'"   id="'.$day_name.'"   >';
 		$option_month  = '<span><select name="'.$month_name.'" id="'.$month_name.'" >';
 		$option_year   = '<span><select name="'.$year_name.'"  id="'.$year_name.'"  >';
 		
 		foreach($arr_days as $arr_day){
			$selected = '';
			if($arr_day==$day_value) $selected = 'selected'; 			
 			$option_day .= '<option value="'.$arr_day.'" '.$selected.'>'.$arr_day.'</option>'; 	
 		}
 		$option_day .= '</select></span> ';
 		
 		for($i=0;$i<count($arr_months);$i++){
 			$selected = '';
 			$value    = $i+1;
			if($value==$month_value) $selected = 'selected';
 			$option_month .= '<option value="'.$value.'" '.$selected.'>'.$arr_months[$i].'</option>'; 	
 		}
 		$option_month .= '</select></span> ';
 		
 		for($i=1982;$i<=($now[2]+5);$i++){
 			$selected = '';
			if($i==$year_value) $selected = 'selected';
 			$option_year .= '<option value="'.$i.'" '.$selected.'>'.$i.'</option>'; 	
 		}
 		$option_year .= '</select></span> ';
 		
 		$option_calendar = <<<BOF
 		$option_day
 		$option_month
 		$option_year
BOF;

		return $option_calendar; 
 	}
 	
 	static function CreateRangeCalendar($date_value=NULL,$param1,$param2,$day_name='day',$month_name='month',$year_name='year'){
 		
 		$arr_days      = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31);
 		$arr_months    = array('January','February','March','April','May','June','July','August','September','October','November','December');
 		
 		$input_start   = '<input type="hidden" name="sdate" id="sdate" value="'.$param1.'" />';
 		$input_end     = '<input type="hidden" name="edate" id="edate" value="'.$param2.'" />';
 		
 		$param1        = explode('-',$param1);
		$day_param1    = $param1[2];
 		$month_param1  = $param1[1];
 		$year_param1   = $param1[0];
 		
 		$param2        = explode('-',$param2);
		$day_param2    = $param2[2];
 		$month_param2  = $param2[1];
 		$year_param2   = $param2[0];
 		 		
 		if($date_value != NULL){
 			$date_value   = explode('-',$date_value);
 			$day_value    = $date_value[2];
	 		$month_value  = $date_value[1];
	 		$year_value   = $date_value[0];
 		}else{
 			$day_value    = $day_param1;
	 		$month_value  = $month_param1;
	 		$year_value   = $year_param1;
 		}
 		 		 		
 		$option_day    = '<span><select name="'.$day_name.'"   id="'.$day_name.'"   >';
 		$option_month  = '<span><select name="'.$month_name.'" id="'.$month_name.'" >';
 		$option_year   = '<span><select name="'.$year_name.'"  id="'.$year_name.'"  >';
 		
 		foreach($arr_days as $arr_day){
			$selected = '';
			if($arr_day==$day_value) $selected = 'selected'; 			
 			$option_day .= '<option value="'.$arr_day.'" '.$selected.'>'.$arr_day.'</option>'; 	
 		}
 		$option_day .= '</select></span> ';
 		
 		for($i=$month_param1;$i<=$month_param2;$i++){
 			$selected = '';
 			$value    = $i-1;
			if($i==$month_value) $selected = 'selected';
 			$option_month .= '<option value="'.$i.'" '.$selected.'>'.$arr_months[$value].'</option>'; 	
 		}
 		$option_month .= '</select></span> ';
 		
 		for($i=$year_param1;$i<=$year_param2;$i++){
 			$selected = '';
			if($i==$year_value) $selected = 'selected';
 			$option_year .= '<option value="'.$i.'" '.$selected.'>'.$i.'</option>'; 	
 		}
 		$option_year .= '</select></span> ';
 		
 		$option_calendar = <<<BOF
 		$option_day
 		$option_month
 		$option_year
 		$input_start
 		$input_end
BOF;

		return $option_calendar; 
 	}
 	
 	static function GetTabIndex($start = 1) {
 		if (self::$tab_index === 1) {
 			self::$tab_index = $start;
 		}
 		
 		$out = self::$tab_index;
 		self::$tab_index += 1;
 		return $out;
 	}
 	
 }	
?>
