<?php
	require_once('Class.GaDateTime.php');
	require_once('gaLogs/Class.LoggerConnection.php');
	require_once('gaLogs/Class.LogTrailTable.php');
	
	class GaLogSystem
	{
		static private $instance = null;
		static public function create(){
			if(is_null(self::$instance)){
				self::$instance = new self();
			}
			return self::$instance;
		}
		
		/*** For Testability Purposes ***/
		static public function reset(){
			self::$instance = null;
		}
		
		private $mDb 			= null;
		private $mVersion		= null;
		private $mDateLastPurged= null;
		
		private function __construct(){
			$this->mDb = LoggerConnection::create()->getDbHandler();
			$rs = new iRecordset($this->mDb->execute('SELECT * FROM `GaLogs`.`tblSystemInfo`'));
			$this->mVersion = $rs->getVersion(0);
			$this->mDateLastPurged = $rs->getDateLastPurged(0);
		}
		
		public function getVersion(){
			return $this->mVersion;
		}
		
		public function getDateLastPurged(){
			return $this->mDateLastPurged;
		}
		/***
		public function isInDebugMode(){
			return false;
		}
		***/
		public function performSystemMaintainance(){
			//check if its time for maintainance
			//if now = datelastpurged + 1 month
			if(!GaDateTime::isValidDate($this->mDateLastPurged) || 1==GaDateTime::dateCompare(GaDateTime::dbDateTimeFormat(),GaDateTime::dbDateTimeFormat(GaDateTime::dateAdd($this->mDateLastPurged,'m',1)))){
				LogTrailTable::purgeAllLogTrailTables();
				$sql = 	'UPDATE GaLogs.tblSystemInfo SET '.
						'dateLastPurged = '.$this->mDb->makeSqlSafeString(GaDateTime::dbDateTimeFormat());
				$this->mDb->execute($sql);
			}
		}
	}
?>