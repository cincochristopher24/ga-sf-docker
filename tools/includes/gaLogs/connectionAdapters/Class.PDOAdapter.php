<?php
	/***
	 * @author: Reynaldo Castellano III
	 * A class that enables the GaLogs system to operate on a PDO connection.
	*****/
	require_once('gaLogs/interface/Interface.IConnectionAdapter.php');
	
	class PDOAdapter implements IConnectionAdapter
	{
		private $mConMap = array();
		private $mConStack = array();
		private $mConID = 0;
		
		public function __construct(){}
		
		public function connect($con){
			return class_exists('PDO') && is_object($con) && ($con instanceof PDO);
		}
		
		public function getActiveDatabase($con){
			$row = $con->query('SELECT DATABASE() as currentDB')->fetch(PDO::FETCH_ASSOC);
			return $row['currentDB'];
		}
		
		public function isConnectionOpen($con){
			return $con instanceof PDO;
		}
		
		public function isInTransaction($con){
			if($this->isConnectionOpen($con)){
				try{
					$con->exec('SAVEPOINT glsvpnt;');
					$res = $con->exec('ROLLBACK TO SAVEPOINT glsvpnt;');
					$con->exec('RELEASE SAVEPOINT glsvpnt;');
					return $res !== false;
				}
				catch(exception $e){}
			}
			return false;
		}
		
		public function getConnection($con){
			foreach($this->mConMap as $conID => $conResource){
				if($con === $conResource){
					return $this->mConStack[$conID];
				}
			}
			//we dont have the resource in the stack, lets create a new ClientConnection object.
			$conID = 'pdo_' . ++$this->mConID;
			//NOTE: We are creating a circular reference here. But this time its ok, since we need the connections untill then end of the script.
			$clientCon = new ClientConnection($con, $conID, $this);
			$this->mConMap[$conID] = $con;
			$this->mConStack[$conID] = $clientCon;
			
			return $clientCon;
		}
		
		public function isSqlSyntaxOk($con,$sql){
			return true;
		}
		
		public function setConnectionVars($con, $vars=array()){
			$conVars = array();
			foreach($vars as $iVarName => $iVarVal){
				$conVars[] = "@$iVarName = '$iVarVal'";
			}
			$con->exec('SET '.implode(', ',$conVars).';');
		}
		
		public function getConnectionVars($con, $varNames){
			$prependedVarNames = array();
			$varValues = array();
			foreach($varNames as $iVarName){
				if(isset($iVarName{0})){
					$prependedVarNames[] = '@'.$iVarName;
					$varValues[$iVarName] = null;
				}
			}
			if(count($prependedVarNames)){
				$sql = 'SELECT '.implode(', ',$prependedVarNames);
				$row = $con->query($sql)->fetch(PDO::FETCH_ASSOC);
				foreach($varValues as $varKey => $varVal){
					$varValues[$varKey] = $row['@'.$varKey];
				}
			}
			return $varValues;
		}
		
	}
?>