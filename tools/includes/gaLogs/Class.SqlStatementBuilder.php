<?php
	/***
		- Other DmlCommands that needs to be supported 
			- Replace
			- Load Data InFile
		- Returns an SqlStatement object from an sql string.
	***/
	class SqlStatementBuilder
	{
		static private $instance = null;
		static public function create(){
			if(is_null(self::$instance)){
				self::$instance = new SqlStatementBuilder();
			}
			return self::$instance;
		}
		
		private $mDmlStmtID 		= 0;
		private $mSqlCommands 		= array();
		private $mSqlStatementMap	= array();
		
		private function __construct(){
			$osDelimiter =  '/';
			$arDir = explode($osDelimiter,__FILE__);
			$strategyDir = implode($osDelimiter, array_slice($arDir,0,count($arDir)-1)).$osDelimiter.'sqlStatementStrategies';
			$strategyFiles = scandir($strategyDir);
			$strategyFiles = array_slice($strategyFiles,2,count($strategyFiles));
			foreach($strategyFiles as $iFile){
				//extract the command key and the classname
				$fileNameLen = strlen($iFile);
				if('.php' == substr($iFile,$fileNameLen-4, $fileNameLen)){
					require_once($strategyDir.'/'.$iFile);
					$comKey = strtoupper(str_replace(array('Class.Sql','StatementStrategy.php'),'',$iFile));
					$clsName = str_replace(array('Class.','.php'),'',$iFile);
					$this->mSqlCommands[$comKey] = $clsName;
				}
			}
		}
			
		public function buildSqlStatement($sql){
			//get the sql Command
			$strategy = $this->getSqlStatementStrategy($sql);
			if(!is_null($strategy) && $strategy->isSyntaxOk()){
				return new SqlStatement($strategy);
			}
			return null;
		}
		
		public function getSqlStatementStrategy($sql){
			$sqlCommand = $this->extractSqlCommand($sql);
			if(array_key_exists($sqlCommand,$this->mSqlCommands)){
				return new $this->mSqlCommands[$sqlCommand]($sql);
			}
			return null;
		}
		
		public function getStoredSqlStatements(){
			return $this->mSqlStatementMap;
		}
		
		private function extractSqlCommand($sqlStr){
			$sqlStr = trim($sqlStr);
			$command = strtoupper(substr($sqlStr,0,strpos($sqlStr,' ')));
			return (array_key_exists($command,$this->mSqlCommands) ? $command : null);
		}
	}
?>