<?php
	/***
	 * @author: Reynaldo Castellano III
	 */
	/***
	- Single Table Syntax: -
	
	DELETE [LOW_PRIORITY] [QUICK] [IGNORE] 
	FROM tbl_name
    [WHERE where_condition]
    [ORDER BY ...]
    [LIMIT row_count]
	    
	- Multiple-table syntax: -
	
	DELETE [LOW_PRIORITY] [QUICK] [IGNORE]
    	tbl_name[.*] [, tbl_name[.*]] ...
    FROM table_references
    [WHERE where_condition]

	- With the key word using: -	
	DELETE [LOW_PRIORITY] [QUICK] [IGNORE]
    FROM tbl_name[.*] [, tbl_name[.*]] ...
    USING table_references
    [WHERE where_condition]
	
	RULES:
		- After the DELETE command, if the next word is not FROM or One of the MODIFIERS, it should be the table names with an optional *.
		- After the FROM word, it should be the table names to be used in the query. 
			If there are tables in the DELETE command, it should also be present in the FROM clause.
		- if there are tables after the DELETE command, There should be no USING clause.
			- If USING is present, we can be sure 
		
	DELETE FROM tblTraveler WHERE travelerID = 0
	
		= SELECT tblTraveler.* FROM tblTraveler WHERE travelerID = 0
	
	DELETE LOW_PRIORITY, QUICK, IGNORE tbl1.*, tbl2.* 
 	FROM tbl1, tbl2,
	***/
	require_once('gaLogs/Class.ClauseTokenizer.php');
	require_once('gaLogs/abstract/Class.AbstractSqlStatementStrategy.php');
	
	final class SqlDeleteStatementStrategy extends AbstractSqlStatementStrategy
	{
		public function __construct($sql){
			$this->mSqlString = $sql;
			if($this->isSyntaxOk()){
				//find the tables that are to be affected by the current dml statement. (Subject Tables)
				$clauseRegExs = array(
					'commandClause' => 	'/DELETE /i',
					'fromClause'	=> 	'/ FROM /i',
					'usingClause'	=>	'/ USING /i',
					'whereClause'	=>	'/ WHERE /i',
					'orderByClause'	=>	'/ ORDER BY /i',
					'limitClause'	=>	'/ LIMIT /i'
				);
				$modifiers = array(
					'commandClause' =>	array(
						'LOW_PRIORITY' 	=> '/ LOW_PRIORITY/i',
						'QUICK' 		=> '/ QUICK/i',
						'IGNORE'		=> '/ IGNORE/i'
					)
				);
				$clauses = ClauseTokenizer::tokenize($this->mSqlString,$clauseRegExs,$modifiers);
				//if there are tables in the DELETE clause those are the tables that will be affected. No subqueries here! =)
				if(strtoupper(trim($clauses['commandClause']['token'])) != 'DELETE'){
					$this->mAffectedTableNames = explode(',',substr($clauses['commandClause']['token'],7,strlen($clauses['commandClause']['token'])-7)); 	
				}
				else{//else tables in the from clause will be the target tables. No subquries here! =)
					$this->mAffectedTableNames = explode(',',substr($clauses['fromClause']['token'],4,strlen($clauses['fromClause']['token'])-4));
				}
			}
		}
		
		public function getCommand(){
			return 'DELETE';
		}
		
		public function getSqlStatement(){
			return $this->mSqlString;
		}
		
		public function getSubjectTableNames(){
			return $this->mAffectedTableNames;
		}
		
		public function isDmlStatement(){
			return true;
		}
		
	}
?>