<?php
	/***
		SYNTAX: PREPARE stmt_name FROM preparable_stmt
		NOTE: This class handles statements that were prepared with the alternative sql syntax.
	***/
	
	require_once('gaLogs/Class.ClauseTokenizer.php');
	require_once('gaLogs/abstract/Class.AbstractSqlStatementStrategy.php');
	require_once('gaLogs/Class.SqlStatementBuilder.php');
	require_once('gaLogs/Class.DataTracker.php');
	
	final class SqlExecuteStatementStrategy extends AbstractSqlStatementStrategy
	{
		private $mCoreStmtStrategy = null;
		private $mVars 	= array();
		
		public function __construct($sql){
			//just get the prepared stmt_name
			$this->mSqlString = $sql;
			$clauseRegExs = array(
				'executeClause' => 	'/EXECUTE /i',
				'usingClause'	=> 	'/ USING /i'
			);
			$clauses = ClauseTokenizer::tokenize($this->mSqlString,$clauseRegExs,array());
			
			//remove 'EXECUTE' from the executeClause to get the stmt_name
			$commandStr = trim($clauses['executeClause']['token']);
			$stmtName = trim(substr($commandStr,7,strlen($commandStr)));
			
			if(isset($stmtName{0})){
				$activeClientCon = DataTracker::create()->getActiveClientConnection();
				$stmtStrategy = $activeClientCon->getDmlPreparedStatement($stmtName);
				if(!is_null($stmtStrategy)){
					$this->mCoreStmtStrategy = $stmtStrategy;
					//get the values of the client vars
					$varNameStr = trim(substr($clauses['usingClause']['token'],5,strlen($clauses['usingClause']['token'])));
					if(isset($varNameStr{0})){
						$varNames = explode(',',str_replace(array('@',';',' '),'',$varNameStr));
						$this->mVars = $activeClientCon->getConnectionVars($varNames);
					}
				}
			}
		}
		
		public function isSyntaxOk(){
			return true;
		}
		
		public function getCommand(){
			return !is_null($this->mCoreStmtStrategy) ? $this->mCoreStmtStrategy->getCommand() : null;
		}
		
		public function getSqlStatement(){
			return !is_null($this->mCoreStmtStrategy) ? $this->mCoreStmtStrategy->getSqlStatement()." \n #PARAMS# \n". json_encode(array_values($this->mVars)) : null;
		}
		
		public function getSubjectTableNames(){
			//return an empty array to avoid reevaluation of the Dml tables as it has already been done during the preparation of the statement (PREPARE)
			return array();
		}
		
		public function isDmlStatement(){
			//If the coreStmtStrategy is not NULL, that means that the prepared stmt is a DML
			return !is_null($this->mCoreStmtStrategy);
		}
		
	}
?>