<?php
	/***
		SYNTAX: PREPARE stmt_name FROM preparable_stmt
		NOTE: This class handles statements that were prepared with the alternative sql syntax.
	***/
	
	require_once('gaLogs/Class.ClauseTokenizer.php');
	require_once('gaLogs/abstract/Class.AbstractSqlStatementStrategy.php');
	require_once('gaLogs/Class.SqlStatementBuilder.php');
	require_once('gaLogs/Class.DataTracker.php');
	
	final class SqlPrepareStatementStrategy extends AbstractSqlStatementStrategy
	{
		private $mCoreStmtStrategy = null;
		private $mStmtName = null;
		
		public function __construct($sql){
			//just get the prepared stmt_name and the preparable_stmt
			$this->mSqlString = $sql;
			$clauseRegExs = array(
				'commandClause' => 	'/PREPARE /i',
				'fromClause'	=> 	'/ FROM /i'
			);
			$clauses = ClauseTokenizer::tokenize($this->mSqlString,$clauseRegExs,array());
			
			//remove 'PREPARE' from the commandClause to get the stmt_name
			$commandStr = trim($clauses['commandClause']['token']);
			$stmtName = trim(substr($commandStr,7,strlen($commandStr)));
			
			$this->mStmtName = $stmtName;
			
			//remove the 'FROM' from the fromClause to get the preparable_stmt
			$stmtStr = trim($clauses['fromClause']['token']);
			$qoutedStmt = trim(substr($stmtStr,4,strlen($stmtStr)));
			$stmt = trim(substr($qoutedStmt, 0, strlen($qoutedStmt)-1));
			if(isset($stmtName{0}) && isset($stmt{0})){				
				//the sql statement is a client variable, lets get its value
				if('@'==$stmt{0}){
					$varName = substr($stmt,1,strlen($stmt));
					$conVars = DataTracker::create()->getActiveClientConnection()->getConnectionVars(array($varName));
					$stmt = $conVars[$varName];
				}
				else{// the statement is a string,  lets remove the sorrounding qoutes that was used to prepare it.
					$stmt = substr($stmt,1,strlen($stmt)-1);
				}
				//if $this->mCoreStmtStrategy is NULL, that means that the prepared statement is NOT DML.
				$this->mCoreStmtStrategy = SqlStatementBuilder::create()->getSqlStatementStrategy($stmt);
			}
			$this->mSqlString = $stmt;
		}
		
		public function trackChanges($res){
			//if successfull, register the strategy to the client connection
			if($res !== false){
				if(!is_null($this->mCoreStmtStrategy)){
					DataTracker::create()->getActiveClientConnection()->registerPreparedDmlStatement($this->mStmtName, $this);
				}
				else{
					DataTracker::create()->getActiveClientConnection()->unregisterPreparedDmlStatement($this->mStmtName);
				}
			}
		}
		
		public function isSyntaxOk(){
			return true;
		}
		
		public function getCommand(){
			return !is_null($this->mCoreStmtStrategy) ? $this->mCoreStmtStrategy->getCommand() : null;
		}
		
		public function getSqlStatement(){
			//return !is_null($this->mCoreStmtStrategy) ? $this->mCoreStmtStrategy->getSqlStatement() : null;
			return $this->mSqlString;
		}
		
		public function getSubjectTableNames(){
			return !is_null($this->mCoreStmtStrategy) ? $this->mCoreStmtStrategy->getSubjectTableNames() : array();
		}
		
		public function isDmlStatement(){
			return false;
		}
		
	}
?>