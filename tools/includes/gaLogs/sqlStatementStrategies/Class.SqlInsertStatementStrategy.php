<?php
	/***
	 * @author: Reynaldo Castellano III
	 */
	/***	
	INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE]
    [INTO] tbl_name [(col_name,...)]
    VALUES ({expr | DEFAULT},...),(...),...
    [ ON DUPLICATE KEY UPDATE col_name=expr, ... ]
	Or:
	
	INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE]
	    [INTO] tbl_name
	    SET col_name={expr | DEFAULT}, ...
	    [ ON DUPLICATE KEY UPDATE col_name=expr, ... ]
	Or:
	
	INSERT [LOW_PRIORITY | HIGH_PRIORITY] [IGNORE]
    [INTO] tbl_name [(col_name,...)]
    SELECT ...
    [ ON DUPLICATE KEY UPDATE col_name=expr, ... ]
    
	***/
	
	/***
	 * If the select is used, there should be no VALUES and INTO clauses.
	 * Good thing, only one table is always involved
	 */
	require_once('gaLogs/Class.ClauseTokenizer.php');
	require_once('gaLogs/abstract/Class.AbstractSqlStatementStrategy.php');
	
	final class SqlInsertStatementStrategy extends AbstractSqlStatementStrategy
	{
		public function __construct($sql){
			$this->mSqlString = $sql;
			if($this->isSyntaxOk()){
				//find the tables that are to be affected by the current dml statement. (Subject Tables)
				$clauseRegExs = array(
					'commandClause' 	=> 	'/INSERT /i',
					'intoClause'		=> 	'/ INTO /i',
					'valueClause'		=>	'/ VALUES /i',
					'setClause'			=>	'/ SET /i',
					'SELECTClause'		=>	'/ SELECT /i',
					'duplicateClause'	=> 	'/ ON DUPLICATE KEY UPDATE /i'
				);
				$modifiers = array(
					'commandClause' =>	array(
						'LOW_PRIORITY' 	=> '/ LOW_PRIORITY/i',
						'HIGH_PRIORITY' => '/ HIGH_PRIORITY/i',
						'IGNORE'		=> '/ IGNORE/i'
					)
				);
				$clauses = ClauseTokenizer::tokenize($this->mSqlString,$clauseRegExs,$modifiers);
				//if there are tables in the INSERT, get the table from there.
				if(strtoupper(trim($clauses['commandClause']['token'])) != 'INSERT'){
					$tableClause =   substr($clauses['commandClause']['token'],6,strlen($clauses['commandClause']['token'])-6);
					$tableClause =   preg_replace(array('/\(.*?\)/'),'',$tableClause);
				}
				elseif(strlen(trim($clauses['intoClause']['token']))){
					$tableClause =   substr($clauses['intoClause']['token'],4,strlen($clauses['intoClause']['token'])-4);
					$tableClause =   preg_replace(array('/\(.*?\)/'),'',$tableClause);
				}
				$this->mAffectedTableNames = array(trim($tableClause));
			}
		}
		
		public function getCommand(){
			return 'INSERT';
		}
		
		public function getSqlStatement(){
			return $this->mSqlString;
		}
		
		public function getSubjectTableNames(){
			return $this->mAffectedTableNames;
		}
		
		public function isDmlStatement(){
			return true;
		}
		
	}
?>