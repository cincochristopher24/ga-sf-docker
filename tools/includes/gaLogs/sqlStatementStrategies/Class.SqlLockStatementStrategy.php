<?php
	/***
		LOCK TABLES tbl_name [AS alias] { READ [LOCAL] | [LOW_PRIORITY] WRITE }
		[, tbl_name [AS alias] {READ [LOCAL] | [LOW_PRIORITY] WRITE}]
	***/
	
	require_once('gaLogs/Class.ClauseTokenizer.php');
	require_once('gaLogs/abstract/Class.AbstractSqlStatementStrategy.php');
	require_once('gaLogs/Class.DataTracker.php');
	
	final class SqlLockStatementStrategy extends AbstractSqlStatementStrategy
	{
		public function __construct($sql){
			//find the tables that are to be affected by the current dml statement. (Subject Tables)
			$this->mSqlString 	= $sql;
			if($this->isSyntaxOk()){
				$clauseRegExs = array(
					'commandClause' => 	'/LOCK TABLES /i'				
				);
				$modifiers = array();
				$clauses = ClauseTokenizer::tokenize($this->mSqlString,$clauseRegExs,$modifiers);			
				//take out the main command from the clauses
				$origCommandClause = trim(substr($clauses['commandClause']['token'],11,strlen($clauses['commandClause']['token'])-11));
			
				//replace all strings quoted with `` (dummy clause)
				$dummyCommandClause = preg_replace_callback('/`.*?`/',create_function('$matches','return str_repeat("~",strlen($matches[0]));'),$origCommandClause);
			
				//remove aliases			
				preg_match_all('/[\s]as[\s]*.*?[\s]|[\s]*LOW_PRIORITY[\s]*/i', $dummyCommandClause, $aliasMatches, PREG_OFFSET_CAPTURE);

				$posCompensate = 0;
				foreach($aliasMatches[0] as $iMatch){
					$origCommandClause = substr_replace($origCommandClause,'',$iMatch[1]-$posCompensate,strlen($iMatch[0]));
					$dummyCommandClause = substr_replace($dummyCommandClause,'',$iMatch[1]-$posCompensate,strlen($iMatch[0]));
					$posCompensate += (strlen($iMatch[0]));
				}
				$origLockClauses = array();
				$dummyLockClauses = explode(',',$dummyCommandClause);
				$pos = 0;
				foreach($dummyLockClauses as $key => $iDumLockClause){
					$lockClauseLen = strlen($iDumLockClause);
					$origLockClauses[] = trim(substr($origCommandClause, $pos ,$lockClauseLen));
					$dummyLockClauses[$key] = trim($iDumLockClause);
					$pos += $lockClauseLen + 1;
				}
			
				foreach($dummyLockClauses as $key => $iDumLockClause){
					//check only the tables with write locks
					preg_match_all('/[\s]?WRITE[\s]*.*/i', $iDumLockClause, $writeMatches, PREG_OFFSET_CAPTURE);
					$posCompensate = 0;
					foreach($writeMatches[0] as $iMatch){
						$this->mAffectedTableNames[] = trim(substr_replace($origLockClauses[$key],'',$iMatch[1]-$posCompensate,strlen($iMatch[0])));
						$posCompensate += (strlen($iMatch[0]));
					}
				}
			}
		}
		
		public function getCommand(){
			return 'LOCK TABLES';
		}
		
		public function getSqlStatement(){
			return $this->mSqlString;
		}
		
		public function getSubjectTableNames(){
			return $this->mAffectedTableNames;
		}
		
		public function isSyntaxOk(){
			if(is_null($this->mIsSyntaxOk)){
				try{
					$loggerDB = LoggerConnection::create()->getDbHandler();
					$loggerDBName = $loggerDB->getActiveDatabase();
					$loggerDB->setDB(DataTracker::create()->getActiveClientConnection()->getActiveDatabase());
					$loggerDB->execute($this->mSqlString);
					$this->mIsSyntaxOk = true;
				}
				catch(exception $e){
					$this->mIsSyntaxOk = false;
				}
				$loggerDB->execute('UNLOCK TABLES');
				$loggerDB->setDB($loggerDBName);
			}
			return $this->mIsSyntaxOk;
		}
		
		public function isDmlStatement(){
			return false;
		}
	}
?>