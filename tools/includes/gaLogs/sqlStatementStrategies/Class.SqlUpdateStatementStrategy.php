<?php
	/***
	 * @author: Reynaldo Castellano III
	 */
	/***
	Single-table syntax:
	
	UPDATE [LOW_PRIORITY] [IGNORE] tbl_name
    SET col_name1=expr1 [, col_name2=expr2 ...]
    [WHERE where_condition]
    [ORDER BY ...]
    [LIMIT row_count]
    
	Multiple-table syntax:
	
	UPDATE [LOW_PRIORITY] [IGNORE] table_references
    SET col_name1=expr1 [, col_name2=expr2 ...]
    [WHERE where_condition]
	***/
	require_once('gaLogs/Class.ClauseTokenizer.php');
	require_once('gaLogs/abstract/Class.AbstractSqlStatementStrategy.php');
	
	final class SqlUpdateStatementStrategy extends AbstractSqlStatementStrategy
	{
		public function __construct($sql){
			$this->mSqlString 	= $sql;
			if($this->isSyntaxOk()){
				//find the tables that are to be affected by the current dml statement. (Subject Tables)
				$clauseRegExs = array(
					'commandClause' => 	'/UPDATE /i',
					'setClause'		=> 	'/ SET /i',
					'whereClause'	=>	'/ WHERE /i',
					'orderByClause'	=>	'/ ORDER BY /i',
					'limitClause'	=>	'/ LIMIT /i'
				);
				$modifiers = array(
					'commandClause' =>	array(
						'LOW_PRIORITY' 	=> '/ LOW_PRIORITY/i',
						'IGNORE'		=> '/ IGNORE/i'
					)
				);
				$clauses = ClauseTokenizer::tokenize($this->mSqlString,$clauseRegExs,$modifiers);
				//take out the main command from the clauses
				$origCommandClause = substr($clauses['commandClause']['token'],6,strlen($clauses['commandClause']['token'])-6);
				//replace all strings quoted with `` (dummy clause)
				$dummyCommandClause = preg_replace_callback('/`.*?`/',create_function('$matches','return str_repeat("~",strlen($matches[0]));'),$origCommandClause);
				preg_match_all('/[\s]+,[\s]+/', $dummyCommandClause, $matches, PREG_OFFSET_CAPTURE);
				$posCompensate = 0;
				foreach($matches[0] as $iMatch){
					$origCommandClause = substr_replace($origCommandClause,',',$iMatch[1]-$posCompensate,strlen($iMatch[0]));
					$posCompensate += (strlen($iMatch[0])-1);
				}
				$this->mAffectedTableNames = explode(',',$origCommandClause);
			}
		}
		
		public function getCommand(){
			return 'UPDATE';
		}
		
		public function getSqlStatement(){
			return $this->mSqlString;
		}
		
		public function getSubjectTableNames(){
			return $this->mAffectedTableNames;
		}
		
		public function isDmlStatement(){
			return true;
		}
	}
?>