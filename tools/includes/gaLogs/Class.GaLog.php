<?php
	/***
	 * @author: Reynaldo Castellano III
	 */
	require_once('gaLogs/exception/Class.GaLogException.php');
	require_once('gaLogs/Class.LoggerConnection.php');
	require_once('gaLogs/Class.GaLogger.php');
	
	class GaLog
	{
		static private $instance = null;
		static public function create(){
			if(is_null(self::$instance)){
				self::$instance = new GaLog();
			}
			return self::$instance;
		}

		/*** For Testability Purposes ***/
		static public function reset(){
			self::$instance = null;
		}
		
		private $mGaLogID;
		private $mClientDomainID;
		private $mDoerTypeID;
		private $mDoerID;
		private $mStartingLogTraceID;
		private $mIpAddress;
		private $mUserAgent;
		private $mTrace = null;
		private $mDb;

		private function __construct($param=null,$data=null){
			$this->mDb = LoggerConnection::create()->getDbHandler();
			$this->clearProperties();
			if(!is_null($param)){
				$sql =	'SELECT `gaLogID`,`clientDomainID`,`doerTypeID`,`doerID`,`startingLogTraceID`,`ipAddress`,`userAgent` '.
						'FROM `GaLogs`.`tblGaLog` '.
						'WHERE `gaLogID` = '.$this->mDb->makeSqlSafeString($param);
				$rs = new iRecordset($this->mDb->execQuery($sql));
				if($rs->retrieveRecordCount()){
					$this->initProperties($rs->retrieveRow(0));
				}
				else{
					throw new exception('Invalid gaLogID '.$param.' passed to object of type GaLog.');
				}
			}
			if(is_array($data)){
				$this->initProperties($data);
			}
		}

		private function clearProperties(){
			$this->setGaLogID(0);
			$this->setClientDomainID(0);
			$this->setDoerTypeID(0);
			$this->setDoerID(0);
			$this->setStartingLogTraceID(0);
			$this->setIpAddress(null);
			$this->setUserAgent(null);
		}

		private function initProperties($data){
			$this->setGaLogID($data['gaLogID']);
			$this->setClientDomainID($data['clientDomainID']);
			$this->setDoerTypeID($data['doerTypeID']);
			$this->setDoerID($data['doerID']);
			$this->setStartingLogTraceID($data['startingLogTraceID']);
			$this->setIpAddress($data['ipAddress']);
			$this->setUserAgent($data['userAgent']);
		}

		//SETTERS
		private function setGaLogID($param=0){
			$this->mGaLogID=$param;
		}

		public function setClientDomainID($param=0){
			$this->mClientDomainID=$param;
		}

		public function setDoerTypeID($param=0){
			$this->mDoerTypeID=$param;
		}

		public function setDoerID($param=0){
			$this->mDoerID=$param;
		}

		public function setStartingLogTraceID($param=0){
			$this->mStartingLogTraceID=$param;
		}

		public function setIpAddress($param=null){
			$this->mIpAddress=$param;
		}

		public function setUserAgent($param=null){
			$this->mUserAgent=$param;
		}
		
		public function setTrace($trace){
			$this->mTrace = $trace;
		}

		//GETTERS
		public function getGaLogID(){
			return $this->mGaLogID;
		}

		public function getClientDomainID(){
			return $this->mClientDomainID;
		}

		public function getDoerTypeID(){
			return $this->mDoerTypeID;
		}

		public function getDoerID(){
			return $this->mDoerID;
		}

		public function getStartingLogTraceID(){
			return $this->mStartingLogTraceID;
		}

		public function getIpAddress(){
			return $this->mIpAddress;
		}

		public function getUserAgent(){
			return $this->mUserAgent;
		}

		//CRUDE
		public function save(){
			$trace = debug_backtrace();
			if(!$this->getGaLogID()){
				$this->mTrace->save();
				$this->setStartingLogTraceID($this->mTrace->getLogTraceID());
				$sql =	'INSERT INTO `GaLogs`.`tblGaLog`('.
							'`clientDomainID`,'.
							'`doerTypeID`,'.
							'`doerID`,'.
							'`startingLogTraceID`,'.
							'`ipAddress`,'.
							'`userAgent`'.
						')VALUES('.
							$this->mDb->makeSqlSafeString($this->getClientDomainID()).','.
							$this->mDb->makeSqlSafeString($this->getDoerTypeID()).','.
							$this->mDb->makeSqlSafeString($this->getDoerID()).','.
							$this->mDb->makeSqlSafeString($this->getStartingLogTraceID()).','.
							$this->mDb->makeSqlSafeString($this->getIpAddress()).','.
							$this->mDb->makeSqlSafeString($this->getUserAgent()).
						')';
				$this->mDb->execQuery($sql);
				$this->setGaLogID($this->mDb->getLastInsertedID());
			}
			/****
			else{
				$sql =	'UPDATE `GaLogs`.`tblGaLog` SET '.
							'`clientDomainID`= '.$this->mDb->makeSqlSafeString($this->getClientDomainID()).','.
							'`doerTypeID`= '.$this->mDb->makeSqlSafeString($this->getDoerTypeID()).','.
							'`doerID`= '.$this->mDb->makeSqlSafeString($this->getDoerID()).','.
							'`startingLogTraceID`= '.$this->mDb->makeSqlSafeString($this->getStartingLogTraceID()).','.
							'`ipAddress`= '.$this->mDb->makeSqlSafeString($this->getIpAddress()).','.
							'`userAgent`= '.$this->mDb->makeSqlSafeString($this->getUserAgent()).' '.
						'WHERE `gaLogID` = '.$this->mDb->makeSqlSafeString($this->getGaLogID());
				$this->mDb->execQuery($sql);
			}
			****/
		}

		public function delete(){
			$sql =	'DELETE FROM `GaLogs`.`tblGaLog` '.
					'WHERE `gaLogID` = '.$this->mDb->makeSqlSafeString($this->getGaLogID());
			$this->mDb->execQuery($sql);
			$this->clearProperties();
		}
		
		
	}
?>