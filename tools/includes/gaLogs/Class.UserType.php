<?php
	/***
	 * @author: Reynaldo Castellano III
	 */
	require_once('gaLogs/exception/Class.GaLogException.php');
	require_once('Class.LoggerConnection.php');
	
	class UserType
	{
		private $mUserTypeID;
		private $mClientDomainID;
		private $mUserTypeName;
		private $mDb;

		public function __construct($param=null,$data=null){
			$this->mDb = LoggerConnection::create()->getDbHandler();
			$this->clearProperties();
			if(!is_null($param)){
				$sql =	'SELECT `userTypeID`,`clientDomainID`,`userTypeName` '.
						'FROM `GaLogs`.`tblUserType` '.
						'WHERE `userTypeID` = '.$this->mDb->makeSqlSafeString($param);
				$rs = new iRecordset($this->mDb->execQuery($sql));
				if($rs->retrieveRecordCount()){
					$this->initProperties($rs->retrieveRow(0));
				}
				else{
					throw new exception('Invalid userTypeID '.$param.' passed to object of type UserType.');
				}
			}
			if(is_array($data)){
				$this->initProperties($data);
			}
		}

		private function clearProperties(){
			$this->setUserTypeID(null);
			$this->setClientDomainID(null);
			$this->setUserTypeName(null);
		}

		private function initProperties($data){
			$this->setUserTypeID($data['userTypeID']);
			$this->setClientDomainID($data['clientDomainID']);
			$this->setUserTypeName($data['userTypeName']);
		}

		//SETTERS
		private function setUserTypeID($param=0){
			$this->mUserTypeID=$param;
		}

		public function setClientDomainID($param=0){
			$this->mClientDomainID=$param;
		}

		public function setUserTypeName($param=null){
			$this->mUserTypeName=$param;
		}

		//GETTERS
		public function getUserTypeID(){
			return $this->mUserTypeID;
		}

		public function getClientDomainID(){
			return $this->mClientDomainID;
		}

		public function getUserTypeName(){
			return $this->mUserTypeName;
		}

		//CRUDE
		public function save(){
			if(!$this->getUserTypeID()){
				$sql =	'INSERT INTO `GaLogs`.`tblUserType`('.
							'`clientDomainID`,'.
							'`userTypeName`'.
						')VALUES('.
							$this->mDb->makeSqlSafeString($this->getClientDomainID()).','.
							$this->mDb->makeSqlSafeString($this->getUserTypeName()).
						')';
				$this->mDb->execQuery($sql);
				$this->setUserTypeID($this->mDb->getLastInsertedID());
			}
			else{
				$sql =	'UPDATE `GaLogs`.`tblUserType` SET '.
							'`clientDomainID`= '.$this->mDb->makeSqlSafeString($this->getClientDomainID()).','.
							'`userTypeName`= '.$this->mDb->makeSqlSafeString($this->getUserTypeName()).' '.
						'WHERE `userTypeID` = '.$this->mDb->makeSqlSafeString($this->getUserTypeID());
				$this->mDb->execQuery($sql);
			}
		}

		public function delete(){
			$sql =	'DELETE FROM `GaLogs`.`tblUserType` '.
					'WHERE `userTypeID` = '.$this->mDb->makeSqlSafeString($this->getUserTypeID());
			$this->mDb->execQuery($sql);
			$this->clearProperties();
		}
		
		public static function getUserTypesByClientDomainID($clientDomainID=0){
			$db = GaLogIni::createGaLogDbHandler();
			$sql =	'SELECT `userTypeID`,`clientDomainID`,`userTypeName` '.
					'FROM `GaLogs`.`tblUserType` '.
					'WHERE `clientDomainID` = '.GaString::makeSqlSafe($clientDomainID);
			$rs = new iRecordset($db->execQuery($sql));
			$ar = array();
			foreach($rs as $row){
				$ar[] = new UserType(null,$row);
			}
			return $ar;
		}

		public static function getUserTypeByClientDomainIDAndUserTypeName($clientDomainID=0,$userTypeName=null){
			$db = LoggerConnection::create()->getDbHandler();
			$sql =	'SELECT `userTypeID`,`clientDomainID`,`userTypeName` '.
					'FROM `GaLogs`.`tblUserType` '.
					'WHERE `clientDomainID` = '.$db->makeSqlSafeString($clientDomainID).' ' .
					'AND `userTypeName` = '.$db->makeSqlSafeString($userTypeName); 
			$rs = new iRecordset($db->execQuery($sql));
			return $rs->retrieveRecordCount() > 0 
				? new UserType(null,$rs->retrieveRow(0))
				: null;
		}
		
	}	
?>