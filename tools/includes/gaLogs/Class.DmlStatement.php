<?php
	/***
	@author: Reynaldo Castellano III
	***/
	require_once('gaLogs/Class.LoggerConnection.php');
	class DmlStatement
	{
		private $mDmlStatementID;
		private $mGaLogID;
		private $mCommand;
		private $mSqlString;
		private $mStartingLogTraceID;
		private $mDb;

		public function __construct($param=null,$data=null){
			$this->mDb = LoggerConnection::create()->getDbHandler();
			$this->clearProperties();
			if(!is_null($param)){
				$sql =	'SELECT `dmlStatementID`,`gaLogID`,`command`,`sqlString`,`startingLogTraceID` '.
						'FROM `GaLogs`.`tblDmlStatement` '.
						'WHERE `dmlStatementID` = '.$this->mDb->makeSqlSafeString($param);
				$rs = new iRecordset($this->mDb->execQuery($sql));
				if($rs->retrieveRecordCount()){
					$this->initProperties($rs->retrieveRow(0));
				}
				else{
					throw new exception('Invalid dmlStatementID '.$param.' passed to object of type DmlStatement.');
				}
			}
			if(is_array($data)){
				$this->initProperties($data);
			}
		}

		private function clearProperties(){
			$this->setDmlStatementID(null);
			$this->setGaLogID(null);
			$this->setCommand(null);
			$this->setSqlString(null);
			$this->setStartingLogTraceID(0);
		}

		private function initProperties($data){
			$this->setDmlStatementID($data['dmlStatementID']);
			$this->setGaLogID($data['gaLogID']);
			$this->setCommand($data['command']);
			$this->setSqlString($data['sqlString']);
			$this->setStartingLogTraceID($data['startingLogTraceID']);
		}

		//SETTERS
		private function setDmlStatementID($param=0){
			$this->mDmlStatementID=$param;
		}

		public function setGaLogID($param=0){
			$this->mGaLogID=$param;
		}

		public function setCommand($param=null){
			$this->mCommand=$param;
		}

		public function setSqlString($param=null){
			$this->mSqlString=$param;
		}

		public function setStartingLogTraceID($param=0){
			$this->mStartingLogTraceID=$param;
		}

		//GETTERS
		public function getDmlStatementID(){
			return $this->mDmlStatementID;
		}

		public function getGaLogID(){
			return $this->mGaLogID;
		}

		public function getCommand(){
			return $this->mCommand;
		}

		public function getSqlString(){
			return $this->mSqlString;
		}

		public function getStartingLogTraceID(){
			return $this->mStartingLogTraceID;
		}

		//CRUDE
		static private $sthSave = null;
		public function save(){
			if(!$this->getDmlStatementID()){
				if(is_null(self::$sthSave)){
					$sql =	'INSERT INTO `GaLogs`.`tblDmlStatement`(`gaLogID`,`command`,`sqlString`,`startingLogTraceID`)'.
							'VALUES( ?, ?, ?, ? )';
					self::$sthSave = $this->mDb->prepareStatement($sql);
				}
				$this->mDb->executeStatement(self::$sthSave,array(
					$this->getGaLogID(),
					$this->getCommand(),
					$this->getSqlString(),
					//$this->getStartingLogTraceID()
					0
				));
				$this->setDmlStatementID($this->mDb->getLastInsertedID());
			}
		}

		public function delete(){
			$sql =	'DELETE FROM `GaLogs`.`tblDmlStatement` '.
					'WHERE `dmlStatementID` = '.$this->mDb->makeSqlSafeString($this->getDmlStatementID());
			$this->mDb->execQuery($sql);
			$this->clearProperties();
		}
	}
?>