<?php
	require_once('gaLogs/Class.LoggerConnection.php');
	require_once('gaLogs/Class.LogThread.php');
	require_once('gaLogs/Class.LogTrailTable.php');
	class LogTrailAuditor
	{
		static private $instance = null;
		static public function create(){	
			if(is_null(self::$instance)){
				self::$instance = new self;
			}
			return self::$instance;
		}
		
		private $mLoggerDb = null;
		private function __construct(){
			$this->mLoggerDb = LoggerConnection::create()->getDbHandler();
		}
		
		/***
			This functin should only be called during script shutdown(Except for testing purposes.)
		***/
		
		public function auditLogTrails(){
			$threadID = LogThread::create()->getLogThreadID();
			$logTrailTables = LogTrailTable::getLogTrailTables();
			$sqlClauses = array();
			foreach($logTrailTables as $iLogTrailTable){
				$sql = 	"SELECT `logThreadID`, `logStatementID`, `logRecordID`, `logDate`, `dbName`, `tableName`, `fieldName`, `oldValue`, `newValue`, `event` ".
						"FROM {$iLogTrailTable->getLogTrailTableName()} ".
						"WHERE `logThreadID` = '$threadID' ";
				$sqlClauses[] = $sql;
			}
			$finalSql = implode(' UNION ALL ', $sqlClauses).' ORDER BY `logStatementID`, `logRecordID`, `dbName`, `tableName`, `fieldName`';
			$rsTrails = new iRecordset($this->mLoggerDb->execute($finalSql));
			
			if(array_key_exists('logdebug',$_GET)){
				$rsTrails->dump();
			}	
			//if there is a logtrail for this thread			
			if($rsTrails->retrieveRecordCount()){
				$gaLog = GaLog::create();
				$gaLog->save();
				$gaLogID = $gaLog->getGaLogID();
				//$sqlStatements = SqlStatementBuilder::create()->getStoredSqlStatements();
				$sqlStatements = DmlStatementRegistry::getInstance()->getAllStatements();
				
				//The logTrails must be grouped by logRecordID called affectedRecords
				//This affectedRecords must be grouped by tableName
				$affectedRecords = array();
				foreach($rsTrails as $rowTrail){
					$logRecordID = $rowTrail['logRecordID'];
					if(!array_key_exists($logRecordID,$affectedRecords)){
						$affectedRecords[$rowTrail['logRecordID']] = array(
							'dbName' 	=> $rowTrail['dbName'],
							'tableName'	=> $rowTrail['tableName'],
							'logStatementID' => $rowTrail['logStatementID'], 
							'logTrails'	=> array()
						);
					}
					$affectedRecords[$logRecordID]['logTrails'][] = $rowTrail;
				}
				$dmlStatementCache = array();
				//group this logTrailMappings by dbname and tablename
				$affectedRecordsByTableName = array();
				foreach($affectedRecords as $iAffectedRecord){
					$logStatementID = $iAffectedRecord['logStatementID'];
					if(!array_key_exists($logStatementID,$dmlStatementCache)){
						//get the sqlStatement 
						$sqlStatement = $sqlStatements[$logStatementID];
						
						$dmlStatement = new DmlStatement();
						$dmlStatement->setGaLogID($gaLogID);
						$dmlStatement->setCommand($sqlStatement->getCommand());
						$dmlStatement->setSqlString($sqlStatement->getSqlStatement());
						$dmlStatement->save();
						$dmlStatementCache[$logStatementID] = $dmlStatement;
					}
					else{
						$dmlStatement = $dmlStatementCache[$logStatementID];
					}
					$logTrailTableKey = $iAffectedRecord['dbName'].'_'.$iAffectedRecord['tableName'];
					if(!array_key_exists($logTrailTableKey, $affectedRecordsByTableName)){
						$affectedRecordsByTableName[$logTrailTableKey] = array(
							'dbName'			=> $iAffectedRecord['dbName'],
							'tableName'			=> $iAffectedRecord['tableName'],
							'affectedRecords'	=> array()	
						);
					}
					$affectedRecordsByTableName[$logTrailTableKey]['affectedRecords'][] = LogTrail::createInstance($iAffectedRecord['logTrails'],$dmlStatement);
				}
				foreach($affectedRecordsByTableName as $affectedLogTable){
					//get the logTable
					$tempLogTable = LogTable::getLogTableByDbNameTableName($affectedLogTable['dbName'],$affectedLogTable['tableName']);
					//get the StorageTable
					$tempStorageTable = LogStorageTable::getInstanceByLogTable($tempLogTable);
					$tempStorageTable->addLogTrails($affectedLogTable['affectedRecords']);
				}
			}
			
			LogTrailTable::purgeAllThreadTrails();
		}
	}
?>