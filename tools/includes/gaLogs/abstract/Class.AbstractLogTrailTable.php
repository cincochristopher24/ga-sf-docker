<?php
	require_once('gaLogs/Class.LogTrail.php');
	require_once('gaLogs/Class.LoggerConnection.php');
	
	abstract class AbstractLogTrailTable 
	{		
		private $mSthLogTrails = null;
		public function getLogTrails($dmlTable){
			$db = LoggerConnection::create()->getDbHandler();
			if(is_null($this->mSthLogTrails)){
				$sql = 	'SELECT * '.
						'FROM '.$this->getLogTrailTableName().' '.
						'WHERE logThreadID = ? '.
						'AND logStatementID = ? '.
						'ORDER BY logRecordID, fieldName ';
				$this->mSthLogTrails = $db->prepareStatement($sql);
			}
			$rs = new iRecordset($db->executeStatement($this->mSthLogTrails,array(
				LogThread::create()->getLogThreadID(),
				$dmlTable->getSqlStatement()->getSqlStatementID()
			)));
			return LogTrail::createInstances($rs);
		}
		
		public function purge($from=null){
			$db = LoggerConnection::create()->getDbHandler();
			$startDate = GaDateTime::isValidDate($from) ? $from : GaDateTime::dbDateTimeFormat(GaDateTime::dateAdd(GaDateTime::dbDateTimeFormat(),'m',-1));
			$sql = 	'DELETE FROM '.$this->getLogTrailTableName().' '.
					'WHERE `logDate` <= '.$db->makeSqlSafeString($startDate);
			$db->execute($sql);
		}
		
		public function purgeThreadTrails($threadID){
			$db = LoggerConnection::create()->getDbHandler();
			$sql = 	'DELETE FROM '.$this->getLogTrailTableName().' '.
					'WHERE logThreadID = '.$db->makeSqlSafeString($threadID);
			$db->execute($sql);
		}
		
		abstract public function getStorageEngineType();
		abstract public function getLogTrailTableName();
	}
?>