<?php
	/***
	 * @author: Reynaldo Castellano III
	 */
	
	require_once('gaLogs/Class.DataTracker.php');
	require_once('gaLogs/interface/Interface.ISqlStatement.php');
	abstract class AbstractSqlStatementStrategy implements ISqlStatement
	{
		static protected $sqlStringStack	= array();
		
		protected $mSqlString  			= null;
		protected $mIsSyntaxOk 			= null;
		protected $mAffectedTableNames 	= array();
		
		public function isSyntaxOk(){
			if(is_null($this->mIsSyntaxOk)){
				if(!in_array($this->mSqlString, self::$sqlStringStack)){
					$this->mIsSyntaxOk = DataTracker::create()->getActiveClientConnection()->isSqlSyntaxOk($this->mSqlString);
				}
				else{
					$this->mIsSyntaxOk = true;
				}
			}
			return $this->mIsSyntaxOk;
		}
		
		public function trackChanges($res){}
	}
?>