<?php
	/***
	@author: Reynaldo Castellano III
	***/
	require_once('gaLogs/Class.LoggerConnection.php');
	class AffectedRecord
	{
		const INSERT_EVENT = 1;
		const UPDATE_EVENT = 2;
		const DELETE_EVENT = 3;
		
		static private $EVENTS = array(
			'INSERT' => self::INSERT_EVENT,
			'UPDATE' => self::UPDATE_EVENT,
			'DELETE' => self::DELETE_EVENT
		);
		
		static private $sthSave = null;
		
		private $mAffectedRecordID;
		private $mDmlStatementID;
		private $mEvent;
		private $mDb;

		public function __construct($param=null,$data=null){
			$this->mDb = LoggerConnection::create()->getDbHandler();
			$this->clearProperties();
			if(!is_null($param)){
				$sql =	'SELECT `affectedRecordID`,`dmlStatementID`,`event` '.
						'FROM `GaLogs`.`tblAffectedRecord` '.
						'WHERE `affectedRecordID` = '.$this->mDb->makeSqlSafeString($param);
				$rs = new iRecordset($this->mDb->execQuery($sql));
				if($rs->retrieveRecordCount()){
					$this->initProperties($rs->retrieveRow(0));
				}
				else{
					throw new exception('Invalid affectedRecordID '.$param.' passed to object of type AffectedRecord.');
				}
			}
			if(is_array($data)){
				$this->initProperties($data);
			}
		}

		private function clearProperties(){
			$this->setAffectedRecordID(null);
			$this->setDmlStatementID(null);
			$this->setEvent(null);
		}

		private function initProperties($data){
			$this->setAffectedRecordID($data['affectedRecordID']);
			$this->setDmlStatementID($data['dmlStatementID']);
			$this->setEvent($data['event']);
		}

		//SETTERS
		private function setAffectedRecordID($param=0){
			$this->mAffectedRecordID=$param;
		}

		public function setDmlStatementID($param=0){
			$this->mDmlStatementID=$param;
		}

		public function setEvent($param=null){
			if(!is_null($param)){
				$this->mEvent= self::$EVENTS[$param];
			}
		}

		//GETTERS
		public function getAffectedRecordID(){
			return $this->mAffectedRecordID;
		}

		public function getDmlStatementID(){
			return $this->mDmlStatementID;
		}

		public function getEvent(){
			return $this->mEvent;
		}

		//CRUDE
		public function save(){
			if(!$this->getAffectedRecordID()){
				if(is_null(self::$sthSave)){
					$sql =	'INSERT INTO `GaLogs`.`tblAffectedRecord`('.
								'`dmlStatementID`,'.
								'`event`'.
							')VALUES( ? , ? )';
					self::$sthSave = $this->mDb->prepareStatement($sql);
				}
				$this->mDb->executeStatement(self::$sthSave,array($this->getDmlStatementID(),$this->getEvent()));
				$this->setAffectedRecordID($this->mDb->getLastInsertedID());
			}
			else{
				$sql =	'UPDATE `GaLogs`.`tblAffectedRecord` SET '.
							'`dmlStatementID`= '.$this->mDb->makeSqlSafeString($this->getDmlStatementID()).','.
							'`event`= '.$this->mDb->makeSqlSafeString($this->getEvent()).' '.
						'WHERE `affectedRecordID` = '.$this->mDb->makeSqlSafeString($this->getAffectedRecordID());
				$this->mDb->execQuery($sql);
			}
		}

		public function delete(){
			$sql =	'DELETE FROM `GaLogs`.`tblAffectedRecord` '.
					'WHERE `affectedRecordID` = '.$this->mDb->makeSqlSafeString($this->getAffectedRecordID());
			$this->mDb->execQuery($sql);
			$this->clearProperties();
		}
	}
?>