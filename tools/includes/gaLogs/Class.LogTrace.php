<?php
	/***
	 * @author: Reynaldo Castellano III
	 */
	require_once('Class.GaDateTime.php');
	require_once('gaLogs/Class.LoggerConnection.php');
	require_once('gaLogs/exception/Class.GaLogException.php');
	
	class LogTrace
	{
		private $mLogTraceID;
		private $mFileName;
		private $mLineNumber;
		private $mExecutionDate;
		private $mDb;

		public function __construct($param=null,$data=null){
			$this->mDb = LoggerConnection::create()->getDbHandler();
			$this->clearProperties();
			if(!is_null($param)){
				$sql =	'SELECT `logTraceID`,`fileName`,`lineNumber`,`executionDate` '.
						'FROM `GaLogs`.`tblLogTrace` '.
						'WHERE `logTraceID` = '.$this->mDb->makeSqlSafeString($param);
				$rs = new iRecordset($this->mDb->execQuery($sql));
				if($rs->retrieveRecordCount()){
					$this->initProperties($rs->retrieveRow(0));
				}
				else{
					throw new exception('Invalid logTraceID '.$param.' passed to object of type LogTrace.');
				}
			}
			if(is_array($data)){
				$this->initProperties($data);
			}
		}

		private function clearProperties(){
			$this->setLogTraceID(null);
			$this->setFileName(null);
			$this->setLineNumber(null);
			$this->setExecutionDate(null);
		}

		private function initProperties($data){
			$this->setLogTraceID($data['logTraceID']);
			$this->setFileName($data['fileName']);
			$this->setLineNumber($data['lineNumber']);
			$this->setExecutionDate($data['executionDate']);
		}

		//SETTERS
		private function setLogTraceID($param=0){
			$this->mLogTraceID=$param;
		}

		public function setFileName($param=null){
			$this->mFileName=$param;
		}

		public function setLineNumber($param=0){
			$this->mLineNumber=$param;
		}

		public function setExecutionDate($param=null){
			$this->mExecutionDate=$param;
		}

		//GETTERS
		public function getLogTraceID(){
			return $this->mLogTraceID;
		}

		public function getFileName(){
			return $this->mFileName;
		}

		public function getLineNumber(){
			return $this->mLineNumber;
		}

		public function getExecutionDate(){
			return $this->mExecutionDate;
		}

		public function initializeTrace($trace=null){
			if(is_array($trace)){
				$this->setFileName( (array_key_exists('file',$trace) ? $trace['file'] : null ) );
				$this->setLineNumber( (array_key_exists('line',$trace) ? $trace['line'] : null ) );
				$this->setExecutionDate( GaDateTime::dbDateTimeFormat() );
			}
			return $this;
		}

		//CRUDE
		static private $sthSave = null;
		public function save(){
			if(!$this->getLogTraceID()){
				if(is_null(self::$sthSave)){
					$sql =	'INSERT INTO `GaLogs`.`tblLogTrace`(`fileName`,`lineNumber`,`executionDate`) '.
							'VALUES( ?, ?, ? )';
					self::$sthSave = $this->mDb->prepareStatement($sql);
				}
				//$this->mDb->execQuery($sql);
				$this->mDb->executeStatement(self::$sthSave,array(
					$this->getFileName(),
					$this->getLineNumber(),
					$this->getExecutionDate()
				));
				$this->setLogTraceID($this->mDb->getLastInsertedID());
			}
			/***
			else{
				$sql =	'UPDATE `GaLogs`.`tblLogTrace` SET '.
							'`fileName`= '.$this->mDb->makeSqlSafeString($this->getFileName()).','.
							'`lineNumber`= '.$this->mDb->makeSqlSafeString($this->getLineNumber()).','.
							'`executionDate`= '.$this->mDb->makeSqlSafeString($this->getExecutionDate()).' '.
						'WHERE `logTraceID` = '.$this->mDb->makeSqlSafeString($this->getLogTraceID());
				$this->mDb->execQuery($sql);
			}
			***/
		}

		public function delete(){
			$sql =	'DELETE FROM `GaLogs`.`tblLogTrace` '.
					'WHERE `logTraceID` = '.$this->mDb->makeSqlSafeString($this->getLogTraceID());
			$this->mDb->execQuery($sql);
			$this->clearProperties();
		}
	}
?>