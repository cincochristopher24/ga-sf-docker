<?php
	/***
	 * @author: Reynaldo Castellano III
	 */
	/**
	 * Trigger naming convention
	 * 	- gaLogTrigger_GaLogs_tblClientDomain_ai
	 */
	
	require_once('gaLogs/Class.LoggerConnection.php');
	class Trigger
	{
		const AFTER_INSERT_TIMING = 0;
		const AFTER_UPDATE_TIMING = 1;
		const AFTER_DELETE_TIMING = 2;
		
		private static $TIMING_ABBR = array(
			self::AFTER_INSERT_TIMING => 'ai',
			self::AFTER_UPDATE_TIMING => 'au',
			self::AFTER_DELETE_TIMING => 'ad'
		);
		
		private static $TIMING_SQL = array(
			self::AFTER_INSERT_TIMING => 'AFTER INSERT',
			self::AFTER_UPDATE_TIMING => 'AFTER UPDATE',
			self::AFTER_DELETE_TIMING => 'AFTER DELETE'
		);

		public static function getInstancesByLogSubjectTable($logSubjectTable){
			$db 		= LoggerConnection::create()->getDbHandler();
			$dbName 	= $logSubjectTable->getDbName();
			$tableName	= $logSubjectTable->getTableName();
			
			//return all the triggers associated with the table regardless of the trigger name and seggregate the triggers based on the action timing
			//This is a patch for the bug that occurs if a table has been ranamed. Since associated triggers are not renamed so by name it doesnt belong to the renamed table
			//but physically it is still part of the table but this time with a different name. So trying to create a trigger will result to an error since triggers are still 
			//present.
			
			$stmt =	'SELECT TRIGGER_NAME, EVENT_OBJECT_SCHEMA, EVENT_OBJECT_TABLE, EVENT_MANIPULATION, ACTION_TIMING, ACTION_STATEMENT ' .
					'FROM INFORMATION_SCHEMA.TRIGGERS ' .
					'WHERE EVENT_OBJECT_SCHEMA = '.$db->makeSqlSafeString($dbName).' '.
					'AND EVENT_OBJECT_TABLE = '.$db->makeSqlSafeString($tableName).' '.
					'AND ACTION_TIMING = \'AFTER\' ';
					//'AND TRIGGER_NAME LIKE '.$db->makeSqlSafeString('gaLogTrigger_'.$dbName.'_'.$tableName.'%');
			$rs = new iRecordset($db->execute($stmt));
			$ar = array();
			if($rs->retrieveRecordCount() > 0){
				for($rs->moveFirst();!$rs->EOF();$rs->moveNext()){
					$ar[] = new Trigger($rs->retrieveRow());
				}
			}
			return $ar;
		}
		
		public static function refreshTriggers($logSubjectTable){
			//get first the associated triggers of the current table
			$currentTriggers = self::getInstancesByLogSubjectTable($logSubjectTable);
			$triggersByTiming = array(
				'AFTER_INSERT' 	=> null,
				'AFTER_UPDATE'	=> null,
				'AFTER_DELETE'	=> null
			);
			foreach($currentTriggers as $iTrigger){
				$triggersByTiming[$iTrigger->getActionTiming().'_'.$iTrigger->getEventManipulation()] = $iTrigger;
			}
			self::createTableTrigger($logSubjectTable,self::AFTER_INSERT_TIMING,$triggersByTiming['AFTER_INSERT']);
			self::createTableTrigger($logSubjectTable,self::AFTER_UPDATE_TIMING,$triggersByTiming['AFTER_UPDATE']);
			self::createTableTrigger($logSubjectTable,self::AFTER_DELETE_TIMING,$triggersByTiming['AFTER_DELETE']);
		}
		
		private static function createTableTrigger($logSubjectTable,$timingConst,$currentTrigger=null){
			$fldNames 	= $logSubjectTable->getFieldNames();
			$dbName 	= $logSubjectTable->getDbName();
			$tableName	= $logSubjectTable->getTableName();
			
			$dbHandler = LoggerConnection::create()->getDbHandler();
			
			$event 		= '\''.str_replace('AFTER ','',self::$TIMING_SQL[$timingConst]).'\'';
			//$triggerName= $dbName.'.gaLogTrigger_'.$dbName.'_'.$tableName.'_'self::$TIMING_ABBR[$timingConst];
			$triggerName = $dbName.'.gaLogTRG_'.md5($dbName.'_'.$tableName).'_'.self::$TIMING_ABBR[$timingConst];
			
			/***
			$triggerSql =   'CREATE TRIGGER '.$triggerName.' '.self::$TIMING_SQL[$timingConst].' ' .
							'ON '.$dbName.'.'.$tableName.' '.
							'FOR EACH ROW BEGIN ' .
								'IF @isLoggingActive = "true"  THEN ' .
									'SELECT IF(@autoRecordNum,@autoRecordNum+1,1) INTO @autoRecordNum; '.
									'SET @gaLogAutoRecordID = CONCAT(CONNECTION_ID(),@autoRecordNum); '.
									'SET @gaLogAffectedRows = @gaLogAffectedRows + 1; ';
			foreach($fldNames as $iFldName){
				$oldRow = (self::AFTER_INSERT_TIMING == $timingConst ? 'NULL' : 'OLD.'.$iFldName);
			 	$newRow = (self::AFTER_DELETE_TIMING == $timingConst ? 'NULL' : 'NEW.'.$iFldName);
				$triggerSql.=		'SELECT COLUMN_NAME INTO @galogTmpClName ' .
									'FROM INFORMATION_SCHEMA.COLUMNS ' .
									'WHERE TABLE_SCHEMA = ' .$dbHandler->makeSqlSafeString($dbName).' '.
										'AND TABLE_NAME = ' .$dbHandler->makeSqlSafeString($tableName).' '.
										'AND COLUMN_NAME =' .$dbHandler->makeSqlSafeString($iFldName).';'.
									'IF @galogTmpClName = '.$dbHandler->makeSqlSafeString($iFldName).' THEN '.
										'INSERT INTO '.LogTrailTable::create($logSubjectTable->getStorageEngine())->getLogTrailTableName().' SET '.
									   		'logThreadID 	= @gaLogThreadID, ' .
									   		'logStatementID	= @gaLogDmlStatementID, '.
											'logRecordID	= @gaLogAutoRecordID, '.
											'dbName 		= '.$dbHandler->makeSqlSafeString($dbName).', '.
									    	'tableName 		= '.$dbHandler->makeSqlSafeString($tableName).', '.
									    	'fieldName 		= '.$dbHandler->makeSqlSafeString($iFldName).', '.
									    	'newValue 		= '.$newRow.', '.
									    	'oldValue  		= '.$oldRow.', '.
									    	'event			= '.$event.', '.
								    		'logDate		= NOW(); '.
									'END IF; ';
			}
			$triggerSql .= 		'END IF; ' .
							'END ';
			***/
			
			$triggerFields = array();
			$dbNameStr = $dbHandler->makeSqlSafeString($dbName);
			$tableNameStr = $dbHandler->makeSqlSafeString($tableName);
			foreach($fldNames as $iFldName){
				$fieldNameStr = $dbHandler->makeSqlSafeString($iFldName);
				$oldRow = (self::AFTER_INSERT_TIMING == $timingConst ? 'NULL' : 'OLD.'.$iFldName);
			 	$newRow = (self::AFTER_DELETE_TIMING == $timingConst ? 'NULL' : 'NEW.'.$iFldName);
				$triggerFields[] =	"(@gaLogThreadID,@gaLogDmlStatementID,@gaLogAutoRecordID,$dbNameStr,$tableNameStr,$fieldNameStr,$newRow,$oldRow,$event,NOW())";
			}
			
			$triggerSql =   'CREATE TRIGGER '.$triggerName.' '.self::$TIMING_SQL[$timingConst].' ' .
							'ON '.$dbName.'.'.$tableName.' '.
							'FOR EACH ROW BEGIN ' .
								'IF @isLoggingActive = "true"  THEN ' .
									'SELECT IF(@autoRecordNum,@autoRecordNum+1,1) INTO @autoRecordNum; '.
									'SET @gaLogAffectedRows = @gaLogAffectedRows + 1; '.
									'SET @gaLogAutoRecordID = CONCAT(@gaLogThreadID,"_",CONNECTION_ID(),"_",@autoRecordNum); '.
									'INSERT INTO '.LogTrailTable::create($logSubjectTable->getStorageEngine())->getLogTrailTableName().'('.
										'`logThreadID`,`logStatementID`,`logRecordID`,`dbName`,`tableName`,`fieldName`,`newValue`,`oldValue`,`event`,`logDate`'.
									') VALUES '.
									implode($triggerFields,', ').'; ';
			$triggerSql .= 		'END IF; ' .
							'END ';
			//always try to drop the trigger when creating a new one...CRITICAL!
			try{
				//drop the currently associated trigger with the same action timing
				if(!is_null($currentTrigger)){
					$dbHandler->execute('DROP TRIGGER '.$currentTrigger->getDbName().'.'.$currentTrigger->getTriggerName());
				}
				//drop the trigger that may have the same name as the to be created trigger
				$dbHandler->execute('DROP TRIGGER '.$triggerName);
			}
			catch(exception $e){
				if(array_key_exists('gaLoggerDebug',$_GET)){
					throw $e;
				}
			}
			$dbHandler->execute($triggerSql);
		}
	
		/** Actual Class Definition **/
		private $mTriggerName 			= null;
		private $mDbName 				= null;
		private $mTableName 			= null;
		private $mEventManipulation 	= null;
		private $mActionTiming			= null;
		private $mActionStatement		= null;
		
		private function __construct($triggerData){
			$this->mTriggerName 		= $triggerData['TRIGGER_NAME'];
			$this->mDbName 				= $triggerData['EVENT_OBJECT_SCHEMA'];
			$this->mTableName 			= $triggerData['EVENT_OBJECT_TABLE'];
			$this->mEventManipulation 	= $triggerData['EVENT_MANIPULATION'];
			$this->mActionTiming		= $triggerData['ACTION_TIMING'];
			$this->mActionStatement		= $triggerData['ACTION_STATEMENT'];
		}
		
		public function getTriggerName(){
			return $this->mTriggerName;
		}
		
		public function getDbName(){
			return $this->mDbName;
		}
		
		public function getTableName(){
			return $this->mTableName;
		}
		
		public function getEventManipulation(){
			return $this->mEventManipulation;
		}
		
		public function getActionTiming(){
			return $this->mActionTiming;
		}
		
		public function getActionStatement(){
			return $this->mActionStatement;
		}
	}
?>