<?php
	require_once('Class.dbHandler.php');
	class AdminLog
	{
		private $mDb 			= null;
		private $mLogID 		= 0;
		private $mLogSection  	= null;
		private $mGenID			= 0;
		private $mUserID		= 0;
		private $mUserAgent		= null;
		private $mLogDate		= null;
		private $mAction		= null;
		private $mNotesTemp		= null;
		
		private function __construct($logID=null){
			$this->mDb = new dbHandler('db=');
			$sql  = 'SELECT * FROM GoAbroad_Main.tbadminlogs2005 WHERE logID = '.GaString::makeSqlSafe($logID);
			$rs = new iRecordset($this->mDb->execute($sql));
			if($rs->retrieveRecordCount() > 0){
				$this->setLogID($rs->getLogID(0));
				$this->setLogSection($rs->getLogSection(0));
				$this->setGenID($rs->getGenID(0));
				$this->setUserID($rs->getUserID(0));
				$this->setUserAgent($rs->getUserAgent(0));
				$this->setLogDate($rs->getLogDate(0));
				$this->setAction($rs->getAction(0));
				$this->setNotesTemp($rs->getNotesTemp(0));
			}
			else{
				throw new exception('Invalid logID '.$logID.' passed to constructor method of object of type AdminLog.');
			}
		}
		
		private function setLogID($param=0){
			$this->mLogID = $param;
		}

		public function setLogSection($param=null){
			$this->mLogSection = $param;
		}

		public function setGenID($param=null){
			$this->mGenID = $param;
		}
		
		public function setUserID($param=0){
			$this->mUserID = $param;
		}

		public function setUserAgent($param=null){
			$this->mUserAgent = $param;
		}
		
		public function setLogDate($param=null){
			$this->mLogDate = $param;
		}
		
		public function setAction($param=null){
			$this->mAction = $param;
		}
		
		public function setNotesTemp($param=null){
			$this->mNotesTemp = $param;
		}
		
		
		public function getLogID(){
			return $this->mLogID;
		}
		
		public function getLogSection(){
			return $this->mLogSection;
		}
		
		public function getGenID(){
			return $this->mGenID;
		}
		
		public function getUserID(){
			return $this->mUserID;
		}
		
		public function getUserAgent(){
			return $this->mUserAgent;
		}
		
		public function getLogDate(){
			return $this->mLogDate;
		}
		
		public function getAction(){
			return $this->mAction;
		}
		
		public function getNotesTemp(){
			return $this->mNotesTemp;
		}
		
		public static function getAdminLogByLogID($logID=0){
			try{
				$adminLog = new AdminLog($logID);
				return $adminLog;
			}
			catch(exception $e){
				return null;
			}
		}
		
		public static function getPreviousAddEditAdminLog($adminLog=null){
			$db  = new dbHandler('db=GoAbroad_Main');
			$sql = 	'SELECT * FROM tbadminlogs2005 '.
					'WHERE logsection = '.GaString::makeSqlSafe($adminLog->getLogSection()).' '.
					'AND (action = "ADD" OR action = "EDIT") '.
					'AND logID < '.GaString::makeSqlSafe($adminLog->getLogID()).' '.
					"ORDER BY logid DESC ".
					"LIMIT 1";
			$rs  = new iRecordset($db->execute($sql)); 
			if($rs->retrieveRecordCount() > 0){
				return new AdminLog($rs->getLogID(0));
			}
			return null;
		}
	}
?>