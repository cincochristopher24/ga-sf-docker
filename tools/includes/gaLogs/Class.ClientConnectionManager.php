<?php
	/***
	 * @author: Reynaldo Castellano III
	 */
	require_once('gaLogs/Class.ClientConnection.php');
	require_once('gaLogs/Class.ConnectionAdapterManager.php');
	
	Class ClientConnectionManager
	{
		static private $instance = null;
		static public function create(){
			if(is_null(self::$instance)){
				self::$instance = new self();
			}
			return self::$instance;
		}
		
		/*** For Testability Purposes ***/
		static public function reset(){
			self::$instance = null;
			ConnectionAdapterManager::reset();
		}
		
		private $mConnectionStack = array();
		private $mDriverManager = null;
		
		private function __construct(){
			$this->mAdapterManager = ConnectionAdapterManager::getInstance();
		}
		
		public function getClientConnection($con){
			$conAdapter = $this->mAdapterManager->getAdapter($con);
			$clientCon = $conAdapter->getConnection($con);
			if(!array_key_exists($clientCon->getConnectionID(), $this->mConnectionStack)){
				$this->mConnectionStack[$clientCon->getConnectionID()] = $clientCon;
			}
			return $clientCon;
		}
		
		public function getAllClientConnections(){
			return $this->mConnectionStack;
		}
	}
?>