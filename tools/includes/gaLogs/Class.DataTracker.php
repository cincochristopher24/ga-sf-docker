<?php
	/***
	 * @author: Reynaldo Castellano III
	 */
	require_once('gaLogs/Class.GaLogger.php');
	require_once('gaLogs/Class.ClientConnectionManager.php');
	require_once('gaLogs/Class.LogTrailTable.php');
	
	final class DataTracker
	{
		/*** Singleton Pattern ***/
		static private $tracker = null;
		static public function create(){
			if(is_null(self::$tracker)){ 
				self::$tracker = new DataTracker(); 
			}
			return self::$tracker;
		}
		
		/*** For Testability Purposes ***/
		static public function reset(){
			self::$tracker = null;
		}
		
		private $mGaLogger 				= null;
		private $mConnectionManager		= null;
		private $mActiveConnection		= null;
		private $mNoTrackingSection		= array();
		private $mDbLoggingIsActive		= false;
		
		private function __construct(){
			$this->mGaLogger			= GaLogger::create();
			$this->mConnectionManager 	= ClientConnectionManager::create();
		}
		
		public function analyzeQuery($sql,$cnn){
			if($this->isInTrackingMode()){
				$this->beginNoTrackingSection();
				$this->mActiveConnection = $this->mConnectionManager->getClientConnection($cnn);
				$this->mActiveConnection->analyzeQuery($sql);
				$this->endNoTrackingSection();
			}
		}
		
		public function trackChanges($res){
			if($this->isInTrackingMode()){
				$this->mActiveConnection->trackChanges($res);
			}
		}
		
		
		public function initPreparedStatement($sql, $objStmt, $cnn){
			if($this->isInTrackingMode()){
				$this->beginNoTrackingSection();
				$this->mActiveConnection = $this->mConnectionManager->getClientConnection($cnn);
				$this->mActiveConnection->initPreparedStatement($sql, $objStmt);
				$this->endNoTrackingSection();
			}
		}
		
		public function analyzePreparedStatement($objStmt, $cnn, $boundVars){
			if($this->isInTrackingMode()){
				$this->beginNoTrackingSection();
				$this->mActiveConnection = $this->mConnectionManager->getClientConnection($cnn);
				$this->mActiveConnection->analyzePreparedStatement($objStmt, $boundVars);
				$this->endNoTrackingSection();
			}
		}
		
		public function trackPreparedStatement($res){
			if($this->isInTrackingMode()){
				$this->mActiveConnection->trackPreparedStatement($res);
			}
		}
		
		public function isInTrackingMode(){
			return count($this->mNoTrackingSection) == 0 && $this->mGaLogger->isOkToTrack();
			//return count($this->mNoTrackingSection) == 0;
		}
		
		public function beginNoTrackingSection(){
			$trace = debug_backtrace();
			$this->mNoTrackingSection[] = $trace[0]['file'].'::'.$trace[0]['line'];
		}
		
		public function endNoTrackingSection(){
			array_pop($this->mNoTrackingSection);
		}
		
		public function getAffectedRows($cnn){
			return $this->mConnectionManager->getClientConnection($cnn)->getAffectedRows();
		}
		
		public function getActiveClientConnection(){
			return $this->mActiveConnection;
		}
		
	}
?>