<?php
	require_once('Class.ToolMan.php');
	require_once('gaLogs/Class.LogDb.php');
	require_once('gaLogs/Class.DBSchema.php');
	
	class SelectModifier{
		
		private static $msKeywords 		= array('_DOER_ID_', '_EXECUTION_DATE_', '_EVENT_', '_SQL_STRING_', 
												'_COMMAND_', '_LOG_ID_', '_DOER_TYPE_', '_IP_ADDRESS_', '_DOMAIN_NAME_');
		private $mModifiedSql 			= '';
		private $mConditions			= array();
		private $mSubConditions			= array();
		private $mReplacedSubConditions = array();
		private $mReplacedTables 		= array();
		private $mTablesWithAliases		= array();
		
		private $mDebugTrace			= null;
		
		public function modify($userQuery){
			//GaLogIni::isReading();
			$this->mModifiedSql = '';
			$this->mConditions = array();
			$this->mReplacedTables = array();
			$this->mSubConditions = array();
			$this->mReplacedSubConditions = array();
			
			$trace = debug_backtrace();
			$this->mDebugTrace = $trace[0];
			for($traceCtr = 0; $traceCtr < count($trace); ++$traceCtr){
				if(array_key_exists('function', $trace[$traceCtr]) && $trace[$traceCtr]['function'] == 'getLogsBySql')
					$this->mDebugTrace = $trace[$traceCtr];
			}
			
			$this->parseSelectQuery($userQuery);
			(preg_match('/^\(/', $userQuery))? $patternSql = preg_replace(array('/^\(/', '/\)$/'), '', $userQuery) : $patternSql = $userQuery;
			$this->mModifiedSql = stripslashes(preg_replace(self::makeSafeRegExPattern($patternSql), $this->mModifiedSql, $userQuery));
			if(count($this->mTablesWithAliases) > 0)
				$this->renameAliasesToReplacedTable();
		}
		
		public function getModifiedSql(){
			return $this->mModifiedSql;
		}
		
		public function getReplacedTables(){
			return $this->mReplacedTables;
		}
		
		private function parseSelectQuery($selectQuery){
			$this->mModifiedSql = $selectQuery;
			
			// strip parenthesis enclosing the select query
			if (preg_match('/^\(/', $selectQuery))
				$this->mModifiedSql = preg_replace(array('/^\(/', '/\)$/'), '', $selectQuery);
			$partitions = preg_split('/\s(FROM)\s/i', $this->mModifiedSql, -1, PREG_SPLIT_NO_EMPTY);
			if (count($partitions) > 1 ) // if with from, populate table references first in order to check fields
				$this->parseTableReferences($partitions[1]);
			
			// parse fields in select_expr
			$fieldLists = preg_split('/[,\(\)]/', preg_replace('/^(SELECT)\s/i', '', $partitions[0]), -1, PREG_SPLIT_NO_EMPTY);
			for ($fieldListIndex = 0; $fieldListIndex < count($fieldLists); ++$fieldListIndex){
				$fieldItems = preg_split('/\s/', $fieldLists[$fieldListIndex], -1, PREG_SPLIT_NO_EMPTY);
				for ($itemCtr = 0; $itemCtr < count($fieldItems); ++$itemCtr)
					$this->mModifiedSql = preg_replace(self::makeSafeRegExPattern(trim($fieldItems[$itemCtr])), $this->replaceField(trim($fieldItems[$itemCtr])), $this->mModifiedSql);
			}
			
			// parse conditions 
			$this->parseConditions();
		}
		
		private function parseTableReferences($sql){
			$originalSql = $sql;
			$patternSql = $originalSql;
			$conditions = preg_split('/(\s((WHERE)|(GROUP BY)|(ORDER BY)|(HAVING))\s*)/i', $sql, -1, PREG_SPLIT_NO_EMPTY);
			$nonCondition = preg_split('/(\s((((((INNER)|(CROSS)|((NATURAL)\s+)?(((LEFT)|(RIGHT))(\s+(OUTER))?))\s+))?(JOIN))|(STRAIGHT_JOIN))\s*)|(,)/i', $conditions[0], -1, PREG_SPLIT_NO_EMPTY);
			array_shift($conditions); // remove nonCondition part
			for ($ctr = 0; $ctr < count($nonCondition); ++$ctr){
				$tempNonCondition = $nonCondition[$ctr];
				$tableReferences = preg_split('/(\s((ON)|(USING))\s)/i', $nonCondition[$ctr], -1, PREG_SPLIT_NO_EMPTY);
				$withAlias = preg_split('/(\s(AS)\s)|\s/i', $tableReferences[0], -1, PREG_SPLIT_NO_EMPTY);
				$replacedTable = $withAlias[0];
				
				//if (0 == preg_match('/^(__#SUB_SELECT(\d)+#__)$/', $withAlias[0])){ // if not a subquery, replace table
					$replacedTable = $this->replaceTable($withAlias[0]);
					if (!in_array($replacedTable, $this->mReplacedTables)){
						(1 == count($withAlias))?$this->mReplacedTables[$withAlias[0]] = $replacedTable : $this->mReplacedTables[$withAlias[0]] = $withAlias[1];
						(isset($withAlias[1])) ? $this->mTablesWithAliases[$withAlias[0]] = $replacedTable: $this->mTablesWithAliases; 
					}
				//}
				//elseif(preg_match('/^(__#SUB_SELECT(\d)+#__)$/'))
				
				// join conditions
				if (1 < count($tableReferences)){
					for ($tblRefIndex = 1; $tblRefIndex < count($tableReferences); ++$tblRefIndex){
						$tempNonCondition = preg_replace(self::makeSafeRegExPattern(trim($tableReferences[$tblRefIndex])), '__#CONDITION' . count($this->mConditions) . '#__', $tempNonCondition);
						$originalSql = preg_replace(self::makeSafeRegExPattern($nonCondition[$ctr]), $tempNonCondition, $originalSql);
						$this->mConditions['__#CONDITION' . count($this->mConditions) . '#__'] = trim($tableReferences[$tblRefIndex]);
					}
				}
				$tempNonCondition = preg_replace(self::makeSafeRegExPattern($withAlias[0]), $replacedTable, $tempNonCondition);
				$sql = preg_replace(self::makeSafeRegExPattern($nonCondition[$ctr]), $tempNonCondition, $sql);
				
			}
			$this->mModifiedSql = preg_replace(self::makeSafeRegExPattern($patternSql), $sql, $this->mModifiedSql);			
			// where conditions
			foreach ($conditions as $condition){
				$this->mModifiedSql = preg_replace(self::makeSafeRegExPattern($condition), '__#CONDITION' . count($this->mConditions) . '#__', $this->mModifiedSql);
				$this->mConditions['__#CONDITION' . count($this->mConditions) . '#__'] = $condition;			
			}
		}
				
		private function parseConditions(){
			
			foreach($this->mConditions as $conditionKey => $condition)
				$this->mConditions[$conditionKey] = $this->parseSubConditions("($condition)", $conditionKey);
			// replace markers with replaced conditions
			$this->mModifiedSql= self::reconstruct($this->mModifiedSql, '/(__#CONDITION(\d)+#__)/', $this->mConditions);
		}
		
		private function parseSubConditions($condition, $key){
			$splitCon = str_split($condition);
			$parenthesis = preg_grep('/[\(\)]/', $splitCon);
			$previous = null;
			foreach($parenthesis as $key => $value){
				if (')' == $value){
					$subCon = implode(array_slice($splitCon, $previous[count($previous) - 1], ($key - $previous[count($previous) - 1]) + 1));
					$replaced = '';
					if (!in_array($subCon, $this->mSubConditions) AND 0==preg_match('/(__#\.+#__)/', $subCon)){
						foreach ($this->mSubConditions as $key => $value)
							$subCon= preg_replace(self::makeSafeRegExPattern($value), $key, $subCon);
						$this->replaceSubCondition($subCon);
						$condition = preg_replace(self::makeSafeRegExPattern($subCon), '__#SUB_CON' . count($this->mSubConditions) . '#__', $condition);
						$this->mSubConditions['__#SUB_CON' . count($this->mSubConditions) . '#__'] = $subCon;
					}
					array_pop($previous);
				}
				else{
					$previous[] = $key;
				}
			}			
			return preg_replace(array('/^\(/', '/\)$/') , '' , self::reconstruct($condition, '/(__#SUB_CON(\d)+#__)/', $this->mReplacedSubConditions));
		}
		
		private function replaceSubCondition($condition){
			$origCon = $condition;
			$condition = preg_replace('/[\(\)]/','', $condition);
			$operands = preg_split('/\s+((AND)|(OR)|(\|\|)|(&&))\s+/', $condition, -1, PREG_SPLIT_NO_EMPTY);
			foreach ($operands as $operand){
				if(0 == preg_match('/__#SUB_CON(\d)+#__/', $operand)){
					$attributes = preg_split('/\s*[=<>(!=)]\s*/', $operand, -1, PREG_SPLIT_NO_EMPTY);
					$keywords = array_intersect($attributes, self::$msKeywords);
					if (count($attributes) > 1){ // condition
						if (count($keywords) > 0) {// with plain KeyWord
							$temp = array();
							foreach($this->mReplacedTables as $replaced){
								$s = $operand;
								foreach($keywords as $keyword)
									$s = preg_replace(self::makeSafeRegExPattern($keyword), "$replaced.$keyword", $s);
								$temp[] = $s;
							}
							$replacedCondition = '(' . implode(' OR ', $temp) . ')';
						}
						else{ // regular conditions
							$replacedCondition = $operand;
							foreach($attributes as $attribute){
								if(0 == preg_match('/(__#SUB_CON(\d)+#__)/', $attribute)){
									$replacedCondition = preg_replace(self::makeSafeRegExPattern($attribute),$this->replaceField($attribute), $replacedCondition);
								}
							}
						}
					}
					else { // function calls
						$replacedCondition = $operand;
						$fields = preg_split('/\s*([\+\-\*\/])\s*/', $attributes[0], -1, PREG_SPLIT_NO_EMPTY);
						foreach($fields as $field)
							if (0 == preg_match('/__#SUB_CON(\d)+#__/', $field))
								$replacedCondition = preg_replace(self::makeSafeRegExPattern($field),$this->replaceField($field), $replacedCondition);
					}
					$origCon = preg_replace(self::makeSafeRegExPattern($operand), $replacedCondition, $origCon);
				}
			}
			$this->mReplacedSubConditions['__#SUB_CON' . count($this->mReplacedSubConditions) . '#__'] = $origCon;
		}
		
		private function replaceField($field){
			if (preg_match('/^["\']/', $field) OR preg_match('/["\']$/', $field))
				return $field;
			$divisions = preg_split('/\./', $field, -1);
			$cntDivisions = count($divisions);
			$usedTables = array_flip($this->mReplacedTables);
			$dbName = '';
			$tableName = '';
			$fieldName = '';
			$replacedStr = $field;
			if (1 == $cntDivisions){
				$fieldName = $divisions[0];
			}
			elseif (2 == $cntDivisions){
				$tableName = $divisions[0];
				$fieldName = $divisions[1];
				$relativeDbs = preg_grep(self::makeSafeRegExPattern($tableName), $usedTables);
				if (0 < count($relativeDbs))
					$replacedStr = $this->mReplacedTables[current($relativeDbs)] . '.' . $fieldName;
			}
			elseif (3 == $cntDivisions){
				$dbName = $divisions[0];
				$tableName = $divisions[1];
				$fieldName = $divisions[2];
				if (in_array($dbName . '.' . $tableName, $usedTables))
					$replacedStr = $this->mReplacedTables[$dbName . '.' . $tableName] . '.' . $fieldName;
			}
			return $replacedStr;
		}
		
		private function replaceTable($tableName){
			$tableName = trim($tableName);
			$divisions = preg_split('/\./', $tableName, -1, PREG_SPLIT_NO_EMPTY);
			$replacedStr = '';
			if (count($divisions) == 2){ // dbName.tableName[.fieldName]
				// check database and table
				$ids = $this->checkTableReference($divisions[0], $divisions[1]);
				$replacedStr = 'gaLogView_' . $ids['DB_ID'] . '_' . $ids['TB_ID'];
			}
			elseif(preg_match('/^(__#SUB_SELECT(\d)+#__)$/', $tableName)){ // if derived table
				$replacedStr = $tableName;
			}
			else{
				throw new GaLogException(GaLogException::INVALID_TABLE_REFERENCE_FORMAT, $this->mDebugTrace, "TbName: '$tableName'");
			}
			return $replacedStr;
		}
		
		private function checkTableReference($databaseName, $tableName){
			/***
			$logDb = LogDb::getLogDbByDbName($databaseName);
			if (is_null($logDb)){
				if (LogDb::isPhysicalDbExists($databaseName)){
					$logDb = new LogDb();
					$logDb->setLogDbName($databaseName);
					$logDb->save();
				}
				else {
					throw new GaLogException(GaLogException::NONE_EXISTING_DATABASE, $this->mDebugTrace, "DbName: '$databaseName'");
				}
			}
			$logTable = $logDb->getLogTableByTableName($tableName);
			if (is_null($logTable)){
				if (DBSchema::isPhysicalTableExists($databaseName, $tableName)){
					//require_once('gaLogs/Class.GaLogger.php');
					$logTable = new LogTable();
					$logTable->setLogDbID($logDb->getLogDbID());
					$logTable->setLogTableName($tableName);
					$logTable->save();
				}
				else{
					throw new GaLogException(GaLogException::NONE_EXISTING_TABLE_FOR_DB, $this->mDebugTrace, "DbName: '$databaseName' TbName: '$tableName'");
				}
			}
			***/
			//$logTable = LogTable::create($databaseName,$tableName);
			require_once('gaLogs/Class.LogSubjectTable.php');
			$logTable = LogTable::create(LogSubjectTable::create($databaseName,$tableName));
			return array('DB_ID' => $logTable->getLogDbID(), 'TB_ID' => $logTable->getLogTableID());
		}
		
		private static function reconstruct($sql, $pattern, $replacement){
			$arr = preg_split('/[\s\(\)]/', $sql, -1, PREG_SPLIT_NO_EMPTY);
			$matches = preg_grep($pattern, $arr);
			if (0 == count($matches))
				return $sql;
			foreach($matches as $match)
				$sql = preg_replace(self::makeSafeRegExPattern($match), $replacement[$match], $sql);
			return self::reconstruct($sql, $pattern, $replacement);
		}
		
		static function makeSafeRegExPattern($str){
			return '/(' . addcslashes($str, '{}(^+-.*|/)') . ')/';
		}
		
		// rename alias to its original table in replacedTables, for error display
		private function renameAliasesToReplacedTable(){
			foreach ($this->mTablesWithAliases as $origTable => $replacedTable)
				$this->mReplacedTables[$origTable] = $replacedTable;
		}
	}
?>