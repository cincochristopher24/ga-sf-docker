<?php
	require_once('Class.DBConnection.php');
	class LoggerConnection
	{
		static $instance = null;
		static public function create(){
			if(is_null(self::$instance)){
				self::$instance = new self();
			}
			return self::$instance;
		}
		
		/*** For Testability Purposes ***/
		static public function reset(){
			self::$instance = null;
		}

		private $mDb = null;
		private function __construct(){			
			require_once('serverConfig/Class.ServerConfig.php');
			$dbConf = ServerConfig::getInstance();
			$this->mDb = new DBConnection('uid=logger;pwd='.$dbConf->getDbPassword().';db=GaLogs');
		}
		
		public function getDbHandler(){
			return $this->mDb;
		}
		
	}
?>