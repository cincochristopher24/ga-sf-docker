<?php
	/***
	 * @author: Reynaldo Castellano III
	 */
	require_once('gaLogs/Class.GaLogger.php');
	require_once('gaLogs/Class.DBSchema.php');
	require_once('gaLogs/exception/Class.GaLogException.php');
	require_once('gaLogs/Class.AffectedRecord.php');
	class LogStorageTable
	{
		static private $OBJECT_CACHE = array();
		static public function getInstanceByLogTable($logTable){
			$key = $logTable->getLogDbID().'_'.$logTable->getLogTableID();
			if(!array_key_exists($key,self::$OBJECT_CACHE)){
				self::$OBJECT_CACHE[$key] = new self($logTable);
			}
			return self::$OBJECT_CACHE[$key];
		}
		
		static public function reset(){
			self::$OBJECT_CACHE = array();
		}
		
		private $mLogTable 			= null;
		private $mLogStorageTableName 	= null;
		private $mStorageFields 	= null;
		private $mActualFields 		= null;
		private $mDb = null;
		
		private function __construct($logTable){
			if(!$logTable->getLogTableID()){
				throw new exception(GaLogException::NONE_PERSISTENT_LOGTABLE);
			}
			$this->mLogTable 			= $logTable;
			$this->mLogStorageTableName = 'tblPhysicalLogTable_'.$logTable->getLogDbID().'_'.$logTable->getLogTableID();
			$this->mDb 					= LoggerConnection::create()->getDbHandler();
			//$this->refreshFields();
		}
		
		private function initFields(){
			$physicalFields = DBSchema::getPhysicalTableFieldNames('GaLogs',$this->mLogStorageTableName);
			$this->mStorageFields 	= array();
			foreach($physicalFields as $iPhysicalField){
				$fieldName = $iPhysicalField['Field'];
				if('affectedRecordID' != $fieldName){
					$this->mActualFields[$fieldName] = $iPhysicalField;
					$fieldNameParts = explode('_',$iPhysicalField['Field']);
					if(count($fieldNameParts) && !strlen($fieldNameParts[0]) && 'OLD' == $fieldNameParts[1]){
						$this->mStorageFields[implode('_',array_slice($fieldNameParts,2))] = $iPhysicalField;
					}
				}
			}
		}
		
		public function getName(){
			return $this->mLogStorageTableName;
		}
		
		public function refreshFields(){
			$this->initFields();
		}

		public function getActualFieldNames(){
			return array_keys($this->getActualFields());
		}
		
		private function getActualFields(){
			if(is_null($this->mActualFields)){
				$this->initFields();
			}
			return $this->mActualFields;
		}
		
		private function getStorageFields(){
			if(is_null($this->mStorageFields)){
				$this->initFields();
			}
			return $this->mStorageFields;
		}
		
		/***
		public function synchronizeWithSubjectTable($subjectTable){
			//check if the physical logTable exists, if not create one.
			$subjectFieldNames = $subjectTable->getFieldNames();
			if(!count($this->mStorageFields)){
				$sqlFieldNames = array(
					'`affectedRecordID` BIGINT(11) NOT NULL'
				);
				foreach($subjectFieldNames as $iSubjectFieldName){
					$sqlFieldNames[] = '`_OLD_'.$iSubjectFieldName.'` TEXT NULL';
					$sqlFieldNames[] = '`_NEW_'.$iSubjectFieldName.'` TEXT NULL';
				}
				$createSql 	= 'CREATE TABLE `GaLogs`.'.$this->mLogStorageTableName.'('.implode(', ',$sqlFieldNames).')ENGINE=MyIsam';
				$this->mDb->execute($createSql);
			}
			else{
				//if it does exists, check if it synchronized with the subject table through the logTable metadata.
				//check only for added fields
				$newSubjectFieldNames = array();
				foreach($subjectFieldNames as $iSubjectFieldName){
					if(!in_array($iSubjectFieldName, $this->mStorageFields)){
						$newSubjectFieldNames[] = 'ADD `_OLD_'.$iSubjectFieldName.'` TEXT NULL';
						$newSubjectFieldNames[] = 'ADD `_NEW_'.$iSubjectFieldName.'` TEXT NULL';
					}
				}
				if(count($newSubjectFieldNames)){
					$alterSql = 'ALTER TABLE `GaLogs`.`'.$this->mLogStorageTableName.'` '.implode(',',$newSubjectFieldNames);
					$this->mDb->execute($alterSql);
				}
			}
			$this->mStorageFields = DBSchema::getPhysicalTableFieldNames('GaLogs',$this->mLogStorageTableName);
			$this->refreshFields();
		}
		***/

		public function synchronizeWithSubjectTable($subjectTable){
			//check if the logStorageTable exists, if not create one
			$subjectFields = $subjectTable->getFields();
			$modified = false;
			$fieldsChanged = false;
			if(!count($this->getStorageFields())){
				$modified = true;
				$fieldsChanged = true;
				$sqlFieldNames = array(
					'`affectedRecordID` BIGINT(11) NOT NULL PRIMARY KEY '
				);
				$indexFields = array();
				foreach($subjectFields as $iSubjectField){
					$oldFieldName = '`_OLD_'.$iSubjectField['Field'].'`';
					$newFieldName = '`_NEW_'.$iSubjectField['Field'].'`';
					$sqlFieldNames[] = $oldFieldName.$iSubjectField['Type'].' NULL';
					$sqlFieldNames[] = $newFieldName.$iSubjectField['Type'].' NULL';

					if(isset($iSubjectField['Key']{0})){
						$indexFields[] = 'KEY '.$oldFieldName. '('.$oldFieldName.')';
						$indexFields[] = 'KEY '.$newFieldName. '('.$newFieldName.')';
					}
				}
				$indexSql = count($indexFields) ? ', '.implode(', ',$indexFields) : '' ;
				$createSql 	= 'CREATE TABLE `GaLogs`.'.$this->mLogStorageTableName.'('.implode(', ',$sqlFieldNames). $indexSql .')ENGINE=MyIsam';
				$this->mDb->execute($createSql);
			}
			else{
				
				//ALTER TABLE  `tblTest4` ADD  `testField4` VARCHAR( 12 ) NOT NULL ;
				//ALTER TABLE `testDB`.`tblUnsyncTable` CHANGE `field1` `field1` smallint( 10 ) NOT NULL
				//ALTER TABLE  `tblTest4` ADD INDEX (  `testField1` )
				//look for added fields, newly indexed fields, fields whose data type changed
				$storageFields = $this->getStorageFields();
				$alterSqlFields = array();
				foreach($subjectFields as $iSubjectField){
					$subjectFieldName = $iSubjectField['Field'];
					$subjectFieldType = $iSubjectField['Type'];
					//check for added fields
					if(!array_key_exists($subjectFieldName, $storageFields)){						
						$fieldsChanged = true;						
						$alterSqlFields[] = "ADD `_OLD_$subjectFieldName` $subjectFieldType NULL";
						$alterSqlFields[] = "ADD `_NEW_$subjectFieldName` $subjectFieldType NULL";						
						if(isset($iSubjectField['Key']{0})){
							$alterSqlFields[] = "ADD INDEX (`_OLD_$subjectFieldName`)";
							$alterSqlFields[] = "ADD INDEX (`_NEW_$subjectFieldName`)";
						}
					}
					else{
						$storageField = $storageFields[$iSubjectField['Field']];
						//check for fields whose datatype was changed
						if($storageField['Type'] != $iSubjectField['Type']){
							$alterSqlFields[] = "CHANGE `_OLD_$subjectFieldName` `_OLD_$subjectFieldName` $subjectFieldType";
							$alterSqlFields[] = "CHANGE `_NEW_$subjectFieldName` `_NEW_$subjectFieldName` $subjectFieldType";
						}
						
						//check for newly indexed fields						
						if(isset($iSubjectField['Key']{0}) && !isset($storageField['Key']{0})){
							$alterSqlFields[] = "ADD INDEX (`_OLD_$subjectFieldName`)";
							$alterSqlFields[] = "ADD INDEX (`_NEW_$subjectFieldName`)";
						}
					}
				}

				if(count($alterSqlFields)){
					$modified = true;
					$alterSql = 'ALTER TABLE `GaLogs`.`'.$this->mLogStorageTableName.'` '.implode(',',$alterSqlFields);
					$this->mDb->execute($alterSql);
				}
			}
			if($modified){
				$this->mDb->execute('OPTIMIZE TABLE '.$this->mLogStorageTableName);
			}
			if($fieldsChanged){
				$subjectTable->refreshTriggers();
				LogView::create()->refresh($this->mLogTable);
			}			
		}

		
		public function addLogTrails($logTrails){
			$records = array();
			$fieldNames = array();		
			foreach($logTrails as $iTrailRecord){
				$dmlStatement 	= $iTrailRecord->getDmlStatement();
				$affectedRecord = new AffectedRecord();
				$affectedRecord->setDmlStatementID($dmlStatement->getDmlStatementID());
				$affectedRecord->setEvent($iTrailRecord->getEvent());
				$affectedRecord->save();
				$fields = array(
					'`affectedRecordID`' => $this->mDb->makeSqlSafeString($affectedRecord->getAffectedRecordID())
				);
				foreach($iTrailRecord->getData() as $field => $value){
					$fields[$field] = (!is_null($value) ? $this->mDb->makeSqlSafeString($value) : 'NULL');
				}
				if(!count($fieldNames)){
					$fieldNames = array_keys($fields);
				}
				$records[] = '('.implode(', ',$fields).')';
			}
			if(count($records)){
				$insertSql ="INSERT INTO GaLogs.{$this->mLogStorageTableName} (".implode(', ',$fieldNames).")".
							"VALUES ".implode(', ',$records);
				$this->mDb->execute($insertSql);
			}
		}
		
	}
?>