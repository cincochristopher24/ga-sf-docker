<?php
	class LogTrail
	{
		static public function createInstance($rs,$dmlStatement){
			$data = array();
			foreach($rs as $row){
				$data['_OLD_'.$row['fieldName']] = $row['oldValue'];
				$data['_NEW_'.$row['fieldName']] = $row['newValue'];
			}
			return new self($rs[0]['event'],$data,$dmlStatement);
		}

		private $mData 	= array();
		private $mEvent = null;
		private $mDmlStatement = null;
		
		private function __construct($event,$data, $dmlStatement){
			$this->mEvent = $event;
			$this->mData = $data;
			$this->mDmlStatement = $dmlStatement;
		}
		
		public function getEvent(){
			return $this->mEvent;
		}

		public function getData(){
			return $this->mData;
		}		
		
		public function getDmlStatement(){
			return $this->mDmlStatement;
		}
		
	}
?>