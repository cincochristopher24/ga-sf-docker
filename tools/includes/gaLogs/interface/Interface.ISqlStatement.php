<?php
	/***
	 * @author: Reynaldo Castellano III
	 */
	interface ISqlStatement
	{
		public function getSubjectTableNames();
		public function getCommand();
		public function getSqlStatement();
	}
?>