<?php
	/***
	 * @author: Reynaldo Castellano III
	 */
	
	require_once('gaLogs/Class.LoggerConnection.php');
	require_once('gaLogs/Class.LogTrailTable.php');
	require_once('gaLogs/Class.GaLogSystem.php');
	
	class LogThread
	{
		const INACTIVE = 0;
		const ACTIVE = 1;
		
		static private $instance = null;
		
		static public function create(){
			if(is_null(self::$instance)){
				self::$instance = new LogThread();
			}
			return self::$instance;
		}
		
		/*** For Testability Purposes ***/
		static public function reset(){
			self::$instance = null;
		}
		
		private $mLogThreadID = null;
		
		private function __construct(){
			$this->mLogThreadID = str_replace('.','',uniqid(rand(), true));
			GaLogSystem::create()->performSystemMaintainance();
		}
		
		public function getLogThreadID(){
			return $this->mLogThreadID;
		}
	}
?>