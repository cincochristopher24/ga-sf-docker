<?php
	class DmlStatementRegistry
	{
		static private $instance = null;
		static public function getInstance(){
			if(is_null(self::$instance)){
				self::$instance = new self;
			}
			return self::$instance;
		}
		
		private $mRegistry = array();
		
		private function __construct(){}
		
		public function register($statement){
			$this->mRegistry[$statement->getSqlStatementID()] = $statement;
		}
		
		public function getAllStatements(){
			return $this->mRegistry;
		}
	}
?>