<?php
	class DmlPreparedStatementRegistry
	{
		static private $instance = null;
		static public function getInstance(){
			if(is_null(self::$instance)){
				self::$instance = new self;
			}
			return self::$instance;
		}
		
		private $mID = 0;
		private $mRegistry = array();
		private function __construct(){}
		
		public function register($stmtObj){
			$this->mRegistry[$stmtObj->getStatementID()] = $stmtObj;
		}
		
		public function unregister($statement){
			$stmtID = $this->getStatementID($statement);
			if(isset($this->mRegistry[$stmtID])){
				unset($this->mRegistry[$stmtID]);
			}
		}
		
		public function getStatement($stmtObj){
			return isset($this->mRegistry[$stmtObj->getStatementID()]) ? $this->mRegistry[$stmtObj->getStatementID()] : null;
		}
		
		public function generateID(){
			return $this->mID++;
		}
		
	}
?>