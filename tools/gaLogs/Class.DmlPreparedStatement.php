<?php
	class DmlPreparedStatement implements ISqlStatement
	{
		private $mPreparedStmtObjID = null;
		private $mSqlStmtObj = null;
		private $mBoundVars = array();
		
		public function __construct($clientPrepStmtObj, $sqlStmtObj){
			$this->mPreparedStmtObjID = $clientPrepStmtObj->getStatementID();
			$this->mSqlStmtObj = $sqlStmtObj;
		}
		
		public function getStatementID(){
			return $this->mPreparedStmtObjID;
		}
		
		public function setBoundVars($vars){
			$this->mBoundVars = $vars;
		}
		
		/*** Interface Functions ***/
		public function getSubjectTableNames(){
			return $this->mSqlStmtObj->getSubjectTableNames();
		}
		
		public function getCommand(){
			return $this->mSqlStmtObj->getCommand();
		}
		
		public function getSqlStatement(){
			$vars = json_encode($this->mBoundVars);
			return $this->mSqlStmtObj->getSqlStatement()."\n #PARAMS# \n $vars";
		}
		
		public function isDmlStatement(){
			return $this->mSqlStmtObj->isDmlStatement();
		}
		
		public function trackChanges($res){
			return $this->mSqlStmtObj->trackChanges($res);
		}		
	}
?>