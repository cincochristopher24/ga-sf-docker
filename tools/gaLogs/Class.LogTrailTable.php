<?php
	require_once('gaLogs/abstract/Class.AbstractLogTrailTable.php');
	require_once('gaLogs/Class.LogThread.php');
	class LogTrailTable extends AbstractLogTrailTable
	{
		/*** Strategy Pattern ***/
		static private $LOG_TRAIL_TABLES = array();
		static public function initStrategies(){
			$osDelimiter =  '/';
			$arDir = explode($osDelimiter,__FILE__);
			$strategyDir = implode($osDelimiter, array_slice($arDir,0,count($arDir)-1)).$osDelimiter.'logTrailTable';
			$strategyFiles = scandir($strategyDir);
			$strategyFiles = array_slice($strategyFiles,2,count($strategyFiles));
			foreach($strategyFiles as $iFile){
				//extract the command key and the classname
				$fileNameLen = strlen($iFile);
				if('.php' == substr($iFile,$fileNameLen-4, $fileNameLen)){
					$comKey = strtolower(str_replace(array('Class.','LogTrailTable.php'),'',$iFile));
					$clsName = str_replace(array('Class.','.php'),'',$iFile);
					require_once($strategyDir.'/'.$iFile);
					$clsInstance =  new $clsName();
					if($clsInstance instanceof AbstractLogTrailTable){
						self::$LOG_TRAIL_TABLES[$comKey] = $clsInstance;
					}
				}
			}
		}
		
		static public function getLogTrailTables(){
			return self::$LOG_TRAIL_TABLES;
		}
		
		static public function purgeAllThreadTrails(){
			$logThreadID = LogThread::create()->getLogThreadID();
			foreach(self::$LOG_TRAIL_TABLES as $iLogTrailTable){
				$iLogTrailTable->purgeThreadTrails($logThreadID);
			}
		}
		
		static public function purgeAllLogTrailTables(){
			foreach(self::$LOG_TRAIL_TABLES as $iLogTrailTable){
				$iLogTrailTable->purge();
			}
		}
		
		static public function isSupportedEngine($engine){
			return array_key_exists(strtolower($engine),self::$LOG_TRAIL_TABLES);
		}
		
		/*** Factory Method Pattern ***/
		static public function create($engineType){
			$engineType = strtolower($engineType);
			if(array_key_exists($engineType,self::$LOG_TRAIL_TABLES)){
				return new self(self::$LOG_TRAIL_TABLES[$engineType]);
			}
			return null;
		}
		
		/*** Actuall Class Definition ***/
		private $mCoreTable = null;
		private function __construct($coreTable){
			$this->mCoreTable = $coreTable;
		}
		
		public function getLogTrails($dmlTable){
			return $this->mCoreTable->getLogTrails($dmlTable);
		}
		
		public function purgeThreadTrails($threadID){
			return $this->mCoreTable->purgeThreadTrails($threadID);
		}
		
		public function purge($from=null){
			return $this->mCoreTable->purge($from);
		}
		
		public function getStorageEngineType(){
			return $this->mCoreTable->getStorageEngineType();
		}
		
		public function getLogTrailTableName(){
			return $this->mCoreTable->getLogTrailTableName();
		}
	}
	LogTrailTable::initStrategies();
?>