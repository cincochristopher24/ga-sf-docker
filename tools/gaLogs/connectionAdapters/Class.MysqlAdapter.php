<?php
	/***
	 * @author: Reynaldo Castellano III
	 ***/	
	require_once('Class.DBConnection.php');
	require_once('gaLogs/Class.GaLogger.php');
	require_once('gaLogs/Class.SqlStatement.php');
	require_once('gaLogs/interface/Interface.IConnectionAdapter.php');
	/*****
	 * A class that enables the GaLogs system to operate on a mysql resource.
	*****/
	
	class MysqlAdapter implements IConnectionAdapter
	{
		private $mConMap = array();
		private $mConStack = array();
		private $mConID = 0;
		
		public function __construct(){}
		
		public function connect($con){
			return is_resource($con) && 'mysql link' == get_resource_type($con);
		}
		
		public function getActiveDatabase($con){
			$db = new DBConnection($con);
			return $db->getActiveDatabase();
		}
		
		public function isConnectionOpen($con){
			$db = new DBConnection($con);
			return $db->isConnectionOpen();
		}
		
		/***
		public function getAffectedRows($con){
			return mysql_affected_rows($con);
		}
		***/
		
		public function isInTransaction($con){
			$db = new DBConnection($con);
			if($db->isConnectionOpen()){
				try{
					$db->execQuery('SAVEPOINT glsvpnt;');
					$db->execQuery('ROLLBACK TO SAVEPOINT glsvpnt;');
					$db->execQuery('RELEASE SAVEPOINT glsvpnt;');
					return true;
				}
				catch(exception $e){}
			}
			return false;
		}
		
		public function getConnection($con){
			//check if there is already a client connection for the given connection resource
			foreach($this->mConMap as $conID => $conResource){
				if($con === $conResource){
					return $this->mConStack[$conID];
				}
			}			
			//we dont have the resource in the stack, lets create a new ClientConnection object.
			$conID = 'mysql_' . ++$this->mConID;
			//NOTE: We are creating a circular reference here. But this time its ok, since we need the connections untill then end of the script.
			$clientCon = new ClientConnection($con, $conID, $this);
			$this->mConMap[$conID] = $con;
			$this->mConStack[$conID] = $clientCon;
			return $clientCon;
		}
		
		public function isSqlSyntaxOk($con,$sql){
			//lets try to disable to boost performance
			/***
			$db = new DBConnection($con);		
			try{
				$db->execute('PREPARE gaLogStmt FROM "'.addslashes($sql).'"');
				return true;
			}
			catch(exception $e){}
			return false;
			***/
			return true;
		}
		
		public function setConnectionVars($con, $vars=array()){
			$db = new DBConnection($con);
			$conVars = array();
			foreach($vars as $iVarName => $iVarVal){
				$conVars[] = "@$iVarName = ".$db->makeSqlSafeString($iVarVal);
			}
			$db->execute('SET '.implode(', ',$conVars).';');
		}
		
		public function getConnectionVars($con, $varNames){
			$db = new DBConnection($con);
			$prependedVarNames = array();
			$varValues = array();
			foreach($varNames as $iVarName){
				if(isset($iVarName{0})){
					$prependedVarNames[] = '@'.$iVarName;
					$varValues[$iVarName] = null;
				}
			}
			if(count($prependedVarNames)){
				$sql = 'SELECT '.implode(', ',$prependedVarNames);
				$rs = new iRecordset($db->execute($sql));
				foreach($varValues as $varKey => $varVal){
					$varValues[$varKey] = $rs->{'get@'.$varKey}(0);
				}
			}
			return $varValues;
		}
		
	}
?>