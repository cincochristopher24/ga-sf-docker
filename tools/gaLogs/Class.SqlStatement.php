<?php
	require_once('gaLogs/interface/Interface.ISqlStatement.php');
	require_once('gaLogs/Class.DmlTable.php');
	require_once('gaLogs/Class.DmlStatement.php');
	require_once('gaLogs/Class.DmlStatementRegistry.php');
	
	class SqlStatement implements ISqlStatement
	{
		private $mStrategyObject	= null;
		private $mSqlStatementID 	= null;
		private $mLogTrace  		= null;
		private $mAffectedRows 		= -1;
		
		/*** Class Definition ***/
		public function __construct($strategy, $checkTables=true){
			$this->mStrategyObject = $strategy;
			$this->mSqlStatementID = spl_object_hash($this);			
			$this->setUpEnvironment();			
			if($checkTables){
				//for each subject table, create a corresponding DmlTable object
				$tblNames = $this->getSubjectTableNames();
				foreach($tblNames as $tableName){
					$dmlTable = DmlTable::create(trim($tableName),$this);
				}
			}
			if($this->isDmlStatement()){
				//register this statement
				DmlStatementRegistry::getInstance()->register($this);
			}
		}
		
		private function setUpEnvironment(){
			DataTracker::create()->getActiveClientConnection()->setConnectionVars(array(
				'gaLogDmlStatementID' 	=> $this->mSqlStatementID,
				'gaLogAffectedRows'		=> 0
			));
		}
		
		public function getAffectedRows(){
			DataTracker::create()->getActiveClientConnection()->getAffectedRows();
		}
		
		public function getSqlStatementID(){
			return $this->mSqlStatementID;
		}
		
		/*** Interface Functions ***/
		public function getSubjectTableNames(){
			return $this->mStrategyObject->getSubjectTableNames();
		}
		
		public function getCommand(){
			return $this->mStrategyObject->getCommand();
		}
		
		public function getSqlStatement(){
			return $this->mStrategyObject->getSqlStatement();
		}
		
		public function isDmlStatement(){
			return $this->mStrategyObject->isDmlStatement();
		}
		
		public function trackChanges($res){
			return $this->mStrategyObject->trackChanges($res);
		}
		
	}
?>