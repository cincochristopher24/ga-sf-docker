<?php
	class ClientTrace
	{
		static private $instance = null;
		static public function create(){
			if(is_null(self::$instance)){
				self::$instance = new ClientTrace();
			}
			return self::$instance;
		}
		
		/*** For Testability Purposes ***/
		static public function reset(){
			self::$instance = null;
		}
		
		private $mCurrentTrace = null;
		
		private function __construct(){}
		
		public function setCurrentTrace($currentTrace){
			$this->mCurrentTrace = $currentTrace;
		}
		
		public function getCurrentTrace(){
			return $this->mCurrentTrace;
		}
		
		public function getFileName(){
			return (array_key_exists('file',$this->mCurrentTrace) ? $this->mCurrentTrace['file'] : null );
		}
		
		public function getLineNumber(){
			return (array_key_exists('line',$this->mCurrentTrace) ? $this->mCurrentTrace['line'] : null );
		}
	}
?>