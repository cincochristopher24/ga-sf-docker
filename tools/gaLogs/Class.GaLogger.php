<?php
	/***
	 * @author 	: Reynaldo Castellano III
	 * @date	: September 11, 2007
	 * @version : 1.3
	 */
	
	require_once('gaLogs/exception/Class.GaLogException.php');
	require_once('gaLogs/Class.ClientDomain.php');
	require_once('gaLogs/Class.UserType.php');
	require_once('gaLogs/Class.LogTrace.php');
	require_once('gaLogs/Class.DataTracker.php');
	require_once('gaLogs/Class.GaLog.php');
	require_once('gaLogs/Class.GaLogSystem.php');
	require_once('gaLogs/Class.ClientConnectionManager.php');
	
	class GaLogger
	{
		static private $logger = null;
		static public function create(){
			if(is_null(self::$logger)){
				ini_set("pcre.backtrack_limit","1000000");
				self::$logger = new GaLogger();
			}
			return self::$logger;
		}
		
		static public function isInitialized(){
			return !is_null(self::$logger);
		}
		
		static public function prepareOnShutdown(){
			register_shutdown_function(array('GaLogger','shutdown'));
		}

		static public function shutdown(){
			self::create()->performShutdown();
		}
		
		/*** For Testability Purposes Only **/
		static public function reset(){
			self::$logger = null;
			ClientConnectionManager::reset();
			//Client_Trace::reset();
			DataTracker::reset();
			GaLog::reset();
			GaLogSystem::reset();
			LoggerConnection::reset();
			LogThread::reset();
			LogTable::reset();
			LogStorageTable::reset();
			LogSubjectTable::reset();
			LogDB::reset();
			
		}
		
		private $mDebugMode				= false;
		
		private $mIsStarted				= false;
		private $mClientDomain			= null;
		private $mInitialTrace			= null;
		
		private $mTransactionStack 		= array();
		private $mNoTrackingSectionStack = array();
		
		private $mIsInTrackingMode		= false;
		
		private function __construct(){}
		
		private function isInNoTrackingSection(){
			return count($this->mNoTrackingSectionStack) > 0;	
		}
		
		public function isStarted(){
			return $this->mIsStarted;
		}
		
		public function start($loggerID=null,$loggerType=null){
			try{
				if(array_key_exists('dbhdebug',$_GET)){
					echo '<br /><br />########## GaLogs Booting Up! ########<br /><br />';
				}
				DataTracker::create()->beginNoTrackingSection();
					$trace = debug_backtrace();
					$this->mClientDomain = ClientDomain::getClientDomainByDomainName($_SERVER['SERVER_NAME']);
					if($this->isStarted()){
						throw new GaLogException(GaLogException::LOGGER_ALREADY_STARTED,$trace[0]);
					}
					if(is_null($this->mClientDomain)){
						throw new GaLogException(GaLogException::UNREGISTERED_LOGDOMAIN,$trace[0]);
					}
					$userType = $this->mClientDomain->getUserTypeByUserTypeName($loggerType);
					if(is_null($userType)){
						throw new GaLogException(GaLogException::INVALID_LOGGER_TYPE,$trace[0]);
					}
					if(is_null($loggerID) || !$loggerID){
						throw new GaLogException(GaLogException::INVALID_LOGGERID,$trace[0]);
					}
					$logTrace = new LogTrace();
					$logTrace->initializeTrace($trace[0]);
					$this->mIsStarted = true;

					$gaLog = GaLog::create();
					$gaLog->setDoerTypeID($userType->getUserTypeID());
					$gaLog->setDoerID($loggerID);
					$gaLog->setClientDomainID($this->mClientDomain->getClientDomainID());
					$gaLog->setIpAddress($_SERVER['REMOTE_ADDR']);
					$gaLog->setUserAgent($_SERVER['HTTP_USER_AGENT']);
					$gaLog->setTrace($logTrace);
				DataTracker::create()->endNoTrackingSection();
			}
			catch(exception $e){
				DataTracker::create()->endNoTrackingSection();
				throw $e;
			}
		}
		
		public function performShutdown(){
			if($this->isStarted()){
				if(array_key_exists('dbhdebug',$_GET)){
					echo '<br /><br />########## GaLogs Shuttting Down! ########<br /><br />';
				}
				DataTracker::create()->beginNoTrackingSection();
				try{
					require_once('gaLogs/Class.LogTrailAuditor.php');
					LogTrailAuditor::create()->auditLogTrails();
				}
				catch(exception $e){
					DataTracker::create()->endNoTrackingSection();
					throw $e;
				}
				DataTracker::create()->endNoTrackingSection();
				$this->reset();
			}
		}

		/*** Data Tracker Interface ***/
		public function isOkToTrack(){
			//it is ok to track if the gaLogger has been started, we are not in a no tracking section and there is an opened section
			return ($this->isStarted() && !$this->isInNoTrackingSection() && $this->mClientDomain->isLoggingEnabled());
		}
		
		/*** Transaction Manager Interface ***/
		public function beginNoTrackingSection(){
			$trace = debug_backtrace();
			$this->mNoTrackingSectionStack[] = $trace[0]['file'].'::'.$trace[0]['line'];
			$this->toggelClientConnections();
		}
		
		public function endNoTrackingSection(){
			if($this->isInNoTrackingSection()){
				array_pop($this->mNoTrackingSectionStack);
			}
			else{
				$trace = debug_backtrace();
				throw new GaLogException(GaLogException::NO_OPENED_NO_TRACKING_SECTION,$trace[0]);
			}
			//$this->toggelClientConnections();
		}
		
		public function toggelClientConnections(){
			$trackingMethod = count($this->mNoTrackingSectionStack) ? 'disableTracking' : 'enableTracking';
			foreach(ClientConnectionManager::create()->getAllClientConnections() as $iClientCon){
				$iClientCon->$trackingMethod();
			}
		}
		
	}
	GaLogger::prepareOnShutdown();
?>