<?php
	/***
	@author: Reynaldo Castellano III
	***/

	require_once('gaLogs/Class.LoggerConnection.php');

	class LogField
	{
		private $mLogFieldID;
		private $mLogTableID;
		private $mLogFieldName;
		private $mDb;

		public function __construct($param=null,$data=null){
			$this->mDb = LoggerConnection::create()->getDbHandler();
			$this->clearProperties();
			if(!is_null($param)){
				$sql =	'SELECT `logFieldID`,`logTableID`,`logFieldName` '.
						'FROM `GaLogs`.`tblLogField` '.
						'WHERE `logFieldID` = '.$this->mDb->makeSqlSafeString($param);
				$rs = new iRecordset($this->mDb->execQuery($sql));
				if($rs->retrieveRecordCount()){
					$this->initProperties($rs->retrieveRow(0));
				}
				else{
					throw new exception('Invalid logFieldID '.$param.' passed to object of type LogField.');
				}
			}
			if(is_array($data)){
				$this->initProperties($data);
			}
		}

		private function clearProperties(){
			$this->setLogFieldID(null);
			$this->setLogTableID(null);
			$this->setLogFieldName(null);
		}

		private function initProperties($data){
			$this->setLogFieldID($data['logFieldID']);
			$this->setLogTableID($data['logTableID']);
			$this->setLogFieldName($data['logFieldName']);
		}

		//SETTERS
		private function setLogFieldID($param=0){
			$this->mLogFieldID=$param;
		}

		public function setLogTableID($param=0){
			$this->mLogTableID=$param;
		}

		public function setLogFieldName($param=null){
			$this->mLogFieldName=$param;
		}

		//GETTERS
		public function getLogFieldID(){
			return $this->mLogFieldID;
		}

		public function getLogTableID(){
			return $this->mLogTableID;
		}

		public function getLogFieldName(){
			return $this->mLogFieldName;
		}

		//CRUDE
		public function save(){
			if(!$this->getLogFieldID()){
				$sql =	'INSERT INTO `GaLogs`.`tblLogField`('.
							'`logTableID`,'.
							'`logFieldName`'.
						')VALUES('.
							$this->mDb->makeSqlSafeString($this->getLogTableID()).','.
							$this->mDb->makeSqlSafeString($this->getLogFieldName()).
						')';
				$this->mDb->execQuery($sql);
				$this->setLogFieldID($this->mDb->getLastInsertedID());
			}
			else{
				$sql =	'UPDATE `GaLogs`.`tblLogField` SET '.
							'`logTableID`= '.$this->mDb->makeSqlSafeString($this->getLogTableID()).','.
							'`logFieldName`= '.$this->mDb->makeSqlSafeString($this->getLogFieldName()).' '.
						'WHERE `logFieldID` = '.$this->mDb->makeSqlSafeString($this->getLogFieldID());
				$this->mDb->execQuery($sql);
			}
		}

		public function delete(){
			$sql =	'DELETE FROM `GaLogs`.`tblLogField` '.
					'WHERE `logFieldID` = '.$this->mDb->makeSqlSafeString($this->getLogFieldID());
			$this->mDb->execQuery($sql);
			$this->clearProperties();
		}
		
		static public function getInstancesByLogTable($logTable){
			$db = LoggerConnection::create()->getDbHandler();
			$stmt=	"SELECT logFieldID, logTableID, logFieldName ".
					"FROM GaLogs.tblLogField ".
					"WHERE logTableID = ".$db->makeSqlSafeString($logTable->getLogTableID());
			$rs = new iRecordset($db->execute($stmt));	
			$ar = array();
			foreach($rs as $row){
				$ar[] = new LogField(null,$row);
			}
			return $ar;
		}
		
	}
?>