<?php
	/**
	 * RULES:
	 * - queries should only be select queries
	 * - table references should be of form 'Database_Name'.'Table_Name'
	 * - physical fields should be prepended with value type _NEW_ / _OLD_ (e.g. _NEW_travelerID, tblTraveler._NEW_travelerID)
	 * - magic keywords are treated as fields :
	 * 		1. _DOER_ID_	2. _EXECUTION_DATE_		3. _DOMAIN_NAME_	4. _DOER_TYPE_	5. _EVENT_
	 * 		6. _LOG_ID_		7. _COMMAND_			8. _SQL_STRING_		9. _IP_ADDRESS_
	 * - anything inside a pair of <$ $> will be ignored 		
	 **/
	
	
	//require_once('gaLogs/Class.GaLogIni.php');
	require_once('gaLogs/Class.LoggerConnection.php');
	require_once('gaLogs/Reader/Class.SelectModifier.php');
	require_once('gaLogs/exception/Class.GaLogException.php');
	
	
	class Reader{
		
		private $mUserSql = '';
		private $mTempSelect = '';
		private $mSubSelects = array();
		
		private $mStringsNotToBeReplaced = array();
		private $mReplacedSubSelects = array();
		private $mTransformedQuery = '';
		private $mSelectModifier = null;
		
		private $mDebugTrace	= null;
		private $mTablesUsed = array();
		
		/**
		 * @param String sqlStatement
		 * @return iRecordset 
		 **/		 
		public function getLogsBySql($sqlStatement){
			// strip single line comments
			$sqlStatement = preg_replace(array('/#(.*)?\n/', '/--(.*)?\n/'), '', $sqlStatement);
			// strip multi line comments, `, and make uniform white spaces
			$sqlStatement = preg_replace(array('/\/\*(.*)?\*\//', '/`/'),'', preg_replace('/\s+/', ' ', $sqlStatement));
			// trim white spaces enclosed in parentheses
			$this->mUserSql = preg_replace('/\s+\(\s*/', ' (', preg_replace('/\s*\)\s*/', ') ', stripslashes(trim($sqlStatement))));
			$this->mUserSql = addcslashes($this->mUserSql,'/?');
			
			$trace = debug_backtrace();
			$this->mDebugTrace = $trace[0];
			for($traceCtr = 0; $traceCtr < count($trace); ++$traceCtr){
				if(array_key_exists('function', $trace[$traceCtr]) && $trace[$traceCtr]['function'] == 'getLogsBySql')
					$this->mDebugTrace = $trace[$traceCtr];
			}
			
			$this->check();
			$this->findQueriesNotToReplace($this->mUserSql); // find queries that should not be parsed
			$this->mSelectModifier = new SelectModifier();
			$this->prepareQuery();
			$this->reconstructStringsNotToBeReplaced($this->mTransformedQuery);
			
			// prepend GaLogs to each used table
			array_walk($this->mTablesUsed, create_function('&$val, $key', '$val = "GaLogs." . $val;'));
			//$dbHandler = GaLogIni::createGaLogDbHandler();
			//GaLogIni::isReading();
			//$dbHandler = GaLogIni::createDbHandler();
			//$dbHandler->setDB('GaLogViews');
			$dbHandler = LoggerConnection::create()->getDbHandler();
			$dbHandler->setDB('GaLogViews');
			try{
				//echo $this->mTransformedQuery; exit;
				return new iRecordset($dbHandler->execQuery($this->mTransformedQuery));
			}
			catch(exception $e){
				throw new GaLogException(GaLogException::MYSQL_ERROR_, $this->mDebugTrace, $this->errorHandler(mysql_errno(), mysql_error()));
			}
			
		}
		
		private function check(){
			if (0 == preg_match('/^(SELECT)\s/i', $this->mUserSql))
				throw new GaLogException(GaLogException::NON_SELECT_SQL, $this->mDebugTrace);
			if ( 0!= (count((preg_grep('/[\(\)]/', str_split($this->mUserSql)))) % 2))
				throw new GaLogException(GaLogException::UNEQUAL_PARENTHESES, $this->mDebugTrace);
		}
		
		private function reconstruct($sql){
			//var_dump($this->mReplacedSubSelects); exit;
			$sql = preg_replace_callback('/(__#SUB_SELECT(\d)+#__)/',create_function('$matches','return " " . $matches[0] ." ";'),$sql);
			$arr = preg_split('/\s/', $sql, -1, PREG_SPLIT_NO_EMPTY);
			$subs = preg_grep('/^(__#SUB_SELECT(\d)+#__)/', $arr);
			
			if ( 0 == count($subs))
				return $sql;
			foreach ($subs as $key => $subIndex){
				$subIndex = preg_replace('/[\(\)]/', '', $subIndex);
				$arr[$key] = $this->mReplacedSubSelects[$subIndex];
			}
			return $this->reconstruct(implode(' ', $arr));
		}
		
		private function prepareQuery(){
			$unions = preg_split('/\s+(UNION)\s+/i', $this->mUserSql, -1, PREG_SPLIT_NO_EMPTY);
			$transformedSelects = array();
			foreach($unions as $subSelect){
				$subSelect = trim($subSelect);
				
				$this->replaceSubSelects($subSelect);
				foreach ($this->mSubSelects as $key => $value)
					$subSelect = preg_replace('/(' . addcslashes($value, '(^+-.*|/)') . ')/', $key, $subSelect);
				$this->mSelectModifier->modify($subSelect);
				
				// get used tables in subQuery
				$this->mTablesUsed = array_merge($this->mTablesUsed, $this->mSelectModifier->getReplacedTables()); 
				
				$this->mReplacedSubSelects['__#SUB_SELECT' . count($this->mSubSelects) . '#__'] = $this->mSelectModifier->getModifiedSql();
				$this->mSubSelects['__#SUB_SELECT' . count($this->mSubSelects) . '#__'] = $subSelect;
				$transformedSelects[] = $this->reconstruct($this->mReplacedSubSelects['__#SUB_SELECT' .( count($this->mReplacedSubSelects) -1 ). '#__']);
			}
			$this->mTransformedQuery = implode(' UNION ', $transformedSelects);
		}
		
		private function replaceSubSelects($sql){
			$arr = str_split($sql);
			$parenthesis = preg_grep('/[\(\)]/', $arr);
			$subSelects = array();
			$previous = null;
			foreach($parenthesis as $strKey => $value){
				if (')' == $value){
					$subSelect = implode(array_slice($arr, $previous[count($previous)-1]['key'], ($strKey - $previous[count($previous)-1]['key']) + 1));
					if(preg_match('/^(SELECT)\s/i', preg_replace('/^\(/','', $subSelect))){
						foreach ($this->mSubSelects as $key => $value)
							$subSelect = preg_replace('/(' . addcslashes($value, '(^+-.*|/)') . ')/', $key, $subSelect);
						$this->mSelectModifier->modify($subSelect);
						
						// get used tables in subQuery
						$this->mTablesUsed = array_merge($this->mTablesUsed, $this->mSelectModifier->getReplacedTables());
						
						$this->mReplacedSubSelects['__#SUB_SELECT' . count($this->mSubSelects) . '#__'] = $this->mSelectModifier->getModifiedSql();
						$this->mSubSelects['__#SUB_SELECT' . count($this->mSubSelects) . '#__'] = $subSelect;
					}
					array_pop($previous);
				}
				else
					$previous[] = array('key' => $strKey);
			}
		}
		
		private function errorHandler($errNo, $error){
			$temp = preg_split("/(GaLogs\.\w+)/", $error, -1, PREG_SPLIT_NO_EMPTY);
			array_walk($temp, create_function('&$val, $key', '$val = "/(" . addcslashes($val, "(^+-.*|/)") . ")/";'));
			$replacedTable = preg_replace($temp, '', $error);
			$errorsToBeReplaced = array(1054 => "#$errNo: " . $error . '. Try to prepend value type _NEW_ or _OLD_ to physical fields.',
										1064 => "#$errNo: You have an error in your SQL syntax!");
			(array_key_exists($errNo, $errorsToBeReplaced))?$s = $errorsToBeReplaced[$errNo]:$s = "#$errNo: $error";
			return $s;
		}
		
		private function findQueriesNotToReplace(&$sql){
			$markers = preg_split('/(<\$\s*)|(\s*\$>)/', $sql, -1, PREG_SPLIT_NO_EMPTY);
			array_shift($markers);
			if(count($markers)%2 != 0)
				throw new GaLogException(GaLogException::UNEQUAL_DOLLAR_MARK, $this->mDebugTrace);
			//array_walk($markers, create_function('$val, $key, &$markers', 'if($key%2 != 0) unset($markers[$key]);'), &$markers);
			//$temp = &$markers;
			foreach($markers as $key => $val){
				if($key%2 != 0) 
					unset($markers[$key]);
			}
			//array_walk($markers, create_function('&$val, $key, &$arr', '$ctr = count($arr); $arr["__#STU_" . $ctr . "#__"]["ORIG"] = $val; $val = \'/(<\$\s*(\' . addcslashes(trim($val),"(^+-.*|/)") . \')\s*\$>)/\'; $arr["__#STU_" . $ctr . "#__"]["PATTERN"] = $val;'), &$this->mStringsNotToBeReplaced);
			//$temp = &$markers;
			foreach($markers as $key => $val){
				$ctr = count($this->mStringsNotToBeReplaced);
				$this->mStringsNotToBeReplaced["__#STU_" . $ctr . "#__"]["ORIG"] = $val;
				$val = '/(<\$\s*(' . addcslashes(trim($val),"(^+-.*|/)") . ')\s*\$>)/';
				$this->mStringsNotToBeReplaced["__#STU_" . $ctr . "#__"]["PATTERN"] = $val;
			}
			//array_walk($this->mStringsNotToBeReplaced, create_function('$val, $key, &$sql', '$sql = preg_replace( $val["PATTERN"], $key, $sql);'), &$sql);
			foreach($this->mStringsNotToBeReplaced as $key => $val){
				$sql = preg_replace( $val["PATTERN"], $key, $sql);
			}
		}
		
		private function temp1($val,$key,&$sql){
			$sql = preg_replace( $val["PATTERN"], $key, $sql);
			echo "$sql<BR>";
		}
		
		private function reconstructStringsNotToBeReplaced(&$sql){
			$temp = preg_split("/[\s\(\)]/", $sql, -1, PREG_SPLIT_NO_EMPTY);
			$stringsNotToBeReplaced = preg_grep("/__#STU_\d+#__/", $temp);
			foreach($stringsNotToBeReplaced as $key)
				$sql = preg_replace('/(' . $key . ')/', $this->mStringsNotToBeReplaced[$key]['ORIG'], $sql);	
		}
	}
?>