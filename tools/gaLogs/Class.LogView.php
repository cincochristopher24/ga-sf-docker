<?php
	/***
	 * @author: Reynaldo Castellano III
	 */
	require_once('gaLogs/Class.LoggerConnection.php');
	class LogView
	{
		private static $instance = null;
		public static function create(){
			if(is_null(self::$instance)){
				self::$instance = new LogView();
			}
			return self::$instance;
		}
		
		private $mDb 					= null;
		private $mLogViewDB				= 'GaLogViews';
		private $mRefreshedLogTables 	= array();
		
		private function __construct(){
			$this->mDb = LoggerConnection::create()->getDbHandler();
		}
		
		public function refresh($logTable){
			/*** Main Table View ***/
			$logStorageTable = $logTable->getLogStorageTable();
			$phyLogFieldNames = $logStorageTable->getActualFieldNames();			
			$logTableStorageName = $logStorageTable->getName();	
			$fieldNames = array();
			foreach($phyLogFieldNames as $iFldName){
				$fieldNames[] = '`GaLogs`.`'.$logTableStorageName.'`.`'.$iFldName.'`';
			}
			//array_shift($fieldNames);
			$viewSql = 	'SELECT ' .
							implode(',',$fieldNames).', ' .
							'tblAffectedRecord.`event` as _EVENT_, ' .
							'tblDmlStatement.sqlString as _SQL_STRING_, ' .
							'tblDmlStatement.command as _COMMAND_, ' .
							'tblLogTrace.executionDate as _EXECUTION_DATE_,' .
							'tblGaLog.gaLogID as _LOG_ID_, ' .
							'tblUserType.userTypeName as _DOER_TYPE_, ' .
							'tblGaLog.doerID as _DOER_ID_, ' .
							'tblGaLog.ipAddress as _IP_ADDRESS_, ' .
							'tblClientDomain.clientDomainName as _DOMAIN_NAME_ ' .
						'FROM ' .
							'GaLogs.'.$logTableStorageName.', ' .
							'GaLogs.tblAffectedRecord, ' .
							'GaLogs.tblDmlStatement, ' .
							'GaLogs.tblLogTrace, '.									
							//'GaLogs.tblTransaction, ' .
							'GaLogs.tblGaLog, ' .
							'GaLogs.tblClientDomain, ' .
							'GaLogs.tblUserType ' .
						'WHERE ' .
							'GaLogs.'.$logTableStorageName.'.affectedRecordID 	= tblAffectedRecord.affectedRecordID ' .
							'AND tblDmlStatement.dmlStatementID 	= tblAffectedRecord.dmlStatementID ' .
							'AND tblGaLog.startingLogTraceID 		= tblLogTrace.logTraceID ' .
							//'AND tblTransaction.transactionID 	= tblDmlStatement.transactionID ' .
							'AND tblGaLog.gaLogID 					= tblDmlStatement.gaLogID ' .
							'AND tblClientDomain.clientDomainID 	= tblGaLog.clientDomainID ' .
							'AND tblUserType.userTypeID 			= tblGaLog.doerTypeID ';
						//'ORDER BY tblGaLog.gaLogID, GaLogs.'.$logTableStorageName.'.affectedRecordID';
			//update the new table view
			$mainViewName =  $this->mLogViewDB.'.`gaLogView_'.$logTable->getLogDbID().'_'.$logTable->getLogTableID().'`';
			//$this->mDb->execute('DROP VIEW IF EXISTS '.$mainViewName.';');				
			$this->mDb->execute('CREATE OR REPLACE VIEW '.$mainViewName.' AS '.$viewSql);
			$this->mRefreshedLogTables[$logTable->getLogTableID()] = $logTable;
		}
		
		public function getInstanceByLogTable($logTable){
			$logDB = $logTable->getLogDb();
			$sql =	'SELECT * ' .
					'FROM `INFORMATION_SCHEMA`.`VIEWS` ' .
					'WHERE TABLE_SCHEMA = "GaLogViews" ' .
					'AND TABLE_NAME LIKE "gaLogView_'.$logDB->getLogDbID().'_'.$logTable->getLogTableID().'"';
			return new iRecordset($this->mDb->execute($sql));
		}
	}
?>