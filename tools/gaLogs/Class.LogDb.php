<?php
	/***
	 * @author: Reynaldo Castellano III
	 */
	
	require_once('gaLogs/Class.GaLogger.php');
	require_once('gaLogs/Class.LogTable.php');
	require_once('gaLogs/exception/Class.GaLogException.php');
	require_once('gaLogs/Class.DBSchema.php');
	require_once('gaLogs/Class.LoggerConnection.php');
	
	class LogDb
	{
		/*** Static Functions ***/
		static private $OBJECT_CACHE_NAME 	= array();
		static private $OBJECT_CACHE_ID		= array();
		
		static private $sthLogDbByDbName = null;
		static public function getLogDbByDbName($name=null){
			if(!array_key_exists($name,self::$OBJECT_CACHE_NAME)){
				$db = LoggerConnection::create()->getDbHandler();
				if(is_null(self::$sthLogDbByDbName)){
					$stmt=	'SELECT `logDbID`,`logDbName` '.
							'FROM `GaLogs`.`tblLogDb` '.
							'WHERE `logDbName` = ?';
					self::$sthLogDbByDbName = $db->prepareStatement($stmt);
				}
				$rs = new iRecordset($db->executeStatement(self::$sthLogDbByDbName,array($name)));	
				if(!$rs->retrieveRecordCount()){
					$logDb = new LogDb();
					$logDb->setLogDbName($name);
					$logDb->save();
				}
				else{
					$logDb = new LogDb(null,$rs->retrieveRow(0));
				}
				self::$OBJECT_CACHE_NAME[$logDb->getLogDbName()] = $logDb;
				self::$OBJECT_CACHE_ID[$logDb->getLogDbID()] = $logDb;
			}
			return self::$OBJECT_CACHE_NAME[$name];
		}
		
		static public function reset(){
			self::$OBJECT_CACHE_NAME = array();
			self::$OBJECT_CACHE_ID = array();
		}
		
		static public function getLogDbByID($id){
			if(!array_key_exists($id,self::$OBJECT_CACHE_ID)){
				try{
					$logDb = new LogDb($id);
					self::$OBJECT_CACHE_NAME[$logDb->getLogDbName()] = $logDb;
					self::$OBJECT_CACHE_ID[$logDb->getLogDbID()] = $logDb;
				}
				catch(exception $e){
					throw $e;
				}
			}
			return self::$OBJECT_CACHE_ID[$id];
		}

		static public function getAllLogDB(){
			$db = LoggerConnection::create()->getDbHandler();
			$rs = new iRecordset($db->execute('SELECT * FROM GaLogs.tblLogDb'));
			$ar = array();
			foreach($rs as $row){
				$ar[] = new LogDb(null,$row);
			}
			return $ar;
		}

		static public function isPhysicalDbExists($dbName=null){
			return DBSchema::isPhysicalDbExists($dbName);
		}
	
		static private $sthConstruct = null;
	
		private $mLogDbID 	= null;
		private $mLogDbName	= null;
		private $mDb		= null;

		private function __construct($param=null,$data=null){
			$this->mDb = LoggerConnection::create()->getDbHandler();
			$this->clearProperties();
			if(!is_null($param)){
				if(is_null(self::$sthConstruct)){
					$sql =	'SELECT `logDbID`,`logDbName` '.
							'FROM `GaLogs`.`tblLogDb` '.
							'WHERE `logDbID` = ? ';
					self::$sthConstruct = $this->mDb->prepareStatement($sql);
				}
				$rs = new iRecordset($this->mDb->executeStatement(self::$sthConstruct,array($param)));
				if($rs->retrieveRecordCount()){
					$this->initProperties($rs->retrieveRow(0));
				}
				else{
					throw new GaLogException(GaLogException::NONE_EXISTING_ENTITY_ID,null,'LogDBID: '.$keyID);
				}
			}
			if(is_array($data)){
				$this->initProperties($data);
			}
		}

		private function clearProperties(){
			$this->setLogDbID(null);
			$this->setLogDbName(null);
		}

		private function initProperties($data){
			$this->setLogDbID($data['logDbID']);
			$this->setLogDbName($data['logDbName']);
		}

		//SETTERS
		private function setLogDbID($param=0){
			$this->mLogDbID=$param;
		}

		public function setLogDbName($param=null){
			$this->mLogDbName=$param;
		}

		//GETTERS
		public function getLogDbID(){
			return $this->mLogDbID;
		}

		public function getLogDbName(){
			return $this->mLogDbName;
		}
		
		public function getLogTableByTableName($tableName=null){
			return LogTable::getLogTableByDbIDAndTableName($this->getLogDbID(),$tableName);
		}
		
		public function getLogTables(){
			return LogTable::getLogTablesByLogDbID($this->getLogDbID());
		}
		
		//CRUDE
		public function save(){
			if(!self::isPhysicalDbExists($this->getLogDbName())){
				throw new GaLogException(GaLogException::NONE_EXISTING_DATABASE);
			}
			/***
			$tempLogDb = LogDb::getLogDbByDbName($this->getLogDbName());
			if(!is_null($tempLogDb) && $tempLogDb->getLogDbID() != $this->getLogDbID()){
				throw new GaLogException(GaLogException::LOGDATABASE_ALREADY_EXISTS);
			}
			***/
			if(is_null($this->getLogDbID())){
				$sql =	'INSERT INTO `GaLogs`.`tblLogDb`('.
							'`logDbName`'.
						')VALUES('.
							$this->mDb->makeSqlSafeString($this->getLogDbName()).
						')';
				$this->mDb->execQuery($sql);
				$this->setLogDbID($this->mDb->getLastInsertedID());
			}
			else{
				$sql =	'UPDATE `GaLogs`.`tblLogDb` SET '.
							'`logDbName`= '.GaString::makeSqlSafe($this->getLogDbName()).' '.
						'WHERE `logDbID` = '.GaString::makeSqlSafe($this->getLogDbID());
				$this->mDb->execQuery($sql);
			}
		}

		public function delete(){
			$sql =	'DELETE FROM `GaLogs`.`tblLogDb` '.
					'WHERE `logDbID` = '.GaString::makeSqlSafe($this->getLogDbID());
			$this->mDb->execQuery($sql);
			$this->clearProperties();
		}
	}
?>