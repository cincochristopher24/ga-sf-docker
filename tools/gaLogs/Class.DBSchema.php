<?php
	/***
	 * @author: Reynaldo Castellano III
	 */
	require_once('gaLogs/Class.LoggerConnection.php');
	class DBSchema
	{
		static $db 	= null;
		static public function initialize(){
			
		}
		
		//static $sthPhysicalTableFieldNames 	= null;
		static public function getPhysicalTableFieldNames($dbName,$tableName){
			/***
			if(is_null(self::$sthPhysicalTableFieldNames)){
				$stmt=	'SELECT COLUMN_NAME as fieldName '.
						'FROM INFORMATION_SCHEMA.COLUMNS '.
						'WHERE TABLE_SCHEMA = ? '.
						'AND TABLE_NAME = ? ' .
						'ORDER BY fieldName';
				self::$sthPhysicalTableFieldNames = self::$db->prepareStatement($stmt);
			}
			$rs = new iRecordset(self::$db->executeStatement(self::$sthPhysicalTableFieldNames, array($dbName,$tableName)));
			***/
			$db = LoggerConnection::create()->getDbHandler();
			try{
				$rs = new iRecordset($db->execute("SHOW COLUMNS FROM `$dbName`.`$tableName`"));
			}
			catch(exception $e){
				return array();
			}
			$fields = array();
			foreach($rs as $row){
				//$fields[] = $row['Field'];
				$fields[] = $row;
			}
			return $fields;
		}
		
		static $sthPhysicalTableInfo = null;
		static public function getPhysicalTableInfo($dbName,$tableName){
			$db = LoggerConnection::create()->getDbHandler();
			if(is_null(self::$sthPhysicalTableInfo)){
				$stmt=	'SELECT TABLE_NAME as tableName, TABLE_SCHEMA as dbName, ENGINE as storageEngine, CREATE_TIME as timeCreated '.
						'FROM INFORMATION_SCHEMA.TABLES '.
						'WHERE TABLE_SCHEMA = ? '.
						'AND TABLE_NAME = ?';
				self::$sthPhysicalTableInfo = $db->prepareStatement($stmt);
			}
			return new iRecordset($db->executeStatement(self::$sthPhysicalTableInfo,array($dbName,$tableName)));
		}
		
		static $sthTableSchemaInfo	= null;
		static public function getTableSchemaInfo($dbName,$tableName){
			$db = LoggerConnection::create()->getDbHandler();
			if(is_null(self::$sthTableSchemaInfo)){
				self::$sthTableSchemaInfo = $db->prepareStatement(
					'SELECT tbl.TABLE_NAME as tableName, tbl.TABLE_SCHEMA as dbName, tbl.ENGINE as storageEngine, tbl.CREATE_TIME as timeCreated, fld.COLUMN_NAME as fieldName '.
					'FROM INFORMATION_SCHEMA.TABLES as tbl, INFORMATION_SCHEMA.COLUMNS as fld '.
					'WHERE  tbl.TABLE_SCHEMA = fld.TABLE_SCHEMA '.
					'AND tbl.TABLE_NAME = fld.TABLE_NAME '.
					'AND tbl.TABLE_SCHEMA = ? '.
					'AND tbl.TABLE_NAME = ? '
				);
			}
			return new iRecordset($db->executeStatement(self::$sthTableSchemaInfo,array($dbName,$tableName)));
		}
		
		static $sthIsPhysicalDbExists = null;
		static public function isPhysicalDbExists($dbName){
			$db = LoggerConnection::create()->getDbHandler();
			if(is_null(self::$sthIsPhysicalDbExists)){
				$stmt=	'SELECT SCHEMA_NAME as dbName '.
						'FROM INFORMATION_SCHEMA.SCHEMATA '.
						'WHERE `SCHEMA_NAME` =  ? ';
				self::$sthIsPhysicalDbExists = $db->prepareStatement($stmt);
			}
			$rs = new iRecordset($db->executeStatement(self::$sthIsPhysicalDbExists,array($dbName)));
			return $rs->retrieveRecordCount() > 0;
		}
		
		static $sthPhysicalTableExists = null;
		static public function isPhysicalTableExists($dbName, $tableName){
			$db = LoggerConnection::create()->getDbHandler();
			if(is_null(self::$sthPhysicalTableExists)){
				$stmt=	'SELECT Table_Name FROM Information_Schema.Tables '.
						'WHERE TABLE_SCHEMA = ? '.
						'AND TABLE_NAME = ? ';
				self::$sthPhysicalTableExists = $db->prepareStatement($stmt);
			}
			$rs = new iRecordset($db->executeStatement(self::$sthPhysicalTableExists,array($dbName,$tableName)));
			return $rs->retrieveRecordCount() > 0;
		}
	}
	DBSchema::initialize();
?>