<?php
	/***
	 * @author: Reynaldo Castellano III
	 */

	require_once('Class.GaDateTime.php');
	require_once('gaLogs/Class.LogDb.php');
	require_once('gaLogs/Class.LogStorageTable.php');
	require_once('gaLogs/Class.LogSubjectTable.php');
	require_once('gaLogs/Class.LogField.php');
	require_once('gaLogs/exception/Class.GaLogException.php');
	require_once('gaLogs/Class.LoggerConnection.php');
	require_once('gaLogs/Class.LogView.php');
	
	class LogTable
	{
		static private $OBJECT_CACHE = array();
		
		static public function create($subjectTable){
			$dbName	= $subjectTable->getDbName();
			$tableName = $subjectTable->getTableName();
			$key = $dbName.'_'.$tableName;
			if(!array_key_exists($key,self::$OBJECT_CACHE)){
				$logTable = self::getLogTableByDbNameAndTableName($dbName,$tableName);
				if(is_null($logTable)){
					//check if the logDb exists, if not create it. And then create the logTable
					$logDb = LogDb::getLogDbByDbName($dbName);
					$logTable = new LogTable();
					$logTable->setLogDbID($logDb->getLogDbID());
					$logTable->setLogTableName($tableName);
					$logTable->save($subjectTable);
				}
				$logTable->initialize($subjectTable);
				self::$OBJECT_CACHE[$key] = $logTable;
				//for optimization purposes, synchronize the logtable only once.
				self::$OBJECT_CACHE[$key]->refresh($subjectTable);
			}
			return self::$OBJECT_CACHE[$key];
		}
		
		static public function getLogTableByDbNameTableName($dbName,$tableName){
			$key = $dbName.'_'.$tableName;
			return array_key_exists($key,self::$OBJECT_CACHE) ? self::$OBJECT_CACHE[$key] : null;
		}
		
		static public function reset(){

			self::$OBJECT_CACHE = array();
		}
		
		static public function getLogTableByDbNameAndTableName($dbname='',$tableName=''){
			$logDb = LogDb::getLogDbByDbName($dbname);
			if(is_null($logDb)){
				return null;
			}
			return $logDb->getLogTableByTableName($tableName);
		}
		
		static private $sthLogTableByDbIDAndTableName = null;
		static public function getLogTableByDbIDAndTableName($dbID=0,$tbName=null){
			$db = LoggerConnection::create()->getDbHandler();
			if(is_null(self::$sthLogTableByDbIDAndTableName)){
				$sql =	'SELECT * '.
						'FROM `GaLogs`.`tblLogTable` '.
						'WHERE `logDbID` =  ? '.
						'AND `logTableName` = ?';
				self::$sthLogTableByDbIDAndTableName = $db->prepareStatement($sql);
			}
			$rs = new iRecordset($db->executeStatement(self::$sthLogTableByDbIDAndTableName,array($dbID,$tbName)));
			return ($rs->retrieveRecordCount() > 0 ? new LogTable(null, $rs->retrieveRow(0)): null);
		}
		
		/*** Class Definition ***/
		
		private $mStorageTable	= null;
		
		private $mLogTableID;
		private $mLogDbID;
		private $mLogTableName;
		private $mStorageEngine;
		private $mTimeCreated;
		private $mSqlSchema;
		private $mDb;
		
		private function __construct($subjectTable=null,$data=null){
			$this->mDb = LoggerConnection::create()->getDbHandler();
			$this->clearProperties();
			if(is_array($data)){
				$this->initProperties($data);
			}
		}
		
		private function initialize($subjectTable){
			$this->mStorageTable = LogStorageTable::getInstanceByLogTable($this,$subjectTable);
		}
		
		public function getFieldNames(){
			return $this->mStorageTable->extractFieldNames();
		}

		private function refreshEntity(){
			$sql =	'SELECT `logTableID`,`logDbID`,`logTableName`,`storageEngine`,`timeCreated`, `sqlSchema` '.
					'FROM `GaLogs`.`tblLogTable` '.
					'WHERE `logTableID` = '.$this->getLogTableID();
			$rs = new iRecordset($this->mDb->execQuery($sql));
			$this->initProperties($rs->retrieveRow(0));
		}
		
		public function refresh($subjectTable=null,$force=false){
			/***
				If the fields are not synchronize, try to refresh the storage tables first before attempting update it.
				Since this is a cached object, other threads might have alread updated the storage table.
			***/
			if((!$this->isSubjectAndStorageSync($subjectTable)) || $force){
				$this->mStorageTable->refreshFields();
				$this->refreshEntity();
				if((!$this->isSubjectAndStorageSync($subjectTable)) || $force){
					//this time update it
					$this->mStorageTable->synchronizeWithSubjectTable($subjectTable);
					//$subjectTable->refreshTriggers();
					//LogView::create()->refresh($this);
					//update the storage engine of the logTable
					$this->setStorageEngine($subjectTable->getStorageEngine());
					$this->setSqlSchema($subjectTable->getSqlSchema());
					$this->save($subjectTable);
				}
			}
		}
		
		private function isSubjectAndStorageSync($subjectTable){
			return ($subjectTable->getSqlSchema() == $this->getSqlSchema());
		}
		
		private function clearProperties(){
			$this->setLogTableID(null);
			$this->setLogDbID(null);
			$this->setLogTableName(null);
			$this->setStorageEngine(null);
			$this->setTimeCreated(null);
			$this->setSqlSchema(null);
		}

		private function initProperties($data){
			$this->setLogTableID($data['logTableID']);
			$this->setLogDbID($data['logDbID']);
			$this->setLogTableName($data['logTableName']);
			$this->setStorageEngine($data['storageEngine']);
			$this->setTimeCreated($data['timeCreated']);
			$this->setSqlSchema($data['sqlSchema']);
		}
		
		//SETTERS
		private function setLogTableID($param=0){
			$this->mLogTableID=$param;
		}

		public function setLogDbID($param=0){
			$this->mLogDbID=$param;
		}

		public function setLogTableName($param=null){
			$this->mLogTableName=$param;
		}

		public function setStorageEngine($param=null){
			$this->mStorageEngine=$param;
		}

		public function setTimeCreated($param=null){
			$this->mTimeCreated=$param;
		}
		
		public function setSqlSchema($param=null){
			$this->mSqlSchema = $param;
		}
		
		//GETTERS
		public function getLogTableID(){
			return $this->mLogTableID;
		}

		public function getLogDbID(){
			return $this->mLogDbID;
		}

		public function getLogTableName(){
			return $this->mLogTableName;
		}

		public function getStorageEngine(){
			return $this->mStorageEngine;
		}

		public function getTimeCreated(){
			return $this->mTimeCreated;
		}
		
		public function getSqlSchema(){
			return $this->mSqlSchema;
		}
		public function getLogDb(){
			return LogDB::getLogDbByID($this->getLogDbID());
		}
		
		public function getLogStorageTable(){
			return LogStorageTable::getInstanceByLogTable($this);
		}
		
		public function refreshTriggers(){
			$subjectTable = $this->getLogSubjectTable();
			if(!is_null($subjectTable)){
				$subjectTable->refreshTriggers();
			}
		}
		
		public function refreshView(){
			LogView::create()->refresh($this);
		}
		
		
		public function saveAuditTrails($logTrails,$dmlStatement){
			$this->mStorageTable->addLogTrails($logTrails,$dmlStatement);
		}
		
		//CRUDE
		public function save($subjectTable){
			//check if the logTable name truly exists.
			if(!DBSchema::isPhysicalTableExists($this->getLogDb()->getLogDbName(), $this->getLogTableName())){
				throw new GaLogException(GaLogException::NONE_EXISTING_TABLE_FOR_DB, $this->mDebugTrace, "DbName: '{$logDB->getLogDbName()}' TbName: '{$this->getLogTableName()}'");
			}
			$this->setTimeCreated($subjectTable->getTimeCreated());
			$this->setStorageEngine($subjectTable->getStorageEngine());
			if(!$this->getLogTableID()){
				$sql =	'INSERT INTO `GaLogs`.`tblLogTable`('.
							'`logDbID`,'.
							'`logTableName`,'.
							'`storageEngine`,'.
							'`timeCreated`, '.
							'`sqlSchema`'.
						')VALUES('.
							$this->mDb->makeSqlSafeString($this->getLogDbID()).','.
							$this->mDb->makeSqlSafeString($this->getLogTableName()).','.
							$this->mDb->makeSqlSafeString($this->getStorageEngine()).','.
							$this->mDb->makeSqlSafeString($this->getTimeCreated()).', '.
							$this->mDb->makeSqlSafeString($this->getSqlSchema()).
						')';
				$this->mDb->execQuery($sql);
				$this->setLogTableID($this->mDb->getLastInsertedID());
			}
			else{
				$sql =	'UPDATE `GaLogs`.`tblLogTable` SET '.
							'`logDbID`= '.$this->mDb->makeSqlSafeString($this->getLogDbID()).','.
							'`logTableName`= '.$this->mDb->makeSqlSafeString($this->getLogTableName()).','.
							'`storageEngine`= '.$this->mDb->makeSqlSafeString($this->getStorageEngine()).','.
							'`timeCreated`= '.$this->mDb->makeSqlSafeString($this->getTimeCreated()).', '.
							'`sqlSchema`= '.$this->mDb->makeSqlSafeString($this->getSqlSchema()).' '.
						'WHERE `logTableID` = '.$this->mDb->makeSqlSafeString($this->getLogTableID());
				$this->mDb->execQuery($sql);
			}
		}

		public function delete(){
			$sql =	'DELETE FROM `GaLogs`.`tblLogTable` '.
					'WHERE `logTableID` = '.$this->mDb->makeSqlSafeString($this->getLogTableID());
			$this->mDb->execQuery($sql);
			$this->clearProperties();
		}
	}
?>