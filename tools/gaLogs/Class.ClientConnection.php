<?php
	/***
	 * @author: Reynaldo Castellano III
	 ***/	
	require_once('gaLogs/Class.GaLogger.php');
	require_once('gaLogs/Class.SqlStatement.php');
	require_once('gaLogs/Class.SqlStatementBuilder.php');
	require_once('gaLogs/Class.DmlPreparedStatement.php');
	require_once('gaLogs/Class.DmlPreparedStatementRegistry.php');
	/*****
	 * A class the represents the client's connection.
	*****/
	
	class ClientConnection
	{
		private $mGaLogger			= null;
		private $mConnID 			= null;
		private $mSqlStatementStack = array();

		private $mLatestSqlStmt		= null;
		private $mDbLoggingIsActive = false;

		private $mCon				= null;
		private $mAdapter 			= null;
		
		private $mDmlPreparedStatements = array();
		
		public function __construct($con, $conID, $adapter){
			$this->mGaLogger = GaLogger::create();
			$this->mCon = $con;
			$this->mConnID = $conID;
			$this->mAdapter = $adapter;
			$this->setUpEnvironment();
		}

		private function setUpEnvironment(){
			$this->mAdapter->setConnectionVars($this->mCon, array('gaLogThreadID'=>LogThread::create()->getLogThreadID()));
		}
		
		private function toggleTrackingMode(){
			$this->mGaLogger->isOkToTrack() ? $this->enableTracking() : $this->disableTracking();
		}
		
		public function enableTracking(){
			if(!$this->mDbLoggingIsActive){
				$this->mAdapter->setConnectionVars($this->mCon, array('isLoggingActive'=>'true'));
				$this->mDbLoggingIsActive = true;
			}
		}
		
		public function disableTracking(){
			if($this->mDbLoggingIsActive){
				$this->mAdapter->setConnectionVars($this->mCon, array('isLoggingActive'=>'false'));
				$this->mDbLoggingIsActive = false;
			}
		}

		public function refresh(){
			$this->setUpEnvironment();
		}
		
		public function getConnectionID(){
			return $this->mConnID;
		}
		
		public function isOpen(){
			return $this->mAdapter->isConnectionOpen($this->mCon);
		}

		/**## Start of Interface for Prepared Statement (Client Verion) ##**/
		public function initPreparedStatement($sql, $stmtObj){
			//we need to initialize an sqlStatement since it is the one who initializes dml tables
			$sqlStatementObj = SqlStatementBuilder::create()->buildSqlStatement($sql);
			if(!is_null($sqlStatementObj)){
				$prepStmt = new DmlPreparedStatement($stmtObj, $sqlStatementObj);
				if($prepStmt->isDmlStatement()){
					DmlPreparedStatementRegistry::getInstance()->register($prepStmt);
				}
				return $prepStmt;
			}
			return null;
		}

		public function analyzePreparedStatement($stmtObj, $boundVars){
			//we have to initialize the connection
			$this->toggleTrackingMode();
			$prepStmtObj = DmlPreparedStatementRegistry::getInstance()->getStatement($stmtObj);
			if(!is_null($prepStmtObj)){
				$newPrepStmtObj = clone $prepStmtObj;
				$newPrepStmtObj->setBoundVars($boundVars);
				$this->mSqlStatementStack[] = new SqlStatement($newPrepStmtObj, false);
			}
		}

		public function trackPreparedStatement($res){
			return $this->mLatestSqlStmt = array_pop($this->mSqlStatementStack);
		}
		
		/**## End of Interface for Prepared Statement (Client Verion) ##**/
		
		public function analyzeQuery($sql){
			$this->toggleTrackingMode();
			$this->mSqlStatementStack[] = ($this->mGaLogger->isOkToTrack() ? SqlStatementBuilder::create()->buildSqlStatement($sql) : null);
		}
		
		public function trackChanges($res){
			$this->mLatestSqlStmt = array_pop($this->mSqlStatementStack);
			if(!is_null($this->mLatestSqlStmt)) $this->mLatestSqlStmt->trackChanges($res);
		}
		
		
		public function isInTransaction(){
			return $this->mAdapter->isInTransaction($this->mCon);
		}
		
		public function isSqlSyntaxOk($sql){
			return $this->mAdapter->isSqlSyntaxOk($this->mCon, $sql);
		}
		
		public function setConnectionVars($vars=array()){
			return $this->mAdapter->setConnectionVars($this->mCon, $vars);
		}
		
		public function getConnectionVars($varNames){
			return $this->mAdapter->getConnectionVars($this->mCon, $varNames);
		}
		
		public function getActiveDatabase(){
			return $this->mAdapter->getActiveDatabase($this->mCon);
		}

		public function registerPreparedDmlStatement($id, $sql){
			$this->mDmlPreparedStatements[$id] = $sql;
		}
		
		public function getDmlPreparedStatement($id){
			return (array_key_exists($id,$this->mDmlPreparedStatements) ? $this->mDmlPreparedStatements[$id] : null);
		}
		
		public function unregisterPreparedDmlStatement($id){
			if(array_key_exists($id,$this->mDmlPreparedStatements)){
				unset($this->mDmlPreparedStatements[$id]);
				return true;
			}
			return false;
		}
		
	}
?>