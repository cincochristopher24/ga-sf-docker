<?php
	/***
	 * @author: Reynaldo Castellano III
	 */
	 
	class ClauseTokenizer
	{
		public static function tokenize($origSql,$clauseRegEx,$clauseModifiers){

			$dummySql = $origSql = trim($origSql);
			$ar = array("/\\\'/",'/\\\"/');

			//replace all escaped characters with asterisk
			$dummySql = preg_replace_callback($ar,create_function('$matches','return str_repeat("~",strlen($matches[0]));'),$dummySql);

			//replace all qouted characters with asterisk
			$dummySql = preg_replace_callback("/'.*?'/",create_function('$matches','return str_repeat("~",strlen($matches[0]));'),$dummySql);

			//replace all double qouted charactes with asterisk
			$dummySql = preg_replace_callback('/".*?"/',create_function('$matches','return str_repeat("~",strlen($matches[0]));'),$dummySql);

			//add space on both sides of string enclosed with () (orginal sql string)
			$dummySql = preg_replace_callback('/\(.*?\)/',create_function('$matches','return " ".$matches[0]." ";'),$dummySql);
			$origSql = preg_replace_callback('/\(.*?\)/',create_function('$matches','return " ".$matches[0]." ";'),$origSql);

			//add spaces on both sides on strings qouted with `` (dummy sql string)
			$dummySql = preg_replace_callback('/`.*?`/',create_function('$matches','return " ".$matches[0]." ";'),$dummySql);
			$origSql = preg_replace_callback('/`.*?`/',create_function('$matches','return " ".$matches[0]." ";'),$origSql);

			//replace all string inside parenthesis but preserving the parenthisis
			$dummySql = preg_replace_callback('/\(.*?\)/',create_function('$matches','return "(".str_repeat("~",strlen($matches[0])-2).")";'),$dummySql);

			//strip out comments
			$arCommentSyntax = array('/\/\*[^!].*?\*\//','/--.*?\n/','/#.*?\n/');
			$origSql = preg_replace($arCommentSyntax,' ',$origSql);
			$dummySql = preg_replace($arCommentSyntax,' ',$dummySql);
			
			//strip out unnecessary characters 
			$origSql = preg_replace('/\s/',' ',$origSql);
			$dummySql = preg_replace('/\s/',' ',$dummySql);

			$OrigSqlLen = strlen($origSql);
			
			//get the positions of each clause from the dummy sql string
			$clausePositions = array();
			foreach($clauseRegEx as $clauseName =>$iRe){
				preg_match_all($iRe, $dummySql, $matches, PREG_OFFSET_CAPTURE);
				$clausePositions[$clauseName] = (isset($matches[0][0][1]) ? $matches[0][0][1]:-1);
			}
			
			//using the positions acquired from the dummy string, extract the exact substring from the original sql string			
			
			$clauseNames = array_keys($clauseRegEx);//contains index => clause
			$clauseNameMap = array_flip($clauseNames);//contains clause => index
			
			$clauses = array();
			foreach($clausePositions as $clauseName => $pos){
				if($pos != -1){
					//determine the boundary
					$endPos = $OrigSqlLen;
					//iterate through the clauses according to the specified order, therefore we need to index the array of clauses
					for($i=$clauseNameMap[$clauseName]+1;$i<count($clauseNameMap);$i++){
						if($clausePositions[$clauseNames[$i]] != -1){
							$endPos = $clausePositions[$clauseNames[$i]];							
							break;	
						}
					}
					//remove the modifiers present in the clause					
					$dumStrClause = trim(substr($dummySql,$pos,$endPos-$pos));
					$strClause = trim(substr($origSql,$pos,$endPos-$pos));
					
					$modifiers = array();
					if(array_key_exists($clauseName,$clauseModifiers)){
						foreach($clauseModifiers[$clauseName] as $modName => $iMod){
							//check if the modifier is in the clause
							$modifiers[$modName] = false;
							preg_match($iMod, $dumStrClause, $match, PREG_OFFSET_CAPTURE);
							if(count($match)){
								$modifiers[$modName] = true;
								$dumStrClause = substr_replace($dumStrClause,'',$match[0][1],strlen($match[0][0]));
								$strClause = substr_replace($strClause,'',$match[0][1],strlen($match[0][0]));
							}
						}
					}
					$token = $strClause;
				}
				else{
					$token = '';
					$modifiers = array();
				}
				
				$clauses[$clauseName] = array(
					'token' 	=> $token,
					'modifiers' => $modifiers
				);			
			}
			return $clauses;
		}
	}
?>