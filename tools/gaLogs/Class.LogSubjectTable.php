<?php
	/***
	 * @author: Reynaldo Castellano III
	 */
	require_once('gaLogs/Class.DBSchema.php');
	require_once('gaLogs/Class.LoggerConnection.php');
	require_once('gaLogs/Class.Trigger.php');
	require_once('gaLogs/Class.LogView.php');
	
	class LogSubjectTable
	{
		static private $OBJECT_CACHE = array();
		static public function create($dbName,$tableName){
			$key = $dbName.'_'.$tableName;
			if(!array_key_exists($key,self::$OBJECT_CACHE)){
				$db = LoggerConnection::create()->getDbHandler();
				try{
					$rsTableInfo = new iRecordset($db->execute("SHOW CREATE TABLE `$dbName`.`$tableName`;"));
				}
				catch(exception $e){
					return null;
				}
				self::$OBJECT_CACHE[$key] = new LogSubjectTable($dbName,$tableName,$rsTableInfo->{'getCreate Table'}(0));
			}
			return self::$OBJECT_CACHE[$key];
		}
		
		static public function getAllExistingLogSubjectTables(){
			$db = LoggerConnection::create()->getDbHandler();
			$rs = new iRecordset($db->execute('
				SELECT tblLogDb.logDbName, tblLogTable.logTableName
				FROM tblLogDb, tblLogTable
				WHERE tblLogDb.logDbID = tblLogTable.logDbID
			'));
			$instances = array();
			foreach($rs as $row){
				$instances[] = self::create($row['logDbName'],$row['logTableName']);
			}
			return $instances;
		}
		
		static public function reset(){
			self::$OBJECT_CACHE = array();
		}
		
		private $mDbName		= null;
		private $mTableName		= null;
		private $mTimeCreated 	= null;
		private $mStorageEngine	= null;		
		private $mFields	 	= null;
		private $mSqlSchema		= null;
		
		private $mDb			= null;
				
		private function __construct($dbName,$tableName, $sqlSchema){
			//remove the auto increment info
			$sqlSchema = preg_replace('/[^`]AUTO_INCREMENT=.*?\s/',' ',$sqlSchema);
			/***
			$this->mDb = LoggerConnection::create()->getDbHandler();
			$this->mTableName 	= $subjectInfo->getTableName();
			$this->mDbName 		= $subjectInfo->getDbName();
			//get the field names and logTableInfo
			$this->mTimeCreated 	= $subjectInfo->getTimeCreated();
			$this->mStorageEngine 	= strtolower($subjectInfo->getStorageEngine());
			$this->mFieldNames = DBSchema::getPhysicalTableFieldNames($this->mDbName,$this->mTableName);
			$rsSqlSchema = new iRecordset($this->mDb->execute("
				SHOW CREATE TABLE `{$this->mDbName}`.`{$this->mTableName}`;
			"));
			//remove the auto increment text
			$sqlSchema = $rsSqlSchema->{'getCreate Table'}(0);			
			$this->mSqlSchema = $rsSqlSchema->{'getCreate Table'}(0);
			***/
			
			/***
			//remove the auto increment info
			$sqlSchema = preg_replace('/[^`]AUTO_INCREMENT=.*?\s/',' ',$sqlSchema);
			//parse the fields names and storage engine
			preg_match_all('/\n\s*?`.*?`/', $sqlSchema, $fields);
			foreach($fields[0] as $iFieldName){
				$iFieldName = trim($iFieldName);
				$fieldNameLen = strlen($iFieldName);
				$this->mFieldNames[] = substr($iFieldName,1,$fieldNameLen-2);
			}
			
			//get the storage engine info
			preg_match_all('/[^`]ENGINE=.*?\s/', $sqlSchema, $engineInfo);
			$arEngine = explode('=',trim($engineInfo[0][0]));
			$this->mStorageEngine = $arEngine[1];
			***/
			$this->mDb = LoggerConnection::create()->getDbHandler();
			$this->mDbName 		= $dbName;
			$this->mTableName 	= $tableName;
			$this->mSqlSchema	= $sqlSchema;
		}
		
		public function getDbName(){
			return $this->mDbName;
		}
		
		public function getTableName(){
			return $this->mTableName;
		}
		
		public function getTimeCreated(){
			return $this->mTimeCreated;
		}
		
		public function getFields(){
			if(is_null($this->mFields)){
				$this->mFields = DBSchema::getPhysicalTableFieldNames($this->mDbName,$this->mTableName);
			}
			return $this->mFields;
		}
		
		public function getFieldNames(){
			$fieldNames = array();
			$fields = $this->getFields();
			foreach($fields as $iField){
				$fieldNames[] = $iField['Field'];
			}
			return $fieldNames;
		}
		
		public function getStorageEngine(){
			//get the storage engine from the sqlSchema
			if(is_null($this->mStorageEngine)){
				preg_match_all('/[^`]ENGINE=.*?\s/', $this->mSqlSchema, $engineInfo);
				$arEngine = explode('=',trim($engineInfo[0][0]));
				$this->mStorageEngine = $arEngine[1];
			}
			return $this->mStorageEngine;
		}
		
		public function getSqlSchema(){
			return $this->mSqlSchema;
		}
		
		public function getTriggers(){
			return Trigger::getInstances($this);
		}
		
		public function refreshTriggers(){
			Trigger::refreshTriggers($this);
		}

	}
?>