<?php
	class ConnectionAdapterManager
	{
		static private $instance = null;
		static public function getInstance(){
			if(is_null(self::$instance)){
				self::$instance = new self;
			}
			return self::$instance;
		}
		
		/** For test **/
		static public function reset(){
			self::$instance = null;
		}
		
		private $mAdapters = array();
		
		private function __construct(){
			$dir = dirname(__FILE__).'/connectionAdapters';
			$files = scandir($dir);
			foreach($files as $iFile){
				//check if it is a php file
				if('Adapter.php' == substr($iFile,strlen($iFile)-11, 11)){
					$clsName = str_replace(array('Class.','.php'),'',$iFile);
					require_once($dir.'/'.$iFile);
					$this->mAdapters[] = new $clsName;
				}
				/***
				$adapterFolder = $dir.'/'.$iFile;
				if(is_dir($adapterFolder)){
					$adapterFileName = 'Class.'.ucfirst($iFile).'Adapter.php';
					$adapterFilePath = $adapterFolder.'/'.$adapterFileName;
					if(file_exists($adapterFilePath)){
						require_once($adapterFilePath);
						$clsName = ucfirst($iFile).'Adapter';
						$this->mAdapters[] = new $clsName;
					}
				}
				***/
			}
		}

		public function getAdapter($con){
			foreach($this->mAdapters as $iAdapter){
				if($iAdapter->connect($con)){
					return $iAdapter;
				}
			}
			throw new exception('No Adapter found for database connection!');
		}		
	}
?>