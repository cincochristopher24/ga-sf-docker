<?php
	/***
	 * @author: Reynaldo Castellano III
	 */
	require_once('gaLogs/Class.LoggerConnection.php');
	class ClientDomain
	{
		private $mClientDomainID;
		private $mClientDomainName;
		private $mIsLoggingEnabled;
		private $mDb;

		public function __construct($param=null,$data=null){
			$this->mDb = LoggerConnection::create()->getDbHandler();
			$this->clearProperties();
			if(!is_null($param)){
				$sql =	'SELECT `clientDomainID`,`clientDomainName`,`isLoggingEnabled` '.
						'FROM `GaLogs`.`tblClientDomain` '.
						'WHERE `clientDomainID` = '.$this->mDb->makeSqlSafeString($param);
				$rs = new iRecordset($this->mDb->execQuery($sql));
				if($rs->retrieveRecordCount()){
					$this->initProperties($rs->retrieveRow(0));
				}
				else{
					throw new exception('Invalid clientDomainID '.$param.' passed to object of type ClientDomain.');
				}
			}
			if(is_array($data)){
				$this->initProperties($data);
			}
		}

		private function clearProperties(){
			$this->setClientDomainID(null);
			$this->setClientDomainName(null);
			$this->setIsLoggingEnabled(null);
		}

		private function initProperties($data){
			$this->setClientDomainID($data['clientDomainID']);
			$this->setClientDomainName($data['clientDomainName']);
			$this->setIsLoggingEnabled($data['isLoggingEnabled']);
		}

		//SETTERS
		private function setClientDomainID($param=0){
			$this->mClientDomainID=$param;
		}

		public function setClientDomainName($param=null){
			$this->mClientDomainName=$param;
		}

		public function setIsLoggingEnabled($param=0){
			$this->mIsLoggingEnabled=$param;
		}

		//GETTERS
		public function getClientDomainID(){
			return $this->mClientDomainID;
		}

		public function getClientDomainName(){
			return $this->mClientDomainName;
		}

		public function getIsLoggingEnabled(){
			return $this->mIsLoggingEnabled;
		}

		//CRUDE
		public function save(){
			if(!$this->getClientDomainID()){
				$sql =	'INSERT INTO `GaLogs`.`tblClientDomain`('.
							'`clientDomainName`,'.
							'`isLoggingEnabled`'.
						')VALUES('.
							$this->mDb->makeSqlSafeString($this->getClientDomainName()).','.
							$this->mDb->makeSqlSafeString($this->getIsLoggingEnabled()).
						')';
				$this->mDb->execQuery($sql);
				$this->setClientDomainID($this->mDb->getLastInsertedID());
			}
			else{
				$sql =	'UPDATE `GaLogs`.`tblClientDomain` SET '.
							'`clientDomainName`= '.$this->mDb->makeSqlSafeString($this->getClientDomainName()).','.
							'`isLoggingEnabled`= '.$this->mDb->makeSqlSafeString($this->getIsLoggingEnabled()).' '.
						'WHERE `clientDomainID` = '.$this->mDb->makeSqlSafeString($this->getClientDomainID());
				$this->mDb->execQuery($sql);
			}
		}

		public function delete(){
			$sql =	'DELETE FROM `GaLogs`.`tblClientDomain` '.
					'WHERE `clientDomainID` = '.$this->mDb->makeSqlSafeString($this->getClientDomainID());
			$this->mDb->execQuery($sql);
			$this->clearProperties();
		}
		
		public function isLoggingEnabled(){			
			return 1 == $this->getIsLoggingEnabled();
		}

		public function getUserTypes(){
			return UserType::getUserTypesByClientDomainID($this->getClientDomainID());
		}

		public function isValidUserType($userTypeName){
			return !is_null(UserType::getUserTypeByClientDomainIDAndUserTypeName($this->getClientDomainID(),$userTypeName));
		}

		public function getUserTypeByUserTypeName($userTypeName){
			return UserType::getUserTypeByClientDomainIDAndUserTypeName($this->getClientDomainID(),$userTypeName);
		}

		public static function getClientDomainByDomainName($name=null){
			$db = LoggerConnection::create()->getDbHandler();
			$sql =	'SELECT `clientDomainID`,`clientDomainName`, `isLoggingEnabled` '.
					'FROM `GaLogs`.`tblClientDomain` '.
					'WHERE `clientDomainName` = '.$db->makeSqlSafeString($name);
			$rs = new iRecordset($db->execute($sql));
			return ( $rs->retrieveRecordCount() > 0 ? new ClientDomain(null,$rs->retrieveRow(0)) : null ); 
		}
	}
?>