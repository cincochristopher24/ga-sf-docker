<?php
	/***
	 * @author: Reynaldo Castellano III
	 */
	require_once('gaLogs/Class.DBSchema.php');
	require_once('gaLogs/Class.LogTable.php');
	require_once('gaLogs/Class.LogSubjectTable.php');
	require_once('gaLogs/Class.LogTrailTable.php');
	
	final class DmlTable
	{
		/**
		 * returns a dmlTable object on success, else NULL.
		 */
		static public function create($tblName='',$sqlStatement){
			//we do not track temporary tables
			//if there is a db name in the tablename, use it instead of the default db
			$tableNameParts = explode('.',$tblName);
			if(count($tableNameParts) > 1){
				$tableName 	= trim($tableNameParts[1]);
				$dbName 	= trim($tableNameParts[0]);
			}
			else{
				$tableName 	= trim($tblName);
				$dbName 	= DataTracker::create()->getActiveClientConnection()->getActiveDatabase();
			}
			//remove the unnecessary charaters like ` and space
			$tableName 	= str_replace(array(' ','`'),'',$tableName);
			$dbName		= str_replace(array(' ','`'),'',$dbName);
			$subjectTable = LogSubjectTable::create($dbName,$tableName);
			if(!is_null($subjectTable) && LogTrailTable::isSupportedEngine($subjectTable->getStorageEngine())){
				return new DmlTable($subjectTable,$sqlStatement);
			}
			return null;
		}
		
		private $mSubjectTable		= null;
		private $mLogTable 			= null;
		private $mSqlStatement 		= null;
		private $mLogTrails			= array();

		private function __construct($subjectTable,$sqlStatement){
			$this->mSubjectTable	= $subjectTable;
			$this->mLogTable 		= LogTable::create($this->mSubjectTable);
			$this->mSqlStatement 	= $sqlStatement;
			
			
			
			/***
				Check if the logSubjectTable and the logTable is sync. If not,
					- Refresh the storage table.
					- refresh the view
					- refresh the trigger
					- refresh the logTable
			
				$isSync = $this->mLogTable->getStorageEngine() == $this->getLogSubjectTable()->getStorageEngine();;
				if($isSync){
					//check if the fields are synchronize
					$logTablesFields = $this->mLogTable->getLogFields();
					$logTableFieldNames = array();
					foreach($logTablesFields as $iLogTableField){
						$logTableFieldNames[] = $iLogTableField->getLogFieldName();
					}
					sort($logTableFieldNames);
					$subjectTableFieldNames = $this->mLogSubjectTable->getFieldNames();
					sort($subjectTableFieldNames);

					$isFieldsSync = false;
					if(count($subjectTableFieldNames) == count($logTableFieldNames)){
						$isFieldsSync = true;
						for($i=0;$i<count($subjectTableFieldNames);$i++){
							if($subjectTableFieldNames[$i] != $logTableFieldNames[$i]){
								$isFieldsSync = false;
								break;
							}
						}
					}
					$isSync = $isFieldsSync;				
				}
				if(!$isSync){
					$this->mLogStorageTable->refresh();
				}
			***/
			
		}
		
		
		public function getLogTable(){
			return $this->mLogTable;
		}
		
		public function getSqlStatement(){
			return $this->mSqlStatement;
		}
		
		public function auditTrails(){
			$this->mLogTrails = LogTrailTable::create($this->mLogTable->getStorageEngine())->getLogTrails($this);
			return count($this->mLogTrails) > 0;
		}
		
		public function saveAuditTrails($dmlStatement){
			if(count($this->mLogTrails)){
				$this->mLogTable->saveAuditTrails($this->mLogTrails,$dmlStatement);
			}
		}
	}
?>