<?php
	class MyIsamLogTrailTable extends AbstractLogTrailTable
	{
		public function getStorageEngineType(){
			return 'myisam';
		}		
		public function getLogTrailTableName(){
			return '`GaLogs`.`tblMyIsamLogTrail`';
		}
	}
?>