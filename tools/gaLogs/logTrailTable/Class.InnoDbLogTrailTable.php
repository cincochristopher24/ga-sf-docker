<?php
	require_once('gaLogs/Class.ClientConnectionManager.php');
	class InnoDbLogTrailTable extends AbstractLogTrailTable
	{		
		public function purgeThreadTrails($threadID){
			//check if there are still hanging transactions. At this point we can assume that all transactions are closed.
			//But then again we can never be sure. If there are is one client connection with an open transaction,
			//abort the purging process as it would leave us hanging, waiting for it to finish which is very unlikely to 
			//happen since we are already shutting down. (We will just have to wait for the system purging to do the job for us.)
			$clientConnections = ClientConnectionManager::create()->getAllClientConnections();
			$withHangingTransaction = false;
			foreach($clientConnections as $iClientConnection){
				if($iClientConnection->isInTransaction()){
					$withHangingTransaction = true;
					break;
				}
			}
			if(!$withHangingTransaction){
				parent::purgeThreadTrails($threadID);
			}
		}
		
		public function getStorageEngineType(){
			return 'innodb';
		}
		
		public function getLogTrailTableName(){
			return '`GaLogs`.`tblInnoDbLogTrail`';
		}
		
	}
?>