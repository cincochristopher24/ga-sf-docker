<?php
	/***
	 * @author: Reynaldo Castellano III
	 */
	//require_once('gaLogs/Class.Client_Trace.php');
	class GaLogException extends Exception
	{		
		const LOGGER_ALREADY_STARTED				= 101;
		const UNREGISTERED_LOGDOMAIN				= 102;
		const INVALID_LOGGER_TYPE					= 103;
		const INVALID_LOGGERID						= 104;				
		const NO_OPENED_NO_TRACKING_SECTION			= 105;
		
		//data model exceptions
		const NONE_EXISTING_DATABASE				= 201;
		const INVALID_DATABASE						= 202;
		const NONE_EXISTING_TABLE_FOR_DB			= 203;
		const STORAGE_TABLE_ALREADY_EXISTS			= 204;
		const NONE_PERSISTENT_LOGTABLE				= 205;
		const NONE_EXISTING_SUBJECT_TABLE			= 206;
		const CONFLICT_SUBJECT_LOG_TABLES			= 207;
		const LOGDATABASE_ALREADY_EXISTS			= 208;
		const NONE_EXISTING_ENTITY_ID				= 209;
		const UNVERIFIABLE_SUBJECT_TABLE			= 210;
		
		// reader exception
		const INVALID_TABLE_REFERENCE_FORMAT		= 301;
		const NON_SELECT_SQL						= 302;	
		const UNEQUAL_PARENTHESES					= 303;
		const UNEQUAL_DOLLAR_MARK					= 304;
		
		// sql exception
		const MYSQL_ERROR_							= 010;
		
		private static $descriptions = array(
			self::LOGGER_ALREADY_STARTED			=> 'GaLogger was already started.',
			self::UNREGISTERED_LOGDOMAIN			=> 'GaLogger encountered an unregistered Log Domain.',
			self::INVALID_LOGGER_TYPE				=> 'GaLogger encountered an invalid User Type in Client Domain.',
			self::INVALID_LOGGERID					=> 'GaLogger encountered an invalid LoggerID.',
			self::NO_OPENED_NO_TRACKING_SECTION		=> 'Trying to invoke a method that requires an open NoTrackingSection.',			
			
			self::NONE_EXISTING_DATABASE			=> 'Operation requires an existing database.',
			self::INVALID_DATABASE					=> 'Invalid database name.',
			self::NONE_EXISTING_TABLE_FOR_DB		=> 'Operation requires an existing table.',
			self::STORAGE_TABLE_ALREADY_EXISTS		=> 'The storage table already exists.',
			self::NONE_PERSISTENT_LOGTABLE			=> 'Trying to invoke a method that requires and existing LogTable.',
			self::NONE_EXISTING_SUBJECT_TABLE		=> 'The subject table does not exist in the database.',
			self::CONFLICT_SUBJECT_LOG_TABLES		=> 'The subject and log tables are not synchronized. It might be that the table has been removed and then readded but with different columns.',
			self::LOGDATABASE_ALREADY_EXISTS		=> 'The LogTable already exists in the GaLog database.',
			self::NONE_EXISTING_ENTITY_ID			=> 'Trying to intantiate an object with an invalid ID.',
			self::UNVERIFIABLE_SUBJECT_TABLE		=> 'The Subject Table could not be verified.', 
			
			self::INVALID_TABLE_REFERENCE_FORMAT	=> "Table references should be of format 'Database_Name'.'Table_Name'.",
			self::NON_SELECT_SQL					=> 'Query must be a select statement.',
			self::UNEQUAL_PARENTHESES				=> 'Uneven number of parentheses.',
			self::UNEQUAL_DOLLAR_MARK				=> 'Uneven number of \'<$\' and \'$>\'.',
			
			self::MYSQL_ERROR_						=> 'MYSQL ERROR'
		);
		
		private $mErrCode = 0;
		
		public function __construct($code,$trace=null,$desc=null){
			//$trace = is_null($trace) ? Client_Trace::create()->getCurrentTrace() : $trace;
			$msg = (is_array($trace)
					? self::$descriptions[$code].' <<< Error occured in FILE: '.$trace['file'].' on LINE: '.$trace['line'].' >>> '
					: self::$descriptions[$code]);
			if(!is_null($desc)){
				$msg.= $desc;
			}
			parent::__construct($msg,$code);
			$this->mErrCode = $code;
		}
		
	 	public function __toString() {
        	return __CLASS__ . ' Error '.$this->mErrCode.': '.$this->getMessage();
    	}		
	}
?>