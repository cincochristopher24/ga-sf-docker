<?php
	require_once('gaLogs/Class.LoggerConnection.php');
	require_once('gaLogs/Class.LogSubjectTable.php');
	class GaLogReader
	{
		static private $instance = null;
		static public function getInstance(){
			if(is_null(self::$instance)) self::$instance = new self();
			return self::$instance;
		}		
		
		private $mDb = null;
		private function __construct(){
			$this->mDb = LoggerConnection::create()->getDbHandler();
		}
		
		public function execute($origSql=''){
			//temporarily replace all charaters that are enclosed with single or double qoutes with ~.
			$dummySql = $origSql = trim($origSql);
			$ar = array("/\\\'/",'/\\\"/');
			$dummySql = preg_replace_callback($ar,create_function('$matches','return str_repeat("~",strlen($matches[0]));'),$dummySql);
			//replace all qouted characters with asterisk
			$dummySql = preg_replace_callback("/'.*?'/",create_function('$matches','return str_repeat("~",strlen($matches[0]));'),$dummySql);
			//replace all double qouted charactes with asterisk
			$dummySql = preg_replace_callback('/".*?"/',create_function('$matches','return str_repeat("~",strlen($matches[0]));'),$dummySql);		
			//add space on both sides of string enclosed with () (orginal sql string)
			$dummySql = preg_replace_callback('/\(.*?\)/',create_function('$matches','return " ".$matches[0]." ";'),$dummySql);
			$origSql = preg_replace_callback('/\(.*?\)/',create_function('$matches','return " ".$matches[0]." ";'),$origSql);
			
			//add spaces on both sides on strings qouted with `` (dummy sql string)			
			//$dummySql = preg_replace_callback('/`.*?`/',create_function('$matches','return " ".$matches[0]." ";'),$dummySql);
			
			//check the string and look of the string that are enclosed with square brackets
			preg_match_all('/\[.*?\]/', $dummySql, $entities, PREG_OFFSET_CAPTURE);
			$offset = 0;
			foreach($entities[0] as $iEntity){
				//remove the brackets
				//var_dump($iEntity[0]);
				$entity = str_replace(array('[',']'),'',$iEntity[0]);
				$entityParts = explode('.',$entity);
				//if the entity parts count is 1 => consider it as a field name.
				//if the entity parts count is 2 => consider it as a combination of dbName.tbName.
				//if the entity parts count is 3 => consider it as a combination of dbName.tbName.fieldName.
				//var_dump($entityParts);
				$partCount = count($entityParts);
				$tableNameAlias = $entity;
				if($partCount >= 2){
					$tableNameAlias = $this->getTableReference($entityParts[0],$entityParts[1]);
					if(3==$partCount){
						$tableNameAlias .= '.'.$entityParts[2];
					}
				}
				//replace the tableNames with the table alias
				$origSql = substr_replace($origSql,$tableNameAlias,$iEntity[1]+$offset,strlen($iEntity[0]));
				$entityLen 	= strlen($iEntity[0]);
				$tbAliasLen = strlen($tableNameAlias);
				$offset = ($tbAliasLen > $entityLen ? $offset + ($tbAliasLen - $entityLen) : $offset - ($entityLen - $tbAliasLen));
			}
			return new iRecordset($this->mDb->execute($origSql));
		}

		public function getTableReference($dbName,$tableName){
			$subjectTable = LogSubjectTable::create($dbName,$tableName);
			if(is_null($subjectTable)){
				throw new exception('Invalid Table Name: '.$dbName.'.'.$tableName);
			}
			$logTable = LogTable::create($subjectTable);
			return '`GaLogViews`.`gaLogView_'.$logTable->getLogDbID().'_'.$logTable->getLogTableID().'`';
		}
		
	}
?>