<?php
	class GaLoggerPropelPDOStatement extends PDOStatement {		
		private $mPDO = null;
		private $mBoundVars = array();
		private $mStmtID = null;
		
		protected function __construct(PDO $pdo){
			$this->mPDO = $pdo;
		}
		
		/**
		 * return a unique id for the statement
		 */
		public function getStatementID(){
			if(is_null($this->mStmtID)){
				$this->mStmtID = DmlPreparedStatementRegistry::getInstance()->generateID();
			}
			return $this->mStmtID;
		}
		
		/**
		 * Overridden for data tracking
		 * @return     int
		 */
		public function execute($input_parameters = null)
		{
			if(!is_null($input_parameters)){
				$this->mBoundVars = $input_parameters;
			}
			DataTracker::create()->analyzePreparedStatement($this, $this->mPDO, $this->mBoundVars);
			$res = parent::execute($input_parameters);
			DataTracker::create()->trackPreparedStatement($res);
			return $res;
		}

		/**
		 * Overridden for variable tracking
		 */
		public function bindValue($pos, $value, $type = PDO::PARAM_STR)
		{
			$this->mBoundVars[$pos] = $value;
			return parent::bindValue($pos, $value, $type);
		}
		
		/**
		 * Overridden for variable tracking	
		**/
		public function bindParam($paramNo, &$param, $type = PDO::PARAM_STR, $maxlen=null, $driverData=array()){
			$this->mBoundVars[$paramNo] = $param;
			return parent::bindParam($paramNo, $param, $type, $maxlen, $driverData);
		}
		
	}	
?>