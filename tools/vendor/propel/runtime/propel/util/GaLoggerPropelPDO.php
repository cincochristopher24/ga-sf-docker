<?php
	require_once('gaLogs/Class.DataTracker.php');
	require_once('PropelPDO.php');
	require_once('GaLoggerPropelPDOStatement.php');
	
	class GaLoggerPropelPDO extends PropelPDO
	{
		public function __construct($dsn, $username = null, $password = null, $driver_options = array())
		{
			parent::__construct($dsn, $username, $password, $driver_options);
			$this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			if (!$this->getAttribute(PDO::ATTR_PERSISTENT)){
				$this->setAttribute(PDO::ATTR_STATEMENT_CLASS, array('GaLoggerPropelPDOStatement', array($this)));
			}
		}
		
		/***
			Overriden to let the DataTracker analyze the sql string to be executed
		***/
		public function exec($sql)
		{
			DataTracker::create()->analyzeQuery($sql,$this);
			$res = parent::exec($sql);
			DataTracker::create()->trackChanges($res);
			return $res;
		}
		
		/***
			Overriden to let the DataTracker analyze the sql string to be prepared
		***/
		public function prepare($statement,$options = array())
		{
			$stmtObj = parent::prepare($statement, $options);
			DataTracker::create()->initPreparedStatement($statement, $stmtObj, $this);
			return $stmtObj;
		}
		
		public function query(){
			$args = func_get_args();
			if(isset($args[0])){ DataTracker::create()->analyzeQuery($args[0],$this); }
			$res = call_user_func_array(array($this, 'parent::query'), $args);
			if(isset($args[0])){ DataTracker::create()->trackChanges($res);	}
			return $res;
		}
	}
?>