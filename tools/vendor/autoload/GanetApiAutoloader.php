<?php
	
	/**
	* @author: acgvelarde
	* this class will register an spl_autoload callback function
	* this can also reload the list of classes to be autoloaded
	**/
	if( !defined('PROJECT_ROOT') )
		define('PROJECT_ROOT', realpath(dirname(dirname(dirname(dirname(__FILE__))))));
	require_once('GanetApiAutoloadList.php');
	
	class GanetApiAutoloader {
		
		private $classes			= array();
		private static $ignores 	= array('.svn', '_svn', 'CVS', '_darcs', '.arch-params', '.monotone', '.bzr', '.git', '.hg', '.', '..');
		private static $instance 	= null;
		private static $registered	= false;
		
		private function __construct(){}
		
		static public function getInstance(){
			if( is_null(self::$instance) ) self::$instance = new self;
			return self::$instance;
		}
		
		/**
		* This is the spl_autoload callback function that will be registered by this class
		* @param String className
		**/
		public function autoload($className){
			
			$classList = GanetApiAutoloadList::getList();
			
			//if( class_exists($className, false) )
			//	return true;
			
			if( array_key_exists($className,$classList) ){
				require_once PROJECT_ROOT.DIRECTORY_SEPARATOR.$classList[$className];
				return true;
			}
			else
				return false;
		}
		
		/**
		* Check if the loader already registered an spl_autoload callback function
		**/
		public function isRegistered(){
			return self::isRegistered();
		}
		
		/**
		* register autoload callback
		**/
		public function register(){
			if (self::$registered)
		      return;

		    ini_set('unserialize_callback_func', 'spl_autoload_call');
		    if (false === spl_autoload_register(array(self::getInstance(), 'autoload'))){
		      throw new exception('Failed to register autoload function');
		    }
		    self::$registered = true;
		}
		
		public function unregister(){
			spl_autoload_unregister(array(self::getInstance(), 'autoload'));
		    self::$registered = false;
		}
		
		/**
		* reload list of classes to be autoloaded based on autoloadfolders.ini
		**/
		public function reloadList(){
			$this->classes 	= array();
			$folders 		= parse_ini_file('ganetapiautoload.ini', true);
			
			foreach( $folders as $folderName => $config ){
				
				//echo "==== FOLDER NAME: $folderName ====\n";
				
				$folderName 		= preg_replace("/\./",DIRECTORY_SEPARATOR, $folderName);
				$folderPath			= PROJECT_ROOT.DIRECTORY_SEPARATOR.$folderName;
				$excludedFolders 	= array_key_exists('excluded_folders', $config) && is_array($config['excluded_folders']) ? $config['excluded_folders'] : array();
				$excludedFiles	 	= array_key_exists('excluded_files', $config) && is_array($config['excluded_files']) ? $config['excluded_files'] : array();
				$isRecursive		= array_key_exists('recursive', $config) ? $config['recursive'] : false;
				$skip				= array_key_exists('skipEntireFolder', $config) ? $config['skipEntireFolder'] : false;
				
				if( $skip )
					continue;
				
				$this->recurse($folderName, scandir($folderPath), array(
					'isRecursive'		=> $isRecursive,
					'excludedFolders'	=> $excludedFolders,
					'excludedFiles'		=> $excludedFiles
				));
			}
			// make list
			$autoloadListFile 	= dirname(__FILE__).DIRECTORY_SEPARATOR.'GanetApiAutoloadList.php';
			$templateFile		= dirname(__FILE__).DIRECTORY_SEPARATOR.'ganetapitemplate';
			
			// load template file
			$templateContents	= file_get_contents($templateFile);
			$templateContents	= preg_replace('/\[@generate_date\]/', date('D F d, Y h:i a'), $templateContents);
			
			// replace template contents with list of files to be loaded
			$contents 			= preg_replace('/private static \$classes = array *\(.*?\)/s', 'private static $classes = '.var_export($this->classes, true), $templateContents);
			// replace contentes of $autoloadListFile
			file_put_contents($autoloadListFile, $contents);
		}
		
		/**
		* @param String $folderPath current directory path where current iteration will take place
		* @param Array $files contents of current directory
		* @param Array $options
		* Recursively iterate through all subdirectories depending on options
		**/
		private function recurse($folderPath, $files, $options = array()){
			$default = array(
				'isRecursive'		=> array(),
				'excludedFolders'	=> array(),
				'excludedFiles'		=> array()
			);
			$options = array_merge($default, $options);
			
			foreach( $files as $fileName ){
				
				$currentPath = PROJECT_ROOT.DIRECTORY_SEPARATOR .$folderPath.DIRECTORY_SEPARATOR.$fileName;
				
				// if file name belongs to ignore list, continue to next item
				if( in_array($fileName, self::$ignores) || in_array($fileName, $options['excludedFiles']) )
					continue;
					
				if( preg_match('/^_/', $fileName) || preg_match('/^\./', $fileName) || preg_match('/^tpl\./i', $fileName) )
					continue;
				
				// if we this is a directory and we are to scan all subdirs, then scan dir this subdirectory
				if( is_dir($currentPath) && $options['isRecursive'] && !in_array($fileName, $options['excludedFolders'])){
					$this->recurse($folderPath.DIRECTORY_SEPARATOR.$fileName, scandir($currentPath), $options );
				}
				// symfony class files
				elseif (preg_match('/\.class\.php$/', $fileName)) {
					$className = trim(preg_replace('/\.class\.php$/', '', $fileName));
					$this->classes[$className] = $folderPath.DIRECTORY_SEPARATOR.$fileName;
				}
				// regular php file
				elseif( preg_match('/\.php$/', $fileName ) ){
					$className = trim(preg_replace(array('/^class\./i', '/\.php$/'), '', $fileName ));
					$this->classes[$className] = $folderPath.DIRECTORY_SEPARATOR.$fileName;
				}
				
			}
		}
		
		/**
		 * Returns the project root path.
		 * 
		 * @return	string	the project root path
		 */
		function getProjectRootPath(){
			return PROJECT_ROOT;
		}
	}